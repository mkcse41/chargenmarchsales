package minifier;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Minifier {
/*
 * Pass path of javascript & CSS folder in argument*/
	public static void main(String[] args) throws Exception{
		System.out.println("Minifying");
		String path = args[0];
		//path=/home/gambit/Desktop/chargenmarch/chargenmarch/trunk/ChargenMarch/src/main/webapp/javascript/
		minifyFolder(path);
	}

	public static void minifyFolder(String folderName) throws Exception{
		File folder = new File(folderName);
		File[] listOfFiles = folder.listFiles();
		String outputDir = System.getProperty("user.dir");
		for(File file : listOfFiles){
			if(!file.isDirectory()){
				System.out.println("Minifying "+file.getName());
				if(file.getName().endsWith(".js") && !file.getName().contains(".min.js")){
					minifyJS(file, outputDir, ".js");
				} else if(file.getName().endsWith(".css") && !file.getName().contains(".min.css")) {
					minifyCSS(file, outputDir, ".css");
				}
			}
		}
	}
	
	public static void minifyJS(File file, String outputPath, String fileType) throws Exception{
		final URL url = new URL("https://javascript-minifier.com/raw");
		minify(file, url, outputPath, fileType);
	}
	public static void minifyCSS(File file, String outputPath, String fileType) throws Exception{
		final URL url = new URL("http://cssminifier.com/raw");
		minify(file, url, outputPath, fileType);
	}
	
	public static void minify(File file, URL url, String outputPath, String fileType) throws Exception{
		// JS File you want to compress
		byte[] bytes = Files.readAllBytes(Paths.get(file.getAbsolutePath()));

		final StringBuilder data = new StringBuilder();
		data.append(URLEncoder.encode("input", "UTF-8"));
		data.append('=');
		data.append(URLEncoder.encode(new String(bytes), "UTF-8"));

		bytes = data.toString().getBytes("UTF-8");

		final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setDoOutput(true);
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		conn.setRequestProperty("charset", "utf-8");
		conn.setRequestProperty("Content-Length", Integer.toString(bytes.length));

		try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
		    wr.write(bytes);
		}

		final int code = conn.getResponseCode();

		System.out.println("Status: " + code);

		if (code == 200) {
		    final BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		    String inputLine;
		    PrintWriter out = new PrintWriter(outputPath+"/"+fileType.substring(1)+"/"+file.getName().substring(0,file.getName().indexOf(fileType))+".min"+fileType);
		    while ((inputLine = in.readLine()) != null) {
		    	out.append(inputLine);
		    }
		    in.close();

		    System.out.println("\n----");
		} else {
		    System.out.println("Oops");
		}
	}
}