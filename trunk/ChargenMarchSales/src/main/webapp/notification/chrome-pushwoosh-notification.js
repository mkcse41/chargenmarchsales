var APPLICATION_CODE = "9ABE6-1CD13"; // Your Application Code from Pushwoosh
var SERVICE_WORKER_URL = basepath + '/service-worker.js';
var pushwooshUrl = "https://cp.pushwoosh.com/json/1.3/";
var hwid = "";
var isPushEnabled = false;
var is_private;
var WEB_SITE_PUSH_ID = 'web.com.ChargenMarch';        // Your unique reverse-domain Website Push ID from the Developer Center, starts with "web."
var isFirstRegister = "isFirstRegister";

function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
}

function startPushWooshService() {
    try{
        //Save id first time case
        saveBrowserNotificationId();

     if(isSupportNotification()){
        // GA
        createWapGAClickEventTrackingNonInteraction('pushNotification', 'installation', 'checking_compatibility');
        // Check that service workers are supported, if so, progressively
        // enhance and add push messaging support, otherwise continue without it.
        if ('serviceWorker' in navigator) {
            //GA
            createWapGAClickEventTrackingNonInteraction('pushNotification', 'installation', 'compatible');
            navigator.serviceWorker.register(SERVICE_WORKER_URL)
                .then(function(serviceWorkerRegistration) {
                    // Are Notifications supported in the service worker?
                    if (!('showNotification' in ServiceWorkerRegistration.prototype)) {
                        //GA
                        createWapGAClickEventTrackingNonInteraction('pushNotification', 'installation', 'notification_not_supported');
                        return;
                    }
                    //GA
                   createWapGAClickEventTrackingNonInteraction('pushNotification', 'installation', 'notification_supported');
                    // Check the current Notification permission.
                    // If its denied, it's a permanent block until the
                    // user changes the permission
                    if (Notification.permission === 'denied') {
                        //GA
                        createWapGAClickEventTrackingNonInteraction('pushNotification', 'installation', 'permission_denied');
                        writeInLocal("pushnotification","denied");
                        return;
                    }
                    //GA
                    createWapGAClickEventTrackingNonInteraction('pushNotification', 'installation', 'permission_granted');
                    // Check if push messaging is supported
                    if (!('PushManager' in window)) {
                        //GA
                        createWapGAClickEventTrackingNonInteraction('pushNotification', 'installation', 'push_manager_not_supported');
                        return;
                    }
                    //GA
                    createWapGAClickEventTrackingNonInteraction('pushNotification', 'installation', 'push_manager_supported');
                   

                    serviceWorkerRegistration.pushManager.getSubscription()
                        .then(function(subscription) {
                            // Enable any UI which subscribes / unsubscribes from
                            // push messages.
                            if (!subscription) {
                               // GA
                                //show smoke screen
                                showSmokeScreen();

                                createWapGAClickEventTrackingNonInteraction('pushNotification', 'subscription_Init', 'scriptCalled');
                               
                                serviceWorkerRegistration.pushManager.subscribe({
                                    name: 'push',
                                    userVisibleOnly: true
                                }).then(function(subscription) {
                                        // GA
                                        createWapGAClickEventTrackingNonInteraction('pushNotification', 'installation', 'success'); 
                                        $(".notifaction").hide();
                                        isPushEnabled = true;
                                        console.log(subscription);
                                        pushToken = getPushToken(subscription);
                                        hwid = generateHwid(pushToken);
                                        pushwooshRegisterDevice(pushToken, hwid);
                                        writeInLocal("pushnotification","success");
                                    })
                                    .catch(function(e) {
                                        if (Notification.permission === 'denied') {
                                            $(".notifaction").hide();
                                            // The user denied the notification permission which
                                            // means we failed to subscribe and the user will need
                                            // to manually change the notification permission to
                                            // subscribe to push messages
                                            deniedCallback();
                                          //Ga
                                          createWapGAClickEventTrackingNonInteraction('pushNotification', 'installation', 'permission_denied'); 
                                        } else {
                                            $(".notifaction").hide();
                                            // A problem occurred with the subscription; common reasons
                                            // include network errors, and lacking gcm_sender_id and/or
                                            // gcm_user_visible_only in the manifest.
                                            delayedCallback();
                                          //GA
                                          createWapGAClickEventTrackingNonInteraction('pushNotification', 'installation', 'permission_delayed'); 
                                        }
                                    });
                                return;
                            }

                            // Keep your server in sync with the latest hwid/pushToken
                            var pushToken = getPushToken(subscription);
                            hwid = generateHwid(pushToken);
                            if (navigator.serviceWorker.controller) {
                                navigator.serviceWorker.controller.postMessage({'hwid': hwid, 'applicationCode': APPLICATION_CODE, 'pushwooshUrl': pushwooshUrl});
                            }
                            updateUser(hwid, pushToken,"yes");
                           //GA
                           createWapGAClickEventTrackingNonInteraction('pushNotification_WAP', 'setCookies', 'scriptCalled');
                           
                           // set cookie, this code is external modified.
                            // Set your UI to show they have subscribed for
                            // push messages
                            isPushEnabled = true;
                            successCallback();
                        })
                        .catch(function(err) {
                            createWapGAClickEventTrackingNonInteraction('pushNotification', 'subscription_Init', 'error_during_subscription');
                            $(".notifaction").hide();
                        });
                })
                .catch(function(err) {
                    createWapGAClickEventTrackingNonInteraction('pushNotification', 'subscription_Init', 'Error_while_service_worker_registration');
                    $(".notifaction").hide();
                });
        } else {
            createWapGAClickEventTrackingNonInteraction('pushNotification', 'Installation', 'service_worker_not_supported');        
        }
      }else{
           $(".notifaction").hide();
      }
    }catch(e){
        $(".notifaction").hide();
    }
}


function showSmokeScreen(){
     if(!is_private && !isFireFoxBrowser()  && isChromeBrowser()){
             setTimeout(function(){ 
                $(".notifaction").show();
              },1000);
     }else if(!is_private && isFireFoxBrowser() && !isChromeBrowser()){
            $(".notifaction").show();
            setTimeout(function(){ 
                    $(".notifaction").hide();
            },1000);
     }
}

function isSupportNotification(){
     if(isChromeBrowser()){
           if(getChromeVersion()!=false && getChromeVersion()>43){
              $(".notifaction img").css("top","126px");
              return true;
           }
           return false;
     }else if(isFireFoxBrowser()){
          $(".notifaction img").css("top","164px");
          return true;
     }
     return false;
}

function isFireFoxBrowser(){
    var isFirefox = typeof InstallTrigger !== 'undefined';
    // At least Safari 3+: "[object HTMLElementConstructor]"
    if(isFirefox){
        return true;
    }
    return false;
}

function isSafariBrowser(){
    var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
    if(isSafari){
        return true;
    }
    return false;
}

function subscribe() {
    console.log("Try to subscribe for push notifications");
    navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {
        serviceWorkerRegistration.pushManager.subscribe()
            .then(function(subscription) {
                // The subscription was successful
                isPushEnabled = true;
                console.log(subscription);
                pushToken = getPushToken(subscription);
                hwid = generateHwid(pushToken);
                pushwooshRegisterDevice(pushToken, hwid);
            })
            .catch(function(e) {
                if (Notification.permission === 'denied') {
                    // The user denied the notification permission which
                    // means we failed to subscribe and the user will need
                    // to manually change the notification permission to
                    // subscribe to push messages
                    createWapGAClickEventTrackingNonInteraction('pushNotification', 'subscription', 'permission_denied');
                } else {
                    // A problem occurred with the subscription; common reasons
                    // include network errors, and lacking gcm_sender_id and/or
                    // gcm_user_visible_only in the manifest.
                    createWapGAClickEventTrackingNonInteraction('pushNotification', 'subscription', 'Unable_to_subscribe');
                }
            });
    });
}



function unsubscribe() {
    navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {
        // To unsubscribe from push messaging, you need get the
        // subscription object, which you can call unsubscribe() on.
        serviceWorkerRegistration.pushManager.getSubscription().then(
            function(pushSubscription) {
                // Check we have a subscription to unsubscribe
                if (!pushSubscription) {
                    // No subscription object, so set the state
                    // to allow the user to subscribe to push
                    isPushEnabled = false;
                    return;
                }

                var pushToken = getPushToken(pushSubscription);

                // We have a subscription, so call unsubscribe on it
                pushSubscription.unsubscribe().then(function(successful) {
                    isPushEnabled = false;
                    var hwdi = generateHwid(pushToken);
                    pushwooshUnregisterDevice(hwdi);
                    updateUser(hwdi, pushToken,"no");
                }).catch(function(e) {
                    // We failed to unsubscribe, this can lead to
                    // an unusual state, so may be best to remove
                    // the users data from your data store and
                    // inform the user that you have done so

                    console.log('Unsubscription error: ', e);
                });
            }).catch(function(e) {
                console.error('Error thrown while unsubscribing from push messaging.', e);
            });
    });
}

// For more information see Pushwoosh API guide https://www.pushwoosh.com/programming-push-notification/pushwoosh-push-notification-remote-api/

function createUUID(pushToken) {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 32; i++) {
        s[i] = hexDigits.substr(pushToken.charCodeAt(i) % hexDigits.length, 1);
    }
    return s.join("");
}

function generateHwid(pushToken) {
    var hwid = APPLICATION_CODE + '_' + createUUID(pushToken);
    return hwid;
}

// Registers device for the application
function pushwooshRegisterDevice(pushToken, hwid){
    //GA
    $(".notifaction").hide();
    closeSmokeScreen();
    createWapGAClickEventTrackingNonInteraction('pushNotification', 'Registeration', 'Registering'); 
    var deviceType = 11; // chrome
    var isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    if (isFirefox) {
        deviceType = 12;
    }
    try {
        var xhr = new XMLHttpRequest(),
            url = pushwooshUrl + 'registerDevice',
            params = {
                "request":{
                    "application": APPLICATION_CODE,
                    "push_token": pushToken,
                    "language": window.navigator.language || 'en',  // optional
                    "hwid": hwid,
                    "timezone": (new Date).getTimezoneOffset(), // offset in seconds
                    "device_model": get_browser_version(),
                    "device_type": deviceType
                }
            };

        xhr.open('POST', url, true);
        xhr.setRequestHeader('Content-Type', 'text/plain;charset=UTF-8');
        xhr.send(JSON.stringify(params));
        xhr.onload = function(){
            if(this.status == 200){
                var response = JSON.parse(this.responseText);
                if (response.status_code == 200) {
                        successCallback();
                    
                    console.log('registerDevice call to Pushwoosh has been successful');
                }
                else {
                    console.log('Error occurred during the registerDevice call to Pushwoosh: ' + response.status_message);
                }
            }else{
                console.log('Error occurred, status code:' + this.status);
            }
        };
        xhr.onerror = function(){
            console.log('Pushwoosh response status code to registerDevice call is not 200')
        };
    } catch(e) {
        console.log('Exception while registering the device with Pushwoosh: ' + e);
        return;
    }   
}


function deniedCallback(){
    writeInLocal("pushnotification","denied");
    //setTimeout("window.close()",10);
   
}
function delayedCallback(){
    writeInLocal("pushnotification","delayed");
}
function successCallback(){
   set_Cookie("hwid", hwid, 180, '/' ,'' ,'');
   var param = getParameterByName('p');
   $(".notifaction").hide();
   if( param == "noprompt"){
        $('#waiting').html("Thank You.");
        setTimeout("window.close()",2000);
   }
   checkService();
}

// Remove device from the application
function pushwooshUnregisterDevice(hwid) {
    // GA
    createWapGAClickEventTrackingNonInteraction('pushNotification', 'Registeration', 'UnRegistering');
    try {
        var xhr = new XMLHttpRequest(),
            url = pushwooshUrl + "unregisterDevice",
            params = {
                request: {
                    application: APPLICATION_CODE,
                    hwid: hwid
                }
            };
        xhr.open('POST', url, true);
        xhr.setRequestHeader('Content-Type', 'text/plain;charset=UTF-8');
        xhr.send(JSON.stringify(params));
        xhr.onload = function(){
            if(this.status == 200){
                var response = JSON.parse(this.responseText);
                if (response.status_code == 200) {
                    createWapGAClickEventTrackingNonInteraction('pushNotification', 'Registeration', 'unregisterDevice_call_successful');
                    //console.log('unregisterDevice call to Pushwoosh has been successful');
                }
                else {
                    createWapGAClickEventTrackingNonInteraction('pushNotification', 'Registeration', 'Error_during_unregisterDevice');
                    //console.log('Error occurred during the unregisterDevice call to Pushwoosh:: ' + response.status_message);
                }
            }else{
                console.log('Error occurred, status code::' + this.status);
            }
        };
        xhr.onerror = function(){
            console.log('Pushwoosh response status code to unregisterDevice call in not 200');
        };
    } catch(e) {
        console.log('Exception while unregistering the device: ' + e);
        return;
    }
}


function getPushToken(pushSubscription) {
    var pushToken = '';
    if (pushSubscription.subscriptionId) {
        pushToken = pushSubscription.subscriptionId;
        console.log("Chrome 42, 43, 44: " + pushToken);
    }
    else {
        pushToken = pushSubscription.endpoint.split('/').pop();
        console.log("Chrome 45+: " + pushToken);
    }
    try{
        saveGCMId(pushToken);
    }
    catch(e){}
    return pushToken;
}


function get_browser_version() {
    var ua = navigator.userAgent, tem,
        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem =  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
        if(tem!= null) return 'Opera '+tem[1];
    }
    M = M[2] ? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/([.\d]+)/i))!= null)
        M.splice(1, 1, tem[1]);
    return M.join(' ');
}
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
function updateUser(hwid, pushToken,isactive){
    var obj = new object();
    obj.userid = get_Cookie("UserId");
    obj.hwid= hwid;
    obj.token = pushToken;
    obj.isactive = isactive;
 //   $.post(basepath+"/ajaxCallAction.do",{parameter:'chromeNotificationUser',json:JSON.stringify(obj)});
}

function createWapGAClickEventTrackingNonInteraction(category,event,label){
    try{
        ga('send', 'event', category, event, label, 0,true);
                //ga('send', 'event', category, event, label, 0,{nonInteraction: true});
    }catch(e){}
    
}

var previousUrl;
function checkService(){
    previousUrl = document.referrer;
    var hwid = get_Cookie("hwid");
    if (hwid == undefined || hwid == null || hwid == "" || hwid == 0) {
        if ($('#switch_input').length) {
           document.getElementById('switch_input').checked = false;
        }
    }else{
        if ($('#switch_input').length) {
           document.getElementById('switch_input').checked = true;
        }
    }
}

function enabledisable(){
    if( document.getElementById('switch_input').checked == true){
        showPushNotificationPopup();
      }
     if( document.getElementById('switch_input').checked == false){
        unsubscribePushWoosh();
      }
  }

 function set_Cookie(C,E,A,H,D,G) {
    var B=new Date();
    B.setTime(B.getTime());
    if(A){
        A=A*1000*60*60*24*1;
    }
    var F=new Date(B.getTime()+(A));
    document.cookie=C+"="+escape(E)+((A)?";expires="+F.toGMTString():"")+((H)?";path="+H:"")+((D)?";domain="+D:"")+((G)?";secure":"");
}
 
/* Function to get cookie. */
function get_Cookie(adName){
    cName ="";
    var pCOOKIES = new Array();
    pCOOKIES = document.cookie.split('; ');
    for(var bb = 0; bb < pCOOKIES.length; bb++){
        var NmeVal = new Array();
        NmeVal = pCOOKIES[bb].split('=');
        if(NmeVal[0] == adName){
            cName = unescape(NmeVal[1]);
        }
    }
    return cName;
}

function showPushNotificationPopup(){   
        //GA
         createWapGAClickEventTrackingNonInteraction('pushNotification_WAP', 'subscribe', 'on_button');
         startPushWooshService();  
}


function unsubscribePushWoosh(){
    // GA
    createWapGAClickEventTrackingNonInteraction('pushNotification_WAP', 'delete_cookie', 'on_button');
      var pushwooshId = get_Cookie("hwid");
      console.log(pushwooshId);
      delete_cookie("hwid");
      document.getElementById('switch_input').checked = false;
      unsubscribe();
    }

 
function delete_cookie( name ) {
     set_Cookie(name,'' , -1, '/' ,'' ,'');
}

function findTargetInNewTab(target)
{
    window.open(target);
}

function findTargetPrevious(){
    location.href=previousUrl;
}
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
function showHideContent(){
    var param = getParameterByName('p');
    if( param == "noprompt"){
        $('#waiting').show();
        $('#waitingImg').show();
    }
    else{
        $('#direct').show();
    }
}


/**
This Method used to show  browser notification screen.

*/
function pushWooshNotification(){
    setTimeout("pushWooshNotificationDelayed()",30000);
    setTimeout("pushGTMRelatedTriggers()",2000);
}
function pushGTMRelatedTriggers(){
    var notificationPopupResonse = getFromLocal("pushnotification");
    if( notificationPopupResonse != "" ){
       dataLayer.push({'pushnotification':notificationPopupResonse }); 
    }
}
function pushWooshNotificationDelayed(){
     if(isFireFoxBrowser()){
         isPrivateBrowser();
         startPushWooshService();
     }else if(isChromeBrowser()){
         isPrivateBrowser(startPushWooshService);
     }/*else if(isSafariBrowser()){
         isPrivateBrowser();
         startSafariNotification();
     }*/
}

function isPrivateBrowser(callback){
    if (window.webkitRequestFileSystem) {
        window.webkitRequestFileSystem(
            window.TEMPORARY, 1,
            function() {
                is_private = false;
                callback();
            },
            function(e) {
                console.log(e);
                is_private = true;
                callback();
            }
        );
    } 
}



function closeSmokeScreen(){
    setTimeout(function(){ 
        $(".notifaction").hide();
   },1050);
}

function closeBackgroundScreen(){
   $(".notifaction").hide();
}


function getChromeVersion () {     
    var raw = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./);
    return raw ? parseInt(raw[2], 10) : false;
}

function isChromeBrowser(){
   if (window.chrome && window.chrome.webstore) {
      return true;     
   }
   return false;
}

/**
This Script use in Safari Browser.

*/
var checkRemotePermission = function (permissionData) {
    console.log(permissionData);
    if (permissionData.permission === 'default') {
        console.log('This is a new web service URL and its validity is unknown.');
         if(!is_private && isSafariBrowser()){
                $(".notifaction img").css("top","200px");
                $(".notifaction img").css("left","500px");
                $(".notifaction").show();
                setTimeout(function(){ 
                        $(".notifaction").hide();
                },3000);
        }
        window.safari.pushNotification.requestPermission(
            pushwooshUrl + 'safari',
            WEB_SITE_PUSH_ID,
            { application: APPLICATION_CODE },
            checkRemotePermission    // The callback function, it not called after Allow/Not Allow
        );
        try {
            localStorage.setItem(isFirstRegister, "true");
        }
        catch(e) {
            console.log("Storage failed");
        }
    } else if (permissionData.permission === 'denied') {
        console.log('The user said no.');
        $(".notifaction").hide();
    } else if (permissionData.permission === 'granted') {
        $(".notifaction").hide();
        console.log('The web service URL is a valid push provider, and the user said yes.');
        console.log('You deviceToken is ' + permissionData.deviceToken);
        // set system tags
        try {
            if (localStorage.getItem(isFirstRegister)) {
                var tags = {
                    "Language": window.navigator.language || 'en',
                    "Device Model": get_browser_version()
                };
                pushwooshSetTags(permissionData.deviceToken, tags);
            }
            localStorage.removeItem(isFirstRegister);
        }
        catch (e) {
            console.log("Storage failed");
        }
    }
};




function startSafariNotification(){
      if ('safari' in window && 'pushNotification' in window.safari) {
        var permissionData = window.safari.pushNotification.permission(WEB_SITE_PUSH_ID);
        checkRemotePermission(permissionData);
    } else {
        console.log('Push Notifications are available for Safari browser only');
    }
    // send to Pushwoosh push open statistics
    try {
        if (navigator.userAgent.indexOf('Safari') > -1) {
            var hashReg = /#P(.*)/,
                hash = decodeURIComponent(document.location.hash);

            if ('safari' in window && 'pushNotification' in window.safari) {
                var permissionData = window.safari.pushNotification.permission(WEB_SITE_PUSH_ID);
            }

            if (hashReg.test(hash) && permissionData) {
                var xhr = new XMLHttpRequest(),
                    url = pushwooshUrl + 'pushStat',
                    params = {
                        "request":{
                            "application": APPLICATION_CODE,
                            "hwid": permissionData.deviceToken.toLowerCase(),
                            "hash": hashReg.exec(hash)[1]
                        }
                    };

                xhr.open('POST', url, true);
                xhr.setRequestHeader('Content-Type', 'text/plain;charset=UTF-8');
                xhr.send(JSON.stringify(params));
            }
        }
    } catch(e) { }
}

function pushwooshSetTags(hwid, tags) {
    console.log('Sending setTags call to Pushwoosh');
    try {
        var xhr = new XMLHttpRequest(),
            url = pushwooshUrl + 'setTags',
            params = {
                request:{
                    application: APPLICATION_CODE,
                    hwid: hwid.toLowerCase(),
                    tags: tags
                }
            };

        xhr.open('POST', url, false);
        xhr.setRequestHeader('Content-Type', 'text/plain;charset=UTF-8');
        xhr.onload = function() {
            if(this.status == 200) {
                var response = JSON.parse(this.responseText);
                if (response.status_code == 200) { console.log('Set tags method were successfully sent to Pushwoosh'); }
                else { console.log('Error occurred while sending setTags to Pushwoosh: ' + response.status_message); }
            } else {
                console.log('Error occurred, status code::' + this.status);
            }
        };
        xhr.onerror = function(){ console.log('Pushwoosh response status code to pushStat call in not 200'); };
        xhr.send(JSON.stringify(params));
    } catch(e) {
        console.log('Exception while sending setTags to Pushwoosh: ' + e);
        return;
    }
}

function writeInLocal(name,val){
    if (typeof(localStorage) !== "undefined") {
         localStorage.setItem(name, val);
    }
}


function getFromLocal(name){
    if (typeof(localStorage) !== "undefined" && localStorage.getItem(name) !== null) {
        return localStorage.getItem(name);
    }
    return "";
}

window.onbeforeunload = function(){
    if ('safari' in window && 'pushNotification' in window.safari) {
        var permissionData = window.safari.pushNotification.permission(WEB_SITE_PUSH_ID);
        checkRemotePermission(permissionData);
    }
};

function saveGCMId(e){
    var i=window.localStorage.getItem("not_id");
    if( !i || i!=e){
        window.localStorage.setItem("not_id",e);
        var cid = window.localStorage.getItem("cid");
        if(undefined != cid){
                $.ajax({
                type : "POST",
                url : basepath + "/user/saveBrowserNotificationLogs",
                data : "browser_notification_id=" + e +"&id=" + cid,
                success : function(response) {
                },
                error : function(e) {
                }
              });
        }
    } 
}

function saveBrowserNotificationId(){
    var id = window.localStorage.getItem("not_id");
    var cid = window.localStorage.getItem("cid");
    if(id != undefined && cid != undefined){
                $.ajax({
                type : "POST",
                url : basepath + "/user/saveBrowserNotificationLogs",
                data : "browser_notification_id=" + id +"&id=" + cid,
                success : function(response) {
                },
                error : function(e) {
                }
              });
    } 
}