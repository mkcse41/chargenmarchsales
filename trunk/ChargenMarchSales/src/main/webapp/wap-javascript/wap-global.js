$('.LoginForm,#changePasswordDiv').find('input, textarea').on('keyup blur focus', function (e) {
  
  var $this = $(this),
      label = $this.prev('label');

    if (e.type === 'keyup') {
      if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
      if( $this.val() === '' ) {
        label.removeClass('active highlight'); 
      } else {
        label.removeClass('highlight');   
      }   
    } else if (e.type === 'focus') {
      
      if( $this.val() === '' ) {
        label.removeClass('highlight'); 
      } 
      else if( $this.val() !== '' ) {
        label.addClass('highlight');
      }
    }

});

$('.Logintab a').on('click', function (e) {
  
  e.preventDefault();
  
  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');
  
  target = $(this).attr('href');

  $('.tab-content-sub > div').not(target).hide();
  
  $(target).fadeIn(600);
  
});

$('.scrollmenu a').on('click', function (e) {
  
  e.preventDefault();
  console.log($(this));
  $(this).parent().addClass('activeTab');
  $(this).parent().siblings().removeClass('activeTab');

  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();
  
  $(target).fadeIn(600);

});  

$( document ).ready(function() {
  var $lateral_menu_trigger = $('#cd-menu-trigger'),
    $content_wrapper = $('.cd-main-content'),
    $navigation = $('header');

  $lateral_menu_trigger.on('click', function(event){
    event.preventDefault();
    $('body').toggleClass('overflow-hidden');
    $('#cd-lateral-nav').toggleClass('lateral-menu-is-open');
    $(".menuOverlay").show();
  });

  $('.menuOverlay').on('click', function(event){
    $('body').removeClass('overflow-hidden');
    $('#cd-lateral-nav').removeClass('lateral-menu-is-open');
    $(".menuOverlay").hide(); 
  });
  
  
  $('.item-has-children').children('a').on('click', function(event){
    event.preventDefault();
    $(this).toggleClass('submenu-open').next('.sub-menu').slideToggle(200).end().parent('.item-has-children').siblings('.item-has-children').children('a').removeClass('submenu-open').next('.sub-menu').slideUp(200);
  });

  $('.login-form,#OTPVerification,.reset-form').find('input').on('click', function(event){
      $(this).siblings('label').addClass('move-up');
  });
  $('.login-form,#OTPVerification,.reset-form').find('input').on('blur', function(event){
    if($(this).val().length==0){  
      $(this).siblings('label').removeClass('move-up');
    }
    else{
     $(this).siblings('label').addClass('move-up'); 
    }
  });

});

function findTarget(url){
   location.href=url;
}

function multipleDataPostRedirect(formAction, jsonParam, jsonParamValue,method) {
    if(method==undefined){
        method = "post";
    }
    var form = "<form action='" + formAction + "' method='"+method+"' id='insForm'>";
    var d = jsonParam.length;
    for (var b = 0; b < d; b++) {
        form = form + "<input type='hidden' name='" + jsonParam[b] + "' value=\"" + jsonParamValue[b] + '" />';
    }
    form = form + "</form>";
    $("body").append(form);
     setTimeout(function(){
                                loaderEnd();
                            },300);
    $("#insForm").submit();
}


function dynamicPostRequestForm(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.
    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);
    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
}


function hideShowDiv(hideDivId,showDivId){
	$("#"+hideDivId).hide();
	$("#"+showDivId).show();
}

function validatePassword(fld) {
    var error = "";
    var illegalChars = /[\W_]/; // allow only letters and numbers 
    if (fld.val() == "") {
        error = "Please Enter a password.\n";
    } else if ((fld.val().length < 7) || (fld.val().length > 20)) {
        error = "Please Enter Password at least 7 characters. \n";
    } /*else if (illegalChars.test(fld.val())) {
        error = "Please Enter Password only letter and number.\n";
    } else if (!((fld.val().search(/(a-z)+/)) && (fld.val().search(/(0-9)+/)))) {
        error = "The password must contain at least one numeral.\n";
    }*/
   return error;
}  

function trim(s){
	if(s!=undefined && s!=""){
      return s.replace(/^\s+|\s+$/, '');
	}
	return s;
} 

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    } else {
        return true;
    }      
}

function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if (charCode==32 || charCode==8 || charCode==46 || (charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
            return true;
        else
            return false;
    }
    catch (err) {
        alert(err.Description);
    }
}

function spaceNotAllowed(e){
    if (e.which==32){
        return false;
    }
}

function allowAlphaNumeric(e){
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }

    return false;
}

function validateEmail(email,errorId) { //Validates the email address
    var emailRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(email.val()!=undefined && email.val()!="" && emailRegex.test(email.val())){
    	return true;
    }
    $('#'+errorId).html('Please Enter valid Email Id.'); 
    email.focus();
    return false;
}


function validatePhone(phone,errorId) { //Validates the phone number
    var phoneRegex = /^[0]?[789]\d{9}$/; // Change this regex based on requirement
    if(phone.val()!=undefined && phone.val()!="" && phone.val().length==10 &&  phoneRegex.test(phone.val())){
    	return true;
    }
    $('#'+errorId).html('Please Enter valid Mobile Number.'); 
    phone.focus();
    return false;
}

function validateName(uname,errorId){   
	var letters = /^[A-z ]+$/;  
	if(uname.val()!=undefined && uname.val()!="" && trim(uname.val()).match(letters)){  
	return true;  
	} else{  
  $('#'+errorId).html('Username must have alphabet characters only');  
	uname.focus();  
	return false;  
	}  
}

function loaderStart(){
  $("#loader-wrapper").removeClass("DispNone");
  $("#mask").removeClass("DispNone");

}

function loaderEnd(){
  $("#loader-wrapper").addClass("DispNone");
  $("#mask").addClass("DispNone");
}


function validateMobileNumber(phone) { //Validates the phone number
    var phoneRegex = /^[0]?[789]\d{9}$/; // Change this regex based on requirement
    if(phone.val()!=undefined && phone.val()!="" && phone.val().length==10 &&  phoneRegex.test(phone.val())){
      return true;
    }
    phone.focus();
    return false;
}

function isBlankOrNull(stringToCheck){
  if(stringToCheck==''){
     return true;
  } else return false;
}
/**
 * This method used to user register source wise 
 */
function userRegister(usernameId,emailId,mobileNo,password,source,isCAMLogin,errorId){
	var nameValue = $('#'+usernameId).val();  
	var emailIdValue = $('#'+emailId).val();  
	var mobileNoValue = $('#'+mobileNo).val(); 
  var passwordValue = $('#'+password).val();
  var quickRechargeCheckVar = $('#quickRechargeCheckId').val();
  var userReferralCode = $('#charger_reg_referral_code').val();
	if(isCAMLogin == 'false'){
	    $('#autopass').prop('checked', true);
	    passwordValue = "";
	 }
    if(validateName($('#'+usernameId),errorId) && validateEmail($('#'+emailId),errorId) && validatePhone($('#'+mobileNo),errorId) && validatePasswordReg($('#'+password),errorId) ){
               loaderStart();    		
               $.ajax({  
    		    type : "Post",   
    		    async : false,
    		    url : basepath+"/user/registered.htm",   
    		    data : "name=" + nameValue.trim() + "&emailId=" + emailIdValue + "&mobileNo="  
    		      + mobileNoValue+ "&password="+passwordValue+ "&source=" + source + "&userReferralCode="+userReferralCode+"&isCAMLogin="+isCAMLogin,  
    		    success : function(response) {  
    		    	//alert(json.message);
    		    	var json = JSON.parse(response);
               setTimeout(userReferralCode="+userReferralCode+"&function(){
                              loaderEnd();
                        },300);  
    		    	if(json.message=="OTP_Screen"){
    		    	    loaderEnd();
                  if(isCAMLogin=='false'){
                      showOTPScreenforThirdParty(); 
                  }else{
                     showOTPScreen();
                  }
    		    	}
              else if(json.message=="User Already Registered"){
                  loaderEnd();
                  redirectReviewOrder(errorId,json.message,'',quickRechargeCheckVar); 
              }else{
                 loaderEnd();
                 $('#'+errorId).html(json.message);
              }
    		    },  
    		    error : function(e) { 
             loaderEnd(); 
             $('#'+errorId).html('Error: ' + e); 
             alert('Error: ' + e);  
    		    }  
    	    });
       }
}


function showOTPScreen(){
  $('#OTPVerification').removeClass('DispNone');
  $('#userRegisteFrm').addClass('DispNone');
}

function submitUserOTP(userOTP,errorId){
  var userOTPValue = $('#'+userOTP).val();
  var quickRechargeCheckVar = $('#quickRechargeCheckId').val();
  if( validateOTP($('#'+userOTP),errorId) ){
          loaderStart();        
          $.ajax({  
            type : "Post",   
            async : false,
            url : basepath+"/user/otpverification.htm",   
            data : "otp=" + userOTPValue.trim(),  
            success : function(response) {  
              //alert(json.message);
              var json = JSON.parse(response);
               setTimeout(function(){
                              loaderEnd();
                        },300);  
              if(json.message=="User Registered Successfully"){
                  redirectReviewOrder('',undefined,'',quickRechargeCheckVar);
              }           
                else{
                 $('#'+errorId).html(json.message);
              }
            },  
            error : function(e) {  
             $('#'+errorId).html('Error: ' + e); 
             alert('Error: ' + e);  
            }  
          });
  }
}

function userLogin(emailId,password,errorId){
  var passwordValue = $('#'+password).val();  
  var emailIdValue = $('#'+emailId).val();  
  var quickRechargeCheckVar = $('#quickRechargeCheckId').val();
  if(validateEmail($('#'+emailId),errorId) && password!=undefined && password!=""){
     loaderStart();
    $.ajax({  
        type : "Post",   
        url : basepath+"/user/login",   
        data : "emailId=" + emailIdValue + "&password="  
          + passwordValue,  
        success : function(response) {  
          var json = JSON.parse(response); 
           setTimeout(function(){
                          loaderEnd();
                    },300);
          if(json.message=="success"){
                     redirectReviewOrder('',undefined,'',quickRechargeCheckVar);
          }
          else if(json.message=="nonverified"){
                     getOTPResponse(errorId,emailIdValue);  
          }
          else if(json.message=="user not exists"){
                $('#'+errorId).html('User not exists.');
                return false;
          } 

          else{
            $('#'+errorId).html('Please Enter Valid Email Id and password.');
          }
        },  
        error : function(e) {  
        $('#'+errorId).html('Error: ' + e);
         alert('Error: ' + e);   
        }  
      });
   }
}


function getOTPResponse(errorId,emailIdValue){
            loaderStart();       
          $.ajax({  
            type : "Post",   
            async : false,
            url : basepath+"/user/otp_response.htm",
            data : "emailId=" + emailIdValue,   
            success : function(response) {  
              //alert(json.message);
              var json = JSON.parse(response);
               setTimeout(function(){
                              loaderEnd();
                        },300);  
               console.log(json.message);
              if(json.message=="OTP_Screen"){
                    showOTPScreenforNonVerifiedUser();
              }           
                else{
                 $('#'+errorId).html(json.message);
              }
            },  
            error : function(e) {  
             $('#'+errorId).html('Error: ' + e); 
             alert('Error: ' + e);  
            }  
          });
}

function showOTPScreenforNonVerifiedUser(){
  $('#loginFrm').addClass('DispNone');
  $('#OTPVerificationLogin').removeClass('DispNone');
  var left = $('.popup').width()/2;
      left = parseInt(left)-19;
  $('.popup').css("margin-left","-"+left+"px");
} 

function addUserInfoByFbGoogle(){  
     userRegisterByThirdParty('chargers_reg_name','charger_reg_emailId','charger_reg_mobileNo','WAP','false','errorIdDemo');
} 

function userRegisterByThirdParty(usernameId,emailId,mobileNo,source,isCAMLogin,errorId){
  var nameValue = $('#'+usernameId).val();  
  var emailIdValue = $('#'+emailId).val();  
  var mobileNoValue = $('#'+mobileNo).val(); 
  var quickRechargeCheckVar = $('#quickRechargeCheckId').val();
  var passwordValue = "";
  if(validateName($('#'+usernameId),errorId) && validateEmail($('#'+emailId),errorId) && validatePhone($('#'+mobileNo),errorId) ){
              loaderStart();        
               $.ajax({  
            type : "Post",   
            async : false,
            url : basepath+"/user/registeredOther.htm",   
            data : "name=" + nameValue.trim() + "&emailId=" + emailIdValue + "&mobileNo="  
              + mobileNoValue+ "&password="+passwordValue+ "&source=" + source + "&isCAMLogin="+isCAMLogin,  
            success : function(response) {  
              //alert(json.message);
              var json = JSON.parse(response);
               setTimeout(function(){
                              loaderEnd();
                        },300); 
                        console.log(json.message); 
                        console.log(isCAMLogin);
              if(json.message=="Show Mobile Number Screen."){
                  /*redirectReviewOrder(); */
                  showMobileScreen();
              }
              else if(json.message=="User Already Registered" && isCAMLogin=='false'){
                  redirectReviewOrder(errorId,json.message,isCAMLogin,quickRechargeCheckVar); 
              }
                else{
                 $('#'+errorId).html(json.message);
              }
            },  
            error : function(e) {  
             $('#'+errorId).html('Error: ' + e); 
             alert('Error: ' + e);  
            }  
          });
       }
}

function showMobileScreen(){
  $('#MobileVerification').removeClass('DispNone');
  $('#loginFrm').addClass('DispNone');
} 

function submitUserMobile(mobileNo,errorId){
  var mobileNumber = $('#'+mobileNo).val();
   $('#charger_reg_mobileNo').val(mobileNumber);
   userRegister('chargers_reg_name','charger_reg_emailId','charger_reg_mobileNo','charger_reg_password','WEB','false',errorId); 
}


function userLoginRegister(activeId){
      $("#login-page").hide();
      $("#recoverPassword").hide();
      $("#userRegister").hide();
      $("#"+activeId).show();
}

function forgotPassword(emailfieldId,errorId){
   var emailId = $("#"+emailfieldId).val();
   if(!isBlankOrNull(emailId)){
			  $.ajax({ 
			  type : "POST",
			  url : basepath+"/user/forgotPassword", 
			  data : "emailId=" + emailId,
			  success : function(response) {  
			     var json = JSON.parse(response);
			     $('#'+errorId).html(json.message);
			     $('#'+errorId).css('color','green');

			  },
			     error : function(e) {  
			        $('#'+errorId).html('Error: ' + e);
			         $('#'+errorId).css('color','red');
			        }  
			});
	}else{
	      $('#'+errorId).html('Please Enter Valid Emailid');
	      $('#'+errorId).css('color','red');
	      return false;
	}
}


function redirectReviewOrder(errorId,message,isCAMLogin,quickRechargeCheckVar){
   var number =  $("#number").val();
   var operatorValue =  $("#operatorValue").val();
   var operatorText = $("#operatorText").val();
   var amount = $("#amount").val();
   var mobileService = $("#mobileService").val();
   var service = $("#service").val();
   var circleValue = $("#circleValue").val();
   var circleText = $("#circleText").val();
   if(service=="DTH"){
      mobileService = "DTH";
   }
   if(message==undefined){
        var url = basepath + "/recharge/review-order.htm";
         if(quickRechargeCheckVar != "" && quickRechargeCheckVar != "false"){
           url = basepath + "/recharge/quick-recharge-confirm-order.htm";
         }
         if(number!=undefined && operatorValue!=undefined && operatorText!=undefined && amount!=undefined && mobileService!=undefined && service!=undefined && number!="" && operatorValue!="" && operatorText!="" && amount!="" && mobileService!="" && service!=""){
              var jsonParam = [];
              jsonParam.push("number");
              jsonParam.push("operatorValue");
              jsonParam.push("operatorText");
              jsonParam.push("service");
              jsonParam.push("mobileRechargeService");
              jsonParam.push("amount"); 
              jsonParam.push("circleValue");
              jsonParam.push("circleName");
              var jsonParamValue = [];
              jsonParamValue.push(number);
              jsonParamValue.push(operatorValue);
              jsonParamValue.push(operatorText);
              jsonParamValue.push(service);
              jsonParamValue.push(mobileService);
              jsonParamValue.push(amount);
              jsonParamValue.push(circleValue);
              jsonParamValue.push(circleText);
              resetRechargeDetail();
              multipleDataPostRedirect(url, jsonParam, jsonParamValue,'post');
          }else{
              location.href = basepath + "/recharge/home";
          }
    }
    else if(message!=undefined && message=="User Already Registered" && isCAMLogin=="false"){
          $("#"+errorId).html("User Already Register.");
          location.href = basepath;
          return false;
    }
    else if(message!=undefined && message=="User Already Registered"){
          $("#"+errorId).html("User Already Register.");
          return false;
    }
   
}




function addCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function deleteCookie(name) {
    addCookie(name,"",-1);
}

function validatePasswordReg(passId,errorId){
      if( $('#autopass').is(':checked') ){
	       /*alert("vvv");*/
	         return true;
      } else {
	       var message = validatePassword(passId);
	       if(message == ""){
	        return true;
	       }
	       else{
	        passId.focus();
	        $('#'+errorId).html(message); 
	       }
      }
}

function validateOTP(otp,errorId){
   var OTPRegex = /^[0-9]{1,6}$/;
    if(otp.val()!=undefined && otp.val()!="" && otp.val().length==6 &&  OTPRegex.test(otp.val())){
      return true;
    }
    $('#'+errorId).html('Please Enter OTP.'); 
    otp.focus();
    return false;
}


function addDropDown(selectObj,selectValue,selectText){
	var option = document.createElement("option");
    option.value = selectValue;
    option.text = selectText;
    selectObj.options.add(option);
}

/*Recharge Box JS*/
function selectMobileServiceType(mobileServiceType){
     if ($('input[name='+mobileServiceType+']:checked').length > 0){
          var selectService = $("input[type='radio'][name="+mobileServiceType+"]:checked").val();
          if(selectService=="prepaid"){
              $("#mobilePrepaidOperator").removeClass('DispNone');
              $("#mobilePostpaidOperator").addClass('DispNone');
              $("#mobilePlanLink").show();
              $("#rechargeMobileNumber").val('');
              $("#mobileCircle").val('');
              $("#mobilePrepaid").val('');
              $("#mobileRechargeAmount").val('');
          }else if(selectService=="postpaid"){
              $("#mobilePrepaidOperator").addClass('DispNone');
              $("#mobilePostpaidOperator").removeClass('DispNone');
              $("#mobilePlanLink").hide();
              $("#rechargeMobileNumber").val('');
              $("#mobileCircle").val('');
              $("#mobilePostpaid").val('');
              $("#mobileRechargeAmount").val('');
          }
    }
}

var quickRechargeCheck = false;
function processedToMobileRechargeCoupon(checkUserLoginId,mobileRechargeService,rechargeMobileNumber,mobilePrepaidOperator,mobilePostpaidOperator,mobileRechargeAmount,mobileState){
     var operator;
     var operatorText;
     var selectService;
     var checkUserLogin = $("#"+checkUserLoginId).val();
     var number = $("#"+rechargeMobileNumber).val();
     var amount = $("#"+mobileRechargeAmount).val();
     var url = basepath + "/recharge/review-order.htm";
     if($('#quikRechargeMobile').is(":checked")){
     quickRechargeCheck = true;
        url = basepath + "/recharge/quick-recharge-confirm-order.htm";
     }
     if ($('input[name='+mobileRechargeService+']:checked').length > 0){
              var selectService = $("input[type='radio'][name="+mobileRechargeService+"]:checked").val();
              if(selectService=="prepaid"){
                   operator = $("#"+mobilePrepaidOperator).val();
                   operatorText = $("#"+mobilePrepaidOperator+ " option:selected").text();
              }else if(selectService=="postpaid"){
                   operator = $("#"+mobilePostpaidOperator).val();
                   operatorText = $("#"+mobilePostpaidOperator+ " option:selected").text();
              }
     }
     var state = $("#"+mobileState).val();
     var stateText = $("#"+mobileState+ " option:selected").text();

     if(number==undefined || number=="" || !validateMobileNumber($("#"+rechargeMobileNumber))){
         $(".mobileNumberErrorMsg").html("Please Enter Valid 10 digits Mobile Number");
         return false;
     }else{
         $(".mobileNumberErrorMsg").html("");
     }

     if(operator!=undefined && operator!=""){
         $(".mobileOperatorErrorMessage").html("");
     }else{
         $(".mobileOperatorErrorMessage").html("Please Select Mobile Operator.");
     }

     if(state!=undefined && state!=""){
         $(".mobileStateMessage").html("");
     }else{
         $(".mobileStateMessage").html("Please Select State.");
         return false;
     }
    
     if(selectService=="prepaid" && (amount==undefined || amount=="" || parseInt(amount)<10 || parseInt(amount)>10000)){
          $(".mobileRechargeAmountErrorMsg").html('Sorry, but your amount must be between Rs. 10 and Rs. 9999.');
          return false;
     }if(selectService=="postpaid" && (amount==undefined || amount=="" || parseInt(amount)<100 || parseInt(amount)>10000)){
          $(".mobileRechargeAmountErrorMsg").html('Sorry, but your amount must be between Rs. 100 and Rs. 9999.');
          return false;
     }else{
         $(".mobileRechargeAmountErrorMsg").html("");
     }

     if(checkUserLogin!=undefined && checkUserLogin=="true"){
                 $("#number").val(number);
                 $("#operatorValue").val(operator);
                 $("#operatorText").val(operatorText);
                 $("#amount").val(amount);
                 $("#mobileService").val(selectService);
                 $("#service").val('Mobile');
                 $("#circleValue").val(state);
                 $("#circleText").val(stateText); 
                 loaderStart();
                 redirectSignInScreen(quickRechargeCheck);
      }else if(checkUserLogin!=undefined && checkUserLogin=="false"){ 
                    resetRechargeDetail();
                    var jsonParam = [];
                    jsonParam.push("number");
                    jsonParam.push("operatorValue");
                    jsonParam.push("operatorText");
                    jsonParam.push("service");
                    jsonParam.push("mobileRechargeService");
                    jsonParam.push("amount");
                    jsonParam.push("circleValue");
                    jsonParam.push("circleName");
                    var jsonParamValue = [];
                    jsonParamValue.push(number);
                    jsonParamValue.push(operator);
                    jsonParamValue.push(operatorText);
                    jsonParamValue.push('Mobile');
                    jsonParamValue.push(selectService);
                    jsonParamValue.push(amount);
                    jsonParamValue.push(state);
                    jsonParamValue.push(stateText);
                    loaderStart();
                    multipleDataPostRedirect(url, jsonParam, jsonParamValue);
      }
}


function getOperatorDetails(mobileNumberId,serviceName,mobilePrepaidOperator,mobilePostpaidOperator,mobileErrorMessage,mobileServiceType,state){
     var sourceNumber = $("#"+mobileNumberId).val();
     var selectService = $("input[type='radio'][name="+mobileServiceType+"]:checked").val();
     if(sourceNumber!=undefined && sourceNumber!="" && validateMobileNumber($("#"+mobileNumberId))){
       loaderStart();
       $.ajax({
          type: "post",
          url : basepath+"/recharge/operatorDetails",  
          cache: false,   
         // async: false, //blocks window close 
          data : "mobileNumber="+sourceNumber,
          success: function(response){
             if(undefined!=response && response!=""){ 
                    var json = JSON.parse(JSON.stringify(response));
                    if(selectService=="prepaid"){
                      if(serviceName=='Mobile'){
                          $("#"+mobilePrepaidOperator).val(json.pay_code);
                      }
                          $("#"+state).val(json.pay_circle_code);
                    }else if(selectService=="postpaid"){
                          $("#"+state).val(json.pay_circle_code);
                    }
                    setTimeout(function(){
                          loaderEnd();
                    },300);
                    $("."+mobileErrorMessage).html("");
            }
          },
          error: function(){      
             $("."+mobileErrorMessage).html("Please Enter Valid 10 Digits Mobile Number.");
             return false;
          }
         });
   }else{ 
         if(sourceNumber>=10){
              $("."+mobileErrorMessage).html("Please Enter Valid 10 Digits Mobile Number.");
         }
         return false;
   }
}

function processedToDTHRechargeCoupon(checkUserLoginId,rechargeNumberId,OperatorId,rechargeAmountId){
     var operator;
     var operatorText;
     var selectService;
     var checkUserLogin = $("#"+checkUserLoginId).val();
     var number = $("#"+rechargeNumberId).val();
     var amount = $("#"+rechargeAmountId).val();
     var url = basepath + "/recharge/review-order.htm";
     if($('#quikRechargeDTH').is(":checked")){
        url = basepath + "/recharge/quick-recharge-confirm-order.htm";
        quickRechargeCheck = true;
     }
     operator = $("#"+OperatorId).val();
     operatorText = $("#"+OperatorId+ " option:selected").text();
     if(operator==undefined || operator=="" || operatorText == undefined || operatorText == ""){
          $(".dthOperatorErrorMsg").html('Please Select DTH Operator.');
          return false;
     }else{
          $(".dthOperatorErrorMsg").html('');
     }
     if(!validateDTHNumber(rechargeNumberId,"dthNumberMessageError",OperatorId)){
        return false;
     }else{
        $(".dthNumberMessageError").html('');
     }

     if(amount==undefined || amount=="" || parseInt(amount)<30 || parseInt(amount)>10000){
          $(".dthAmountMessage").html('Sorry, but your amount must be between Rs. 10 and Rs. 10000.');
          return false;
     }else{
         $(".dthAmountMessage").html("");
     }

     if(checkUserLogin!=undefined && checkUserLogin=="true"){
               $("#number").val(number);
               $("#operatorValue").val(operator);
               $("#operatorText").val(operatorText);
               $("#amount").val(amount);
               $("#mobileService").val(selectService);
               $("#service").val('DTH');
               loaderStart();    
               redirectSignInScreen(quickRechargeCheck);
     }else if(checkUserLogin!=undefined && checkUserLogin=="false"){ 
                resetRechargeDetail();
                var jsonParam = [];
                jsonParam.push("number");
                jsonParam.push("operatorValue");
                jsonParam.push("operatorText");
                jsonParam.push("service");
                jsonParam.push("mobileRechargeService");
                jsonParam.push("amount");
                var jsonParamValue = [];
                jsonParamValue.push(number);
                jsonParamValue.push(operator);
                jsonParamValue.push(operatorText);
                jsonParamValue.push('DTH');
                jsonParamValue.push(selectService);
                jsonParamValue.push(amount);
                loaderStart();
                multipleDataPostRedirect(url, jsonParam, jsonParamValue);
     }
}

function processedToDatacardRechargeCoupon(checkUserLoginId,datacardRechargeService,rechargeDatacardNumber,datacardPrepaidOperator,datacardPostpaidOperator,datacardRechargeAmount,datacardState){
     var operator;
     var operatorText;
     var selectService;
     var checkUserLogin = $("#"+checkUserLoginId).val();
     var number = $("#"+rechargeDatacardNumber).val();
     var amount = $("#"+datacardRechargeAmount).val();
     var url = basepath + "/recharge/review-order.htm";
     if($('#quikRechargeDataCard').is(":checked")){
        url = basepath + "/recharge/quick-recharge-confirm-order.htm";
        quickRechargeCheck = true;
     }
     if ($('input[name='+datacardRechargeService+']:checked').length > 0){
              var selectService = $("input[type='radio'][name="+datacardRechargeService+"]:checked").val();
              if(selectService=="prepaid"){
                   operator = $("#"+datacardPrepaidOperator).val();
                   operatorText = $("#"+datacardPrepaidOperator+ " option:selected").text();
              }else if(selectService=="postpaid"){
                   operator = $("#"+datacardPostpaidOperator).val();
                   operatorText = $("#"+datacardPostpaidOperator+ " option:selected").text();
              }
     }
     var state = $("#"+datacardState).val();
     var stateText = $("#"+datacardState+ " option:selected").text();

     if(number==undefined || number=="" || !validateMobileNumber($("#"+rechargeDatacardNumber))){
         $(".DataCardNumberMessage").html("Please Enter Valid 10 digits DataCard Number");
         return false;
     }else{
         $(".DataCardNumberMessage").html("");
     }

     if(operator!=undefined && operator!=""){
         $(".datacardPrepaidOperatorError").html("");
     }else{
         $(".datacardPrepaidOperatorError").html("Please Select Mobile Operator.");
     }

     if(state!=undefined && state!=""){
         $(".DataCardStateErrorMessage").html("");
     }else{
         $(".DataCardStateErrorMessage").html("Please Select State.");
         return false;
     }

    if(amount==undefined || amount=="" || parseInt(amount)<10 || parseInt(amount)>10000){
          $(".datacardRechargeAmountErrorMessage").html('Sorry, but your amount must be between Rs. 10 and Rs. 10000.');
          return false;
     }else{
         $(".datacardRechargeAmountErrorMessage").html("");
     }

     if(checkUserLogin!=undefined && checkUserLogin=="true"){
                 $("#number").val(number);
                 $("#operatorValue").val(operator);
                 $("#operatorText").val(operatorText);
                 $("#amount").val(amount);
                 $("#mobileService").val(selectService);
                 $("#service").val('DataCard');
                 $("#circleValue").val(state);
                 $("#circleText").val(stateText);
                 loaderStart();
                 redirectSignInScreen(quickRechargeCheck);
      }else if(checkUserLogin!=undefined && checkUserLogin=="false"){ 
                    resetRechargeDetail(); 
                    var jsonParam = [];
                    jsonParam.push("number");
                    jsonParam.push("operatorValue");
                    jsonParam.push("operatorText");
                    jsonParam.push("service");
                    jsonParam.push("mobileRechargeService");
                    jsonParam.push("amount");
                    jsonParam.push("circleValue");
                    jsonParam.push("circleName");
                    var jsonParamValue = [];
                    jsonParamValue.push(number);
                    jsonParamValue.push(operator);
                    jsonParamValue.push(operatorText);
                    jsonParamValue.push('DataCard');
                    jsonParamValue.push(selectService);
                    jsonParamValue.push(amount);
                    jsonParamValue.push(state);
                    jsonParamValue.push(stateText);
                    loaderStart();
                    multipleDataPostRedirect(url, jsonParam, jsonParamValue);
      }
}

function resetRechargeDetail(){
       $("#number").val('');
       $("#operatorValue").val('');
       $("#operatorText").val('');
       $("#amount").val(amount);
       $("#mobileService").val('');
       $("#service").val('');
       $("#circleValue").val('');
       $("#circleText").val('');
}


function validateDTHNumber(dthNumberId,validateMessageId,operatorId){
	var dthNumber = $("#"+dthNumberId).val();
	var operator = $("#"+operatorId+ " option:selected").text();
   if(operator=="Dish TV" || operator=="DISH TV"){
      if(checkFirstDTHNumber(dthNumber,"0") && dthNumber.length==11){
         $("."+validateMessageId).html("");
        return true;
      }else{
        $("#"+dthNumberId).focus();
        $("."+validateMessageId).html("Dish TV Card Number starts with 0 and have only 11 digits.");
      }
    }else if(operator=="Reliance Digital TV" || operator=="RELIANCE BIG TV"){
      if(checkFirstDTHNumber(dthNumber,"2") && dthNumber.length==12){
         $("."+validateMessageId).html("");
        return true;
      }else{
        $("#"+dthNumberId).focus();
        $("."+validateMessageId).html("This Card Number starts with 2 and have only 12 digits.");
      }
    }else if(operator=="Airtel Digital TV" || operator=="AIRTEL DTH"){
      if(checkFirstDTHNumber(dthNumber,"3") && dthNumber.length==10){
         $("."+validateMessageId).html("");
        return true;
      }else{
        $("#"+dthNumberId).focus();
        $("."+validateMessageId).html("Airtel Customer ID starts with 3 and have only 10 digits.");
      }
    }else if(operator=="Tata Sky" || operator=="TATA SKY"){
      if(checkFirstDTHNumber(dthNumber,"1") && dthNumber.length==10){
         $("."+validateMessageId).html("");
        return true;
      }else{
        $("#"+dthNumberId).focus();
        $("."+validateMessageId).html("This ID starts with 1 and have only 10 digits.");
      }
    }else if(operator=="Sun Direct" || operator=="SUN DIRECT DTH"){
      if((checkFirstDTHNumber(dthNumber,"4") || checkFirstDTHNumber(dthNumber,"1")) && dthNumber.length==11){
         $("."+validateMessageId).html("");
        return true;
      }else{
        $("#"+dthNumberId).focus();
        $("."+validateMessageId).html("This Number starts with 4 or 1 and have only 11 digits.");
      }
    }else if(operator=="Videocon D2H" || operator=="VIDEOCON D2H"){
      if(dthNumber.length<=14){
        $("."+validateMessageId).html("");
        return true;
      }else{
        $("#"+dthNumberId).focus();
        $("."+validateMessageId).html("Please Enter Valid Videocon D2H Smart Card Number.");
      }
    }
    return false;
}

function checkFirstDTHNumber(number,firstNumber){
	if(number!=undefined && number>1 && stringStartsWith(number,firstNumber)){
		return true;
	}
    return false;
}


function stringStartsWith(string, prefix) { 
	return string.slice(0, prefix.length) == prefix; 
}

function redirectSignInScreen(quickRechargeCheckVal){
    loaderStart();
    var number =  $("#number").val();
    var operatorValue =  $("#operatorValue").val();
    var operatorText = $("#operatorText").val();
    var amount = $("#amount").val();
    var mobileService = $("#mobileService").val();
    var service = $("#service").val();
    var circleValue = $("#circleValue").val();
    var circleText = $("#circleText").val();
    if(service=="DTH"){
      mobileService = "DTH";
    }
    var url = basepath + "/user/signIn";
    var jsonParam = [];
    jsonParam.push("number");
    jsonParam.push("operatorValue");
    jsonParam.push("operatorText");
    jsonParam.push("service");
    jsonParam.push("mobileRechargeService");
    jsonParam.push("amount"); 
    jsonParam.push("circleValue");
    jsonParam.push("circleName");
    jsonParam.push("quickRechargeCheckVal");
    var jsonParamValue = [];
    jsonParamValue.push(number);
    jsonParamValue.push(operatorValue);
    jsonParamValue.push(operatorText);
    jsonParamValue.push(service);
    jsonParamValue.push(mobileService);
    jsonParamValue.push(amount);
    jsonParamValue.push(circleValue);
    jsonParamValue.push(circleText);
    jsonParamValue.push(quickRechargeCheckVal);
    resetRechargeDetail();
    multipleDataPostRedirect(url, jsonParam, jsonParamValue,'post');
}


function confirmOrder(couponId,serviceType,isRewardsPointsRedeemId,isUseWalletId){
    var couponName = $("#"+couponId).val();
    var isUseWallet = "";
    var isRedeem = $("#"+isRewardsPointsRedeemId).val(); 
    if($("#"+isUseWalletId).prop("checked") == true){
        isUseWallet = "true"; 
    }  
    var isUseReferral = '';
    if ($("#isUseReferral").prop("checked") == true) {
      isUseReferral = "true";
    }
    var url = basepath + "/recharge/confirm-order.htm";
    var jsonParam = [];
    jsonParam.push("couponName");
    jsonParam.push("serviceType");
    jsonParam.push("isRedeem");
    jsonParam.push("isUseWallet");
    jsonParam.push("isUseReferral");
    var jsonParamValue = [];
    jsonParamValue.push(couponName);
    jsonParamValue.push(serviceType);
    jsonParamValue.push(isRedeem);
    jsonParamValue.push(isUseWallet);
    jsonParamValue.push(isUseReferral);
    if(isWalletBalance){
        multipleDataPostRedirect(url, jsonParam, jsonParamValue);
    }else{
       $(".userReferralErrorMessage").html("Your CAMllet Balance is Less then Recharge Balance. Please add money in CAMllet");
    }
}


function redeemRewardsPoints(emailId, couponSpanId, totalAmount, amountId,
    isReedomRewardsPointsId, walletBalanceId, rewardsPointsId,
    rewardsPointRedeemAmountId) {
  if ($("#isUseReferral" ).prop("checked") == true){
       $(".userReferralErrorMessage").html("Only one time one operation allowed");
  }else{
   if (emailId != undefined && emailId !== "") {
    $.ajax({
      type : "Post",
      async : false,
      url : basepath + "/rewardsPoints/redeemPoints",
      data : "emailId=" + emailId,
      success : function(response) {
        var json = JSON.parse(response);
        if (json != undefined && json != "") {
          var redeemPoints = parseInt(json.rewardsPoint_value);
          var amount = parseInt($("#" + amountId).html());
          var walletBalance = parseFloat($("#" + walletBalanceId)
              .html());
          $("#" + rewardsPointRedeemAmountId).val(redeemPoints);
          if (redeemPoints > amount) {
            $("#" + amountId).html('0');
            $("#" + walletBalanceId).html(
                walletBalance + (redeemPoints - amount));
          } else {
            $("#" + amountId).html(amount - redeemPoints);
          }
          $("#" + rewardsPointsId).html('0');
          $("#" + isReedomRewardsPointsId).val('true');
          $(".redeemRewardsErrorMessage").html(
              "Loyalty Reward Points redeemed successfully.");
          $("#" + couponSpanId).html("Confirm Order");
        } else {
          $(".redeemRewardsErrorMessage").html(
              "Error in Redeem Rewards Points.");
        }
      },
      error : function(e) {
        $(".redeemRewardsErrorMessage").html(
            "Error in Redeem Rewards Points.");
      }
    });
  }
  }
}


function redeemCoupon(couponId,rechargeAmount,userEmailId,serviceType,couponErrorMessage,couponSpanId,amountId,walletBalanceId){
     var couponName = $("#"+couponId).val();
     if(couponName!=undefined && couponName!==""){
         $.ajax({  
                type : "Post",
                async : false,   
                url : basepath+"/coupon/redeem",   
                data : "couponName=" + couponName + "&amount="  
                  + rechargeAmount+"&emailId="+userEmailId+"&couponServiceType="+serviceType,  
                success : function(response) {  
                    var json = JSON.parse(response); 
                    if(json!=undefined && json!="" && json.isCouponApply){
                          $("."+couponErrorMessage).html("Coupon "+couponName.toUpperCase()+" applied successfully. "); 
                          $("#"+couponSpanId).html("Confirm Order");  
                    }else{
                         $("."+couponErrorMessage).html("Invalid coupon. Please try again.");
                         $("."+couponErrorMessage).addClass("couponError");
                         $("#"+couponSpanId).html("Confirm Order");     
                    }
                },  
                error : function(e) {  
                   $("."+couponErrorMessage).html("Error in Redeem Coupon");
                   $("."+couponErrorMessage).addClass("couponError");
                }  
            });
    }else{
        $("."+couponErrorMessage).html("Please Enter Coupon Name.");
        $("."+couponErrorMessage).addClass("couponError");
    }
}


function manageWalletBalance(walletBalanceId, isUseWalletId, paidAmountId,
    rewardsPointRedeemAmountId, totalAmount, totalWalletBalance,isUserReferralId,userReferralBalance) {
  var amount = parseInt($("#" + paidAmountId).html());
  var walletBalance = parseInt(totalWalletBalance);
  var userReferralBalance = parseInt(userReferralBalance);
  if( $("#" + isUserReferralId).prop("checked") == true){
    $(".userReferralErrorMessage").html("You can not avail any other feature like referral balance or reward points if you redeem wallet balance for recharge.");
    $("#" + isUseWalletId).attr("checked",false);
  }else if ($("#" + isUseWalletId).prop("checked") == true) {
    var paidAmount = 0;
    var redeemPointsAmount = parseInt($("#" + rewardsPointRedeemAmountId)
        .val());
    if (walletBalance >= amount) {
        paidAmount = walletBalance - amount;
        $("#" + paidAmountId).html('0');
        $("#" + walletBalanceId).html(paidAmount);
        $(".userReferralErrorMessage").html("");
    } else {
        paidAmount = amount - walletBalance;
        $("#" + paidAmountId).html(paidAmount);
        $("#" + walletBalanceId).html('0');
        isWalletBalance = false;
        $(".userReferralErrorMessage").html("Your CAMllet Balance is Less then Recharge Balance. Please add money in CAMllet");
    
    }
    
  } else {
    var calTotalAmount = parseInt(totalAmount);
    var redeemPointsAmount = parseInt($("#" + rewardsPointRedeemAmountId)
        .val());
    var paidAmount = 0;
    if (calTotalAmount >= redeemPointsAmount) {
      paidAmount = calTotalAmount - redeemPointsAmount;
    } else {
      paidAmount = redeemPointsAmount - calTotalAmount;
    }
    $("#" + paidAmountId).html(paidAmount);
    $("#" + walletBalanceId).html(totalWalletBalance);
    $(".userReferralErrorMessage").html("");
  }
}

function manageUserReferralBalance(walletBalanceId, isUseWalletId, paidAmountId,
    rewardsPointRedeemAmountId, totalAmount, totalWalletBalance,isUserReferralId,userReferralBalance) {
  var amount = parseInt($("#" + paidAmountId).html());
  var walletBalance = parseInt(totalWalletBalance);
  var userReferralBalance = parseInt(userReferralBalance);
  if ($("#" + isUseWalletId).prop("checked") == true) {
    $(".userReferralErrorMessage").html("You can not avail any other feature like wallet balance or reward points if you redeem referral amount for recharge.");
    $("#" + isUserReferralId).attr("checked",false);
  }else if ($("#" + isUserReferralId).prop("checked") == true) {
    var paidAmount = 0;
    var percentageAmount = parseFloat((amount * 3)/100); 
    percentageAmount = Math.round(percentageAmount);
    paidAmount = amount - percentageAmount;
    if(percentageAmount <= userReferralBalance){
      userReferralBalance = userReferralBalance - percentageAmount;
      $("#" + paidAmountId).html(paidAmount);
      $("#userReferralBalanceId").html(userReferralBalance);
    }else{
      $("#" + paidAmountId).html(paidAmount);
      $("#userReferralBalanceId").html('0');    
        $(".userReferralErrorMessage").html('');
    }
  } else {
    var calTotalAmount = parseInt(totalAmount);
    var redeemPointsAmount = parseInt($("#" + rewardsPointRedeemAmountId)
        .val());
    var paidAmount = 0;
    if (calTotalAmount >= redeemPointsAmount) {
      paidAmount = calTotalAmount - redeemPointsAmount;
    } else {
      paidAmount = redeemPointsAmount - calTotalAmount;
    }
    $("#" + paidAmountId).html(paidAmount);
    $("#" + walletBalanceId).html(totalWalletBalance);
    $("#userReferralBalanceId").html(userReferralBalance);  
    $(".userReferralErrorMessage").html("");
  }
}


  $(document).ready(function(){ 
           $('#autopass').change(function() {
                  if($(this).is(":checked")) {
                        var passwordInput = document.getElementById('charger_reg_password');
                        $('#charger_reg_password').val("");
                        $('#charger_reg_password').addClass('DispNone');
                        $('#showpassdiv').addClass('DispNone')
                  }
                  else{
                      $('#charger_reg_password').removeClass('DispNone');
                      $('#showpassdiv').removeClass('DispNone') 
                  }
                  /*$('#textbox1').val($(this).is(':checked'));   */     
              });
}); 

 function addMoney(){
  var checkUserLogin = $("#checkUserLogin").val();
    if(checkUserLogin!=undefined && checkUserLogin=="true"){
      loaderStart();
                 redirectSignInScreen();
    }
    else{
      loaderStart();
       loadAmount('loadAmount');
    }
 }

 function loadAmount(loadAmountId){
    var amount = $("#"+loadAmountId).val();
    if(amount!=undefined && amount!=""){
          var url = basepath + "/user/addMoney";
        var jsonParam = [];
        jsonParam.push("amount");
        var jsonParamValue = [];
        jsonParamValue.push(amount);
        multipleDataPostRedirect(url, jsonParam, jsonParamValue);
    }else{
      setTimeout(function(){
                                loaderEnd();
                            },300);
       alert("Please Enter Valid Amount.");
    }
} 

function getMobilePlan(mobilePlanId,mobileNumberId,planData){
    $("#mobileRechargePlan").html("");
     var mobileNumber = $("#"+mobileNumberId).val();
     var operatorText;
     var operatorVal;
     var zoneCode;
     var zoneText;
     if ($('input[name=mobileRechargeService]:checked').length > 0){
            var selectService = $("input[type='radio'][name=mobileRechargeService]:checked").val();
            if(selectService=="prepaid"){
                 operatorText = $( "#mobilePrepaid option:selected" ).text();
                 operatorVal = $( "#mobilePrepaid option:selected" ).val();
                 zoneCode = $( "#mobileCircle option:selected" ).val();
                 zoneText = $( "#mobileCircle option:selected" ).text();
            }
     }
     if(mobileNumber!=undefined && mobileNumber!="" && validateMobileNumber($("#"+mobileNumberId)) && operatorVal!="" && operatorVal != undefined && zoneCode!=undefined && zoneCode!=""){
  
              $("#operatorPlanName").html(operatorText + " Recharge Plans");
              loaderStart();
             $.ajax({  
                    type : "Post",
                   // async : false,   
                    url : basepath+"/recharge/getPlans",  
                    cache: false,   
                    data : "mobileNumber=" + mobileNumber+ "&operatorCode=" +operatorVal+ "&zoneCode=" +zoneCode,
                    success : function(response) {  
                        if(response!=undefined && response!=""){
                            openMobilePlan();
                            setTimeout(function(){
                                loaderEnd();
                            },300);
                            $("#"+mobilePlanId).append(response);
                            showMobilePlan('FTTMobileRechargePlanDiv','talktimetab');
                        }
                    },  
                    error : function(e) {  
                       alert("Error in Get Mobile Plan.");
                       closeMobilePlan();
                    }  
                });
          $(".mobileOperatorErrorMessage").html("");
         $(".mobileStateMessage").html("");
      }else{
         closeMobilePlan();
        // $(".mobileNumberErrorMsg").html("Please Enter Valid 10 digits Mobile Number");
         $(".mobileOperatorErrorMessage").html("Please Select Mobile Operator.");
         $(".mobileStateMessage").html("Please Select State.");
         return false;
      }
}

function showMobilePlan(planDiv,categoryDiv){
     $("#mobileRechargePlan>div").hide();
     $("#"+planDiv).show();
     $("#"+planDiv+">div").show();
     $('#plansCategory a').removeClass('ActivePlanTab');
     $("#"+categoryDiv).addClass('ActivePlanTab');
}

function openMobilePlan(){
  //$('#plans').addClass('visible');
  $('#plans').addClass('csspop-overlay-show');
  $('#mobileplanOverlay').removeClass('DispNone');
}

function closeMobilePlan(){
  $('#plans').removeClass('csspop-overlay-show');
  //$('#mobileplanOverlay').addClass('DispNone');
}
function selectMobilePlan(amount,mobileAmountId,mobilePlanId){
   $("#"+mobileAmountId).val(amount);
   closeMobilePlan();
}

function redirectNewUrl(url){
	var jsonParam = [];
	var jsonParamValue = [];
	jsonParam.push("agent");
	jsonParamValue.push('android');
	multipleDataPostRedirect(url, jsonParam, jsonParamValue,'post');
}

function createWapGAClickEventTrackingNonInteraction(category,event,label){
    try{
        ga('send', 'event', category, event, label, 0,true);
                //ga('send', 'event', category, event, label, 0,{nonInteraction: true});
    }catch(e){}
    
}

function addFundsRequest(){
    var amount = $("#addRequestamount").val();
    var paymentMethod = $("#campmethod").val();
    var bankName = $("#campaybankaccount").val();
    var banfRefId = $("#camBankrefid").val();
    var remark = $("#camremark").val();
    if(amount != undefined && paymentMethod != undefined && bankName!=undefined && banfRefId != undefined
      && amount != "" && amount >= 100 && paymentMethod != "" && bankName != "" && banfRefId != ""){
      var addRequestamount = parseInt(amount);
    $.ajax({
        type : "POST",
        url : basepath + "/funds/addRequest",
        data : "amount=" + addRequestamount+"&paymentMethod="+paymentMethod+"&bankName="+bankName+"&bankReferenceId="+banfRefId+"&remarks="+remark,
        success : function(response) {
          if(response == "success"){
                         alert("Funds Request Added Successfully.");
                         location.href = basepath + "/user/profile";
          }else{
                         alert("Please Enter Valid Bank Reference Id.");
                         //location.href = basepath + "/user/profile";
          }
        },
        error : function(e) {
          alert("Error in Add Funds Request.");
          //location.href = basepath + "/user/profile";
        }
      });
    }else{
      alert("Please Enter Valid Details.");
    }
}

function updatePassword(oldPasswordId, newPasswordId, confirmNewPassId){
  var oldPasswordFromInput = $("#"+oldPasswordId).val();
  var newPassword = $("#"+newPasswordId).val();
  var confirmNewPass = $("#"+confirmNewPassId).val();
  if(isBlankOrNull(oldPasswordFromInput)){
    alert("Please enter your current password.");
    return;
  }
  if(isBlankOrNull(newPassword)){
    alert("Please enter your new desired password.");
    return; 
  }
  if(isBlankOrNull(confirmNewPass)){
    alert("Please confirm your new password.");
    return;
  }
    var isValidPassword = validatePassword($("#"+newPasswordId));
  if(isValidPassword!=""){
        alert(isValidPassword);
        return;
  }

  if(matchNewPasswords(newPassword, confirmNewPass)){
      $.ajax({  
            type : "Post",
            url : basepath+"/user/changePassword",   
            data : "newPass=" + newPassword+"&oldPass="+oldPasswordFromInput,  
            success : function(response) {  
            //  var json = JSON.parse(response);  
              alert(response);
            },  
            error : function(e) {  
             alert('Error in changing password. Please try again');   
            }  
          });
    } else {
      alert("Passwords do not match. Please check again.")
    }
  
}

function matchNewPasswords(newPass, confirmNewPass){
  if(newPass == confirmNewPass)
    return true;
  else return false;
}