$('.form').find('input, textarea').on('keyup blur focus', function (e) {
  
  var $this = $(this),
      label = $this.prev('label');

    if (e.type === 'keyup') {
      if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
      if( $this.val() === '' ) {
        label.removeClass('active highlight'); 
      } else {
        label.removeClass('highlight');   
      }   
    } else if (e.type === 'focus') {
      
      if( $this.val() === '' ) {
        label.removeClass('highlight'); 
      } 
      else if( $this.val() !== '' ) {
        label.addClass('highlight');
      }
    }

});

$('.tab a').on('click', function (e) {
  
  e.preventDefault();
  
  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');
  
  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();
  
  $(target).fadeIn(600);
  
});

function validateName(uname,errorId){   
  var letters = /^[A-z ]+$/;  
  if(uname.val()!=undefined && uname.val()!="" && trim(uname.val()).match(letters)){  
  return true;  
  } else{  
  $('#'+errorId).html('Username must have alphabet characters only');  
  uname.focus();  
  return false;  
  }  
}


function hideShowDiv(hideDivId,showDivId){
  $("#"+hideDivId).hide();
  $("#"+showDivId).show();
}

function validatePassword(fld) {
    var error = "";
    var illegalChars = /[\W_]/; // allow only letters and numbers 
    if (fld.val() == "") {
        error = "Please Enter a password.\n";
    } else if ((fld.val().length < 7) || (fld.val().length > 20)) {
        error = "Please Enter Password at least 7 characters. \n";
    } /*else if (illegalChars.test(fld.val())) {
        error = "Please Enter Password only letter and number.\n";
    } else if (!((fld.val().search(/(a-z)+/)) && (fld.val().search(/(0-9)+/)))) {
        error = "The password must contain at least one numeral.\n";
    }*/
   return error;
}  

function trim(s){
  if(s!=undefined && s!=""){
      return s.replace(/^\s+|\s+$/, '');
  }
  return s;
} 

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    } else {
        return true;
    }      
}

function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if (charCode==32 || charCode==8 || charCode==46 || (charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
            return true;
        else
            return false;
    }
    catch (err) {
        alert(err.Description);
    }
}

function spaceNotAllowed(e){
    if (e.which==32){
        return false;
    }
}

function allowAlphaNumeric(e){
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }

    return false;
}

function validateEmail(email,errorId) { //Validates the email address
    var emailRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(email.val()!=undefined && email.val()!="" && emailRegex.test(email.val())){
      return true;
    }
    $('#'+errorId).html('Please Enter valid Email Id.'); 
    email.focus();
    return false;
}


function validatePhone(phone,errorId) { //Validates the phone number
    var phoneRegex = /^[0]?[789]\d{9}$/; // Change this regex based on requirement
    if(phone.val()!=undefined && phone.val()!="" && phone.val().length==10 &&  phoneRegex.test(phone.val())){
      return true;
    }
    $('#'+errorId).html('Please Enter valid Mobile Number.'); 
    phone.focus();
    return false;
}

function isBlankOrNull(stringToCheck){
  if(stringToCheck==''){
     return true;
  } else return false;
}


function validatePasswordReg(passId,errorId){
      if( $('#autopass').is(':checked') ){
         /*alert("vvv");*/
           return true;
      } else {
         var message = validatePassword(passId);
         if(message == ""){
          return true;
         }
         else{
          passId.focus();
          $('#'+errorId).html(message); 
         }
      }
}

function findTarget(url){
   location.href=url;
}    

function loaderStart(){
  $("#loader-wrapper").removeClass("DispNone");
  $("#mask").removeClass("DispNone");

}

function loaderEnd(){
  $("#loader-wrapper").addClass("DispNone");
  $("#mask").addClass("DispNone");
}

function closePopUp (ppopupId) {
  $('#'+ppopupId).removeClass('overlayTarget');
}


function validateOTP(otp,errorId){
   var OTPRegex = /^[0-9]{1,6}$/;
    if(otp.val()!=undefined && otp.val()!="" && otp.val().length==6 &&  OTPRegex.test(otp.val())){
      return true;
    }
    $('#'+errorId).html('Please Enter OTP.'); 
    otp.focus();
    return false;
}


function userRegister(usernameId,emailId,mobileNo,password,source,isCAMLogin,errorId){
	var nameValue = $('#'+usernameId).val();  
	var emailIdValue = $('#'+emailId).val();  
	var mobileNoValue = $('#'+mobileNo).val(); 
  var passwordValue = $('#'+password).val();

    if(validateName($('#'+usernameId),errorId) && validateEmail($('#'+emailId),errorId) && validatePhone($('#'+mobileNo),errorId) && validatePasswordReg($('#'+password),errorId) ){
               loaderStart();    		
               $.ajax({  
    		    type : "POST",   
    		    async : false,
    		    url : basepath+"/userwallet/register",   
    		    data : "name=" + nameValue.trim() + "&emailId=" + emailIdValue + "&mobileNo="  
    		      + mobileNoValue+ "&password="+passwordValue+ "&source=" + source + "&isCAMLogin="+isCAMLogin,  
    		    success : function(response) {  
    		    	console.log(response);
    		    	var json = JSON.parse(response);
              console.log(json);
               setTimeout(function(){
                              loaderEnd();
                        },300);  
    		    	if(json.message=="OTP_Screen"){
    		    	    loaderEnd();
                  if(isCAMLogin=='false'){
                      showOTPScreenforThirdParty(); 
                  }else{
                     showOTPScreen('userOtp');
                  }
    		    	}
              else if(json.message=="User Already Registered"){
                  loaderEnd();
                  $('#'+errorId).html(json.message);
              }else{
                 loaderEnd();
                 $('#'+errorId).html(json.message);
              }
    		    },  
    		    error : function(e) { 
             loaderEnd(); 
             $('#'+errorId).html('Error: ' + e); 
             alert('Error: ' + e);  
    		    }  
    	    });
       }
}

function showOTPScreen(OtpScreen){
  $('#'+OtpScreen).addClass('overlayTarget');
  return false;
}


function submitUserOTP(userOTP,otpScreen,errorId){
  var userOTPValue = $('#'+userOTP).val();
  if( validateOTP($('#'+userOTP),errorId) ){
          loaderStart();        
          $.ajax({  
            type : "Post",   
            async : false,
            url : basepath+"/userwallet/otpresponse",   
            data : "otp=" + userOTPValue.trim(),  
            success : function(response) {  
              //alert(json.message);
              var json = JSON.parse(response);
               setTimeout(function(){
                              loaderEnd();
                        },300);  
              if(json.message=="User Registered Successfully"){
                  redirectReviewScreen(otpScreen);
              }           
                else{
                 $('#'+errorId).html(json.message);
              }
            },  
            error : function(e) {  
             $('#'+errorId).html('Error: ' + e); 
             alert('Error: ' + e);  
            }  
          });
  }
}

function userLogin(emailId,password,errorId){
  var passwordValue = $('#'+password).val();  
  var emailIdValue = $('#'+emailId).val();  
  if(validateEmail($('#'+emailId),errorId) && password!=undefined && password!=""){
     loaderStart();
    $.ajax({  
        type : "Post",   
        url : basepath+"/userwallet/login",   
        data : "emailId=" + emailIdValue + "&password="  
          + passwordValue,  
        success : function(response) {  
          var json = JSON.parse(response); 
           setTimeout(function(){
                          loaderEnd();
                    },300);
          if(json.message=="success"){
                     showOTPScreen('userOtp');
          }
          else if(json.message=="nonverified"){
                     showOTPScreen('userOtp');
          }
          else if(json.message=="user not exists"){
                $('#'+errorId).html('User not exists.');
                return false;
          } 

          else{
            $('#'+errorId).html('Please Enter Valid Email Id and password.');
          }
        },  
        error : function(e) {  
        $('#'+errorId).html('Error: ' + e);
         alert('Error: ' + e);   
        }  
      });
   }
}


function redirectReviewScreen (otpScreen) {
  $('#'+otpScreen).removeClass('overlayTarget');
  var url = basepath + "/userwallet/camwalletReviewOrder";
  location.href = url;

}

function showForgotPassword (forgotPasswordPopUp) {
  $('#'+forgotPasswordPopUp).addClass('overlayTarget');
}

function forgotPassword(emailfieldId,errorId){
   var emailId = $("#"+emailfieldId).val();
   if(!isBlankOrNull(emailId) && validateEmail($('#'+emailfieldId),errorId)){
        $.ajax({ 
        type : "POST",
        url : basepath+"/userwallet/forgotPassword", 
        data : "emailId=" + emailId,
        success : function(response) {  
           var json = JSON.parse(response);
           $('#'+errorId).html(json.message);
           $('#'+errorId).css('color','green');

        },
           error : function(e) {  
              $('#'+errorId).html('Error: ' + e);
               $('#'+errorId).css('color','red');
              }  
      });
  }else{
        $('#'+errorId).html('Please Enter Valid Emailid');
        $('#'+errorId).css('color','red');
        return false;
  }
}


function addUserInfoByFbGoogle(){  
     userRegisterByThirdParty('chargers_reg_name','charger_reg_emailId','charger_reg_mobileNo','WAP','false','errorIdThirdParty');
} 

function userRegisterByThirdParty(usernameId,emailId,mobileNo,source,isCAMLogin,errorId){
  var nameValue = $('#'+usernameId).val();  
  var emailIdValue = $('#'+emailId).val();  
  var mobileNoValue = $('#'+mobileNo).val(); 
  var passwordValue = "";
  if(validateName($('#'+usernameId),errorId) && validateEmail($('#'+emailId),errorId) && validatePhone($('#'+mobileNo),errorId) ){
              loaderStart();        
               $.ajax({  
            type : "Post",   
            async : false,
            url : basepath+"/userwallet/registeredOther.htm",   
            data : "name=" + nameValue.trim() + "&emailId=" + emailIdValue + "&mobileNo="  
              + mobileNoValue+ "&password="+passwordValue+ "&source=" + source + "&isCAMLogin="+isCAMLogin,  
            success : function(response) {  
              //alert(json.message);
              var json = JSON.parse(response);
               setTimeout(function(){
                              loaderEnd();
                        },300); 
                        console.log(json.message); 
              if(json.message=="Show Mobile Number Screen."){
                  /*redirectReviewOrder(); */
                  showMobileScreen();
              }
              else if(json.message=="User Already Registered"){
                  showOTPScreen('userOtp');
              }
                else{
                 $('#'+errorId).html(json.message);
              }
            },  
            error : function(e) {  
             $('#'+errorId).html('Error: ' + e); 
             alert('Error: ' + e);  
            }  
          });
       }
}

function showMobileScreen(){
  $('#MobileVerification').addClass('overlayTarget');
} 

function submitUserMobile(mobileNo,errorId){
  var mobileNumber = $('#'+mobileNo).val();
   $('#charger_reg_mobileNo').val(mobileNumber);
   userRegister('chargers_reg_name','charger_reg_emailId','charger_reg_mobileNo','charger_reg_password','WAP','false',errorId); 
}