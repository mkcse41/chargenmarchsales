	/*Google Login*/
var googleUser = {};
  var startApp = function() {
    gapi.load('auth2', function(){
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      auth2 = gapi.auth2.init({
      client_id: '167369363912-imvgt0ts0jce5rqceqpsqn6f7m50oe3d.apps.googleusercontent.com',//for live version 
        //client_id: '1051842101187-hnrfh49g8hgsuj3rdfugkkaf093o0c0q.apps.googleusercontent.com', //for beta version
        cookiepolicy: 'single_host_origin',
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      attachSignin(document.getElementById('customBtn'));
    });
  };

  function attachSignin(element) {
    auth2.attachClickHandler(element, {},
        function(googleUser) {
                $('#chargers_reg_name').val(googleUser.getBasicProfile().getName());
                $('#charger_reg_emailId').val(googleUser.getBasicProfile().getEmail());  
                $('#charger_reg_mobileNo').val('9999999999');
           addUserInfoByFbGoogle();
        }, function(error) {
          alert(JSON.stringify(error, undefined, 2));
        });
  }

  startApp();