<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
	<c:forEach var="transaction" items="${transactionList}">
							<table width="100%" style="margin: 0;">
								<tr>
								   <c:if test="${transaction.isSuccesfulRecharge==1}">
									 	<td ><div><img src="${applicationScope['baseUrl']}/resources/images/tick.png" class="successTick" /></div></td> 
									</c:if>
									<c:if test="${transaction.isSuccesfulRecharge!=1}">
									 	<td ><div><img src="${applicationScope['baseUrl']}/resources/images/test-fail-icon.png" /></div></td> 
									</c:if>
									<td><joda:format value="${transaction.rechargeDate}" pattern="MMM dd yyyy"/></td>
									<c:if test="${transaction.serviceType!='Wallet'}">
									     <td>${transaction.number} (${transaction.operatorName})</td>
									</c:if>
									<c:if test="${transaction.serviceType=='Wallet'}">
									     <td>${transaction.orderId}(OrderId)</td>
									</c:if>
									<td>Rs. ${transaction.amount}<br>${transaction.serviceType}</td>
								</tr>
							</table>
						 <br>
						</c:forEach>