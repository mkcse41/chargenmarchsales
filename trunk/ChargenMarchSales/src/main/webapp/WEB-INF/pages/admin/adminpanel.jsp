<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<compress:html removeComments="true" removeIntertagSpaces="true">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="msvalidate.01" content="4C0DE38ADCA26A9DED45D4FF7E9AAAA3" />
<title>ChargenMarch - Recharge Mobile, DTH, DataCard and Landline, Fast Recharge, Rewards</title>
<meta name="keywords" content="Online recharge, Free Recharge, Prepaid Recharge, Online mobile recharge, Datacard Recharge"/>
<meta name="Description" content="ChargenMarch.com-Simple way to recharge Mobile, DataCard, DTH, pay landline bills and keep on Marching forward. Get rewards with every payment on ChargenMarch"/>
<meta name="fragment" content="!" />
<meta name="google-site-verification" content="-o7OyH8W7bswsEjg9P2kHGskRYeaq56ZmWh7PBudP_A" />
<meta property="og:title" content="ChargenMarch - Recharge Mobile, DTH and DataCard, Fast & Free Recharge, Rewards | ChargenMarch.com" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://chargenmarch.com" />
<meta property="og:description" content="ChargenMarch.com - Simple way to Recharge all Prepaid Mobile, DataCard, DTH and keep on Marching forward. Rewards with Recharge. " />
<meta name="h12-site-verification" content="0ac51fbcf3609395f435cab1ca495b7d"/>
<%@include file="../jsp-includes/css-javascript-include-home.jsp" %>
<script src="${applicationScope['baseUrl']}/resources/javascript/document_home.js" onload="loadAddThisScript();" defer></script> 
<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="images/png" rel="shortcut icon" />
<link rel="manifest" href="${applicationScope['baseUrl']}/resources/manifest.json">
<style type="text/css">
   .block{width: 80%;
    text-align: center;margin-left: 10%;}
    input{width: 50% !important;}
    .dataInnerDiv{width: 70%;text-align: left;display: inline-block;}
    .innerShowDataDiv{width: 50%;display: inline-block;}
    .dataOuterDiv{ margin-left: 10%;}
    .divTitle{    width: 50%;
    text-align: center;
    font-size: 30px;
    border: 1px solid #ccc;display: inline-block;}
    .selected{    background: rgb(195, 199, 196);color: #717070;}
</style>
</head>
<body>
<!-- Header Start -->
<%@include file="../jsp-includes/cam_header.jsp" %>
<!-- Header End -->

<section class="wrapper profile" style="background:rgba(234, 234, 234, 0.17);">
  <div class="divTitle selected">User Information</div>
  <div class="divTitle">Recharge Data</div>
</section>

<section class="wrapper profile" style="background:rgba(234, 234, 234, 0.17);    padding-top: 0;">

	<div class="block">
	 <input type="text" id="userId" name="UserId" placeholder="User Id"/>
	  <div class="button">
            <a href="" onclick="clearAllFields();">clear</a>
            <button onclick="return getDataByUserId('userId');">Get Data</button>
              <button onclick="return getUserTrasactionHistoryByUserId('userId');">Get Transaction History</button>
           </div>
	</div>
      <div class="block">
	  <input type="text" id="email" name="EmailId" placeholder="Email Id" />
	   <div class="button">
        <a href="" onclick="clearAllFields();">clear</a>
            <button onclick="return getDataByEmail('email');">Get Data</button>
           </div>
       </div>
     <div class="block">
	   <input type="text" id="mobile" name="MobileNo" placeholder="Mobile Number"/>
	   	  <div class="button">
            <a href="" onclick="clearAllFields();">clear</a>
            <button onclick="return getDataByMobile('mobile');">Get Data</button>
           </div>
       </div>


<!-- User Information  -->
<div class="dataOuterDiv">
	<div class="dataInnerDiv" id="dataInnerDiv">

     </div>
<div id="errorId"></div>
<div id="successId"></div>
     <div id="updateButton" style="display:none;">
         <div class="button">
            <button onclick="return updateUserInformation('id','name','emailId','mobileNo','rechargeCount','registrationDate','source','password','walletBalance','isverified','referralCode','referralBalance','ByEmail','errorId');">Update By Email</button>
           </div>
            <div class="button">
            <button onclick="return updateUserInformation('id','name','emailId','mobileNo','rechargeCount','registrationDate','source','password','walletBalance','isverified','referralCode','referralBalance','ById','errorId');">Update By Id</button>
           </div>
     </div>
</div>

<!-- User Recharge History -->
	<!-- Transaction Tab Content Code Start -->
				<div class="trans-tabcontent contentpart" style="width: 80%;margin-left: 10%;">
					<div class="trans-heading Hand ">TRANSACTION HISTORY</div>
					<hr>
					<div style="margin-top:48px"></div>
					<div id="transactionHistory" style="overflow:auto;height:400px">
					</div>
				</div>
				<!-- Transaction Tab Content Code End -->


  <!-- Transaction Tab Content Code Start -->
  <div class="trans-tabcontent contentpart" style="width: 80%;margin-left: 10%;">
        <div class="trans-heading Hand ">Funds Request HISTORY</div>
        <hr>
        <div style="margin-top:48px"></div>
        <div id="fundsRequestHistory" style="overflow:auto;height:400px">
              <c:forEach var="fundsRequest" items="${addFundsRequestList}">
              <table width="100%">
                <tr>
                    <td>${fundsRequest.user.id}</td>
                     <td>${fundsRequest.user.name}</td>
                    <td>${fundsRequest.amount}</td>
                    <td>${fundsRequest.paymentMethod}</td>
                    <td>${fundsRequest.bankReferenceId}</td>
                    <td>${fundsRequest.status}</td>
                    <td><joda:format value="${fundsRequest.addDate}" pattern="MMM dd yyyy"/></td>
                    <td><input type="button" value="Approved" onclick="processedFundRequest('${fundsRequest.user.id}','${fundsRequest.bankReferenceId}','${fundsRequest.amount}')" /></td>
                </tr>
              </table>
             <br>
            </c:forEach>

        </div>
  </div>
        <!-- Transaction Tab Content Code End -->
         <div class="trans-tabcontent contentpart" style="width: 80%;margin-left: 10%;">
        <div class="trans-heading Hand ">Update Margin</div>
        <hr>
            <a href="${applicationScope['baseUrl']}/admin/margin">Update Margin</a>
        </div>
</section>
<!-- Footer Section Start -->
<%@include file="../jsp-includes/cam_footer.jsp" %>
<!-- Footer Section End -->
<script type="text/javascript" src="${applicationScope['baseUrl']}/resources/javascript/admin/admin.js" defer></script> 
</body>
</html>
</compress:html>