<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Privacy Policy | Recharge Mobile, DTH and DataCard | Chargenmarch.com</title>
<c:set var="now" value="<%=new java.util.Date()%>" />
<%@include file="jsp-includes/css-javascript-include-other.jsp" %>
<link type="text/css" href="${applicationScope['baseUrl']}/resources/css/cam_payment_failure.css" rel="stylesheet" />
<link type="text/css" href="${applicationScope['baseUrl']}/resources/css/cam_payment_success.css" rel="stylesheet" />
<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="${applicationScope['baseUrl']}/resources/images/png" rel="shortcut icon" />
</head>
<body >
<!-- Header Start -->
<%@include file="jsp-includes/cam_header.jsp" %>
<!-- Header End -->
<!--  Login and register form  -->
<%@include file="jsp-includes/chargers_login.jsp" %>

  <section>
 
<div class="inner mid">
        <div class="privacyPolicyDiv" id="privacypolicy">
         <p class="welcome" style="margin:0px;padding:15px 15px 0px 15px;text-align:center">Privacy Policy</p>
         <div>
					&nbsp;</div>
				
         <div style="margin-left: 14px;text-align: justify; margin-right: 14px;">
		         <div >
					Chargenmarch recognises the importance of maintaining your privacy. We value your privacy and appreciate your trust in us. This Policy describes how we treat user information we collect on chargenmarch.com and other offline sources. This Privacy Policy applies to current and former visitors to our website and to our online customers. By visiting and/or using our website, you agree to this Privacy Policy.&nbsp;</div>
				<div class="fontBold">
					Chargenmarch is a property of LifeSoft Technology Plot no 44, R.G. Ramawat, Shekhawati Nagar Extension, Near Govindam Tower, Govind Pura Jaipur - 302012 .&nbsp;</div>
				<div>
					&nbsp;</div>
				<div>
					&nbsp;</div>
				<div class="fontBold">
					INFORMATION WE COLLECT&nbsp;</div>
				<div>
					Contact information. We might collect your Name , Email , Phone Payment and billing information. We might collect your billing name, billing address and payment method. We NEVER collect your credit card number or credit card expiry date or other details pertaining to your credit card on our website. Credit card information will be obtained and processed by our online payment partner.&nbsp;</div>
				<div>
					Information you post. We collect information you post in a public space on our website or on a third-party social media site belonging to Chargenmarch .&nbsp;</div>
				<div>
					Demographic information. We may collect demographic information about you, or any other information provided by your during the use of our website. We might collect this as a part of a survey also.&nbsp;</div>
				<div>
					Other information. If you use our website, we may collect information about your IP address and the browser you&#39;re using. We might look at what site you came from, duration of time spent on our website, pages accessed or what site you visit when you leave us. We might also collect the type of mobile device you are using, or the version of the operating system your computer or device is running.&nbsp;</div>
				<div>
					&nbsp;</div>
				<div>
					&nbsp;</div>
				<div class="fontBold">
					WE COLLECT INFORMATION IN DIFFERENT WAYS.&nbsp;</div>
				<div>
					We collect information directly from you. We collect information directly from you when you contact us. We also collect information if you post a comment on our websites or ask us a question through phone or email.&nbsp;</div>
				<div>
					We collect information from you passively. We use tracking tools like Google Analytics, Google Webmaster, browser cookies and web beacons for collecting information about your usage of our website.&nbsp;</div>
				<div>
					We get information about you from third parties. For example, if you use an integrated social media feature on our websites. The third-party social media site will give us certain information about you. This could include your name and email address.&nbsp;</div>
				<div>
					&nbsp;</div>
				
				<div class="fontBold">
					USE OF YOUR PERSONAL INFORMATION</div>
				
				<div>
					We use information to contact you: We might use the information you provide to contact you for confirmation of a purchase on our website or for other promotional purposes.&nbsp;</div>
				<div>
					We use information to respond to your requests or questions. We might use your information to confirm your registration for an event or contest.&nbsp;</div>
				<div>
					We use information to improve our products and services. We might use your information to customize your experience with us. This could include displaying content based upon your preferences.&nbsp;</div>
				<div>
					We use information to look at site trends and customer interests. We may use your information to make our website and products better. We may combine information we get from you with information about you we get from third parties.</div>
				<div>
					We use information for security purposes. We may use information to protect our company, our customers, or our websites.&nbsp;</div>
				<div>
					We use information for marketing purposes. We might send you information about special promotions or offers. We might also tell you about new features or products. These might be our own offers or products, or third-party offers or products we think you might find interesting. Or, for example, if you buy product(s) from us we&#39;ll enroll you in our newsletter.&nbsp;</div>
				<div>
					We use information to send you transactional communications. We might send you emails or SMS about your account or a purchase.&nbsp;</div>
				<div>
					We use information as otherwise permitted by law.&nbsp;</div>

		         <div></div>
		             
		        </div>
		    </div>
 </div>
</section>
 <!-- Failre Code End -->
<!-- Footer Section Start -->
<%@include file="jsp-includes/cam_footer.jsp" %>
<!-- Footer Section End -->
<!-- Menu Bar Code Start -->
<%@include file="jsp-includes/cam_menu_script.jsp" %>
<%@include file="jsp-includes/cam_menu.jsp" %>

<!-- Menu Bar Section End -->
</body>
<script type="text/javascript">
  function checkPasswords(passId, confPassId){
    var passvalue = $("#"+passId).val();
    var confpassvalue = $("#"+confPassId).val();
    if(passvalue == confpassvalue){
      return true;
    } else{
      alert("Passwords Do Not Match");
     return false;
   }
  }
</script>
</html>
