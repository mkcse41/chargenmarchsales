<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<compress:html removeComments="true" removeIntertagSpaces="true"><html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Successful Payment | Recharge Mobile, DTH, DataCard and Landline, Fast Recharge, Rewards</title>
	<meta name="keywords" content="Online recharge, Successful Recharge, Online mobile recharge, Rewards, Coupons"/>
	<meta name="Description" content="ChargenMarch.com - Your Recharge Of Rs. ${rechargeDTO.amount}/- is successful. Please proceed with next recharge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
	<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="images/png" rel="shortcut icon" />
	<%@include file="wap-jspincludes/css-javascript-include.jsp" %>
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-6260839974232701",
    enable_page_level_ads: true
  });
</script>
</head>
<body>
	<div class="cam-page">
		<div class="cam-main">
			<!-- Header Start -->
			<%@include file="wap-jspincludes/wap_cam_header.jsp" %>
			<div class="topbackground-failure">
				<div class="imageBack" style="height:260px;">
					<div class="topbackground mobile-bg active-bg" id="mobilepage">
					<div class="title_circle">
					  <span class="icoprofile-fill reviewUserIcon"></span>
				     </div>
				     <div style="color:white;font-family:sans-serif;font-family:16px;font-weight:600;margin-top:12px;text-align:center;"><span>Your recharge for Rs. 20 is done.</span></div>
				</div>
			</div> 
		</div>
		<div class="cam_charger_box">
			<div class="mobilepage slide_page" data-page-bg="mobilepage" id="mobile_section">
				<div class="cam-white-card-wrapper cam-pad-l-r mobile-section">
					<div class="cam-main-card">

	           <!--     <div class="TextCenterReview"  style="color:#000;margin-top:16px;">
							<span class="reviewMobileNo">${rechargeDTO.number}</span><br>
							<span class="reviewOperator">${rechargeDTO.operatorName}-${rechargeDTO.circleName} 
								<c:if test="${rechargeDTO.serviceType=='Mobile'}">
								<span class="Capatilaze">${rechargeDTO.mobileService}</span>
								<span class="Capatilaze" style="margin-top: 5px;display: inline-block;">OrderId:${rechargeDTO.orderId}</span>
							</c:if>
							</span>
					</div> -->

						<div class="row" style="margin-top: 35px;font-size: 15px;padding: 10px;color: #008000;">
							<div class="col-md-12"><div class="msg" style="float: right;color:gray;">Your recharge is done. You should get a confirmation SMS from your operator shortly. If you need any future assistance please write us at <a  style="color:#000;" href="mailto:care@chargenmarch.com" title="care@chargenmarch.com">care@chargenmarch.com</a> .</div></div>
						</div>
						<div class="line">&nbsp;</div>
						
					</div>
				</div>
			</div>
		</div>

	</div>
	<div style="margin-top: 10px;margin-left:19px;"> 
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Recharge Success Page -->
<ins class="adsbygoogle"
     style="display:inline-block;width:320px;height:50px"
     data-ad-client="ca-pub-6260839974232701"
     data-ad-slot="9774910673"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
     	<div  style="font-family: sans-serif;font-family: sans-serif;display: inline-block;background: #fff;margin: 6%;width: 86%;padding: 5px;"><a href="javascript:void()">Need some help?</a> We are more than happy to help you!
							Write an email at <a href="mailto:care@chargenmarch.com" title="care@chargenmarch.com">care@chargenmarch.com</a>		
<input type="button" value="DO ANOTHER RECHARGE" class="bluBtn" style="box-shadow: 3px 4px 2px #aaa;margin-top:20px;" onclick="redirectNewUrl('${applicationScope['baseUrl']}')">
							</div>
	
<%@include file="wap-jspincludes/wap-cam-footer.jsp" %>
	</div>
</body>
</html></compress:html>