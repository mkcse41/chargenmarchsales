<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<compress:html removeComments="true" removeIntertagSpaces="true"><html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ChargenMarch - Recharge Mobile, DTH, DataCard and Landline, Fast Recharge, Rewards</title>
	<meta name="keywords" content="Online recharge, Free Recharge, Prepaid Recharge, Online mobile recharge, Datacard Recharge"/>
	<meta name="Description" content="ChargenMarch.com-Simple way to recharge Mobile, DataCard, DTH, pay landline bills and keep on Marching forward. Get rewards with every payment on ChargenMarch"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
	<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="images/png" rel="shortcut icon" />
	<%@include file="wap-jspincludes/css-javascript-include.jsp" %>
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-6260839974232701",
    enable_page_level_ads: true
  });
</script>
</head>
<body>
<div id="mask"></div>
<div id="loader-wrapper">
            <div class="logo"><a href="javascript:void(0)"><span>Charge</span>&March</a></div>
            <div id="loader">
            </div>
</div>
<div class="cam-page">
		<div class="cam-main">
			<!-- <div class="cam-white-card-wrapper mobile-section" style="padding: 1em 1em;">
				<div class="cam-main-card" style="margin-top:0;">
					<img src="${applicationScope['baseUrl']}/resources/images/downloadappback.webp" style="width:85px;height:85px;padding:10px;"/>
					<div class="info-box-top">  <h1 class="document-title" itemprop="name"> <div class="id-app-title" tabindex="0">ChargenMarch</div> </h1> <div style="clear:both"></div> <div style="clear:both"></div>
						</div>
						<div  class="info-box-top">
					<div class="appinstallbutton"><a href="http://bit.ly/2aJ8QOS"> Download App  </a></div>
						</div>
				
				</div>
			</div>-->				

			<!-- Header Start -->
			<%@include file="wap-jspincludes/wap_cam_header.jsp" %>
			<%@include file="wap-jspincludes/wap-plans.jsp" %>
<c:if test="${empty(userbean) || (!empty(sessionScope.userbean) && sessionScope.userbean.isverified == 0)}">
			<input type="hidden" value="true" id="checkUserLogin"/>
		</c:if>
		<c:if test="${!empty(userbean)  && sessionScope.userbean.isverified == 1}">
		<input type="hidden" value="false" id="checkUserLogin"/>
</c:if>
		<div class="imageBack">
			<div class="topbackground mobile-bg">
			    <div id="mobilepage">
					<div class="title_circle">
					 <span class="icomobile" style="margin-top: 10px;font-weight:600;"></span>
					</div>
			   </div>
			   	<div class="DispNone" id="dthpage">
					<div class="title_circle">
					 <span class="icodth" style="margin-top: 10px;"></span>
					</div>
		    	</div>
		    	<div class="DispNone" id="datacardpage">
					<div class="title_circle">
					   <span class="icodatacard" style="margin-top: 10px;"></span>
					</div>
		    	</div>
		    	<div class="DispNone" id="moneypage">
					<div class="title_circle">
					   <span class="icocashpay" style="margin-top: 10px;"></span>
					</div>
			    </div>
			  <section class="mnUnitTb ng-scope"> 
			  <font> 
		      <a class="active showArrow mobile" onclick="showThemeDiv('mobilepage','dthpage','datacardpage','moneypage');" ><span  style="font-weight:800" class="colorbordermobile">Mobile</span> </a> 
		      <a  onclick="showThemeDiv('dthpage','mobilepage','datacardpage','moneypage');" class="showArrow dth"> <span  style="font-weight:800" class="colorborderdth">DTH</span> </a> 
		      <a class="showArrow data"  onclick="showThemeDiv('datacardpage','mobilepage','dthpage','moneypage');"> <span  style="font-weight:800" class="colorborderdatacard">Data card</span> </a> 
		       <a class="showArrow landline"  onclick="showThemeDiv('moneypage','mobilepage','dthpage','datacardpage');"> <span  style="font-weight:800" class="colorborderlandline">Add Money</span> </a> 
		      </font>
		      </section>
			</div>
		</div> 

	<div>
		<div class="mobilepage" id="mobile_section">
			<div class="cam-white-card-wrapper mobile-section">
				<div class="cam-main-card">
					<div style="font-family: sans-serif;padding: 10px;">
						<label for="mobilePrepaidType">
							<input type="radio" checked="checked" name="mobileRechargeService" id="mobilePrepaidType" value="prepaid" data-ng-model="formData.connectionType" data-ng-change="changeType(formData.connectionType)" class="ng-pristine ng-untouched ng-valid" onclick="selectMobileServiceType('mobileRechargeService')">
							<i class="cstmsd_radio"></i>Prepaid</label>
							<label for="mobilePostpaidType">
								<input type="radio" tabindex="2"name="mobileRechargeService" id="mobilePostpaidType" value="postpaid" data-ng-model="formData.connectionType" data-ng-change="changeType(formData.connectionType)" class="ng-pristine ng-untouched ng-valid"onclick="selectMobileServiceType('mobileRechargeService')">
								<i class="cstmsd_radio"></i>Postpaid</label>
							</div>   
							<div>
								<div>
									<input class="HomeInput" type="tel" value="" maxlength="10" placeholder="Enter Your Mobile Number" data-required="true" id="rechargeMobileNumber" name="rechargeMobileNumber" onkeypress="return isNumberKey(event);" onblur="getOperatorDetails('rechargeMobileNumber','Mobile','mobilePrepaid','mobilePostpaid','mobileNumberErrorMsg','mobileRechargeService','mobileCircle');">
									<div class="errorText mobileNumberErrorMsg"></div>
								</div>

								<div class="select-box" id="mobilePrepaidOperator">
									<select class="HomeInput W88P" id="mobilePrepaid" name="mobilePrepaid" data-focus="none" data-trigger="blur" data-required="false">
										<option id="operatorDefault" disabled="" selected="" value="">Select Operator</option>
									</select>
									<div class="errorText mobileOperatorErrorMessage"></div>
								</div>

								<div class="select-box DispNone" id="mobilePostpaidOperator">
									<select class="HomeInput W88P" id="mobilePostpaid" name="mobilePostpaid" data-focus="none" data-trigger="blur" data-required="false">
										<option id="operatorDefault" disabled="" selected="" value="">Select Operator</option>
									</select>
									<div class="errorText mobileOperatorErrorMessage"></div>
								</div>
								<div class="select-box">
									<select class="HomeInput W88P" id="mobileCircle" name="mobileCircle" data-focus="none" data-trigger="blur" data-required="false">
										<option id="operatorDefault" disabled="" selected="" value="">Select Circle</option>
									</select>
									<div class="errorText mobileStateMessage"></div>
								</div>

								<div>
									<input class="HomeInput" id="mobileRechargeAmount" type="tel" value="" name="mobileRechargeAmount" style="width: 60% !important;" maxlength="10" placeholder="Enter Recharge Amount" data-required="true" onkeypress="return isNumberKey(event)" maxlength="4">
									<c:if test="${rechargeServiceName=='JOLO'}">
									<span id="mobilePlanLink"><a href="javascript:void(0)" class="check-plans ng-scope plan" style="color: #4dbea0;font-size: 14px;margin-left: -10px;" onclick=" createWapGAClickEventTrackingNonInteraction('viewplans_WAP', 'Details', 'on_button');  return getMobilePlan('mobilePlanDiv','rechargeMobileNumber','mobilePlanData')">View Plans</a></span>
										</c:if>
									<div class="errorText mobileRechargeAmountErrorMsg"></div>
								</div>
								 <div class="QuikRechargeText">
                                 <input type="checkbox" id="quikRechargeMobile" name="quikRechargeMobile" class="quikRechargeCheckBox" style="width: 14px;display: inline-block;">
                                 <label for="quikRechargeMobile">One Step Recharge</label>
                                 </div>
								<input type="button" value="Recharge Now" class="bluBtn" onclick=" createWapGAClickEventTrackingNonInteraction('MobileRecharge_WAP', 'Details', 'on_button'); return processedToMobileRechargeCoupon('checkUserLogin','mobileRechargeService','rechargeMobileNumber','mobilePrepaid','mobilePostpaid','mobileRechargeAmount','mobileCircle')">

							</div>  

						</div>

					</div>
				</div>
				<div class="dthpage DispNone" id="dth_section">
					<div class="cam-white-card-wrapper mobile-section">

						<div class="cam-main-card">
							<div class="select-box" id="dthOperatorDiv">
								<select class="HomeInput W88P" id="dthoperator" name="dthoperator" data-focus="none" data-trigger="blur" data-required="false">
									<option id="operatorDefault" disabled="" selected="" value="">Select DTH Operator</option>
								</select>
								<div class="errorText dthOperatorErrorMsg"></div>
							</div>
							<div>
								<input class="HomeInput" type="tel" value="" placeholder="Enter Your Viewing Card Number" data-required="true" id="dthOpertorNumber" name="dthOpertorNumber" onkeypress="return isNumberKey(event);" maxlength="14"  onblur="validateDTHNumber('dthOpertorNumber','dthNumberMessageError','dthDataOperator');">
								<div class="errorText dthNumberMessageError"></div>
							</div>
							<div>
								<input class="HomeInput" type="tel" value="" placeholder="Enter DTH Recharge Amount" data-required="true" onkeypress="return isNumberKey(event)" id="dthAmount" name="dthAmount" maxlength="5">
								<div class="errorText dthAmountMessage"></div>
							</div>
							  <div class="QuikRechargeText">
                               <input type="checkbox" id="quikRechargeDTH" name="quikRechargeDTH" class="quikRechargeCheckBox" style="width: 14px;display: inline-block;">
                               <label for="quikRechargeDTH">One Step Recharge</label>
              				  </div>
							<input type="button" value="Recharge Now" class="bluBtn" style="background: #4DB6AC !important;" onclick="createWapGAClickEventTrackingNonInteraction('DTHRecharge_WAP', 'Details', 'on_button');  return processedToDTHRechargeCoupon('checkUserLogin','dthOpertorNumber','dthoperator','dthAmount')">
						</div>

					</div>
				</div>
				<div class="datacardpage DispNone" id="datacard_section">
					<div class="cam-white-card-wrapper mobile-section">

						<div class="cam-main-card">
							<div style="font-family: sans-serif;padding: 10px;" class="DispNone">
								<label for="datacardPrepaidType">
									<input type="radio" checked="checked" name="dataCardRechargeService" id="datacardPrepaidType" value="prepaid" data-ng-model="formData.connectionType" data-ng-change="changeType(formData.connectionType)" class="ng-pristine ng-untouched ng-valid" onclick="selectMobileServiceType('dataCardRechargeService')">
									<i class="cstmsd_radio"></i>Prepaid</label>
									<label for="mobilePostpaidType">
										<input type="radio" tabindex="2"name="dataCardRechargeService" id="datacardPostpaidType" value="postpaid" data-ng-model="formData.connectionType" data-ng-change="changeType(formData.connectionType)" class="ng-pristine ng-untouched ng-valid"onclick="selectMobileServiceType('dataCardRechargeService')">
										<i class="cstmsd_radio"></i>Postpaid</label>
									</div>   
									<div>
										<div>
											<input class="HomeInput" type="tel" value="" maxlength="10" placeholder="Enter Data Card Number" data-required="true" id="datacardRechargeNumber" name="datacardRechargeNumber" onkeypress="return isNumberKey(event);" onblur="getOperatorDetails('datacardRechargeNumber','DTH','datacardoperator','mobilePostpaid','DataCardNumberMessage','dataCardRechargeService','datacardCircle');">
											<div class="errorText DataCardNumberMessage"></div>
										</div>
										<div class="select-box" id="mobilePrepaidOperator">
											<select class="HomeInput W88P" id="datacardoperator" name="datacardoperator" data-focus="none" data-trigger="blur" data-required="false">
												<option id="operatorDefault" disabled="" selected="" value="">Select Operator</option>
											</select>
											<div class="errorText datacardPrepaidOperatorError"></div>
										</div>
										<div class="select-box DispNone" id="mobilePostpaidOperator">
											<select class="HomeInput W88P" id="datacardPostpaid" name="datacardPostpaid" data-focus="none" data-trigger="blur" data-required="false">
												<option id="operatorDefault" disabled="" selected="" value="">Select Operator</option>
											</select>
											<div class="errorText datacardPrepaidOperatorError"></div>
										</div>
										<div class="select-box">
											<select class="HomeInput W88P" id="datacardCircle" name="datacardCircle" data-focus="none" data-trigger="blur" data-required="false">
												<option id="operatorDefault" disabled="" selected="" value="">Select Circle</option>
											</select>
											<div class="errorText DataCardStateErrorMessage"></div>
										</div>
										<div>
											<input class="HomeInput" type="tel" value="" placeholder="Enter DataCard Recharge Amount" data-required="true" onkeypress="return isNumberKey(event)" id="datacardRechargeAmount" name="datacardRechargeAmount" maxlength="5">
											<div class="errorText datacardRechargeAmountErrorMessage"></div>
										</div>
										  <div class="QuikRechargeText">
                                            <input type="checkbox" id="quikRechargeDataCard" name="quikRechargeDataCard" class="quikRechargeCheckBox" style="width: 14px;display: inline-block;">
               								<label for="quikRechargeDataCard">One Step Recharge</label>
              							  </div>
										<input type="button" value="Recharge Now" class="bluBtn" style="background: #00897B !important;" onclick="createWapGAClickEventTrackingNonInteraction('DataCardRecharge_WAP', 'Details', 'on_button');return processedToDatacardRechargeCoupon('checkUserLogin','dataCardRechargeService','datacardRechargeNumber','datacardoperator','','datacardRechargeAmount','datacardCircle')">
									</div>  
								</div>
							</div>
						</div>
						<div class="moneypage DispNone" id="money_section">
					      <div class="cam-white-card-wrapper mobile-section">
						       <div class="cam-main-card">
									<div>
										<div>
											<input class="HomeInput" type="tel" value="" maxlength="4" placeholder="Add Money" data-required="true" id="loadAmount" name="loadAmount" onkeypress="return isNumberKey(event);">
											<div class="errorText DataCardNumberMessage"></div>
										</div>
										<input type="button" value="ADD" class="bluBtn" style="background: #00897B !important;" onclick="createWapGAClickEventTrackingNonInteraction('AddMoney_WAP', 'Details', 'on_button');return addMoney();">
									</div> 
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- WAP Add money -->
<ins class="adsbygoogle"
     style="display:inline-block;width:320px;height:100px"
     data-ad-client="ca-pub-6260839974232701"
     data-ad-slot="9635309878"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
 
								</div>

							</div>
						</div>
					</div>
				</div>
<%@include file="wap-jspincludes/wap-cam-footer.jsp" %>
  </div>
  

<script type="text/javascript">
				var prepaidSelectBox = document.getElementById("mobilePrepaid");
				var postpaidSelectBox = document.getElementById("mobilePostpaid");
				var dthSelectBox = document.getElementById("dthoperator");
				var datacardSelectBox = document.getElementById("datacardoperator");
				var mobileCircle = document.getElementById("mobileCircle");
				var datacardCircle = document.getElementById("datacardCircle");
				//alert("${rechargeServiceName}");
			</script>
<c:if test="${rechargeServiceName=='JOLO'}">
	<c:forEach var="operator" items="${prepaidoperators}">
		<script type="text/javascript">
		addPageScript(function() {
			addDropDown(prepaidSelectBox,"${operator.pay_code}","${operator.operatorName}");
		});
		</script>
	</c:forEach>
	<c:forEach var="postpaidoperator" items="${postpaidoperators}">
			<script type="text/javascript">
			addPageScript(function() {
				addDropDown(postpaidSelectBox,"${postpaidoperator.pay_code}","${postpaidoperator.operatorName}");
			});
			</script>
	</c:forEach>
	<c:forEach var="dthoperator" items="${dthoperators}">
			<script type="text/javascript">
			addPageScript(function() {
				addDropDown(dthSelectBox,"${dthoperator.pay_code}","${dthoperator.operatorName}");
			});
			</script>
	</c:forEach>
	<c:forEach var="dataCardOperator" items="${datacardoperators}">
			<script type="text/javascript">
			addPageScript(function() {
				addDropDown(datacardSelectBox,"${dataCardOperator.pay_code}","${dataCardOperator.operatorName}");
			});
			</script>
	</c:forEach>
	<c:forEach var="state" items="${circles}">
			<script type="text/javascript">
			addPageScript(function() {
				addDropDown(mobileCircle,"${state.circleCode}","${state.circleName}");
				addDropDown(datacardCircle,"${state.circleCode}","${state.circleName}");
			});
			</script>
	</c:forEach>
</c:if>


<c:if test="${rechargeServiceName!='JOLO'}">
	<c:forEach var="operator" items="${prepaidoperators}">
		<script type="text/javascript">
		addPageScript(function() {
			addDropDown(prepaidSelectBox,"${operator}","${operator.operatorName}");
		});
		</script>
	</c:forEach>
	<c:forEach var="postpaidoperator" items="${postpaidoperators}">
			<script type="text/javascript">
			addPageScript(function() {
				addDropDown(postpaidSelectBox,"${postpaidoperator}","${postpaidoperator.operatorName}");
			});
			</script>
	</c:forEach>
	<c:forEach var="dthoperator" items="${dthoperators}">
			<script type="text/javascript">
			addPageScript(function() {
				addDropDown(dthSelectBox,"${dthoperator}","${dthoperator.operatorName}");
			});
			</script>
	</c:forEach>
	<c:forEach var="dataCardOperator" items="${datacardoperators}">
			<script type="text/javascript">
			addPageScript(function() {
				addDropDown(datacardSelectBox,"${dataCardOperator}","${dataCardOperator.operatorName}");
			});
			</script>
	</c:forEach>
	<c:forEach var="state" items="${circles}">
			<script type="text/javascript">
			addPageScript(function() {
				addDropDown(mobileCircle,"${state.circleCode}","${state}".replace("_"," "));
				addDropDown(datacardCircle,"${state.circleCode}","${state}".replace("_"," "));
			});
			</script>
	</c:forEach>
</c:if>
<script type="text/javascript">
	addPageScript(function() {
			loaderEnd();
			$('.mnUnitTb a').on('click', function (e) {
			  var $this = $(this);
			  $this.addClass('active');
			  $this.siblings().removeClass('active');
			});

		});
function showThemeDiv (divmain,div1,div2,div3) {
$('#'+divmain).removeClass('DispNone');
$('#'+div1).addClass('DispNone');
$('#'+div2).addClass('DispNone');
$('#'+div3).addClass('DispNone');

$('.'+divmain).removeClass('DispNone');
$('.'+div1).addClass('DispNone');
$('.'+div2).addClass('DispNone');
$('.'+div3).addClass('DispNone');

}
</script>
<!-- Recharge details use after Login Details -->
<input type="hidden" id="number">
<input type="hidden" id="operatorValue">
<input type="hidden" id="mobileService">
<input type="hidden" id="amount">
<input type="hidden" id="service">
<input type="hidden" id="operatorText">
<input type="hidden" id="circleValue">
<input type="hidden" id="circleText">
<input type="hidden" id="isMobilePlan" value="false">
</body>
</html></compress:html>