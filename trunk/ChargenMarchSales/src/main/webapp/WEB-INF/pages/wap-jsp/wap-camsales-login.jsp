<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<compress:html removeComments="true" removeIntertagSpaces="true"><html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ChargenMarch - Recharge Mobile, DTH and DataCard, Fast & Free Recharge, Rewards | ChargenMarch.com</title>
<meta name="keywords" content="Online recharge, Mobile recharge, Prepaid Recharge, Online mobile recharge, Datacard Recharge"/>
<meta name="Description" content="ChargenMarch.com - Simple way to Recharge Prepaid Mobile, DataCard, DTH and keep on Marching forward. Get rewards with every Online Recharge on ChargenMarch. Store money in Wallet on ChargenMarch"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="images/png" rel="shortcut icon" />
<script type="text/javascript">var basepath = "${applicationScope['baseUrl']}";</script>
<script src="${applicationScope['baseUrl']}/resources/wap-javascript/jquery-1.11.3.min.js" defer></script>
<script src="${applicationScope['baseUrl']}/resources/wap-javascript/wap-global.js" defer onload="loadPageScript();"></script>
<link rel="stylesheet" type="text/css" href="${applicationScope['baseUrl']}/resources/wap-css/wap-userlogin.css">
</head>
<script type="text/javascript">
  var pageScript=[],pageLoaded=!1,addPageScript=function(o){"function"==typeof o&&(pageLoaded?o():pageScript.push(o))},loadPageScript=function(){pageLoaded=!0;for(var o in pageScript)pageScript[o]()},windowLoadScript=[],windowLoaded=!1,addWindowLoadScript=function(o){"function"==typeof o&&(windowLoaded?o():windowLoadScript.push(o))};window.onload=function(){windowLoaded=!0;for(var o in windowLoadScript)windowLoadScript[o]()};
  </script>
<body>
  <div id="mask"></div>
<div id="loader-wrapper">
            <div class="logo"><a href="javascript:void(0)"><span>Charge</span>&March</a></div>
            <div id="loader">
            </div>
</div>
 <div class="cam-header" id="header">
     <div id="headerLogo" class="headerLogo" style="color: #FFF;">
     <span onclick="redirectNewUrl('${applicationScope['baseUrl']}')" style="margin-left: 20px;font-size: 16px;font-family: sans-serif;font-weight: 600;">ChargeNmarch</span>
     </div>
  </div>  
<div class="LoginForm">
      
      <ul class="tab-group">
        <li class="Logintab active"><a href="#login">Log In</a></li>
      </ul>
      
      <div class="tab-content">
        
        <div id="login">   
          <h1>Welcome Back!</h1>
          
          
            <div class="field-wrap">
            <label>
              Email Address<span class="req">*</span>
            </label>
            <input type="email" id="chargers_emailId" name="chargers_emailId" value=""  maxlength="30" onkeypress="return spaceNotAllowed(event)" required autocomplete="off"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Password<span class="req">*</span>
            </label>
            <input id="chargers_password" name="chargers_password" value="" type="password" required autocomplete="off"/>
          </div>
          
          <div id="errorMessagelogin" class="errorText"></div>
          <button class="button button-block" onclick="return userLogin('chargers_emailId','chargers_password','errorMessagelogin')">Continue</button>
          

        </div>
      </div><!-- tab-content -->

       <div id="userOtp" class="overlay">
          <div class="popup">
           <!--  <a class="close" href="#">&times;</a> -->
            <div class="content">
                <div class="field-wrap">
                  <label style="color:black">OTP<span class="req">*</span></label>
                  <input style="color:black" type="mobile" id="regOTP"  maxlength="6"  name="regOTP" onkeypress="return isNumberKey(event)" required autocomplete="off"/>

                    <button type="button" id="otpButton" onclick="return submitUserOTP('regOTP','userOtp','errorMessageOTP');"  class="buttonOTP">Continue</button>

                     <div id="errorMessageOTP" class="errorText"></div>

                </div>
            </div>
          </div>
        </div>
</div> <!-- /form -->
<script type="text/javascript">
  addPageScript(function() {
      loaderEnd();
    });
</script>

</body>
</html></compress:html>