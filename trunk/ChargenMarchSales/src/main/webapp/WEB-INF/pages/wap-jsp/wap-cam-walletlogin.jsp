<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<compress:html removeComments="true" removeIntertagSpaces="true"><html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ChargenMarch - Recharge Mobile, DTH and DataCard, Fast & Free Recharge, Rewards | ChargenMarch.com</title>
<meta name="keywords" content="Online recharge, Mobile recharge, Prepaid Recharge, Online mobile recharge, Datacard Recharge"/>
<meta name="Description" content="ChargenMarch.com - Simple way to Recharge Prepaid Mobile, DataCard, DTH and keep on Marching forward. Get rewards with every Online Recharge on ChargenMarch. Store money in Wallet on ChargenMarch"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="images/png" rel="shortcut icon" />
<script type="text/javascript">var basepath = "${applicationScope['baseUrl']}";</script>
<script src="${applicationScope['baseUrl']}/resources/wap-javascript/jquery-1.11.3.min.js" defer></script>
<script src="${applicationScope['baseUrl']}/resources/wap-javascript/wap-userwallet.js" defer></script>
  <script src="https://apis.google.com/js/api:client.js" defer></script>
  <script src="${applicationScope['baseUrl']}/resources/wap-javascript/google_signIn.js" defer></script>
<link rel="stylesheet" type="text/css" href="${applicationScope['baseUrl']}/resources/wap-css/wap-userwallet.css">
</head>
<body>

<div class="form">
      
      <ul class="tab-group">
        <li class="tab active"><a href="#signup">Register</a></li>
        <li class="tab"><a href="#login">Log In</a></li>
      </ul>
      
      <div class="tab-content">
        <div id="signup">   

            <div class="field-wrap">
              <label>
                First Name<span class="req">*</span>
              </label>
              <input type="text" required autocomplete="off" id="chargers_reg_name" name="chargers_reg_name" maxlength="30"  onkeypress="return onlyAlphabets(event,this)"/>
            </div>

          <div class="field-wrap">
            <label>
              Email Address<span class="req">*</span>
            </label>
            <input type="email" id="charger_reg_emailId" name="charger_reg_emailId" onkeypress="return spaceNotAllowed(event)" maxlength="30" onkeypress="return spaceNotAllowed(event)"  required autocomplete="off"/>
          </div>

            <div class="field-wrap">
            <label>
              Mobile Number<span class="req">*</span>
            </label>
            <input type="mobile" id="charger_reg_mobileNo"  maxlength="10"  name="charger_reg_mobileNo" onkeypress="return isNumberKey(event)" required autocomplete="off"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Set A Password<span class="req">*</span>
            </label>
            <input type="password" id="charger_reg_password" maxlength="14" name="charger_reg_password" onkeypress="return spaceNotAllowed(event)" required autocomplete="off"/>
          </div>
          <div id="errorMessagesignin" class="errorText"></div>
          <button type="button" id="regButton" onclick="return userRegister('chargers_reg_name','charger_reg_emailId','charger_reg_mobileNo','charger_reg_password','WAP','true','errorMessagesignin')" class="button button-block">Continue</button>
          

        </div>


        
        <div id="login" style="display:none;">   
          <h1>Welcome Back!</h1>
          
          
            <div class="field-wrap">
            <label>
              Email Address<span class="req">*</span>
            </label>
            <input type="email" id="chargers_emailId" name="chargers_emailId" value=""  maxlength="30" onkeypress="return spaceNotAllowed(event)" required autocomplete="off"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Password<span class="req">*</span>
            </label>
            <input id="chargers_password" name="chargers_password" value="" type="password" required autocomplete="off"/>
          </div>
          
          <p class="forgot"><a href="#" onclick="showForgotPassword('forgotPassword');">Forgot Password?</a></p>
          <div id="errorMessagelogin" class="errorText"></div>
          <button class="button button-block" onclick="return userLogin('chargers_emailId','chargers_password','errorMessagelogin')">Continue</button>
          

        </div>
      </div><!-- tab-content -->

       <div id="userOtp" class="overlay">
          <div class="popup">
           <!--  <a class="close" href="#">&times;</a> -->
            <div class="content">
                <div class="field-wrap">
                  <label style="color:black">OTP<span class="req">*</span></label>
                  <input style="color:black" type="mobile" id="regOTP"  maxlength="6"  name="regOTP" onkeypress="return isNumberKey(event)" required autocomplete="off"/>

                    <button type="button" id="otpButton" onclick="return submitUserOTP('regOTP','userOtp','errorMessageOTP');"  class="buttonOTP">Continue</button>

                     <div id="errorMessageOTP" class="errorText"></div>

                </div>
            </div>
          </div>
        </div>

          <div id="MobileVerification" class="overlay">
          <div class="popup">
           <!--  <a class="close" href="#">&times;</a> -->
            <div class="content">
                <div class="field-wrap">
                  <label style="color:black">Mobile Number<span class="req">*</span></label>
                  <input style="color:black" type="mobile" id="userMobileNo"  maxlength="10"  name="userMobileNo" onkeypress="return isNumberKey(event)" required autocomplete="off"/>

                    <button type="button" id="userMobileNoButton" onclick="return submitUserMobile('userMobileNo','errorMessageLoginmobile');"  class="buttonOTP">Continue</button>

                         <div id="errorMessageLoginmobile" class="errorText"></div>   

                </div>
            </div>
          </div>
        </div>


         <div id="forgotPassword" class="overlay">
          <div class="popup">
            <a class="close" href="#" onclick="closePopUp('forgotPassword');">&times;</a>
            <div class="content">
                <div class="field-wrap">
                  <label style="color:black">Email Id<span class="req">*</span></label>
                  <input style="color:black" type="text" id="forgotEmail"  maxlength="30"  name="forgotEmail" onkeypress="return spaceNotAllowed(event)" required autocomplete="off"/>

                    <button type="button" id="otpButton" onclick="return forgotPassword('forgotEmail','errorMessageForgot');"  class="buttonOTP">Continue</button>

                     <div id="errorMessageForgot" class="errorText"></div>

                </div>
            </div>
          </div>
        </div>

</div> <!-- /form -->

   <div class="bottomfooter">
          <span class="colorWhite">Or sign in with</span>
          <div style="margin-top: 10px;margin-bottom: 10px;" id="gSignInWrapper">
            <div id="customBtn">
            <button class="buttonlogin">Google</button></div></div>
        </div>

</body>
</html></compress:html>