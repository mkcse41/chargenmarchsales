<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<compress:html removeComments="true" removeIntertagSpaces="true"><html lang="en" class="Hight100P">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Review Order - Apply coupons, Get discounts and Cashback | ChargenMarch.com</title>
	<meta name="keywords" content="Online mobile recharge, Review Recharge, Prepaid Recharge, Easy Rewards, Free Recharge"/>
	<meta name="Description" content="ChargenMarch.com - Apply coupons to get exciting cashbacks and discounts. Get rewards with every payment on ChargenMarch"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
	<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="images/png" rel="shortcut icon" />
	<c:set var="pageName" value="ReviewPage"/>
	<%@include file="wap-jspincludes/css-javascript-include-other.jsp" %>
</head>
<body class="Hight100P">
	 <div id="mask"></div>
	<div id="loader-wrapper">
            <div class="logo"><a href="javascript:void(0)"><span>Charge</span>&March</a></div>
            <div id="loader">
            </div>
</div>

	<div class="cam-page Hight100P">
		<div class="cam-main">
			<!-- Header Start -->
			<%@include file="wap-jspincludes/wap_cam_header.jsp" %>
			<div class="topbackground-wrp reviewOrder-wrp">
					<div class="imageBack" style="height: 221px;">
						<div class="topbackground mobile-bg active-bg" id="mobilepage">
							<div class="title_circle">
								<span class="icoprofile-fill reviewUserIcon"></span>
							</div>
							<div class="TextCenterWalletReview">
								<span>CAM Wallet : <i class="fa fa-rupee"></i>20</span><br>
								 <span class="errormsg userReferralErrorMessage" style="display: inline-block;color:red !important;margin-top:0px;"></span>
							</div>
						</div>
						
					</div> 



	        </div>
					<div class="cam_charger_box" style="margin-top:-29px">
					  <div class="mobilepage slide_page" data-page-bg="mobilepage" id="mobile_section">
						<div class="cam-white-card-wrapper cam-pad-l-r mobile-section">
							<div class="cam-main-card">
								<div class="reviewSpan">
								<span>You Pay : </span><div style="display: inline-block;" class="paidAmount"><span>
								<span id="paidAmount">10</span></span></div>
							   </div>
								
								   <button type="button" class="reviewButton button-block" id="regButton" onclick="return userRegister('chargers_reg_name','charger_reg_emailId','charger_reg_mobileNo','charger_reg_password','WAP','true','errorMessagesignin')" >Continue</button>
								</div>
							</div>
						</div>
					  </div>
					</div>
	     </div>
	   </div>     
	<script type="text/javascript">
	addPageScript(function() {
			loaderEnd();
		});
</script>
</body>
</html></compress:html>