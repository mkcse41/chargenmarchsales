<script type="text/javascript">
	var pageScript=[],pageLoaded=!1,addPageScript=function(o){"function"==typeof o&&(pageLoaded?o():pageScript.push(o))},loadPageScript=function(){pageLoaded=!0;for(var o in pageScript)pageScript[o]()},windowLoadScript=[],windowLoaded=!1,addWindowLoadScript=function(o){"function"==typeof o&&(windowLoaded?o():windowLoadScript.push(o))};window.onload=function(){windowLoaded=!0;for(var o in windowLoadScript)windowLoadScript[o]()};
</script>
<link rel="manifest" href="${applicationScope['baseUrl']}/resources/manifest.json">
<script type="text/javascript">var basepath = "${applicationScope['baseUrl']}";window.localStorage.setItem("cid",'${user.id}');</script>
<script src="${applicationScope['baseUrl']}/resources/wap-javascript/jquery-1.11.3.min.js" defer></script>
<script src="${applicationScope['baseUrl']}/resources/wap-javascript/wap-global.js" defer onload="loadPageScript();"></script> 
<script type="text/javascript" src="${applicationScope['baseUrl']}/resources/notification/wap-chrome-pushwoosh-notification.js" defer onload="startPushWooshService()"></script>
<link rel="stylesheet" type="text/css" href="${applicationScope['baseUrl']}/resources/wap-css/wap-global.css">
