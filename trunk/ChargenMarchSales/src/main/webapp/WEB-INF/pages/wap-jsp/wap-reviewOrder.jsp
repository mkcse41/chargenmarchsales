<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<compress:html removeComments="true" removeIntertagSpaces="true"><html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Review Order - Apply coupons, Get discounts and Cashback | ChargenMarch.com</title>
	<meta name="keywords" content="Online mobile recharge, Review Recharge, Prepaid Recharge, Easy Rewards, Free Recharge"/>
	<meta name="Description" content="ChargenMarch.com - Apply coupons to get exciting cashbacks and discounts. Get rewards with every payment on ChargenMarch"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
	<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="images/png" rel="shortcut icon" />
	<c:set var="pageName" value="ReviewPage"/>
	<%@include file="wap-jspincludes/css-javascript-include-other.jsp" %>
</head>
<body>
	 <div id="mask"></div>
	<div id="loader-wrapper">
            <div class="logo"><a href="javascript:void(0)"><span>Charge</span>&March</a></div>
            <div id="loader">
            </div>
</div>
	<div class="cam-page">
		<div class="cam-main">
			<!-- Header Start -->
			<%@include file="wap-jspincludes/wap_cam_header.jsp" %>
		<div class="imageBack" style="height: 221px;">
			<div class="topbackground mobile-bg active-bg" id="mobilepage" style="padding: 65px 0 50px;">
				<div style="text-align: center;color: #fff;">
					<span class="icoprofile-fill reviewUserIcon"></span>
				</div>
				<div class="TextCenterReview">
					<span class="reviewMobileNo">${mobileNo}</span><br>
					<span class="reviewOperator Capatilaze">${operator}<c:if test="${!empty(mobileRechargeDto.circleName)}">-${mobileRechargeDto.circleName}</c:if> 
						<c:if test="${mobileRechargeDto.serviceType=='Mobile'}">
						<span class="Capatilaze">-${mobileRechargeDto.mobileService}</span>
					    </c:if>
					</span>
					 <span class="errormsg userReferralErrorMessage" style="display: inline-block;color:red !important;margin-top:0px;"></span>
				</div>
			</div>
			
		</div> 
	<div class="cam_charger_box">
		<div class="mobilepage slide_page" data-page-bg="mobilepage" id="mobile_section">
			<div class="cam-white-card-wrapper cam-pad-l-r mobile-section">
				<div class="cam-main-card">
					<!-- Coupon Apply Div Start  -->
					<div class="coupon">
						<div style="padding: 10px;">
							<div style="display: inline-block;" class="paidAmount">
								<span ><i class="fa fa-rupee"></i> <span id="paidAmount">${amount}</span></span>
							</div>
								<div style="width: 70%;float: right;height: 40px;margin-top:6px;">
								<input type="text" class="amount parsley-validated parsley-error" data-required="true" name="couponName" id="couponName" placeholder="Coupon Code" onkeypress="allowAlphaNumeric(event)" style="height: 36px;font-size: 16px;width: 100px;    border: 1px solid #ccc;border-radius: 6px;padding: 4px;">
									<input type="hidden" id="isReedomRewardsPointsId" value="false">
									<input type="hidden" id="rewardsPointRedeemAmount" value="0">
									<input type="button" class="bluBtnCoupon" onclick="createWapGAClickEventTrackingNonInteraction('ApplyCoupon_WAP', 'Details', 'on_button'); redeemCoupon('couponName','${amount}','${userbean.emailId}','${service}','couponErrorMessage','couponSpanId','paidAmount','walletBalanceId')" value="Apply" style="height: 43px;width: 65px;background: rgb(0, 137, 123) none repeat scroll 0% 0%;color: rgb(255, 255, 255);border: medium none;box-shadow: 0px 2px 4px 1px rgb(204, 204, 204);margin-left: 5px;border-radius: 6px;margin-top: -5px;" />
								</div>
							</div>
						</div>
						<c:if test="${rechargeDTO.mobileService=='postpaid'}">
					        <span style="margin-top:10px;color:black;font-size:12px">&nbsp; &nbsp; Recharge Amount: Rs. ${amount - extraamount}</span>
					        <span style="margin-top:10px;margin-bottom:5px;color:black;font-size:12px">&nbsp; &nbsp; Extra Charges: Rs. ${extraamount}</span>
					    </c:if>
						<p class="couponErrorMessage" style="padding:3px;font-size:14px;color:green">Note: Coupon applied here is not valid for payment done through CAM Wallet a.k.a. CAMllet</p>
					</div>
				</div>
				<div class="cam-white-card-wrapper cam-pad-l-r mobile-section" >
					<div class="cam-main-card" style="height:110px;">
						<!-- Coupon Apply Div Start  -->
						<div class="rewardsPoint">
							Rewards Points
								<div class="help-tip">
									<p>Loyalty Rewards Points:  ChargenMarch rewards the chargers with 1 loyalty point with every successfull transaction worth &#8377;10. These points can later be redeemed to pay in future transactions.</p>
								</div>
							<div id="" class="showValuePoints">
								<span id="rewardsPointsId" class="ShowPoints">${rewardsPoints.rewardsPoint}</span> <c:if test="${rewardsPoints.rewardsPoint>=10}"><span id="redeemRewardsPointsId"
								style=" float: left;margin-left: -56px;margin-top: 23px;width:150px;">
								<a style=" -moz-text-decoration-style: wavy;text-decoration: underline;" onclick="redeemRewardsPoints('${userbean.emailId}','couponSpanId','${amount}','paidAmount','isReedomRewardsPointsId','walletBalanceId','rewardsPointsId','rewardsPointRedeemAmount')">Redeem Points</a></span></c:if>
								   <span class="errormsg redeemRewardsErrorMessage" style="display: inline-block;"></span>
								  
							</div>
						</div>
							<div class="walletPoint">
							  Wallet
							  <div id="" class="showValuePoints">
								<span id="walletBalanceId" class="ReviewOrderColor ShowPoints">${userbean.walletBalance}</span>
							   </div>
							   <c:if test="${userbean.walletBalance>0}">
								<div class="isUseWalletCss"> <input type="checkbox" id="isUseWallet" name="isUseWallet" value="yes" onclick="manageWalletBalance('walletBalanceId','isUseWallet','paidAmount','rewardsPointRedeemAmount','${amount}','${userbean.walletBalance}','isUseReferral','${userbean.referralBalance}');" checked disabled/> <span style="color: black;"> Use Wallet </span></div></c:if>
							</div>
					</div>
				</div>

<c:if test="${userbean.referralBalance>0}">
<div class="cam-white-card-wrapper cam-pad-l-r mobile-section">
					<div class="cam-main-card" style="height:110px;">
						 <div style="padding:5px;"> Referral Amount <div class="help-tip">
									<p>Referral amount worth 3% of the total recharge amount can be redeemed at a time. Also you can not avail any other feature like wallet balance or reward points if you redeem referral amount for recharge..</p>
								</div> </div>
						  <div id="" class="refferalBlock">
							<span id="walletBalanceId" class="ReviewOrderColor ShowPoints">${userbean.referralBalance}</span>
						   </div>
						<c:if test="${userbean.referralBalance>0}">
							<div class="isrefeeralcss"> <input type="checkbox" style="margin-left:37%;" id="isUseReferral" name="isUseReferral" value="yes" onclick="manageUserReferralBalance('walletBalanceId','isUseWallet','paidAmount','rewardsPointRedeemAmount','${amount}','${userbean.walletBalance}','isUseReferral','${userbean.referralBalance}')"/> <span style="color: black;float:right;"> Use Referral </span></div>
						</c:if>
		</div>
		</div>				
</c:if>
<input type="button" value="Recharge Now" class="bluBtn" style="box-shadow: 3px 2px 2px #aaa;margin-top:20px;" onclick="createWapGAClickEventTrackingNonInteraction('ProceedOrder_WAP', 'Details', 'on_button'); confirmOrder('couponName','${service}','isReedomRewardsPointsId','isUseWallet')">
			</div>
		</div>
	</div>
		<%@include file="wap-jspincludes/wap-cam-footer.jsp" %>
	</div>
	<script type="text/javascript">
	var isWalletBalance = true;
	addPageScript(function() {
			loaderEnd();
            var camWalletBal = "${userbean.walletBalance}";
			manageWalletBalance('walletBalanceId','isUseWallet','paidAmount','rewardsPointRedeemAmount','${amount}','${userbean.walletBalance}','isUseReferral','${userbean.referralBalance}');
			if(undefined != camWalletBal && parseInt(camWalletBal) == 0){
               isWalletBalance = false;
             }
   });
</script>
</body>
</html></compress:html>