<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<compress:html removeComments="true" removeIntertagSpaces="true"><html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ChargenMarch - Recharge Mobile, DTH and DataCard, Fast & Free Recharge, Rewards | ChargenMarch.com</title>
<meta name="keywords" content="Online recharge, Mobile recharge, Prepaid Recharge, Online mobile recharge, Datacard Recharge"/>
<meta name="Description" content="ChargenMarch.com - Simple way to Recharge Prepaid Mobile, DataCard, DTH and keep on Marching forward. Get rewards with every Online Recharge on ChargenMarch. Store money in Wallet on ChargenMarch"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="images/png" rel="shortcut icon" />
<script type="text/javascript">var basepath = "${applicationScope['baseUrl']}";</script>
<script src="${applicationScope['baseUrl']}/resources/wap-javascript/jquery-1.11.3.min.js" defer></script>
<script src="${applicationScope['baseUrl']}/resources/wap-javascript/wap-global.js" defer onload="loadPageScript();"></script>
<link rel="stylesheet" type="text/css" href="${applicationScope['baseUrl']}/resources/wap-css/wap-userlogin.css">
<style type="text/css">
.profileSPAN{font-size: 18px;display: block;color: #dad5d5;padding: 7px 0;}
label{    color: #fff !important;}
</style>
<script type="text/javascript">
  var pageScript=[],pageLoaded=!1,addPageScript=function(o){"function"==typeof o&&(pageLoaded?o():pageScript.push(o))},loadPageScript=function(){pageLoaded=!0;for(var o in pageScript)pageScript[o]()},windowLoadScript=[],windowLoaded=!1,addWindowLoadScript=function(o){"function"==typeof o&&(windowLoaded?o():windowLoadScript.push(o))};window.onload=function(){windowLoaded=!0;for(var o in windowLoadScript)windowLoadScript[o]()};
  </script>
</head>
<body>
 <div id="mask"></div>
<div id="loader-wrapper">
            <div class="logo"><a href="javascript:void(0)"><span>Charge</span>&March</a></div>
            <div id="loader">
            </div>
</div>
<%@include file="wap-jspincludes/wap_cam_header.jsp" %> 

<div class="scrollmenu">
  <li class="activeTab"><a href="#addfund">Add Fund Request</a></li>
   <li ><a href="#changePasswordDiv">Change Password</a></li>
  <li><a href="#viewfund">View Fund History</a></li>
  <li ><a href="#transaction">Transaction</a></li>
  <li ><a href="#rechargemargin">Recharge Margin</a></li>
</div>

<div class="tab-content">
  <div class="LoginForm" id="addfund" style="    padding: 20px 10px 10px 10px;">

        <ul class="tab-group Logintab" >
          <li class="active"><a href="#addfundForm">Add Fund Request</a></li>
          <li><a href="#bankDetailForm">Account Detail</a></li>
        </ul>
       
      <div class="tab-content-sub">
        
        <div id="addfundForm">
            <div class="BalanceDiv">BALANCE : RS. ${user.walletBalance}</div>
           	<span class="profileSPAN">Enter Amount (Min - 100)</span>
             <div class="field-wrap">
              <label>Amount<span class="req">*</span></label>
              <input onkeypress="return isNumberKey(event)" type="text"  id="addRequestamount" name="addRequestamount" value=""  maxlength="7" onkeypress="return spaceNotAllowed(event)" required autocomplete="off"/>
            </div>
            <div class="field-wrap">
                <span class="profileSPAN">Select Payment Method</span>
                <select name="campmethod" id="campmethod" class="form-control">
                      <option value="BY SAME BANK FUND TRANSFER"> BY SAME BANK FUND TRANSFER
                        </option>
                        <option value="BY NEFT / RTGS"> BY NEFT / RTGS
                        </option>
                        <option value="BY SAME BANK FUND TRANSFER"> BY ATM TRANSFER
                        </option>
                        <option value="BY CHEQUE"> BY CHEQUE
                        </option>
                        <option value="BY IMPS"> BY IMPS
                        </option>
                </select>
            </div>
            <div class="field-wrap">
                <span class="profileSPAN">ChargenMarch Bank Name</span>
                <select name="campaybankaccount" class="form-control" id="campaybankaccount">
                    <option value="ICICI Bank">ICICI Bank</option>
                </select>
           </div>
           <div class="field-wrap">
            <label>Bank Reference Id<span class="req">*</span></label>
            <input type="text"  id="camBankrefid" name="camBankrefid" value="" onkeypress="return spaceNotAllowed(event)" required autocomplete="off"/>

             <span class="profileSPAN" style="margin-top:10px;">Please use Bank Reference Number, Other wise
                                                    Payment will be reject, do not use any spacial charactor
                                                    or duplicate</span>
          </div>
           <div class="field-wrap">
            <label>Remark<span class="req">*</span></label>
            <textarea  id="camremark" name="camremark" value="" required autocomplete="off"> </textarea>
           </div> 

            <button class="button button-block" onclick="addFundsRequest()">Proceed to Pay</button>
        </div>

        <div id="bankDetailForm" style="display:none">
          <div style="border: 1px solid #ccc;color: #fff;padding: 3px;">
                            <div class="col-md-6 md-margin-bottom-50">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="fa fa-edit"></i>ICICI BANK LTD
                                            </h3>
                                        </div>
                                        <table class="table table-striped">
                                            <tbody>
                                            <tr>
                                                <td>Account Name</td>
                                                <td style="text-align: right;">LifeSoft Technology</td>
                                            </tr>
                                            <tr>
                                                <td>Account Number</td>
                                                <td style="text-align: right;">675005500091</td>
                                            </tr>
                                            <tr>
                                                <td>IFSC</td>
                                                <td style="text-align: right;">ICIC0006750</td>
                                            </tr>
                                            <tr>
                                                <td>Branch</td>
                                                <td style="text-align: right;">Jaipur, C-Scheme Branch</td>
                                            </tr>
                                            </tbody>
                                        </table>
                            </div>
          </div>
        </div>

    </div> 

   </div>

    <div id="changePasswordDiv" style="display:none;">
             <ul class="tab-group" style="margin: 10px 0px 10px 20px">
              <li class="active"><a>Change Password</a></li>
            </ul>
            <div style="padding:10px;">
            <div class="field-wrap">
              <label>Old Password<span class="req">*</span></label>
               <input type="password"  id="oldPassword" name="oldPassword" value="" required autocomplete="off"/>
            </div>
              <div class="field-wrap">
              <label>New Password<span class="req">*</span></label>
               <input type="password" id="newPassword" name="newPassword" value="" required autocomplete="off"/>
            </div>
              <div class="field-wrap">
              <label>Confirm Password<span class="req">*</span></label>
               <input type="password" id="confirmPassword" name="confirmPassword" value="" required autocomplete="off"/>
            </div>
             <button class="button button-block" onclick="return updatePassword('oldPassword','newPassword','confirmPassword')">Proceed to Pay</button>
           </div>
    </div>               

   <div id="viewfund" style="display:none;">
      <ul class="tab-group" style="margin: 10px 0px 10px 20px">
        <li class="active"><a>Fund History</a></li>
      </ul>
         
          <div id="transactionHistory" style="overflow:auto;height:400px;padding: 5px;">
            <c:forEach var="fundsRequest" items="${addFundsRequestList}">
              <table width="100%" style="background: white;margin-bottom: 10px;font-size: 12px;">
                <tr>
                  <td style="width: 70%;"><span> Payment Method :</span> <br><span>${fundsRequest.paymentMethod}</span>
                    <br>
                  <span style="margin-top: 5px;display: block;"> Ref Id :${fundsRequest.bankReferenceId}</span>
                   <span style="margin-top: 5px;display: block;"> <joda:format value="${fundsRequest.addDate}" pattern="MMM dd yyyy"/></span>
                  </td>

                  <td style="width: 27%;text-align: right;">
                    <span> ${fundsRequest.amount} </span>
                     <br>
                     <span style="margin-top: 5px;display: block;">${fundsRequest.status}</span>
                  </td>
                </tr>
              </table>
             <br>
            </c:forEach>
          </div>
    </div>

   <div id="transaction" style="display:none;">
      <ul class="tab-group" style="margin: 10px 0px 10px 20px">
        <li class="active"><a>Transaction History</a></li>
      </ul>
         
          <div id="transactionHistory" style="overflow:auto;height:400px;padding: 5px;">
            <c:forEach var="transaction" items="${transactionList}">
              <table width="100%" style="background: white;margin-bottom: 10px;font-size: 12px;">
                <tr>

                  <td style="width: 70%;">
                  <c:if test="${transaction.serviceType!='Wallet'}">
                  <span>${transaction.number} (${transaction.operatorName})</span>
                   </c:if>
                    <c:if test="${transaction.serviceType=='Wallet'}">
                       ${transaction.orderId}(OrderId)
                  </c:if> 
                    <br>
                   <span style="margin-top: 5px;display: block;"> <joda:format value="${transaction.rechargeDate}" pattern="MMM dd yyyy"/></span>
                  </td>

                  <td style="width: 27%;text-align: right;">
                    <span>Rs. ${transaction.amount} (${transaction.serviceType})</span>
                     <br>
                       <c:if test="${transaction.isSuccesfulRecharge==1}">
                   <span style="margin-top: 5px;display: block;">  <img src="${applicationScope['baseUrl']}/resources/images/tick.png" class="successTick" /></span>
                  </c:if>
                  <c:if test="${transaction.isSuccesfulRecharge!=1}">
                    <span style="margin-top: 5px;display: block;"> <img src="${applicationScope['baseUrl']}/resources/images/test-fail-icon.png" /></span>
                  </c:if>
                  </td>
                </tr>
              </table>
             <br>
            </c:forEach>
          </div>
    </div>
    <div id="rechargemargin" style="display:none;">
      <ul class="tab-group" style="margin: 10px 0px 10px 20px">
        <li class="active"><a>Recharge Margin Log</a></li>
      </ul>
         
          <div id="transactionHistory" style="overflow:auto;height:400px;padding: 5px;">
              <table width="100%" style="background: rgba(247, 72, 18, 0.87);margin-bottom: 10px;font-size: 12px;font-weight: 600;
    color: #fff;">
              <tr>
                  <td style="width:30%"><span style="margin-top: 5px;display: block;"> Margin Date </span> </td>
                   <td style="width:35%"><span style="margin-top: 5px;display: block;"> Recharge Amount </span> </td>
                    <td style="width:30%"> <span style="margin-top: 5px;display: block;"> Margin Amount </span>
                  </td>
                </tr>
                </table>
            <c:forEach var="rechargeMargin" items="${rechargeMarginLogs}">
              <table width="100%" style="background: white;margin-bottom: 10px;font-size: 12px;">
                <tr>
                  <td style="width:30%;text-align: center;"><span style="margin-top: 5px;display: block;"> ${rechargeMargin.margindate}</span> </td>
                   <td style="width:35%;text-align: center;"><span style="margin-top: 5px;display: block;"> ${rechargeMargin.totalRechargeAmount}</span> </td>
                    <td style="width:30%;text-align: center;"> <span style="margin-top: 5px;display: block;"> ${rechargeMargin.marginAmount} </span>
                  </td>
                </tr>
              </table>
             <br>
            </c:forEach>
          </div>
    </div>



 </div>  

   <script type="text/javascript">
  addPageScript(function() {
      loaderEnd();
    });
</script>
 </body>
</html></compress:html>          