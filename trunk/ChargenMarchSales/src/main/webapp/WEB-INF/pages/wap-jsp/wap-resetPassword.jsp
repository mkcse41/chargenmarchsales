<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ChargenMarch - Recharge Mobile, DTH and DataCard, Fast & Free Recharge, Rewards | ChargenMarch.com</title>
<meta name="keywords" content="Online recharge, Mobile recharge, Prepaid Recharge, Online mobile recharge, Datacard Recharge"/>
<meta name="Description" content="ChargenMarch.com - Simple way to Recharge Prepaid Mobile, DataCard, DTH and keep on Marching forward. Get rewards with every Online Recharge on ChargenMarch. Store money in Wallet on ChargenMarch"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="images/png" rel="shortcut icon" />
<script type="text/javascript">var basepath = "${applicationScope['baseUrl']}";</script>
<link rel="stylesheet" type="text/css" href="${applicationScope['baseUrl']}/resources/wap-css/wap-global.css">
<link rel="stylesheet" type="text/css" href="${applicationScope['baseUrl']}/resources/wap-css/materialize.min.css"/>

</head>
<body style="background: #EFEFEF;">
 <div class="cam-header" style="background:rgba(49, 57, 71, 1) none repeat scroll 0 0;top: 0;">
     <h1 id="headerLogo" class="headerLogo" style="color: #FFF;"><span onclick="findTarget('${applicationScope['baseUrl']}')" style="margin-left: 20px;">ChargeNmarch</span>
     </h1>
  </div> 
<div id="login-page" class="row MainLoginDiv" style="margin-top: 20%; margin-left: 2.5%; width: 95%;">
    <div class="col s12 z-depth-6 card-panel">
      
      <form class="reset-form" action="${applicationScope['baseUrl']}/user/updatedPassword" method="post" id="reset-form">
         <input type="hidden" name="email" value="${user.emailId}">
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="password" name="password" value="" type="password" maxlength="14">
            <label for="password">Password</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="confirmpass" name="confirmpass" value="" type="password" maxlength="14">
            <label for="confirmpass">Conirm Password</label>
          </div>
        </div>
         
         <div class="row">
        <div id="errorMessageloginReset" class="errorText"></div>   
          <div class="input-field col s12" onclick="return checkPasswords('password','confirmpass');">
             <a  class="btn waves-effect waves-light col s12" style="margin-bottom: 10px;">UPDATE</a>
          </div>
        </div>
       </form>
     </div>
  </div>   
   <script src="${applicationScope['baseUrl']}/resources/wap-javascript/jquery-1.11.3.min.js"  defer></script>
  <script src="${applicationScope['baseUrl']}/resources/wap-javascript/wap-global.js" defer></script> 
    <!--materialize js-->
  <script type="text/javascript">
   function checkPasswords(passId, confPassId){
    var passvalue = $("#"+passId).val();
    var confpassvalue = $("#"+confPassId).val();
    if(passvalue!=undefined && confpassvalue!="" && confpassvalue!="" && passvalue!="" && passvalue == confpassvalue){
      document.getElementById("reset-form").submit();
    } else{
      $("#errorMessageloginReset").html("Passwords Do Not Match");
      return false;
   }
  }
   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-70793562-1', 'auto');
  ga('send', 'pageview');
  </script>
  </body>
</html>