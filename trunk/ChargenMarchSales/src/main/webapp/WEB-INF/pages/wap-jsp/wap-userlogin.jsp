<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<compress:html removeComments="true" removeIntertagSpaces="true"><html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ChargenMarch - Recharge Mobile, DTH and DataCard, Fast & Free Recharge, Rewards | ChargenMarch.com</title>
<meta name="keywords" content="Online recharge, Mobile recharge, Prepaid Recharge, Online mobile recharge, Datacard Recharge"/>
<meta name="Description" content="ChargenMarch.com - Simple way to Recharge Prepaid Mobile, DataCard, DTH and keep on Marching forward. Get rewards with every Online Recharge on ChargenMarch. Store money in Wallet on ChargenMarch"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="images/png" rel="shortcut icon" />
<%@include file="wap-jspincludes/css-javascript-include-other.jsp" %>
<style type="text/css">
html,body {
    height: 100%;
    width: 100%;
}
html {
    display: table;
    margin: auto;
}
body {
    display: table-cell;
    vertical-align: middle;
}
.MainLoginDiv{width: 95%;margin-left: 2.5%;margin-top: 16px;}
.margin {margin: 0 !important;}
.DispNone{display: none;}
.ErrorMessage{margin-left: 54px;color: red;}
.Hidden{display: none;}
#gSignInWrapper{text-align: center;margin-bottom: 10px;margin-top: 10px;}
#customBtn {text-align:left;display: block;color: #FFF;width: 160px;height: 50px;font-weight: bold;font-size: 12px;margin-left: 26%;}
#customBtn:hover {cursor: pointer;}
#customBtn span.label {font-weight: bold;}
#customBtn span.buttonText {display: inline-block;vertical-align: middle;font-size: 12px;color: #fff;width: 60%;margin-left: -4px;}
.Hand{cursor: pointer; }
.btn-google {background: #dd4d3b none repeat scroll 0 0;}
#demo3_tip{display: block;
    padding: 16px;
    opacity: 1;
    visibility: visible;
    transition: all 200ms;
    overflow: visible;
    z-index: 9999999999;    line-height: 16px;
    border-width: 1px;
    color: #333;
    border-color: #bbb;
    padding: 8px;
    font-size: 12px;
    font-family: Verdana,Arial;
    border-radius: 6px;
    background-color: #f6f6f6;box-shadow: 0 1px 8px rgba(0,0,0,.5);
}
#mcttCloseButton {
    cursor: pointer;
    top: 16px;border-radius: 4px;
    width: 18px;
    height: 20px;
    margin-right: 2px;
    margin-top: 2px;
    background: #000;float: right;
}
#mcttCloseButton::after {
    content: "+";
    display: block;
    transform: rotate(45deg);
}
#mcttCloseButton:after {
    font: normal 38px/18px arial,sans-serif;
    color: #fff;
    top: 0;
    left: -2px;
}
</style>  
  <script src="https://apis.google.com/js/api:client.js" defer></script>
</head>
<body style="background: #EFEFEF;">
  <div id="mask"></div>
<div id="loader-wrapper">
            <div class="logo"><a href="javascript:void(0)"><span>Charge</span>&March</a></div>
            <div id="loader">
            </div>
</div>
 <div style="text-align: center;padding: 10px;margin-top: 44px;">  
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script src="${applicationScope['baseUrl']}/resources/wap-javascript/google_signIn.js" defer></script>
<!-- WAP Footer -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-6260839974232701"
     data-ad-slot="8357053070"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div> 
<div style="min-height: 400px;">
 <div class="cam-header" style="background:rgba(49, 57, 71, 1) none repeat scroll 0 0;top: 0;">
     <h1 id="headerLogo" class="headerLogo" style="color: #FFF;"><span onclick="redirectNewUrl('${applicationScope['baseUrl']}')" style="margin-left: 20px;">ChargeNmarch</span>
     </h1>
  </div>   
  <div id="login-page" class="row MainLoginDiv">
    <div class="col s12 z-depth-6 card-panel">
<div class="UserIconLogin">
    <i class="fa fa-user-secret"></i>
</div>
      <form class="login-form" id="loginFrm">
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input  type="email" id="chargers_emailId" name="chargers_emailId" value="" onkeypress="return spaceNotAllowed(event)">
            <label for="chargers_emailId" data-error="wrong" data-success="right" class="center-align">Email</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="chargers_password" name="chargers_password" value="" type="password">
            <label for="password">Password</label>
          </div>
        </div>
         <div id="errorMessagelogin" class="errorText"></div>
        <div class="row">
          <div class="input-field col s12">
            <a class="btn waves-effect waves-light col s12" onclick="return userLogin('chargers_emailId','chargers_password','errorMessagelogin')">Login</a>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s6 m6 l6">
            <p class="margin medium-small"><a onclick="userLoginRegister('userRegister')">Register Now!</a></p>
          </div>
          <div class="input-field col s6 m6 l6">
              <p class="margin right-align medium-small"><a onclick="userLoginRegister('recoverPassword')">Forgot password?</a></p>
          </div>          
        </div>
                <span class="TextCenter">OR</span>
        <div id="gSignInWrapper">
          <div id="customBtn" class="btn-google">
            <i class="sprite ic_google"></i>
            <span class="buttonText">Google</span>
          </div>
        </div>
      </form>
      <div id="MobileVerification" class="DispNone">
        <div class="row margin">
          <div class="input-field col s12">
                  <input id="login_mobile" value="" maxlength="10" name="login_mobile" onkeypress="return isNumberKey(event)" type="text" >
                  <label for="login_mobile">Mobile No</label>
                  <div id="errorMessageLoginmobile" class="errorText"></div>   
                   <a onclick="return submitUserMobile('login_mobile','errorMessageLoginmobile');" class="btn waves-effect waves-light col s12" style="margin-bottom: 10px;">Proceed</a> 
           </div>
        </div>
        </div>
        <div class="DispNone" id="OTPVerificationLogin">
          <div class="row margin">
          <div class="input-field col s12">
           <!--  <i class="mdi-action-lock-outline prefix"></i> -->
            <input id="loginotp" value="" maxlength="6" name="loginotp" onkeypress="return isNumberKey(event)" type="text" >
            <label for="loginotp">OTP</label>
            <div id="errorMessageLoginotp" class="errorText"></div>    
             <a onclick="return submitUserOTP('loginotp','errorMessageLoginotp');" class="btn waves-effect waves-light col s12" style="margin-bottom: 10px;">Proceed</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="recoverPassword" class="row DispNone MainLoginDiv" style="margin-top: 26%;">
    <div class="col s12 z-depth-6 card-panel">
      <form class="login-form">
      <div class="UserIconLogin">
      <i class="fa fa-user-times"></i>
      </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input id="chargers_emailId_recover" name="chargers_emailId_recover" value="" onkeypress="return spaceNotAllowed(event)" type="email">
            <label for="chargers_emailId_recover" data-error="wrong" data-success="right" class="center-align">Email</label>
          </div>
        </div>
        <div class="row">
        <div id="errorMessageloginForgot" class="errorText"></div>   
          <div class="input-field col s12">
            <a onclick="return forgotPassword('chargers_emailId_recover','errorMessageloginForgot');" class="btn waves-effect waves-light col s12">Recover my Password</a>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s6 m6 l6">
            <p class="margin medium-small"><a onclick="userLoginRegister('userRegister')">Register Now!</a></p>
          </div>
          <div class="input-field col s6 m6 l6">
              <p class="margin right-align medium-small"><a onclick="userLoginRegister('login-page')">Login</a></p>
          </div>          
        </div>

      </form>
    </div>
  </div>
  <div id="userRegister" class="row DispNone MainLoginDiv" style="margin-top:40px !important;">
    <div class="col s12 z-depth-6 card-panel">
      <form class="login-form" id="userRegisteFrm">
           <div class="UserIconLogin">
              <i class="mdi-action-account-box"></i>
            </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input id="chargers_reg_name" name="chargers_reg_name" value=""  maxlength="30"  type="text" class="validate" onkeypress="return onlyAlphabets(event,this)">
            <label for="username" class="center-align">Username</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-communication-email prefix"></i>
            <input type="email" id="charger_reg_emailId" name="charger_reg_emailId" value=""  maxlength="30" class="validate" onkeypress="return spaceNotAllowed(event)" >
            <label for="email" class="center-align">Email</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
           <i class="mdi-communication-phone prefix"></i>
            <input id="charger_reg_mobileNo" value="" maxlength="10" name="charger_reg_mobileNo" onkeypress="return isNumberKey(event)" type="text" >
            <label for="charger_reg_mobileNo">Mobile Number</label>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12 tooltip">
           <i class="mdi-communication-vpn-key prefix"></i>
            <input id="charger_reg_referral_code" value="" maxlength="10" name="charger_reg_referral_code" type="text" >
            <label for="charger_reg_referral_code">Referral Code</label>
          </div>
        </div>
        <a href="#" onclick="popToltip('demo3_tip'); return false;">Referral?</a>
        <div id="demo3_tip" style="display:none;">
        <div id="mcttCloseButton" style="display: block;" onclick="closeToltip('demo3_tip');"></div>
            <div class="demo3_tip_inside">
                With CAM referral program, now you can earn without recharging. Just share your referral code with new registrant and earn a cashback of 5% on first three recharges of registrant.
            </div>
        </div>
        <div class="row">          
          <div class="input-field col s12 m12 l12  login-text">
              <input type="checkbox" id="autopass" />
              <label for="autopass">auto generated password</label>
          </div>
        </div>
        <div class="row margin" id="showpassdiv">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="charger_reg_password" value="" maxlength="14" name="charger_reg_password" onkeypress="return spaceNotAllowed(event)" type="password" >
            <label for="password">Password</label>
          </div>
        </div>
        <div id="errorMessagesignin" class="errorText"></div>
        <div class="row">
          <div class="input-field col s12">
            <a onclick="return userRegister('chargers_reg_name','charger_reg_emailId','charger_reg_mobileNo','charger_reg_password','WAP','true','errorMessagesignin')" class="btn waves-effect waves-light col s12">Register Now</a>
          </div>
          <div class="input-field col s12">
            <p class="margin center medium-small sign-up">Already have an account? <a onclick="userLoginRegister('login-page')">Login</a></p>
          </div>
        </div>
      </form>
      <div class="DispNone" id="OTPVerification">
          <div class="row margin">
          <div class="input-field col s12">
           <!--  <i class="mdi-action-lock-outline prefix"></i> -->
            <input id="otp" value="" maxlength="6" name="otp" onkeypress="return isNumberKey(event)" type="text" >
            <label for="otp">OTP</label>
            <div id="errorMessageotp" class="errorText"></div>    
             <a onclick="return submitUserOTP('otp','errorMessageotp');" class="btn waves-effect waves-light col s12" style="margin-bottom: 10px;">Proceed</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  


<!-- Recharge details use after Login Details -->
<c:set var="quickRechargeCheckSession" value="${sessionScope.quickRechargeCheckVal}"/>
<c:set var="rechargeFormObj" value="${sessionScope.rechargeForm}"/>
<input type="hidden" id="number" value="${rechargeFormObj.number}">
<input type="hidden" id="operatorValue" value="${rechargeFormObj.operatorValue}">
<input type="hidden" id="mobileService" value="${rechargeFormObj.mobileRechargeService}">
<input type="hidden" id="amount" value="${rechargeFormObj.amount}">
<input type="hidden" id="service" value="${rechargeFormObj.service}">
<input type="hidden" id="operatorText" value="${rechargeFormObj.operatorText}">
<input type="hidden" id="circleValue" value="${rechargeFormObj.circleValue}">
<input type="hidden" id="circleText" value="${rechargeFormObj.circleName}">
<input type="hidden" id="quickRechargeCheckId" value="${quickRechargeCheckSession}">
<input type="hidden" id="isMobilePlan" value="false">
 <!-- jQuery Library -->
<%@include file="wap-jspincludes/wap-cam-footer.jsp" %>

  </div>
  <script type="text/javascript">
  addPageScript(function() {
      loaderEnd();
    });
 function popToltip(id){
	 $("#"+id).show();
 }
 
 function closeToltip(id){
	 $("#"+id).hide();
 }
</script>
</body>
</html></compress:html>