<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<compress:html removeComments="true" removeIntertagSpaces="true"><html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<c:if test="${paymentStatus!='success' && paymentStatus!='paymentsuccess'}">
	     <title>Payment Failed | ChargenMarch - Recharge Mobile, DTH, DataCard and Landline, Fast Recharge, Rewards</title>
	      <meta name="Description" content="ChargenMarch.com - Your Payment Of Rs. ${rechargeDTO.amount}/- failed."/>
	</c:if>
	<c:if test="${paymentStatus=='success'}">
	     <title>Recharge Failed | ChargenMarch - Recharge Mobile, DTH, DataCard and Landline, Fast Recharge, Rewards</title>
	      <meta name="Description" content="ChargenMarch.com - Your Payment Of Rs. ${rechargeDTO.amount}/- failed."/>
	</c:if>
		<c:if test="${paymentStatus=='paymentsuccess'}">
	     <title>Recharge Failed | ChargenMarch - Recharge Mobile, DTH, DataCard and Landline, Fast Recharge, Rewards</title>
	      <meta name="Description" content="ChargenMarch.com - Your Payment Of Rs. ${rechargeDTO.amount}/- failed."/>
	</c:if>
	<meta name="keywords" content="Online recharge, Successful Recharge, Online mobile recharge, Rewards, Coupons"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
	<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="images/png" rel="shortcut icon" />
	<%@include file="wap-jspincludes/css-javascript-include.jsp" %>
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-6260839974232701",
    enable_page_level_ads: true
  });
</script>
</head>
<body>
	<div class="cam-page">
		<div class="cam-main">
			<!-- Header Start -->
			<%@include file="wap-jspincludes/wap_cam_header.jsp" %>
			<c:if test="${paymentStatus!='success' && paymentStatus!='paymentsuccess'}">
				<div class="topbackground-failure">
					<div class="imageBack" style="height:260px;">
						<div class="topbackground mobile-bg active-bg" id="mobilepage">
						<div class="title_circle">
						   <span class="icoprofile-fill reviewUserIcon"></span>
					     </div>
					     <div class="errormsg"><span>Oops !! Your Payment order ${rechargeDTO.orderId} Of Rs. ${rechargeDTO.amount}/- failed.</span></div>
						
						

					</div>
				</div> 
			</div>
		</c:if>
		<c:if test="${paymentStatus=='paymentsuccess'}">
				<div class="topbackground-failure">
					<div class="imageBack" style="height:260px;">
						<div class="topbackground mobile-bg active-bg" id="mobilepage">
						<div class="title_circle">
						   <span class="icoprofile-fill reviewUserIcon"></span>
					     </div>
					     <div class="errormsg"><span>Due to some technical issues with the operator<c:if test="${!empty(rechargeDTO.error)}"><span style="font-size:18px;color:black">(${rechargeDTO.error})</span></c:if>, your recharge was not successful. Please try after a while.</span></div>
					</div>
				</div> 
			</div>
		</c:if>
		<c:if test="${paymentStatus=='success'}">
				<div class="topbackground-failure">
					<div class="imageBack" style="height:260px;">
						<div class="topbackground mobile-bg active-bg" id="mobilepage">
						<div class="title_circle">
						   <span class="icoprofile-fill reviewUserIcon"></span>
					     </div>
					     <div class="errormsg"><span>Transaction of Rs. <b>${rechargeDTO.amount}</b> is successful. But Due to some technical issues with the operator<c:if test="${!empty(rechargeDTO.error)}"><span style="font-size:18px;color:black">(${rechargeDTO.error})</span></c:if>, your recharge was not successful. Please try after a while.</span></div>
					</div>
				</div> 
			</div>
		</c:if>
		<div class="cam_charger_box" style="margin: 10px;border: 2px solid #ccc;">
			<div class="mobilepage" id="mobile_section">
				<div class="cam-white-card-wrapper mobile-section">
					<div class="cam-main-card">

	               <div class="TextCenterReview"  style="color:#000;margin-top:16px;">
							<span class="reviewMobileNo">${rechargeDTO.number}</span><br>
							<span class="reviewOperator">${rechargeDTO.operatorName}<c:if test="${!empty(rechargeDTO.circleName)}">-${rechargeDTO.circleName}</c:if>  
								<c:if test="${rechargeDTO.serviceType=='Mobile'}">
								<span class="Capatilaze">-${rechargeDTO.mobileService}</span>
							</c:if>
							</span>
					</div>

						<div class="row" style="margin-top: 35px;font-size: 15px;padding: 10px;color: #008000;">
							<div class="col-md-12"><div class="msg" style="float: right;">In case of unsuccesful recharge,  money deducted from your account will be added to CAMlet.</div></div>
						</div>
						<div class="line">&nbsp;</div>
						
					</div>
				</div>
			</div>
		</div>

	</div>
		<div style="margin-top: 10px;margin-left:19px;"> 
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Recharge Failure Page -->
<ins class="adsbygoogle"
     style="display:inline-block;width:320px;height:50px"
     data-ad-client="ca-pub-6260839974232701"
     data-ad-slot="8298177472"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>

	<div style="margin: 20px 10px 10px 10px;border: 2px solid #ccc;padding:10px;font-family: sans-serif;"><a href="javascript:void()">Need some help?</a> We are more than happy to help you!	Write an email at <a href="mailto:care@chargenmarch.com" title="care@chargenmarch.com">care@chargenmarch.com</a>
<input type="button" value="Click Here to Retry" class="bluBtn" style="box-shadow: 3px 4px 2px #aaa;margin-top:20px;" onclick="redirectNewUrl('${applicationScope['baseUrl']}')">
							</div>
<%@include file="wap-jspincludes/wap-cam-footer.jsp" %>	
</div>
</body>
</html></compress:html>