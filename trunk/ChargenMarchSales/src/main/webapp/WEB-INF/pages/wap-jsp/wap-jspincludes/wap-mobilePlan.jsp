<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
  <div id="mobileRechargePlan"  >
  <c:forEach var="planType" items="${planList}">
      <div id="${planType.key}MobileRechargePlanDiv" style="margin-bottom: 50px;">
            <c:forEach var="plans" items="${planType.value}">
            <div style="width:92%;height:72px;border:solid 2px #EAEAEA;margin:10px" class="Hand" onclick="selectMobilePlan('${plans.amount}','mobileRechargeAmount','plandata')" title="${plans.description}">
              <div style="width:23%;float:left;background:#B9DCFF;color:#219DC5;text-align:center;font-size:18px;height:73px;"><span style="margin-top: 23px;display: block;">Rs. ${plans.amount}</span></div>
              <div style="width:75%;float:left;border-bottom:solid 1px #EAEAEA">
                <table width="100%" align="center" style="width:100%;height:70px;background:#fff;margin:0px">
                  <tR style="background:#fff;border:none">
                    <td colspan="2" align="justify" style="background:#fff;"><b style="color:#000;font-weight:500">
                    <c:if test="${fn:length(plans.description)>50}">${fn:substring(plans.description, 0, 49)}...</c:if>
                    <c:if test="${fn:length(plans.description)<50}">${plans.description}</c:if>
                    </b></td>
                  </tR>
                  <tr style="background:#fff;border:none">
                    <tD style="background:#fff;color:#999;padding:5px">Validity | ${plans.validity}</tD>
                    <td align="right" style="background:#fff;color:#999;padding:5px"></td>
                  </tr>
                </table>
              </div>
            </div>
            </c:forEach>
        </div>
    </c:forEach>
</div>



  
