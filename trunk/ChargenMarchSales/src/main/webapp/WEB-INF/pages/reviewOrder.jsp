<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<compress:html removeComments="true" removeIntertagSpaces="true"><html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Review Order - Apply coupons, Get discounts and Cashback | ChargenMarch.com</title>
	<meta name="keywords" content="Online mobile recharge, Review Recharge, Prepaid Recharge, Easy Rewards, Free Recharge"/>
	<meta name="Description" content="ChargenMarch.com - Apply coupons to get exciting cashbacks and discounts. Get rewards with every payment on ChargenMarch"/>
	<meta name="fragment" content="!">
	<meta name="google-site-verification" content="-o7OyH8W7bswsEjg9P2kHGskRYeaq56ZmWh7PBudP_A" />
	<meta property="og:title" content="ChargenMarch - Recharge Mobile, DTH and DataCard, Fast & Free Recharge, Rewards | ChargenMarch.com" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="https://chargenmarch.com" />
	<meta property="og:description" content="ChargenMarch.com - Simple way to Recharge all Prepaid Mobile, DataCard, DTH and keep on Marching forward. Rewards with Recharge. " />

  <%@include file="jsp-includes/css-javascript-include-other.jsp" %>
  <link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="images/png" rel="shortcut icon" />
  <link rel="stylesheet" href="${applicationScope['baseUrl']}/resources/css/reviewOrder.css" type="text/css">
  <link type="text/css" href="${applicationScope['baseUrl']}/resources/css/ihover.min.css" rel="stylesheet" />

</head>
<body >

  <!-- Header Start -->
  <%@include file="jsp-includes/cam_header.jsp" %>
  <!-- Header End -->
  <!--  Login and register form  -->
  <%@include file="jsp-includes/chargers_login.jsp" %>
  <!-- Calculation Div Start -->      
  <section id="secondpageslider">
    <div class="inner" <c:if test="${userbean.walletBalance>0}">style="padding-top:50px"</c:if>>
     <div class="calculation" <c:if test="${userbean.referralBalance>0}">style="height:197px"</c:if>
     <c:if test="${userbean.referralBalance == 0}">style="min-height:120px"</c:if>>
       <div class="mobo">
        <p>${operator} </p> 
        <h4>${mobileNo}</h4> 
        <c:if test="${service != 'LandLineBill' && rechargeDTO.mobileService!='postpaid'}">
        <h6 style="padding-top:5px">Rs. ${amount}</h6></c:if>
        <c:if test="${service == 'LandLineBill' || rechargeDTO.mobileService=='postpaid'}">
        <h6 style="padding-top:5px;width: 170px;">Recharge Amount: Rs. ${amount - extraamount}</h6>
        <h6 style="padding-top:5px">Extra Charges: Rs. ${extraamount}</h6>
        </c:if>

      </div>
      <div style="width:50px;float:left"><img src="${applicationScope['baseUrl']}/resources/images/plus.png" style="border-radius:50%;position:absolute;margin-left:-15px;margin-top:33px" /></div>
      <div class="mobo">
       <h4>
       Rewards Points
          <div id="">
              <div class="help-tip">
                    <p>Loyalty Rewards Points:  ChargenMarch rewards the chargers with 1 loyalty point with every successfull transaction worth &#8377;10. These points can later be redeemed to pay in future transactions.</p>
                    </div>
          </div>

       </h4>
       <h6 id="rewardsPointsId">${rewardsPoints.rewardsPoint}</span> <c:if test="${rewardsPoints.rewardsPoint>=10}"><span id="redeemRewardsPointsId"><a onclick="redeemRewardsPoints('${userbean.emailId}','couponSpanId','${amount}','paidAmount','isReedomRewardsPointsId','walletBalanceId','rewardsPointsId','rewardsPointRedeemAmount')">Redeem Points</a></c:if></h6>
     </div>
      <div style="width:50px;float:left"><img src="${applicationScope['baseUrl']}/resources/images/plus.png" style="border-radius:50%;position:absolute;margin-left:-15px;margin-top:33px" /></div>
      <div class="mobo">
       <h4>CAM Wallet</h4>
       <h6 ><span id="walletBalanceId" class="ReviewOrderColor">${userbean.walletBalance}</span>  </h6>
       <c:if test="${userbean.walletBalance>0}"><span class="isUseWalletCss" style="margin-left:-13px"> <input type="checkbox" name="isUseWallet" id="isUseWallet" name="isUseWallet" value="yes" onclick="manageWalletBalance('walletBalanceId','isUseWallet','paidAmount','rewardsPointRedeemAmount','${amount}','${userbean.walletBalance}','isUseReferral','${userbean.referralBalance}')" checked disabled/> <span style="margin-left: -4px; color: black;"> Use Wallet </span></span></c:if>
      <c:if test="${userbean.referralBalance>0}">
       <h4>Referral Balance </h4>
       <h6 ><span id="userReferralBalanceId" class="ReviewOrderColor">${userbean.referralBalance}</span>  </h6>
        <span class="isUseWalletCss" style="margin-left:-13px"> <input type="checkbox" name="isUseReferral" id="isUseReferral" value="yes" onclick="manageUserReferralBalance('walletBalanceId','isUseWallet','paidAmount','rewardsPointRedeemAmount','${amount}','${userbean.walletBalance}','isUseReferral','${userbean.referralBalance}')"/> <span style="margin-left: -4px; color: black;"> Use Referral <div id="">
               </span></span><div class="help-tip">
                    <p>Referral amount worth 3% of the total recharge amount can be redeemed at a time. Also you can not avail any other feature like wallet balance or reward points if you redeem referral amount for recharge.</p>
                    </div>
          </div></c:if>
     </div>



     <div style="width:50px;float:left"><img src="${applicationScope['baseUrl']}/resources/images/equal.jpg" style="border-radius:50%;position:absolute;margin-left:-15px;margin-top:33px" /></div>
     <div class="mobo">
       <h4>Total Order</h4>
       <h6>TO PAY Rs.</h6> <div style="color: #34495e;margin-top: 10px;">Rs.<span id="paidAmount" style="color: black;">${amount}</span></div>
     </div>
     <div class="mobo last">
      <span class="btn plan" style="margin-left:-25px" id="couponSpanId" onclick="createWebGAClickEventTrackingNonInteraction('ProceedOrder_WEB', 'Details', 'on_button');  confirmOrder('couponName','${service}','isReedomRewardsPointsId','isUseWallet')">PROCEED</span>
    </div>
  </div>
   <br />
   <p class="errormsg redeemRewardsErrorMessage" style="margin-left:163px"></p>
   <p class="errormsg userReferralErrorMessage" style="color:red !important;margin-top:-82px"></p>
  <!-- Calculation Div End --> 

  <!-- Coupon Apply Div Start  -->
  <section class="wrapper style2" id="two" >
   <div class="coupon" style="text-align:center;">
     <form name="couponform" id="couponform">
       <div class="modthird">
         <input type="text" class="amount parsley-validated parsley-error" data-required="true" style="color:#fff;width:90%;padding-right:10px"  name="couponName"  id="couponName" placeholder="Enter Coupon Code"  onkeypress="allowAlphaNumeric(event)" />

         <input type="hidden" id="isReedomRewardsPointsId" value='false'>
         <input type="hidden" id="rewardsPointRedeemAmount" value='0'>
       </div>
       <div class="modone">
         <span class="btn plan" onclick="createWebGAClickEventTrackingNonInteraction('ApplyCoupon_WEB', 'Details', 'on_button');  redeemCoupon('couponName','${amount}','${userbean.emailId}','${service}','couponErrorMessage','couponSpanId','paidAmount','walletBalanceId')">Apply</span>
       </div>
     </form>
   </div>
   <br />
   <p class="errormsg couponErrorMessage">Note: Coupon applied here is not valid for payment done through CAM Wallet a.k.a. CAMllet</p>
 </section>
 <!-- Coupon Apply Div End --> 
</div>
</section>

<section id="two" class="wrapper style2" style="padding:2em 0 3em 0">
  <div class="inner">
    <header class="major" style="text-align: center;">
      <h2>Hassle Free Online Payments With ChargenMarch</h2>
      <p>A glimpse of what we offer</p>
    </header>
    <div class="features secondpage"><section class="first"></section>
      <section>
        <div class="ih-item square effect15 left_to_right">
          <a href="javascript:void(0)">
            <span class="icon major fa-envelope-o"></span>
            <h3>Loyalty Rewards Points</h3>
            <p>ChargeNmarch rewards our loyal chargers with rewards points. More you charge with us, better you march with us.</p>
            <div class="info offerContent">
              <h3 class="offerContentTitle">Loyalty Rewards Points</h3>
              <span ></span>
              <p class="hovercontent"> 
                ChargenMarch rewards the chargers with 1 loyalty point with every successfull transaction worth &#8377;10. These points can later be redeemed to pay in future transactions.
              </p>
            </div>
          </a>
        </div>
      </section>
      <section>
        <div class="ih-item square effect15 left_to_right">
          <a href="javascript:void(0)">

            <span class="icon major fa-briefcase"></span>
            <h3>CAM Wallet</h3>
            <p>Load up the money in CAM Wallet a.k.a CAMllet &amp; forget the extra efforts of using cards or netbanking.</p>
            <div class="info offerContent">
              <h3 class="offerContentTitle"> CAM Wallet </h3>
              <span ></span>
              <p class="hovercontent"> 
                ChargeNmarch provides you with option to load money in wallet where it'll be safe and secure with us. This money loaded in wallet can be used to carry out payments in future without using any card or netbanking.
              </p>
            </div>
          </a>
        </div>
      </section>
      <section class="first"></section>
      <section class="first"></section>
      <section>
        <div class="ih-item square effect15 left_to_right">
          <a href="javascript:void(0)">
            <span class="icon major fa-fast-forward"></span>
            <h3 >2-Step Recharge</h3>
            <p>ChargenMarch respects the value of time for our loyal customers.</p>
            <div class="info offerContent ">
              <h3 class="offerContentTitle"> 2-Step Recharge </h3>
              <span ></span>
              <p class="hovercontent"> 
                ChargeNmarch knows how valuable and precious time is for our customers. We respect it by providing recharge in 2 simple and fast steps.
              </p>
            </div>
          </a>
        </div>
      </section>
      <section>
        <div class="ih-item square effect15 left_to_right">
          <a href="javascript:void(0)">

            <span class="icon major fa-cloud"></span>
            <h3 >128-Bit SSL Secured</h3>
            <p>Not just time, ChargenMarch takes care of your money too &amp; fully secures all the transfers with 128-bit SSL encryption.</p>
            <div class="info offerContent">
              <h3 class="offerContentTitle">128-Bit SSL Secured</h3>
              <span ></span>
              <p class="hovercontent"> 
               ChargenMarch takes care of the security of all the details that charger provides like his/her card details. So just sit back &amp; relax. Keep on charging and keep on marching.
              </p>
            </div>
          </a>
        </div>
      </section>
    </div>
  </div>
</section>
<!-- Rewards Points Section End -->
<%@include file="jsp-includes/cam_footer.jsp" %>
<!-- Footer Section End -->
<!-- Menu Bar Code Start -->
<%@include file="jsp-includes/cam_menu_script.jsp" %>
<%@include file="jsp-includes/cam_menu.jsp" %>
<script type="text/javascript">
var isWalletBalance = true;
var camWalletBal = "${userbean.walletBalance}";
$(document).ready(function() {
    manageWalletBalance('walletBalanceId','isUseWallet','paidAmount','rewardsPointRedeemAmount','${amount}','${userbean.walletBalance}','isUseReferral','${userbean.referralBalance}');
    if(undefined != camWalletBal && parseInt(camWalletBal) == 0){
       isWalletBalance = false;
    }
});
</script>
  </body>
  </html></compress:html>