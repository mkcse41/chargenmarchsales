
  <div class="inner HiddenSearchPage">
    <!-- My Form -->
    <div class="airForm">
    <h2>Book Domestic & International Flight Tickets</h2>
   

<div class="responsive-tabs">

  <form action="${applicationScope['baseUrl']}/march/air/search" name="airTicketForm" id="airTicketForm" method="post">
    <input type="hidden" name="serviceType" id="serviceType"/>
    <input type="hidden" name="mode" id="mode"/> 
    <input type="hidden" name="origin" id="origin"/> 
    <input type="hidden" name="destination" id="destination"/>
    <input type="hidden" name="departDate" id="departDate"/>
    <input type="hidden" name="returnDate" id="returnDate"/>
    <input type="hidden" name="adultPax" id="adultPax"/>
    <input type="hidden" name="childPax" id="childPax"/>
    <input type="hidden" name="infantPax" id="infantPax"/>
    <input type="hidden" name="classPreferred" id="classPreferred"/>
    <input type="hidden" name="carrierPreferred" id="carrierPreferred"/>  
  </form>
    <a  onclick="showHideDiv('domestic','international');" class="domestic">Domestic</a> <a onclick="showHideDiv('international','domestic');" class="international">International</a>
    
<!-- Tab Working -->    
  <div class="padding" id="domestic">
    <table style="width:50%"><tr><td width="5%">
    <input type="radio" name="domtrip" id="domtripone" class="radio" checked="true"/></td> 
    <td style="vertical-align:middle;text-align:justify"><span>ONE WAY</span></td>
    <td><input type="radio" class="radio" name="domtrip" id="domtripround" /></td> 
    <td style="vertical-align:middle;text-align:justify"><span>ROUND TRIP</span></td></tr></table>
  
    <table width="100%">
    <tr>
      <td width="45%" style="text-align:justify"><span>From</span></td>
      <td rowspan="2" valign="bottom"><div class="arrow"></div></td>
      <td width="45%" style="text-align:justify"><span>To</span></td>
    </tr>
    <tr>
    <tD style="text-align:justify;" id="domesticDepartureCities" >
           <input list="domesticdeparture" name="domesticdeparture" id="domesticdepartureInput" class="select" placeholder="Type Departure City" style="width:100%" >
          <datalist id="domesticdeparture">
           <c:forEach var="airDomesticCitiesIter" items="${airDomesticCities}">
               <option value="${airDomesticCitiesIter.city},${airDomesticCitiesIter.airportName}- ${airDomesticCitiesIter.country_name}(${airDomesticCitiesIter.airportCode})">
           </c:forEach>
          </datalist>
    </tD>
     <tD style="text-align:justify;" id="domesticdestincationCities">
         <input list="domesticdestination" name="domesticdestination" id="domesticdestinationInput" class="select" placeholder="Type Destination City" style="width:100%">
        <datalist id="domesticdestination">
                 <c:forEach var="airDomesticCitiesIter" items="${airDomesticCities}">
                      <option value="${airDomesticCitiesIter.city},${airDomesticCitiesIter.airportName}- ${airDomesticCitiesIter.country_name}(${airDomesticCitiesIter.airportCode})">
                 </c:forEach>
         </datalist>
    </tD>
    </tr>
    <tr>
    <td>
       <table style="margin-top:20px"><tr>
      <td width="40%" style="text-align:justify"><span>Departure</span></td>
      <td width="10%">&nbsp;</td>
      <td width="40%" style="text-align:justify"><span>Return</span></td>
    </tr>
    <tR><td style="text-align:justify;width:45%"><input type="text" class="datepicker" id="domDepartureDate" placeholder="mm/dd/yy" /></td>
    <td width="10%">&nbsp;</td>
    <td style="text-align:justify;width:45%"><input type="text" class="datepicker" id="domReturnDate" placeholder="mm/dd/yy" /></td></tR></table>
    </td>
    <td>&nbsp;</td>
    <td>
       <table><tr>
      <td style="text-align:justify"><span style="font-size:10px">ADULT: (12+ YRS)</span></td>
      <td style="text-align:justify"><span style="font-size:10px">CHILD: (2-11 YRS)</span></td>
      <td style="text-align:justify"><span style="font-size:10px">INFANT: (0-2 YRS)</span></td>
    </tr>
    <tR>
    <td style="text-align:justify;">
    <input list="domadult" name="domadult" id="domadultInput" class="select1" placeholder="1">
  <datalist id="domadult">
    <option value="1" selected>
    <option value="2">
    <option value="3">
    <option value="4">
    <option value="5">
    <option value="6">
  </datalist>
    </td>
    <td style="text-align:justify;">
    <input list="domchild" name="domchild" id="domchildInput" class="select1" placeholder="0">
  <datalist id="domchild">
    <option value="0" selected>
    <option value="1">
    <option value="2">
    <option value="3">
    <option value="4">
    <option value="5">
    <option value="6">
  </datalist>
    </td>
    <td>
    <input list="dominfant" name="dominfant" id="dominfantInput" class="select1" placeholder="0">
  <datalist id="dominfant">
    <option value="0" selected>
    <option value="1">
    <option value="2">
    <option value="3">
    <option value="4">
    <option value="5">
    <option value="6">
  </datalist>
    </td>
    </tR></table>
  </td>
  </tr>


  <tr>
    <td>
    <table>
      <tr>
     <td style="text-align:justify;">
   <select name="domclassPreferredInput" id="domclassPreferredInput" placeholder="Preferred Class">
    <option value="E" selected> Economy </option>
    <option value="B">Business </option>
    <option value="B">First Class </option>
  </select>
    </td>
     <td style="text-align:justify;">
    <select name="domcarrierPreferredInput" id="domcarrierPreferredInput" placeholder="Preferred Airline">
    <c:forEach var="serviceProviders" items="${serviceProvider}">
     <option value="${serviceProviders.carrier_code}"> ${serviceProviders.carrier_name} </option>
    </c:forEach>
  </select>
    </td>
    </tr>
  </table>


    </td>
    </tr>
    </table>
  <div class="btn-primary-red" onclick="return checkAllInformation();">Search Flights</div>
  </div>
    <!-- Tab Working -->

  <!-- Tab Working -->    
  <div class="padding DispNone" id="international">
    <table style="width:50%"><tr><td width="5%"><input type="radio" name="intetrip" id="intetripone" class="radio" checked="true" /></td> 
    <td style="vertical-align:middle;text-align:justify"><span>ONE WAY</span></td>
    <td><input type="radio" class="radio" name="intetrip" id="intetripround" /></td> 
    <td style="vertical-align:middle;text-align:justify"><span>ROUND TRIP</span></td></tr></table>
  
    <table width="100%">
    <tr>
      <td width="45%" style="text-align:justify"><span>From</span></td>
      <td rowspan="2" valign="bottom"><div class="arrow"></div></td>
      <td width="45%" style="text-align:justify"><span>To</span></td>
    </tr>
    <tr>
       <tD style="text-align:justify;" id="internationalDepartureCities" >
           <input list="internationaldeparture" name="internationaldeparture" id="internationaldepartureInput" class="select" placeholder="Type Departure City" style="width:100%">
          <datalist id="internationaldeparture">
           <c:forEach var="airinternationalCitiesIter" items="${airALLCities}">
               <option value="${airinternationalCitiesIter.city},${airinternationalCitiesIter.airportName}- ${airinternationalCitiesIter.country_name}(${airinternationalCitiesIter.airportCode})">
           </c:forEach>
          </datalist>
    </tD>
     <tD style="text-align:justify;" id="internationaldestincationCities">
         <input list="internationaldestination" name="internationaldestination" id="internationaldestinationInput" class="select" placeholder="Type Destination City" style="width:100%">
        <datalist id="internationaldestination">
                 <c:forEach var="airinternationalCitiesIter" items="${airALLCities}">
                     <option value="${airinternationalCitiesIter.city},${airinternationalCitiesIter.airportName}- ${airinternationalCitiesIter.country_name}(${airinternationalCitiesIter.airportCode})">
                 </c:forEach>
  </datalist>
    </tD>
    </tr>
    <tr>
    <td>
       <table style="margin-top:20px"><tr>
      <td width="40%" style="text-align:justify"><span>Departure</span></td>
      <td width="10%">&nbsp;</td>
      <td width="40%" style="text-align:justify"><span>Return</span></td>
    </tr>
    <tR><td style="text-align:justify;width:45%"><input type="text" class="datepicker"  id="interDepartureDate" placeholder="mm/dd/yy" /></td>
    <td width="10%">&nbsp;</td>
    <td style="text-align:justify;width:45%"><input type="text" class="datepicker" id="interReturnDate" placeholder="mm/dd/yy" /></td></tR></table>
    </td>
    <td>&nbsp;</td>
    <td>
       <table><tr>
      <td style="text-align:justify"><span style="font-size:10px">ADULT: (12+ YRS)</span></td>
      <td style="text-align:justify"><span style="font-size:10px">CHILD: (2-11 YRS)</span></td>
      <td style="text-align:justify"><span style="font-size:10px">INFANT: (0-2 YRS)</span></td>
    </tr>
    <tR>
    <td style="text-align:justify;">
    <input list="interadult" name="interadult" id="interadultInput" class="select1" placeholder="1">
  <datalist id="interadult">
    <option value="1" selected>
    <option value="2">
    <option value="3">
    <option value="4">
    <option value="5">
    <option value="6">
  </datalist>
    </td>
    <td style="text-align:justify;">
    <input list="interchild" name="interchild" id="interchildInput" class="select1" placeholder="0">
  <datalist id="interchild">
    <option value="0" selected>
    <option value="1">
    <option value="2">
    <option value="3">
    <option value="4">
    <option value="5">
    <option value="6">
  </datalist>
    </td>
    <td>
    <input list="interinfant" name="interinfant" id="interinfantInput" class="select1" placeholder="0">
  <datalist id="interinfant">
    <option value="0" selected>
    <option value="1">
    <option value="2">
    <option value="3">
    <option value="4">
    <option value="5">
    <option value="6">
  </datalist>
    </td>
    </tR></table>
      </td>
  </tr>


  <tr>
    <td>
    <table>
      <tr>
     <td style="text-align:justify;">
    <select name="interclassPreferred" id="interclassPreferredInput" placeholder="Preferred Class">
  <!-- <datalist id="interclassPreferred"> -->
    <option value="E" selected> Economy </option>
    <option value="B">Business </option>
    <option value="B">First Class </option>
  </select>
  <!-- </datalist> -->
    </td>

     <td style="text-align:justify;">
    <select name="intercarrierPreferredInput" id="intercarrierPreferredInput" placeholder="Preferred Airline">
    <c:forEach var="serviceProvider" items="${serviceProvider}">
     <option value="${serviceProvider.carrier_code}"> ${serviceProvider.carrier_name} </option>
    </c:forEach>
  </select>
    </td>
    </tr>
  </table>

    </td>
    </tr>
    </table>
 <div class="btn-primary-red" onclick="return checkAllInformation();">Search Flights</div>
  </div>
    <!-- Tab Working -->   
    </div>
    <!-- My Form -->
  </div>
  </div>