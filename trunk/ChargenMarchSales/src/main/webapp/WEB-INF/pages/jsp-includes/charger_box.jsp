
<!-- Boxes Section Start -->

<section id="homepageslider">

  <div class="inner">
 
  <!-- Middle Box Working Start -->
  <!-- Code View  -->
<div class="mnUnit" id="chargersBox">
  <div class="lft uiViewHome">
    <!-- Tab View  -->
    <div data-ng-include="''" class="ng-scope">

      <div id="mobileicon">
       <div class="title_circle mobileBack">
        <span class="icon fa-mobile" style="font-size: 50px;margin-top: -10px;"></span>
        </div>
      </div>

      <div id="dthicon" class="Hidden">
       <div class="title_circle dthBack">
       <span class="icodth"></span>
       </div>
      </div>
      
      <div id="datacardicon" class="Hidden">
       <div class="title_circle dataBack">
        <span class="icodatacard"></span>
        </div>
      </div>

        <div id="landlineicon" class="Hidden">
       <div class="title_circle landlineBack">
        <span class="icolandline"></span>
        </div>
      </div>


      <section class="mnUnitTb ng-scope"> <font> 
      <a class="active showArrow mobile" onclick="showThemeDiv('mobileicon','dthicon','datacardicon','landlineicon');" ><span  style="font-weight:800" class="colorbordermobile">Mobile</span> </a> 
      <a  onclick="showThemeDiv('dthicon','mobileicon','datacardicon','landlineicon');" class="showArrow dth"> <span  style="font-weight:800" class="colorborderdth">DTH</span> </a> 
      <a class="showArrow data"  onclick="showThemeDiv('datacardicon','mobileicon','dthicon','landlineicon');"> <span  style="font-weight:800" class="colorborderdatacard">Data card</span> </a> 
       <a class="showArrow landline"  onclick="showThemeDiv('landlineicon','mobileicon','dthicon','datacardicon');"> <span  style="font-weight:800" class="colorborderlandline">LandLine</span> </a> 
      </font>
      </section>
    </div>
    <!-- Tab View  -->
    <!-- Content View  -->
    <div data-ui-view="" class="ng-scope">
      <!-- Mobile View  -->
      <div class="mobileView rechargeView ng-scope">
        <div class="frmnwrp">
          <div class="frm jsCrawl blackThm ng-isolate-scope">
            <form name="mobileForm" class="mobileForm ng-pristine ng-valid ng-valid-maxlength" data-ng-submit="submitForm()">
              <div class="frmControl noline">
                <p class="radioFamily" tabindex="1">
                  <label for="mobilePrepaidType">
                  <input type="radio" checked="checked" name="mobileRechargeService" id="mobilePrepaidType" value="prepaid" data-ng-model="formData.connectionType" data-ng-change="changeType(formData.connectionType)" class="ng-pristine ng-untouched ng-valid" onclick="selectMobileServiceType('mobileRechargeService')">
                  <i class="cstmsd_radio"></i>Prepaid</label>
                  <label for="mobilePostpaidType">
                  <input type="radio" tabindex="2" name="mobileRechargeService" id="mobilePostpaidType" value="postpaid" data-ng-model="formData.connectionType" data-ng-change="changeType(formData.connectionType)" class="ng-pristine ng-untouched ng-valid" onclick="selectMobileServiceType('mobileRechargeService')">
                  <i class="cstmsd_radio"></i>Postpaid</label>
                </p>
                <span class="error"></span></div>
             
              <div class="formdiv">
                <p><i class="icon fa-phone"></i>
                  <input type="tel" maxlength="10" placeholder="Enter Mobile Number" style="heiht:70px;" id="rechargeMobileNumber" name="rechargeMobileNumber" onkeypress="return isNumberKey(event);" onblur="getOperatorDetails('rechargeMobileNumber','Mobile','mobilePrepaidOperator','mobilePostpaidOperator','mobileNumberErrorMsg','mobileRechargeService','mobileState');">
                </p>
                <span class="error jsError mobileNumberErrorMsg" style="visibility:visible"></span>
              </div>
             
              <div class="savedconn ng-hide" data-ng-show="options.showConnections"> </div>
              <div class="frmControl jsValidate" style="height: 53px;">
                <p>
                <table id="mobilePrepaidOperator" class="DispNone" style="height:100%;">
                  <tr>
                    <td><i class="icon fa-signal"></i></td>
                    <td>                   
                        <div class="optr mobilePrepaidOperator">
                        </div>
                        <div class="spacer">
                        </div>
                      </td>
                  </tr>
                </table>
                 <table id="mobilePostpaidOperator" class="DispNone" style="height:100%;">
                  <tr>
                    <td><i class="icon fa-signal"></i></td>
                    <td>                   
                        <div class="optr mobilepPostpaidOperator">
                        </div>
                        <div class="spacer">
                        </div>
                      </td>
                  </tr>
                </table>
                </p>
                <span class="error jsError mobileOperatorErrorMessage" style="visibility:visible;margin-top:-50px;position:absolute"></span></div>
              <div class="frmControl jsValidate" style="height: 53px;">
                <p>
                <table id="mobileState" style="height:100%;">
                  <tr>
                    <td><i class="icon fa-location-arrow"></i></td>
                    <td>
                        <div class="state">
                        </div>
                        <div class="spacer">
                        </div>
                      </td>
                  </tr>
                </table>
                </p>
                <span class="error jsError mobileStateMessage" style="visibility:visible;margin-top:-50px;position:absolute"></span></div>
              <div class="frmControl jsValidate ng-scope" data-ng-if="!formData.viewBill" style="height: 70px; ">
                <p class="brwsbtn"><i class="icon fa-inr"></i> <strong style="display:none">Rs.</strong>
                  <input type="text" tabindex="7" name="mobileRechargeAmount" id="mobileRechargeAmount" maxlength="4" class="jsField ng-pristine ng-untouched ng-valid ng-isolate-scope ng-valid-maxlength" placeholder="Enter Amount" data-rule="validateRechargeAmount" data-ng-blur="$root.validateField($event, true)" onkeypress="return isNumberKey(event)" >
                 <c:if test="${rechargeServiceName=='JOLO'}">
                   <span id="mobilePlanLink"><a href="javascript:void(0)" class="check-plans ng-scope plan" onclick="createWebGAClickEventTrackingNonInteraction('viewplans_WEB', 'Details', 'on_button'); return getMobilePlan('mobilePlanDiv','rechargeMobileNumber','mobilePlanData')">View Plans</a></span>
                </c:if>
                </p>
                <span class="error jsError mobileRechargeAmountErrorMsg" style="visibility:visible;"></span></div>
              <div class="clear20"></div>
              <div class="QuikRechargeText">
              <input type="checkbox" id="quikRechargeMobile" name="quikRechargeMobile" class="quikRechargeCheckBox" style="width: 14px;display: inline-block;">
               <label for="quikRechargeMobile">1-Step Recharge</label>
              </div>
              <div class="frmControl noline" style="height: 63px;">
                <input type="button" tabindex="11" data-ng-bind="formData.viewBill?'View Bill':(formData.connectionType=='postpaid'?'Pay Bill':'Recharge Now')" class="bluBtn ng-binding" onclick=" createWebGAClickEventTrackingNonInteraction('MobileRecharge_WEB', 'Details', 'on_button'); return processedToMobileRechargeCoupon('checkUserLogin','mobileRechargeService','rechargeMobileNumber','mobilePrepaidOperator','mobilePostpaidOperator','mobileRechargeAmount','mobileState')" value="Recharge Now">
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- Mobile View  -->
      <!-- DTH View -->
      <div class="dthView rechargeView ng-scope">
        <div class="frmnwrp">
          <div class="frm jsCrawl blackThm ng-isolate-scope">
            <form name="dthForm" class="dthForm ng-pristine ng-valid ng-valid-maxlength" data-ng-submit="submitForm()">

                <div class="frmControl jsValidate" >
                <p>
                <table style="margin-top:4px" id="dthDataOperator">
                  <tr style="border-bottom: #fff;border-color:white">
                    <td><i class="icon fa-signal"></i></td>
                    <td>                   
                        <div id="dthdata">
                        </div>
                        <div class="spacer">
                        </div>
                      </td>
                  </tr>
                </table>
                </p>
                <span class="error jsError dthOperatorErrorMsg" style="visibility:visible;margin-top:-48px;position:absolute"></span></div>

             <div class="frmControl jsValidate" style="height: 70px;">
                <p><i class="icon fa-hashtag"></i>
                  <input type="tel" class="jsField ng-pristine ng-untouched ng-valid ng-isolate-scope" tabindex="2" placeholder="Enter Your Viewing Card Number" data-rule="validateDthNumber" data-ng-model="formData.connectionNumber" data-ng-focus="showConnections($event)" data-ng-blur="hideConnections($event)" id="dthOpertorNumber" name="dthOpertorNumber" onkeypress="return isNumberKey(event);" maxlength="14"  onblur="validateDTHNumber('dthOpertorNumber','dthNumberMessageError','dthDataOperator');">
                </p>
                <span class="error jsError dthNumberMessageError" style="visibility:visible"></span></div>

          
              
              <div class="frmControl jsValidate ng-scope" style="height: 70px;">
                <p class="brwsbtn"><i class="icon fa-inr"></i>
                  <input type="text" class="jsField ng-pristine ng-untouched ng-valid ng-isolate-scope ng-valid-maxlength" tabindex="3" maxlength="5" placeholder="Enter Amount" data-rule="validateRechargeAmount" data-ng-model="formData.amount" id="dthAmount" onkeypress="return isNumberKey(event)">
                  
                  </p>
                <span class="error jsError dthAmountMessage" style="visibility:visible"></span></div>
              <div class="clear20"></div>
              <div class="QuikRechargeText">
              <input type="checkbox" id="quikRechargeDTH" name="quikRechargeDTH" class="quikRechargeCheckBox" style="width: 14px;display: inline-block;">
               <label for="quikRechargeDTH">1-Step Recharge</label>
              </div>
              <div class="frmControl noline" style="margin-left:-12px">
                <input type="button" tabindex="4"  class="d2hBtn" value="Recharge Now" onclick="createWebGAClickEventTrackingNonInteraction('DTHRecharge_WEB', 'Details', 'on_button'); return processedToDTHRechargeCoupon('checkUserLogin','dthOpertorNumber','dthdata','dthAmount')">
              </div>
                         </form>
          </div>
          </div>
      </div>
      <!-- DTH View -->

      <!-- Data View -->
      <div class="dataView rechargeView ng-scope">
        <div class="frmnwrp">
          <div class="frm jsCrawl blackThm ng-isolate-scope">
            <form name="mobileForm" class="mobileForm ng-pristine ng-valid ng-valid-maxlength" >
             <div class="frmControl noline DispNone" ><p class="radioFamily" tabindex="1"><label for="datacardPrepaidType">
             
             <input type="radio" checked="checked" tabindex="1" id="datacardPrepaidType"  value="prepaid" data-ng-model="formData.connectionType" data-ng-change="changeType(formData.connectionType)" class="ng-pristine ng-untouched ng-valid" name="dataCardRechargeService"><i class="cstmsd_radio"></i>Prepaid</label><label for="datacardPostpaidType"><input type="radio" tabindex="2" id="datacardPostpaidType"  value="postpaid" data-ng-model="formData.connectionType" data-ng-change="changeType(formData.connectionType)" class="ng-untouched ng-valid ng-dirty ng-valid-parse" name="dataCardRechargeService"><i class="cstmsd_radio"></i>Postpaid</label></p><span class="error"></span></div>
              <div style="height: 65px">
                <p><i class="icon fa-hashtag"></i>
                  <input type="tel" maxlength="10" class="jsField ng-pristine ng-untouched ng-valid ng-isolate-scope ng-valid-maxlength" tabindex="3" placeholder="Enter Data Card Number" id="datacardRechargeNumber" name="datacardRechargeNumber" onkeypress="return isNumberKey(event);" onblur="getOperatorDetails('datacardRechargeNumber','DataCard','datacardPrepaidOperator','','DataCardNumberMessage','dataCardRechargeService','dataCardState');">
                </p>
                <span class="error jsError DataCardNumberMessage" style="visibility:visible"></span></div>
              <div class="savedconn ng-hide" data-ng-show="options.showConnections"> </div>
              <div class="frmControl jsValidate" style="height:50px">
                <p>
                <table style="margin-top:4px" id="datacardPrepaidOperator" >
                  <tr  style="border-bottom: #fff;">
                    <td><i class="icodatacard"></i></td>
                    <td> <div class="datacardPrepaidOperator">
                        </div>
                        <div class="spacer">
                        </div></td>
                  </tr>
                </table>
                </p>
                <span class="error jsError datacardPrepaidOperatorError Message" style="visibility:visible;margin-top:-48px;position:absolute"></span></div>
              <div class="frmControl jsValidate" style="height:50px" >
                <p>
                <table style="margin-top:-1px" id="dataCardState">
                  <tr>
                    <td><i class="icon fa-location-arrow"></i></td>
                    <td> <div class="datacardState">
                        </div>
                        <div class="spacer">
                        </div></td>
                  </tr>
                </table>
                </p>
                <span class="error jsError DataCardStateErrorMessage" style="visibility:visible;margin-top:-48px;position:absolute"></span> </div>
              <div class="frmControl jsValidate ng-scope" data-ng-if="!formData.viewBill" style="height:70px">
                <p class="brwsbtn"><i class="icon fa-inr"></i> <strong style="display:none">Rs.</strong>
                  <input type="text" tabindex="7" maxlength="5" class="jsField ng-pristine ng-untouched ng-valid ng-isolate-scope ng-valid-maxlength" placeholder="Enter Amount" data-rule="validateRechargeAmount" onkeypress="return isNumberKey(event)" id="datacardRechargeAmount" name="datacardRechargeAmount">
                 
                
                </p>
                <span class="error jsError datacardRechargeAmountErrorMessage" style="visibility:visible;"></span></div>
              <div class="clear20"></div>
              <div class="QuikRechargeText">
              <input type="checkbox" id="quikRechargeDataCard" name="quikRechargeDataCard" class="quikRechargeCheckBox" style="width: 14px;display: inline-block;">
               <label for="quikRechargeDataCard">1-Step Recharge</label>
              </div>
              <div class="frmControl noline">
                <input type="button" tabindex="11" data-ng-bind="formData.viewBill?'View Bill':(formData.connectionType=='postpaid'?'Pay Bill':'Recharge Now')" class="dataBtn ng-binding" value="Recharge Now" onclick="createWebGAClickEventTrackingNonInteraction('DataCardRecharge_WEB', 'Details', 'on_button'); return processedToDatacardRechargeCoupon('checkUserLogin','dataCardRechargeService','datacardRechargeNumber','datacardPrepaidOperator','','datacardRechargeAmount','dataCardState')"/>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- Data View -->
      <!-- LandLine view -->
       <div class="landlineView rechargeView ng-scope">
        <div class="frmnwrp">
          <div class="frm jsCrawl blackThm ng-isolate-scope">
            <form name="landlineForm" class="landlineForm" >
             
               <div class="frmControl" style="height:50px">
                  <p>
                  <table style="margin-top:4px" id="landlineOperator">
                    <tr  style="border-bottom: #fff;">
                      <td><i class="icon fa-signal"></i></td>
                      <td> <div class="landlineOperator">
                          </div>
                          <div class="spacer">
                          </div></td>
                    </tr>
                  </table>
                  </p>
                  <span class="error jsError landlineOperatorError Message" style="visibility:visible;margin-top:-48px;position:absolute"></span>
                </div>


          <div id="landlinedivshowonoperator" class="Hidden">
              <div style="height: 69px;border-bottom:3px solid #ddd;" id="landlineAccountDiv">
                <p><i class="icon fa-user"></i>
                  <input type="tel" class="jsField ng-pristine ng-untouched ng-valid ng-isolate-scope ng-valid-maxlength" tabindex="3" placeholder="Customer Account Number" id="landlineAccountNumber" name="landlineAccountNumber" onkeypress="return isNumberKey(event);">
                </p>
                <span class="error jsError landlineAccountNumberMessage" style="visibility:visible"></span>
              </div>

              <div style="height: 69px;border-bottom:3px solid #ddd;" id="landlinestd">
                <p><i class="icon fa-hashtag"></i>
                  <input type="tel" class="jsField ng-pristine ng-untouched ng-valid ng-isolate-scope ng-valid-maxlength" tabindex="3" placeholder="STD code" id="landlineNumberstdcode" name="landlineNumberstdcode" maxlength="6" onkeypress="return isNumberKey(event);">
                </p>
                <span class="error jsError landlineNumberstdcodeMessage" style="visibility:visible"></span>
              </div>

               <div style="height: 69px;border-bottom:3px solid #ddd;" id="landlinenumberDiv">
                <p>  <span class="icolandline"></span>
                  <input type="tel" class="jsField ng-pristine ng-untouched ng-valid ng-isolate-scope ng-valid-maxlength" tabindex="3" placeholder="LandLine Number" id="landlineNumber" name="landlineNumber" maxlength="10" onkeypress="return isNumberKey(event);">
                </p>
                <span class="error jsError landlineNumberMessage" style="visibility:visible"></span>
              </div>

              <div class="frmControl jsValidate ng-scope" style="height:70px">
                  <p class="brwsbtn"><i class="icon fa-inr"></i> <strong style="display:none">Rs.</strong>
                    <input type="text" tabindex="7" maxlength="5" class="jsField ng-pristine ng-untouched ng-valid ng-isolate-scope ng-valid-maxlength" placeholder="Enter Amount" onkeypress="return isNumberKey(event)" id="landlineRechargeAmount" name="landlineRechargeAmount"> 
                  </p>
                  <span class="error jsError landlineRechargeAmountErrorMessage" style="visibility:visible;"></span>
              </div>
           </div>   


              <div class="clear20"></div>
              <!-- <div class="QuikRechargeText">
              <input type="checkbox" id="quikRechargeLandLine" name="quikRechargeLandLine" class="quikRechargeCheckBox" style="width: 14px;display: inline-block;">
               <label for="quikRechargeDataCard">One Step Recharge</label>
              </div> -->
              <div class="frmControl noline">
                <input type="button" tabindex="11" class="landlineBtn ng-binding" value="Proceed to Bill Pay" onclick="createWebGAClickEventTrackingNonInteraction('LandlineRecharge_WEB', 'Details', 'on_button');return   processedToLandLineBillingRecharge('checkUserLogin','landlineOperator','landlineAccountNumber','landlineNumberstdcode','landlineNumber','landlineRechargeAmount')"/>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- bill view -->
    </div>
    <!-- Content View  -->
  </div>
</div>
<!-- Code View  -->
  <!-- Middle Box Working End -->
  
  </div>
</section>
<!-- Mobile Plan -->
<div id="mobileplanOverlay" class="DispNone"></div>
 <nav id="mobilePlan">
      <a class="close" onclick="closeMobilePlan();"></a>
      <div>
          <div style="color:black;margin-bottom:20px"> 
                 <span style="font-size: 20px;color:black;" id="operatorPlanName"></span>
          </div>
          <div style="margin-bottom: 10px;width: 100%;display: inline-block;" id="plansCategory">
            <a onclick="showMobilePlan('FTTMobileRechargePlanDiv','talktimetab');" class="planTab" id="talktimetab">FTT</a>
            <a onclick="showMobilePlan('3GMobileRechargePlanDiv','3gtab');" class="planTab" id="3gtab">3G</a>
            <a onclick="showMobilePlan('2GMobileRechargePlanDiv','2gtab');" class="planTab" id="2gtab">2G</a>
            <a onclick="showMobilePlan('SMSMobileRechargePlanDiv','ssmstab');" class="planTab" id="ssmstab">SMS</a> 
            <a onclick="showMobilePlan('LSCMobileRechargePlanDiv','lsctab');" class="planTab" id="lsctab">LSC</a> 
            <a onclick="showMobilePlan('TUPMobileRechargePlanDiv','specialtab');" class="planTab" id="specialtab">TOP-UP</a>
            <a onclick="showMobilePlan('RMGMobileRechargePlanDiv','roamingtab');" class="planTab" id="roamingtab">Roaming</a>
            <a onclick="showMobilePlan('OTRMobileRechargePlanDiv','othertab');" class="planTab" id="othertab">Other</a>
          </div>

      </div>
       <div id="mobilePlanDiv">
                 
        </div>
</nav>
<!-- Boxes Section End -->
<c:if test="${rechargeServiceName=='JOLO'}">
<script type="text/javascript">
//prepaidOperator
var prepaidOperatorData = [
<c:forEach var="operator" items="${prepaidoperators}">
{
  text: "${operator.operatorName}",
  value: "${operator.pay_code}",
},
</c:forEach>
];
//DthOperator
var dthOperatorData = [
<c:forEach var="dthoperator" items="${dthoperators}">
{
	text: "${dthoperator.operatorName}",
	value: "${dthoperator.pay_code}",
},
</c:forEach> 
];
//PostPaid operator
var postpaidOperatorData = [
<c:forEach var="postpaidoperator" items="${postpaidoperators}">
{
	text: "${postpaidoperator.operatorName}",
	value: "${postpaidoperator.pay_code}",
},
</c:forEach>
];      

//DataCard Operator
var dataCardOperator = [
<c:forEach var="dataCardOperator" items="${datacardoperators}">
{
	text: "${dataCardOperator.operatorName}",
	value: "${dataCardOperator.pay_code}",
},
</c:forEach>
]; 

//Landline operators
var landlineOperator = [
 <c:forEach var="landlineOperator" items="${landlineoperators}">
 {
    text: "${landlineOperator.operatorName}",
    value: "${landlineOperator}",
 },
 </c:forEach>
];

 var circles = [
<c:forEach var="state" items="${circles}">
{
	  text: "${state.circleName}",
	  value: "${state.circleCode}",
},
</c:forEach>
];
</script>
</c:if>
<c:if test="${rechargeServiceName!='JOLO'}">
<script type="text/javascript">
 //prepaidOperator
  var prepaidOperatorData = [
  <c:forEach var="operator" items="${prepaidoperators}">
  {
    text: "${operator.operatorName}",
    value: "${operator}",
  },
  </c:forEach>
  ];
//DthOperator
var dthOperatorData = [
<c:forEach var="dthoperator" items="${dthoperators}">
  {
    text: "${dthoperator.operatorName}",
    value: "${dthoperator}",
  },
 </c:forEach> 
];
//PostPaid operator
 var postpaidOperatorData = [
  <c:forEach var="postpaidoperator" items="${postpaidoperators}">
  {
    text: "${postpaidoperator.operatorName}",
    value: "${postpaidoperator}",
  },
  </c:forEach>
  ];      

//DataCard Operator
 var dataCardOperator = [
  <c:forEach var="dataCardOperator" items="${datacardoperators}">
  {
    text: "${dataCardOperator.operatorName}",
    value: "${dataCardOperator}",
  },
  </c:forEach>
  ]; 

//Landline operators
var landlineOperator = [
 <c:forEach var="landlineOperator" items="${landlineoperators}">
 {
    text: "${landlineOperator.operatorName}",
    value: "${landlineOperator}",
 },
 </c:forEach>
];
   var circles = [
  <c:forEach var="state" items="${circles}">
  {
    text: "${state}".replace("_"," "),
    value: "${state.circleCode}",
  },
  </c:forEach>
  ];
</script>
</c:if>
<script type="text/javascript">
function showThemeDiv (divmain,div1,div2,div3) {
$('#'+divmain).removeClass('Hidden');
$('#'+div1).addClass('Hidden');
$('#'+div2).addClass('Hidden');
$('#'+div3).addClass('Hidden');
}
</script>

