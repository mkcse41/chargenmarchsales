<style type="text/css">
    .Hidden{display: none;}
    #gSignInWrapper{text-align: center;}
    #customBtn {text-align:left;display: block;color: #FFF;width: 160px;height: 50px;font-weight: bold;font-size: 12px;margin-left: 26%;}
    #customBtn:hover {cursor: pointer;}
    #customBtn span.label {font-weight: bold;}
    #customBtn span.buttonText {display: inline-block;vertical-align: middle;font-size: 12px;color: #fff;width: 60%;margin-left: -4px;}
    .Hand{cursor: pointer; }
</style>
<!-- <script src="https://connect.facebook.net/en_US/all.js"></script> -->
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css"/>
  <script src="https://apis.google.com/js/api:client.js"></script>
<script>
/*Google Login*/
var googleUser = {};
  var startApp = function() {
    gapi.load('auth2', function(){
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      auth2 = gapi.auth2.init({
    	client_id: '167369363912-imvgt0ts0jce5rqceqpsqn6f7m50oe3d.apps.googleusercontent.com',//for live version 
        //client_id: '1051842101187-hnrfh49g8hgsuj3rdfugkkaf093o0c0q.apps.googleusercontent.com', //for beta version
        cookiepolicy: 'single_host_origin',
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      attachSignin(document.getElementById('customBtn'));
    });
  };

  function attachSignin(element) {
    auth2.attachClickHandler(element, {},
        function(googleUser) {
                $('#chargers_reg_name').val(googleUser.getBasicProfile().getName());
                $('#charger_reg_emailId').val(googleUser.getBasicProfile().getEmail());  
                $('#charger_reg_mobileNo').val('9999999999');
           addUserInfoByFbGoogle();
        }, function(error) {
          alert(JSON.stringify(error, undefined, 2));
        });
  }

</script>
<!-- Popup Code Of Login/Signup -->
<c:if test="${empty(userbean) || (!empty(sessionScope.userbean) && sessionScope.userbean.isverified == 0)}">
   <input type="hidden" value="true" id="checkUserLogin"/>
</c:if>
<c:if test="${!empty(userbean)  && sessionScope.userbean.isverified == 1}">
   <input type="hidden" value="false" id="checkUserLogin"/>
</c:if>
<div class="popwrap hidden" id="chargersLogin">
  <div class="overlay"></div>
  <div class="popup hidden" style="margin-top:0;">

   <div id="loginOtp" class="hidden">
    <form name="OTPVerification" >
         <p class="welcome" style="margin:0px;padding:15px 15px 0px 15px">Please insert your OTP.</p>
           <span class="input input--hoshi" style="max-width:520px;height:50px;top:0px" >
                <input class="input__field input__field--hoshi" type="text" style="padding-left:10px;color:#fff;font-weight:300;width:100%;margin:30px 0 30px 0; height:40px;" id="otpthirdparty" value="" maxlength="6" name="otpthirdparty" placeholder="One Time Password" onkeypress="return isNumberKey(event)"/>
            </span>
              <div id="errorMessageotpthirdparty"></div>    
          <input type="button" class="registrationButton" style="display: block;margin-left: 20px;" value="Proceed" onclick="return submitUserOTP('otpthirdparty','errorMessageotpthirdparty');"/>  
      </form>
    </div>

  <div id="MobileVerification" class="hidden">
    <form name="MobileVerification">
         <p class="welcome" style="margin:0px;padding:15px 15px 0px 15px">Please insert Mobile Number.</p>
           <span class="input input--hoshi" style="max-width:520px;height:50px;top:0px" >
                <input class="input__field input__field--hoshi" type="text" style="padding-left:10px;color:#fff;font-weight:300;width:100%;margin:30px 0 30px 0; height:40px;" id="login_mobile" value="" maxlength="10" name="mobilenumber" placeholder="Mobile Number" onkeypress="return isNumberKey(event)"/>
            </span>
              <div id="errorMessagemobile"></div>    
          <input type="button" class="registrationButton" style="display: block;margin-left: 20px;" value="Proceed" onclick="return submitUserMobile('login_mobile','errorMessagemobile');"/>  
      </form>
    </div>


  <div id="loginDiv">
    <div class="chargenmarchlogin textcenter">
      <p class="welcome" style="margin:0px;padding:15px 15px 0px 15px">Welcome back! Enter your password to login!</p>
      <form action="#" name="loginform">

        <span class="input input--hoshi" style="max-width:520px;height:50px;top:0px" >
          <input class="input__field input__field--hoshi" type="email"  style="padding-left:10px;color:#fff;font-weight:300;width:92%;margin:30px;height:40px;" id="chargers_emailId" name="chargers_emailId" value="" placeholder="Email Id" onkeypress="return spaceNotAllowed(event)"/>
        </span>

        <span class="input input--hoshi" style="max-width:520px;height:50px;top:0px" >
          <input class="input__field input__field--hoshi" type="password" style="padding-left:10px;color:#fff;font-weight:300;width:92%;margin:30px;height:40px;" id="chargers_password" name="chargers_password" value="" placeholder="Password"/>
        </span>
         <div id="errorMessagelogin"></div>   
        <input type="button" class="loginButton" value="Sign in" onclick="return userLogin('chargers_emailId','chargers_password','errorMessagelogin')">
         <a class="forgot Hand" title="Forgot Password" onclick="forgotPassword('chargers_emailId','errorMessagelogin');">Forgot Password?</a>
      </form>
      <p>Don't have a Charge&amp;March account? <span class="Hand" onclick="showSignInPopUp();" style="color:#4dbea0;font-weight:900">Click here</span> to create one</p>
    </div>



   <div class="socialLoginLinks">

   <div  class="textcenter" style="padding-bottom:15px;padding-top:40px;"><span> Or sign in with</span></div>
<!--       <a class="btn-fb Hand" id="loginBtnFacebook" title="facebook" onclick="logIn(window.location.href);" value="Login"><i class="sprite ic_fb"></i>Facebook</a>
 -->
      <div id="gSignInWrapper">
      <div id="customBtn" class="btn-google">
      <i class="sprite ic_google"></i>
        <span class="buttonText">Google</span>
      </div>
    </div>
    </div>
  </div>



  </div>
</div>
<!-- Signup FOrm Code -->
<div class="popwrap1 hidden" id="chargersSignup">
  <div class="overlay"></div>
  <div class="popup1 hidden">
      <form action="#" id="OTPVerification" class="hidden">
      <p class="welcome" style="margin:0px;padding:15px 15px 0px 15px">Please insert your OTP.</p>
           <span class="input input--hoshi" style="max-width:520px;height:50px;top:0px" >
                <input class="input__field input__field--hoshi" type="text" style="padding-left:10px;color:#fff;font-weight:300;width:100%;margin:30px 0 30px 0; height:40px;" id="otp" value="" maxlength="6" name="otp" placeholder="One Time Password" onkeypress="return isNumberKey(event)"/>
            </span>
              <div id="errorMessageotp"></div>    
          <input type="button" class="registrationButton" style="display: block;margin-left: 20px;" value="Proceed" onclick="return submitUserOTP('otp','errorMessageotp');"/>  
      </form>
    
  <div class="chargenmarchlogin textcenter" id="charger_registerDiv">
    <p class="welcome" style="margin:0px;padding:15px 15px 0px 15px">Welcome User! Share Your Details For Register!</p>
      <form action="#" name="registerform" style="width:500px;margin-left:25px;" id="registerform">
        <span class="input input--hoshi" style="max-width:520px;height:50px;top:0px" >
        <input class="input__field input__field--hoshi" type="text" style="padding-left:10px;color:#fff;font-weight:300;width:92%;margin:30px 0 30px 0; height:40px;" id="chargers_reg_name" name="chargers_reg_name" value=""  maxlength="30" placeholder="Name" onkeypress="return onlyAlphabets(event,this)" />
        </span>
        <span class="input input--hoshi" style="max-width:520px;height:50px;top:0px" >
          <input class="input__field input__field--hoshi" type="email" id="charger_reg_emailId" name="charger_reg_emailId" value=""  maxlength="30" style="padding-left:10px;color:#fff;font-weight:300;width:92%;margin:30px 0 30px 0; height:40px;" placeholder="Email Id" onkeypress="return spaceNotAllowed(event)" />
        </span>
        <span class="input input--hoshi" style="max-width:520px;height:50px;top:0px" >
          <input class="input__field input__field--hoshi" type="text" style="padding-left:10px;color:#fff;font-weight:300;width:92%;margin:30px 0 30px 0; height:40px;" id="charger_reg_mobileNo" value="" maxlength="10" name="charger_reg_mobileNo" placeholder="Mobile Number" onkeypress="return isNumberKey(event)"/>
        </span>
        
         <span class="input input--hoshi tooltip" style="max-width:520px;height:50px;top:0px" >
          <input class="input__field input__field--hoshi" type="text" style="padding-left:10px;color:#fff;font-weight:300;width:92%;margin:30px 0 30px 0; height:40px;" id="charger_reg_referral_code" value="" maxlength="10" name="charger_reg_referral_code" placeholder="Referral Code"/>
           <span class="tooltiptext">With CAM referral program, now you can earn without recharging. Just share your referral code with new registrant and earn a cashback of 5% on first three recharges of registrant.</span>
        </span>

          <div style="text-align: left;margin-top: -25px;"> <input type="checkbox" id="autopass" name="autopass" style="width: 14px;display: inline-block;"><span> auto generated password </span></div>

        <span class="input input--hoshi" style="max-width:520px;height:50px;top:0px" >
          <input class="input__field input__field--hoshi" type="password" style="padding-left:10px;color:#fff;font-weight:300;width:92%;margin:30px 0 30px 0; height:40px;" id="charger_reg_password" value="" maxlength="14" name="charger_reg_password" placeholder="Password" onkeypress="return spaceNotAllowed(event)"/>
        </span>

         <div style="text-align: left;margin-top:-25px;" id="showpassdiv"> <input type="checkbox" id="showpass" name="showpass" style="width: 14px;display: inline-block;"><span>show password </span></div>


                            <!--<input type="email" name="email" onClick="removetext(this.name)" onBlur="addtext(this.value)" value="" />
                            <input type="password" name="password" onClick="removetext(this.name)" onBlur="addtext(this.value)" value="" />
                            <input type="password" name="cpassword" onClick="removetext(this.name)" onBlur="addtext(this.value)" value="" />-->
                         <div id="errorMessagesignin"></div>   
                            <input type="button" class="registrationButton" value="Sign Up" onclick="return userRegister('chargers_reg_name','charger_reg_emailId','charger_reg_mobileNo','charger_reg_password','WEB','true','errorMessagesignin')"/>
                          </form>
                          <p style="margin-top:5px;">Already have a Charge&amp;March account? <span onclick="showLoginPopUp();" class="showpopup" style="color:#4dbea0;font-weight:900;">Click here</span> to Login</p>



                        </div>
                      </div>
                    </div>

                    <!-- Popup Code Of Login/Sigup -->

  <script>startApp();</script>
