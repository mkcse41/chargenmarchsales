<style>
.outerMainDiv{
        width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(http://localhost:8080/chargenmarch/resources/images/widget_transparent.png) bottom center repeat;
    position: fixed;
    display: block;
    top: 0;
}

.Mainpopup
{
width: 600px;
    background-color: #408080;
    border-top: 5px solid #fcc135;
    margin: auto;
    height: 200px;
    z-index: 999999;
    margin-top: 18%;
    display: block;
}
.charge{width:40%;float:left;}.march{width:40%;float:right;text-align:right}.mid{width:15%;float:left;}
.mid{margin-top:15%;text-align:center;padding-left:10px}


.onoffdiv{
    list-style:none;
    width:140px;
    height:26px;
    position:absolute;
 display: block;
}

.onoffdiv li{
    float:left;
    line-height:23px;
    font-size:11px;
    padding:2px 10px 0; 
    
    background: #E5E5E5;
    background-image:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#F3F3F3), to(#E5E5E5));
    text-shadow:0 1px 0 #FFF;
    border-left:1px solid #D5D5D5;
    border-top:1px solid #D5D5D5;
    border-bottom:1px solid #D5D5D5;
    -webkit-box-shadow:0 1px 0 #FFF inset, 0 0 5px rgba(0, 0, 0, .1) inset, 0 1px 1px rgba(0, 0, 0, .3);
}

.onoffdiv li:first-child{
    -webkit-border-radius:5px 0 0 5px;
}
.onoffdiv li:last-child{
    -webkit-border-radius:0 5px 5px 0;
}

.onoffdiv li a{
    text-decoration: none;
    font-family:Helvetica, Arial;
    text-transform:uppercase;
    color:#a1a1a1;
}

.on{
    background: #505050 !important;
    background-image:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#777), to(#505050)) !important;
    text-shadow:0 -1px 0 #444, 0 0 7px #9AE658 !important;
    border-right:1px solid #444 !important;
    border-top:1px solid #444 !important;
    border-bottom:1px solid #444 !important;
    -webkit-box-shadow:0 1px 2px rgba(0, 0, 0, .7) inset, 0 1px 0 #FFF !important;  
}

.onoffdiv li:not(.on):active{
    background-image:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#ddd), to(#f1f1f1));
}


.onoffdiv li.on a{
    color:#7BBA47;
    cursor: default;
}

.on-off-switch{
    position:relative;
    cursor:pointer;
    overflow:hidden;
    user-select:none;
}

.on-off-switch-track{
    position:absolute;
    border : solid #888;
    z-index:1;
    background-color: #fff;
    overflow:hidden;
}

/* semi transparent white overlay */
.on-off-switch-track-white{
    background-color:#FFF;
    position:absolute;
    opacity:0.2;
    z-index:30;
}
/* Track for "on" state */
.on-off-switch-track-on{
    background-color:#336666;
    border-color:#008844;
    position:absolute;
    z-index:10;
    overflow:hidden;
}
/* Track for "off" state */
.on-off-switch-track-off{
    position:absolute;
    border-color:#CCC;
    z-index:1;
}

.on-off-switch-thumb{
    position:absolute;
    z-index:2;
    overflow:hidden;
}

.on-off-switch-thumb-shadow{
    opacity:0.5;
    border:1px solid #000;
    position:absolute;
}

.track-on-gradient, .track-off-gradient{

    background: -webkit-linear-gradient(180deg,rgba(0,0,0,0.2), rgba(0,0,0,0)); /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(180deg, rgba(0,0,0,0.2), rgba(0,0,0,0)); /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(180deg, rgba(0,0,0,0.2), rgba(0,0,0,0)); /* For Firefox 3.6 to 15 */
    background: linear-gradient(180deg, rgba(0,0,0,0.2), rgba(0,0,0,0)); /* Standard syntax */
    position:absolute;
    width:100%;
    height:5px;
}


.on-off-switch-thumb-color{
    background: -webkit-linear-gradient(45deg, #BBB, #FFF); /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(45deg, #BBB, #FFF); /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(45deg, #BBB, #FFF); /* For Firefox 3.6 to 15 */
    background: linear-gradient(45deg, #BBB, #FFF); /* Standard syntax */
    background-color:#F0F0F0;
    position:absolute;
}

.on-off-switch-thumb-off{
    border-color:#AAA;
    position:absolute;
}
.on-off-switch-thumb-on{
    border-color:#008855;
    z-index:10;
}
.on-off-switch-text{
    width:100%;
    position:absolute;
    font-family:arial;
    user-select:none;
    font-size:10px;
}

.on-off-switch-text-on{
    color:#FFF;
    text-align:left;
}
.on-off-switch-text-off{
    color:#000;
    text-align:right;
}
/* Mouse over thumb effect */
.on-off-switch-thumb-over{
    background-color:#F5F5F5;
    background: -webkit-linear-gradient(45deg, #CCC, #FFF); /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(45deg, #CCC, #FFF); /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(45deg, #CCC, #FFF); /* For Firefox 3.6 to 15 */
    background: linear-gradient(45deg, #CCC, #FFF); /* Standard syntax */

}
  .MainPopUpClose{  background-image: url("http://localhost:8080/chargenmarch/resources/images/close.svg");
    background-repeat: no-repeat;
    background-size: contain;
    background-position: right top;
    border: 0px none;
    color: #EF0808;
    cursor: pointer;
    height: 63px;
    position: fixed;
    vertical-align: middle;
    width: 6em;
    z-index: 9999;
    width: 57px;
    font-size: 15px;
    display: block;
    left: 70%;
    margin-top: -18px;}
</style>
<c:set var="popUpInfo" value="${sessionScope.updatePopUpInfo}"/>
<div class="outerMainDiv DispNoneImp" id="outerMainDiv">
<div class="Mainpopup DispNoneImp" id="Mainpopup">
<a class="MainPopUpClose" onclick="updatePopUpInfo('${applicationScope['baseUrl']}');"></a>
<div class="charge"><img src="${applicationScope['baseUrl']}/resources/images/charge.png" /></div>
<div class="mid">

<div id="onoffdiv1" class="onoffdiv DispNoneImp">
    <li class="on"><a>Charge</a></li>
    <li class="Hand" onclick="updatePopUpInfo('${applicationScope['baseUrl']}/march/air/airticket');"><a>March</a></li>
</div>

<div id="onoffdiv2" class="onoffdiv DispNoneImp">
    <li class="Hand" onclick="updatePopUpInfo('${applicationScope['baseUrl']}');"><a>Charge</a></li>
    <li class="on"><a>March</a></li>
</div>


<div id="listener-text"></div>
</div>
<div class="march"><img src="${applicationScope['baseUrl']}/resources/images/march.png" /></div>
</div>
</div>

<script type="text/javascript">
var pageType = '${pageType}';
if(pageType == "march"){
    $('#onoffdiv2').removeClass('DispNoneImp');
}
if(pageType == 'charge'){
   $('#onoffdiv1').removeClass('DispNoneImp');
}
  $(document).ready(function() {
    var popUpInfo = '${popUpInfo}';
    console.log("popUpInfo"+popUpInfo);
    if(popUpInfo != "true"){
      $('#Mainpopup').removeClass('DispNoneImp');
      $('#outerMainDiv').removeClass('DispNoneImp');
    }
  });

  $(function(){
    $(".onoffdiv li").click(function(){
        $(".onoffdiv li").removeClass("on");
        $(this).addClass("on"); 
    });
});
</script>
