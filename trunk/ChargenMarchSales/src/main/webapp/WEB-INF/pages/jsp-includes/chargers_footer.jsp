 <footer>
        <div class="container90">
            <div class="clearfix">
                <div class="footercolumn floatleft">
                    <div class="footerblock">
                        <h5>MOBILE RECHARGE</h5>
                        <ul>
                            <li><a href="#">Airtel</a>(<a href="#" title="Plans">Plans</a>)</li>
                            <li><a href="#">Aircel</a>(<a href="#" title="Plans">Plans</a>)</li>
                            <li><a href="#">Vodafone</a>(<a href="#" title="Plans">Plans</a>)</li>
                            <li><a href="#">BSNL</a>(<a href="#" title="Plans">Plans</a>)</li>
                            <li><a href="#">Tata Docomo GSM</a>(<a href="#" title="Plans">Plans</a>)</li>
                            <li><a href="#">Idea</a>(<a href="#" title="Plans">Plans</a>)</li>
                            <li><a href="#">Indicom Walky</a>(<a href="#" title="Plans">Plans</a>)</li>
                            <li><a href="#">MTNL Delhi</a>(<a href="#" title="Plans">Plans</a>)</li>
                            <li><a href="#">Reliance CDMA</a>(<a href="#" title="Plans">Plans</a>)</li>
                            <li><a href="#">Reliance GSM</a>(<a href="#" title="Plans">Plans</a>)</li>
                            <li><a href="#">Tata Indicom</a>(<a href="#" title="Plans">Plans</a>)</li>
                            <li><a href="#">Uninor</a>(<a href="#" title="Plans">Plans</a>)</li>
                            <li><a href="#">MTS</a>(<a href="#" title="Plans">Plans</a>)</li>
                            <li><a href="#">Videocon</a>(<a href="#" title="Plans">Plans</a>)</li>
                            <li><a href="#">Virgin CDMA</a>(<a href="#" title="Plans">Plans</a>)</li>
                            <li><a href="#">Virgin GSM</a>(<a href="#" title="Plans">Plans</a>)</li>
                            <li><a href="#">Tata Docomo CDMA</a>(<a href="#" title="Plans">Plans</a>)</li>
                        </ul>
                    </div>
                </div>
                
                <div class="footercolumn floatleft">
                    <div class="footerblock">
                        <h5>DATA CARD RECHARGE</h5>
                        <ul>
                            <li><a href="#">Tata Photon</a></li>
                            <li><a href="#">MTS MBlaze</a></li>
                            <li><a href="#">MTS MBrowse</a></li>
                            <li><a href="#">Reliance NetConnect</a></li>
                            <li><a href="#">Airtel</a></li>
                            <li><a href="#">BSNL</a></li>
                            <li><a href="#">Aircel</a></li>
                            <li><a href="#">MTNL Delhi</a></li>
                            <li><a href="#">Vodafone</a></li>
                            <li><a href="#">Idea</a></li>
                            <li><a href="#">Reliance-GSM</a></li>
                            <li><a href="#">MTNL Mumbai</a></li>
                            <li><a href="#">T24</a></li>
                            <li><a href="#">Tata Docomo</a></li>
                        </ul>
                    </div>
                    <div class="footerblock">
                        <h5>DTH(TV) RECHARGE</h5>
                        <ul>
                            <li><a href="#">Airtel Digital TV</a></li>
                            <li><a href="#">Reliance Digital TV </a></li>
                            <li><a href="#">Dish TV </a></li>
                            <li><a href="#">Tata Sky </a></li>
                            <li><a href="#">Sun Direct </a></li>
                            <li><a href="#">Videocon D2H </a></li>
                        </ul>
                    </div>
                </div>
               
                <div class="footercolumn floatleft">
                    <div class="footerblock">
                        <h5>POSTPAID</h5>
                        <ul>
                            <li><a href="#">Airtel Bill Payment</a></li>
                            <li><a href="#">BSNL Bill Payment</a></li>
                            <li><a href="#">Tata Docomo GSM Bill Payment</a></li>
                            <li><a href="#">Tata Docomo CDMA Bill Payment</a></li>
                            <li><a href="#">Idea Bill Payment</a></li>
                            <li><a href="#">Vodafone Bill Payment</a></li>
                            <li><a href="#">Reliance GSM Bill Payment</a></li>
                            <li><a href="#">Reliance CDMA Bill Payment</a></li>
                        </ul>
                     </div>
                    <div class="footerblock">
                        <h5>FREECHARGE</h5>
                        <ul>
                            <li><a href="#" target="_blank">About Us</a></li>
                            <li><a href="#" target="_blank">Careers</a></li>
                            <li><a href="#" target="_blank">Support</a></li>
                            <li><a href="#" target="_blank">Contact Us</a></li>
                        </ul>
                        <ul>
                            <li><a href="#" target="_blank">Sitemap</a></li>
                            <li><a href="#" target="_blank">Geekery</a></li>
                            <li><a href="#" target="_blank">T &amp; C</a></li>
                            <li><a href="#" target="_blank">Blog</a></li>
                        </ul>
                    </div>
                    <div class="footerblock">
                        <h5>MOBILE</h5>
                        <ul>
                            <li><a href="#">Android App</a></li>
                            <li><a href="#">Windows</a></li>
                        </ul>
                        <ul>
                            <li><a href="#">Mobile Site</a></li>
                            <li><a href="#">iOS</a></li>
                        </ul>
                    </div>
                </div>
                         
                <div class="footercolumn floatleft">
                    <div class="footerblock">
                        <h5>SECURED</h5>
                        <img src='resources/images/visa-mastercard.png' alt="Online recharge"/>         
                    </div>
                    <div class="footerblock">
                        <h5>SUPPORT</h5>
                        For help, send email to <a href="mailto:care@chargenmarch.com">care@chargenmarch.com</a>
                    </div>
                    <div class="footerblock">
                        <h5>JOIN US ON</h5>
                        <div class="clearfix">
                            <a href="#"><div class="fb"></div></a>
                            <a href="#"><div class="twitter"></div></a>
                            <a href="#"><div class="googleplus"></div></a>
                        </div>
                    </div>
                </div>
            </div>
                  
            <div class="footer-bottom textcenter">
                <p>&copy; LifeSoft Technology Pvt. Ltd. All Rights Reserved.</p>
            </div>
         </div>
    </footer>