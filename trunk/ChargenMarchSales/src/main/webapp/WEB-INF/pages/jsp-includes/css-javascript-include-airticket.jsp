<script type="text/javascript">var basepath = "${applicationScope['baseUrl']}";</script>
<link rel="stylesheet" href="${applicationScope['baseUrl']}/resources/css/ghpages-materialize.css" type="text/css">
<link rel="stylesheet" href="${applicationScope['baseUrl']}/resources/css/air-ticket-common.css" type="text/css"/>
<link rel="stylesheet" href="${applicationScope['baseUrl']}/resources/css/air-ticket.css" type="text/css"/>
<link rel="stylesheet" href="${applicationScope['baseUrl']}/resources/css/jquery-ui.css" />
<script type="text/javascript" src="${applicationScope['baseUrl']}/resources/javascript/jquery.min.js"></script>
<script type="text/javascript" src="${applicationScope['baseUrl']}/resources/javascript/global.js" defer></script>
<script src="${applicationScope['baseUrl']}/resources/javascript/jquery-ui.js" defer></script>
<script type="text/javascript" src="${applicationScope['baseUrl']}/resources/javascript/air-ticket.js" onload="loadAddThisScript();" defer></script>  