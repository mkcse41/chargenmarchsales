<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
  <div id="mobileRechargePlan"  >
  <c:forEach var="planType" items="${planList}">
      <div id="${planType.key}MobileRechargePlanDiv" style="display:none">
            <c:forEach var="plans" items="${planType.value}">
            <div style="width:100%;height:71px;border:solid 1px #EAEAEA;margin:10px" class="Hand" onclick="selectMobilePlan('${plans.amount}','mobileRechargeAmount','plandata')" title="${plans.description}">
              <div style="width:20%;float:left;background:#B9DCFF;color:#219DC5;text-align:center;font-size:16px;height:70px;padding-top:20px">Rs. ${plans.amount}</div>
              <div style="width:80%;float:left;border-bottom:solid 1px #EAEAEA">
                <table width="100%" align="center" style="width:100%;height:70px;background:#fff;margin:0px">
                  <tR style="background:#fff;border:none">
                    <td colspan="2" align="justify" style="background:#fff;padding:5px"><b style="color:#000;font-weight:500">
                    <c:if test="${fn:length(plans.description)>30}">${fn:substring(plans.description, 0, 29)}...</c:if>
                    <c:if test="${fn:length(plans.description)<30}">${plans.description}</c:if>
                    </b></td>
                  </tR>
                  <tr style="background:#fff;border:none">
                    <tD style="background:#fff;color:#999;padding:5px">Validity | ${plans.validity}</tD>
                    <td align="right" style="background:#fff;color:#999;padding:5px"></td>
                  </tr>
                </table>
              </div>
            </div>
            </c:forEach>
        </div>
    </c:forEach>
</div>



  
