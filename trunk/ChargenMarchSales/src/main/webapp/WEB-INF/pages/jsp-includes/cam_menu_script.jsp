<script>
    $(document).ready(function() {
        /* $('select').niceSelect();  */

         //Menu Script
         $('#cssmenu > ul > li > a').click(function() {
              $('#cssmenu li').removeClass('active');
              $(this).closest('li').addClass('active'); 
              var checkElement = $(this).next();
              if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                $(this).closest('li').removeClass('active');
                checkElement.slideUp('normal');
              }
              if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                $('#cssmenu ul ul:visible').slideUp('normal');
                checkElement.slideDown('normal');
              }
              if($(this).closest('li').find('ul').children().length == 0) {
                return true;
              } else {
                return false; 
              }   
        }); 
                   
    });
</script>
    <script>
      (function() {
        // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
        if (!String.prototype.trim) {
          (function() {
            // Make sure we trim BOM and NBSP
            var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
            String.prototype.trim = function() {
              return this.replace(rtrim, '');
            };
          })();
        }

        [].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
          
        } );

       
      })();
    </script>
<!-- Required JS For : Right Menu Effect & Header Fix On ScrollDown -->