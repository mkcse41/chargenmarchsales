
<div id="reward">
	<h4 style="font-size: 1.9rem;">All Your Online Recharges are completely safe and secure.</h4>
	<span>Your Online Recharge is completely safe and secure on
		ChargenMarch.</span>

	<div class="row">
		<div class="col-md-5">
			<div class="col-md-3">
				<img src="${applicationScope['baseUrl']}/resources/images/visa-mastercard.png" />
			</div>
			<div class="col-md-9">
				<h5 align="justify">ChargeNMarch is 128 bit EV SSL secured</h5>
				<p align="justify" style="color: #fff">All connections to
					ChargeNMarch through the browser and application are 128-bit
					encrypted. This simply means that all your transactional & personal
					card details added while doing your online recharge will always be
					safe & secure.</p>
			</div>
		</div>

		<div class="col-md-5">
			<div class="col-md-3">
				<img src="${applicationScope['baseUrl']}/resources/images/landing-norton.png" />
			</div>
			<div class="col-md-9">
				<h5 align="justify">ChargeNMarch is 128 bit EV SSL secured</h5>
				<p align="justify" style="color: #fff">All connections to
					ChargeNMarch through the browser and application are 128-bit
					encrypted. This simply means that all your transactional & personal
					card details added while doing your online recharge will always be
					safe & secure.</p>
			</div>
		</div>
	</div>
	<div class="row"
		style="width: 90%; text-align: center; margin: auto; margin-top: 30px">
		<div class="col-md-5">
			<div class="col-md-3">
				<img src="${applicationScope['baseUrl']}/resources/images/landing-payment.png" />
			</div>
			<div class="col-md-9">
				<h5 align="justify">ChargeNMarch is 128 bit EV SSL secured</h5>
				<p align="justify" style="color: #fff">All connections to
					ChargeNMarch through the browser and application are 128-bit
					encrypted. This simply means that all your transactional & personal
					card details added while doing your online recharge will always be
					safe & secure.</p>
			</div>
		</div>

		<div class="col-md-5">
			<div class="col-md-3">
				<img src="${applicationScope['baseUrl']}/resources/images/landing-pci.png" />
			</div>
			<div class="col-md-9">
				<h5 align="justify">ChargeNMarch is 128 bit EV SSL secured</h5>
				<p align="justify" style="color: #fff">All connections to
					ChargeNMarch through the browser and application are 128-bit
					encrypted. This simply means that all your transactional & personal
					card details added while doing your online recharge will always be
					safe & secure.</p>
			</div>
		</div>
	</div>
</div>

<!-- Reward End -->

<div class="reference">
	<h4>FASTEST & SIMPLEST WAY FOR ONLINE RECHARGE</h4>
	<span>You can complete online mobile recharges and bill payments
		on FreeCharge in less than 10 seconds!</span>
	<div class="row">
		<div class="col-md-2">&nbsp;</div>
		<div class="col-md-4">
			<p align="justify">We make your online recharge experience faster
				with FreeCharge through:</p>
			<p align="justify">
				<b>Saved Cards & Payment preferences -</b> Forget the hassle of
				remembering your card details.Your card & payment preferences are
				saved for faster subsequent recharges.
			</p>
		</div>
		<div class="col-md-4">
			<p align="justify">
				<b>FreeCharge Balance -</b> Pre-fill money into your account to have
				the fastest recharge experience. Completing a transaction is then,
				just a click away!
			</p>
			<p align="justify">
				<b>Turbo recharges -</b> We remember your recharged numbers that
				makes it a easy reference when you come on FreeCharge the next time
				to do an online recharge.
			</p>
		</div>
		<div class="col-md-2">&nbsp;</div>
	</div>

	<div class="row ope1">
		<img src="${applicationScope['baseUrl']}/resources/images/Operator/icon1.jpg" /> <img
			src="${applicationScope['baseUrl']}/resources/images/Operator/icon2.jpg" /> <img
			src="${applicationScope['baseUrl']}/resources/images/Operator/icon3.jpg" /> <img
			src="${applicationScope['baseUrl']}/resources/images/Operator/icon4.jpg" /> <img
			src="${applicationScope['baseUrl']}/resources/images/Operator/icon5.jpg" /> <img
			src="${applicationScope['baseUrl']}/resources/images/Operator/icon6.jpg" /> <img
			src="${applicationScope['baseUrl']}/resources/images/Operator/icon8.jpg" /> <img
			src="${applicationScope['baseUrl']}/resources/images/Operator/icon10.jpg" /> <img
			src="${applicationScope['baseUrl']}/resources/images/Operator/icon11.jpg" /> <img
			src="${applicationScope['baseUrl']}/resources/images/Operator/icon12.jpg" />
	</div>
</div>
