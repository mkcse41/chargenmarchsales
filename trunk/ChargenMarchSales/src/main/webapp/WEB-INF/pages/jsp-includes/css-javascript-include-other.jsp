<script type="text/javascript">var basepath = "${applicationScope['baseUrl']}";window.localStorage.setItem("cid",'${userbean.id}');</script>
<link rel="manifest" href="${applicationScope['baseUrl']}/resources/manifest.json">
<link rel="stylesheet" href="${applicationScope['baseUrl']}/resources/css/ghpages-materialize.css" type="text/css">
<link rel="stylesheet" href="${applicationScope['baseUrl']}/resources/css/responsive-tabs.css" type="text/css">
<link rel="stylesheet" href="${applicationScope['baseUrl']}/resources/css/style.css" type="text/css">
<link rel="stylesheet" href="${applicationScope['baseUrl']}/resources/css/global.css" type="text/css">
<script src="${applicationScope['baseUrl']}/resources/javascript/jquery.min.js"></script>
 <script src="${applicationScope['baseUrl']}/resources/javascript/global.js" defer></script>
<script src="${applicationScope['baseUrl']}/resources/javascript/jquery.scrollex.min.js" defer></script>
<script type="text/javascript" src="${applicationScope['baseUrl']}/resources/notification/chrome-pushwoosh-notification.js" defer onload="startPushWooshService()"></script>
<script src="${applicationScope['baseUrl']}/resources/javascript/responsiveTabs.min.js" onload="loadAddThisScript();" defer></script>
 