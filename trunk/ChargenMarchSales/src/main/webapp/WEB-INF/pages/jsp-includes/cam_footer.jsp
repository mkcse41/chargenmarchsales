<!-- Ads screen -->
<div style="text-align: center;padding: 10px;">
 <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- WEB Footer -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-6260839974232701"
     data-ad-slot="7432049871"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
<footer id="footer">
    <ul class="footerlink lk">
      <li><a class="mobile Hand" href="${applicationScope['baseUrl']}">Mobile</a></li>
      <li><a class="dth Hand" href="${applicationScope['baseUrl']}" >DTH</a></li>
      <li><a class="data Hand" href="${applicationScope['baseUrl']}">Data Card</a></li>
    </ul>
     <ul class="footerlink lk">
      <li><a href="${applicationScope['baseUrl']}/contactus" class="Hand" title="Contact Us">Contact Us</a></li>
      <li><a href="${applicationScope['baseUrl']}/privacy-policy" title="Privacy Policy" class="Hand">Privacy Policy</a></li>
      <li><a href="${applicationScope['baseUrl']}/termsandcondition" title="Terms and Condition" class="Hand">Terms and Condition</a></li>
    </ul>
    <p style="text-align: center">Copyright LifeSoft Technology &copy; 2016 All Rights Are Reserved</p>
</footer>
<!-- Footer Section End -->
<script>
    function fbshareCurrentPage()
    {window.open("https://www.facebook.com/sharer/sharer.php?u="+escape(window.location.href)+"&t="+document.title, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false; }

    function googleshareCurrentPage()
    {window.open("https://plus.google.com/share?url="+escape(window.location.href)+'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false; }

    /* function twittershareCurrentPage()
    {window.open("https://twitter.com/share?url="+escape(window.location.href)+"&text="+document.title+"&via=chargeandmarch"+"&hashtags=chargenmarch", '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false; 

}*/
</script>

<script>
   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-70793562-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript">
function loadAddThisScript(url)
{
    // Adding the script tag to the head as suggested before
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = "//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56d29ae773d21773";

    // Fire the loading
    head.appendChild(script);
}
</script>

<!-- Loader Code -->
<div id="mask" class="DispNone"></div>
<div id="loader-wrapper" class="DispNone">
            <div class="logo"><a href="javascript:void(0)"><span>Charge</span>&March</a></div>
            <div id="loader">
            </div>
</div>

