  <div id="menuOverlay" class="DispNone"> </div>

  <c:if test="${empty(sessionScope.userbean) || (!empty(sessionScope.userbean) && sessionScope.userbean.isverified == 0)}">
    <nav id="menu">
      <ul class="footerlink" id="foot">
        <li><a class="mobile Hand">MOBILE</a></li>
        <li><a class="dth Hand">DTH</a></li>
        <li><a class="data Hand">DATA CARD</a></li>
<!--         <li><a href="#">BILL PAYMENT</a></li>
 -->      </ul>

         <!-- <ul class="actions vertical">
            <li><a  class="button singupopen" onclick="showSignInPopUp();">Sign Up</a></li>
            <li><a class="button loginPopUp" onclick="showLoginPopUp();">Log In</a></li>
        </ul> -->
      <a class="close" onclick="closeMenu();"></a>
    </nav>

</c:if>

<c:if test="${!empty(sessionScope.userbean) && sessionScope.userbean.isverified == 1}">
      <c:set var="user" value="${sessionScope.userbean}"/>
      <nav id="menu">
      <div id='cssmenu'>
             <div style="color:black;margin-bottom:20px"> 
                 <span style="font-size: 24px;color:black;">${user.name}</span><br>
                 <span style="color:black;">${user.emailId}</span><br><br>
                <%--  <span style="font-size: 18px;color:black;">Referral Code : ${user.referralCode}</span><br><br>
                 <span style="font-size: 18px;color:black;">Referral Amount : ${user.referralBalance}</span><br><br> --%>
                 <span style="color:black;font-size: 18px;margin-top: 12px;display: block;border-top: 1px solid #ccc;border-bottom: 1px solid #ccc;padding-top: 13px;padding-bottom: 13px;">Wallet Balance: ${user.walletBalance}</span>
             </div>
              <ul>
                      <li class='has-sub Hand'><a  >Account</a>
                        <ul>
                          <li class='last'><a href='${applicationScope["baseUrl"]}/user/profile'>Profile</a></li>
                          <li class='last'><a href='${applicationScope["baseUrl"]}/user/profile'>Setting</a></li>
                          <li class='last'><a href='${applicationScope["baseUrl"]}/user/profile'>My Transactions</a></li>
                          <li class='last'><a href='${applicationScope["baseUrl"]}/user/logout.htm'>Logout</a></li>
                        </ul>
                      </li>
              </ul>
            </div>
        <a onclick="closeMenu();" class="close"></a>
      </nav>
</c:if>
