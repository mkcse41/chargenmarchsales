<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>ChargenMarch - Recharge Mobile, DTH and DataCard, Fast & Free Recharge, Rewards | ChargenMarch.com</title>
	<meta name="keywords" content="Online recharge, Mobile recharge, Prepaid Recharge, Online mobile recharge, Datacard Recharge"/>
	<meta name="Description" content="ChargenMarch.com - Simple way to Recharge Prepaid Mobile, DataCard, DTH and keep on Marching forward. Get rewards with every Online Recharge on ChargenMarch. Store money in Wallet on ChargenMarch"/>
	<meta name="fragment" content="!">
	<meta name="google-site-verification" content="-o7OyH8W7bswsEjg9P2kHGskRYeaq56ZmWh7PBudP_A" />
	<meta property="og:title" content="ChargenMarch - Recharge Mobile, DTH and DataCard, Fast & Free Recharge, Rewards | ChargenMarch.com" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="https://chargenmarch.com" />
	<meta property="og:description" content="ChargenMarch.com - Simple way to Recharge all Prepaid Mobile, DataCard, DTH and keep on Marching forward. Rewards with Recharge. " />
<c:set var="now" value="<%=new java.util.Date()%>" />
<%@include file="jsp-includes/css-javascript-include-other.jsp" %>
<link type="text/css" href="${applicationScope['baseUrl']}/resources/css/cam_saleslogin.css" rel="stylesheet" />
<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="${applicationScope['baseUrl']}/resources/images/png" rel="shortcut icon" />
</head>
<body >
<c:set var="pageName" value="Cricketleague"/>
<!-- Header Start -->
<%@include file="jsp-includes/cam_header.jsp" %>
<!-- Header End -->
<!--  Login and register form  -->


  <section>
 
<div class="inner mid">
        <div class="ResetPassWordDiv" id="resetPassword">
              <p class="welcome" style="margin:0px;padding:15px 15px 0px 15px">Welcome back! Enter your password to login!</p>
              <form name="loginform" method="post">
                <span class="input input--hoshi" style="max-width:520px;height:50px;top:0px" >
                  <input class="input__field input__field--hoshi" type="email" style="padding-left:10px;border-top:none;border-left:none;border-right:none;color:#000;font-weight:300;width:95%;line-height:1.5em" id="chargers_emailId" name="chargers_emailId" value="" placeholder="Email Id" onkeypress="return spaceNotAllowed(event)"/>
                </span>
                <span class="input input--hoshi" style="max-width:520px;height:50px;top:0px" >
                  <input class="input__field input__field--hoshi" type="password" style="padding-left:10px;border-top:none;border-left:none;border-right:none;color:#000;font-weight:300;width:95%;line-height:1.5em" id="chargers_password" name="chargers_password" value="" placeholder="Password"/>
                </span>
                <input type="button" value="Sign in" onclick="return userLogin('chargers_emailId','chargers_password','errorMessagelogin')"/>
              </form>
            </div>
 </div>
</section>
 <!-- Failre Code End -->
<!-- Footer Section Start -->
<%@include file="jsp-includes/cam_footer.jsp" %>
<!-- Footer Section End -->
<!-- Menu Bar Code Start -->
<%@include file="jsp-includes/cam_menu_script.jsp" %>
<%@include file="jsp-includes/cam_menu.jsp" %>

<!-- Menu Bar Section End -->
</body>
</html>
