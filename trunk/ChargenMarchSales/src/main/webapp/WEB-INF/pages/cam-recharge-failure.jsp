<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<compress:html removeComments="true" removeIntertagSpaces="true"><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<c:if test="${paymentStatus!='success' && paymentStatus!='paymentsuccess'}">
  <title>Payment Failed | ChargenMarch - Recharge Mobile, DTH, DataCard and Landline, Fast Recharge, Rewards</title>
  <meta name="Description" content="ChargenMarch.com - Your Payment Of Rs. ${rechargeDTO.amount}/- failed."/>
</c:if>
<c:if test="${paymentStatus=='success'}">
  <title>Recharge Failed | ChargenMarch - Recharge Mobile, DTH, DataCard and Landline, Fast Recharge, Rewards</title>
  <meta name="Description" content="ChargenMarch.com - Your Recharge Of Rs. ${rechargeDTO.amount}/- failed."/>
</c:if>
<c:if test="${paymentStatus=='paymentsuccess'}">
  <title>Recharge Failed | ChargenMarch - Recharge Mobile, DTH, DataCard and Landline, Fast Recharge, Rewards</title>
  <meta name="Description" content="ChargenMarch.com - Your Recharge Of Rs. ${rechargeDTO.amount}/- failed."/>
</c:if>
  <meta name="fragment" content="!">
  <meta name="keywords" content="Online recharge, Successful Recharge, Online mobile recharge, Rewards, Coupons"/>
  
  <meta name="google-site-verification" content="-o7OyH8W7bswsEjg9P2kHGskRYeaq56ZmWh7PBudP_A" />
  <meta property="og:title" content="ChargenMarch - Recharge Mobile, DTH and DataCard, Fast & Free Recharge, Rewards | ChargenMarch.com" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="https://chargenmarch.com" />
  <meta property="og:description" content="ChargenMarch.com - Simple way to Recharge all Prepaid Mobile, DataCard, DTH and keep on Marching forward. Rewards with Recharge. " />
<%@include file="jsp-includes/css-javascript-include-other.jsp" %>
  <link type="text/css" href="${applicationScope['baseUrl']}/resources/css/cam_payment_failure.css" rel="stylesheet" />

<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="${applicationScope['baseUrl']}/resources/images/png" rel="shortcut icon" />
</head>
<body >
<!-- Header Start -->
<%@include file="jsp-includes/cam_header.jsp" %>
<!-- Header End -->
<!--  Login and register form  -->
<%@include file="jsp-includes/chargers_login.jsp" %>

<div style="text-align: center;padding: 83px;">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Recharge Page -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-6260839974232701"
     data-ad-slot="6874829873"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>

 <!-- Failre Code Start -->
  <section class="failureBackground" style="margin-top:-50px;margin-bottom:90px">
 
<div class="inner mid">
<c:if test="${paymentStatus!='success' && paymentStatus!='paymentsuccess'}">
       <div class="container">
       <div class="row">
       <div class="col-md-1">&nbsp;</div>
       <div class="col-md-10"><div class="errormsg"><img src="${applicationScope['baseUrl']}/resources/images/test-fail-icon.png" /> <span>Oops !! Your Payment Of Rs. ${rechargeDTO.amount}/- failed.</span></div></div>
       <div class="col-md-1">&nbsp;</div>
       </div>
       <div class="row">
       <div class="col-md-12 sorrymsg"> <span>Oops !!</span> Transaction has failed. Please try again with a fresh recharge.</div>
       </div>
       <div class="row">
       <div class="col-md-1">&nbsp;</div>
       <div class="col-md-7 orderhistory">
       <table width="100%">
       <tr>
       <th>Mobile No.</th>
        <th>Operator</th>
       <th>Recharge Amount</th>
       <th>Payment Order Id</th>
       </tr>
       <tr>
       <td> <c:if test="${rechargeDTO.serviceType != 'LandLineBill'}">${rechargeDTO.number}</c:if><c:if test="${rechargeDTO.serviceType == 'LandLineBill'}">${rechargeDTO.stdcode}${rechargeDTO.landlinenumber}</c:if></td>
       <td>${rechargeDTO.operatorName}</td>
       <td>Rs.${rechargeDTO.amount}/-</td>
       <td>${rechargeDTO.orderId}</td>
       </tr>
       </table>
       </div>
       <div class="col-md-4 button1"><a href="${applicationScope['baseUrl']}" >Click Here To Retry</a></div>
       </div>
       <div class="row">
        <div class="col-md-12"><div class="msg">In case of unsuccesful recharge,  money deducted from your account will be added to CAMlet.</div></div>
       </div>
       <div class="line">&nbsp;</div>
        <div class="row"> <div class="col-md-12">
       <div class="help failureText"><a href="javascript:void()">Need some help?</a> We are more than happy to help you!</div>
       <div class="help failureText">Write an email at care@chargenmarch.com</div>
       </div></div>
       </div>
 </c:if>
 </div>
 <c:if test="${paymentStatus=='success'}">
       <div class="container" style="height:636px">
       <div class="row">
       <div class="col-md-1">&nbsp;</div>
       <div class="col-md-10"><div class="errormsg"><img src="${applicationScope['baseUrl']}/resources/images/test-fail-icon.png" /> <span>Oops !! Your Recharge Of Rs. ${rechargeDTO.amount}/- failed.</span></div></div>
       <div class="col-md-1">&nbsp;</div>
       </div>
       <div class="row">
       <div class="col-md-12 sorrymsg">${rechargeDTO.error}</div>
       <div class="col-md-12 sorrymsg">Transaction of Rs. <b>${rechargeDTO.amount}</b> is successful. But due to some technical issues with the operator<c:if test="${!empty(rechargeDTO.error)}"><span style="font-size:18px;color:black">(${rechargeDTO.error})</span></c:if>, your recharge was not successful. Amount of Rs. <b>${rechargeDTO.amount}</b> has been added to the wallet (CAMllet). Please try recharging directly from the amount stored in wallet again after a while. Sorry for the inconvenience caused.</div>
       </div>
       <div class="row">
       <div class="col-md-1">&nbsp;</div>
       <div class="col-md-7 orderhistory">
       <table width="100%">
       <tr>
       <th>Mobile No.</th>
        <th>Operator</th>
       <th>Recharge Amount</th>
       <th>Payment Order Id</th>
       </tr>
       <tr>
       <td> <c:if test="${rechargeDTO.serviceType != 'LandLineBill'}">${rechargeDTO.number}</c:if><c:if test="${rechargeDTO.serviceType == 'LandLineBill'}">${rechargeDTO.stdcode}${rechargeDTO.landlinenumber}</c:if></td>
       <td>${rechargeDTO.operatorName}</td>
       <td>Rs.${rechargeDTO.amount}/-</td>
       <td>${rechargeDTO.orderId}</td>
       </tr>
       </table>
       </div>
       <div class="col-md-4 button1"><a href="${applicationScope['baseUrl']}" >Click Here To Retry</a></div>
       </div>
       <div class="row">
        <div class="col-md-12"><div class="msg">In case of unsuccesful recharge,  money deducted from your account will be added to CAMlet.</div></div>
       </div>
       <div class="line">&nbsp;</div>
        <div class="row"> <div class="col-md-12">
       <div class="help failureText"><a href="javascript:void()">Need some help?</a> We are more than happy to help you!</div>
       <div class="help failureText">Write an email at care@chargenmarch.com</div>
       </div></div>
       </div>
 </c:if>
  <c:if test="${paymentStatus=='paymentsuccess'}">
       <div class="container" style="height:636px">
       <div class="row">
       <div class="col-md-1">&nbsp;</div>
       <div class="col-md-10"><div class="errormsg"><img src="${applicationScope['baseUrl']}/resources/images/test-fail-icon.png" /> <span>Oops !! Your Recharge Of Rs. ${rechargeDTO.amount}/- failed.</span></div></div>
       <div class="col-md-1">&nbsp;</div>
       </div>
       <div class="row">
       <div class="col-md-12 sorrymsg">Due to some technical issues with the operator<c:if test="${!empty(rechargeDTO.error)}"><span style="font-size:18px;color:black">(${rechargeDTO.error})</span></c:if>, your recharge was not successful. Please try after a while. Sorry for the inconvenience caused.</div>
       </div>
       <div class="row">
       <div class="col-md-1">&nbsp;</div>
       <div class="col-md-7 orderhistory">
       <table width="100%">
       <tr>
       <th>Mobile No.</th>
        <th>Operator</th>
       <th>Recharge Amount</th>
       <th>Payment Order Id</th>
       </tr>
       <tr>
       <td> <c:if test="${rechargeDTO.serviceType != 'LandLineBill'}">${rechargeDTO.number}</c:if><c:if test="${rechargeDTO.serviceType == 'LandLineBill'}">${rechargeDTO.stdcode}${rechargeDTO.landlinenumber}</c:if></td>
        <td>${rechargeDTO.operatorName}</td>
       <td>Rs.${rechargeDTO.amount}/-</td>
       <td>${rechargeDTO.orderId}</td>
       </tr>
       </table>
       </div>
       <div class="col-md-4 button1"><a href="${applicationScope['baseUrl']}" >Click Here To Retry</a></div>
       </div>
       <div class="row">
        <div class="col-md-12"><div class="msg">In case of unsuccesful recharge,  money deducted from your account will be added to CAMlet.</div></div>
       </div>
       <div class="line">&nbsp;</div>
        <div class="row"> <div class="col-md-12">
       <div class="help failureText"><a href="javascript:void()">Need some help?</a> We are more than happy to help you!</div>
       <div class="help failureText">Write an email at care@chargenmarch.com</div>
       </div></div>
       </div>
 </c:if>
 </div>
</section>
 <!-- Failre Code End -->
<!-- Footer Section Start -->
<%@include file="jsp-includes/cam_footer.jsp" %>
<!-- Footer Section End -->
<!-- Menu Bar Code Start -->
<%@include file="jsp-includes/cam_menu_script.jsp" %>
<%@include file="jsp-includes/cam_menu.jsp" %>

<!-- Menu Bar Section End -->
</body>
</html>
</compress:html>