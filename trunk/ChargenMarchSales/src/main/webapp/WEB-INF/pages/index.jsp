<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<compress:html removeComments="true" removeIntertagSpaces="true">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="msvalidate.01" content="4C0DE38ADCA26A9DED45D4FF7E9AAAA3" />
<title>ChargenMarch - Recharge Mobile, DTH, DataCard and Landline, Fast Recharge, Rewards</title>
<meta name="keywords" content="Online recharge, Free Recharge, Prepaid Recharge, Online mobile recharge, Datacard Recharge"/>
<meta name="Description" content="ChargenMarch.com-Simple way to recharge Mobile, DataCard, DTH, pay landline bills and keep on Marching forward. Get rewards with every payment on ChargenMarch"/>
<meta name="fragment" content="!" />
<meta name="google-site-verification" content="-o7OyH8W7bswsEjg9P2kHGskRYeaq56ZmWh7PBudP_A" />
<meta property="og:title" content="ChargenMarch - Recharge Mobile, DTH and DataCard, Fast & Free Recharge, Rewards | ChargenMarch.com" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://chargenmarch.com" />
<meta property="og:description" content="ChargenMarch.com - Simple way to Recharge all Prepaid Mobile, DataCard, DTH and keep on Marching forward. Rewards with Recharge. " />
<meta name="h12-site-verification" content="0ac51fbcf3609395f435cab1ca495b7d"/>
<%@include file="jsp-includes/css-javascript-include-home.jsp" %>
<script src="${applicationScope['baseUrl']}/resources/javascript/document_home.js" onload="loadAddThisScript();" defer></script> 
<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="images/png" rel="shortcut icon" />
</head>
<body>
<!-- Header Start -->
<%@include file="jsp-includes/cam_header.jsp" %>
<!-- Header End -->

<!--  Login and register form  -->
<%@include file="jsp-includes/chargers_login.jsp" %>
<!-- Mobile DTH,Datacard box -->
<%@include file="jsp-includes/charger_box.jsp" %>
<!-- Our Profile Code Start-->

<section class="wrapper profile" style="background:rgba(234, 234, 234, 0.17)">
  <div class="inner">
    <div class="content">
      <h1>What is ChargenMarch?!</h1><hr />
      <p style="text-align: justify">Looking for hassle free online payment spot? ChargenMarch is the perfect place for you. We, at CAM, offer seamless experience in online recharges. With us, you can go for mobile recharge, DTH recharge&amp; Datacard recharge for all the operators available within less than 2 minutes. Apart from this, we also provide you the option to load up the money &amp; forget using the cards with CAM Wallet a.k.a CAMllet. </p>
	  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- WEB Part Ads -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:50px"
     data-ad-client="ca-pub-6260839974232701"
     data-ad-slot="7292449076"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
    </div>

    <div class="home-circles">
      <div class="circle_images"><span class="image"><img src="${applicationScope['baseUrl']}/resources/images/dth_icon.png" alt="ChargenMarch DTH Icon" width="113" height="114"></span><span class="image"><img src="${applicationScope['baseUrl']}/resources/images/data.png" alt="ChargenMarch Datacard Icon" width="121" height="121"></span></div>
      <div class="circle_images"><span class="image"><img src="${applicationScope['baseUrl']}/resources/images/mobile_icon.png" alt="ChargenMarch Mobile Icon" width="114" height="114"></span><span class="image"><img src="${applicationScope['baseUrl']}/resources/images/dth_icon.png" alt="ChargenMarch DTH Icon" width="113" height="114"></span></div>
    </div>
  
  </div>
</section>

<!-- DreamsCareIndia -->
<%--
<section class="wrapper profile">
  <div class="inner">
  <div class="home-circles" onclick="findTarget('http://dreamscareindia.org/')">
      <div class="circle_images"><span class="image"><img src="${applicationScope['baseUrl']}/resources/images/dreamscareindia.jpg" alt="ChargenMarch DTH Icon" width="800" height="271" style="width:700px;height:271px;margin-left:-342px;margin-top:34px"></span></div>
     
    </div>
    <div class="content" onclick="findTarget('http://dreamscareindia.org')" style="margin-right: 0em;">
      <h1 style="font-size: 23px;margin-top: -69px;" >Dreams Care India</h1>
      <p style="text-align: justify">ChargenMarch is proud to have an association with Dreams Care India. With this, we have taken a step forward towards a better world with equal opportunities for everyone. Dreams Care India is a registered Non-Government &amp;Not-For-Profit Charitable Organization, established by a group of committed social workers and volunteers in 2015 registered under Public Charitable Trust. The organization works for Health &amp; Hygiene, Sanitation, Environment, Educational, Rural development, Skill development/Vocational Training, Women empowerment and etc. various programs for needy peoples in the community. </p>  
      <p style="text-align: justify">Dreams Care India continues making efforts for the betterment of marginalized, deprived, vulnerable, ignored section of society by giving them a platform through its creative programs and activities. It believes in the dignity of people and their capacity to overcome their problems; it words with the poorest and the most vulnerable in their struggle against poverty, sufferings, and injustice. </p>
    </div>
  </div>
</section>
--%>
<!-- Our Profile Code End -->
<!-- Reward Points Section Start -->
<section id="two" class="wrapper style2">
  <div class="inner">
    <div class="textcenter">
      <h2>Why ChargenMarch?</h2>
      <p>We assure you of complete security on chargeNmarch</p>
    </div>
    <div class="features">
      <section><span class="icon major fa-envelope-o"></span>
        <h3>Loyalty Rewards Points</h3>
        <p>ChargenMarch rewards the chargers with 1 loyalty point with every successfull transaction worth &#8377;10. These points can later be redeemed to pay in future transactions.</p>
      </section>
      <section><span class="icon major fa-ticket"></span>
        <h3>We Love to Give Back</h3>
        <p>At chargenmarch, the more You Charge, the more You Get. CAM offers you 1% cashback on every 20th recharge you carry out with us. The cashback amount is added to your CAM Wallet a.k.a CAMllet. This amount is calculated from last 10 online payments only. </p>
      </section>
      <section><span class="icon major fa-cloud"></span>
        <h3>128 Bit-SSL Secured</h3>
        <p>We take care of your security. Chargenmarch connects the application with the browser with 128-Bit encryption. This means all the transactions carried out with us are safe &amp; secure.</p>
      </section>
    </div>
  </div>
</section>
<!-- Rewards Points Section End -->
<!-- Footer Section Start -->
<%@include file="jsp-includes/cam_footer.jsp" %>
<!-- Footer Section End -->
<!-- Menu Bar Code Start -->
<%@include file="jsp-includes/cam_menu_script.jsp" %>
<%@include file="jsp-includes/cam_menu.jsp" %>

<!-- Recharge details use after Login Details -->
<input type="hidden" id="number">
<input type="hidden" id="operatorValue">
<input type="hidden" id="mobileService">
<input type="hidden" id="amount">
<input type="hidden" id="service">
<input type="hidden" id="operatorText">
<input type="hidden" id="circleValue">
<input type="hidden" id="circleText">
<input type="hidden" id="isMobilePlan" value="false">

<!-- Recharge details use after Login Details for landline bill-->
<input type="hidden" id="landlineaccountno">
<input type="hidden" id="landlinestdcode">
<input type="hidden" id="landlinenumberwithoutstd">

<!-- End -->

<!-- Menu Bar Section End -->
</body>
</html>
</compress:html>