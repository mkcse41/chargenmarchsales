<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>CAM League | Recharge Mobile, DTH and DataCard | Chargenmarch.com</title>
<c:set var="now" value="<%=new java.util.Date()%>" />
<%@include file="jsp-includes/css-javascript-include-other.jsp" %>
<link type="text/css" href="${applicationScope['baseUrl']}/resources/css/leagueQuestions.css" rel="stylesheet" />
<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="${applicationScope['baseUrl']}/resources/images/png" rel="shortcut icon" />
<c:if test="${empty(userbean) || (!empty(sessionScope.userbean) && sessionScope.userbean.isverified == 0)}">
  <input type="hidden" value="true" id="checkUserLogin"/>
</c:if>
<c:if test="${!empty(userbean)  && sessionScope.userbean.isverified == 1}">
   <input type="hidden" value="false" id="checkUserLogin"/>
</c:if>
<c:set var="pageName" value="Cricketleague"/>
</head>
<body >
<!-- Header Start -->
<%@include file="jsp-includes/cam_header.jsp" %>
<!-- Header End -->
<!--  Login and register form  -->
<%@include file="jsp-includes/chargers_login.jsp" %>

<div class="mainOuterDiv">

   <div class="inner midtnc">

   <div id="questionAnswerDiv" style="margin-top: 43px;font-size: 19px;">
     <c:if test="${status=='success'}">
           You've joined the CAM Cricket league for the ${matchname} match. Win the prizes and boast your skills in your gang! <br/>
            <c:forEach var="cricketLeaguesResultDtoObj" items="${cricketLeaguesResultDto.cricketLeaguesDtoList}">
                ${cricketLeaguesResultDtoObj.name} <br/>
            </c:forEach>
            <input type="button" class="submitButton" tabindex="11" value="CAM Cricket League" onclick="findTarget('${applicationScope['baseUrl']}/league/cricket/cam_cricket_leagues.htm');">         
     </c:if>

     <c:if test="${status!='success'}">
       <span>Oops !!</span> Transaction has failed. Please try again.
       <div class="row" style="margin-top: 35px;font-size: 15px;padding: 10px;color: #008000;">
              <div class="col-md-12"><div class="msg" style="float: right;">In case of unsuccesful recharge,  money deducted from your account will be added to CAMlet.</div></div>
            </div>

        <input type="button" class="submitButton" tabindex="11" value="CAM Cricket League" onclick="findTarget('${applicationScope['baseUrl']}/league/cricket/cam_cricket_leagues.htm');">

     </c:if>
   </div>
</div>

<div  style="font-family: sans-serif;font-family: sans-serif;display: inline-block;background: #fff;margin: 6%;width: 86%;padding: 5px;"><a href="javascript:void()">Need some help?</a> We are more than happy to help you!
              Write an email at <a href="mailto:care@chargenmarch.com" title="care@chargenmarch.com">care@chargenmarch.com</a>    
<input type="button" value="Recharge Now" class="bluBtn" style="box-shadow: 3px 4px 2px #aaa;margin-top:20px;" onclick="findTarget('${applicationScope['baseUrl']}')">
              </div>
</div>

 <!-- Failre Code End -->
<!-- Footer Section Start -->
<%@include file="jsp-includes/cam_footer.jsp" %>
<!-- Footer Section End -->
<!-- Menu Bar Code Start -->
<%@include file="jsp-includes/cam_menu_script.jsp" %>
<%@include file="jsp-includes/cam_menu.jsp" %>
<!-- Menu Bar Section End -->
</body>

</html>