<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<form action="addedtypes" method="post">
<c:forEach var="types" items="${typeList}">
${types.key} <br /><hr />
	<c:forEach var="typeDesc" items="${types.value}">
		<input type="text" name="type" value="${typeDesc.type}" readonly="true" />
		<select name="typeLocal">
			<c:forEach var="typeselect" items="${mobilePlanTypes}">
				<option name="typesoption" value="${typeselect}" <c:if test="${typeDesc.type_local==typeselect}">selected</c:if>>${typeselect}</option>
			</c:forEach>
		</select>
		<br />
	</c:forEach>
<br/>
</c:forEach>
<input type="submit"/>
</form>
</body>
</html>