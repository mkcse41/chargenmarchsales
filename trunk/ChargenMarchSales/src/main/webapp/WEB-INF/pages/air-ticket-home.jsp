<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<compress:html removeComments="true" removeIntertagSpaces="true">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="images/png" rel="shortcut icon" />
<meta name="msvalidate.01" content="4C0DE38ADCA26A9DED45D4FF7E9AAAA3" />
<title>ChargenMarch - Recharge Mobile, DTH, DataCard and Landline, Fast Recharge, Rewards</title>
<meta name="keywords" content="Online recharge, Free Recharge, Prepaid Recharge, Online mobile recharge, Datacard Recharge"/>
<meta name="Description" content="ChargenMarch.com-Simple way to recharge Mobile, DataCard, DTH, pay landline bills and keep on Marching forward. Get rewards with every payment on ChargenMarch"/>
<meta name="fragment" content="!" />
<meta name="google-site-verification" content="-o7OyH8W7bswsEjg9P2kHGskRYeaq56ZmWh7PBudP_A" />
<meta property="og:title" content="ChargenMarch - Recharge Mobile, DTH and DataCard, Fast & Free Recharge, Rewards | ChargenMarch.com" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://chargenmarch.com" />
<meta property="og:description" content="ChargenMarch.com - Simple way to Recharge all Prepaid Mobile, DataCard, DTH and keep on Marching forward. Rewards with Recharge. " />
<%@include file="jsp-includes/css-javascript-include-airticket.jsp" %>
</head>
<body>
<!-- Header Start -->
<!-- Header End -->
<!--  Login and register form  -->
<%@include file="jsp-includes/chargers_login.jsp" %>

<section id="homepageslider">
<%@include file="jsp-includes/flightBookingBox.jsp" %>
</section>

<section class="wrapper book">
  <div class="container">
    <div class="row">
      <div class="col-md-12 air_heading">
        <h2>Why Book With Us?<span class="col1">Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac, lorem</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="prop">
          <div class="maxheight" style="height: 96px;">
            <div class="box_inner">
              <div class="fa fa-trophy"></div>
              <div class="title">Best Price Guarantee</div>
              <p>Find our lowest price to destinations worldwide, guaranteed</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="prop pp1">
          <div class="maxheight" style="height: 96px;">
            <div class="box_inner">
              <div class="fa fa-book"></div>
              <div class="title">Easy Booking</div>
              <p>Search, select and save - the fastest way to book your trip</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="prop pp2">
          <div class="maxheight" style="height: 96px;">
            <div class="box_inner">
              <div class="fa fa-comments"></div>
              <div class="title">24/7 Customer Care</div>
              <p>Get booking assistance and special deals by calling +1 800 889 9898 </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="about">
  <div class="f_top">
    <div class="container" style="width:90%;margin:auto">
      <div class="row">
        <div class="col-md-4" align="justify">
          <h4>about </h4>
          Looking for hassle free online payment spot? ChargenMarch is the perfect place for you. We, at CAM, offer seamless experience in online recharges. With us, you can go for mobile recharge, DTH recharge& Datacard recharge for all the operators available within less than 2 minutes. Apart from this, we also provide you the option to load up the money & forget using the cards with CAM Wallet a.k.a CAMllet. </div>
        <div class="col-md-4">
          <h4>travel news</h4>
          <img src="assets/images/f_img1.jpg" alt="">
          <div class="extra_wrapper"> <strong class="col2"><a href="#">Phasellus porta. </a></strong> <br>
            Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes. <br>
            <time class="col4" datetime="2014-01-01">25 April. 2014</time>
          </div>
          <div class="clear cl2"></div>
          <img src="assets/images/f_img1.jpg" alt="">
          <div class="extra_wrapper"> <strong class="col2"><a href="#">Phasellus porta. </a></strong> <br>
            Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes. <br>
            <time class="col4" datetime="2014-01-01">25 April. 2014</time>
          </div>
        </div>
        <div class="col-md-4">
          <h4>locations</h4>
          <address>
          8901 Marmora Road, Glasgow, D04 89GR.
          <div class="col4">+1 800 559 6580</div>
          </address>
          <div class="socials"> <a href="#" class="fa fa-facebook"></a> <a href="#" class="fa fa-google-plus"></a> <a href="#" class="fa fa-rss"></a> <a href="#" class="fa fa-pinterest"></a> <a href="#" class="fa fa-linkedin"></a> </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Footer Section Start -->
<%@include file="jsp-includes/cam_footer.jsp" %>
<!-- Footer Section End -->
<!-- Menu Bar Code Start --> 
</body>
</html>
</compress:html>