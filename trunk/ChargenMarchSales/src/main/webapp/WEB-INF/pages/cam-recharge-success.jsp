<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<compress:html removeComments="true" removeIntertagSpaces="true"><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Successful Payment | Recharge Mobile, DTH, DataCard and Landline, Fast Recharge, Rewards</title>
	<meta name="keywords" content="Online recharge, Successful Recharge, Online mobile recharge, Rewards, Coupons"/>
	<meta name="Description" content="ChargenMarch.com - Your Recharge Of Rs. ${rechargeDTO.amount}/- is successful. Please proceed with next recharge"/>
	<meta name="fragment" content="!">
	<meta name="google-site-verification" content="-o7OyH8W7bswsEjg9P2kHGskRYeaq56ZmWh7PBudP_A" />
	<meta property="og:title" content="ChargenMarch - Recharge Mobile, DTH and DataCard, Fast & Free Recharge, Rewards | ChargenMarch.com" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="https://chargenmarch.com" />
	<meta property="og:description" content="ChargenMarch.com - Simple way to Recharge all Prepaid Mobile, DataCard, DTH and keep on Marching forward. Rewards with Recharge. " />
<c:set var="now" value="<%=new java.util.Date()%>" />
<%@include file="jsp-includes/css-javascript-include-other.jsp" %>
<link type="text/css" href="${applicationScope['baseUrl']}/resources/css/cam_payment_failure.css" rel="stylesheet" />
<link type="text/css" href="${applicationScope['baseUrl']}/resources/css/cam_payment_success.css" rel="stylesheet" />
<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="${applicationScope['baseUrl']}/resources/images/png" rel="shortcut icon" />
</head>
<body >
<!-- Header Start -->
<%@include file="jsp-includes/cam_header.jsp" %>
<!-- Header End -->
<!--  Login and register form  -->
<%@include file="jsp-includes/chargers_login.jsp" %>


<div style="text-align: center;padding: 83px;">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Recharge Page -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-6260839974232701"
     data-ad-slot="6874829873"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>

<section style="margin-top:-130px;height:512px">
 
<div class="inner mid">

 <div class="container">
 <div class="row">
 <div class="col-md-1">&nbsp;</div>
 <div class="col-md-10"><div class="successmsg">
 <table  style="margin:0px;" align="center"><tr style="border:none;background: rgba(243, 243, 247,0.4) !important;"><td align="right" style="width:22%;"><img src="${applicationScope['baseUrl']}/resources/images/tick.png" class="successTick" /></td><td style="text-align:justify;width:70%;padding:0px"> <span style="vertical-align:top">Your ${rechargeDTO.serviceType} recharge for Rs. ${rechargeDTO.amount} is done</span></td></tr></table></div></div>
 <div class="col-md-1">&nbsp;</div>
 </div>
 <div class="row"><div class="col-md-12">&nbsp;</div></div>
 <div class="row">
 <div class="col-md-12" style="background:#fff;padding:0px;margin-top:-63px">
<!-- Success Table -->
<div class="inner successtable">
 <div class="calculation">
 
 
 <div class="mobo">
   <table>
   <tr>
   <td rowspan="2" width="80px"><!-- <img src="${applicationScope['baseUrl']}/resources/images/vd.jpg" /> --></td>
   <td><span class="call">
  <c:if test="${rechargeDTO.serviceType != 'LandLineBill'}">
   ${rechargeDTO.number}</c:if>
   <c:if test="${rechargeDTO.serviceType == 'LandLineBill'}">
   ${rechargeDTO.stdcode}${rechargeDTO.landlinenumber}</c:if></span></td>
   </tr>
   <tr><tD><span class="subcall"> ${rechargeDTO.operatorName}</span></tD></tr>
   </table>
 </div>
 <div style="width:50px;float:left"><img src="${applicationScope['baseUrl']}/resources/images/plus.png" class="plusSuccess" /></div>
 <div class="mobo">
   <h4 style="font-weight:900;font-size:24px"><c:if test="${empty(rechargeDTO.couponName)}">0</c:if><c:if test="${!empty(rechargeDTO.couponName)}">${rechargeDTO.couponName}</c:if><c:if test="${rechargeDTO.couponValue>0}">-Rs. ${rechargeDTO.couponValue}</c:if></h4>
   <h6 style="margin-top: -23px;">COUPONS</h6>
 </div>
 <div style="width:50px;float:left"><img src="${applicationScope['baseUrl']}/resources/images/equal.jpg" class="equalSuccess" /></div>
 <div class="mobo">
   <h4 class="camAmount">Rs. ${rechargeDTO.amount}</h4>
   <!-- <h6>INVOICE</h6> -->
 </div>

</div>

<div class="calculation" style="padding-top:10px">
<p align="justify" class="successmsg1">Your recharge is done. You should get a confirmation SMS from your operator shortly. If you need any future assistance please write us at <span>care@chargenmarch.com</span></p>
</div>
<div class="calculation" style="border:none;margin-top:20px">
<table width="100%" class="lasttbl">
<tr>
<td style="text-align:left;padding-top:10px"><span class="btn plan" onclick="findTarget('${applicationScope['baseUrl']}')">DO ANOTHER RECHARGE</span></td>
<td style="text-align:right">
DATE : <span class="bluecolor"> <fmt:formatDate type="date" value="${now}" /></span><br />
ORDER ID : <span class="bluecolor">${rechargeDTO.orderId}</span>
</td>
</tr>
</table>
</div>
</div>
<!-- Success Table -->

 </div>
 </div>
 <div class="line">&nbsp;</div>
  <div class="row"> <div class="col-md-12" >
 <div class="help"><a href="javascript:void()">Need any kind of Help?</a> Then we are happy to help you!</div>
 <div class="help">Write an email at care@chargenmarch.com</div>
 </div></div>
 </div>
 </div>
</section>
 <!-- Failre Code End -->
<!-- Footer Section Start -->
<%@include file="jsp-includes/cam_footer.jsp" %>
<!-- Footer Section End -->
<!-- Menu Bar Code Start -->
<%@include file="jsp-includes/cam_menu_script.jsp" %>
<%@include file="jsp-includes/cam_menu.jsp" %>

<!-- Menu Bar Section End -->
</body>
</html>
</compress:html>