<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Contact Us | Recharge Mobile, DTH and DataCard | Chargenmarch.com</title>
<c:set var="now" value="<%=new java.util.Date()%>" />
<%@include file="jsp-includes/css-javascript-include-other.jsp" %>
<link type="text/css" href="${applicationScope['baseUrl']}/resources/css/cam_payment_failure.css" rel="stylesheet" />
<link type="text/css" href="${applicationScope['baseUrl']}/resources/css/cam_payment_success.css" rel="stylesheet" />
<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="${applicationScope['baseUrl']}/resources/images/png" rel="shortcut icon" />
</head>
<body >
<!-- Header Start -->
<%@include file="jsp-includes/cam_header.jsp" %>
<!-- Header End -->
<!--  Login and register form  -->
<%@include file="jsp-includes/chargers_login.jsp" %>

  <section>
 
<div class="inner mid">
        <div class="contactusDiv" id="contactus">
            <p class="welcome" style="margin:0px;padding:15px 15px 0px 15px;text-align:center">Contact Us</p>
              <div style="margin-top:10px">
                   <span style="margin-top:10px;color:#3fa7ba;font-size:30px">LifeSoft Technology</span><br/>
                   <span style="margin-top:10px;color:#000;font-size:16px"> 44,RG Ramawat, Shekhawati Nagar Extn., Near Govindam Tower,Govindpura Jaipur-302012  </span><br/>
                   <span style="margin-top:10px;color:#000;font-size:16px">care@chargenmarch.com</span><br/>
                   <!-- <span style="margin-top:10px;color:#000;font-size:16px">9785205444</span> -->
              <div>
        </div>
 </div>
</section>
 <!-- Failre Code End -->
<!-- Footer Section Start -->
<%@include file="jsp-includes/cam_footer.jsp" %>
<!-- Footer Section End -->
<!-- Menu Bar Code Start -->
<%@include file="jsp-includes/cam_menu_script.jsp" %>
<%@include file="jsp-includes/cam_menu.jsp" %>

<!-- Menu Bar Section End -->
</body>
<script type="text/javascript">
  function checkPasswords(passId, confPassId){
    var passvalue = $("#"+passId).val();
    var confpassvalue = $("#"+confPassId).val();
    if(passvalue == confpassvalue){
      return true;
    } else{
      alert("Passwords Do Not Match");
     return false;
   }
  }
</script>
</html>
