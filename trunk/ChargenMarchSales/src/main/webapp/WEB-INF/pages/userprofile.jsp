<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Welcome to ChargenMarch - Recharge Mobile, DTH and DataCard, Fast & Free Recharge, Rewards | ChargenMarch.com</title>
	<meta name="keywords" content="Online recharge, Mobile recharge, Prepaid Recharge, Online mobile recharge, Datacard Recharge"/>
	<meta name="Description" content="ChargenMarch.com - Simple way to Recharge Prepaid Mobile, DataCard, DTH and keep on Marching forward. Get rewards with every Online Recharge on ChargenMarch. Store money in Wallet on ChargenMarch"/>
	<meta name="fragment" content="!">
	<meta name="google-site-verification" content="-o7OyH8W7bswsEjg9P2kHGskRYeaq56ZmWh7PBudP_A" />
	<meta property="og:title" content="ChargenMarch - Recharge Mobile, DTH and DataCard, Fast & Free Recharge, Rewards | ChargenMarch.com" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="https://chargenmarch.com" />
	<meta property="og:description" content="ChargenMarch.com - Simple way to Recharge all Prepaid Mobile, DataCard, DTH and keep on Marching forward. Rewards with Recharge. " />
	<%@include file="jsp-includes/css-javascript-include-other.jsp" %>
	<link rel="stylesheet" href="${applicationScope['baseUrl']}/resources/css/userProfile.css" type="text/css">
	<link rel="stylesheet" href="${applicationScope['baseUrl']}/resources/css/jquery-ui.css" type="text/css">
	<script src="${applicationScope['baseUrl']}/resources/javascript/jquery-ui.js"></script>
    <script src="${applicationScope['baseUrl']}/resources/javascript/userprofile.js"></script>
	<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="images/png" rel="shortcut icon" />
</head>
<body>
	<!-- Header Start -->
	<%@include file="jsp-includes/cam_header.jsp" %>
	<!-- Header End -->
	<!--  Login and register form  -->
	<%@include file="jsp-includes/chargers_login.jsp" %>

 <section>
	<div class="inner">
		<div class="profilearea">
			<div class="col-md-3 tabpanel">
				<h3 align="center Hand">Setting Manager</h3>
				<ul>
					<li class="trans-tab Hand"><a >TRANSACTIONS</a></li>
					<!-- <li class="address-tab"><a href="#">ADDRESS BOOK</a></li> -->
					<li class="cash-tab Hand"><a >CASH RECORDS</a></li>
					<li class="profile-tab Hand"><a style="color:#219DC5">MANAGE PROFILE</a></li>
				    <li class="addfund-tab Hand"><a>Add Fund Request</a></li> 
				    <li class="fundsummary-tab Hand"><a >View Fund History</a></li> 
				    <li class="rechargemargin-tab Hand"><a >Recharge Margin</a></li> 
				</ul>

			</div>
			<div class="col-md-9 tabcontent">

				<!-- Transaction Tab Content Code Start -->
				<div class="trans-tabcontent contentpart">
					<div class="trans-heading Hand ">TRANSACTION HISTORY</div>
					<hr>
					<div style="margin-top:48px"></div>
					<div id="transactionHistory" style="overflow:auto;height:400px">
						<c:forEach var="transaction" items="${transactionList}">
							<table width="100%">
								<tr>
								   <c:if test="${transaction.isSuccesfulRecharge==1}">
									 	<td ><div><img src="${applicationScope['baseUrl']}/resources/images/tick.png" class="successTick" /></div></td> 
									</c:if>
									<c:if test="${transaction.isSuccesfulRecharge!=1}">
									 	<td ><div><img src="${applicationScope['baseUrl']}/resources/images/test-fail-icon.png" /></div></td> 
									</c:if>
									<td><joda:format value="${transaction.rechargeDate}" pattern="MMM dd yyyy"/></td>
									<c:if test="${transaction.serviceType!='Wallet'}">
									     <td>${transaction.number} (${transaction.operatorName})</td>
									</c:if>
									<c:if test="${transaction.serviceType=='Wallet'}">
									     <td>${transaction.orderId}(OrderId)</td>
									</c:if>
									<td>Rs. ${transaction.amount}<br>${transaction.serviceType}</td>
								</tr>
							</table>
						 <br>
						</c:forEach>
					</div>
				</div>
				<!-- Transaction Tab Content Code End -->


				<!-- Address Tab Content Code Start -->
				<div class="address-tabcontent contentpart">
					<div class="address-heading Hand">ADDRESS BOOK</div>
					<hr>

					<center><img src="<c:url value="/resources/images/Address-book-Icon.png" />" /></center>
					<br>
					<span class="msg">You don't have any addresses yet.</span><br>
					<div class="add"><a href="javascript:void(0)" class="addaddress">Add Address</a></div>

					<div class="addaddressform">
						<div class="chargenmarchlogin textcenter">
							<p class="welcome" style="margin:0px;padding:15px 15px 0px 15px">Welcome User! Share Your Address!</p>
							<form action="#" name="registerform">
								<span class="input input--hoshi" style="max-width:520px;height:50px;top:0px" >
									<input class="input__field input__field--hoshi" type="text" id="input-4" style="padding-left:10px;border-top:none;border-left:none;border-right:none;color:#000;font-weight:300;width:100%;line-height:1.5em" />
									<label class="input__label input__label--hoshi input__label--hoshi-color-1" for="input-4" style="margin-left:0px;">
										<span class="input__label-content input__label-content--hoshi" style="color:#000;font-weight:300">Address Name</span>
									</label>
								</span>
								<span class="input input--hoshi" style="max-width:520px;height:50px;top:0px" >
									<input class="input__field input__field--hoshi" type="text" id="input-4" style="padding-left:10px;border-top:none;border-left:none;border-right:none;color:#000;font-weight:300;width:100%;line-height:1.5em" />
									<label class="input__label input__label--hoshi input__label--hoshi-color-1" for="input-4" style="margin-left:0px;">
										<span class="input__label-content input__label-content--hoshi" style="color:#000;font-weight:300">Address Line1</span>
									</label>
								</span>
								<span class="input input--hoshi" style="max-width:520px;height:50px;top:0px" >
									<input class="input__field input__field--hoshi" type="text" id="input-4" style="padding-left:10px;border-top:none;border-left:none;border-right:none;color:#000;font-weight:300;width:100%;line-height:1.5em" />
									<label class="input__label input__label--hoshi input__label--hoshi-color-1" for="input-4" style="margin-left:0px;">
										<span class="input__label-content input__label-content--hoshi" style="color:#000;font-weight:300">Address Line2</span>
									</label>
								</span>
								<input type="submit" value="Add Address" />
							</form>
						</div>
					</div>
				</div>
				<!-- Address Tab Content Code End -->


				<!-- Cash Tab Content Code Start -->
				<div class="cash-tabcontent contentpart" style="text-align:justify">
					<div class="cash-heading Hand">CASH RECORDS</div>
					<hr>
					<div style="margin-top:33px;color:#5a5555">BALANCE : RS. ${user.walletBalance}</div>
					<br>
                    <div>
							<span class="input input--hoshi" style="max-width:520px;height: 63px;top: 0px;margin-left:-20px" >
								<input class="input__field input__field--hoshi" type="text" id="loadAmount" style="padding-left:10px;border-top:none;border-left:none;border-right:none;color:#000;font-weight:300;width:50%;line-height: 50px;" name="loadAmount"   maxlength="4" placeholder="ADD MONEY"/>
							<input type="submit" value="ADD" onclick="return loadAmount('loadAmount');" style="margin-top: 28px;"/>
							</span>
							<span></span>
				    </div>
				</div>
				<!-- Cash Tab Content Code End -->


				<!-- Profile Tab Content Code Start -->
				<div class="profile-tabcontent contentpart" style="text-align:justify">
					<div class="profile-heading">MANAGE PROFILE</div>
					<hr>
					<b>${user.name}</b><br>
					${user.emailId}<br>
					+91-${user.mobileNo}<br>
					<%-- <span style="font-size: 18px;color:black;"> Referral Code : ${user.referralCode} </span> <br/> --%>
					<div class="setting"><a href="javascript:void(0)">Setting</a></div>
					<ul class="settingcontent" style="display: inline-block; width: 20%;">
						<!-- <li><a href="#" class="editprofile">Edit Profile</a></li> -->
						<li><a class="changepassword Hand">Change Password</a></li>
						<li><a class="Hand" href="${applicationScope['baseUrl']}/user/logout.htm">Logout</a></li>
					</ul>

					<!-- Edit Account -->
					<div class="accountsetting" id="changePasswordDiv">
						<div class="textcenter changepw" style="border: 1px solid;">
							<p style="color: #666;">Change Your Password</p>
							<form action="#" name="registerform" id="changePasswordform">
								 <span class="input input--hoshi" style="max-width:520px;height:71px;top:37px" >
						          <input class="input__field input__field--hoshi" type="password"  style="padding-left:10px;color:#fff;font-weight:300;width:92%;margin-left:30px;" id="oldPassword" name="oldPassword" value="" placeholder="Old Password"/>
						        </span>
								<span class="input input--hoshi" style="max-width:520px;height:71px;top:-3px" >
						          <input class="input__field input__field--hoshi" type="password"  style="padding-left:10px;color:#fff;font-weight:300;width:92%;margin-left:30px;" id="newPassword" name="newPassword" value="" placeholder="New Password"/>
						        </span>
                                <span class="input input--hoshi" style="max-width:520px;height:71px;top:-43px" >
						          <input class="input__field input__field--hoshi" type="password"  style="padding-left:10px;color:#fff;font-weight:300;width:92%;margin-left:30px;" id="confirmPassword" name="confirmPassword" value="" placeholder="Confirm Password"/>
						        </span>
								<input type="button" value="UPDATE" onclick="return updatePassword('oldPassword','newPassword','confirmPassword')" />
							</form>
						</div>
					</div>               
					<!-- Edit Account -->               
				</div>
				<!-- Profile Tab Content Code End -->
				<!-- Fund Tab Content Code start -->
                <div class="fundsummary-tabcontent contentpart">
					<div class="trans-heading Hand ">Fund HISTORY</div>
					<hr>
					<div style="margin-top:48px"></div>
					<table width="100%">
								<tr style="background: orange;color: black;">
									<td>Amount</td>
									<td>Payment Method</td>
									<td>Ref Id</td>
									<td>Status</td>
									<td>Date</td>
								</tr>
					</table>				
					<div id="transactionHistory" style="overflow:auto;height:400px">
						<c:forEach var="fundsRequest" items="${addFundsRequestList}">
							<table width="100%">
								<tr>
								    <td>${fundsRequest.amount}</td>
								    <td>${fundsRequest.paymentMethod}</td>
								    <td>${fundsRequest.bankReferenceId}</td>
								    <td>${fundsRequest.status}</td>
									<td><joda:format value="${fundsRequest.addDate}" pattern="MMM dd yyyy"/></td>
								</tr>
							</table>
						 <br>
						</c:forEach>
					</div>
				</div>
				<!-- Fund Tab Content Code End -->
				<!-- Recharge Margin Content Code start -->
                <div class="rechargemargin-tabcontent contentpart">
					<div class="trans-heading Hand ">Recharge Margin Log</div>
					<hr>
					<div style="margin-top:48px"></div>
					<table width="100%">
								<tr style="background: orange;color: black;">
									<td>Margin Date</td>
									<td>Recharge Amount</td>
									<td>Margin Amount</td>
								</tr>
					</table>				
					<div style="overflow:auto;height:400px">
						 <c:forEach var="rechargeMargin" items="${rechargeMarginLogs}">
							<table width="100%">
								<tr>
								    <td style=" width: 25%;">${rechargeMargin.margindate}</td>
								    <td>${rechargeMargin.totalRechargeAmount}</td>
								    <td>${rechargeMargin.marginAmount}</td>
								</tr>
							</table>
						 <br>
						</c:forEach>
					</div>
				</div>
				<!-- Recharge Margin Content Code End -->
				<!-- Cash Tab Content Code Start -->
				<div class="addfund-tabcontent contentpart" style="text-align:justify;width: 114%;">
					<div class="cash-heading Hand">Add Fund Request</div>
					<hr>
					<div style="margin-top:33px;color:#5a5555;margin-top: 33px;color: #5a5555;display: inline-block;width: 35%;float: right;border: 1px solid #ccc;">
                            <div class="col-md-6 md-margin-bottom-50">
                                <div class="panel panel-warning margin-bottom-40">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="fa fa-edit"></i>ICICI BANK LTD
                                            </h3>
                                        </div>
                                        <table class="table table-striped">
                                            <tbody>
                                            <tr>
                                                <td>Account Name</td>
                                                <td>LifeSoft Technology</td>
                                            </tr>
                                            <tr>
                                                <td>Account Number</td>
                                                <td>675005500091</td>
                                            </tr>
                                            <tr>
                                                <td>IFSC</td>
                                                <td>ICIC0006750</td>
                                            </tr>
                                            <tr>
                                                <td>Branch</td>
                                                <td>Jaipur, C-Scheme Branch</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
					</div>
					<br>
                   <div class="tab-pane fade active in" id="tab-1" style=" width: 60%;">
                   	BALANCE : RS. ${user.walletBalance}
                                    <div class="tab-body" style="border: 1px solid #ccc;">
                                        <div class="profileDIV">
                                            <span class="profileSPAN">Enter Amount (Min - 100)</span>
                                             <input maxlength="7" id="addRequestamount" placeholder="Please Enter Amount" name="addRequestamount" style="margin: 5px 3px;" type="text" onkeypress="return isNumberKey(event)">
                                        </div>


                                 
                                        <div class="profileDIV">
                                                <span class="profileSPAN">Select Payment Method</span>
                                                <select name="campmethod" id="campmethod" class="form-control">
                                                      <option value="BY SAME BANK FUND TRANSFER"> BY SAME BANK FUND TRANSFER
                                                        </option>
                                                        <option value="BY NEFT / RTGS"> BY NEFT / RTGS
                                                        </option>
                                                        <option value="BY SAME BANK FUND TRANSFER"> BY ATM TRANSFER
                                                        </option>
                                                        <option value="BY CHEQUE"> BY CHEQUE
                                                        </option>
                                                        <option value="BY IMPS"> BY IMPS
                                                        </option>
                                                </select>
                                            </div>
                                              
                                            <div class="profileDIV">
                                                <span class="profileSPAN">ChargenMarch Bank Name</span>
                                                <select name="campaybankaccount" class="form-control" id="campaybankaccount">
                                                    <option value="ICICI Bank">ICICI Bank</option>
                                                </select>
                                            </div>
                                                
                                             
                                            <div class="profileDIV">
                                                <input placeholder="Please Enter Bank Reference Id" id="camBankrefid" type="text"  name="camBankrefid" style="margin: 5px 3px;">
                                                <span class="profileSPAN">Please use Bank Reference Number, Other wise
                                                    Payment will be reject, do not use any spacial charactor
                                                    or duplicate</span>
                                            </div>
                                       
                                            <div class="profileDIV">

                                                <span class="profileSPAN">Remark</span>
                                                <textarea class="form-control" name="camremark" id="camremark"></textarea>
                                            </div>
                                  
                                            <div class="profileDIV">
                                                <button id="btn" onclick="addFundsRequest()" class="btn btn-lg btn-block btn-alt btn-icon btn-icon-right btn-icon-go">
                                                    <span>Proceed to Pay</span>
                                                </button>
                                            </div>
                                    </div>
                                </div>
				</div>
				<!-- Cash Tab Content Code End -->



				<!-- Payment Tab Content Code Start -->
				<div class="payment-tabcontent contentpart">
					<div class="payment-heading">PAYMENT METHOD</div>
					<hr>
					<center><img src="<c:url value="/resources/images/No-card-icon.png" />" /></center>
					<br>
					<span class="msg">You don't have any stored cards yet.</span><br>
				</div>
				<!-- Payment Tab Content Code End -->

			</div>
		</div>



	</div>
</section>

<!-- Footer Section Start -->
<%@include file="jsp-includes/cam_footer.jsp" %>
<!-- Footer Section End -->
<!-- Menu Bar Code Start -->
<%@include file="jsp-includes/cam_menu_script.jsp" %>
<%@include file="jsp-includes/cam_menu.jsp" %>

<!-- Menu Bar Code Start -->
<script src="${applicationScope['baseUrl']}/resources/javascript/document-account.js"></script> 

</body>
</html>