<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>CAM League | Recharge Mobile, DTH and DataCard | Chargenmarch.com</title>
<c:set var="now" value="<%=new java.util.Date()%>" />
<%@include file="jsp-includes/css-javascript-include-other.jsp" %>
<link type="text/css" href="${applicationScope['baseUrl']}/resources/css/leagueQuestions.css" rel="stylesheet" />
<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="${applicationScope['baseUrl']}/resources/images/png" rel="shortcut icon" />
<c:if test="${empty(userbean) || (!empty(sessionScope.userbean) && sessionScope.userbean.isverified == 0)}">
  <input type="hidden" value="true" id="checkUserLogin"/>
</c:if>
<c:if test="${!empty(userbean)  && sessionScope.userbean.isverified == 1}">
   <input type="hidden" value="false" id="checkUserLogin"/>
</c:if>
<c:set var="pageName" value="Cricketleague"/>
</head>
<body >
<!-- Header Start -->
<%@include file="jsp-includes/cam_header.jsp" %>
<!-- Header End -->
<!--  Login and register form  -->
<%@include file="jsp-includes/chargers_login.jsp" %>

<form action="${applicationScope['baseUrl']}/league/cricket/submit_leagues_answer.htm" id="leagueSubmit" name="leagueSubmit" method="post">
<input type="hidden" name="Userleagues1" id="Userleagues1" />
<input type="hidden" name="Userleagues2" id="Userleagues2" />
<input type="hidden" name="Userleagues3" id="Userleagues3" />
<input type="hidden" name="Userleagues4" id="Userleagues4" />
<input type="hidden" name="leaguesMatchId" id="leaguesMatchId" />
<input type="hidden" name="leaguesIdArr" id="leaguesIdArr" />
<input type="hidden" name="questionIds" id="questionIds" />
<input type="hidden" name="answers" id="answers" />
<input type="hidden" name="service" id="service" value="cricketLeague" />
<div class="mainOuterDiv">
<c:set var="matchSelected" value=""/>
  <div id="teamSelectionDiv" class="teamSelectionDiv Hand">
        <c:set var="count" value="0"/>
          <c:set var="todayMatchCount" value="${cricketQADtoList.size()}"/>
        <c:forEach var="cricketQADtoList" items="${cricketQADtoList}">
             <c:set var="cricketQADtoListVal" value="${cricketQADtoList.value}"/>
             <c:set var="cricketQADtoObj" value="${cricketQADtoListVal['iplFixtureDTO']}"/>
               <li id="matchId${cricketQADtoObj.id}" class="W30P <c:if test='${count == 0}'>leagueon</c:if>">

               <c:if test="${count == 0}">
               <c:set var="matchSelected" value="${cricketQADtoObj.id}"/>
               </c:if>
               <span class="spanTeam">${cricketQADtoObj.homeTeamCode} VS ${cricketQADtoObj.awayTeamCode}</span>
               <span>Match ${cricketQADtoObj.matchNumber}</span>
              
             </li>
             <c:set var="count" value="${count+1}"/>
          </c:forEach>
  </div>

    <div style="margin-top:5px;" id="leagueType"> 
     <c:forEach var="cricketLeagueMap" items="${cricketLeagueMap}" >
        <c:set var="cricketLeagueMapVal" value="${cricketLeagueMap.value}"/>
        <div class="W17P" id="league${cricketLeagueMapVal.id}">
         <span class="ColorRed Hand" onclick="showleaguedetail('PopUpDiv${cricketLeagueMapVal.id}')">  ${cricketLeagueMapVal.name} </span> 
         <a class="ColorBlue Hand" onclick="selectLeague('league${cricketLeagueMapVal.id}');">Join</a> 
           <input type="hidden" value="league${cricketLeagueMapVal.id}">
        </div>
          

          <div class="Hidden outerMainDivLeaguePopUp" id="PopUpDiv${cricketLeagueMapVal.id}">
           <div class="MainLeaguepopup"> 
           <a class="LeaguePopUpClose" onclick="closeLeagueMenu('PopUpDiv${cricketLeagueMapVal.id}');"></a> 
               <div>Pay Rs. ${cricketLeagueMapVal.price}</div>
                <div> Minimum Users: ${cricketLeagueMapVal.minUser}</div>
                  <li class="FloatL">Winning Position</li> <li class="FloatR"> Price Money</li>
             <c:forEach var="leagueDetail" items="${cricketLeagueMapVal.cricketLeaguesDetailDto}">
               <div>
                <li class="FloatL">${leagueDetail.leagueWinningNumber}</li>
                <li class="FloatR">${leagueDetail.leagueWinningAmount}</li>
               </div>
             </c:forEach>
           </div>
          </div>
       </c:forEach>
   </div>


   <div class="inner midtnc">

   <div id="questionAnswerDiv">
     <c:set var="count" value="0"/>
      <c:set var="closeCount" value="0"/>
       <c:forEach var="cricketQADtoListforQues" items="${cricketQADtoList}">
             <c:set var="cricketQADtoListforQuesVal" value="${cricketQADtoListforQues.value}"/>
             <c:set var="cricketQADtoObjforQues" value="${cricketQADtoListforQuesVal['cricketQuestionList']}"/>
              <c:set var="cricketQADtoObj" value="${cricketQADtoListforQuesVal['iplFixtureDTO']}"/>
              
                   <li id="matchIdList${cricketQADtoObj.id}" class="<c:if test='${count == 1}'>DispNoneImp</c:if> ListStyleNone">
                  <c:if test="${cricketQADtoObj.isClosedLeague == 'false'}">
                   <div class="TeamHeadDiv">
                    <span class="whiteColor">${cricketQADtoObj.homeTeam}</span> <span class="vs">Vs</span> <span class="whiteColor">${cricketQADtoObj.awayTeam}</span>
                      
                      <div class="otherInfo">Venue: ${cricketQADtoObj.venue}</div>
                      <div class="otherInfo">Day: ${cricketQADtoObj.matchDay}</div>

                   </div>
                     <c:set var="quetionArrayVar" value=""/>
                    <c:forEach var="cricQuestion" items="${cricketQADtoObjforQues}" varStatus="index">
                          
                            <c:choose>
                            <c:when test="${quetionArrayVar == ''}">
                              <c:set var="quetionArrayVar" value="${cricQuestion.id}"/>
                            </c:when>
                             <c:otherwise>
                               <c:set var="quetionArrayVar" value="${quetionArrayVar},${cricQuestion.id}"/>
                            </c:otherwise>
                            </c:choose>


                    <div class="quesnOuterDiv">
                        <div class="quesnDiv">  ${index.count}. </div>
                      <div class="quesnDiv">  ${cricQuestion.question} </div>
                      <div class="answerDiv">  
                        <c:forEach var="answerList" items="${cricQuestion.answerList}" varStatus="index">
                              <input type="radio" name="${cricketQADtoObj.id}answer${cricQuestion.id}" value="${answerList}" id="${cricketQADtoObj.id}answer${cricQuestion.id}"> ${answerList}
                          
                           </c:forEach> 
                      </div> 
                    </div>
                 </c:forEach>
                      <input type="hidden" value="${quetionArrayVar}" id="currentMatch${cricketQADtoObj.id}"/>
                   </c:if>
                   <c:if test="${cricketQADtoObj.isClosedLeague == 'true'}">
                         <div style="color:red;">League Closed.</div>
                         <c:set var="closeCount" value="${closeCount+1}"/>
                  </c:if>
              </li>
         
          
        <c:set var="count" value="${count+1}"/>
     </c:forEach>
   </div>
   <div>

   <c:if test="${todayMatchCount!=closeCount}">
         <input type="button" class="submitButton" tabindex="11" value="Submit" onclick="submitLeagueData('checkUserLogin');">
    </c:if>
</div>
    <div style="width: 50%;float: right;text-align: left;margin-top: 10px;font-size: 10px;">
       <div style="color: #008978">Rules</div>
      <div>1. Every league needs a minimum of 50 participants to start.</div>
      <div>2. If a league has less than 50 participants, participation amount paid will be transferred to CAM Wallet of user.</div>
      <div>3. Competition will finish one hour prior to start of respective match.</div>
      <div>4. Winning amount will be transferred to CAM Wallet of each player.</div>
      <div>5. Score will be calculated as the total number of correct answers.</div>
      <div>6. Top 5 players from each league will get award. In case of tie, merit will be based upon the time of answer submission. Earlier you play, better the chances of winning!</div>
    </div>

   </div>

</div>
</form>
 <!-- Failre Code End -->
<!-- Footer Section Start -->
<%@include file="jsp-includes/cam_footer.jsp" %>
<!-- Footer Section End -->
<!-- Menu Bar Code Start -->
<%@include file="jsp-includes/cam_menu_script.jsp" %>
<%@include file="jsp-includes/cam_menu.jsp" %>
<!-- Menu Bar Section End -->
</body>
<script type="text/javascript">
var matchSelected= '${matchSelected}';
$('#leaguesMatchId').val(matchSelected); 
   $(function(){
    $(".teamSelectionDiv li").click(function(){
        $(".teamSelectionDiv li").removeClass("leagueon");
        $(this).addClass("leagueon"); 
        var matchId = $(this).context.id;
        var matchNumId = matchId.replace('matchId','');
        $('#leaguesMatchId').val(matchNumId); 
        $("#questionAnswerDiv li").addClass("DispNoneImp");
        $("#matchIdList"+matchNumId).removeClass("DispNoneImp");
    });
});
   function selectLeague(id){
    var idNumber = id.replace('league','');

     if($('#'+id).hasClass("colorBorderBlue")){
      $('#'+id).removeClass("colorBorderBlue"); 
       $('#Userleagues'+idNumber).val('');
     }else{
       $('#'+id).addClass("colorBorderBlue"); 
       $('#Userleagues'+idNumber).val(idNumber);
      }
   }

   function submitLeagueData(checkUserLoginId){
       var useSelectedLeagueArray='';
       var checkUserLogin = $("#"+checkUserLoginId).val();
       
       for (var i = 1; i <= 4; i++) {
          var useSelectedLeague = $('#Userleagues'+i).val();
          if(useSelectedLeague != undefined && useSelectedLeague != ""){
            if(useSelectedLeagueArray==''){
              useSelectedLeagueArray = useSelectedLeague;
            }else{
              useSelectedLeagueArray = useSelectedLeagueArray+ "," + useSelectedLeague;
            } 
          }
       };
       if(useSelectedLeagueArray == "" || useSelectedLeagueArray == undefined){
               alert("Please Select any League");
               return false;
       }
       else{
              $('#leaguesIdArr').val(useSelectedLeagueArray);
       }

       var matchId = $("#leaguesMatchId").val();
       var questionArray = $("#currentMatch"+matchId).val();
       $('#questionIds').val(questionArray);

       var totalques = questionArray.split(',');
       var questionArray = '';
       for(var j=0;j<totalques.length;j++){
             var answerInputId = matchId +"answer" + totalques[j];
              var value = $("input[type='radio'][name="+answerInputId+"]:checked").val();
             if(value!=undefined){
                   if(questionArray== ''){
                        questionArray = value;
                   }else{
                        questionArray =questionArray+ "," + value;
                   }
              }else{
                  if(questionArray== ''){
                            questionArray = "NULL";
                    }else{
                            questionArray = questionArray + "," + "NULL";
                    }
               }
        } 
        $("#answers").val(questionArray);
       if(checkUserLogin!=undefined && checkUserLogin=="true"){
           showLoginPopUp();
       }
       else{
           $('#leagueSubmit').submit();
       }
   }

   function showleaguedetail(id){
    $('#'+id).removeClass('Hidden');
   }
   function closeLeagueMenu(id){
    $('#'+id).addClass('Hidden');
   }
</script>
</html>