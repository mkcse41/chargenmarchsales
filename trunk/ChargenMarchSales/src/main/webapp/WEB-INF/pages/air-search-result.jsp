<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<compress:html removeComments="true" removeIntertagSpaces="true">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="images/png" rel="shortcut icon" />
<meta name="msvalidate.01" content="4C0DE38ADCA26A9DED45D4FF7E9AAAA3" />
<title>ChargenMarch - Recharge Mobile, DTH, DataCard and Landline, Fast Recharge, Rewards</title>
<meta name="keywords" content="Online recharge, Free Recharge, Prepaid Recharge, Online mobile recharge, Datacard Recharge"/>
<meta name="Description" content="ChargenMarch.com-Simple way to recharge Mobile, DataCard, DTH, pay landline bills and keep on Marching forward. Get rewards with every payment on ChargenMarch"/>
<meta name="fragment" content="!" />
<meta name="google-site-verification" content="-o7OyH8W7bswsEjg9P2kHGskRYeaq56ZmWh7PBudP_A" />
<meta property="og:title" content="ChargenMarch - Recharge Mobile, DTH and DataCard, Fast & Free Recharge, Rewards | ChargenMarch.com" />
<meta property="og:type" content="website" />
<meta property="og:url" content="https://chargenmarch.com" />
<meta property="og:description" content="ChargenMarch.com - Simple way to Recharge all Prepaid Mobile, DataCard, DTH and keep on Marching forward. Rewards with Recharge. " />
<%@include file="jsp-includes/css-javascript-include-airticket.jsp" %>
</head>
<style type="text/css">
.HiddenSearchPage{display: none !important;}
.Hidden{display: none;}
</style>
<body>
<!-- Header Start -->
<!-- Header End -->
<!--  Login and register form  -->
<%@include file="jsp-includes/chargers_login.jsp" %>

<section id="homepageslider" class="HiddenSearchPage">

  <div class="inner HiddenSearchPage" id="bookingBoxSearch">
    <div class="close" onclick="closeDiv();"></div>
    <!-- My Form -->
    <div class="airForm">
    <h2>Book Domestic & International Flight Tickets</h2>
   

<div class="responsive-tabs">

  <form action="${applicationScope['baseUrl']}/march/air/search" name="airTicketForm" id="airTicketForm" method="post">

    <c:set var ="flightSearchFormVar" value="${flightSearchForm}"/>

    <input type="hidden" name="serviceType" id="serviceType"/>
    <input type="hidden" name="mode" id="mode"/> 
    <input type="hidden" name="origin" id="origin"/> 
    <input type="hidden" name="destination" id="destination"/>
    <input type="hidden" name="departDate" id="departDate"/>
    <input type="hidden" name="returnDate" id="returnDate"/>
    <input type="hidden" name="adultPax" id="adultPax"/>
    <input type="hidden" name="childPax" id="childPax"/>
    <input type="hidden" name="infantPax" id="infantPax"/>
    <input type="hidden" name="classPreferred" id="classPreferred"/>
    <input type="hidden" name="carrierPreferred" id="carrierPreferred"/>  
  </form>
    <a  onclick="showHideDiv('domestic','international');" class="domestic">Domestic</a> <a onclick="showHideDiv('international','domestic');" class="international">International</a>
    
<!-- Tab Working --> 
 <c:set var="serviceTypeDOM" value="false"/>
 <c:set var="serviceTypeINT" value="false"/>

  <c:if test="${flightSearchFormVar.serviceType == 'DOM'}">
    <c:set var="serviceTypeDOM" value="true"/>
  </c:if>

  <c:if test="${flightSearchFormVar.serviceType == 'INT'}">
     <c:set var="serviceTypeINT" value="true"/>
  </c:if>

  <div class="padding <c:if test="${serviceTypeDOM == 'false'}"> Hidden </c:if>" id="domestic">
    <table style="width:50%"><tr><td width="5%">
    <input type="radio" name="domtrip" id="domtripone" class="radio" <c:if test="${serviceTypeDOM == 'true' && flightSearchFormVar.mode == 'ONE'}">checked="true"</c:if>/></td> 
    <td style="vertical-align:middle;text-align:justify"><span>ONE WAY</span></td>
    <td><input type="radio" class="radio" name="domtrip" id="domtripround" <c:if test="${serviceTypeDOM == 'true' && flightSearchFormVar.mode == 'ROUND'}">checked="true"</c:if> /></td> 
    <td style="vertical-align:middle;text-align:justify"><span>ROUND TRIP</span></td></tr></table>
  
    <table width="100%">
    <tr>
      <td width="45%" style="text-align:justify"><span>From</span></td>
      <td rowspan="2" valign="bottom"><div class="arrow"></div></td>
      <td width="45%" style="text-align:justify"><span>To</span></td>
    </tr>
    <tr>
    <tD style="text-align:justify;" id="domesticDepartureCities" >
           <input list="domesticdeparture" name="domesticdeparture" id="domesticdepartureInput" class="select" placeholder="Type Departure City" style="width:100%" <c:if test="${serviceTypeDOM == 'true'}"> value="${flightSearchFormVar.origin}" </c:if> >
          <datalist id="domesticdeparture">
           <c:forEach var="airDomesticCitiesIter" items="${airDomesticCities}">
               <option value="${airDomesticCitiesIter.city},${airDomesticCitiesIter.airportName}- ${airDomesticCitiesIter.country_name}(${airDomesticCitiesIter.airportCode})">
           </c:forEach>
          </datalist>
    </tD>
     <tD style="text-align:justify;" id="domesticdestincationCities">
         <input list="domesticdestination" name="domesticdestination" id="domesticdestinationInput" class="select" placeholder="Type Destination City" style="width:100%" <c:if test="${serviceTypeDOM == 'true'}"> value="${flightSearchFormVar.destination}" </c:if>>
        <datalist id="domesticdestination">
                 <c:forEach var="airDomesticCitiesIter" items="${airDomesticCities}">
                      <option value="${airDomesticCitiesIter.city},${airDomesticCitiesIter.airportName}- ${airDomesticCitiesIter.country_name}(${airDomesticCitiesIter.airportCode})">
                 </c:forEach>
         </datalist>
    </tD>
    </tr>
    <tr>
    <td>
       <table style="margin-top:20px"><tr>
      <td width="40%" style="text-align:justify"><span>Departure</span></td>
      <td width="10%">&nbsp;</td>
      <td width="40%" style="text-align:justify"><span>Return</span></td>
    </tr>
    <tR><td style="text-align:justify;width:45%">
      <input type="text" class="datepicker" id="domDepartureDate" placeholder="mm/dd/yy" <c:if test="${serviceTypeDOM == 'true'}"> value="<fmt:formatDate pattern="yyyy-MM-dd" 
            value="${flightSearchFormVar.departDate}" />" </c:if> /></td>
    <td width="10%">&nbsp;</td>
    <td style="text-align:justify;width:45%">
      <input type="text" class="datepicker" id="domReturnDate" placeholder="mm/dd/yy" <c:if test="${serviceTypeDOM == 'true'}"> value="<fmt:formatDate pattern="yyyy-MM-dd" 
            value="${flightSearchFormVar.returnDate}" />" </c:if> <c:if test="${flightSearchFormVar.returnDate == null}"> disabled </c:if> /></td></tR></table>
    </td>
    <td>&nbsp;</td>
    <td>
       <table><tr>
      <td style="text-align:justify"><span style="font-size:10px">ADULT: (12+ YRS)</span></td>
      <td style="text-align:justify"><span style="font-size:10px">CHILD: (2-11 YRS)</span></td>
      <td style="text-align:justify"><span style="font-size:10px">INFANT: (0-2 YRS)</span></td>
    </tr>
    <tR>
    <td style="text-align:justify;">
    <input list="domadult" name="domadult" id="domadultInput" class="select1" placeholder="1" <c:if test="${serviceTypeDOM == 'true'}"> value="${flightSearchFormVar.adultPax}" </c:if>>
  <datalist id="domadult">
    <option value="1">
    <option value="2">
    <option value="3">
    <option value="4">
    <option value="5">
    <option value="6">
  </datalist>
    </td>
    <td style="text-align:justify;">
    <input list="domchild" name="domchild" id="domchildInput" class="select1" placeholder="0"  <c:if test="${serviceTypeDOM == 'true'}"> value="${flightSearchFormVar.childPax}" </c:if>>
  <datalist id="domchild">
    <option value="0">
    <option value="1">
    <option value="2">
    <option value="3">
    <option value="4">
    <option value="5">
    <option value="6">
  </datalist>
    </td>
    <td>
    <input list="dominfant" name="dominfant" id="dominfantInput" class="select1" placeholder="0" <c:if test="${serviceTypeDOM == 'true'}"> value="${flightSearchFormVar.infantPax}" </c:if> >
  <datalist id="dominfant">
    <option value="0">
    <option value="1">
    <option value="2">
    <option value="3">
    <option value="4">
    <option value="5">
    <option value="6">
  </datalist>
    </td>
    </tR></table>
  </td>
  </tr>


  <tr>
    <td>
    <table>
      <tr>
     <td style="text-align:justify;">
   <select name="domclassPreferredInput" id="domclassPreferredInput" placeholder="Preferred Class">
    <option value="E" selected> Economy </option>
    <option value="B">Business </option>
    <option value="B">First Class </option>
  </select>
    </td>
     <td style="text-align:justify;">
    <select name="domcarrierPreferredInput" id="domcarrierPreferredInput" placeholder="Preferred Airline">
    <c:forEach var="serviceProviders" items="${serviceProvider}">
     <option value="${serviceProviders.carrier_code}" <c:if test="${serviceTypeDOM == 'true' && serviceProviders.carrier_code == flightSearchFormVar.carrierPreferred}"> selected </c:if> >> ${serviceProviders.carrier_name} </option>
    </c:forEach>
  </select>
    </td>
    </tr>
  </table>


    </td>
    </tr>
    </table>
  <div class="btn-primary-red" onclick="return checkAllInformation();">Search Flights</div>
  </div>
    <!-- Tab Working -->

  <!-- Tab Working -->    
  <div class="padding  <c:if test="${serviceTypeINT == 'false'}"> Hidden </c:if>" id="international">
    <table style="width:50%"><tr><td width="5%"><input type="radio" name="intetrip" id="intetripone" class="radio" checked="true" <c:if test="${serviceTypeINT == 'true' && flightSearchFormVar.mode == 'ONE'}">checked="true"</c:if>/></td> 
    <td style="vertical-align:middle;text-align:justify"><span>ONE WAY</span></td>
    <td><input type="radio" class="radio" name="intetrip" id="intetripround" <c:if test="${serviceTypeINT == 'true' && flightSearchFormVar.mode == 'ROUND'}">checked="true"</c:if>/></td> 
    <td style="vertical-align:middle;text-align:justify"><span>ROUND TRIP</span></td></tr></table>
  
    <table width="100%">
    <tr>
      <td width="45%" style="text-align:justify"><span>From</span></td>
      <td rowspan="2" valign="bottom"><div class="arrow"></div></td>
      <td width="45%" style="text-align:justify"><span>To</span></td>
    </tr>
    <tr>
       <tD style="text-align:justify;" id="internationalDepartureCities" >
           <input list="internationaldeparture" name="internationaldeparture" id="internationaldepartureInput" class="select" placeholder="Type Departure City" style="width:100%" <c:if test="${serviceTypeINT == 'true'}"> value="${flightSearchFormVar.origin}" </c:if>>
          <datalist id="internationaldeparture">
           <c:forEach var="airinternationalCitiesIter" items="${airALLCities}">
               <option value="${airinternationalCitiesIter.city},${airinternationalCitiesIter.airportName}- ${airinternationalCitiesIter.country_name}(${airinternationalCitiesIter.airportCode})">
           </c:forEach>
          </datalist>
    </tD>
     <tD style="text-align:justify;" id="internationaldestincationCities">
         <input list="internationaldestination" name="internationaldestination" id="internationaldestinationInput" class="select" placeholder="Type Destination City" style="width:100%" <c:if test="${serviceTypeINT == 'true'}"> value="${flightSearchFormVar.destination}" </c:if>>
        <datalist id="internationaldestination">
                 <c:forEach var="airinternationalCitiesIter" items="${airALLCities}">
                     <option value="${airinternationalCitiesIter.city},${airinternationalCitiesIter.airportName}- ${airinternationalCitiesIter.country_name}(${airinternationalCitiesIter.airportCode})">
                 </c:forEach>
  </datalist>
    </tD>
    </tr>
    <tr>
    <td>
       <table style="margin-top:20px"><tr>
      <td width="40%" style="text-align:justify"><span>Departure</span></td>
      <td width="10%">&nbsp;</td>
      <td width="40%" style="text-align:justify"><span>Return</span></td>
    </tr>
    <tR><td style="text-align:justify;width:45%"><input type="text" class="datepicker"  id="interDepartureDate" placeholder="mm/dd/yy" <c:if test="${serviceTypeINT == 'true'}"> value=" <fmt:formatDate pattern="yyyy-MM-dd" 
            value="${flightSearchFormVar.departDate}" />" </c:if>/></td>
    <td width="10%">&nbsp;</td>
    <td style="text-align:justify;width:45%"><input type="text" class="datepicker" id="interReturnDate" placeholder="mm/dd/yy" <c:if test="${serviceTypeINT == 'true'}"> 
    value="<fmt:formatDate pattern="yyyy-MM-dd" value="${flightSearchFormVar.returnDate}"/>" 
    </c:if> <c:if test="${flightSearchFormVar.returnDate == null}"> disabled </c:if> /></td></tR></table>
    </td>
    <td>&nbsp;</td>
    <td>
       <table><tr>
      <td style="text-align:justify"><span style="font-size:10px">ADULT: (12+ YRS)</span></td>
      <td style="text-align:justify"><span style="font-size:10px">CHILD: (2-11 YRS)</span></td>
      <td style="text-align:justify"><span style="font-size:10px">INFANT: (0-2 YRS)</span></td>
    </tr>
    <tR>
    <td style="text-align:justify;">
    <input list="interadult" name="interadult" id="interadultInput" class="select1" placeholder="1" <c:if test="${serviceTypeINT == 'true'}"> value="${flightSearchFormVar.adultPax}" </c:if>>
  <datalist id="interadult">
    <option value="1">
    <option value="2">
    <option value="3">
    <option value="4">
    <option value="5">
    <option value="6">
  </datalist>
    </td>
    <td style="text-align:justify;">
    <input list="interchild" name="interchild" id="interchildInput" class="select1" placeholder="0" <c:if test="${serviceTypeINT == 'true'}"> value="${flightSearchFormVar.childPax}" </c:if>>
  <datalist id="interchild">
    <option value="0">
    <option value="1">
    <option value="2">
    <option value="3">
    <option value="4">
    <option value="5">
    <option value="6">
  </datalist>
    </td>
    <td>
    <input list="interinfant" name="interinfant" id="interinfantInput" class="select1" placeholder="0" <c:if test="${serviceTypeINT == 'true'}"> value="${flightSearchFormVar.infantPax}" </c:if>>
  <datalist id="interinfant">
    <option value="0">
    <option value="1">
    <option value="2">
    <option value="3">
    <option value="4">
    <option value="5">
    <option value="6">
  </datalist>
    </td>
    </tR></table>
      </td>
  </tr>


  <tr>
    <td>
    <table>
      <tr>
     <td style="text-align:justify;">
    <select name="interclassPreferred" id="interclassPreferredInput" placeholder="Preferred Class">
  <!-- <datalist id="interclassPreferred"> -->
    <option value="E" selected> Economy </option>
    <option value="B">Business </option>
    <option value="B">First Class </option>
  </select>
  <!-- </datalist> -->
    </td>

     <td style="text-align:justify;">
    <select name="intercarrierPreferredInput" id="intercarrierPreferredInput" placeholder="Preferred Airline">
    <c:forEach var="serviceProvider" items="${serviceProvider}">
     <option value="${serviceProvider.carrier_code}" <c:if test="${serviceTypeINT == 'true' && serviceProvider.carrier_code == flightSearchFormVar.carrierPreferred}"> selected </c:if> > ${serviceProvider.carrier_name} </option>
    </c:forEach>
  </select>
    </td>
    </tr>
  </table>

    </td>
    </tr>
    </table>
 <div class="btn-primary-red" onclick="return checkAllInformation();">Search Flights</div>
  </div>
    <!-- Tab Working -->   
    </div>
    <!-- My Form -->
  </div>
  </div>
</section>

<!-- SHOW DETAIL POPUP -->
<!-- <div id="showdetail">
<table width="100%">
<tr>
<td width="40%" style="vertical-align:top">
<style>
.responsive-tabs li{font-size:14px!important}
#tablist1-tab1{padding:12px!important}
#tablist1-tab2{padding:12px!important}
</style>
<div class="responsive-tabs">
  <h2 class="detail_head">ITINERARY</h2>
  <div class="padding">
        <p><span class="pull-left">1 Adult(s)</span><span class="pull-right"><span class="INR">Rs.</span>1,100</span></p>
        <p class="clearfix"><span class="pull-left">Total (Base Fare)</span><span class="pull-right"><span class="INR">Rs.</span>1,100</span></p>
        <p class="clearfix"><span class="pull-left">Passenger Service Fee</span>
          <span class="pull-right"><span class="INR">Rs.</span> 238</span>
        </p>
                <p class="clearfix"><span><span class="pull-left">Service Tax</span></span>
          <span class="pull-right"><span class="INR">Rs.</span> 75</span>
        </p>
                <p class="clearfix"><span class="pull-left">Airline Fuel Charge</span>
          <span class="pull-right"><span class="INR">Rs.</span> 237</span>
        </p>
                <p class="clearfix"><span class="pull-left">User Development Fee</span>         
                    <span class="pull-right"><span class="INR">Rs.</span> 388</span>
        </p>
        <p class="grand_total clearfix">
          <span class="pull-left"><strong>GRAND TOTAL</strong></span><span class="pull-right"><span class="INR"> Rs.</span>2,038</span>
        </p>
  </div>
  <h2 class="detail_head" style="font-size:16px!important;padding:5px!important">FARE DETAILS & RULES</h2>
  <div class="padding">
        <p><span class="pull-left">1 Adult(s)</span><span class="pull-right"><span class="INR">Rs.</span>1,100</span></p>
        <p class="clearfix"><span class="pull-left">Total (Base Fare)</span><span class="pull-right"><span class="INR">Rs.</span>1,100</span></p>
        <p class="clearfix"><span class="pull-left">Passenger Service Fee</span>
          <span class="pull-right"><span class="INR">Rs.</span> 238</span>
        </p>
                <p class="clearfix"><span><span class="pull-left">Service Tax</span></span>
          <span class="pull-right"><span class="INR">Rs.</span> 75</span>
        </p>
                <p class="clearfix"><span class="pull-left">Airline Fuel Charge</span>
          <span class="pull-right"><span class="INR">Rs.</span> 237</span>
        </p>
                <p class="clearfix"><span class="pull-left">User Development Fee</span>         
                    <span class="pull-right"><span class="INR">Rs.</span> 388</span>
        </p>
        <p class="grand_total clearfix">
          <span class="pull-left"><strong>GRAND TOTAL</strong></span><span class="pull-right"><span class="INR"> Rs.</span>2,038</span>
        </p>
  </div>
</div>  
</td>
<td width="60%" align="justify" style="text-align:justify">
<span class="detail_heading"><strong>Cancellation fee</strong> (per passenger)</span>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="detail_table">
                  <tbody>
                  <tr>
                    <th>Sector</th>
                    <th colspan="2">Cancellation Fee</th>
                  </tr>
                   <tr>
                     <td>JAI-DEL</td>                     
                     <td colspan="2">
                      <p class="ng-binding"><span class="INR">Rs.</span>1100 + <span class="INR">Rs.</span> 300*</p></td>
                   </tr>
                </tbody>
</table>
<span>* MakeMyTrip cancellation service fee</span><br>
<span class="detail_heading"><strong>Date change fee</strong> (per passenger)</span>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="detail_table">
                  <tbody>
                  <tr>
                    <th>Sector</th>
                    <th colspan="2">Cancellation Fee</th>
                  </tr>
                   <tr>
                     <td>JAI-DEL</td>                     
                     <td colspan="2">
                      <p class="ng-binding"><span class="INR">Rs.</span>1100 + <span class="INR">Rs.</span> 300*</p></td>
                   </tr>
                </tbody>
</table>
<span>* MakeMyTrip Date Change fee</span><br>
<span class="detail_heading"><strong>Baggage allowance</strong></span>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="detail_table">
                  <tbody>
                  <tr>
                    <th>Sector</th>
                    <th>Adult</th><th>Child</th><th>Infant</th>
                  </tr>
                   <tr>
                     <td>JAI-DEL</td>                     
                     <td><p class="ng-binding">15kg</p></td><td><p class="ng-binding">15kg</p></td><td><p class="ng-binding">0kg</p></td>
                   </tr>
                </tbody>
</table>
</td>
</tr>
</table>
</div> -->
<!-- SHOW DETAIL POPUP -->
<c:set var ="flightSearchFormVar" value="${flightSearchForm}"/>
  <c:if test="${flightSearchFormVar.mode == 'ONE'}">
   <c:set var="trip" value="ONE WAY-TRIP"/>
  </c:if>
  <c:if test="${flightSearchFormVar.mode == 'ROUND'}">
   <c:set var="trip" value="ROUND-TRIP"/>
  </c:if>

    <c:if test="${flightSearchFormVar.childPax != null}">
   <c:set var="childCount" value="${flightSearchFormVar.childPax}"/>
  </c:if>
  <c:if test="${flightSearchFormVar.childPax == null}">
   <c:set var="childCount" value="--"/>
  </c:if>

   <c:if test="${flightSearchFormVar.infantPax != null}">
   <c:set var="infantPaxCount" value="${flightSearchFormVar.infantPax}"/>
  </c:if>
   <c:if test="${flightSearchFormVar.infantPax == null}">
   <c:set var="infantPaxCount" value="--"/>
  </c:if>


<div id="mainDiv">
<section id="homepageslider" style="min-height:400px;height:400px">
  <div class="inner"> 
   <!--<center><img src="assets/images/ad.png" /></center>-->
   <!-- Modift Search -->
   <div class="searchdiv">
   <div class="part1">
   <span class="topheading">${trip}</span><br>
   <span class="subheading">${fn:substringBefore(flightSearchFormVar.origin, ",")} to ${fn:substringBefore(flightSearchFormVar.destination, ",")}</span>
   </div>
   <div class="part2">
     <div class="departure"><span class="topheading">DEPARTURE</span><br><span class="subheading" style="font-size: 14px;"><fmt:formatDate pattern="(E)dd MMMM yy"
            value="${flightSearchFormVar.departDate}" /></span></div>
     <div class="arrow1"> > </div>
     <div class="return"><span class="topheading">RETURN</span><br><span class="subheading" style="font-size: 14px;"><fmt:formatDate pattern="(E)dd MMMM yy"
            value="${flightSearchFormVar.returnDate}" /></span></div>
   </div>
   <div class="part3">
   <div class="part31"><span class="topheading">ADULT</span><br><span class="subheading">${flightSearchFormVar.adultPax}</span></div>
   <div class="part31"><span class="topheading">CHILD</span><br><span class="subheading"> ${childCount} </span></div>
   <div class="part31"><span class="topheading">INFANT</span><br><span class="subheading"> ${infantPaxCount} </span></div>
   <div class="part32" id="modifySearchBTN"> <div class="btn-primary-red">+ Modify Search</div></div>
   </div>
   </div>
   <!-- Modify Search -->
  </div>
</section>
<!-- Mid Part Of Page -->
   <div class="midpart">
   <div class="leftadd"><h1 style="font-size:18px">Advertisement <br> Section</h1></div>
   <div class="rightresult">
     <table width="100%" align="center">
     <tr style="border:none">
     <td><img src="assets/images/air_plane_airport.png" /></td>
     <td valign="top" style="vertical-align:middle;text-align:justify"><span class="subheading"> Jaipur to New Delhi-<span class="topheading">Fri,15 Apr 16</span></span>
     <h1 style="line-height:0;font-size:12px">22 Flights found</h1></td>
     </tr>
     </table>
     
     <div class="tabbing"><table width="100%"><tr><td width="20%">DEPARTURE </td><td width="20%"> ARRIVAL</td><td width="20%"> DURATION</td>
     <td style="border-bottom:2px #FFCC33 solid" width="18%">PRICE</td><td width="150px">&nbsp;</td></tr></table></div>

     <div class="content_tab">

       <c:forEach var="i" begin="1" end="2" varStatus="count">
       <div class="flight first">
       <table width="100%" class="tabl">
       <tr>
       <td width="15%">
        <img src="assets/images/air_plane_airport.png" /><br>
        <span class="topheading">IndiGo</span></td>
       <td width="15%"><span class="subheading">6.00</span><br><span class="topheading">Jaipur</span></td>
       <td width="15%"><span class="subheading">6.50</span><br><span class="topheading">Delhi</span></td>
       <td width="15%"><span class="subheading">0.50</span><br><span class="topheading">Non Stop</span></td>
       <td width="20%"><span class="rupee"> Rs. 1089.00</span></td>
       <td style="text-align:center;width:40%"><a href="#" class="book"><div class="btn-primary-red">Book</div></a><a class="detail showDetailDiv${count.index}" onclick="showDetailDiv(${count.index});">+ Show Details...</a></td>
       </tr>
       </table>

       <!-- SHOW DETAIL POPUP -->
 <div id="showdetail${count.index}" class="Hidden">


    <ul class="responsive-tabs__list" role="tablist">
      <li class="tablist1-tab tablist1-tabactive" aria-controls="tablist1-panel1" role="tab" tabindex="0"><a href="#fairDetail${count.index}" class="${count.index}">Fare details</a></li>
      <li class="tablist1-tab" aria-controls="tablist1-panel2" role="tab" tabindex="0"><a href="#collectionfee${count.index}" class="${count.index}">Cancellation fee</a></li>
      <li class="tablist1-tab" aria-controls="tablist1-panel2" role="tab" tabindex="0"><a href="#datechangefee${count.index}" class="${count.index}">Date change fee</a></li>
      <li class="tablist1-tab" aria-controls="tablist1-panel2" role="tab" tabindex="0"><a href="#baggageallowance${count.index}" class="${count.index}">Baggage allowance</a></li>

    </ul>

<div class="tab-content${count.index}">

    <div class="responsive-tabs" id="fairDetail${count.index}">
      <div class="padding" style="display: inline-block;">
           <span class="pull-left">1 Adult(s)</span><span class="pull-right"><span class="INR">Rs.</span>1,100</span>
           <span class="pull-left">Total (Base Fare)</span><span class="pull-right"><span class="INR">Rs.</span>1,100</span>
            <span class="pull-left">Passenger Service Fee</span>
              <span class="pull-right"><span class="INR">Rs.</span> 238</span>
           <span><span class="pull-left">Service Tax</span></span>
              <span class="pull-right"><span class="INR">Rs.</span> 75</span>
            <span class="pull-left">Airline Fuel Charge</span>
              <span class="pull-right"><span class="INR">Rs.</span> 237</span>
           <span class="pull-left">User Development Fee</span>         
                        <span class="pull-right"><span class="INR">Rs.</span> 388</span>
            
              <span class="pull-left"><strong>GRAND TOTAL</strong></span><span class="pull-right"><span class="INR"> Rs.</span>2,038</span>
          
      </div>
    </div>  

    <div class="responsive-tabs" id="collectionfee${count.index}" style="display: none;">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="detail_table">
                      <tbody>
                      <tr>
                        <th>Sector</th>
                        <th colspan="2">Cancellation Fee</th>
                      </tr>
                       <tr>
                         <td>JAI-DEL</td>                     
                         <td colspan="2">
                          <p class="ng-binding"><span class="INR">Rs.</span>1100 + <span class="INR">Rs.</span> 300*</p></td>
                       </tr>
                    </tbody>
    </table>
    </div>

    <div class="responsive-tabs" id="datechangefee${count.index}" style="display: none;">
  <table cellpadding="0" cellspacing="0" border="0" width="100%" class="detail_table">
                    <tbody>
                    <tr>
                      <th>Sector</th>
                      <th colspan="2">Cancellation Fee</th>
                    </tr>
                     <tr>
                       <td>JAI-DEL</td>                     
                       <td colspan="2">
                        <p class="ng-binding"><span class="INR">Rs.</span>1100 + <span class="INR">Rs.</span> 300*</p></td>
                     </tr>
                  </tbody>
  </table>
  </div>

    <div class="responsive-tabs" id="baggageallowance${count.index}" style="display: none;">
  <table cellpadding="0" cellspacing="0" border="0" width="100%" class="detail_table">
                    <tbody>
                    <tr>
                      <th>Sector</th>
                      <th>Adult</th><th>Child</th><th>Infant</th>
                    </tr>
                     <tr>
                       <td>JAI-DEL</td>                     
                       <td><p class="ng-binding">15kg</p></td><td><p class="ng-binding">15kg</p></td><td><p class="ng-binding">0kg</p></td>
                     </tr>
                  </tbody>
  </table>
  </div>

 </div> 
</div>  
 <!-- SHOW DETAIL POPUP -->

</div>
</c:forEach>
</div>
<!-- Tab Content -->

  
   </div>
   </div>

   <!-- Mid Part Of Page -->

    </div>
   <!-- Main Div -->

<!-- Footer Section Start -->
<%@include file="jsp-includes/cam_footer.jsp" %>


</body>
</html>
</compress:html>