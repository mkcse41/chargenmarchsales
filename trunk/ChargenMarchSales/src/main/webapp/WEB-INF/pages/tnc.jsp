<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Terms & Conditions | Recharge Mobile, DTH and DataCard | Chargenmarch.com</title>
<c:set var="now" value="<%=new java.util.Date()%>" />
<%@include file="jsp-includes/css-javascript-include-other.jsp" %>
<link type="text/css" href="${applicationScope['baseUrl']}/resources/css/cam_payment_failure.css" rel="stylesheet" />
<link type="text/css" href="${applicationScope['baseUrl']}/resources/css/cam_payment_success.css" rel="stylesheet" />
<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="${applicationScope['baseUrl']}/resources/images/png" rel="shortcut icon" />

</head>
<body >
<!-- Header Start -->
<%@include file="jsp-includes/cam_header.jsp" %>
<!-- Header End -->
<!--  Login and register form  -->
<%@include file="jsp-includes/chargers_login.jsp" %>

  <section>
 
<div class="inner midtnc">
        <div class="tnc" id="tnc">
         <p class="welcome" style="margin:0px;padding:15px 15px 0px 15px;text-align:center">Terms and Conditions</p><br>

          <p class="welcome" style="margin:0px;padding:15px 15px 0px 15px;text-align:center">LifeSoft Technology</p>
 	<div style="margin-left: 14px;text-align: justify; margin-right: 14px;">
		         <div>
					LAST REVISION:26-12-2015&nbsp;</div>
				<div>
					&nbsp;</div>
				<div class="fontBold">
					PLEASE READ THIS TERMS OF SERVICE AGREEMENT CAREFULLY. BY USING THIS WEBSITE OR ORDERING PRODUCTS FROM THIS WEBSITE YOU AGREE TO BE BOUND BY ALL OF THE TERMS AND CONDITIONS OF THIS AGREEMENT.</div>
				<div>
					&nbsp;</div>
				
				<div>
					This Terms of Service Agreement (the &quot;Agreement&quot;) governs your use of this website, chargenmarch.com (the &quot;Website&quot;) and Chargenmarch (&quot;Business Name&quot;) offer of products for purchase on this Website, or your purchase of products available on this Website. This Agreement includes, and incorporates by this reference, the policies and guidelines referenced below. Chargenmarch reserves the right to change or revise the terms and conditions of this Agreement at any time by posting any changes or a revised Agreement on this Website. Chargenmarch will alert you that changes or revisions have been made by indicating on the top of this Agreement the date it was last revised. The changed or revised Agreement will be effective immediately after it is posted on this Website. Your use of the Website following the posting any such changes or of a revised Agreement will constitute your acceptance of any such changes or revisions. Chargenmarch encourages you to review this Agreement whenever you visit the Website to make sure that you understand the terms and conditions governing use of the Website. This Agreement does not alter in any way the terms or conditions of any other written agreement you may have with Chargenmarch for other products or services. If you do not agree to this Agreement (including any referenced policies or guidelines), please immediately terminate your use of the Website. If you would like to print this Agreement, please click the print button on your browser toolbar.</div>
				<div>
					&nbsp;</div>
				
				<div class="fontBold">
					I. PRODUCTS</div>
				<div>
					Chargenmarch Terms of Offer. This Website offers for sale certain products (the &quot;Products&quot;). By placing an order for Products through this Website, you agree to the terms set forth in this Agreement.&nbsp;</div>
				<div>
					Customer Solicitation: Unless you notify our third party call center reps or direct Chargenmarch sales reps, while they are calling you, of your desire to opt out from further direct company communications and solicitations, you are agreeing to continue to receive further emails and call solicitations Chargenmarch and its designated in house or third party call team(s).&nbsp;</div>
				<div>
					&nbsp;</div>
				<div>
					Opt Out Procedure: We provide 3 easy ways to opt out of from future solicitations.&nbsp;</div>
				<div>
					1. You may use the opt out link found in any email solicitation that you may receive.&nbsp;</div>
				<div>
					2. You may also choose to opt out, via sending your email address to: &nbsp;care@chargenmarch.com&nbsp;</div>
				<div>
					3. You may send a written remove request to Plot no 44, R.G. Ramawat, Shekhawati Nagar Extension, Near Govindam Tower, Govind Pura Jaipur Rajasthan &nbsp;- &nbsp;302012 .&nbsp;</div>
				<div>
					&nbsp;</div>
				
				<div>
					Proprietary Rights. Chargenmarch , has proprietary rights and trade secrets in the Products. You may not copy, reproduce, resell or redistribute any Product manufactured and/or distributed by Chargenmarch . &nbsp;Chargenmarch also has rights to all trademarks and trade dress and specific layouts of this webpage, including calls to action, text placement, images and other information.&nbsp;</div>
				<div>
					Sales Tax. If you purchase any Products, you will be responsible for paying any applicable sales tax.</div>
				<div>
					&nbsp;</div>
				
				<div class="fontBold">
					II. WEBSITE&nbsp;</div>
				<div>
					Content; Intellectual Property; Third Party Links. In addition to making Products available, this Website also offers information and marketing materials. This Website also offers information, both directly and through indirect links to third-party websites, about nutritional and dietary supplements. Chargenmarch does not always create the information offered on this Website; instead the information is often gathered from other sources. To the extent that Chargenmarch does create the content on this Website, such content is protected by intellectual property laws of the India, foreign nations, and international bodies. Unauthorized use of the material may violate copyright, trademark, and/or other laws. You acknowledge that your use of the content on this Website is for personal, noncommercial use. Any links to third-party websites are provided solely as a convenience to you. Chargenmarch does not endorse the contents on any such third-party websites. Chargenmarch is not responsible for the content of or any damage that may result from your access to or reliance on these third-party websites. If you link to third-party websites, you do so at your own risk.</div>
				<div>
					&nbsp;</div>
				
				<div>
					Use of Website; Chargenmarch &nbsp; is not responsible for any damages resulting from use of this website by anyone. You will not use the Website for illegal purposes. You will (1) abide by all applicable local, state, national, and international laws and regulations in your use of the Website (including laws regarding intellectual property), (2) not interfere with or disrupt the use and enjoyment of the Website by other users, (3) not resell material on the Website, (4) not engage, directly or indirectly, in transmission of &quot;spam&quot;, chain letters, junk mail or any other type of unsolicited communication, and (5) not defame, harass, abuse, or disrupt other users of the Website.</div>
				<div>
					&nbsp;</div>
				
				<div>
					License. By using this Website, you are granted a limited, non-exclusive, non-transferable right to use the content and materials on the Website in connection with your normal, noncommercial, use of the Website. You may not copy, reproduce, transmit, distribute, or create derivative works of such content or information without express written authorization from Chargenmarch or the applicable third party (if third party content is at issue).</div>
				<div>
					&nbsp;</div>
				
				<div>
					Posting. By posting, storing, or transmitting any content on the Website, you hereby grant Chargenmarch a perpetual, worldwide, non-exclusive, royalty-free, assignable, right and license to use, copy, display, perform, create derivative works from, distribute, have distributed, transmit and assign such content in any form, in all media now known or hereinafter created, anywhere in the world. Chargenmarch does not have the ability to control the nature of the user-generated content offered through the Website. You are solely responsible for your interactions with other users of the Website and any content you post. Chargenmarch is not liable for any damage or harm resulting from any posts by or interactions between users. Chargenmarch reserves the right, but has no obligation, to monitor interactions between and among users of the Website and to remove any content Chargenmarch deems objectionable, in MuscleUP Nutrition &#39;s sole discretion.</div>
				<div>
					&nbsp;</div>
				
				<div class="fontBold">
					III. DISCLAIMER OF WARRANTIES&nbsp;</div>
				<div>
					YOUR USE OF THIS WEBSITE AND/OR PRODUCTS ARE AT YOUR SOLE RISK. THE WEBSITE AND PRODUCTS ARE OFFERED ON AN &quot;AS IS&quot; AND &quot;AS AVAILABLE&quot; BASIS. Chargenmarch EXPRESSLY DISCLAIMS ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT WITH RESPECT TO THE PRODUCTS OR WEBSITE CONTENT, OR ANY RELIANCE UPON OR USE OF THE WEBSITE CONTENT OR PRODUCTS. (&quot;PRODUCTS&quot; INCLUDE TRIAL PRODUCTS.)</div>
				<div>
					&nbsp;</div>
				
				<div>
					WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, Chargenmarch &nbsp; MAKES NO WARRANTY:</div>
				<div>
					&nbsp;</div>
				
				<div class="fontBold" >
					IV. LIMITATION OF LIABILITY&nbsp;</div>
				<div>
					Chargenmarch ENTIRE LIABILITY, AND YOUR EXCLUSIVE REMEDY, IN LAW, IN EQUITY, OR OTHWERWISE, WITH RESPECT TO THE WEBSITE CONTENT AND PRODUCTS AND/OR FOR ANY BREACH OF THIS AGREEMENT IS SOLELY LIMITED TO THE AMOUNT YOU PAID, LESS SHIPPING AND HANDLING, FOR PRODUCTS PURCHASED VIA THE WEBSITE.</div>
				<div>
					&nbsp;</div>
				
				<div>
					Chargenmarch WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES IN CONNECTION WITH THIS AGREEMENT OR THE PRODUCTS IN ANY MANNER, INCLUDING LIABILITIES RESULTING FROM (1) THE USE OR THE INABILITY TO USE THE WEBSITE CONTENT OR PRODUCTS; (2) THE COST OF PROCURING SUBSTITUTE PRODUCTS OR CONTENT; (3) ANY PRODUCTS PURCHASED OR OBTAINED OR TRANSACTIONS ENTERED INTO THROUGH THE WEBSITE; OR (4) ANY LOST PROFITS YOU ALLEGE.&nbsp;</div>
				<div>
					SOME JURISDICTIONS DO NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES SO SOME OF THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU.</div>
				<div>
					&nbsp;</div>
				
				<div class="fontBold">
					V. INDEMNIFICATION&nbsp;</div>
				<div>
					WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, Chargenmarch , and any of its contractors, agents, employees, officers, directors, shareholders, affiliates and assigns from all liabilities, claims, damages, costs and expenses, including reasonable attorneys&#39; fees and expenses, of third parties relating to or arising out of (1) this Agreement or the breach of your warranties, representations and obligations under this Agreement; (2) the Website content or your use of the Website content; (3) the Products or your use of the Products (including Trial Products); (4) any intellectual property or other proprietary right of any person or entity; (5) your violation of any provision of this Agreement; or (6) any information or data you supplied to Chargenmarch . &nbsp;When Chargenmarch is threatened with suit or sued by a third party, &nbsp;Chargenmarch may seek written assurances from you concerning your promise to indemnify Chargenmarch ; &nbsp;your failure to provide such assurances may be considered by Chargenmarch &nbsp;to be a material breach of this Agreement. Chargenmarch will have the right to participate in any defense by you of a third-party claim related to your use of any of the Website content or Products, with counsel of Chargenmarch choice at its expense. Chargenmarch will reasonably cooperate in any defense by you of a third-party claim at your request and expense. You will have sole responsibility to defend Chargenmarch against any claim, but you must receive Chargenmarch &nbsp;prior written consent regarding any related settlement. The terms of this provision will survive any termination or cancellation of this Agreement or your use of the Website or Products.</div>
				<div>
					&nbsp;</div>
				
				<div class="fontBold">
					VI. PRIVACY&nbsp;</div>
				<div>
					Chargenmarch believes strongly in protecting user privacy and providing you with notice of MuscleUP Nutrition &#39;s use of data. Please refer to Chargenmarch privacy policy, incorporated by reference herein, that is posted on the Website.&nbsp;</div>
				<div>
					&nbsp;</div>
				
				<div class="fontBold">
					VII. AGREEMENT TO BE BOUND&nbsp;</div>
				<div>
					By using this Website or ordering Products, you acknowledge that you have read and agree to be bound by this Agreement and all terms and conditions on this Website.</div>
				<div>
					&nbsp;</div>
				
				<div class="fontBold">
					VIII. GENERAL&nbsp;</div>
				<div>
					Force Majeure. Chargenmarch will not be deemed in default hereunder or held responsible for any cessation, interruption or delay in the performance of its obligations hereunder due to earthquake, flood, fire, storm, natural disaster, act of God, war, terrorism, armed conflict, labor strike, lockout, or boycott.</div>
				<div>
					&nbsp;</div>
				
				<div>
					Cessation of Operation. Chargenmarch may at any time, in its sole discretion and without advance notice to you, cease operation of the Website and distribution of the Products.</div>
				<div>
					&nbsp;</div>
				
				<div>
					Entire Agreement. This Agreement comprises the entire agreement between you and Chargenmarch and supersedes any prior agreements pertaining to the subject matter contained herein.</div>
				<div>
					&nbsp;</div>
				
				<div>
					Effect of Waiver. The failure of Chargenmarch to exercise or enforce any right or provision of this Agreement will not constitute a waiver of such right or provision. If any provision of this Agreement is found by a court of competent jurisdiction to be invalid, the parties nevertheless agree that the court should endeavor to give effect to the parties&#39; intentions as reflected in the provision, and the other provisions of this Agreement remain in full force and effect.</div>
				<div>
					&nbsp;</div>
				
				<div>
					Governing Law; Jurisdiction. This Website originates from the Rajasthan High Court . This Agreement will be governed by the laws of the State of Rajasthan High Court without regard to its conflict of law principles to the contrary. Neither you nor Chargenmarch will commence or prosecute any suit, proceeding or claim to enforce the provisions of this Agreement, to recover damages for breach of or default of this Agreement, or otherwise arising under or by reason of this Agreement, other than in courts located in State of Rajasthan High Court . &nbsp;By using this Website or ordering Products, you consent to the jurisdiction and venue of such courts in connection with any action, suit, proceeding or claim arising under or by reason of this Agreement. You hereby waive any right to trial by jury arising out of this Agreement and any related documents. &nbsp;</div>
				<div>
					&nbsp;</div>
				
				<div>
					Statute of Limitation. You agree that regardless of any statute or law to the contrary, any claim or cause of action arising out of or related to use of the Website or Products or this Agreement must be filed within one (1) year after such claim or cause of action arose or be forever barred.</div>
				<div>
					&nbsp;</div>
				
				<div>
					Waiver of Class Action Rights. BY ENTERING INTO THIS AGREEMENT, YOU HEREBY IRREVOCABLY WAIVE ANY RIGHT YOU MAY HAVE TO JOIN CLAIMS WITH THOSE OF OTHER IN THE FORM OF A CLASS ACTION OR SIMILAR PROCEDURAL DEVICE. ANY CLAIMS ARISING OUT OF, RELATING TO, OR CONNECTION WITH THIS AGREEMENT MUST BE ASSERTED INDIVIDUALLY.</div>
				<div>
					&nbsp;</div>
				
				<div>
					Termination. Chargenmarch reserves the right to terminate your access to the Website if it reasonably believes, in its sole discretion, that you have breached any of the terms and conditions of this Agreement. Following termination, you will not be permitted to use the Website and Chargenmarch may, in its sole discretion and without advance notice to you, cancel any outstanding orders for Products. If your access to the Website is terminated, Chargenmarch reserves the right to exercise whatever means it deems necessary to prevent unauthorized access of the Website. This Agreement will survive indefinitely unless and until Chargenmarch chooses, in its sole discretion and without advance to you, to terminate it. &nbsp;Domestic Use. Chargenmarch makes no representation that the Website or Products are appropriate or available for use in locations outside India. Users who access the Website from outside India do so at their own risk and initiative and must bear all responsibility for compliance with any applicable local laws, Assignment. You may not assign your rights and obligations under this Agreement to anyone. Chargenmarch may assign its rights and obligations under this Agreement in its sole discretion and without advance notice to you.&nbsp;</div>
				<div>
					&nbsp;</div>
				<div>
					BY USING THIS WEBSITE OR ORDERING PRODUCTS FROM THIS WEBSITE YOU AGREE TO BE BOUND BY ALL OF THE TERMS AND CONDITIONS OF THIS AGREEMENT.&nbsp;</div>

		             
		        </div>
		    </div>
 </div>
</section>
 <!-- Failre Code End -->
<!-- Footer Section Start -->
<%@include file="jsp-includes/cam_footer.jsp" %>
<!-- Footer Section End -->
<!-- Menu Bar Code Start -->
<%@include file="jsp-includes/cam_menu_script.jsp" %>
<%@include file="jsp-includes/cam_menu.jsp" %>

<!-- Menu Bar Section End -->
</body>
<script type="text/javascript">
  function checkPasswords(passId, confPassId){
    var passvalue = $("#"+passId).val();
    var confpassvalue = $("#"+confPassId).val();
    if(passvalue == confpassvalue){
      return true;
    } else{
      alert("Passwords Do Not Match");
     return false;
   }
  }
</script>
</html>
