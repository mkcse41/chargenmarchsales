<!DOCTYPE html><%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Payment Response</title>
</head>
<body>
<c:if test="${empty(errorStringList)}">
<form action="${url}" method="post" id="paymentResponseForm"> 
    <input type="hidden" name="response" value='${response}'/>
</form>
<script type="text/javascript">
      setTimeout(document.getElementById("paymentResponseForm").submit(),500)
</script>
</c:if>
<c:if test="${not empty(errorStringList)}">
Error in payment process : <br/>
<c:forEach var="errorStringListIter" items="${errorStringList}" >
            <span style="color:red"> ${errorStringListIter}</span>
 </c:forEach> 
</c:if>
</body>
</html>