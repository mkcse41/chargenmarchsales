<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>PayU Payment</title>
</head>
<body>

<form action="${url}" method="post" id="PayuForm"> 
    <input type="hidden" name="firstName" value="${orderInfo.firstName}"/>
     <input type="hidden" name="lastName" value="${orderInfo.lastName}"/>
     <input type="hidden" name="emailId" value="${orderInfo.emailId}"/>
     <input type="hidden" name="mobileNo" value="${orderInfo.mobileno}"/>
     <input type="hidden" name="pincode" value="${orderInfo.pinCode}"/>
     <input type="hidden" name="city" value="${orderInfo.city}"/>
     <input type="hidden" name="state" value="${orderInfo.state}"/>
     <input type="hidden" name="address" value="${orderInfo.address}"/>
     <input type="hidden" name="productName" value="${orderInfo.productName}"/>
     <input type="hidden" name="amount" value="${orderInfo.amount}"/>
     <input type="hidden" name="pgType" value="${orderInfo.pgType}"/>
     <input type="hidden" name="userCredentials" value="${orderInfo.userCredentials}"/>
      <!-- <input type="submit" value="submit">  -->
</form>
<script type="text/javascript">
      setTimeout(document.getElementById("PayuForm").submit(),100)
</script>
</body>
</html>