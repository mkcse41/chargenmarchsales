<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Internal Server Error | ChargenMarch.com</title>
<c:set var="now" value="<%=new java.util.Date()%>" />
<%@include file="jsp-includes/css-javascript-include-other.jsp" %>
<link type="text/css" href="${applicationScope['baseUrl']}/resources/css/cam_payment_failure.css" rel="stylesheet" />
<link type="text/css" href="${applicationScope['baseUrl']}/resources/css/cam_payment_success.css" rel="stylesheet" />
<link href="${applicationScope['baseUrl']}/resources/images/favicon1.png" type="${applicationScope['baseUrl']}/resources/images/png" rel="shortcut icon" />
</head>
<body >
<!-- Header Start -->
<%@include file="jsp-includes/cam_header.jsp" %>
<!-- Header End -->
<!--  Login and register form  -->
<%@include file="jsp-includes/chargers_login.jsp" %>

  
 <!-- Failre Code Start -->
  <section>
 
<div class="inner mid">

 <div class="container" style="margin:auto">
  <div class="row" style="text-align:center">
  <div class="col-md-2" style="width:15%">&nbsp;</div>
 <div class="col-md-8" style="background:#000;padding:0px;width:73%;margin:auto">
  
  <div style="width:50%;float:left"><img src="${applicationScope['baseUrl']}/resources/images/warning-icon.png" /></div>
  <div style="width:50%;float:left;text-align:justify">
    <span class="call" style="font-size:36px">Exception</span><br />
    <span class="call" style="font-weight:600;color:#999">Internal Server Error</span><br />
    <h3 style="font-size:16px;height:15px">There was some error with the server. Please visit <a href="${applicationScope['baseUrl']}">homepage</a> &amp; try again.</h3>
  </div>
 </div>
 <div class="col-md-2" style="width:15%">&nbsp;</div>
 </div>
 <div class="line">&nbsp;</div>
  <div class="row"> <div class="col-md-12">
 <div class="help"><a href="javascript:void()">Need any kind of Help?</a> We are happy to help you!</div>
 <div class="help">Write an email at <a href="mailto:help@chargenmarch.com">help@chargenmarch.com</a></div>
 </div></div>
 </div>
 </div>
</section>
 <!-- Failre Code End -->
<!-- Footer Section Start -->
<%@include file="jsp-includes/cam_footer.jsp" %>
<!-- Footer Section End -->
<!-- Menu Bar Code Start -->
<%@include file="jsp-includes/cam_menu_script.jsp" %>
<%@include file="jsp-includes/cam_menu.jsp" %>

<!-- Menu Bar Section End -->
</body>
</html>
