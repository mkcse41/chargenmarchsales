
			<!-- <div id="closeFeedback" class="close hidden" onclick="feedbackClose();"></div> -->
<div id="slideout" >
    <span onclick="feedbackOpen();" class="Hand"><img src="${applicationScope['baseUrl']}/resources/images/feedback.png" alt="feedback" width="11" height="72"/></span>
    <div id="slideout_inner">
			<form>
			  <h1 style="margin-top:-28px">FeedBack</h1>
			  <input placeholder="Name" type="text" name="name" id="feedbackname" value="" required class="marginb10" style="margin-top:-10px" onkeypress="return onlyAlphabets(event,this)">
			  <input placeholder="Email address" type="email" id="feedbackemailId" name="emailId" onblur="this.setAttribute('value', this.value);" value="" required class="marginb10" onkeypress="return spaceNotAllowed(event)">
			  <input placeholder="Mobile" maxlength="10" type="text" value="" name="mobileNo" id="feedbackmobileNo" required class="marginb10" onkeypress="return isNumberKey(event)">
			  <div class="flex marginb10" style="width:105%;padding-left:12px;">
			    <textarea placeholder="Message" rows="1" name="message" id="feedbackmessage" required></textarea>
			  </div>
               <div id="errorMessagefeedback"></div>   
			   <input type="button" class="feedbackButton" value="Feedback" onclick="return userFeedback('feedbackname','feedbackemailId','feedbackmobileNo','feedbackmessage','errorMessagefeedback');">
				</form>
    </div>
</div>

<style type="text/css">
	#slideout {
    position: fixed;
    top: 200px;
    right: 0;
    width: 35px;
    height:100px;
    padding: 12px 0;
    text-align: center;
    background: #6DAD53;
    -webkit-transition-duration: 0.3s;
    -moz-transition-duration: 0.3s;
    transition-duration: 0.3s;
    -o-transition-duration: 0.3s;
    -webkit-border-radius: 0 5px 5px 0;
    -moz-border-radius: 0 5px 5px 0;
    border-radius: 5px 0px 0px 5px;
    z-index: 9999;
}
#slideout_inner {
    right: -300px;
    background: #eee;
    width: 300px;
    padding: 25px;
    height: 328px;
    -webkit-transition-duration: 0.3s;
    -moz-transition-duration: 0.3s;
    transition-duration: 0.3s;
    -o-transition-duration: 0.3s;
    text-align: left;
    -webkit-border-radius: 0 0 0px 5px;
    -moz-border-radius: 0 0 0px 5px;
    border-radius: 0 0 0px 5px;
    display: block;
    position: fixed;
    top: 200px;
    box-shadow: 0 0 0 4px rgba(255,255,255,0.2);
}
#slideout_inner input{
  padding-left: -21px;
}
#slideout_inner textarea{
  padding-left: 10px;
  height: 60px;
  width: 104%;
}
.close {
    background-image: url("http://localhost:8080/chargenmarch/resources/images/close.svg");
    background-repeat: no-repeat;
    background-size: contain;
    background-position: right top;
    border: 0px none;
    color: #999;
    cursor: pointer;
    height: 35px;
    position: fixed;
    right: 0px;
    text-align: right;
    top: 166px;
    vertical-align: middle;
    width: 6em;
    z-index: 9999;
    width: 36px;
    font-size: 15px;
}
.feedbackButton{
	width: 103%;
	padding: 0px;
}
</style>

<script type="text/javascript">
function feedbackOpen(){
    var right = $("#slideout").css("right");
    if(right=="300px"){
        feedbackClose();
    }else{
        $('#slideout').css("right","300px");
        $('#slideout_inner').css("right","0");
        $('#closeFeedback').removeClass('hidden');
    }
     	
};

function feedbackClose(){
        $('#slideout').css("right","0px");
		$('#slideout_inner').css("right","-300px");
		$('#closeFeedback').addClass('hidden');
	 		
};

function feedbackEmpty(){
       $('#feedbackname').val("");
       $('#feedbackemailId').val("");
       $('#feedbackmobileNo').val("");
       $('#feedbackmessage').val("");
       setTimeout(function(){ feedbackClose(); }, 3000);
}
</script>