<!DOCTYPE html><%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<html>
<head>
<title>Payment Process</title>
</head>
<body>
<c:if test="${empty(paymentErrorStringList)}">
<form action="${orderInfo.paymentGateWayInfo.url}" method="post" id="paymentGateWayForm"> 
    <input type="hidden" name="firstname" value="${orderInfo.firstName}"/>
     <input type="hidden" name="surl" value="${orderInfo.successUrl}"/>
     <input type="hidden" name="phone" value="${orderInfo.mobileno}"/>
     <input type="hidden" name="key" value="${orderInfo.paymentGateWayInfo.merchantId}"/>
     <input type="hidden" name="hash" value="${orderInfo.hash}"/>
     <input type="hidden" name="curl" value="${orderInfo.cancelUrl}"/>
     <input type="hidden" name="furl" value="${orderInfo.failureUrl}"/>
     <input type="hidden" name="txnid" value="${orderInfo.txnId}"/>
     <input type="hidden" name="productinfo" value="${orderInfo.productName}"/>
     <input type="hidden" name="amount" value="${orderInfo.amount}"/>
     <input type="hidden" name="email" value="${orderInfo.emailId}"/>
     <input type="hidden" name="user_credentials" value="${orderInfo.userCredentials}"/>
     <!--  <input type="hidden" name="service_provider" value="payu_paisa" /> -->
</form>
<script type="text/javascript">
      setTimeout(document.getElementById("paymentGateWayForm").submit(),100)
</script>
</c:if>
<c:if test="${not empty(paymentErrorStringList)}">
Required Data : <br/>
<c:forEach var="errorStringListIter" items="${paymentErrorStringList}">
     <span style="color:red"> ${errorStringListIter}</span>
 </c:forEach> 
</c:if>
</body>
</html>