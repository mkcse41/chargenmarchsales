$(document).ready(function() {

	//Hide All 4 Tabs
	$('.trans-tabcontent').hide();
	$('.address-tabcontent').hide();
	$('.cash-tabcontent').hide();
	$('.payment-tabcontent').hide();
	$('.addfund-tabcontent').hide();
	$('.fundsummary-tabcontent').hide();
	$('.rechargemargin-tabcontent').hide();
	//Hide All 4 Tabs
	//Transaction Tab Code
	$(".trans-tab").click(function(){
		$('.trans-tabcontent').fadeIn("fast");$('.addfund-tabcontent').hide();$('.fundsummary-tabcontent').hide();$('.address-tabcontent').hide();$('.cash-tabcontent').hide();$('.payment-tabcontent').hide();$('.profile-tabcontent').hide();$('.rechargemargin-tabcontent').hide();
	});
	 //Address Tab Code
	 $(".address-tab").click(function(){
	 	$('.trans-tabcontent').hide();$('.address-tabcontent').fadeIn("fast");$('.cash-tabcontent').hide();$('.payment-tabcontent').hide();$('.profile-tabcontent').hide();
	    $('.addfund-tabcontent').hide();$('.fundsummary-tabcontent').hide();$('.rechargemargin-tabcontent').hide();
	 });
	 //Cash Tab Code
	 $(".cash-tab").click(function(){
	 	$('.trans-tabcontent').hide();$('.address-tabcontent').hide();$('.cash-tabcontent').fadeIn("fast");$('.payment-tabcontent').hide();$('.profile-tabcontent').hide();
	    $('.addfund-tabcontent').hide();$('.fundsummary-tabcontent').hide();$('.rechargemargin-tabcontent').hide();
	 });
	 //Profile Tab Code
	 $(".profile-tab").click(function(){
	 	$('.trans-tabcontent').hide();$('.address-tabcontent').hide();$('.cash-tabcontent').hide();$('.payment-tabcontent').hide();$('.profile-tabcontent').fadeIn("slow");
	    $('.addfund-tabcontent').hide();$('.fundsummary-tabcontent').hide();$('.rechargemargin-tabcontent').hide();
	 });
	 //Payment Tab Code
	 $(".payment-tab").click(function(){
	 	$('.trans-tabcontent').hide();$('.address-tabcontent').hide();$('.cash-tabcontent').hide();$('.payment-tabcontent').fadeIn("fast");$('.profile-tabcontent').hide();
	    $('.addfund-tabcontent').hide();$('.fundsummary-tabcontent').hide();$('.rechargemargin-tabcontent').hide();
	 });

	 //add funds Tab Code
	 $(".addfund-tab").click(function(){
	 	$('.trans-tabcontent').hide();$('.address-tabcontent').hide();$('.cash-tabcontent').hide();$('.payment-tabcontent').hide();$('.profile-tabcontent').hide();
	    $('.addfund-tabcontent').fadeIn("fast");$('.fundsummary-tabcontent').hide();$('.rechargemargin-tabcontent').hide();
	 });
	 //fund summary Code
	 $(".fundsummary-tab").click(function(){
	 	$('.trans-tabcontent').hide();$('.address-tabcontent').hide();$('.cash-tabcontent').hide();$('.payment-tabcontent').hide();$('.profile-tabcontent').hide();
	    $('.addfund-tabcontent').hide();$('.fundsummary-tabcontent').fadeIn("fast");$('.rechargemargin-tabcontent').hide();
	 });
	  $(".rechargemargin-tab").click(function(){
	 	$('.trans-tabcontent').hide();$('.address-tabcontent').hide();$('.cash-tabcontent').hide();$('.payment-tabcontent').hide();$('.profile-tabcontent').hide();
	    $('.addfund-tabcontent').hide();$('.fundsummary-tabcontent').hide();$('.rechargemargin-tabcontent').fadeIn("fast");
	 });

	 $(".addaddressform").hide();
	 $(".settingcontent").hide();
	 $(".editaccount").hide();
	 $(".changepw").hide();

	 $(".editprofile").click(function(){
	 	$(".editaccount").slideToggle("fast");
	 	$(".changepw").hide();
	 });
	 $(".changepassword").click(function(){
	 	$(".changepw").slideToggle("fast");
	 	$(".editaccount").hide();
	 });

	 $(".setting").click(function(){
	 	$(".settingcontent").slideToggle("fast");
	 });

	 $(".addaddress").click(function(){
	 	$(".addaddressform").slideToggle("slow");
	 });
	
	 $( "#accordion" ).accordion();

	});