function NotBeforeToday(date){
    var now = new Date();//this gets the current date and time
    if (date.getFullYear() == now.getFullYear() && date.getMonth() == now.getMonth() && date.getDate() > now.getDate())
        return [true];
    if (date.getFullYear() >= now.getFullYear() && date.getMonth() > now.getMonth())
       return [true];
     if (date.getFullYear() > now.getFullYear())
       return [true];
    return [false];
}


 var roundTripDom = false;
 var roundTripInt = false;


$(function() {

$("#interDepartureDate").datepicker({ 
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    minDate: new Date(),
    maxDate: '+2y',
    onSelect: function(date){

        var selectedDate = new Date(date);
        var msecsInADay = 86400000;
        var endDate = new Date(selectedDate.getTime() + msecsInADay);

        $("#interReturnDate").datepicker({minDate: endDate});

    }
});

    function enableIntReturnDate(){
        var date = $('#interDepartureDate').val();
        var selectedDate = new Date(date);
        var msecsInADay = 86400000;
        var endDate = new Date(selectedDate.getTime() + msecsInADay);
        $("#interReturnDate").datepicker({
            minDate: endDate,
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            maxDate: '+2y',
        });
    }


$("#domDepartureDate").datepicker({ 
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    minDate: new Date(),
    maxDate: '+2y',
    onSelect: function(date){

        var selectedDate = new Date(date);
        var msecsInADay = 86400000;
        var endDate = new Date(selectedDate.getTime() + msecsInADay);

        $("#domReturnDate").datepicker({minDate: endDate});

    }
});

function enableDomReturnDate(){
        var date = $('#domDepartureDate').val();
        var selectedDate = new Date(date);
        var msecsInADay = 86400000;
        var endDate = new Date(selectedDate.getTime() + msecsInADay);
        $("#domReturnDate").datepicker({
            minDate: endDate,
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            maxDate: '+2y',
        });
    }

$('#interReturnDate').on('click', function (e) {
    enableIntReturnDate();
});

$('#domReturnDate').on('click', function (e) {
    enableDomReturnDate();
});

 $("#mode").val('ONE');

$("input[name='intetrip']").change(function(event){
	if(event.currentTarget.id == "intetripone"){
        $('#interReturnDate').attr('disabled','disabled');
        $('#interReturnDate').val("");
        $("#mode").val('ONE');
	}
	else if(event.currentTarget.id == "intetripround"){
        $('#interReturnDate').attr('disabled',false);
         $("#mode").val('ROUND');
        roundTripInt = true;
        enableIntReturnDate();
	}
});
$("input[name='domtrip']").change(function(event){
	if(event.currentTarget.id == "domtripone"){
         $('#domReturnDate').attr('disabled','disabled');
         $('#domReturnDate').val("");
         $("#mode").val('ONE');
	}
	else if(event.currentTarget.id == "domtripround"){
           $('#domReturnDate').attr('disabled',false);
            $("#mode").val('ROUND'); 
            roundTripDom = true;
           enableDomReturnDate();
	}
});

  });

function checkAllInformation(){
    var isDone = false;
    if($("#domestic").is(':visible')){

        $("#serviceType").val('DOM');
       
        $("#classPreferred").val($('#domclassPreferredInput').val());
        $("#carrierPreferred").val($('#domcarrierPreferredInput').val());

        if(checkInputField("origin","domesticdepartureInput","Please Select Departure City")){
            return false;
        }
        if(checkInputField("destination","domesticdestinationInput","Please Select Destination City")){
            return false;
        }
        if(checkInputField("departDate","domDepartureDate","Please Select Departure Date")){
            return false;
        }
        if(checkInputField("adultPax","domadultInput","Please Select Adult")){
            return false;
        }
        if(checkInputField("childPax","domchildInput","Please Select Child")){
            return false;
        }
        if(checkInputField("infantPax","dominfantInput","Please Select Infant")){
            return false;
        }

        if(roundTripDom && checkInputField("returnDate","domReturnDate","Please Select Return Date")){
                    return false;
        }

        isDone = true;


    }else if($("#international").is(':visible')){

        $("#serviceType").val('INT');
        
        
        $("#classPreferred").val($('#interclassPreferred').val());
        $("#carrierPreferred").val($('#intercarrierPreferredInput').val());

        if(checkInputField("origin","internationaldepartureInput","Please Select Departure City")){
            return false;
        }
        if(checkInputField("destination","internationaldestinationInput","Please Select Destination City")){
            return false;
        }
        if(checkInputField("departDate","interDepartureDate","Please Select Departure Date")){
            return false;
        }
        if(checkInputField("adultPax","interadultInput","Please Select Adult")){
            return false;
        }
        if(checkInputField("childPax","interchildInput","Please Select Child")){
            return false;
        }
        if(checkInputField("infantPax","interinfantInput","Please Select Infant")){
            return false;
        }

         if(roundTripInt && checkInputField("returnDate","interReturnDate","Please Select Return Date")){
                    return false;
                }

        isDone = true;
    }

    if(isDone){
        $("#airTicketForm").submit();
    }
}

function checkInputField(inputID, SelectBoxId, ErrorMsg){
    var selectedValue = $('#' +SelectBoxId).val();
    if(selectedValue == "" || selectedValue == null){
        alert(ErrorMsg);
        return true;
    }else{
       $('#' +inputID).val(selectedValue); 
        return false;
    }
}

$(document).ready(function() {
                $("#modifySearchBTN").click(function(){
                      $("#bookingBoxSearch").removeClass("HiddenSearchPage");
                      $("#homepageslider").removeClass("HiddenSearchPage");
                      $("#mainDiv").addClass("HiddenSearchPage");
                 });
});

function closeDiv () {
                      $("#bookingBoxSearch").addClass("HiddenSearchPage");
                      $("#homepageslider").addClass("HiddenSearchPage");
                      $("#mainDiv").removeClass("HiddenSearchPage");
}

function showDetailDiv(divId){

     if($("#showdetail" +divId).is(':visible')){
        $(".showDetailDiv" +divId).html('+ Show Details...');
        $("#showdetail" +divId).addClass('Hidden');
     }
     else{
        $(".showDetailDiv" +divId).html('- Show Details...');
        $("#showdetail" +divId).removeClass('Hidden');
     }
}

$('.responsive-tabs__list a').on('click', function (e) {
  
  e.preventDefault();
  var currentDivClass = e.currentTarget.className;
  $(this).parent().addClass('tablist1-tabactive');
  $(this).parent().siblings().removeClass('tablist1-tabactive');

  target = $(this).attr('href');

 $('.tab-content'+currentDivClass+' > div').not(target).hide();

  $(target).fadeIn(600);

}); 