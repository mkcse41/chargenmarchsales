//var basepath = "http://localhost:8080/chargenmarch";
function openMenu() {
	$('#menu').addClass('visible');
	/*$('#menu').removeClass('DispNone');*/
	$('#menuOverlay').removeClass('DispNone');
	/*  $('#menuOverlay').removeClass('DispNone');*/
}

function closeMenu() {
	$('#menu').removeClass('visible');
	$('#menuOverlay').addClass('DispNone');
	/*$('#menu').addClass('DispNone');*/
	/*$('#menuOverlay').addClass('DispNone');*/
}

$(window).scroll(
		function() {
			if ($(this).scrollTop() > 0) {
				$("#header").css("background",
						"rgba(49, 57, 71, 1) none repeat scroll 0 0");
				$("#header").addClass('headerscroll');
			} else {
				$("#header").css("background",
						"rgba(49, 57, 71, 0) none repeat scroll 0 0");
				$("#header").removeClass('headerscroll');
			}
		});

$(document).ready(
		function() {

			$(".overlay,.popupclose").click(function() {
				$(".popwrap").hide();
				$(".popwrap1").hide();
				$(".popup").hide();
				$(".popup1").hide();
				$("body").removeClass("body_fixed");
				$("#menuOverlay").addClass("DispNone");

				$("#OTPVerification").addClass("hidden");
				$("#loginOtp").addClass("hidden");
				$("#MobileVerification").addClass("hidden");
				$("#charger_registerDiv").removeClass("hidden");
			});

			$('#menuOverlay').click(function() {
				$('#menu').removeClass('visible');
				$('#menuOverlay').addClass('DispNone');
			});

			$('#mobileplanOverlay').click(function() {
				$('#mobilePlan').removeClass('visible');
				$('#mobileplanOverlay').addClass('DispNone');
			});

			$('#autopass').change(
					function() {
						if ($(this).is(":checked")) {
							var passwordInput = document
									.getElementById('charger_reg_password');
							$('#charger_reg_password').val("");
							$('#charger_reg_password').addClass('DispNoneImp');
							$('#showpassdiv').addClass('DispNoneImp')
						} else {
							$('#charger_reg_password').removeClass(
									'DispNoneImp');
							$('#showpassdiv').removeClass('DispNoneImp')
						}
						/*$('#textbox1').val($(this).is(':checked'));   */
					});

			$('#showpass').change(
					function() {
						if ($(this).is(":checked")) {
							var passwordInput = document
									.getElementById('charger_reg_password');
							passwordInput.type = 'text';
						} else {
							var passwordInput = document
									.getElementById('charger_reg_password');
							passwordInput.type = 'password';
						}
						/*$('#textbox1').val($(this).is(':checked'));   */
					});

		});

function isBlankOrNull(stringToCheck) {
	if (stringToCheck == '') {
		return true;
	} else
		return false;
}
/**
 * This method used to user register source wise 
 */
function userRegister(usernameId, emailId, mobileNo, password, source,
		isCAMLogin, errorId) {

	var nameValue = $('#' + usernameId).val();
	var emailIdValue = $('#' + emailId).val();
	var mobileNoValue = $('#' + mobileNo).val();
	var passwordValue = $('#' + password).val();
	var userReferralCode = $('#charger_reg_referral_code').val();

	if (isCAMLogin == 'false') {
		$('#autopass').prop('checked', true);
		passwordValue = "";
	}

	if (validateName($('#' + usernameId), errorId)
			&& validateEmail($('#' + emailId), errorId)
			&& validatePhone($('#' + mobileNo), errorId)
			&& validatePasswordReg($('#' + password), errorId)) {
		loaderStart();
		$.ajax({
			type : "Post",
			async : false,
			url : basepath + "/user/registered.htm",
			data : "name=" + nameValue.trim() + "&emailId=" + emailIdValue
					+ "&mobileNo=" + mobileNoValue + "&password="
					+ passwordValue + "&source=" + source + "&userReferralCode="+userReferralCode+"&isCAMLogin="
					+ isCAMLogin,
			success : function(response) {
				//alert(json.message);
				var json = JSON.parse(response);
				setTimeout(function() {
					loaderEnd();
				}, 300);
				if (json.message == "OTP_Screen") {
					/*redirectReviewOrder(); */
					if (isCAMLogin == 'false') {
						showOTPScreenforThirdParty();
					} else {
						showOTPScreen();
					}
				} else if (json.message == "User Already Registered") {
					redirectReviewOrder(errorId, json.message);
				} else {
					$('#' + errorId).html(json.message);
				}
			},
			error : function(e) {
				$('#' + errorId).html('Error: ' + e);
				alert('Error: ' + e);
			}
		});
	}
}

function userRegisterByThirdParty(usernameId, emailId, mobileNo, source,
		isCAMLogin, errorId) {
	var nameValue = $('#' + usernameId).val();
	var emailIdValue = $('#' + emailId).val();
	var mobileNoValue = $('#' + mobileNo).val();
	var passwordValue = "";

	if (validateName($('#' + usernameId), errorId)
			&& validateEmail($('#' + emailId), errorId)
			&& validatePhone($('#' + mobileNo), errorId)) {
		loaderStart();
		$.ajax({
			type : "Post",
			async : false,
			url : basepath + "/user/registeredOther.htm",
			data : "name=" + nameValue.trim() + "&emailId=" + emailIdValue
					+ "&mobileNo=" + mobileNoValue + "&password="
					+ passwordValue + "&source=" + source + "&isCAMLogin="
					+ isCAMLogin,
			success : function(response) {
				//alert(json.message);
				var json = JSON.parse(response);
				setTimeout(function() {
					loaderEnd();
				}, 300);
				console.log(json.message);
				console.log(isCAMLogin);
				if (json.message == "Show Mobile Number Screen.") {
					/*redirectReviewOrder(); */
					showMobileScreen();
				} else if (json.message == "User Already Registered"
						&& isCAMLogin == 'false') {
					redirectReviewOrder(errorId, json.message, isCAMLogin);
				} else {
					$('#' + errorId).html(json.message);
				}
			},
			error : function(e) {
				$('#' + errorId).html('Error: ' + e);
				alert('Error: ' + e);
			}
		});
	}
}

function addUserInfoByFbGoogle() {
	userRegisterByThirdParty('chargers_reg_name', 'charger_reg_emailId',
			'charger_reg_mobileNo', 'WEB', 'false', 'errorIdDemo');
}

function showMobileScreen() {
	$('#MobileVerification').removeClass('hidden');
	$('#loginDiv').addClass('hidden');
	var left = $('.popup1').width() / 2;
	left = parseInt(left) - 19;
	$('.popup1').css("margin-left", "-" + left + "px");
}

function submitUserMobile(mobileNo, errorId) {
	var mobileNumber = $('#' + mobileNo).val();
	$('#charger_reg_mobileNo').val(mobileNumber);
	userRegister('chargers_reg_name', 'charger_reg_emailId',
			'charger_reg_mobileNo', 'charger_reg_password', 'WEB', 'false',
			errorId);
}

function submitUserOTP(userOTP, errorId) {
	var userOTPValue = $('#' + userOTP).val();
	if (validateOTP($('#' + userOTP), errorId)) {
		loaderStart();
		$.ajax({
			type : "Post",
			async : false,
			url : basepath + "/user/otpverification.htm",
			data : "otp=" + userOTPValue.trim(),
			success : function(response) {
				//alert(json.message);
				var json = JSON.parse(response);
				setTimeout(function() {
					loaderEnd();
				}, 300);
				if (json.message == "User Registered Successfully") {
					redirectReviewOrder();
				} else {
					$('#' + errorId).html(json.message);
				}
			},
			error : function(e) {
				$('#' + errorId).html('Error: ' + e);
				alert('Error: ' + e);
			}
		});
	}
}

function showOTPScreen() {
	$('#OTPVerification').removeClass('hidden');
	$('#charger_registerDiv').addClass('hidden');
	var left = $('.popup1').width() / 2;
	left = parseInt(left) - 19;
	$('.popup1').css("margin-left", "-" + left + "px");
}

function showOTPScreenforThirdParty() {
	$('#MobileVerification').addClass('hidden');
	$('#loginOtp').removeClass('hidden');
	var left = $('.popup1').width() / 2;
	left = parseInt(left) - 19;
	$('.popup1').css("margin-left", "-" + left + "px");
}

function showOTPScreenforNonVerifiedUser() {
	$('#loginDiv').addClass('hidden');
	$('#loginOtp').removeClass('hidden');
	var left = $('.popup').width() / 2;
	left = parseInt(left) - 19;
	$('.popup').css("margin-left", "-" + left + "px");
}

function validatePasswordReg(passId, errorId) {
	if ($('#autopass').is(':checked')) {
		/*alert("vvv");*/
		return true;
	} else {
		var message = validatePassword(passId);
		if (message == "") {
			return true;
		} else {
			passId.focus();
			$('#' + errorId).html(message);
		}
	}

}

function userFeedback(name, emailId, mobileNo, message, errorId) {

	var nameValue = $('#' + name).val();
	var emailIdValue = $('#' + emailId).val();
	var mobileNoValue = $('#' + mobileNo).val();
	var messageValue = $('#' + message).val();

	if (validateName($('#' + name), errorId)
			&& validateEmail($('#' + emailId), errorId)
			&& validatePhone($('#' + mobileNo), errorId)) {
		loaderStart();
		$
				.ajax({
					type : "Post",
					// async : false,
					url : basepath + "/feedback/feedback.htm",
					data : "name=" + nameValue.trim() + "&emailId="
							+ emailIdValue + "&mobileNo=" + mobileNoValue
							+ "&message=" + messageValue,
					success : function(response) {
						//alert(json.message);
						var json = JSON.parse(response);
						setTimeout(function() {
							loaderEnd();
						}, 300);
						$('#' + name).val('');
						$('#' + emailId).val('');
						$('#' + mobileNo).val('');
						$('#' + message).val('');
						$('#' + errorId).css('color', 'green');
						$('#' + errorId).html(json.message);
						feedbackEmpty();
					},
					error : function(e) {
						alert('Error: ' + e);
					}
				});
	}
}

function findTarget(url) {
	location.href = url;
}


function findTargetInNewTab(url){
	window.open(url);
}

function dynamicPostRequestForm(path, params, method) {
	method = method || "post"; // Set method to post by default if not specified.
	// The rest of this code assumes you are not using a library.
	// It can be made less wordy if you use one.
	var form = document.createElement("form");
	form.setAttribute("method", method);
	form.setAttribute("action", path);
	for ( var key in params) {
		if (params.hasOwnProperty(key)) {
			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", key);
			hiddenField.setAttribute("value", params[key]);

			form.appendChild(hiddenField);
		}
	}

	document.body.appendChild(form);
	form.submit();
}

function userLogin(emailId, password, errorId) {
	var passwordValue = $('#' + password).val();
	var emailIdValue = $('#' + emailId).val();
	if (validateEmail($('#' + emailId), errorId) && passwordValue != undefined
			&& passwordValue != "") {
		loaderStart();
		$.ajax({
			type : "Post",
			url : basepath + "/user/login",
			data : "emailId=" + emailIdValue + "&password=" + passwordValue,
			success : function(response) {
				var json = JSON.parse(response);
				setTimeout(function() {
					loaderEnd();
				}, 300);
				if (json.message == "success") {
					redirectReviewOrder();
				} else if (json.message == "nonverified") {
					getOTPResponse(errorId, emailIdValue);
				} else {
					$('#' + errorId).html(
							'Please Enter Valid Email Id and password.');
				}
			},
			error : function(e) {
				$('#' + errorId).html('Error: ' + e);
				alert('Error: ' + e);
			}
		});
	}
	else{
		$('#' + errorId).html("Please Insert Valid Information");
	}
}

function getOTPResponse(errorId, emailIdValue) {
	loaderStart();
	$.ajax({
		type : "Post",
		async : false,
		url : basepath + "/user/otp_response.htm",
		data : "emailId=" + emailIdValue,
		success : function(response) {
			//alert(json.message);
			var json = JSON.parse(response);
			setTimeout(function() {
				loaderEnd();
			}, 300);
			console.log(json.message);
			if (json.message == "OTP_Screen") {
				showOTPScreenforNonVerifiedUser();
			} else {
				$('#' + errorId).html(json.message);
			}
		},
		error : function(e) {
			$('#' + errorId).html('Error: ' + e);
			alert('Error: ' + e);
		}
	});
}

function hideShowDiv(hideDivId, showDivId) {
	$("#" + hideDivId).hide();
	$("#" + showDivId).show();
}

function validatePassword(fld) {
	var error = "";
	var illegalChars = /[\W_]/; // allow only letters and numbers 
	if (fld.val() == "") {
		error = "Please Enter a password.\n";
	} else if ((fld.val().length < 7) || (fld.val().length > 20)) {
		error = "Please Enter Password at least 7 characters. \n";
	} /*else if (illegalChars.test(fld.val())) {
	       error = "Please Enter Password only letter and number.\n";
	   } else if (!((fld.val().search(/(a-z)+/)) && (fld.val().search(/(0-9)+/)))) {
	       error = "The password must contain at least one numeral.\n";
	   }*/
	return error;
}

function trim(s) {
	if (s != undefined && s != "") {
		return s.replace(/^\s+|\s+$/, '');
	}
	return s;
}

function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	} else {
		return true;
	}
}

function onlyAlphabets(e, t) {
	try {
		if (window.event) {
			var charCode = window.event.keyCode;
		} else if (e) {
			var charCode = e.which;
		} else {
			return true;
		}
		if (charCode == 32 || (charCode > 64 && charCode < 91)
				|| (charCode > 96 && charCode < 123))
			return true;
		else
			return false;
	} catch (err) {
		alert(err.Description);
	}
}

function spaceNotAllowed(e) {
	if (e.which == 32) {
		return false;
	}
}

function allowAlphaNumeric(e) {
	var regex = new RegExp("^[a-zA-Z0-9]+$");
	var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
	if (regex.test(str)) {
		return true;
	}

	e.preventDefault();
	return false;
}

function validateEmail(email, errorId) { //Validates the email address
	var emailRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (email.val() != undefined && email.val() != ""
			&& emailRegex.test(email.val())) {
		return true;
	}
	$('#' + errorId).html('Please Enter valid Email Id.');
	email.focus();
	return false;
}

function validateOTP(otp, errorId) {
	var OTPRegex = /^[0-9]{1,6}$/;
	if (otp.val() != undefined && otp.val() != "" && otp.val().length == 6
			&& OTPRegex.test(otp.val())) {
		return true;
	}
	$('#' + errorId).html('Please Enter Numeric OTP.');
	otp.focus();
	return false;
}

function validatePhone(phone, errorId) { //Validates the phone number
	var phoneRegex = /^[0]?[789]\d{9}$/; // Change this regex based on requirement
	if (phone.val() != undefined && phone.val() != ""
			&& phone.val().length == 10 && phoneRegex.test(phone.val())) {
		return true;
	}
	$('#' + errorId).html('Please Enter valid Mobile Number.');
	phone.focus();
	return false;
}

function validateName(uname, errorId) {
	var letters = /^[A-z ]+$/;
	if (uname.val() != undefined && uname.val() != ""
			&& trim(uname.val()).match(letters)) {
		return true;
	} else {
		$('#' + errorId).html('Username must have alphabet characters only');
		uname.focus();
		return false;
	}
}

function selectMobileServiceType(mobileServiceType) {
	if ($('input[name=' + mobileServiceType + ']:checked').length > 0) {
		var selectService = $(
				"input[type='radio'][name=" + mobileServiceType + "]:checked")
				.val();
		if (selectService == "prepaid") {
			$("#mobilePrepaidOperator").removeClass('DispNone');
			$("#mobilePostpaidOperator").addClass('DispNone');
			$("#mobilePlanLink").show();
			$("#rechargeMobileNumber").val('');
			$("#mobileState ").val('');
			$("#mobileState .dd-selected-value").val('');
			$("#mobileState .dd-selected").html('Select State');
			$("#mobilePrepaidOperator .dd-selected-value").val('');
			$("#mobilePrepaidOperator .dd-selected").html(
					'Select Mobile Operator');
		} else if (selectService == "postpaid") {
			$("#mobilePrepaidOperator").addClass('DispNone');
			$("#mobilePostpaidOperator").removeClass('DispNone');
			$("#mobilePlanLink").hide();
			$("#rechargeMobileNumber").val('');
			$("#mobileState .dd-selected-value").val('');
			$("#mobileState .dd-selected").html('Select State');
			$("#mobilePostpaidOperator .dd-selected-value").val('');
			$("#mobilePostpaidOperator .dd-selected").html(
					'Select Mobile Operator');
		}
	}
}

function hideShowRechargeDiv(rechargeDivId, stepNumber, totalStep) {
	var stepCount = parseInt(stepNumber);
	var totalCount = parseInt(totalStep);
	for (var i = 1; i <= totalStep; i++) {
		if (i == stepCount) {
			$("#" + rechargeDivId + i).css("display", "inline-block");
			$("#" + rechargeDivId + i).removeClass("DispNone");
		} else {
			$("#" + rechargeDivId + i).hide();

		}
	}

}

function loaderStart() {
	$("#loader-wrapper").removeClass("DispNone");
	$("#mask").removeClass("DispNone");

}

function loaderEnd() {
	$("#loader-wrapper").addClass("DispNone");
	$("#mask").addClass("DispNone");
}

function validateMobileNumber(phone) { //Validates the phone number
	var phoneRegex = /^[0]?[789]\d{9}$/; // Change this regex based on requirement
	if (phone.val() != undefined && phone.val() != ""
			&& phone.val().length == 10 && phoneRegex.test(phone.val())) {
		return true;
	}
	phone.focus();
	return false;
}

function getOperatorDetails(mobileNumberId, serviceName, mobilePrepaidOperator,
		mobilePostpaidOperator, mobileErrorMessage, mobileServiceType, state) {
	$("#mobileRechargePlan").html("");
	var sourceNumber = $("#" + mobileNumberId).val();
	var selectService = $(
			"input[type='radio'][name=" + mobileServiceType + "]:checked")
			.val();
	if (sourceNumber != undefined && sourceNumber != ""
			&& validateMobileNumber($("#" + mobileNumberId))) {
		if (serviceName == "Mobile") {
			$("#isMobilePlan").val('false');
		}
		loaderStart();
		$.ajax({
			type : "post",
			url : basepath + "/recharge/operatorDetails",
			cache : false,
			// async: false, //blocks window close 
			data : "mobileNumber=" + sourceNumber,
			success : function(response) {
				if (undefined != response && response != "") {
					var json = JSON.parse(JSON.stringify(response));
					if (selectService == "prepaid") {
						if (serviceName == 'Mobile') {
							$(
									"#" + mobilePrepaidOperator
											+ " .dd-selected-value").val(
									json.pay_code);
							$("#" + mobilePrepaidOperator + " .dd-selected")
									.html(json.operator);
						}
						$("#" + state + " .dd-selected").html(json.circle);
						$("#" + state + " .dd-selected-value").val(
								json.pay_circle_code);
						if (json.pay_code == "" || json.pay_code == undefined
								|| json.pay_circle_code == ""
								|| json.pay_circle_code == undefined) {
							$("#" + mobilePrepaidOperator + " .dd-selected")
									.html("Select Mobile Operator");
							$("#" + state + " .dd-selected").html(
									"Select State");
						}
						$(".mobileOperatorErrorMessage").html("");
						$(".mobileStateMessage").html("");
					} else if (selectService == "postpaid") {
						//$("#"+mobilePostpaidOperator+" .dd-selected-value").val(json.pay_code);
						//$("#"+mobilePostpaidOperator+" .dd-selected").html(json.operator);
						$("#" + state + " .dd-selected").html(json.circle);
						$("#" + state + " .dd-selected-value").val(
								json.pay_circle_code);
					}
					setTimeout(function() {
						loaderEnd();
					}, 300);
					$("." + mobileErrorMessage).html("");
				}
			},
			error : function() {
				$("." + mobileErrorMessage).html(
						"Please Enter Valid 10 Digits Mobile Number.");
				return false;
			}
		});
	} else {
		if (sourceNumber >= 10) {
			$("." + mobileErrorMessage).html(
					"Please Enter Valid 10 Digits Mobile Number.");
		}
		return false;
	}
}

function processedToMobileRechargeCoupon(checkUserLoginId,
		mobileRechargeService, rechargeMobileNumber, mobilePrepaidOperator,
		mobilePostpaidOperator, mobileRechargeAmount, mobileState) {
	var operator;
	var operatorText;
	var selectService;
	var checkUserLogin = $("#" + checkUserLoginId).val();
	var number = $("#" + rechargeMobileNumber).val();
	var amount = $("#" + mobileRechargeAmount).val();
	var url = basepath + "/recharge/review-order.htm";
	if ($('#quikRechargeMobile').is(":checked")) {
		url = basepath + "/recharge/quick-recharge-confirm-order.htm";
	}
	if ($('input[name=' + mobileRechargeService + ']:checked').length > 0) {
		selectService = $(
				"input[type='radio'][name=" + mobileRechargeService
						+ "]:checked").val();
		if (selectService == "prepaid") {
			operator = $("#" + mobilePrepaidOperator + " .dd-selected-value")
					.val();
			operatorText = $(
					"#" + mobilePrepaidOperator
							+ " .dd-selected .dd-selected-text").html();
			if (operatorText == undefined) {
				operatorText = $("#" + mobilePrepaidOperator + " .dd-selected")
						.html();
			}
		} else if (selectService == "postpaid") {
			operator = $("#" + mobilePostpaidOperator + " .dd-selected-value")
					.val();
			operatorText = $(
					"#" + mobilePostpaidOperator
							+ " .dd-selected .dd-selected-text").html();
		}
	}
	var state = $("#" + mobileState + " .dd-selected-value").val();
	var stateText = $("#" + mobileState + " .dd-selected").html();

	if (number == undefined || number == ""
			|| !validateMobileNumber($("#" + rechargeMobileNumber))) {
		$(".mobileNumberErrorMsg").html(
				"Please Enter Valid 10 digits Mobile Number");
		return false;
	} else {
		$(".mobileNumberErrorMsg").html("");
	}

	if (operator != undefined && operator != "") {
		$(".mobileOperatorErrorMessage").html("");
	} else {
		$(".mobileOperatorErrorMessage").html("Please Select Mobile Operator.");
	}

	if (state != undefined && state != "") {
		$(".mobileStateMessage").html("");
	} else {
		$(".mobileStateMessage").html("Please Select State.");
		return false;
	}

	if (selectService == "prepaid"
			&& (amount == undefined || amount == "" || parseInt(amount) < 10 || parseInt(amount) > 10000)) {
		$(".mobileRechargeAmountErrorMsg").html(
				'Sorry, but your amount must be between Rs. 10 and Rs. 9999.');
		return false;
	}
	if (selectService == "postpaid"
			&& (amount == undefined || amount == "" || parseInt(amount) < 100 || parseInt(amount) > 10000)) {
		$(".mobileRechargeAmountErrorMsg").html(
				'Sorry, but your amount must be between Rs. 100 and Rs. 9999.');
		return false;
	} else {
		$(".mobileRechargeAmountErrorMsg").html("");
	}

	if (checkUserLogin != undefined && checkUserLogin == "true") {
		showLoginPopUp();
		$("#number").val(number);
		$("#operatorValue").val(operator);
		$("#operatorText").val(operatorText);
		$("#amount").val(amount);
		$("#mobileService").val(selectService);
		$("#service").val('Mobile');
		$("#circleValue").val(state);
		$("#circleText").val(stateText);
	} else if (checkUserLogin != undefined && checkUserLogin == "false") {
		resetRechargeDetail();
		var jsonParam = [];
		jsonParam.push("number");
		jsonParam.push("operatorValue");
		jsonParam.push("operatorText");
		jsonParam.push("service");
		jsonParam.push("mobileRechargeService");
		jsonParam.push("amount");
		jsonParam.push("circleValue");
		jsonParam.push("circleName");
		var jsonParamValue = [];
		jsonParamValue.push(number);
		jsonParamValue.push(operator);
		jsonParamValue.push(operatorText);
		jsonParamValue.push('Mobile');
		jsonParamValue.push(selectService);
		jsonParamValue.push(amount);
		jsonParamValue.push(state);
		jsonParamValue.push(stateText);
		multipleDataPostRedirect(url, jsonParam, jsonParamValue);
	}
}
function redirectReviewOrder(errorId, message, isCAMLogin) {
	var service = $("#service").val();
	if (service == "LandLineBill") {
		redirectReviewOrderForBilling(errorId, message, isCAMLogin);
	} else if (service == "cricketLeague") {
		redirectPaymentGatewayCricketLeague(errorId, message, isCAMLogin);
	} else {
		redirectReviewOrderForRecharge(errorId, message, isCAMLogin);
	}
}

function redirectReviewOrderForBilling(errorId, message, isCAMLogin) {

	var operator = $("#operatorValue").val();
	var operatorText = $("#operatorText").val();
	var service = $("#service").val();
	;
	var landlineaccountno = $("#landlineaccountno").val();
	var landlinenumberstdcode = $("#landlinestdcode").val();
	var landlinenumber = $("#landlinenumberwithoutstd").val();
	var amount = $("#amount").val();

	if (message == undefined) {
		var url = basepath + "/billing/review-order.htm";
		if ($('#quikRechargeMobile').is(":checked")
				|| $('#quikRechargeDTH').is(":checked")
				|| $('#quikRechargeDataCard').is(":checked")) {
			url = basepath + "/billing/quick-recharge-confirm-order.htm";
		}
		if (operator != "" && operatorText != "" && service != ""
				&& landlinenumber != "" && amount != "") {
			var jsonParam = [];
			jsonParam.push("landlinenumber");
			jsonParam.push("stdcode");
			jsonParam.push("customeraccountno");
			jsonParam.push("operatorValue");
			jsonParam.push("operatorText");
			jsonParam.push("service");
			jsonParam.push("amount");
			var jsonParamValue = [];
			jsonParamValue.push(landlinenumber);
			jsonParamValue.push(landlinenumberstdcode);
			jsonParamValue.push(landlineaccountno);
			jsonParamValue.push(operator);
			jsonParamValue.push(operatorText);
			jsonParamValue.push('LandLineBill');
			jsonParamValue.push(amount);
			multipleDataPostRedirect(url, jsonParam, jsonParamValue);
		} else {
			location.href = basepath;
		}
	} else if (message != undefined && message == "User Already Registered"
			&& isCAMLogin == "false") {
		$("#" + errorId).html("User Already Register.");
		location.href = basepath;
		return false;
	} else if (message != undefined && message == "User Already Registered") {
		$("#" + errorId).html("User Already Register.");
		return false;
	}

}

function redirectReviewOrderForRecharge(errorId, message, isCAMLogin) {
	var number = $("#number").val();
	var operatorValue = $("#operatorValue").val();
	var operatorText = $("#operatorText").val();
	var amount = $("#amount").val();
	var mobileService = $("#mobileService").val();
	var service = $("#service").val();
	var circleValue = $("#circleValue").val();
	var circleText = $("#circleText").val();
	if (service == "DTH") {
		mobileService = "DTH";
	}

	if (message == undefined) {
		var url = basepath + "/recharge/review-order.htm";
		if ($('#quikRechargeMobile').is(":checked")
				|| $('#quikRechargeDTH').is(":checked")
				|| $('#quikRechargeDataCard').is(":checked")) {
			url = basepath + "/recharge/quick-recharge-confirm-order.htm";
		}
		if (number != undefined && operatorValue != undefined && operatorText != undefined
				&& amount != undefined && mobileService != undefined && service != undefined && number != "" && operatorValue != "" && operatorText != ""
				&& amount != "" && mobileService != "" && service != "") {
			var jsonParam = [];
			jsonParam.push("number");
			jsonParam.push("operatorValue");
			jsonParam.push("operatorText");
			jsonParam.push("service");
			jsonParam.push("mobileRechargeService");
			jsonParam.push("amount");
			jsonParam.push("circleValue");
			jsonParam.push("circleName");
			var jsonParamValue = [];
			jsonParamValue.push(number);
			jsonParamValue.push(operatorValue);
			jsonParamValue.push(operatorText);
			jsonParamValue.push(service);
			jsonParamValue.push(mobileService);
			jsonParamValue.push(amount);
			jsonParamValue.push(circleValue);
			jsonParamValue.push(circleText);
			resetRechargeDetail();
			multipleDataPostRedirect(url, jsonParam, jsonParamValue, 'post');
		} else {
			location.href = basepath + "/recharge/home";
		}
	} else if (message != undefined && message == "User Already Registered"
			&& isCAMLogin == "false") {
		$("#" + errorId).html("User Already Register.");
		location.href = basepath;
		return false;
	} else if (message != undefined && message == "User Already Registered") {
		$("#" + errorId).html("User Already Register.");
		return false;
	}

}

function redirectPaymentGatewayCricketLeague(errorId, message, isCAMLogin) {
	if (message == undefined) {
		$('#leagueSubmit').submit();
	} else if (message != undefined && message == "User Already Registered"
			&& isCAMLogin == "false") {
		$("#" + errorId).html("User Already Register.");
		location.href = basepath;
		return false;
	} else if (message != undefined && message == "User Already Registered") {
		$("#" + errorId).html("User Already Register.");
		return false;
	}

}

function processedToDTHRechargeCoupon(checkUserLoginId, rechargeNumberId,
		OperatorId, rechargeAmountId) {
	var operator;
	var operatorText;
	var selectService;
	var checkUserLogin = $("#" + checkUserLoginId).val();
	var number = $("#" + rechargeNumberId).val();
	var amount = $("#" + rechargeAmountId).val();
	var url = basepath + "/recharge/review-order.htm";
	if ($('#quikRechargeDTH').is(":checked")) {
		url = basepath + "/recharge/quick-recharge-confirm-order.htm";
	}
	operator = $("#" + OperatorId + " .dd-selected-value").val();
	operatorText = $("#" + OperatorId + " .dd-selected-text").html();
	if (operator == undefined || operator == "" || operatorText == undefined
			|| operatorText == "") {
		$(".dthOperatorErrorMsg").html('Please Select DTH Operator.');
		return false;
	} else {
		$(".dthOperatorErrorMsg").html('');
	}
	if (!validateDTHNumber(rechargeNumberId, "dthNumberMessageError",
			OperatorId)) {
		return false;
	} else {
		$(".dthNumberMessageError").html('');
	}

	if (amount == undefined || amount == "" || parseInt(amount) < 30
			|| parseInt(amount) > 10000) {
		$(".dthAmountMessage").html(
				'Sorry, but your amount must be between Rs. 10 and Rs. 10000.');
		return false;
	} else {
		$(".dthAmountMessage").html("");
	}

	if (checkUserLogin != undefined && checkUserLogin == "true") {
		showLoginPopUp();
		$("#number").val(number);
		$("#operatorValue").val(operator);
		$("#operatorText").val(operatorText);
		$("#amount").val(amount);
		$("#mobileService").val(selectService);
		$("#service").val('DTH');
	} else if (checkUserLogin != undefined && checkUserLogin == "false") {
		resetRechargeDetail();
		var jsonParam = [];
		jsonParam.push("number");
		jsonParam.push("operatorValue");
		jsonParam.push("operatorText");
		jsonParam.push("service");
		jsonParam.push("mobileRechargeService");
		jsonParam.push("amount");
		var jsonParamValue = [];
		jsonParamValue.push(number);
		jsonParamValue.push(operator);
		jsonParamValue.push(operatorText);
		jsonParamValue.push('DTH');
		jsonParamValue.push(selectService);
		jsonParamValue.push(amount);
		multipleDataPostRedirect(url, jsonParam, jsonParamValue);
	}
}

function processedToDatacardRechargeCoupon(checkUserLoginId,
		datacardRechargeService, rechargeDatacardNumber,
		datacardPrepaidOperator, datacardPostpaidOperator,
		datacardRechargeAmount, datacardState) {
	var operator;
	var operatorText;
	var selectService;
	var checkUserLogin = $("#" + checkUserLoginId).val();
	var number = $("#" + rechargeDatacardNumber).val();
	var amount = $("#" + datacardRechargeAmount).val();
	var url = basepath + "/recharge/review-order.htm";
	if ($('#quikRechargeDataCard').is(":checked")) {
		url = basepath + "/recharge/quick-recharge-confirm-order.htm";
	}
	if ($('input[name=' + datacardRechargeService + ']:checked').length > 0) {
		var selectService = $(
				"input[type='radio'][name=" + datacardRechargeService
						+ "]:checked").val();
		if (selectService == "prepaid") {
			operator = $("#" + datacardPrepaidOperator + " .dd-selected-value")
					.val();
			operatorText = $(
					"#" + datacardPrepaidOperator + " .dd-selected-text")
					.html();
		} else if (selectService == "postpaid") {
			operator = $("#" + datacardPostpaidOperator + " .dd-selected-value")
					.val();
			operatorText = $(
					"#" + datacardPostpaidOperator + " .dd-selected-text")
					.html();
		}
	}
	var state = $("#" + datacardState + " .dd-selected-value").val();
	var stateText = $("#" + datacardState + " .dd-selected").html();

	if (number == undefined || number == ""
			|| !validateMobileNumber($("#" + rechargeDatacardNumber))) {
		$(".DataCardNumberMessage").html(
				"Please Enter Valid 10 digits DataCard Number");
		return false;
	} else {
		$(".DataCardNumberMessage").html("");
	}

	if (operator != undefined && operator != "") {
		$(".datacardPrepaidOperatorError").html("");
	} else {
		$(".datacardPrepaidOperatorError").html(
				"Please Select Mobile Operator.");
	}

	if (state != undefined && state != "") {
		$(".DataCardStateErrorMessage").html("");
	} else {
		$(".DataCardStateErrorMessage").html("Please Select State.");
		return false;
	}

	if (amount == undefined || amount == "" || parseInt(amount) < 10
			|| parseInt(amount) > 10000) {
		$(".datacardRechargeAmountErrorMessage").html(
				'Sorry, but your amount must be between Rs. 10 and Rs. 10000.');
		return false;
	} else {
		$(".datacardRechargeAmountErrorMessage").html("");
	}

	if (checkUserLogin != undefined && checkUserLogin == "true") {
		showLoginPopUp();
		$("#number").val(number);
		$("#operatorValue").val(operator);
		$("#operatorText").val(operatorText);
		$("#amount").val(amount);
		$("#mobileService").val(selectService);
		$("#service").val('DataCard');
		$("#circleValue").val(state);
		$("#circleText").val(stateText);
	} else if (checkUserLogin != undefined && checkUserLogin == "false") {
		resetRechargeDetail();
		var jsonParam = [];
		jsonParam.push("number");
		jsonParam.push("operatorValue");
		jsonParam.push("operatorText");
		jsonParam.push("service");
		jsonParam.push("mobileRechargeService");
		jsonParam.push("amount");
		jsonParam.push("circleValue");
		jsonParam.push("circleName");
		var jsonParamValue = [];
		jsonParamValue.push(number);
		jsonParamValue.push(operator);
		jsonParamValue.push(operatorText);
		jsonParamValue.push('DataCard');
		jsonParamValue.push(selectService);
		jsonParamValue.push(amount);
		jsonParamValue.push(state);
		jsonParamValue.push(stateText);
		multipleDataPostRedirect(url, jsonParam, jsonParamValue);
	}
}

function processedToLandLineBillingRecharge(checkUserLoginId, landlineOperator,
		landlineAccountno, landlineNumberstdcode, landlineNumber,
		landlineRechargeAmount) {
	var operator;
	var operatorText;
	var selectService;
	var checkUserLogin = $("#" + checkUserLoginId).val();
	var landlineaccountno = $("#" + landlineAccountno).val();
	var landlinenumberstdcode = $("#" + landlineNumberstdcode).val();
	var landlinenumber = $("#" + landlineNumber).val();
	var amount = $("#" + landlineRechargeAmount).val();
	var url = basepath + "/billing/review-order.htm";
	if ($('#quikRechargeLandLine').is(":checked")) {
		url = basepath + "/billing/quick-recharge-confirm-order.htm";
	}
	operator = $("#" + landlineOperator + " .dd-selected-value").val();
	operatorText = $("#" + landlineOperator + " .dd-selected .dd-selected-text")
			.html();
	if (operatorText == undefined) {
		operatorText = $("#" + landlineOperator + " .dd-selected").html();
	}

	if (operator == undefined || operator == "") {
		$('.landlineOperatorError').html('Please Select Landline Operator.');
		return false;
	} else {
		$('.landlineOperatorError').html('');
	}

	if ($("#landlineAccountDiv").is(":visible")
			&& (landlineaccountno == undefined || landlineaccountno == "" || !validateLandLineAccountNumber(landlineaccountno))) {
		$('.landlineAccountNumberMessage').html(
				'Please enter a valid 10 digit Customer A/C Number');
		return false;
	} else {
		$(".landlineAccountNumberMessage").html("");
	}

	if ($("#landlinestd").is(":visible")
			&& (landlinenumberstdcode == undefined || landlinenumberstdcode == "")) {
		$('.landlineNumberstdcodeMessage')
				.html('Please enter a valid STD Code');
		return false;
	} else {
		$(".landlineNumberstdcodeMessage").html("");
	}

	if ($("#landlinenumberDiv").is(":visible")
			&& (landlinenumber == undefined || landlinenumber == "" || !validateLandlineumber(
					landlinenumber, operator))) {
		$('.landlineNumberMessage')
				.html('Please enter a valid LandLine Number');
		return false;
	} else {
		$(".landlineNumberMessage").html("");
	}

	if (amount == undefined || amount == "" || parseInt(amount) < 100
			|| parseInt(amount) > 10000) {
		$(".landlineRechargeAmountErrorMessage").html(
				'Sorry, but your amount must be between Rs. 100 and Rs. 9999.');
		return false;
	} else {
		$(".landlineRechargeAmountErrorMessage").html("");
	}

	if (checkUserLogin != undefined && checkUserLogin == "true") {
		showLoginPopUp();
		$("#landlineaccountno").val(landlineaccountno);
		$("#landlinestdcode").val(landlinenumberstdcode);
		$("#landlinenumberwithoutstd").val(landlinenumber);
		$("#operatorValue").val(operator);
		$("#operatorText").val(operatorText);
		$("#amount").val(amount);
		$("#service").val('LandLineBill');
	} else if (checkUserLogin != undefined && checkUserLogin == "false") {
		resetRechargeDetail();
		var jsonParam = [];
		jsonParam.push("landlinenumber");
		jsonParam.push("stdcode");
		jsonParam.push("customeraccountno");
		jsonParam.push("operatorValue");
		jsonParam.push("operatorText");
		jsonParam.push("service");
		jsonParam.push("amount");
		var jsonParamValue = [];
		jsonParamValue.push(landlinenumber);
		jsonParamValue.push(landlinenumberstdcode);
		jsonParamValue.push(landlineaccountno);
		jsonParamValue.push(operator);
		jsonParamValue.push(operatorText);
		jsonParamValue.push('LandLineBill');
		jsonParamValue.push(amount);
		multipleDataPostRedirect(url, jsonParam, jsonParamValue);
	}
}

function validateLandLineAccountNumber(landlineaccountno) {
	if (landlineaccountno.length != 10) {
		return false;
	}
	return true;
}

function validateLandlineumber(landlinenumber, operator) {
	if (operator != 'MTDL' && landlinenumber.length != 7) {
		return false;
	} else if (operator == 'MTDL' && landlinenumber.length != 8) {
		return false;
	}
	return true;
}

function resetRechargeDetail() {
	$("#number").val('');
	$("#operatorValue").val('');
	$("#operatorText").val('');
	$("#amount").val(amount);
	$("#mobileService").val('');
	$("#service").val('');
	$("#circleValue").val('');
	$("#circleText").val('');
}

function redeemCoupon(couponId, rechargeAmount, userEmailId, serviceType,
		couponErrorMessage, couponSpanId, amountId, walletBalanceId) {
	var couponName = $("#" + couponId).val();
	if (couponName != undefined && couponName !== "") {
		$
				.ajax({
					type : "Post",
					async : false,
					url : basepath + "/coupon/redeem",
					data : "couponName=" + couponName + "&amount="
							+ rechargeAmount + "&emailId=" + userEmailId
							+ "&couponServiceType=" + serviceType,
					success : function(response) {
						var json = JSON.parse(response);
						if (json != undefined && json != ""
								&& json.isCouponApply) {
							$("." + couponErrorMessage)
									.html(
											"Coupon applied successfully. On successful recharge, You'll receive a cashback of amount Rs. "
													+ json.couponReedomValue
													+ " in your wallet a.k.a CAMllet. ");
							$("#" + couponSpanId).html("Confirm Order");
						} else {
							$("." + couponErrorMessage).html(
									"Invalid coupon. Please try again.");
							$("." + couponErrorMessage).addClass("couponError");
							$("#" + couponSpanId).html("Confirm Order");
						}
					},
					error : function(e) {
						$("." + couponErrorMessage).html(
								"Error in Redeem Coupon");
						$("." + couponErrorMessage).addClass("couponError");
					}
				});
	} else {
		$("." + couponErrorMessage).html("Please Enter Coupon Name.");
		$("." + couponErrorMessage).addClass("couponError");
	}
}


function manageWalletBalance(walletBalanceId, isUseWalletId, paidAmountId,
		rewardsPointRedeemAmountId, totalAmount, totalWalletBalance,isUserReferralId,userReferralBalance) {
	var amount = parseInt($("#" + paidAmountId).html());
	var walletBalance = parseInt(totalWalletBalance);
	var userReferralBalance = parseInt(userReferralBalance);
	if( $("#" + isUserReferralId).prop("checked") == true){
		$(".userReferralErrorMessage").html("You can not avail any other feature like referral balance or reward points if you redeem wallet balance for recharge.");
		$("#" + isUseWalletId).attr("checked",false);
	}else if ($("#" + isUseWalletId).prop("checked") == true) {
		var paidAmount = 0;
		var redeemPointsAmount = parseInt($("#" + rewardsPointRedeemAmountId)
				.val());
		if (walletBalance >= amount) {
			paidAmount = walletBalance - amount;
			$("#" + paidAmountId).html('0');
			$("#" + walletBalanceId).html(paidAmount);
			$(".userReferralErrorMessage").html("");
		} else {
			paidAmount = amount - walletBalance;
			$("#" + paidAmountId).html(paidAmount);
			$("#" + walletBalanceId).html('0');
			isWalletBalance = false;
			$(".userReferralErrorMessage").html("Your CAMllet Balance is Less then Recharge Balance. Please add money in CAMllet");
		}
	} else {
		var calTotalAmount = parseInt(totalAmount);
		var redeemPointsAmount = parseInt($("#" + rewardsPointRedeemAmountId)
				.val());
		var paidAmount = 0;
		if (calTotalAmount >= redeemPointsAmount) {
			paidAmount = calTotalAmount - redeemPointsAmount;
		} else {
			paidAmount = redeemPointsAmount - calTotalAmount;
		}
		$("#" + paidAmountId).html(paidAmount);
		$("#" + walletBalanceId).html(totalWalletBalance);
		$(".userReferralErrorMessage").html("");
	}
}

function manageUserReferralBalance(walletBalanceId, isUseWalletId, paidAmountId,
		rewardsPointRedeemAmountId, totalAmount, totalWalletBalance,isUserReferralId,userReferralBalance) {
	var amount = parseInt($("#" + paidAmountId).html());
	var walletBalance = parseInt(totalWalletBalance);
	var userReferralBalance = parseInt(userReferralBalance);
	if ($("#" + isUseWalletId).prop("checked") == true) {
		$(".userReferralErrorMessage").html("You can not avail any other feature like wallet balance or reward points if you redeem referral amount for recharge.");
		$("#" + isUserReferralId).attr("checked",false);
	}else if ($("#" + isUserReferralId).prop("checked") == true) {
		var paidAmount = 0;
		var percentageAmount = parseFloat((amount * 3)/100); 
		percentageAmount = Math.round(percentageAmount);
		paidAmount = amount - percentageAmount;
		if(percentageAmount <= userReferralBalance){
			userReferralBalance = userReferralBalance - percentageAmount;
			$("#" + paidAmountId).html(paidAmount);
			$("#userReferralBalanceId").html(userReferralBalance);
		}else{
			$("#" + paidAmountId).html(paidAmount);
			$("#userReferralBalanceId").html('0');		
		    $(".userReferralErrorMessage").html('');
		}
	} else {
		var calTotalAmount = parseInt(totalAmount);
		var redeemPointsAmount = parseInt($("#" + rewardsPointRedeemAmountId)
				.val());
		var paidAmount = 0;
		if (calTotalAmount >= redeemPointsAmount) {
			paidAmount = calTotalAmount - redeemPointsAmount;
		} else {
			paidAmount = redeemPointsAmount - calTotalAmount;
		}
		$("#" + paidAmountId).html(paidAmount);
		$("#" + walletBalanceId).html(totalWalletBalance);
		$("#userReferralBalanceId").html(userReferralBalance);	
		$(".userReferralErrorMessage").html("");
	}
}

function redeemRewardsPoints(emailId, couponSpanId, totalAmount, amountId,
		isReedomRewardsPointsId, walletBalanceId, rewardsPointsId,
		rewardsPointRedeemAmountId) {
  if ($("#isUseReferral" ).prop("checked") == true){
       $(".userReferralErrorMessage").html("you can not avail any other feature like wallet balance or reward points if you redeem referral amount for recharge.");
  }else{
 	 if (emailId != undefined && emailId !== "") {
		$.ajax({
			type : "Post",
			async : false,
			url : basepath + "/rewardsPoints/redeemPoints",
			data : "emailId=" + emailId,
			success : function(response) {
				var json = JSON.parse(response);
				if (json != undefined && json != "") {
					var redeemPoints = parseInt(json.rewardsPoint_value);
					var amount = parseInt($("#" + amountId).html());
					var walletBalance = parseFloat($("#" + walletBalanceId)
							.html());
					$("#" + rewardsPointRedeemAmountId).val(redeemPoints);
					if (redeemPoints > amount) {
						$("#" + amountId).html('0');
						$("#" + walletBalanceId).html(
								walletBalance + (redeemPoints - amount));
					} else {
						$("#" + amountId).html(amount - redeemPoints);
					}
					$("#" + rewardsPointsId).html('0');
					$("#" + isReedomRewardsPointsId).val('true');
					$(".redeemRewardsErrorMessage").html(
							"Loyalty Reward Points redeemed successfully.");
					$("#" + couponSpanId).html("Confirm Order");
				} else {
					$(".redeemRewardsErrorMessage").html(
							"Error in Redeem Rewards Points.");
				}
			},
			error : function(e) {
				$(".redeemRewardsErrorMessage").html(
						"Error in Redeem Rewards Points.");
			}
		});
	}
  }
}

function selectMobilePlan(amount, mobileAmountId, mobilePlanId) {
	$("#" + mobileAmountId).val(amount);
	closeMobilePlan();
}

function getMobilePlan(mobilePlanId, mobileNumberId, planData) {
	$("#mobileRechargePlan").html("");
	$("#mobileRechargePlan").remove();
	var mobileNumber = $("#" + mobileNumberId).val();
	var operator;
	var operatorText;
	if ($('input[name=mobileRechargeService]:checked').length > 0) {
		var selectService = $(
				"input[type='radio'][name=mobileRechargeService]:checked")
				.val();
		if (selectService == "prepaid") {
			operator = $("#mobilePrepaidOperator .dd-selected-value").val();
		}
		operatorText = $(
				"#mobilePrepaidOperator .dd-selected .dd-selected-text").html();
		if (operatorText == undefined) {
			operatorText = $("#mobilePrepaidOperator .dd-selected").html();
		}
	}
	var state = $("#mobileState .dd-selected-value").val();
	var stateText = $("#mobileState .dd-selected .dd-selected-text").html();
	if (stateText == undefined) {
		stateText = $("#mobileState .dd-selected").html();
	}
	if (mobileNumber != undefined && mobileNumber != ""
			&& validateMobileNumber($("#" + mobileNumberId)) && operator != ""
			&& operator != undefined && state != undefined && state != "") {
		$("#operatorPlanName").html(operatorText + " Recharge Plans");
		loaderStart();
		$.ajax({
			type : "Post",
			// async : false,   
			url : basepath + "/recharge/getPlans",
			cache : false,
			data : "mobileNumber=" + mobileNumber + "&operatorCode=" + operator
					+ "&zoneCode=" + state,
			success : function(response) {
				if (response != undefined && response != "") {
					openMobilePlan();
					setTimeout(function() {
						loaderEnd();
					}, 300);
					$("#" + mobilePlanId).append(response);
					showMobilePlan('FTTMobileRechargePlanDiv', 'talktimetab');
				}
			},
			error : function(e) {
				alert("Error in Get Mobile Plan.");
				closeMobilePlan();
			}
		});
		$(".mobileOperatorErrorMessage").html("");
		$(".mobileStateMessage").html("");
	} else {
		closeMobilePlan();
		//$(".mobileNumberErrorMsg").html("Please Enter Valid 10 digits Mobile Number");
		$(".mobileOperatorErrorMessage").html("Please Select Mobile Operator.");
		$(".mobileStateMessage").html("Please Select State.");
		return false;
	}
}

function showMobilePlan(planDiv, categoryDiv) {
	$("#mobileRechargePlan>div").hide();
	$("#" + planDiv).show();
	$("#" + planDiv + ">div").show();
	$('#plansCategory a').removeClass('ActivePlanTab');
	$("#" + categoryDiv).addClass('ActivePlanTab');
}

function confirmOrder(couponId, serviceType, isRewardsPointsRedeemId,
		isUseWalletId) {
	var couponName = $("#" + couponId).val();
	var isUseWallet = "";
	var isRedeem = $("#" + isRewardsPointsRedeemId).val();
	if ($("#" + isUseWalletId).prop("checked") == true) {
		isUseWallet = "true";
	}
	var isUseReferral = '';
	if ($("#isUseReferral").prop("checked") == true) {
		isUseReferral = "true";
	}
	if (serviceType == 'LandLineBill') {
		var url = basepath + "/billing/confirm-order.htm";
	} else {
		var url = basepath + "/recharge/confirm-order.htm";
	}
	var jsonParam = [];
	jsonParam.push("couponName");
	jsonParam.push("serviceType");
	jsonParam.push("isRedeem");
	jsonParam.push("isUseWallet");
	jsonParam.push("isUseReferral");
	var jsonParamValue = [];
	jsonParamValue.push(couponName);
	jsonParamValue.push(serviceType);
	jsonParamValue.push(isRedeem);
	jsonParamValue.push(isUseWallet);
	jsonParamValue.push(isUseReferral);
	if(isWalletBalance){
	    multipleDataPostRedirect(url, jsonParam, jsonParamValue);
	}else{
		$(".userReferralErrorMessage").html("Your CAMllet Balance is Less then Recharge Balance. Please add money in CAMllet");
	}
}

function multipleDataPostRedirect(formAction, jsonParam, jsonParamValue, method) {
	if (method == undefined) {
		method = "post";
	}
	var form = "<form action='" + formAction + "' method='" + method
			+ "' id='insForm'>";
	var d = jsonParam.length;
	for (var b = 0; b < d; b++) {
		form = form + "<input type='hidden' name='" + jsonParam[b]
				+ "' value=\"" + jsonParamValue[b] + '" />';
	}
	form = form + "</form>";
	$("body").append(form);
	$("#insForm").submit();
}

function validateDTHNumber(dthNumberId, validateMessageId, operatorId) {
	var dthNumber = $("#" + dthNumberId).val();
	var operator = $("#" + operatorId + " .dd-selected-text").html();
	if (operator == "Dish TV" || operator == "DISH TV") {
		if (checkFirstDTHNumber(dthNumber, "0") && dthNumber.length == 11) {
			$("." + validateMessageId).html("");
			return true;
		} else {
			$("#" + dthNumberId).focus();
			$("." + validateMessageId)
					.html(
							"Dish TV Card Number starts with 0 and have only 11 digits.");
		}
	} else if (operator == "Reliance Digital TV"
			|| operator == "RELIANCE BIG TV") {
		if (checkFirstDTHNumber(dthNumber, "2") && dthNumber.length == 12) {
			$("." + validateMessageId).html("");
			return true;
		} else {
			$("#" + dthNumberId).focus();
			$("." + validateMessageId).html(
					"This Card Number starts with 2 and have only 12 digits.");
		}
	} else if (operator == "Airtel Digital TV" || operator == "AIRTEL DTH") {
		if (checkFirstDTHNumber(dthNumber, "3") && dthNumber.length == 10) {
			$("." + validateMessageId).html("");
			return true;
		} else {
			$("#" + dthNumberId).focus();
			$("." + validateMessageId)
					.html(
							"Airtel Customer ID starts with 3 and have only 10 digits.");
		}
	} else if (operator == "Tata Sky" || operator == "TATA SKY") {
		if (checkFirstDTHNumber(dthNumber, "1") && dthNumber.length == 10) {
			$("." + validateMessageId).html("");
			return true;
		} else {
			$("#" + dthNumberId).focus();
			$("." + validateMessageId).html(
					"This ID starts with 1 and have only 10 digits.");
		}
	} else if (operator == "Sun Direct" || operator == "SUN DIRECT DTH") {
		if ((checkFirstDTHNumber(dthNumber, "4") || checkFirstDTHNumber(
				dthNumber, "1"))
				&& dthNumber.length == 11) {
			$("." + validateMessageId).html("");
			return true;
		} else {
			$("#" + dthNumberId).focus();
			$("." + validateMessageId).html(
					"This Number starts with 4 or 1 and have only 11 digits.");
		}
	} else if (operator == "Videocon D2H" || operator == "VIDEOCON D2H") {
		if (dthNumber.length <= 14) {
			$("." + validateMessageId).html("");
			return true;
		} else {
			$("#" + dthNumberId).focus();
			$("." + validateMessageId).html(
					"Please Enter Valid Videocon D2H Smart Card Number.");
		}
	}
	return false;
}

function checkFirstDTHNumber(number, firstNumber) {
	if (number != undefined && number > 1
			&& stringStartsWith(number, firstNumber)) {
		return true;
	}
	return false;
}

function stringStartsWith(string, prefix) {
	return string.slice(0, prefix.length) == prefix;
}

function showLoginPopUp() {
	$(".popwrap1").hide();
	$(".popup1").hide();
	$(".popwrap").show();
	$("#menu").removeClass('visible');
	$("#menuOverlay").addClass('DispNone');
	$('#loginDiv').removeClass('hidden');
	setHeight();
	$(window).resize(function() {
		setHeight();
		if ($(window).height() > $('.popup').height()) {
			$('.popup').css({
				'position' : 'absolute',
				'left' : '50%',
				'top' : '0%',
				'z-index' : '1000',
				'margin-left' : -$('.popup').width() / 2
			});
		} else {
			$('.popup').css({
				'position' : 'absolute',
				'left' : '50%',
				'top' : '0',
				'z-index' : '1000',
				'margin-left' : -$('.popup').width() / 2,
				'margin-top' : '0'
			});
		}

	});
	$(window).trigger('resize');
	$(".popup").slideDown();
	$("body").addClass("body_fixed");
}

function showSignInPopUp() {
	$(".popwrap").hide();
	$(".popup").hide();
	$("#menu").removeClass('visible')
	$("#menuOverlay").addClass('DispNone');
	$(".popwrap1").show();
	setHeight();
	$(window).resize(function() {
		setHeight();
		if ($(window).height() > $('.popup1').height()) {
			$('.popup1').css({
				'position' : 'absolute',
				'left' : '50%',
				'top' : '0',
				'z-index' : '1000',
				'margin-left' : -$('.popup1').width() / 2,
				'margin-top' : '0'
			});
		}

		else {
			$('.popup1').css({
				'position' : 'absolute',
				'left' : '50%',
				'top' : '0',
				'z-index' : '1000',
				'margin-left' : -$('.popup1').width() / 2,
				'margin-top' : '0'
			});
		}

	});
	$(window).trigger('resize');
	$(".popup1").slideDown();
	$("body").addClass("body_fixed");
}

function forgotPassword(emailfieldId, errorId) {
	var emailId = $("#" + emailfieldId).val();
	if (!isBlankOrNull(emailId)) {
		$.ajax({
			type : "POST",
			url : basepath + "/user/forgotPassword",
			data : "emailId=" + emailId,
			/*  error : function(e) { 
			 $('#'+errorId).html('Error in changing password. Please try again');
			 } */
			success : function(response) {
				//alert(json.message);
				var json = JSON.parse(response);
				$('#' + errorId).html(json.message);
				$('#' + errorId).css('color', '#fff');

			},
			error : function(e) {
				$('#' + errorId).html('Error: ' + e);
				$('#' + errorId).css('color', 'red');
			}
		});
	} else {
		$('#' + errorId).html('Please enter an emailid');
		$('#' + errorId).css('color', 'red');
	}
}
function openMobilePlan() {
	$('#mobilePlan').addClass('visible');
	$('#mobileplanOverlay').removeClass('DispNone');
}

function closeMobilePlan() {
	$('#mobilePlan').removeClass('visible');
	$('#mobileplanOverlay').addClass('DispNone');
}

function showHideDiv(div1, div2) {
	$('.' + div1).css('border-bottom', '4px solid #fdd000');
	$('.' + div2).css('border-bottom', 'none');
	$('#' + div1).removeClass('DispNone');
	$('#' + div2).addClass('DispNone');
}

function updatePopUpInfo(url) {
	$.ajax({
		type : "Post",
		async : false,
		url : basepath + "/user/updatePopUpInfo.htm",
		success : function(response) {
			var json = JSON.parse(response);
			if (json.message == "successfully_updated") {
				window.location = url;
			} else {

			}
		},
		error : function(e) {
			$('#' + errorId).html('Error: ' + e);
			alert('Error: ' + e);
		}
	});
}
function setHeight() {
	windowHeight = $(window).innerHeight();
	$('.popwrap1').css('height', windowHeight);
};
//To Handle Enter press in Login window for password
$("#chargers_password").keyup(
		function(e) {
			userLoginOnEnterPress('chargers_emailId', 'chargers_password',
					'errorMessagelogin', e);
		});

//To Handle Enter press in Login window for email
$("#chargers_emailId").keyup(
		function(e) {
			userLoginOnEnterPress('chargers_emailId', 'chargers_password',
					'errorMessagelogin', e);
		});

function userLoginOnEnterPress(emailId, password, errorId, e) {
	if (e.keyCode == 13) {
		return userLogin(emailId, password, errorId);
	}
}

function createWebGAClickEventTrackingNonInteraction(category,event,label){
    try{
        ga('send', 'event', category, event, label, 0,true);
                //ga('send', 'event', category, event, label, 0,{nonInteraction: true});
    }catch(e){}
    
}

function addFundsRequest(){
    var amount = $("#addRequestamount").val();
    var paymentMethod = $("#campmethod").val();
    var bankName = $("#campaybankaccount").val();
    var banfRefId = $("#camBankrefid").val();
    var remark = $("#camremark").val();
    if(amount != undefined && paymentMethod != undefined && bankName!=undefined && banfRefId != undefined
    	&& amount != "" && amount >= 100 && paymentMethod != "" && bankName != "" && banfRefId != ""){
    	var addRequestamount = parseInt(amount);
		$.ajax({
				type : "POST",
				url : basepath + "/funds/addRequest",
				async:false,
				data : "amount=" + addRequestamount+"&paymentMethod="+paymentMethod+"&bankName="+bankName+"&bankReferenceId="+banfRefId+"&remarks="+remark,
				success : function(response) {
					if(response == "success"){
                         alert("Funds Request Added Successfully.");
                         location.href = basepath + "/user/profile";
					}else{
                         alert("Please Enter Valid Bank Reference Id.");
                         //location.href = basepath + "/user/profile";
					}
				},
				error : function(e) {
					alert("Error in Add Funds Request.");
					//location.href = basepath + "/user/profile";
				}
			});
    }else{
    	alert("Please Enter Valid Details.");
    }
}