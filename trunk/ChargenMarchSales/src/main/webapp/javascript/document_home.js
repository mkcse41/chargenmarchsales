// JavaScript Document
$(document).ready(function() {
	$("#plandata").hide();$("#plandata1").hide();
	$(".dataView").hide();$(".dthView").hide();$(".landlineView").hide();
	RESPONSIVEUI.responsiveTabs();
	/*$(".plan").click(function(){
		$("#plandata").slideDown("slow");
	});
	$(".plan1").click(function(){
		$("#plandata1").slideDown("slow");
	});*/
	$(".close1").click(function(){
		$("#plandata").slideUp("slow");
		$("#plandata1").slideUp("slow");
	});
	$(".mobile").click(function(){
		$(".mobileView").slideDown("slow");
		$(".dthView").slideUp("slow");
		$(".dataView").slideUp("slow");
		$(".landlineView").slideUp("slow");
		$(".mobile").addClass("active");$(".dth").removeClass("active");$(".data").removeClass("active");$(".landline").removeClass("active");
	});
	$(".dth").click(function(){
		$(".mobileView").slideUp("slow");
		$(".dthView").slideDown("slow");
		$(".dataView").slideUp("slow");
		$(".landlineView").slideUp("slow");
		$(".mobile").removeClass("active");$(".dth").addClass("active");$(".data").removeClass("active");$(".landline").removeClass("active");
	});
	$(".data").click(function(){
		$(".mobileView").slideUp("slow");
		$(".dthView").slideUp("slow");
		$(".dataView").slideDown("slow");
		$(".landlineView").slideUp("slow");
		$(".mobile").removeClass("active");$(".dth").removeClass("active");$(".data").addClass("active");$(".landline").removeClass("active");
	});	
	$(".landline").click(function(){
		$(".mobileView").slideUp("slow");
		$(".dthView").slideUp("slow");
		$(".dataView").slideUp("slow");
		$(".landlineView").slideDown("slow");
		$(".mobile").removeClass("active");$(".dth").removeClass("active");$(".data").removeClass("active");$(".landline").addClass("active");
	});	
	
	$("#close-modal").on('click', function () {
		$('#dd-modal').effect('drop');
	});

           
$('.mobilePrepaidOperator').ddslick({
	data: prepaidOperatorData,
	width: 300,
	imagePosition: "left",
	selectText: "Select Operator",
	onSelected: function (data) {
		console.log(data);
	}
});

$('.mobilepPostpaidOperator').ddslick({
	data: postpaidOperatorData,
	width: 300,
	imagePosition: "left",
	selectText: "Select Operator",
	onSelected: function (data) {
		console.log(data);
	}
});

$('.state').ddslick({
	data: circles,
	width: 300,
	imagePosition: "left",
	selectText: "Select State",
	onSelected: function (data) {
		console.log(data);
	}
});

$('.datacardState').ddslick({
	data: circles,
	width: 300,
	imagePosition: "left",
	selectText: "Select State",
	onSelected: function (data) {
		console.log(data);
	}
});

$('#dthdata').ddslick({
	data: dthOperatorData,
	width: 300,
	imagePosition: "left",
	selectText: "Select DTH Operator",
	onSelected: function (data) {
		console.log(data);
	}
});

$('.datacardPrepaidOperator').ddslick({
	data: dataCardOperator,
	width: 300,
	imagePosition: "left",
	selectText: "Select DataCard Operator",
	onSelected: function (data) {
		console.log(data);
	}
});

$('.landlineOperator').ddslick({
	data: landlineOperator,
	width: 300,
	imagePosition: "left",
	selectText: "Select Operator",
	onSelected: function (data) {
		if(data.selectedData.value == 'BSL' || data.selectedData.value == 'MTDL'){
			$('#landlineAccountDiv').show();
		}
		else if(data.selectedData.value != 'BSL' && data.selectedData.value != 'MTDL'){
		   $('#landlineAccountDiv').hide();	
		}

		if(data.selectedData.value == 'MTDL'){
		   $('#landlinestd').hide();	
		}else if(data.selectedData.value != 'MTDL'){
		   $('#landlinestd').show();	
		}
        $(".landlineAccountNumberMessage").html("");
		$('#landlinedivshowonoperator').show();

	}
});

$(".mnUnit input",this).focus(function(){
	$(this,".frm .frmControl").css("box-shadow","inset 0px -2px 0px 0px #069");
});
$(".mnUnit input",this).blur(function(){
	$(this,".frm.blackThm .frmControl").css("box-shadow","none");
});	
$(".dd-select").click(function(){
	$(".dd-options").css("border-top","solid 3px #069");
});

selectMobileServiceType('mobileRechargeService');

});

$(document).ready(function(){
	
	try{ 
		$("form").attr('autocomplete', 'off');
	}catch(e){}

	$(".checkoverlay").hide();
	$("#plandata").hide();
	$(".operator").hide();
	$("#accountdiv").hide();


	$(".slider").click(function(){
		$(".slider").animate({
			'left':-1000
		},2000);
		$(".mobopen").show();
	});

	$(".ope1").click(function(){
		$(".operator").slideToggle(1000);
	});

	$(".ope").click(function(){
		$(".slider").animate({
			'left':-1000
		},2000);
		$(".mobopen").show();
	});

	$( document.body ).on( 'click', '.dropdown-menu li', function( event ) {
		var $target = $( event.currentTarget );
		$target.closest( '.btn-group' )
		.find( '[data-bind="label"]' ).text( $target.text() )
		.end()
		.children( '.dropdown-toggle' ).dropdown( 'toggle' );
		return false;
	});

	$(".product").click(function(){
		$('.checkoverlay',this).animate({
			height: "toggle",
			opacity: "toggle"
		}, "fast");
	});
	
	RESPONSIVEUI.responsiveTabs();

	/*$(".plan").click(function(){
		$("#plandata").slideToggle("slow");
	});*/


$(".overlay,.popupclose").click(function(){
	$(".popwrap").hide();
	$(".popwrap1").hide();
	$(".popup").hide();
	$(".popup1").hide();
	$("body").removeClass("body_fixed");
	$("#menuOverlay").addClass("DispNone");

	$("#OTPVerification").addClass("hidden");
	$("#loginOtp").addClass("hidden");
	$("#MobileVerification").addClass("hidden");
	$("#charger_registerDiv").removeClass("hidden");
});

});


function addtext(str){
	if(str==""){
		document.loginform.email.value="Enter Email";
		document.registerform.email.value="Enter Email";
	}
	if(str==""){
		document.loginform.password.value="Enter Password";
		document.registerform.password.value="Enter Password";
	}
	if(str==""){
		document.registerform.cpassword.value="Enter Password";
	}
	if(str==""){
		document.couponform.coupon.value="Enter Coupon Code";
	}
}

function removetext(str){
	if(str=="email"){
		document.loginform.email.value="";	
		document.registerform.email.value="";	
	}
	if(str=="password"){
		document.loginform.password.value="";
		document.registerform.password.value="";	
	}
	if(str=="cpassword"){
		document.registerform.cpassword.value="";	
	}
	if(str=="coupon"){
		document.couponform.coupon.value="";
	}
	if(str=="couponmsg"){
		document.couponform.couponmsg.value="";	
	}
}

function addvalues(){
	//document.loginform.email.value="Enter Email";		
	document.loginform.password.value="Enter Password";
	document.registerform.email.value="Enter Email";		
	document.registerform.password.value="Enter Password";
	document.registerform.cpassword.value="Enter Password";
}

function addvalues1(){
	document.couponform.coupon.value="Enter Coupon Code";
}

function setHeight() {
	windowHeight = $(window).innerHeight();
	$('.popwrap').css('height', windowHeight);
};

