function getDataByMobile(fieldId){
var mobileNo = $('#'+fieldId).val();

		loaderStart();
				$.ajax({
					type : "Post",
					async : false,
					url : basepath + "/admin/getUserInformationByMobile",
					data : "mobileNo=" + mobileNo.trim(),
					success : function(response) {
						//alert(json.message);
						var json = JSON.parse(response);
						setTimeout(function() {
							loaderEnd();
						}, 300);
						showData(json);
					},
					error : function(e) {
						$('#' + errorId).html('Error: ' + e);
						alert('Error: ' + e);
					}
				});

}

function getDataByEmail(fieldId){
var emailId = $('#'+fieldId).val();

		loaderStart();
				$.ajax({
					type : "Post",
					async : false,
					url : basepath + "/admin/getUserInformationByEmail",
					data : "emailId=" + emailId.trim(),
					success : function(response) {
						//alert(json.message);
						var json = JSON.parse(response);
						setTimeout(function() {
							loaderEnd();
						}, 300);
						showData(json);
					},
					error : function(e) {
						$('#' + errorId).html('Error: ' + e);
						alert('Error: ' + e);
					}
				});

}

function getDataByUserId(fieldId){
var userId = $('#'+fieldId).val();

		loaderStart();
				$.ajax({
					type : "Post",
					async : false,
					url : basepath + "/admin/getUserInformationByUserId",
					data : "userId=" + userId,
					success : function(response) {
						//alert(json.message);
						var json = JSON.parse(response);
						console.log(json.length);
						setTimeout(function() {
							loaderEnd();
						}, 300);
						showData(json);
					},
					error : function(e) {
						$('#' + errorId).html('Error: ' + e);
						alert('Error: ' + e);
					}
				});

}

function showData (json) {
	$("#dataInnerDiv").html("");
	if(json != 'user not available'){
			var content = "";
			for (var i = 0; i < Object.keys(json).length; i++) {
				var adsad = Object.keys(json)[i];
				 content += '<div class="innerShowDataDiv"><span>' + Object.keys(json)[i] +'</span> <input type="text" id="'+Object.keys(json)[i]+'" name="'+Object.keys(json)[i]+'" value="'+json[adsad]+'"/></div>';
			};
			$("#dataInnerDiv").append(content);
			$("#updateButton").show();
    } else{
    	var content = "user not avaliable";
    	$("#dataInnerDiv").append(content);
    	$("#updateButton").hide();
    }
				
}

function getUserTrasactionHistoryByUserId (fieldId) {
   var userId = $('#'+fieldId).val();

		loaderStart();
				$.ajax({
					type : "Post",
					async : false,
					url : basepath + "/admin/getUserTransactionHistoryByUserId",
					data : "userId=" + userId.trim(),
					success : function(response) {
						//alert(json.message);
						
						setTimeout(function() {
							loaderEnd();
						}, 300);
						$("#transactionHistory").html("");
						$("#transactionHistory").append(response);
					},
					error : function(e) {
						$('#' + errorId).html('Error: ' + e);
						alert('Error: ' + e);
					}
				});
}


function updateUserInformation(id,name,emailId,mobileNo,rechargeCount,registrationDate,source,
	password,walletBalance,isverified,referralCode,referralBalance,BySource,errorId) {
	$('#successId').html("");
	$('#'+errorId).html("");

	var idValue = $('#' + id).val();
	var nameValue = $('#' + name).val();
	var emailValue = $('#' + emailId).val();
	var mobileNoValue = $('#' + mobileNo).val();
	var rechargeCountValue = $('#' + rechargeCount).val();
	var registrationDateValue = $('#' + registrationDate).val();
	var sourceValue = $('#' + source).val();
	var passwordValue = $('#' + password).val();
	var walletBalanceValue = $('#' + walletBalance).val();
	var isverifiedValue = $('#' + isverified).val();
	var referralCodeValue = $('#' + referralCode).val();
	var referralBalanceValue = $('#' + referralBalance).val();


	if (validateName($('#' + name), errorId)
			&& validateEmail($('#' + emailId), errorId)
			&& validatePhone($('#' + mobileNo), errorId)
			&& validatePasswordReg($('#' + password), errorId)) {

		if(BySource == 'ByEmail'){
				var correctUrl = basepath + "/admin/updateUserByEmailAdmin";
		}else{
				var correctUrl = basepath + "/admin/updateUserByUserIdAdmin";
		}
		loaderStart();
		$.ajax({
			type : "Post",
			async : false,
			url : correctUrl,
			data : "id=" + idValue + "&name=" + nameValue.trim() + "&emailId=" + emailValue
					+ "&mobileNo=" + mobileNoValue + "&rechargeCount=" + rechargeCountValue + 
					"&registrationDate=" + registrationDateValue +
					"&source=" + sourceValue +
					"&password=" + passwordValue +
					"&walletBalance=" + walletBalanceValue +
					"&isverified=" + isverifiedValue +
					 "&referralCode=" + referralCodeValue + 
					 "&referralBalance=" + referralBalanceValue ,
			success : function(response) {
				//alert(json.message);
				var json = JSON.parse(response);
				setTimeout(function() {
					loaderEnd();
				}, 300);
				if (json == "user updated successfully") {
					$('#successId').html(json);
				} else {
					$('#' + errorId).html(json);
				}
			},
			error : function(e) {
				$('#' + errorId).html('Error: ' + e);
				alert('Error: ' + e);
			}
		});
	}
}

function clearAllFields () {
	$("#dataInnerDiv").html();
	$("#updateButton").hide();
	$("#transactionHistory").hide();
}


function processedFundRequest(chargersId,bankRefId,amount){
	if(amount != undefined && amount > 0 && chargersId != undefined && chargersId != 0 && bankRefId != undefined && bankRefId != ""){
		loaderStart();
		var correctUrl = basepath + "/funds/processedRequest";
		$.ajax({
			type : "Post",
			async : false,
			url : correctUrl,
			data : "chargersId=" + chargersId + "&bankRefId=" + bankRefId.trim() + "&amount=" + amount,
			success : function(response) {
				//alert(json.message);
				var json = JSON.parse(response);
				setTimeout(function() {
					loaderEnd();
				}, 300);
				if (json == "success") {
					alert("Funds added in User Account successfully");
					location.href = basepath + "/admin/userInformation";
				} else {
					alert("Not Valid Details");
					location.href = basepath + "/admin/userInformation";
				}
			},
			error : function(e) {
				setTimeout(function() {
					loaderEnd();
				}, 300);
				alert('Error: ' + e);
				location.href = basepath + "/admin/userInformation";
			}
		});
	}
}