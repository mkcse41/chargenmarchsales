function updatePassword(oldPasswordId, newPasswordId, confirmNewPassId){
	var oldPasswordFromInput = $("#"+oldPasswordId).val();
	var newPassword = $("#"+newPasswordId).val();
	var confirmNewPass = $("#"+confirmNewPassId).val();
	if(isBlankOrNull(oldPasswordFromInput)){
		alert("Please enter your current password.");
		return;
	}
	if(isBlankOrNull(newPassword)){
		alert("Please enter your new desired password.");
		return;	
	}
	if(isBlankOrNull(confirmNewPass)){
		alert("Please confirm your new password.");
		return;
	}
    var isValidPassword = validatePassword($("#"+newPasswordId));
	if(isValidPassword!=""){
        alert(isValidPassword);
        return;
	}

	if(matchNewPasswords(newPassword, confirmNewPass)){
			$.ajax({  
				    type : "Post",
				    url : basepath+"/user/changePassword",   
				    data : "newPass=" + newPassword+"&oldPass="+oldPasswordFromInput,  
				    success : function(response) {  
				    //	var json = JSON.parse(response);  
				    	alert(response);
				    },  
				    error : function(e) {  
				     alert('Error in changing password. Please try again');   
				    }  
			    });
		} else {
			alert("Passwords do not match. Please check again.")
		}
	
}

function matchNewPasswords(newPass, confirmNewPass){
	if(newPass == confirmNewPass)
		return true;
	else return false;
}

function loadAmount(loadAmountId){
	  var amount = $("#"+loadAmountId).val();
	  if(amount!=undefined && amount!=""){
	  	    var url = basepath + "/user/addMoney";
		    var jsonParam = [];
		    jsonParam.push("amount");
		    var jsonParamValue = [];
		    jsonParamValue.push(amount);
		    multipleDataPostRedirect(url, jsonParam, jsonParamValue);
	  }else{
	  	 alert("Please Enter Valid Amount.");
	  }
}