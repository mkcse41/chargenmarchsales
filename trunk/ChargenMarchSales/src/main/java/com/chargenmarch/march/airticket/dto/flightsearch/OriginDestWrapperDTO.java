package com.chargenmarch.march.airticket.dto.flightsearch;

import java.io.Serializable;
import java.util.List;

public class OriginDestWrapperDTO implements Serializable{

	private static final long serialVersionUID = 5800202368849920139L;
	
	List<OriginDestinationOptionDTO> OriginDestinationOption;

	public List<OriginDestinationOptionDTO> getOriginDestinationOption() {
		return OriginDestinationOption;
	}

	public void setOriginDestinationOption(List<OriginDestinationOptionDTO> originDestinationOption) {
		this.OriginDestinationOption = originDestinationOption;
	}

	@Override
	public String toString() {
		return "OriginDestWrapperDTO [OriginDestinationOption=" + OriginDestinationOption + "]";
	}
	
}
