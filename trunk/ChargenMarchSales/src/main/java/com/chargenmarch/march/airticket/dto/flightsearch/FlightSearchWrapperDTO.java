package com.chargenmarch.march.airticket.dto.flightsearch;

import java.io.Serializable;

public class FlightSearchWrapperDTO implements Serializable {

	private static final long serialVersionUID = 8278846158835973505L;
	
	OriginDestWrapperDTO OriginDestinationOptions;

	public OriginDestWrapperDTO getOriginDestinationOptions() {
		return OriginDestinationOptions;
	}

	public void setOriginDestinationOptions(OriginDestWrapperDTO originDestinationOptions) {
		this.OriginDestinationOptions = originDestinationOptions;
	}

	@Override
	public String toString() {
		return "FlightSearchWrapperDTO [OriginDestinationOptions=" + OriginDestinationOptions + "]";
	}
	
}
