package com.chargenmarch.march.airticket.dto.flightsearch;

import java.io.Serializable;

public class BookingClassFareDTO implements Serializable {

	private static final long serialVersionUID = 5884063879897801809L;

	String adultFare;
	String bookingclass;       
	String childFare;
	String classType;          
	String farebasiscode;      
	String infantfare;
	String Rule;
	String adultCommission;
	String childCommission;
	String commissionOnTCharge;
	public String getAdultFare() {
		return adultFare;
	}
	public void setAdultFare(String adultFare) {
		this.adultFare = adultFare;
	}
	public String getBookingclass() {
		return bookingclass;
	}
	public void setBookingclass(String bookingclass) {
		this.bookingclass = bookingclass;
	}
	public String getChildFare() {
		return childFare;
	}
	public void setChildFare(String childFare) {
		this.childFare = childFare;
	}
	public String getClassType() {
		return classType;
	}
	public void setClassType(String classType) {
		this.classType = classType;
	}
	public String getFarebasiscode() {
		return farebasiscode;
	}
	public void setFarebasiscode(String farebasiscode) {
		this.farebasiscode = farebasiscode;
	}
	public String getInfantfare() {
		return infantfare;
	}
	public void setInfantfare(String infantfare) {
		this.infantfare = infantfare;
	}
	public String getRule() {
		return Rule;
	}
	public void setRule(String rule) {
		this.Rule = rule;
	}
	public String getAdultCommission() {
		return adultCommission;
	}
	public void setAdultCommission(String adultCommission) {
		this.adultCommission = adultCommission;
	}
	public String getChildCommission() {
		return childCommission;
	}
	public void setChildCommission(String childCommission) {
		this.childCommission = childCommission;
	}
	public String getCommissionOnTCharge() {
		return commissionOnTCharge;
	}
	public void setCommissionOnTCharge(String commissionOnTCharge) {
		this.commissionOnTCharge = commissionOnTCharge;
	}
	@Override
	public String toString() {
		return "BookingClassFareDTO [adultFare=" + adultFare + ", bookingclass=" + bookingclass + ", childFare="
				+ childFare + ", classType=" + classType + ", farebasiscode=" + farebasiscode + ", infantfare="
				+ infantfare + ", Rule=" + Rule + ", adultCommission=" + adultCommission + ", childCommission="
				+ childCommission + ", commissionOnTCharge=" + commissionOnTCharge + "]";
	}
	
}
