package com.chargenmarch.march.airticket.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.march.airticket.constants.AirTicketConstants;
import com.chargenmarch.march.airticket.dto.AirportCities;


public class AirPortCitiesMapper implements RowMapper<AirportCities>{

	@Override
	public AirportCities mapRow(ResultSet rs, int rowNum) throws SQLException {
		AirportCities airportCities = new AirportCities();
		airportCities.setAirportCode(rs.getString(AirTicketConstants.AIRPORT_CODE));
		airportCities.setAirportName(rs.getString(AirTicketConstants.AIRPORT_NAME));
		airportCities.setCountry_code(rs.getString(AirTicketConstants.COUNTRY_CODE));
		airportCities.setCountry_name(rs.getString(AirTicketConstants.COUNTRY_NAME));
		airportCities.setCity(rs.getString(AirTicketConstants.CITY));
		airportCities.setLatitude(rs.getString(AirTicketConstants.LATITUDE));
		airportCities.setLongitude(rs.getString(AirTicketConstants.LONGITUDE));
		airportCities.setTime_zone(rs.getString(AirTicketConstants.TIME_ZONE));
		airportCities.setCitydisplayname(rs.getString(AirTicketConstants.CITYDISPLAYNAME));
		airportCities.setCityType(rs.getString(AirTicketConstants.CITYTYPE));
		airportCities.setIsPopular(rs.getString(AirTicketConstants.ISPOPULAR));
		return airportCities;
	}

}
