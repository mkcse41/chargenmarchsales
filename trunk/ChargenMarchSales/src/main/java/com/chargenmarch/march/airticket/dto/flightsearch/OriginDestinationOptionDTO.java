package com.chargenmarch.march.airticket.dto.flightsearch;

import java.io.Serializable;

public class OriginDestinationOptionDTO implements Serializable{

	private static final long serialVersionUID = -2449244998365301252L;
	String id;
	String key;
	
	FareDetailsDTO FareDetails;
	
	FlightSegmentWrapperDTO FlightSegments;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public FareDetailsDTO getFareDetails() {
		return FareDetails;
	}

	public void setFareDetails(FareDetailsDTO fareDetails) {
		this.FareDetails = fareDetails;
	}

	public FlightSegmentWrapperDTO getFlightSegments() {
		return FlightSegments;
	}

	public void setFlightSegments(FlightSegmentWrapperDTO flightSegments) {
		this.FlightSegments = flightSegments;
	}

	@Override
	public String toString() {
		return "OriginDestinationOptionDTO [id=" + id + ", key=" + key + ", FareDetails=" + FareDetails
				+ ", FlightSegments=" + FlightSegments + "]";
	}
	
}
