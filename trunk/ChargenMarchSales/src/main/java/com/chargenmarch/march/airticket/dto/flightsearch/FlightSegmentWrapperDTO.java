package com.chargenmarch.march.airticket.dto.flightsearch;

import java.io.Serializable;
import java.util.List;

public class FlightSegmentWrapperDTO implements Serializable{

	private static final long serialVersionUID = 267568846530534707L;
	
	Object FlightSegment;
	
	public Object getFlightSegment() {
		return FlightSegment;
	}

	public void setFlightSegment(Object flightSegment) {
		FlightSegment = flightSegment;
	}

	@Override
	public String toString() {
		return "FlightSegmentWrapperDTO [FlightSegment=" + FlightSegment + "]";
	}

	
}
