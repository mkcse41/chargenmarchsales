package com.chargenmarch.march.airticket.dto.flightsearch;

import java.io.Serializable;

public class ChargeableFares implements Serializable {
	
	private static final long serialVersionUID = 2129878855525591522L;
	
	Double ActualBaseFare;
    Double Tax;
    Double STax;
    Double SCharge;
    Double TDiscount;
    Double TPartnerCommission;
    Double YDiscount;
	public Double getActualBaseFare() {
		return ActualBaseFare;
	}
	public void setActualBaseFare(Double actualBaseFare) {
		this.ActualBaseFare = actualBaseFare;
	}
	public Double getTax() {
		return Tax;
	}
	public void setTax(Double tax) {
		this.Tax = tax;
	}
	public Double getsTax() {
		return STax;
	}
	public void setsTax(Double sTax) {
		this.STax = sTax;
	}
	public Double getsCharge() {
		return SCharge;
	}
	public void setsCharge(Double sCharge) {
		this.SCharge = sCharge;
	}
	public Double gettDiscount() {
		return TDiscount;
	}
	public void settDiscount(Double tDiscount) {
		this.TDiscount = tDiscount;
	}
	public Double gettPartnerCommission() {
		return TPartnerCommission;
	}
	public void settPartnerCommission(Double tPartnerCommission) {
		this.TPartnerCommission = tPartnerCommission;
	}
	public Double getyDiscount() {
		return YDiscount;
	}
	public void setyDiscount(Double yDiscount) {
		this.YDiscount = yDiscount;
	}
	@Override
	public String toString() {
		return "ChargeableFares [actualBaseFare=" + ActualBaseFare + ", tax=" + Tax + ", sTax=" + STax + ", sCharge="
				+ SCharge + ", tDiscount=" + TDiscount + ", tPartnerCommission=" + TPartnerCommission + ", yDiscount="
				+ YDiscount + "]";
	}
	
}
