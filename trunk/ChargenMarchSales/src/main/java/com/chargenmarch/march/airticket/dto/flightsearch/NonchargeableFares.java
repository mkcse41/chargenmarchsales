package com.chargenmarch.march.airticket.dto.flightsearch;

import java.io.Serializable;

public class NonchargeableFares implements Serializable{
	private static final long serialVersionUID = -2274629566519837681L;
	
	Double TCharge;
	Double TMarkup;
	Double TSdiscount;
	public Double gettCharge() {
		return TCharge;
	}
	public void settCharge(Double tCharge) {
		this.TCharge = tCharge;
	}
	public Double gettMarkup() {
		return TMarkup;
	}
	public void settMarkup(Double tMarkup) {
		this.TMarkup = tMarkup;
	}
	public Double gettSdiscount() {
		return TSdiscount;
	}
	public void settSdiscount(Double tSdiscount) {
		this.TSdiscount = tSdiscount;
	}
	@Override
	public String toString() {
		return "NonchargeableFares [tCharge=" + TCharge + ", tMarkup=" + TMarkup + ", tSdiscount=" + TSdiscount + "]";
	}
	
}
