package com.chargenmarch.march.airticket.form.flightsearch;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.chargenmarch.common.constants.Constants;

public class FlightSearchForm {
	//private static final SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DateConstants.DEFAULT_DATE_FORMAT);
	
	@NotNull
	private String origin;
	
	@NotNull
	private String destination;
	
	@NotNull
	@DateTimeFormat(pattern=Constants.DateConstants.DEFAULT_DATE_FORMAT)
	private Date departDate;
	
	@DateTimeFormat(pattern=Constants.DateConstants.DEFAULT_DATE_FORMAT)
	private Date returnDate;
	
	@Min(1)
	private Integer adultPax = 1;
	
	@NotNull
	private Integer childPax = 0;
	private Integer infantPax = 0;
	
	@NotNull
	private String classPreferred;
	
	@NotNull
	private String mode;
	
	@NotNull
	private String serviceType;
	
	private String carrierPreferred;
	
	
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public Date getDepartDate() {
		return departDate;
	}
	public void setDepartDate(Date departDate) {
		this.departDate = departDate;
	}
	public Date getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}
	public Integer getAdultPax() {
		return adultPax;
	}
	public void setAdultPax(Integer adultPax) {
		this.adultPax = adultPax;
	}
	public Integer getChildPax() {
		return childPax;
	}
	public void setChildPax(Integer childPax) {
		this.childPax = childPax;
	}
	public Integer getInfantPax() {
		return infantPax;
	}
	public void setInfantPax(Integer infantPax) {
		this.infantPax = infantPax;
	}
	public String getClassPreferred() {
		return classPreferred;
	}
	public void setClassPreferred(String classPreferred) {
		this.classPreferred = classPreferred;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	
	public String getCarrierPreferred() {
		return carrierPreferred;
	}
	public void setCarrierPreferred(String carrierPreferred) {
		this.carrierPreferred = carrierPreferred;
	}
	
}
