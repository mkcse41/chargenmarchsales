package com.chargenmarch.march.airticket.dto.flightsearch;

import java.io.Serializable;
import java.util.Date;

public class FlightSegmentDTO implements Serializable{
	
	private static final long serialVersionUID = -5569621815125396871L;
	
	String AirEquipType;
	String ArrivalAirportCode;
	Date ArrivalDateTime;
	String DepartureAirportCode;
	Date DepartureDateTime;
	String FlightNumber;
	String OperatingAirlineCode;
	String OperatingAirlineFlightNumber;
	String RPH;
	String StopQuantity;
	String airLineName;
	String airportTax;
	String imageFileName;
	String viaFlight;
	Double Discount;
	String airportTaxChild;
	String airportTaxInfant;
	String adultTaxBreakup;
	String childTaxBreakup;
	String infantTaxBreakup;
	String octax;
	BookingClassDTO BookingClass;
	BookingClassFareDTO BookingClassFare;
	public String getAirEquipType() {
		return AirEquipType;
	}
	public void setAirEquipType(String airEquipType) {
		AirEquipType = airEquipType;
	}
	public String getArrivalAirportCode() {
		return ArrivalAirportCode;
	}
	public void setArrivalAirportCode(String arrivalAirportCode) {
		ArrivalAirportCode = arrivalAirportCode;
	}
	public Date getArrivalDateTime() {
		return ArrivalDateTime;
	}
	public void setArrivalDateTime(Date arrivalDateTime) {
		ArrivalDateTime = arrivalDateTime;
	}
	public String getDepartureAirportCode() {
		return DepartureAirportCode;
	}
	public void setDepartureAirportCode(String departureAirportCode) {
		DepartureAirportCode = departureAirportCode;
	}
	public Date getDepartureDateTime() {
		return DepartureDateTime;
	}
	public void setDepartureDateTime(Date departureDateTime) {
		DepartureDateTime = departureDateTime;
	}
	public String getFlightNumber() {
		return FlightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		FlightNumber = flightNumber;
	}
	public String getOperatingAirlineCode() {
		return OperatingAirlineCode;
	}
	public void setOperatingAirlineCode(String operatingAirlineCode) {
		OperatingAirlineCode = operatingAirlineCode;
	}
	public String getOperatingAirlineFlightNumber() {
		return OperatingAirlineFlightNumber;
	}
	public void setOperatingAirlineFlightNumber(String operatingAirlineFlightNumber) {
		OperatingAirlineFlightNumber = operatingAirlineFlightNumber;
	}
	public String getRPH() {
		return RPH;
	}
	public void setRPH(String rPH) {
		RPH = rPH;
	}
	public String getStopQuantity() {
		return StopQuantity;
	}
	public void setStopQuantity(String stopQuantity) {
		StopQuantity = stopQuantity;
	}
	public String getAirLineName() {
		return airLineName;
	}
	public void setAirLineName(String airLineName) {
		this.airLineName = airLineName;
	}
	public String getAirportTax() {
		return airportTax;
	}
	public void setAirportTax(String airportTax) {
		this.airportTax = airportTax;
	}
	public String getImageFileName() {
		return imageFileName;
	}
	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}
	public String getViaFlight() {
		return viaFlight;
	}
	public void setViaFlight(String viaFlight) {
		this.viaFlight = viaFlight;
	}
	public Double getDiscount() {
		return Discount;
	}
	public void setDiscount(Double discount) {
		Discount = discount;
	}
	public String getAirportTaxChild() {
		return airportTaxChild;
	}
	public void setAirportTaxChild(String airportTaxChild) {
		this.airportTaxChild = airportTaxChild;
	}
	public String getAirportTaxInfant() {
		return airportTaxInfant;
	}
	public void setAirportTaxInfant(String airportTaxInfant) {
		this.airportTaxInfant = airportTaxInfant;
	}
	public String getAdultTaxBreakup() {
		return adultTaxBreakup;
	}
	public void setAdultTaxBreakup(String adultTaxBreakup) {
		this.adultTaxBreakup = adultTaxBreakup;
	}
	public String getChildTaxBreakup() {
		return childTaxBreakup;
	}
	public void setChildTaxBreakup(String childTaxBreakup) {
		this.childTaxBreakup = childTaxBreakup;
	}
	public String getInfantTaxBreakup() {
		return infantTaxBreakup;
	}
	public void setInfantTaxBreakup(String infantTaxBreakup) {
		this.infantTaxBreakup = infantTaxBreakup;
	}
	public String getOctax() {
		return octax;
	}
	public void setOctax(String octax) {
		this.octax = octax;
	}
	public BookingClassDTO getBookingClass() {
		return BookingClass;
	}
	public void setBookingClass(BookingClassDTO bookingClass) {
		BookingClass = bookingClass;
	}
	public BookingClassFareDTO getBookingClassFare() {
		return BookingClassFare;
	}
	public void setBookingClassFare(BookingClassFareDTO bookingClassFare) {
		BookingClassFare = bookingClassFare;
	}
	
	@Override
	public String toString() {
		return "FlightSegmentDTO [AirEquipType=" + AirEquipType + ", ArrivalAirportCode=" + ArrivalAirportCode
				+ ", ArrivalDateTime=" + ArrivalDateTime + ", DepartureAirportCode=" + DepartureAirportCode
				+ ", DepartureDateTime=" + DepartureDateTime + ", FlightNumber=" + FlightNumber
				+ ", OperatingAirlineCode=" + OperatingAirlineCode + ", OperatingAirlineFlightNumber="
				+ OperatingAirlineFlightNumber + ", RPH=" + RPH + ", StopQuantity=" + StopQuantity + ", airLineName="
				+ airLineName + ", airportTax=" + airportTax + ", imageFileName=" + imageFileName + ", viaFlight="
				+ viaFlight + ", Discount=" + Discount + ", airportTaxChild=" + airportTaxChild + ", airportTaxInfant="
				+ airportTaxInfant + ", adultTaxBreakup=" + adultTaxBreakup + ", childTaxBreakup=" + childTaxBreakup
				+ ", infantTaxBreakup=" + infantTaxBreakup + ", octax=" + octax + ", BookingClass=" + BookingClass
				+ ", BookingClassFare=" + BookingClassFare + "]";
	}
	
}
