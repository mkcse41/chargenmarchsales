package com.chargenmarch.march.airticket.dto;

public class AirServiceProvider {
	Integer id;
	String carrier_name;
	String carrier_code;
	String service;
	String logo_url;
	Double margin_base;
	Double margin_yq;
	Double margin_segment;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCarrier_name() {
		return carrier_name;
	}
	public void setCarrier_name(String carrier_name) {
		this.carrier_name = carrier_name;
	}
	public String getCarrier_code() {
		return carrier_code;
	}
	public void setCarrier_code(String carrier_code) {
		this.carrier_code = carrier_code;
	}
	public String getLogo_url() {
		return logo_url;
	}
	public void setLogo_url(String logo_url) {
		this.logo_url = logo_url;
	}
	public Double getMargin_base() {
		return margin_base;
	}
	public void setMargin_base(Double margin_base) {
		this.margin_base = margin_base;
	}
	public Double getMargin_yq() {
		return margin_yq;
	}
	public void setMargin_yq(Double margin_yq) {
		this.margin_yq = margin_yq;
	}
	public Double getMargin_segment() {
		return margin_segment;
	}
	public void setMargin_segment(Double margin_segment) {
		this.margin_segment = margin_segment;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	@Override
	public String toString() {
		return "AirServiceProvider [carrier_name=" + carrier_name + ", carrier_code=" + carrier_code + ", service="
				+ service + ", logo_url=" + logo_url + ", margin_base=" + margin_base + ", margin_yq=" + margin_yq
				+ ", margin_segment=" + margin_segment + "]";
	}
	
}