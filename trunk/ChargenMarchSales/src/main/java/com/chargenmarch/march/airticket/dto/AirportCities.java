package com.chargenmarch.march.airticket.dto;

/**
 * 
 * @author mukesh
 *
 */
public class AirportCities {

	private String airportCode;
	private String airportName;
	private String country_code;
	private String country_name;
	private String longitude;
	private String latitude;
	private String time_zone;
	private String citydisplayname;
	private String cityType;
	private String city;
	private String isPopular;
	
	public String getAirportCode() {
		return airportCode;
	}
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}
	public String getAirportName() {
		return airportName;
	}
	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}
	public String getCountry_code() {
		return country_code;
	}
	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}
	public String getCountry_name() {
		return country_name;
	}
	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getTime_zone() {
		return time_zone;
	}
	public void setTime_zone(String time_zone) {
		this.time_zone = time_zone;
	}
	public String getCitydisplayname() {
		return citydisplayname;
	}
	public void setCitydisplayname(String citydisplayname) {
		this.citydisplayname = citydisplayname;
	}
	public String getCityType() {
		return cityType;
	}
	public void setCityType(String cityType) {
		this.cityType = cityType;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getIsPopular() {
		return isPopular;
	}
	public void setIsPopular(String isPopular) {
		this.isPopular = isPopular;
	}
	
	
}

