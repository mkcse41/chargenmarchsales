package com.chargenmarch.march.airticket.dto.flightsearch;

import java.io.Serializable;

public class FareDetailsDTO implements Serializable{

	private static final long serialVersionUID = -4683349820330899958L;

	ChargeableFares	ChargeableFares;
	NonchargeableFares NonchargeableFares;
	public ChargeableFares getChargeableFares() {
		return ChargeableFares;
	}
	public void setChargeableFares(ChargeableFares chargeableFares) {
		this.ChargeableFares = chargeableFares;
	}
	public NonchargeableFares getNonchargeableFares() {
		return NonchargeableFares;
	}
	public void setNonchargeableFares(NonchargeableFares nonchargeableFares) {
		this.NonchargeableFares = nonchargeableFares;
	}
	@Override
	public String toString() {
		return "FareDetailsDTO [ChargeableFares=" + ChargeableFares + ", NonchargeableFares=" + NonchargeableFares
				+ "]";
	}
	
}
