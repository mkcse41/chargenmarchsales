package com.chargenmarch.march.airticket.service;

import java.util.List;

import com.chargenmarch.march.airticket.dto.AirServiceProvider;
import com.chargenmarch.march.airticket.dto.AirportCities;
import com.chargenmarch.march.airticket.dto.flightsearch.FlightSearchDTO;
import com.chargenmarch.march.airticket.dto.flightsearch.FlightSearchResultDTO;
import com.chargenmarch.march.airticket.form.flightsearch.FlightSearchForm;

/**
 * 
 * @author mukesh
 *
 */
public interface IAirTicketService {

	/**
	 * Retrieves list of Air Ticket Service Providers From API. Save Them in DB
	 */
	public void insertServiceProvidersList(String providerType);

	/**
	 * Fetches list of all the Air Service Providers
	 * 
	 * @return List<AirServiceProvider>
	 */
	public List<AirServiceProvider> getAllServiceProviders();

	/**
	 * Returns a list of all the flight results as per user provided
	 * requirements
	 * 
	 * @param flightSearchForm
	 * @return FlightSearchResultDTO
	 */
	public FlightSearchResultDTO fetchFlightList(FlightSearchDTO flightSearchForm);
	public List<AirportCities> getAllAirportDomesticCities() ;
	public List<AirportCities> getAllAirportCities();
}
