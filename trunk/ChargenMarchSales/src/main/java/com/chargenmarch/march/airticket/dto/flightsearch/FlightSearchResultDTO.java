package com.chargenmarch.march.airticket.dto.flightsearch;

import java.io.Serializable;

public class FlightSearchResultDTO implements Serializable{

	private static final long serialVersionUID = 2193037795682327434L;
	FlightSearchWrapperDTO Response__Depart;
	FlightSearchWrapperDTO Response__Return;
	
	public FlightSearchWrapperDTO getResponse__Depart() {
		return Response__Depart;
	}
	public void setResponse__Depart(FlightSearchWrapperDTO response__Depart) {
		this.Response__Depart = response__Depart;
	}
	public FlightSearchWrapperDTO getResponse__Return() {
		return Response__Return;
	}
	public void setResponse__Return(FlightSearchWrapperDTO response__Return) {
		this.Response__Return = response__Return;
	}
	@Override
	public String toString() {
		return "FlightSearchResultDTO [Response__Depart=" + Response__Depart + ", Response__Return=" + Response__Return
				+ "]";
	}
	
}
