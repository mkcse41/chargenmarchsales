package com.chargenmarch.march.airticket.dao;

import java.util.List;

import com.chargenmarch.march.airticket.dto.AirServiceProvider;
import com.chargenmarch.march.airticket.dto.AirportCities;

public interface IAirTicketDao {
	/**
	 * Saves AirService Provider By Object
	 * @param airServiceProvider
	 * @return int
	 */
	public int saveAirService(AirServiceProvider airServiceProvider);
	
	/**
	 * Fetches list of all the Air Service Providers
	 * @return List<AirServiceProvider>
	 */
	public List<AirServiceProvider> getAllServiceProviders();
	public List<AirportCities> getAllAirportDomesticCities() ;
	public List<AirportCities> getAllAirportCities();
}
