package com.chargenmarch.march.airticket.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import ma.glasnost.orika.MapperFacade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.chargenmarch.camairline.service.IAirService;
import com.chargenmarch.common.constants.RequestConstants;
import com.chargenmarch.common.constants.ViewConstants;
import com.chargenmarch.common.utility.IUtility;
import com.chargenmarch.common.utility.impl.CustomMapper;
import com.chargenmarch.common.utility.impl.OrikaMapper;
import com.chargenmarch.march.airticket.constants.AirTicketConstants;
import com.chargenmarch.march.airticket.dto.AirServiceProvider;
import com.chargenmarch.march.airticket.dto.flightsearch.FlightSearchDTO;
import com.chargenmarch.march.airticket.dto.flightsearch.FlightSearchResultDTO;
import com.chargenmarch.march.airticket.form.flightsearch.FlightSearchForm;
import com.chargenmarch.march.airticket.service.IAirTicketService;

/**
 * @author Gambit
 * This class handles requests related to Air Ticketing 
 */
@Controller
@RequestMapping(value=RequestConstants.MarchRequest.MARCH_AIR)
public class AirTicketController {
	
	private final static Logger logger = LoggerFactory.getLogger(AirTicketController.class);
	
	@Autowired
	IAirTicketService airTicketService;
	

	@Autowired
	IUtility utility;
	
	@Autowired
	IAirService airService;
	
	@Autowired
	OrikaMapper mapper;
	
	@RequestMapping(value = "/serviceprovider/insert/{providerType}", method = RequestMethod.GET)
	public void insertAirServiceProviders(@PathVariable("providerType") String providerType) {
		airTicketService.insertServiceProvidersList(providerType);
	}
	
	@RequestMapping(value = "/serviceprovider/fetchall", method = RequestMethod.GET)
	public List<AirServiceProvider> fetchAllAirServiceProvider() {
		return airTicketService.getAllServiceProviders();
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public ModelAndView fetchFlightList(@Valid FlightSearchForm flightSearchForm, BindingResult bindingResult,HttpServletRequest request) {
		if(bindingResult.hasErrors()) {
			logger.error("FlightSearch Form Has Errors\n"+bindingResult);
			return null;
		}
		
		//fetch result
		FlightSearchDTO flightSearchDTO = mapper.map(flightSearchForm, FlightSearchDTO.class);
		flightSearchDTO.setDepartDate(flightSearchForm.getDepartDate());
		flightSearchDTO.setReturnDate(flightSearchForm.getReturnDate());
		//FlightSearchDTO flightSearchDTO = (FlightSearchDTO)CustomMapper.map(flightSearchForm, FlightSearchDTO.class);
		FlightSearchResultDTO flightSearchResultDTO = airTicketService.fetchFlightList(flightSearchDTO);
		
		//set variables and objects
		ModelAndView mnv = new ModelAndView(utility.getviewResolverName(request,ViewConstants.AIR_SEARCH_RESULT));
		mnv.addObject("flightSearchResultDTO", flightSearchResultDTO);
		mnv.addObject("flightSearchForm", flightSearchForm);
		mnv.addObject("passengerExtraInfo", airService.getPassengerExtraInfo());
		request.setAttribute(AirTicketConstants.AIR_ALL_CITIES, airService.getAllAirportCities());
		request.setAttribute(AirTicketConstants.AIR_DOMESTIC_CITIES, airService.getAllAirportDomesticCities());	
		request.setAttribute(AirTicketConstants.SERVICE_PROVIDER, airTicketService.getAllServiceProviders());
		return mnv;
	}

	@RequestMapping(value = "/airticket", method = RequestMethod.GET)
	public ModelAndView airticket(HttpServletRequest request) {
		ModelAndView mnv = new ModelAndView(utility.getviewResolverName(request,ViewConstants.AIR_TICKET_HOME));
		mnv.addObject("passengerExtraInfo", airService.getPassengerExtraInfo());
		request.setAttribute(AirTicketConstants.AIR_ALL_CITIES, airService.getAllAirportCities());
		request.setAttribute(AirTicketConstants.AIR_DOMESTIC_CITIES, airService.getAllAirportDomesticCities());	
		request.setAttribute(AirTicketConstants.SERVICE_PROVIDER, airTicketService.getAllServiceProviders());
		return mnv;
	}
	
	@RequestMapping(value = "/airjar", method = RequestMethod.POST)
	public @ResponseBody com.chargenmarch.camairline.dto.flightsearch.FlightSearchResultDTO airticketjar(com.chargenmarch.camairline.form.FlightSearchForm flightSearchForm) {
		return airService.fetchFlightList(flightSearchForm);
	}
	
}
