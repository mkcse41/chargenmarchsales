package com.chargenmarch.march.airticket.query;

public interface AirTicketQueries {

	public static final String INSERT_INTO_AIR_SERVICE_PROVIDER = "insert into air_service_provider set carrier_name = :carrier_name, carrier_code=:carrier_code, service = :service, logo_url=:logo_url, margin_base=:margin_base, margin_yq=:margin_yq, margin_segment=:margin_segment"; 
	public static final String SELECT_ALL_AIR_SERVICE_PROVIDER = "select * from air_service_provider";
	public static final String SELECT_ALL_AIR_DOMESTIC_CITY_INFO = "select * from airline_cityinfo where cityType='domestic' order by city";
	public static final String SELECT_ALL_AIR_INTERNATIONAL_CITY = "select * from airline_cityinfo order by city";
}