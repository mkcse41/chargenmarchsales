package com.chargenmarch.march.airticket.constants;

public interface AirTicketConstants {
	public static final String DOMESTIC_PROVIDER = "domestic";
	public static final String INTERNATIONAL_PROVIDER = "international";
	public static final String AIRPORT_CODE = "airport_code";
	public static final String AIRPORT_NAME = "airport_name";
	public static final String COUNTRY_CODE = "country_code";
	public static final String COUNTRY_NAME = "country_name";
	public static final String CITY = "city";
	public static final String LONGITUDE = "longitude";
	public static final String LATITUDE = "latitude";
	public static final String TIME_ZONE = "time_zone";
	public static final String ISPOPULAR = "isPopular";
	public static final String CITYDISPLAYNAME = "citydisplayname";
	public static final String CITYTYPE = "cityType";
	public static final String AIR_DOMESTIC_CITIES = "airDomesticCities";
	public static final String AIR_ALL_CITIES = "airALLCities";
	public static final String SERVICE_PROVIDER = "serviceProvider";
}
