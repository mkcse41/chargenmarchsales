package com.chargenmarch.march.airticket.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.chargenmarch.march.airticket.dao.IAirTicketDao;
import com.chargenmarch.march.airticket.dto.AirServiceProvider;
import com.chargenmarch.march.airticket.dto.AirportCities;
import com.chargenmarch.march.airticket.mapper.AirPortCitiesMapper;
import com.chargenmarch.march.airticket.mapper.AirServiceProviderMapper;
import com.chargenmarch.march.airticket.query.AirTicketQueries;

/**
 * @author Gambit
 * DAO Layer for Air Ticketing
 */
@Repository
public class AirTicketDaoImpl implements IAirTicketDao {

	@Autowired
	private NamedParameterJdbcTemplate namedParamJdbcTemplate;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/**
	 * Saves AirService Provider By Object
	 * @param airServiceProvider
	 * @return int
	 */
	public int saveAirService(AirServiceProvider airServiceProvider) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(airServiceProvider);
		return namedParamJdbcTemplate.update(AirTicketQueries.INSERT_INTO_AIR_SERVICE_PROVIDER, params);
	}
	
	/**
	 * Fetches list of all the Air Service Providers
	 * @return List<AirServiceProvider>
	 */
	public List<AirServiceProvider> getAllServiceProviders() {
		return namedParamJdbcTemplate.query(AirTicketQueries.SELECT_ALL_AIR_SERVICE_PROVIDER, new AirServiceProviderMapper());
	}
	
	
	/**
	 * Fetches list of all airport domestic cities.
	 * @return List<AirServiceProvider>
	 */
	public List<AirportCities> getAllAirportDomesticCities() {
		return jdbcTemplate.query(AirTicketQueries.SELECT_ALL_AIR_DOMESTIC_CITY_INFO, new AirPortCitiesMapper());
	}
	
	/**
	 * Fetches list of all airport domestic cities.
	 * @return List<AirServiceProvider>
	 */
	public List<AirportCities> getAllAirportCities() {
		return jdbcTemplate.query(AirTicketQueries.SELECT_ALL_AIR_INTERNATIONAL_CITY, new AirPortCitiesMapper());
	}
}
