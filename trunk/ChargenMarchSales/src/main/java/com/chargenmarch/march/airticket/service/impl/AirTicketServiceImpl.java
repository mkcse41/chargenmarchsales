package com.chargenmarch.march.airticket.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.FilePathConstants;
import com.chargenmarch.common.constants.PropertyKeyConstants;
import com.chargenmarch.common.service.http.IHTTPService;
import com.chargenmarch.common.utility.IUtility;
import com.chargenmarch.march.airticket.constants.AirTicketConstants;
import com.chargenmarch.march.airticket.dao.IAirTicketDao;
import com.chargenmarch.march.airticket.dto.AirServiceProvider;
import com.chargenmarch.march.airticket.dto.AirportCities;
import com.chargenmarch.march.airticket.dto.flightsearch.FlightSearchDTO;
import com.chargenmarch.march.airticket.dto.flightsearch.FlightSearchResultDTO;
import com.chargenmarch.march.airticket.form.flightsearch.FlightSearchForm;
import com.chargenmarch.march.airticket.service.IAirTicketService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * @author Gambit Service Layer for Air Ticketing
 */
@Service
@PropertySource(FilePathConstants.CLASSPATH + FilePathConstants.AIR_TICKET_PROPERTIES)
public class AirTicketServiceImpl implements IAirTicketService {

	private final static Logger logger = LoggerFactory.getLogger(AirTicketServiceImpl.class);

	@Autowired
	IHTTPService httpService;

	@Autowired
	IUtility utility;

	@Autowired
	IAirTicketDao airTicketDao;

	@Autowired
	Environment env;

	/**
	 * Retrieves list of Air Ticket Service Providers From API. Save Them in DB
	 */
	public void insertServiceProvidersList(String providerType) {
		int count = 0;
		String url = null;
		if (AirTicketConstants.DOMESTIC_PROVIDER.equalsIgnoreCase(providerType)) {
			url = env.getProperty(PropertyKeyConstants.AirTicketProperties.SERVICE_PROVIDER_URL_DOMESTIC);
		} else if (AirTicketConstants.INTERNATIONAL_PROVIDER.equalsIgnoreCase(providerType)) {
			url = env.getProperty(PropertyKeyConstants.AirTicketProperties.SERVICE_PROVIDER_URL_INTERNATIONAL);
		}
		String response = httpService.HTTPGetRequest(url);
		Gson gson = utility.getGson();

		List<AirServiceProvider> airServiceProviderList = null;
		try {
			airServiceProviderList = gson.fromJson(response, new TypeToken<List<AirServiceProvider>>() {
			}.getType());
			if (airServiceProviderList != null && airServiceProviderList.size() > 0) {
				for (AirServiceProvider airServiceProvider : airServiceProviderList) {
					try {
						airTicketDao.saveAirService(airServiceProvider);
						++count;
					} catch (Exception e) {
						logger.error("Exception in inserting Air Service Provider : " + airServiceProvider, e);
					}
				}
			}
		} catch (Exception e) {
			logger.error("Please Check Air Service Provider API Response. Some Error Occured.", e);
		}

		logger.debug(count + " Service Providers Inserted in DB.");
	}

	/**
	 * Fetches list of all the Air Service Providers
	 * 
	 * @return List<AirServiceProvider>
	 */
	@Cacheable("airServiceProviderCache")
	public List<AirServiceProvider> getAllServiceProviders() {
		return airTicketDao.getAllServiceProviders();
	}

	/**
	 * Returns a list of all the flight results as per user provided
	 * requirements
	 * 
	 * @param flightSearchForm
	 * @return FlightSearchResultDTO
	 */
	public FlightSearchResultDTO fetchFlightList(FlightSearchDTO flightSearchDTO) {
		Map<String, String> paramMap = utility.convertObjectToMap(flightSearchDTO);
		String url = env.getProperty(PropertyKeyConstants.AirTicketProperties.AIR_SEARCH_API);
		String response = httpService.HTTPPostRequest(url, paramMap);
		Gson gson = utility.getGson(Constants.DateConstants.FLIGHT_SEARCH_DATE_FORMAT);

		FlightSearchResultDTO flightSearchList = null;

		try {
			flightSearchList = gson.fromJson(response, new TypeToken<FlightSearchResultDTO>() {
			}.getType());
		} catch (Exception e) {
			logger.error("Please Check Air Search API Response. Some Error Occured.\nJSON Response is : "+response);
		}
		return flightSearchList;
	}
	
	@Cacheable("airportDomesticCityCache")
	public List<AirportCities> getAllAirportDomesticCities(){
		return airTicketDao.getAllAirportDomesticCities();
	}

	@Cacheable("airportInternationalCityCache")
	public List<AirportCities> getAllAirportCities(){
		return airTicketDao.getAllAirportCities();
	}
}
