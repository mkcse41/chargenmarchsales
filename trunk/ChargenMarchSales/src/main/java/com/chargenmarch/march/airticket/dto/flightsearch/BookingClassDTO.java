package com.chargenmarch.march.airticket.dto.flightsearch;

import java.io.Serializable;

public class BookingClassDTO implements Serializable{

	private static final long serialVersionUID = 2011497752845530851L;
	
	String Availability;
    String ResBookDesigCode;
	public String getAvailability() {
		return Availability;
	}
	public void setAvailability(String availability) {
		this.Availability = availability;
	}
	public String getResBookDesigCode() {
		return ResBookDesigCode;
	}
	public void setResBookDesigCode(String resBookDesigCode) {
		this.ResBookDesigCode = resBookDesigCode;
	}
	@Override
	public String toString() {
		return "BookingClassDTO [Availability=" + Availability + ", ResBookDesigCode=" + ResBookDesigCode + "]";
	}
    
}
