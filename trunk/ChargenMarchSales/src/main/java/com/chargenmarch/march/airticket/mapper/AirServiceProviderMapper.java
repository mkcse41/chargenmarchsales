package com.chargenmarch.march.airticket.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.march.airticket.dto.AirServiceProvider;

public class AirServiceProviderMapper implements RowMapper<AirServiceProvider>{

	@Override
	public AirServiceProvider mapRow(ResultSet rs, int rowNum) throws SQLException {
		AirServiceProvider airServiceProvider = new AirServiceProvider();
		airServiceProvider.setId(rs.getInt("id"));
		airServiceProvider.setCarrier_code(rs.getString("carrier_code"));
		airServiceProvider.setCarrier_name(rs.getString("carrier_name"));
		airServiceProvider.setLogo_url(rs.getString("logo_url"));
		airServiceProvider.setMargin_base(rs.getDouble("margin_base"));
		airServiceProvider.setMargin_yq(rs.getDouble("margin_yq"));
		airServiceProvider.setMargin_segment(rs.getDouble("margin_segment"));
		airServiceProvider.setService(rs.getString("service"));
		return airServiceProvider;
	}

}
