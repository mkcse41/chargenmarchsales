package com.chargenmarch.campaign.user.Query;

public class UserCampaignQuery {
	public static final String INSERT_CAMPAIGN_TRACKING_LOG = "insert into campaign_tracking_log set ip_address=:ipaddress, city=:city, request_url=:request_url, session_id=:session_id, source=:source,browser=:browser,os=:operatingSystem,timestamp=now()";
	
	public static final String INSERT_CAMPAIGN_TRACKING_DETAIL_LOG = "insert into campaign_tracking_detail_log set request_url=:request_url, session_id=:session_id,timestamp=now()";
}
