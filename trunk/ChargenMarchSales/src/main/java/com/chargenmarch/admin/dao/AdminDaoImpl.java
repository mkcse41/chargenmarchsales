package com.chargenmarch.admin.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.chargenmarch.admin.Query.AdminQueries;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.service.user.IUserService;

@Repository
public class AdminDaoImpl implements IAdminDao{

	@Autowired 
	IUserService userServie;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public int updateUserByEmailId(UserDTO user) {
		if(userServie.getUserInfoMapByEmailId().containsKey(user.getEmailId())){
			Object[] args = {user.getName(), user.getMobileNo(), user.getSource(),user.getPassword(),user.getRechargeCount() ,user.getWalletBalance(), user.getReferralBalance(), user.getReferralCode(), user.getIsverified() ,user.getEmailId()};
			return jdbcTemplate.update(AdminQueries.UPDATE_USER_DETAILS_BY_EMAIL, args);
		}
		return 0;
	}

	@Override
	public int updateUserByUserId(UserDTO user) {
		if(userServie.getUserInfoMapById().containsKey(user.getId())){
			Object[] args = {user.getName(), user.getMobileNo(), user.getEmailId() ,user.getSource(),user.getPassword(),user.getRechargeCount() ,user.getWalletBalance(), user.getReferralBalance(), user.getReferralCode(), user.getIsverified(), user.getId()};
			return jdbcTemplate.update(AdminQueries.UPDATE_USER_DETAILS_BY_USERID, args);
		}
		return 0;
	}
	
	
}
