package com.chargenmarch.admin.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chargenmarch.admin.dao.IAdminDao;
import com.chargenmarch.common.dao.user.IUserDao;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.service.user.IUserService;

@Service
public class AdminServiceImpl implements IAdminService{

	private final static Logger logger = LoggerFactory.getLogger(AdminServiceImpl.class);
	
	@Autowired
	IAdminDao adminDao;
	
	@Autowired
	IUserService userService;
	
	//update user
	//call add user method of user service, in this method if user exits then update user according to mail id
	
	@Override
	public int updateUserByEmail(UserDTO user) {
		try {
			int status = adminDao.updateUserByEmailId(user);
			if(status==1){
				userService.updateUserCacheMaps(user);
			}
			return status;
		} catch (Exception e) {
			logger.error("error in update user info by admin panel"+user);
			e.printStackTrace();
		}
		return 0;
	}
	
	@Override
	public int updateUserByUserId(UserDTO user) {
		try {
			int status = adminDao.updateUserByUserId(user);
			if(status==1){
				userService.updateUserCacheMaps(user);
			}
			return status;
		} catch (Exception e) {
			logger.error("error in update user info by admin panel"+user);
			e.printStackTrace();
		}
		return 0;
	}

}
