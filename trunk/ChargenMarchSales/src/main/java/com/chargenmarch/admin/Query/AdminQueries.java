package com.chargenmarch.admin.Query;

public class AdminQueries {

	public static final String UPDATE_USER_DETAILS_BY_EMAIL = "update charger_details set name=?,mobileno=?,source=?,password=?,updatedate=now(),rechargecount=?,walletbal=?,referralAmount=?,referralCode=?,isverified=? where emailid=?";
	public static final String UPDATE_USER_DETAILS_BY_USERID = "update charger_details set name=?,mobileno=?,emailid=?,source=?,password=?,updatedate=now(),rechargecount=?,walletbal=?,referralAmount=?,referralCode=?,isverified=? where id=?";
}
