package com.chargenmarch.admin.dao;

import com.chargenmarch.common.dto.user.UserDTO;

public interface IAdminDao {

	public int updateUserByEmailId(UserDTO user);
	public int updateUserByUserId(UserDTO user);
}
