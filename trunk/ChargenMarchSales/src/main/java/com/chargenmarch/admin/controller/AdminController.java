package com.chargenmarch.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.chargenmarch.admin.service.IAdminService;
import com.chargenmarch.charge.dto.funds.AddFundsRequestDTO;
import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.charge.joloapi.service.IRechargeService;
import com.chargenmarch.charge.service.mobilerecharge.IMobileRechargeService;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.RequestConstants;
import com.chargenmarch.common.constants.ViewConstants;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.service.user.IUserService;
import com.google.gson.Gson;

@Controller
@RequestMapping(value = RequestConstants.AdminConstant.ADMIN)
public class AdminController {

	@Autowired
	IUserService userService;
	
	@Autowired
	IAdminService adminService;
	
	@Autowired
	IMobileRechargeService mobileRechargeService;
	
	@Autowired
	private IRechargeService rService;
	
	@RequestMapping(value = RequestConstants.AdminConstant.USER_INFORMATION , method = RequestMethod.GET)
	public ModelAndView adminPanel(HttpServletRequest request, HttpServletResponse reaponse) throws Exception{
		ModelAndView mav = new ModelAndView(ViewConstants.AdminPanelViewConstanat.ADMIN+ViewConstants.AdminPanelViewConstanat.ADMIN_PANEL);
		List<AddFundsRequestDTO> addFundsRequest = mobileRechargeService.getPendingFundsRequest();
		mav.addObject("addFundsRequestList", addFundsRequest);
		return mav;
	}
	
	@RequestMapping(value=  RequestConstants.AdminConstant.USER_INFORMATION_BYMOBILE , method = RequestMethod.POST)
	public @ResponseBody String getUSerInformationByMobile(@RequestParam(Constants.MOBILE_NO) String mobileNumber,
			HttpServletRequest request){
		Gson gson = new Gson();
		UserDTO userDto = userService.getUserByMobileNo(mobileNumber);
		if(null != userDto){
			String userDtoStr = gson.toJson(userDto);
			return userDtoStr;
		}
	return gson.toJson("user not available");
	}
	
	@RequestMapping(value=  RequestConstants.AdminConstant.USER_INFORMATION_BYEMAIL , method = RequestMethod.POST)
	public @ResponseBody String getUSerInformationByEmailId(@RequestParam(Constants.EMAIL_ID) String emailId,
			HttpServletRequest request){
		Gson gson = new Gson();
		UserDTO userDto = userService.getUserByEmailId(emailId);
		if(null != userDto){
			String userDtoStr = gson.toJson(userDto);
			return userDtoStr;
		}
	return gson.toJson("user not available");
	}
	
	@RequestMapping(value=  RequestConstants.AdminConstant.USER_INFORMATION_BYUSERID , method = RequestMethod.POST)
	public @ResponseBody String getUserInformationByUserId(@RequestParam(Constants.USER_ID) Integer userId,
			HttpServletRequest request){
		Gson gson = new Gson();
		UserDTO userDto = userService.getUserById(userId);
		if(null != userDto){
			String userDtoStr = gson.toJson(userDto);
			return userDtoStr;
		}
	return gson.toJson("user not available");
	}
	
	@RequestMapping(value=  RequestConstants.AdminConstant.USER_TRANSACTION_HISTORY_BYUSERID , method = RequestMethod.POST)
	public @ResponseBody ModelAndView getUserTransactionHistoryByUserId(@RequestParam(Constants.USER_ID) Integer userId,
			HttpServletRequest request){
		Gson gson = new Gson();
		ModelAndView mnv = new ModelAndView(ViewConstants.AdminPanelViewConstanat.ADMIN+"userTransactionHistory");
		List<RechargeDTO> transactionList = mobileRechargeService.getRechargeListByChargerId(String.valueOf(userId));
		if(null != transactionList && !transactionList.isEmpty()){
			mnv.addObject("transactionList", transactionList);
		}
		return mnv;
	}
	
	@RequestMapping(value=RequestConstants.AdminConstant.UPDATE_USER_BY_EMAIL_ADMIN, method = RequestMethod.POST)
	@ResponseBody
	public String updateUserByEmail(@Valid UserDTO user, BindingResult result, HttpServletRequest request){
		Gson gson = new Gson();
		int userAddStatus = adminService.updateUserByEmail(user);
		if(userAddStatus == 1){
			return gson.toJson("user updated successfully");
		}
		return gson.toJson("user not updated successfully");
	}
	
	@RequestMapping(value=RequestConstants.AdminConstant.UPDATE_USER_BY_USERID_ADMIN, method = RequestMethod.POST)
	@ResponseBody
	public String updateUserByUserId(@Valid UserDTO user, BindingResult result, HttpServletRequest request){
		Gson gson = new Gson();
		int userAddStatus = adminService.updateUserByUserId(user);
		if(userAddStatus == 1){
			return gson.toJson("user updated successfully");
		}
		return gson.toJson("user not updated successfully");
	}
	
	@RequestMapping(value=RequestConstants.AdminConstant.MARGIN, method = RequestMethod.GET)
	public String updateUserMargin(HttpServletRequest request){
		try {
			rService.updateRechargeMarginForSalesUser();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
