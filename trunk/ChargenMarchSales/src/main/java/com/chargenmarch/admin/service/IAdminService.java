package com.chargenmarch.admin.service;

import com.chargenmarch.common.dto.user.UserDTO;

public interface IAdminService {

	public int updateUserByEmail(UserDTO user);
	public int updateUserByUserId(UserDTO user);
}
