package com.chargenmarch.leagues.DTO;

import java.io.Serializable;
import java.util.List;

public class CricketQADto implements Serializable {

	private static final long serialVersionUID = -3541585037713019119L;
	
	private IPLFixtureDTO iplFixtureDTO;
	private List<CricketQuestionDto> cricketQuestionList;
	
	public IPLFixtureDTO getIplFixtureDTO() {
		return iplFixtureDTO;
	}
	public void setIplFixtureDTO(IPLFixtureDTO iplFixtureDTO) {
		this.iplFixtureDTO = iplFixtureDTO;
	}
	public List<CricketQuestionDto> getCricketQuestionList() {
		return cricketQuestionList;
	}
	public void setCricketQuestionList(List<CricketQuestionDto> cricketQuestionList) {
		this.cricketQuestionList = cricketQuestionList;
	}
	@Override
	public String toString() {
		return "CricketQADto [iplFixtureDTO=" + iplFixtureDTO + ", cricketQuestionList=" + cricketQuestionList + "]";
	}
	
}
