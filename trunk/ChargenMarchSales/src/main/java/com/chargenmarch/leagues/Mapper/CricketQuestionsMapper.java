package com.chargenmarch.leagues.Mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.leagues.DTO.CricketQuestionDto;
import com.chargenmarch.leagues.enums.AnswerTypeEnum;

public class CricketQuestionsMapper implements RowMapper<CricketQuestionDto>{

	@Override
	public CricketQuestionDto mapRow(ResultSet rs, int args) throws SQLException {
		CricketQuestionDto cricketQuestionDto = new CricketQuestionDto();
		cricketQuestionDto.setId(rs.getInt("id"));
		cricketQuestionDto.setQuestion(rs.getString("question"));
		cricketQuestionDto.setAnswerType(AnswerTypeEnum.valueOf(rs.getString("answer_type")));
		cricketQuestionDto.setCorrectAnswer(rs.getString("correctanswer"));
		return cricketQuestionDto;
	}
	

}
