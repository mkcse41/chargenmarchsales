package com.chargenmarch.leagues.services;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.chargenmarch.leagues.DTO.CricketLeaguesDto;
import com.chargenmarch.leagues.DTO.CricketLeaguesResultDto;
import com.chargenmarch.leagues.DTO.CricketQADto;
import com.chargenmarch.leagues.DTO.CricketQuestionDto;
import com.chargenmarch.leagues.DTO.IPLFixtureDTO;

public interface ILeagueServices {
	public List<CricketQuestionDto> getAllCricketQuestions();
	public int updateCorrectAnswer(CricketQuestionDto cricketQuestionsDto);
	
	/**
	 * Returns a list of matches on the specified date
	 * @param matchDate
	 * @return List<IPLFixtureDTO>
	 */
	public List<IPLFixtureDTO> getIPLFixturesByDate(String matchDate);
	
	public Map<Integer, CricketQADto> getMatchIdWiseCricketQAMap();

	public Map<Integer, CricketLeaguesDto> getLeaguesMap();
	public int insertCricketLeaguesAnswer(CricketLeaguesResultDto cricketLeaguesResultDto);
	/**
	 * Checks for the correctness of answers provided by User
	 * Updates the Correct answer count in DB
	 * @param matchId
	 * @return int
	 */
	public int checkAnswers(int matchId);
	public Map<Integer, CricketQADto> getCricketQuestionList(Date date) ;
}
