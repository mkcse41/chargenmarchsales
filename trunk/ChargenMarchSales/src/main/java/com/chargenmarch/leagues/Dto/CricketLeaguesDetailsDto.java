package com.chargenmarch.leagues.Dto;

public class CricketLeaguesDetailsDto {

	private int cricketLeaguesId;
	private int leagueWinningNumber;
	private int leagueWinningAmount;
	public int getCricketLeaguesId() {
		return cricketLeaguesId;
	}
	public void setCricketLeaguesId(int cricketLeaguesId) {
		this.cricketLeaguesId = cricketLeaguesId;
	}
	public int getLeagueWinningNumber() {
		return leagueWinningNumber;
	}
	public void setLeagueWinningNumber(int leagueWinningNumber) {
		this.leagueWinningNumber = leagueWinningNumber;
	}
	public int getLeagueWinningAmount() {
		return leagueWinningAmount;
	}
	public void setLeagueWinningAmount(int leagueWinningAmount) {
		this.leagueWinningAmount = leagueWinningAmount;
	}
	@Override
	public String toString() {
		return "CricketLeaguesDetailsDto [cricketLeaguesId=" + cricketLeaguesId
				+ ", leagueWinningNumber=" + leagueWinningNumber
				+ ", leagueWinningAmount=" + leagueWinningAmount+"]";
	}
	
	
}


