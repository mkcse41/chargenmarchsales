package com.chargenmarch.leagues.Mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.leagues.DTO.IPLFixtureDTO;
import com.chargenmarch.leagues.enums.IPLTeamsEnum;

public class IPLFixtureMapper implements RowMapper<IPLFixtureDTO>{

	@Override
	public IPLFixtureDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		IPLFixtureDTO iplFixtureDTO = new IPLFixtureDTO();
		iplFixtureDTO.setId(rs.getInt(Constants.ID));
		iplFixtureDTO.setHomeTeam(rs.getString("home_team"));
		iplFixtureDTO.setAwayTeam(rs.getString("away_team"));
		iplFixtureDTO.setMatchDay(rs.getString("match_day"));
		iplFixtureDTO.setMatchNumber(rs.getInt("match_number"));
		iplFixtureDTO.setMatchTime(rs.getString("match_time"));
		iplFixtureDTO.setVenue(rs.getString("venue"));
		iplFixtureDTO.setAwayTeamCode(IPLTeamsEnum.getByName(rs.getString("away_team")));
		iplFixtureDTO.setHomeTeamCode(IPLTeamsEnum.getByName(rs.getString("home_team")));
		return iplFixtureDTO;
	}
	

}
