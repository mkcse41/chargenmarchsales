package com.chargenmarch.leagues.constants;

public class RequestConstants {

    // constants for urls 	
	public static final String CAM_CRICKET_LEAGUES = "/cam_cricket_leagues.htm";
	public static final String SHOW_CRICKET_QUESTIONS = "/show_cricket_questions.htm";
	public static final String SUBMIT_CRICKET_QUESTIONS = "/submit_cricket_questions.htm";
	
	//Request Constants
	public static final String CRICKET_QUESTIONS = "cricketquestions";
	public static final String MESSAGE = "message";
	
	//redirect pages
	public static final String CRICKET_LEAGUES = "cricketleagues";
	public static final String CRICKET_LEAGUES_QUESTIONS = "cricketleaguesQuestions";
	public static final String SUBMIT_CRICKET_LEAGUES_ANSWER = "submit_leagues_answer.htm";
	public static final String CRICKET_LEAGUES_RESULT = "cricketleaguesresult";
	public static final String NAME = "name";
	public static final String PRICE = "price";
	public static final String MIN_USER = "min_user";
	public static final String MAX_USER = "max_user";
	public static final String MAX_USER_PRIZE_COUNT = "max_user_prize_count";
	public static final String CRICKET_LEAUGE_ID = "cricket_leauge_Id";
	public static final String LEAGUE_WINNING_NUMBER = "league_winning_number";
	public static final String LEAGUE_WINNING_AMOUNT = "league_winning_amount";
	public static final String ID = "id";
	public static final String BLANK = "";
	public static final String SLASH = "/";
	public static final String COMMA = ",";
	public static final String CRICKET_LEAGUE = "CricketLeague";
	public static final String PAY_U = "PayU";
	public static final String URL = "url";
	public static final String ORDER_INFO = "orderInfo";
	public static final String PROCESSED_PAYMENT = "/processedPayment";
	public static final String CRICKET_LEAGUES_RESULT_DTO = "cricketLeaguesResultDto";
	public static final int cricketLeague = 0;
	public static final String PAYMENT_RESPONSE = "paymentresponse";
	public static final String RESPONSE = "response";
	public static final String CRICKET_LEAGUE_PAYMENT_RESPONSE = "cricketleagues/paymentresponse.htm";

}
