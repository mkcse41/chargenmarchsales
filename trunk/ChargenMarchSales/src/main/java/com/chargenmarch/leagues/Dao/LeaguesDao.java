package com.chargenmarch.leagues.Dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.WebApplicationContext;

import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.leagues.DTO.CricketLeaguePrize;
import com.chargenmarch.leagues.DTO.CricketLeaguesDto;
import com.chargenmarch.leagues.DTO.CricketLeaguesResultDto;
import com.chargenmarch.leagues.DTO.CricketQuestionDto;
import com.chargenmarch.leagues.DTO.IPLFixtureDTO;
import com.chargenmarch.leagues.Dto.CricketLeaguesDetailsDto;
import com.chargenmarch.leagues.Mapper.CricketLeaguesDetailMapper;
import com.chargenmarch.leagues.Mapper.CricketLeaguesMapper;
import com.chargenmarch.leagues.Mapper.CricketQuestionsMapper;
import com.chargenmarch.leagues.Mapper.CricketResultMapper;
import com.chargenmarch.leagues.Mapper.IPLFixtureMapper;
import com.chargenmarch.leagues.Mapper.LeaguePrizeMapper;
import com.chargenmarch.leagues.Query.LeaguesQuery;

@Repository
public class LeaguesDao implements ILeaguesDao {

	@Autowired
	private NamedParameterJdbcTemplate namedParamJdbcTemplateLeagues;

	@Autowired
	private WebApplicationContext context;
	
	/*@Autowired
	private JdbcTemplate leaguesJdbcTemplate;
	*/
	public List<CricketQuestionDto> getAllCricketQuestions(){
			return namedParamJdbcTemplateLeagues.query(LeaguesQuery.SELECT_ALL_QUESTIONS_CRICKET_LEAGUES, new CricketQuestionsMapper());
	}

	public int updateCorrectAnswer(CricketQuestionDto cricketQuestionDto) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(cricketQuestionDto);
		return namedParamJdbcTemplateLeagues.update(LeaguesQuery.UPDATE_CORRECT_ANSWER, params);
	}

	/**
	 * Returns a list of matches on the specified date
	 * @param matchDate
	 * @return List<IPLFixtureDTO>
	 */
	public List<IPLFixtureDTO> getIPLFixturesByDate(String matchDate) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("matchDate", Constants.PERCENT+matchDate+Constants.PERCENT);
		return namedParamJdbcTemplateLeagues.query(LeaguesQuery.SELECT_IPL_FIXTURES_BY_DATE,params, new IPLFixtureMapper());
	}
	
	public List<CricketLeaguesDto> getAllCricketLeagues(){
		return namedParamJdbcTemplateLeagues.query(LeaguesQuery.SELECT_ALL_CRICKET_LEAGUES, new CricketLeaguesMapper());
    }
	
	public List<CricketLeaguesDetailsDto> getCricketLeaguesDetailsById(int cricketLeaguesId){
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("cricketLeaguesId", cricketLeaguesId);
		return namedParamJdbcTemplateLeagues.query(LeaguesQuery.SELECT_CRICKET_LEAGUES_DETAILS_BY_ID, params, new CricketLeaguesDetailMapper());
    }
	
	public int insertCricketLeaguesAnswer(CricketLeaguesResultDto cricketLeaguesResultDto){
/*		Object args[]= new Object[] {cricketLeaguesResultDto.getLeaguesMatchId(),cricketLeaguesResultDto.getUserDto().getId(),cricketLeaguesResultDto.getCricketLeagueId()
				                     , cricketLeaguesResultDto.getAnswer1(),cricketLeaguesResultDto.getAnswer2(),cricketLeaguesResultDto.getAnswer3()
				                     , cricketLeaguesResultDto.getAnswer4(), cricketLeaguesResultDto.getAnswer5(),cricketLeaguesResultDto.getAnswer6()
				                     , cricketLeaguesResultDto.getAnswer7(), cricketLeaguesResultDto.getAnswer8(), cricketLeaguesResultDto.getAnswer9()
				                     , cricketLeaguesResultDto.getAnswer10()};
*/		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(cricketLeaguesResultDto);
		return namedParamJdbcTemplateLeagues.update(LeaguesQuery.INSERT_CRICKET_LEAGUE_ANSWER, params);
    }
	
	/**
	 * Gives Leagues results by match Id
	 * @param matchId
	 * @return
	 */
	public List<CricketLeaguesResultDto> getLeagueResultsByMatchId(int matchId) {
		MapSqlParameterSource params = new MapSqlParameterSource();
		params.addValue("matchId", matchId);
		return namedParamJdbcTemplateLeagues.query(LeaguesQuery.SELECT_FROM_LEAGUE_RESULT_BY_MATCH_ID, params, context.getBean(CricketResultMapper.class));
	}
	
	/**
	 * Update correct answer count as per league result DTO
	 * @param leaguesResultDto
	 * @return int
	 */
	public int updateCorrectAnswerCount(CricketLeaguesResultDto leaguesResultDto) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(leaguesResultDto);
		return namedParamJdbcTemplateLeagues.update(LeaguesQuery.UPDATE_CORRECT_ANSWER_COUNT_IN_LEAGUE_RESULT, params);
	}
	
	/**
	 * Returns all the prize available in DB
	 * @return List<CricketLeaguePrize>
	 */
	public List<CricketLeaguePrize> getAllLeaguePrizes() {
		return namedParamJdbcTemplateLeagues.query(LeaguesQuery.SELECT_ALL_CRICKET_LEAGUE_PRIZE, new LeaguePrizeMapper());
	}
}
