package com.chargenmarch.leagues.DTO;

import java.io.Serializable;

import com.chargenmarch.leagues.enums.IPLTeamsEnum;

public class IPLFixtureDTO implements Serializable{

	private static final long serialVersionUID = -5629751321772550231L;
	
	private Integer id;
	private Integer matchNumber;
	private String homeTeam;
	private String awayTeam;
	private IPLTeamsEnum homeTeamCode;
	private IPLTeamsEnum awayTeamCode;
	private String matchTime;
	private String venue;
	private String matchDay;
	private String isClosedLeague;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getMatchNumber() {
		return matchNumber;
	}
	public void setMatchNumber(Integer matchNumber) {
		this.matchNumber = matchNumber;
	}
	public String getHomeTeam() {
		return homeTeam;
	}
	public void setHomeTeam(String homeTeam) {
		this.homeTeam = homeTeam;
	}
	public String getAwayTeam() {
		return awayTeam;
	}
	public void setAwayTeam(String awayTeam) {
		this.awayTeam = awayTeam;
	}
	public String getMatchTime() {
		return matchTime;
	}
	public void setMatchTime(String matchTime) {
		this.matchTime = matchTime;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getMatchDay() {
		return matchDay;
	}
	public void setMatchDay(String matchDay) {
		this.matchDay = matchDay;
	}
	public IPLTeamsEnum getHomeTeamCode() {
		return homeTeamCode;
	}
	public void setHomeTeamCode(IPLTeamsEnum homeTeamCode) {
		this.homeTeamCode = homeTeamCode;
	}
	public IPLTeamsEnum getAwayTeamCode() {
		return awayTeamCode;
	}
	public void setAwayTeamCode(IPLTeamsEnum awayTeamCode) {
		this.awayTeamCode = awayTeamCode;
	}
	
	public String getIsClosedLeague() {
		return isClosedLeague;
	}
	public void setIsClosedLeague(String isClosedLeague) {
		this.isClosedLeague = isClosedLeague;
	}
	@Override
	public String toString() {
		return "IPLFixtureDTO [id=" + id + ", matchNumber=" + matchNumber
				+ ", homeTeam=" + homeTeam + ", awayTeam=" + awayTeam
				+ ", homeTeamCode=" + homeTeamCode + ", awayTeamCode="
				+ awayTeamCode + ", matchTime=" + matchTime + ", venue="
				+ venue + ", matchDay=" + matchDay + ", isClosedLeague="
				+ isClosedLeague + "]";
	}
	
	
	

}
