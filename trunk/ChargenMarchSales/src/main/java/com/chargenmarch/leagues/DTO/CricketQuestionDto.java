package com.chargenmarch.leagues.DTO;

import java.util.List;

import com.chargenmarch.leagues.enums.AnswerTypeEnum;

public class CricketQuestionDto {

	private int id;
	private String question;
	private List<String> answerList;
	private AnswerTypeEnum answerType;
	private String correctAnswer;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public List<String> getAnswerList() {
		return answerList;
	}
	public void setAnswerList(List<String> answerList) {
		this.answerList = answerList;
	}
	public String getCorrectAnswer() {
		return correctAnswer;
	}
	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}
	public AnswerTypeEnum getAnswerType() {
		return answerType;
	}
	public void setAnswerType(AnswerTypeEnum answerType) {
		this.answerType = answerType;
	}
	@Override
	public String toString() {
		return "CricketQuestionDto [id=" + id + ", question=" + question + ", answerList=" + answerList
				+ ", answerType=" + answerType + ", correctAnswer=" + correctAnswer + "]";
	}
	
}
