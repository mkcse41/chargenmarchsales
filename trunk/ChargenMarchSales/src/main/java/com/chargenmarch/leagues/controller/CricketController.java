package com.chargenmarch.leagues.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.chargenmarch.charge.constants.recharge.RechargeConstants;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.RequestConstants;
import com.chargenmarch.common.constants.SessionConstants;
import com.chargenmarch.common.constants.ViewConstants;
import com.chargenmarch.common.constants.paymentgateway.PaymentConstants;
import com.chargenmarch.common.dto.email.EmailDTO;
import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.service.user.IUserService;
import com.chargenmarch.common.utility.IUtility;
import com.chargenmarch.leagues.DTO.CricketLeaguesDto;
import com.chargenmarch.leagues.DTO.CricketLeaguesResultDto;
import com.chargenmarch.leagues.DTO.CricketQADto;
import com.chargenmarch.leagues.DTO.CricketQuestionDto;
import com.chargenmarch.leagues.DTO.IPLFixtureDTO;
import com.chargenmarch.leagues.form.CricketLeaguesResultForm;
import com.chargenmarch.leagues.services.ILeagueServices;
import com.chargenmarch.mailer.MailSenderService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
@Controller
@RequestMapping(value=RequestConstants.Leagues.LEAGUE_CRICKET)
public class CricketController {

	private final static Logger logger = LoggerFactory.getLogger(CricketController.class);
	
	@Autowired
	private ILeagueServices leagueServices;

	
	@Autowired
	private IUserService userService;
	
	@Autowired 
	private IUtility utility;
	
	@Autowired
	private IEmailSenderService emailSenderService;
	
	@Autowired MailSenderService mailSender;
	
	private static final String CRICKET_LEAGUES_RESULT_FROM = "cricketLeaguesResultFrom";
	
	@RequestMapping(value=RequestConstants.Leagues.CAM_CRICKET_LEAGUES, method = RequestMethod.GET)
	public ModelAndView CricketLeaguePage(HttpServletRequest request,HttpServletResponse response) throws ParseException, IOException{
		ModelAndView mv = new ModelAndView(utility.getviewResolverName(request,RequestConstants.Leagues.CRICKET_LEAGUES));
		UserDTO sessionUser = (UserDTO)request.getSession().getAttribute(SessionConstants.USER_BEAN);
		mv.addObject(SessionConstants.USER_BEAN, sessionUser);
		Map<Integer, CricketQADto> cricketQADtoList = leagueServices.getCricketQuestionList(new Date());
		checkLeagueClosedorNot(cricketQADtoList);
		mv.addObject(RequestConstants.Leagues.CRICKET_QADTO_LIST, cricketQADtoList);
		Map<Integer, CricketLeaguesDto> cricketLeagueMap = leagueServices.getLeaguesMap();
		mv.addObject(RequestConstants.Leagues.CRICKET_LEAGUE_MAP, cricketLeagueMap);
		request.getSession().removeAttribute(RequestConstants.Leagues.CRICKET_LEAGUES_RESULT_DTO);
		//return mv;
		response.sendRedirect(utility.getBaseUrl());
		return null;
	}
	
	
	private void checkLeagueClosedorNot(Map<Integer, CricketQADto> cricketQADtoList) throws ParseException{
		
		DateFormat sdff = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date currentDatetime = new Date();
		try{
			for (Map.Entry<Integer, CricketQADto> entry : cricketQADtoList.entrySet()) {
				int matchId = entry.getKey();
				CricketQADto cricketQADto = entry.getValue();
				IPLFixtureDTO iplFixtureDto = cricketQADto.getIplFixtureDTO();
				String matchTime = iplFixtureDto.getMatchTime();
				Date matchTimeDate = (Date) sdff.parse(matchTime);
				matchTimeDate.setHours(matchTimeDate.getHours()-1);
				if(matchTimeDate.before(currentDatetime)){
					iplFixtureDto.setIsClosedLeague(RequestConstants.Leagues.TRUE);
				}else{
					iplFixtureDto.setIsClosedLeague(RequestConstants.Leagues.FALSE);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	@RequestMapping(value=RequestConstants.Leagues.SUBMIT_CRICKET_LEAGUES_ANSWER, method={ RequestMethod.GET, RequestMethod.POST })
	public ModelAndView submitCricketLeaguePage(HttpServletRequest request,HttpServletResponse response,@ModelAttribute(CRICKET_LEAGUES_RESULT_FROM) CricketLeaguesResultForm cricketLeaguesResultForm) throws IOException{
		UserDTO userDto = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
		if(null!=cricketLeaguesResultForm && null!=userDto){
			String[] answers = cricketLeaguesResultForm.getAnswers().split(Constants.COMMA);
			String[] questionIds = cricketLeaguesResultForm.getQuestionIds().split(Constants.COMMA);
			//This is check for keeping the number of questions and answers same.
			//As whole logic revolves around these arrays.
			if(questionIds.length != answers.length) {
				logger.error("Question ID & answers length do not match.\nQuestionIDs : "+questionIds+"\nAnswers : "+answers);
				return null;
			}
		    int leaguesPaidAmount = getTotalLeaguePaidAmount(cricketLeaguesResultForm.getLeaguesIdArr());
		    CricketLeaguesResultDto cricketLeaguesResultDto = getCricketLeaguesResultObject(cricketLeaguesResultForm, userDto);
		    IPLFixtureDTO iplFixtureDTO = leagueServices.getMatchIdWiseCricketQAMap().get(cricketLeaguesResultDto.getLeaguesMatchId()).getIplFixtureDTO();
		    String matchName = iplFixtureDTO.getHomeTeamCode() + " VS " + iplFixtureDTO.getAwayTeamCode();
		    request.getSession().setAttribute(RequestConstants.Leagues.CRICKET_LEAGUES_RESULT_DTO, cricketLeaguesResultDto);
		    if(userDto.getWalletBalance() >= leaguesPaidAmount){
		    	 userDto.setWalletBalance(userDto.getWalletBalance() - leaguesPaidAmount);
			     userService.updateUserWalletBalance(userDto);
			     String leagueArrResult[] = cricketLeaguesResultForm.getLeaguesIdArr().split(RequestConstants.Leagues.COMMA);
			     for (String cricketLeagueId : leagueArrResult) {
			    	    CricketLeaguesDto cricketLeaguesDto = leagueServices.getLeaguesMap().get(utility.convertStringToInt(cricketLeagueId));
			    	    cricketLeaguesResultDto.setCricketLeagueId(utility.convertStringToInt(cricketLeagueId));
			            leagueServices.insertCricketLeaguesAnswer(cricketLeaguesResultDto);
			            userDto.setWalletBalance(userDto.getWalletBalance() - cricketLeaguesDto.getPrice());
					    userService.updateUserWalletBalance(userDto);
					    String message = utility.classpathResourceLoader("leagueinvoice.html").replace("%NAME%", userDto.getName()).replace("%LEAGUENAME%", cricketLeaguesDto.getName()).replace("%MATCHNAME%", matchName);
						EmailDTO emailDto = new EmailDTO("CAM Cricket League - You're in!", userDto.getEmailId().split(Constants.COMMA), Constants.NOREPLY_EMAIL_ID, null, null, message, null, null);
						emailSenderService.sendEmail(emailDto);
				 } 
			     request.setAttribute(RequestConstants.Leagues.MATCH_NAME, matchName);
			     request.setAttribute(RequestConstants.Leagues.STATUS, RequestConstants.Leagues.SUCCESS);
		    	 ModelAndView mv = new ModelAndView(RequestConstants.Leagues.CRICKET_LEAGUES_RESULT);
				 return mv;
		    }else{
		    	int totalPaidAmount = leaguesPaidAmount - userDto.getWalletBalance();
			    iniliazeOrderInfo(cricketLeaguesResultDto, request, totalPaidAmount);
			    ModelAndView view = new ModelAndView(ViewConstants.PAY_U_LOWER);
				return view;
		    }
		}
		response.sendRedirect(utility.getBaseUrl() + RequestConstants.Leagues.SLASH + RequestConstants.Leagues.LEAGUE_CRICKET  + RequestConstants.Leagues.CAM_CRICKET_LEAGUES);
		return null;
	}
	
	
	@RequestMapping(value=RequestConstants.Leagues.CRICKET_LEAGUE_PAYMENT_RESPONSE, method={ RequestMethod.GET, RequestMethod.POST })
	public ModelAndView cricketLeaguePaymentResponse(HttpServletRequest request,HttpServletResponse response) throws IOException{
		 String responsejson = request.getParameter(RequestConstants.Leagues.RESPONSE);
		 ModelAndView view = new ModelAndView(RequestConstants.Leagues.CRICKET_LEAGUES_RESULT);
		 try{
			 if(response!=null && null!=request.getSession().getAttribute(SessionConstants.USER_BEAN) && null!=response 
						&& null!=request.getSession().getAttribute(RequestConstants.Leagues.CRICKET_LEAGUES_RESULT_DTO)){
				    Gson gson = new GsonBuilder().create();
					OrderInfo orderInfo = gson.fromJson(responsejson, OrderInfo.class);
					UserDTO user = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
					CricketLeaguesResultDto cricketLeaguesResultDto = (CricketLeaguesResultDto) request.getSession().getAttribute(RequestConstants.Leagues.CRICKET_LEAGUES_RESULT_DTO);
					IPLFixtureDTO iplFixtureDTO = leagueServices.getMatchIdWiseCricketQAMap().get(cricketLeaguesResultDto.getLeaguesMatchId()).getIplFixtureDTO();
				    String matchName = iplFixtureDTO.getHomeTeamCode() + " VS " + iplFixtureDTO.getAwayTeamCode();
					String orderId = orderInfo.getTxnId();
					String status = orderInfo.getStatus();
					int amount = (int) orderInfo.getAmount();
					user.setWalletBalance(user.getWalletBalance() + amount);
				    userService.updateUserWalletBalance(user);
					if(!orderId.equals(Constants.BLANK) && status.equals(Constants.SUCCESS)){
					     for (CricketLeaguesDto cricketLeaguesDto: cricketLeaguesResultDto.getCricketLeaguesDtoList()) {
					    	    cricketLeaguesResultDto.setCricketLeagueId(cricketLeaguesDto.getId());
					            leagueServices.insertCricketLeaguesAnswer(cricketLeaguesResultDto);
					            user.setWalletBalance(user.getWalletBalance() - cricketLeaguesDto.getPrice());
							    userService.updateUserWalletBalance(user);
							    String message = utility.classpathResourceLoader("leagueinvoice.html").replace("%NAME%", user.getName()).replace("%LEAGUENAME%", cricketLeaguesDto.getName()).replace("%MATCHNAME%", matchName);
							    EmailDTO emailDto = new EmailDTO("CAM Cricket League - You're in!", user.getEmailId().split(Constants.COMMA), Constants.NOREPLY_EMAIL_ID, null, null, message, null, null);
								emailSenderService.sendEmail(emailDto);
						 } 
					     request.setAttribute(RequestConstants.Leagues.CRICKET_LEAGUES_RESULT_DTO, cricketLeaguesResultDto);
					}
					request.setAttribute(RequestConstants.Leagues.ORDER_INFO, orderInfo);
					request.setAttribute(RequestConstants.Leagues.MATCH_NAME, matchName);
					request.setAttribute(RequestConstants.Leagues.STATUS, orderInfo.getStatus());
					return view;
			 }
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 response.sendRedirect(utility.getBaseUrl() + RequestConstants.Leagues.SLASH + RequestConstants.Leagues.LEAGUE_CRICKET  + RequestConstants.Leagues.CAM_CRICKET_LEAGUES);
	     return null;
	}
	
	private int getTotalLeaguePaidAmount(String leagueArr){
		int paidAmount = 0;
		String leagueArrResult[] = leagueArr.split(RequestConstants.Leagues.COMMA);
		for (String leagueId : leagueArrResult) {
			 CricketLeaguesDto cricketLeaguesDto = leagueServices.getLeaguesMap().get(utility.convertStringToInt(leagueId));
			 if(null!=cricketLeaguesDto){
			     paidAmount += cricketLeaguesDto.getPrice();
			 }
		}
		return paidAmount;
	}
	
	
	private OrderInfo iniliazeOrderInfo(CricketLeaguesResultDto cricketLeaguesResultDto,HttpServletRequest request,int paidAmount){
		String userCredentials = cricketLeaguesResultDto.getUserDto().getId() + cricketLeaguesResultDto.getUserDto().getEmailId();
		OrderInfo orderInfo = utility.initializeOrderInfoForPaymentGateway(cricketLeaguesResultDto.getUserDto().getName(), null, cricketLeaguesResultDto.getUserDto().getEmailId(), cricketLeaguesResultDto.getUserDto().getMobileNo(), 
	    		null, null, null, null, RequestConstants.Leagues.CRICKET_LEAGUE, paidAmount, RequestConstants.Leagues.PAY_U,userCredentials);
	    request.setAttribute(RequestConstants.Leagues.ORDER_INFO, orderInfo);
	    request.setAttribute(RequestConstants.Leagues.URL, utility.getBaseUrl() + RechargeConstants.SLASH + PaymentConstants.PAYMENT.toLowerCase() + RequestConstants.Leagues.PROCESSED_PAYMENT );
	    return orderInfo;
	}
	
	private CricketLeaguesResultDto getCricketLeaguesResultObject(CricketLeaguesResultForm cricketLeaguesResultForm, UserDTO userDto){
		   CricketLeaguesResultDto cricketLeaguesResultDto = new CricketLeaguesResultDto();
		   List<CricketLeaguesDto> cricketLeaguesDtoList = new ArrayList<CricketLeaguesDto>();
		   cricketLeaguesResultDto.setUserDto(userDto);
		   cricketLeaguesResultDto.setEmailId(userDto.getEmailId());
		   cricketLeaguesResultDto.setLeaguesMatchId(cricketLeaguesResultForm.getLeaguesMatchId());
		   String leagueArrResult[] = cricketLeaguesResultForm.getLeaguesIdArr().split(RequestConstants.Leagues.COMMA);
			for (String leagueId : leagueArrResult) {
				 CricketLeaguesDto cricketLeaguesDto = leagueServices.getLeaguesMap().get(utility.convertStringToInt(leagueId));
				 if(null!=cricketLeaguesDto){
					 cricketLeaguesDtoList.add(cricketLeaguesDto);
				 }
			}
		   cricketLeaguesResultDto.setCricketLeaguesDtoList(cricketLeaguesDtoList);
		   cricketLeaguesResultDto.setQuestionIds(cricketLeaguesResultForm.getQuestionIds());
		   cricketLeaguesResultDto.setAnswers(cricketLeaguesResultForm.getAnswers());
		   return cricketLeaguesResultDto;
		
	}
	

	@RequestMapping(value=RequestConstants.Leagues.SHOW_CRICKET_QUESTIONS, method = RequestMethod.GET)
	public ModelAndView ShowCricketQuestionsPage(HttpServletRequest request,HttpServletResponse response){
		ModelAndView mv = new ModelAndView(RequestConstants.Leagues.CRICKET_LEAGUES_QUESTIONS);
		List<CricketQuestionDto> cricketQuestions = leagueServices.getAllCricketQuestions();
		mv.addObject(RequestConstants.Leagues.CRICKET_QUESTIONS, cricketQuestions);
		return mv;
	}
	
	@RequestMapping(value=RequestConstants.Leagues.SUBMIT_CORRECT_ANSWER, method = RequestMethod.POST)
	@ResponseBody
	public String updateCorrectAnswer(CricketQuestionDto cricketQuestionDto ,BindingResult result, HttpServletRequest request){
		JsonObject response = new JsonObject();
		int status = leagueServices.updateCorrectAnswer(cricketQuestionDto);
        response.addProperty(RequestConstants.Leagues.MESSAGE, status);		
		return response.toString();
	}

	@RequestMapping(value=RequestConstants.Leagues.FETCH_QUESTIONS, method = RequestMethod.GET)
	@ResponseBody
	public Map<Integer, CricketQADto> getQuestions(){
		return leagueServices.getMatchIdWiseCricketQAMap();
	}
	
	@RequestMapping(value=RequestConstants.Leagues.CHECK_ANSWERS, method = RequestMethod.POST)
	public void checkCorrectAnswers(@PathVariable int matchId) {
		leagueServices.checkAnswers(matchId);
	}
	@RequestMapping(value=RequestConstants.Leagues.WINNERS, method = RequestMethod.POST)
	public void generateWinnersForMatch(@PathVariable int matchId) {
		
	}
	
	@RequestMapping(value="/mailSender", method = RequestMethod.GET)
	public void MailSender() {
		logger.info("League Mail Start.");
		mailSender.sendMail();
		logger.info("League Mail Stop.");
	}
}
