package com.chargenmarch.leagues.Dao;

import java.util.List;

import com.chargenmarch.leagues.DTO.CricketLeaguePrize;
import com.chargenmarch.leagues.DTO.CricketLeaguesDto;
import com.chargenmarch.leagues.DTO.CricketLeaguesResultDto;
import com.chargenmarch.leagues.DTO.CricketQuestionDto;
import com.chargenmarch.leagues.DTO.IPLFixtureDTO;
import com.chargenmarch.leagues.Dto.CricketLeaguesDetailsDto;

public interface ILeaguesDao {
	public List<CricketQuestionDto> getAllCricketQuestions();
	public int updateCorrectAnswer(CricketQuestionDto cricketQuestionDto);
	
	/**
	 * Returns a list of matches on the specified date
	 * @param matchDate
	 * @return List<IPLFixtureDTO>
	 */
	public List<IPLFixtureDTO> getIPLFixturesByDate(String matchDate);

	public List<CricketLeaguesDto> getAllCricketLeagues();
	public List<CricketLeaguesDetailsDto> getCricketLeaguesDetailsById(int cricketLeaguesId);
	public int insertCricketLeaguesAnswer(CricketLeaguesResultDto cricketLeaguesResultDto);
	/**
	 * Gives Leagues results by match Id
	 * @param matchId
	 * @return
	 */
	public List<CricketLeaguesResultDto> getLeagueResultsByMatchId(int matchId);
	
	/**
	 * Update correct answer count as per league result DTO
	 * @param leaguesResultDto
	 * @return int
	 */
	public int updateCorrectAnswerCount(CricketLeaguesResultDto leaguesResultDto);
	
	/**
	 * Returns all the prize available in DB
	 * @return List<CricketLeaguePrize>
	 */
	public List<CricketLeaguePrize> getAllLeaguePrizes();
	
}
