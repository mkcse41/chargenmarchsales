package com.chargenmarch.leagues.Mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.leagues.DTO.CricketLeaguePrize;

public class LeaguePrizeMapper implements RowMapper<CricketLeaguePrize>{

	@Override
	public CricketLeaguePrize mapRow(ResultSet rs, int rowNum) throws SQLException {
		CricketLeaguePrize prize = new CricketLeaguePrize();
		prize.setId(rs.getInt("id"));
		prize.setLeagueId(rs.getInt("cricket_league_id"));
		prize.setRank(rs.getInt("league_winning_number"));
		prize.setWinningAmount(rs.getInt("league_winning_amount"));
		return prize;
	}
}
