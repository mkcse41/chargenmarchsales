package com.chargenmarch.leagues.Query;

public class LeaguesQuery {

	public static final String SELECT_ALL_QUESTIONS_CRICKET_LEAGUES = "select * from cricketquestions";
	public static final String UPDATE_CORRECT_ANSWER = "update cricketquestions set correctanswer=:correctAnswer where id=:id";
	public static final String SELECT_IPL_FIXTURES_BY_DATE = "select * from ipl_schedule where match_time like :matchDate";
	public static final String INSERT_INTO_QUESTIONS_CRICKET_LEAGUES = "insert into cricketquestions set answer1=:answer1,answer2=:answer2,answer3=:answer3,answer4=:answer4,correctanswer=:correctanswer where question=:question";
	public static final String SELECT_ALL_CRICKET_LEAGUES = "select * from cricket_league";
	public static final String SELECT_CRICKET_LEAGUES_DETAILS_BY_ID = "select * from cricket_league_prize where cricket_leauge_Id=:cricketLeaguesId";
	public static final String INSERT_CRICKET_LEAGUE_ANSWER = "insert into cricket_league_result set cricketMatchId=:leaguesMatchId, chargers_id=:userDto.id, leagueId=:cricketLeagueId, answers=:answers, questionIds=:questionIds, timestamp=now()";
	public static final String UPDATE_CORRECT_ANSWER_COUNT_IN_LEAGUE_RESULT = "update cricket_league_result set correct_answer_count=:correctAnswerCount where chargers_id=:userDto.id and cricketMatchId=:leaguesMatchId and leagueId=:cricketLeagueId";
	public static final String SELECT_FROM_LEAGUE_RESULT_BY_MATCH_ID = "select * from cricket_league_result where cricketMatchId = :matchId";
	public static final String SELECT_ALL_CRICKET_LEAGUE_PRIZE = "select * from cricket_league_prize";
}

