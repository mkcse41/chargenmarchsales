package com.chargenmarch.leagues.Mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.leagues.Dto.CricketLeaguesDetailsDto;
import com.chargenmarch.leagues.constants.RequestConstants;

public class CricketLeaguesDetailMapper implements RowMapper<CricketLeaguesDetailsDto>{

	@Override
	public CricketLeaguesDetailsDto mapRow(ResultSet rs, int args) throws SQLException {
		CricketLeaguesDetailsDto cricketLeaguesDetailsDto = new CricketLeaguesDetailsDto();
		cricketLeaguesDetailsDto.setCricketLeaguesId(rs.getInt(RequestConstants.CRICKET_LEAUGE_ID));
		cricketLeaguesDetailsDto.setLeagueWinningAmount(rs.getInt(RequestConstants.LEAGUE_WINNING_AMOUNT));
		cricketLeaguesDetailsDto.setLeagueWinningNumber(rs.getInt(RequestConstants.LEAGUE_WINNING_NUMBER));
		return cricketLeaguesDetailsDto;
	}

}
