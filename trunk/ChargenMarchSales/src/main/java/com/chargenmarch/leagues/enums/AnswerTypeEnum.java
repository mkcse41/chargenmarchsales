package com.chargenmarch.leagues.enums;

/**
 * @author Gambit 
 * 
 * This enum holds different types of answers league questions
 * can have
 */
public enum AnswerTypeEnum {
	TEAM,
	RUN,
	PLAYER_TYPE,
	WICKET,
	CATCH,
	BOWLED
}
