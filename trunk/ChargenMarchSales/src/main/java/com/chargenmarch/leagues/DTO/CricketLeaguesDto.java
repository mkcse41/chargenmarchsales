package com.chargenmarch.leagues.DTO;

import java.util.List;

import com.chargenmarch.leagues.Dto.CricketLeaguesDetailsDto;


/**
 *  `name` varchar(45) NOT NULL,
  `price` varchar(45) NOT NULL,
  `min_user` int(11),
  `max_user` int(11),
  `max_user_prize_count` int(11),
 * @author mukesh
 *
 */
public class CricketLeaguesDto {
	
	private int Id;
	private String name;
	private int price;
	private int minUser;
	private int maxUser;
	private int maxUserPrizeCount;
	private List<CricketLeaguesDetailsDto> cricketLeaguesDetailDto;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getMinUser() {
		return minUser;
	}
	public void setMinUser(int minUser) {
		this.minUser = minUser;
	}
	public int getMaxUser() {
		return maxUser;
	}
	public void setMaxUser(int maxUser) {
		this.maxUser = maxUser;
	}
	public int getMaxUserPrizeCount() {
		return maxUserPrizeCount;
	}
	public void setMaxUserPrizeCount(int maxUserPrizeCount) {
		this.maxUserPrizeCount = maxUserPrizeCount;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	
	public List<CricketLeaguesDetailsDto> getCricketLeaguesDetailDto() {
		return cricketLeaguesDetailDto;
	}
	public void setCricketLeaguesDetailDto(
			List<CricketLeaguesDetailsDto> cricketLeaguesDetailDto) {
		this.cricketLeaguesDetailDto = cricketLeaguesDetailDto;
	}
	@Override
	public String toString() {
		return "CricketLeaguesDto [Id=" + Id + ", name=" + name + ", price="
				+ price + ", minUser=" + minUser + ", maxUser=" + maxUser
				+ ", maxUserPrizeCount=" + maxUserPrizeCount
				+ ", cricketLeaguesDetailDto=" + cricketLeaguesDetailDto + "]";
	}
	
	
	
	
	
}
