package com.chargenmarch.leagues.Mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.leagues.DTO.CricketLeaguesDto;
import com.chargenmarch.leagues.constants.RequestConstants;

public class CricketLeaguesMapper implements RowMapper<CricketLeaguesDto>{

	@Override
	public CricketLeaguesDto mapRow(ResultSet rs, int args) throws SQLException {
		
		CricketLeaguesDto cricketLeaguesDto = new CricketLeaguesDto();
		cricketLeaguesDto.setId(rs.getInt(RequestConstants.ID));
		cricketLeaguesDto.setName(rs.getString(RequestConstants.NAME));
		cricketLeaguesDto.setPrice(rs.getInt(RequestConstants.PRICE));
		cricketLeaguesDto.setMinUser(rs.getInt(RequestConstants.MIN_USER));
		cricketLeaguesDto.setMaxUser(rs.getInt(RequestConstants.MAX_USER));
		cricketLeaguesDto.setMaxUserPrizeCount(rs.getInt(RequestConstants.MAX_USER_PRIZE_COUNT));
		return cricketLeaguesDto;
	}

}
