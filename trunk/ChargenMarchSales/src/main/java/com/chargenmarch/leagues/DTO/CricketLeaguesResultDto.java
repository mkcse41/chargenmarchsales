package com.chargenmarch.leagues.DTO;

import java.util.Date;
import java.util.List;

import com.chargenmarch.common.dto.user.UserDTO;

public class CricketLeaguesResultDto {

	private int leaguesMatchId;
	private int id;
	private String emailId;
	//Comma Seperated Question ID List
	private String questionIds;
	//Comma Seperated Answers List. Order must be same as question ids
	private String answers;
	private int cricketLeagueId;
	private int correctAnswerCount;
	private UserDTO userDto;
	private List<CricketLeaguesDto> cricketLeaguesDtoList;
	private Date timestamp;
	
	public int getLeaguesMatchId() {
		return leaguesMatchId;
	}
	public void setLeaguesMatchId(int leaguesMatchId) {
		this.leaguesMatchId = leaguesMatchId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getQuestionIds() {
		return questionIds;
	}
	public void setQuestionIds(String questionIds) {
		this.questionIds = questionIds;
	}
	public String getAnswers() {
		return answers;
	}
	public void setAnswers(String answers) {
		this.answers = answers;
	}
	public int getCricketLeagueId() {
		return cricketLeagueId;
	}
	public void setCricketLeagueId(int cricketLeagueId) {
		this.cricketLeagueId = cricketLeagueId;
	}
	public UserDTO getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDTO userDto) {
		this.userDto = userDto;
	}
	public List<CricketLeaguesDto> getCricketLeaguesDtoList() {
		return cricketLeaguesDtoList;
	}
	public void setCricketLeaguesDtoList(List<CricketLeaguesDto> cricketLeaguesDtoList) {
		this.cricketLeaguesDtoList = cricketLeaguesDtoList;
	}
	public int getCorrectAnswerCount() {
		return correctAnswerCount;
	}
	public void setCorrectAnswerCount(int correctAnswerCount) {
		this.correctAnswerCount = correctAnswerCount;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	@Override
	public String toString() {
		return "CricketLeaguesResultDto [leaguesMatchId=" + leaguesMatchId + ", id=" + id + ", emailId=" + emailId
				+ ", questionIds=" + questionIds + ", answers=" + answers + ", cricketLeagueId=" + cricketLeagueId
				+ ", correctAnswerCount=" + correctAnswerCount + ", userDto=" + userDto + ", cricketLeaguesDtoList="
				+ cricketLeaguesDtoList + ", timestamp=" + timestamp + "]";
	}
}
