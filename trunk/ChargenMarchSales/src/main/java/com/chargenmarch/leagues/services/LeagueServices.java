package com.chargenmarch.leagues.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.leagues.DTO.CricketLeaguePrize;
import com.chargenmarch.leagues.DTO.CricketLeaguesDto;
import com.chargenmarch.leagues.DTO.CricketLeaguesResultDto;
import com.chargenmarch.leagues.DTO.CricketQADto;
import com.chargenmarch.leagues.DTO.CricketQuestionDto;
import com.chargenmarch.leagues.DTO.IPLFixtureDTO;
import com.chargenmarch.leagues.Dao.ILeaguesDao;
import com.chargenmarch.leagues.Dto.CricketLeaguesDetailsDto;
import com.chargenmarch.leagues.enums.AnswerTypeEnum;

/**
 * @author gambit
 *
 */
/**
 * @author gambit
 *
 */
@Service
public class LeagueServices implements ILeagueServices{

	
	private final static Logger logger = LoggerFactory.getLogger(LeagueServices.class);
	@Autowired
	private ILeaguesDao leaguesDao;
	private Map<Integer,CricketLeaguesDto> leaguesMap = new HashMap<Integer,CricketLeaguesDto>();

	/**
	 * Key is Match Number
	 */
	private Map<Integer, CricketQADto> matchIdWiseCricketQAMap;

	@PostConstruct
	public void init(){
		logger.info("LeagueServices Service Initilization");
		loadLeaguesData();
	}
	
	public Map<Integer, CricketQADto> getCricketQuestionList(Date date) {
			List<IPLFixtureDTO> iplFixtureList = getIPLFixturesByDate(new SimpleDateFormat(Constants.DateConstants.DEFAULT_DATE_FORMAT).format(date));
			if(iplFixtureList.size() == 1){
				Calendar c = Calendar.getInstance(); 
				c.setTime(date); 
				c.add(Calendar.DATE, 1);
				date = c.getTime();
				List<IPLFixtureDTO> iplFixtureSecondList = getIPLFixturesByDate(new SimpleDateFormat(Constants.DateConstants.DEFAULT_DATE_FORMAT).format(date));
				if(iplFixtureSecondList.size()>0){
					iplFixtureList.add(iplFixtureSecondList.get(0));
				}
			}
			Map<Integer, CricketQADto> tempCricketQADtoList = new HashMap<Integer, CricketQADto>();
			for(IPLFixtureDTO iplFixtureDTO : iplFixtureList) {
				List<CricketQuestionDto> questionList = getAllCricketQuestions();
				for(CricketQuestionDto questionDto : questionList) {
					questionDto.setAnswerList(getAnswerListByAnswerType(iplFixtureDTO, questionDto.getAnswerType()));
				}
				CricketQADto cricketQADto = new CricketQADto();
				cricketQADto.setCricketQuestionList(questionList);
				cricketQADto.setIplFixtureDTO(iplFixtureDTO);
				tempCricketQADtoList.put(iplFixtureDTO.getMatchNumber(), cricketQADto);
			    matchIdWiseCricketQAMap = tempCricketQADtoList;
		 }
	    return tempCricketQADtoList;
	}
	
	
	
	
	
	@Cacheable("cricketLeagueCache")
	public List<CricketQuestionDto> getAllCricketQuestions(){
		return leaguesDao.getAllCricketQuestions();
	}
	
	public int updateCorrectAnswer(CricketQuestionDto cricketQuestionsDto){
		int count = leaguesDao.updateCorrectAnswer(cricketQuestionsDto);
		if(count >= 1) {
			getCricketQuestionList(new Date());
		}
		return count;
	}
	private void loadLeaguesData() {
		List<CricketLeaguesDto> cricketLeaguesDtoList = leaguesDao.getAllCricketLeagues();
		for (CricketLeaguesDto cricketLeaguesDto : cricketLeaguesDtoList) {
			cricketLeaguesDto.setCricketLeaguesDetailDto(getCricketLeaguesDetailById(cricketLeaguesDto.getId()));
			leaguesMap.put(cricketLeaguesDto.getId(), cricketLeaguesDto);
		}
		
	}

	/**
	 * Returns a list of matches on the specified date
	 * @param matchDate
	 * @return List<IPLFixtureDTO>
	 */
	@Cacheable("cricketLeagueCache")
	public List<IPLFixtureDTO> getIPLFixturesByDate(String matchDate){
		return leaguesDao.getIPLFixturesByDate(matchDate);
	}

	
	/**
	 * This method returns answer choices depending upon the answer type
	 * @param iplFixtureDTO
	 * @param answerType
	 * @return List<String>
	 */
	private List<String> getAnswerListByAnswerType(IPLFixtureDTO iplFixtureDTO, AnswerTypeEnum answerType) {
		List<String> answerList;
		switch (answerType.ordinal()) {
			case 0 :
				answerList = new ArrayList<String>();
				answerList.add(iplFixtureDTO.getHomeTeam());
				answerList.add(iplFixtureDTO.getAwayTeam());
				break;
			case 1 :
				answerList = new ArrayList<String>();
				answerList.add("Upto 130");
				answerList.add("131-160");
				answerList.add("161-180");
				answerList.add("180+");
				break;
			case 2 :
				answerList = new ArrayList<String>();
				answerList.add("Bowler");
				answerList.add("Batsman");
				answerList.add("All Rounder");
				answerList.add("Wicket Keeper");
				break;
			case 3 :
				answerList = new ArrayList<String>();
				answerList.add("Upto 7");
				answerList.add("8-11");
				answerList.add("12-15");
				answerList.add("15+");
				break;
			case 4 :
				answerList = new ArrayList<String>();
				answerList.add("Upto 3");
				answerList.add("3-5");
				answerList.add("5-7");
				answerList.add("7+");
				break;
			case 5 :
				answerList = new ArrayList<String>();
				answerList.add("Upto 3");
				answerList.add("3-5");
				answerList.add("5-7");
				answerList.add("7+");
				break;
			default:
				answerList = null;
				break;
		}
		return answerList;
	}

	public Map<Integer, CricketQADto> getMatchIdWiseCricketQAMap() {
		return matchIdWiseCricketQAMap;
	}
	
	@Cacheable("cricketLeagueCache")
	public List<CricketLeaguesDetailsDto> getCricketLeaguesDetailById(int cricketLeaguesId){
		return leaguesDao.getCricketLeaguesDetailsById(cricketLeaguesId);
	}
	
	public int insertCricketLeaguesAnswer(CricketLeaguesResultDto cricketLeaguesResultDto){
		return leaguesDao.insertCricketLeaguesAnswer(cricketLeaguesResultDto);
	}

	public Map<Integer, CricketLeaguesDto> getLeaguesMap() {
		return leaguesMap;
	}
	
	/**
	 * Gives Leagues results by match Id
	 * @param matchId
	 * @return
	 */
	public List<CricketLeaguesResultDto> getLeagueResultsByMatchId(int matchId) {
		return leaguesDao.getLeagueResultsByMatchId(matchId);
	}
	
	/**
	 * Converts List of CricketQuestionDto to a map of Id -> Cricket Question DTO
	 * @param cricketQuestionDtos
	 * @return
	 */
	private Map<Integer, CricketQuestionDto> fetchIdWiseMapFromList(List<CricketQuestionDto> cricketQuestionDtos) {
		Map<Integer, CricketQuestionDto> map = new HashMap<Integer, CricketQuestionDto>();
		for(CricketQuestionDto cricketQuestionDto : cricketQuestionDtos) {
			map.put(cricketQuestionDto.getId(), cricketQuestionDto);
		}
		return map;
	}
	
	/**
	 * Checks for the correctness of answers provided by User
	 * Updates the Correct answer count in DB
	 * @param matchId
	 */
	public int checkAnswers(int matchId) {
		CricketQADto cricketQADto = getMatchIdWiseCricketQAMap().get(matchId);
		List<CricketQuestionDto> cricketQuestionDtos = cricketQADto.getCricketQuestionList();
		Map<Integer, CricketQuestionDto> idWiseCricketQuestionMap = fetchIdWiseMapFromList(cricketQuestionDtos);
		List<CricketLeaguesResultDto> resultList = getLeagueResultsByMatchId(matchId);
		for(CricketLeaguesResultDto leaguesResultDto : resultList) {
			int correctAnswerCount = 0;
			String[] questionIds = leaguesResultDto.getQuestionIds().split(Constants.COMMA);
			String[] answers = leaguesResultDto.getAnswers().split(Constants.COMMA);
			if(questionIds.length == answers.length) {
				for(int i=0; i<questionIds.length; i++) {
					if(checkAnswerCorrectness(Integer.parseInt(questionIds[i]), answers[i], idWiseCricketQuestionMap)) {
						++correctAnswerCount;
					}
				}
			} else {
				logger.error("Error for Question Answer Length for result id : " +leaguesResultDto.getId());
			}
			leaguesResultDto.setCorrectAnswerCount(correctAnswerCount);
			return leaguesDao.updateCorrectAnswerCount(leaguesResultDto);
		}
		return Constants.MINUS_ONE;
	}
	
	/**
	 * Returns true if answer for question ID match with correct answer from Map
	 * @param questionId
	 * @param answer
	 * @param idWiseCricketQuestionMap
	 * @return
	 */
	private boolean checkAnswerCorrectness(int questionId, String answer, Map<Integer, CricketQuestionDto> idWiseCricketQuestionMap) {
		return answer.equals(idWiseCricketQuestionMap.get(questionId).getCorrectAnswer());
	}
	
	/**
	 * Returns all the prize available
	 * @return List<CricketLeaguePrize>
	 */
	@Cacheable("cricketLeagueCache")
	public List<CricketLeaguePrize> getAllLeaguePrizes() {
		return leaguesDao.getAllLeaguePrizes();
	}
}
