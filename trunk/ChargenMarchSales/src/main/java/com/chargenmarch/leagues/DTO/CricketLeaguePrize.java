package com.chargenmarch.leagues.DTO;

public class CricketLeaguePrize {
	int id;
	int leagueId;
	int rank;
	int winningAmount;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getLeagueId() {
		return leagueId;
	}
	public void setLeagueId(int leagueId) {
		this.leagueId = leagueId;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public int getWinningAmount() {
		return winningAmount;
	}
	public void setWinningAmount(int winningAmount) {
		this.winningAmount = winningAmount;
	}
	@Override
	public String toString() {
		return "CricketLeaguePrize [id=" + id + ", leagueId=" + leagueId + ", rank=" + rank + ", winningAmount="
				+ winningAmount + "]";
	}
	
}
