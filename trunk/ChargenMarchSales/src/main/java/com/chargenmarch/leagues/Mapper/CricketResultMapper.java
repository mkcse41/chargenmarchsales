package com.chargenmarch.leagues.Mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.chargenmarch.common.service.user.IUserService;
import com.chargenmarch.leagues.DTO.CricketLeaguesResultDto;

@Component
@Scope("prototype")
public class CricketResultMapper implements RowMapper<CricketLeaguesResultDto>{

	@Autowired
	IUserService userService;
	
	@Override
	public CricketLeaguesResultDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		CricketLeaguesResultDto cricketLeaguesResultDto = new CricketLeaguesResultDto();
		cricketLeaguesResultDto.setAnswers(rs.getString("answers"));
		cricketLeaguesResultDto.setQuestionIds(rs.getString("questionIds"));
		cricketLeaguesResultDto.setId(rs.getInt("id"));
		cricketLeaguesResultDto.setCricketLeagueId(rs.getInt("leagueId"));
		cricketLeaguesResultDto.setLeaguesMatchId(rs.getInt("cricketMatchId"));
		cricketLeaguesResultDto.setUserDto(userService.getUserById(rs.getInt("chargers_id")));
		cricketLeaguesResultDto.setCorrectAnswerCount(rs.getInt("correct_answer_count"));
		cricketLeaguesResultDto.setTimestamp(rs.getDate("timestamp"));
		return cricketLeaguesResultDto;
	}

}
