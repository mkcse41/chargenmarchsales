package com.chargenmarch.leagues.enums;

import java.util.HashMap;
import java.util.Map;

public enum IPLTeamsEnum {

	KKR("Kolkata Knight Riders"),
	MI("Mumbai Indians"),
	KXI("Kings XI Punjab"),
	RCB("Royal Challengers Bangalore"),
	GJ("Gujarat Lions"),
	DD("Delhi Daredevils"),
	SRH("Sunrisers Hyderabad"),
	RPS("Rising Pune Supergiants");
	
	String fullName;
	
	private IPLTeamsEnum(String fullName) {
		this.fullName = fullName;
	}
	
	public String getFullName(){
		return fullName;
	}
	
	// Reverse-lookup map for getting a Team Name from an abbreviation
    private static final Map<String, IPLTeamsEnum> lookup = new HashMap<String, IPLTeamsEnum>();
    
	  static {
	        for (IPLTeamsEnum d : IPLTeamsEnum.values()) {
	            lookup.put(d.getFullName(), d);
	        }
	    }
	  
	  public static IPLTeamsEnum getByName(String fullName) {
	        return lookup.get(fullName);
	    }
}
