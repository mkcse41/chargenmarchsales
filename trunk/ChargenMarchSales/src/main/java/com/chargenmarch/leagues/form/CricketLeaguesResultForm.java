package com.chargenmarch.leagues.form;

public class CricketLeaguesResultForm {

	private int leaguesMatchId;
	private String emailId;
	//Comma Seperated Question ID List
	private String questionIds;
	//Comma Seperated Answers List. Order must be same as question ids
	private String answers;
	private String leaguesIdArr;
	public int getLeaguesMatchId() {
		return leaguesMatchId;
	}
	public void setLeaguesMatchId(int leaguesMatchId) {
		this.leaguesMatchId = leaguesMatchId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getQuestionIds() {
		return questionIds;
	}
	public void setQuestionIds(String questionIds) {
		this.questionIds = questionIds;
	}
	public String getAnswers() {
		return answers;
	}
	public void setAnswers(String answers) {
		this.answers = answers;
	}
	public String getLeaguesIdArr() {
		return leaguesIdArr;
	}
	public void setLeaguesIdArr(String leaguesIdArr) {
		this.leaguesIdArr = leaguesIdArr;
	}
	@Override
	public String toString() {
		return "CricketLeaguesResultForm [leaguesMatchId=" + leaguesMatchId + ", emailId=" + emailId + ", questionIds="
				+ questionIds + ", answers=" + answers + ", leaguesIdArr=" + leaguesIdArr + "]";
	}
	
}
