package com.chargenmarch.charge.dto.funds;


import org.joda.time.DateTime;

import com.chargenmarch.common.dto.user.UserDTO;

/**
 * 
 * @author mukesh
 *
 */
public class AddFundsRequestDTO {

	 private int amount;
	 private String paymentMethod;
	 private String bankName;
	 private String bankReferenceId;
	 private String remarks;
	 private UserDTO user;
	 private DateTime addDate;
	 private String status;
	 
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankReferenceId() {
		return bankReferenceId;
	}
	public void setBankReferenceId(String bankReferenceId) {
		this.bankReferenceId = bankReferenceId;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	
	public DateTime getAddDate() {
		return addDate;
	}
	public void setAddDate(DateTime addDate) {
		this.addDate = addDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
