package com.chargenmarch.charge.query.sms;

public class SMSQueries {

	public final static String INSERT_INTO_SMS_LOG = "insert into sms_logs set mobile_number=?, message=?, gateway=?, insert_time=now(), status=?,response='', update_time=now();";
	public final static String UPDATE_SMS_STATUS = "update sms_logs set status=?,response=? where id=?";
}
