package com.chargenmarch.charge.controller.recharge.mobile;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.chargenmarch.charge.constants.recharge.RechargeConstants;
import com.chargenmarch.charge.dto.mobilerecharge.Operator;
import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.charge.dto.rewardspoint.RewardsPoint;
import com.chargenmarch.charge.enums.recharge.DTHOperatorCodes;
import com.chargenmarch.charge.enums.recharge.DatacardOperatorCodes;
import com.chargenmarch.charge.enums.recharge.PostpaidOperatorCodes;
import com.chargenmarch.charge.enums.recharge.PrepaidOperatorCodes;
import com.chargenmarch.charge.enums.recharge.StateCodes;
import com.chargenmarch.charge.forms.recharge.RechargeForm;
import com.chargenmarch.charge.joloapi.dto.JOLOOperator;
import com.chargenmarch.charge.joloapi.enums.billing.LandlineOperatorCodes;
import com.chargenmarch.charge.joloapi.service.IRechargeService;
import com.chargenmarch.charge.service.coupon.ICouponService;
import com.chargenmarch.charge.service.mobilerecharge.IMobileRechargeService;
import com.chargenmarch.charge.service.rewardspoint.IRewardsPointsService;
import com.chargenmarch.charge.thread.PaymentTrackerThread;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.RequestConstants;
import com.chargenmarch.common.constants.SessionConstants;
import com.chargenmarch.common.constants.ViewConstants;
import com.chargenmarch.common.constants.paymentgateway.PaymentConstants;
import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.chargenmarch.common.dto.user.PaymentLogDto;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.service.user.IPaymentLogService;
import com.chargenmarch.common.service.user.IUserService;
import com.chargenmarch.common.utility.IUtility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 
 * @author mukesh
 *
 */

@Controller
@RequestMapping(value = RequestConstants.RECHARGE)
public class RechargeController {

	private final static Logger LOGGER = LoggerFactory.getLogger(RechargeController.class);

	@Autowired
	IMobileRechargeService mobileRechargeService;

	@Autowired
	IUtility utility;

	@Autowired
	IRewardsPointsService rewardsPointsService;

	@Autowired
	IUserService userService;

	@Autowired
	IEmailSenderService emailSenderService;

	@Autowired
	ICouponService couponService;

	@Autowired
	IRechargeService rechargeService;

	@Autowired
	ThreadPoolTaskExecutor taskExecutor;

	@Autowired
	PaymentTrackerThread tracker;

	@Autowired
	IPaymentLogService trackingService;
	
	@RequestMapping(value = "/home", method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD})
	public ModelAndView welcome(ModelMap model,HttpServletRequest request,HttpServletResponse response) throws IOException {
		ModelAndView mnv = new ModelAndView(utility.getviewResolverName(request,ViewConstants.VIEW_INDEX));
		if(null!=request.getSession().getAttribute(SessionConstants.USER_BEAN)){
			UserDTO user = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
			request.setAttribute(Constants.USER, user);
			setOperatorCodeValues(model, request);
			removeSessionAttributes(request);
			return mnv;
		}
		response.sendRedirect(utility.getBaseUrl());
	    return null;
	}
	
	/**
	 * Get operator details by mobile number
	 * @param mobileNumber
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = RechargeConstants.OPERATOR_DETAILS, method = RequestMethod.POST)
	public @ResponseBody Operator getOperatorDetails(@RequestParam(RechargeConstants.MOBILE_NUMBER) String mobileNumber,
			HttpServletRequest request) throws Exception {
		if (null != mobileNumber && !mobileNumber.equals(Constants.BLANK)) {
			Operator operator = null;
			LOGGER.info("Recharge Service Name==>" + mobileRechargeService.getRechargeServiceName(request));
			if (mobileRechargeService.getRechargeServiceName(request).equalsIgnoreCase(Constants.JOLO)) {
				LOGGER.info("Recharge Service Name==>JOLO");
				operator = rechargeService.getOperatorDetailByNumber(mobileNumber);
			} else {
				LOGGER.info("Recharge Service Name==>Pay2All");
				operator = mobileRechargeService.getOperatorbyNumber(mobileNumber);
			}
			return operator;
		}
		return null;
	}

	/**
	 * Review mobile recharge order. (i) Show reedom points details (ii) mobile
	 * plan details
	 * 
	 * @throws IOException
	 */
	@RequestMapping(value = RequestConstants.REVIEW_ORDER_HTM, method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView reviewOrder(HttpServletRequest request, HttpServletResponse response) throws IOException {
		request.getSession().removeAttribute(RequestConstants.MOBILE_RECHARGE_DTO);
		RechargeForm rechargeForm = getRechargeFormObject(request);
		RechargeDTO rechargeDto = null;
		UserDTO user = null;
		try {
			if (null != request.getSession().getAttribute(SessionConstants.USER_BEAN) && null != rechargeForm) {
				ModelAndView view = new ModelAndView(utility.getviewResolverName(request, ViewConstants.REVIEW_ORDER));
				user = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
				int rechargeAmount = utility.convertStringToInt(rechargeForm.getAmount());
				int paidAmount = calculatePaidAmount(rechargeAmount, user.getWalletBalance());
				rechargeDto = mobileRechargeService.setValuesInrechargeDto(
						utility.convertStringToInt(rechargeForm.getAmount()), user, null, rechargeForm.getNumber(),
						null, null);
				rechargeDto = setValuesInRechargeDto(rechargeDto, rechargeForm, request);
				request.getSession().setAttribute(RequestConstants.MOBILE_RECHARGE_DTO, rechargeDto);
				request.getSession().setAttribute(SessionConstants.RECHARGE_FORM, rechargeForm);
				view.addObject(Constants.MOBILE_NO, rechargeForm.getNumber());
				view.addObject(Constants.AMOUNT, rechargeDto.getAmount());
				view.addObject(RequestConstants.EXTRA_AMOUNT, rechargeDto.getInternetCharges());
				view.addObject(Constants.PAID_AMOUNT, paidAmount);
				view.addObject(Constants.OPERATOR, rechargeForm.getOperatorText());
				view.addObject(RequestConstants.SERVICE, rechargeForm.getService());
				view.addObject(RequestConstants.RECHARGE_DTO, rechargeDto);
				view.addObject(RequestConstants.REWARDS_POINTS, rewardsPointsService.getRewardsPointDetails(user));
				return view;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		response.sendRedirect(utility.getBaseUrl());
		return null;
	}

	/**
	 * Confirm mobile recharge order. (i) Calculate recharge Amount (reedom
	 * rewards points) (ii) Redirect payment gateway
	 * 
	 * @throws IOException
	 */
	@RequestMapping(value = RequestConstants.CONFIRM_ORDER, method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView confirmOrder(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			int paidAmount = 0;
			if (null != request.getSession().getAttribute(SessionConstants.USER_BEAN)
					&& null != request.getSession().getAttribute(RequestConstants.MOBILE_RECHARGE_DTO)) {
				String isUseWallet = request.getParameter(RequestConstants.IS_USE_WALLET);
				String isUseReffer = request.getParameter(RequestConstants.IS_USE_REFERRAR);
				String isReedom = request.getParameter(RequestConstants.IS_REDEEM);
				String couponName = request.getParameter(RequestConstants.COUPON_NAME);
				String serviceType = request.getParameter(RequestConstants.SERVICE_TYPE);
				UserDTO user = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
				request.getSession().removeAttribute(RequestConstants.RECHARGE_FORM);
				request.getSession().removeAttribute(RequestConstants.QUICK_RECHARGE_CHECK_VAL);
				RechargeDTO rechargeDto = (RechargeDTO) request.getSession()
						.getAttribute(RequestConstants.MOBILE_RECHARGE_DTO);
				rechargeDto.setCouponName(couponName);
				rechargeDto.setServiceType(serviceType);
				rechargeDto.setSessionId(request.getSession().getId());
				if (isUseWallet != null && isUseWallet.equals(Constants.TRUE)) {
					rechargeDto.setUseWallet(true);
				}else{
				   rechargeDto.setUseWallet(true);// Set wallet true for ChargenMarch Sales user.
				}
				if (null != isUseReffer && isUseReffer.equals(Constants.TRUE)) {
					rechargeDto.setUseReferral(true);
				}
				/**
				 * 17 Sept 2016
				 * For Browser Notification
				 */
				if(null != user.getBrowserNotificationId() && null != rechargeDto.getCouponName() && rechargeDto.getCouponName().contains(Constants.NOTIFICATION)){
					rechargeDto.setCouponName(couponName);
				}else{
					rechargeDto.setCouponName(Constants.BLANK);
				}
				// If user redeem your rewards points in Case calculate paid
				// amount for recharge.
				paidAmount = calculatePaidAmountRewardsPointCase(isReedom, user, rechargeDto, paidAmount);
				// If user use CAM wallet
				if (rechargeDto.isUseWallet()) {
					if (user.getWalletBalance() >= paidAmount) {
						// rechargeDto.setCouponName(Constants.BLANK); //for
						// commented code recharge wallet case
						String orderId = utility.generateRandomString();
						rechargeDto = setRechargeAmount(rechargeDto);
						if (mobileRechargeService.getRechargeServiceName(request).equalsIgnoreCase(Constants.JOLO)) {
							rechargeDto = rechargeService.mobileDthAndDataCardRecharge(rechargeDto, orderId, user);
						} else {
							rechargeDto = mobileRechargeService.mobileDthAndDataCardRecharge(rechargeDto, orderId,
									user);
						}
						request.setAttribute(RequestConstants.RECHARGE_DTO, rechargeDto);
						request.getSession().removeAttribute(RequestConstants.MOBILE_RECHARGE_DTO);
						if (rechargeDto.getIsSuccesfulRecharge() == 1) {
							// update wallet balance
							user.setWalletBalance(user.getWalletBalance() - rechargeDto.getAmount());
							userService.updateUserWalletBalance(user);
							ModelAndView view = new ModelAndView(
									utility.getviewResolverName(request, ViewConstants.RECHARGE_SUCCESS));
							//Block Begins to track User Activities
							paymentTrackingDetail(request, rechargeDto, user, true, true, true,false);
							//Block Ends to track User Activities
							return view;
						} else {
							request.setAttribute(PaymentConstants.PAYMENT_STATUS,
									PaymentConstants.PAYMENT + Constants.SUCCESS_CAP.toLowerCase());
							ModelAndView view = new ModelAndView(
									utility.getviewResolverName(request, ViewConstants.RECHARGE_FAIURE));
							//Block Begins to track User Activities
							paymentTrackingDetail(request, rechargeDto, user, false, true, true, false);
							//Block Ends to track User Activities
							return view;
						}
					} else {
						request.setAttribute(PaymentConstants.PAYMENT_STATUS,
								PaymentConstants.PAYMENT + Constants.SUCCESS_CAP.toLowerCase());
						ModelAndView view = new ModelAndView(
								utility.getviewResolverName(request, ViewConstants.RECHARGE_FAIURE));
						//Block Begins to track User Activities
						paymentTrackingDetail(request, rechargeDto, user, false, true, true, false);
						//Block Ends to track User Activities
						return view;
					}
				} else {
					request.setAttribute(PaymentConstants.PAYMENT_STATUS,
							PaymentConstants.PAYMENT + Constants.SUCCESS_CAP.toLowerCase());
					ModelAndView view = new ModelAndView(
							utility.getviewResolverName(request, ViewConstants.RECHARGE_FAIURE));
					//Block Begins to track User Activities
					paymentTrackingDetail(request, rechargeDto, user, false, true, true, false);
					//Block Ends to track User Activities
					return view;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("error in confirm mobile recharge order");
			emailSenderService.sendEmailWithMultipleRecipient(Constants.NOREPLY_EMAIL_ID,
					RechargeConstants.RECHARGE_ERROR_EMAIL_ID.split(Constants.COMMA), null,
					"error in confirm mobile recharge order", e.getMessage(), null, null, false);
		}
		response.sendRedirect(utility.getBaseUrl());
		return null;
	}

	/**
	 * if recharge service type is mobile postpaid case,add some extra amount.
	 * 
	 * @param rechargeDto
	 * @return
	 */
	private RechargeDTO setRechargeAmount(RechargeDTO rechargeDto) {
		if (null != rechargeDto.getMobileService() && rechargeDto.getMobileService().equals(Constants.POSTPAID)) {
			rechargeDto.setAmount(rechargeDto.getAmount() - rechargeDto.getInternetCharges());
		} else {
			rechargeDto.setInternetCharges(0);
		}
		return rechargeDto;
	}

	/**
	 * If any case postpaid recharge is fail then recharge amount and internet
	 * charge amount to added in CAM Wallet,
	 * 
	 * @param rechargeDto
	 * @return
	 */
	private RechargeDTO getPaidPostpaidRechargeAmount(RechargeDTO rechargeDto) {
		if (null != rechargeDto.getMobileService() && rechargeDto.getMobileService().equals(Constants.POSTPAID)) {
			rechargeDto.setAmount(rechargeDto.getAmount() + rechargeDto.getInternetCharges());
		}
		return rechargeDto;
	}

	/**
	 * If user select 1-step recharge order. Confirm mobile recharge order. (i)
	 * Calculate recharge Amount (reedom rewards points) (ii) Redirect payment
	 * gateway
	 * 
	 * @throws IOException
	 */
	@RequestMapping(value = RequestConstants.QUICK_RECHARGE_CONFIRM_ORDER, method = { RequestMethod.GET,
			RequestMethod.POST })
	public ModelAndView quickRechrageConfirmOrder(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		try {
			int paidAmount = 0;
			UserDTO user = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
			RechargeForm rechargeForm = getRechargeFormObject(request);
			RechargeDTO rechargeDto = mobileRechargeService.setValuesInrechargeDto(
					utility.convertStringToInt(rechargeForm.getAmount()), user, null, rechargeForm.getNumber(), null,
					null);
			rechargeDto = setValuesInRechargeDto(rechargeDto, rechargeForm, request);
			rechargeDto.setIsQuickRecharge(Constants.ONE);
			if (null != request.getSession().getAttribute(SessionConstants.USER_BEAN) && null != rechargeDto) {
				String isUseWallet = request.getParameter(RequestConstants.IS_USE_WALLET);
				String isReedom = request.getParameter(RequestConstants.IS_REDEEM);
				String couponName = request.getParameter(RequestConstants.COUPON_NAME);
				request.getSession().removeAttribute(RequestConstants.RECHARGE_FORM);
				request.getSession().removeAttribute(RequestConstants.QUICK_RECHARGE_CHECK_VAL);
				rechargeDto.setCouponName(couponName);
				rechargeDto.setUseWallet(true);// Set wallet true for ChargenMarch Sales user.
				// If user redeem your rewards points in Case calculate paid
				// amount for recharge.
				paidAmount = calculatePaidAmountRewardsPointCase(isReedom, user, rechargeDto, paidAmount);
				// If user use CAM wallet
				if (rechargeDto.isUseWallet()) {
					if (user.getWalletBalance() >= paidAmount) {
						// rechargeDto.setCouponName(Constants.BLANK); //for
						// commented code recharge wallet case
						String orderId = String.valueOf(new Date().getTime()).substring(0, 7);
						rechargeDto = setRechargeAmount(rechargeDto);
						if (mobileRechargeService.getRechargeServiceName(request).equalsIgnoreCase(Constants.JOLO)) {
							rechargeDto = rechargeService.mobileDthAndDataCardRecharge(rechargeDto, orderId, user);
						} else {
							rechargeDto = mobileRechargeService.mobileDthAndDataCardRecharge(rechargeDto, orderId,
									user);
						}
						request.setAttribute(RequestConstants.RECHARGE_DTO, rechargeDto);
						request.getSession().removeAttribute(RequestConstants.MOBILE_RECHARGE_DTO);
						if (rechargeDto.getIsSuccesfulRecharge() == 1) {
							// update wallet balance
							user.setWalletBalance(user.getWalletBalance() - rechargeDto.getAmount());
							userService.updateUserWalletBalance(user);
							//Block Begins to track User Activities
							paymentTrackingDetail(request, rechargeDto, user, true, true, true,false);
							//Block Ends to track User Activities
							ModelAndView view = new ModelAndView(
									utility.getviewResolverName(request, ViewConstants.RECHARGE_SUCCESS));
							return view;
						} else {
							request.setAttribute(PaymentConstants.PAYMENT_STATUS,
									PaymentConstants.PAYMENT + Constants.SUCCESS_CAP.toLowerCase());
							ModelAndView view = new ModelAndView(
									utility.getviewResolverName(request, ViewConstants.RECHARGE_FAIURE));
							//Block Begins to track User Activities
							paymentTrackingDetail(request, rechargeDto, user, false, false, false,true);
							//Block Ends to track User Activities
							return view;
						}
					} else {
						request.setAttribute(PaymentConstants.PAYMENT_STATUS,
								PaymentConstants.PAYMENT + Constants.SUCCESS_CAP.toLowerCase());
						ModelAndView view = new ModelAndView(
								utility.getviewResolverName(request, ViewConstants.RECHARGE_FAIURE));
						//Block Begins to track User Activities
						paymentTrackingDetail(request, rechargeDto, user, false, false, false,true);
						//Block Ends to track User Activities
						return view;
					}
				} else {
					request.setAttribute(PaymentConstants.PAYMENT_STATUS,
							PaymentConstants.PAYMENT + Constants.SUCCESS_CAP.toLowerCase());
					ModelAndView view = new ModelAndView(
							utility.getviewResolverName(request, ViewConstants.RECHARGE_FAIURE));
					//Block Begins to track User Activities
					paymentTrackingDetail(request, rechargeDto, user, false, false, false,true);
					//Block Ends to track User Activities
					return view;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("error in confirm mobile recharge order");
			emailSenderService.sendEmailWithMultipleRecipient(Constants.NOREPLY_EMAIL_ID,
					RechargeConstants.RECHARGE_ERROR_EMAIL_ID.split(Constants.COMMA), null,
					"error in confirm mobile recharge order", e.getMessage(), null, null, false);
		}
		response.sendRedirect(utility.getBaseUrl());
		return null;
	}

	private int calculatePaidAmountRewardsPointCase(String isReedom, UserDTO user, RechargeDTO rechargeDto,
			int paidAmount) {
		paidAmount = rechargeDto.getAmount();
		if (null != isReedom && isReedom.equals(Constants.TRUE)) {
			RewardsPoint rewardsPoint = rewardsPointsService.reedomRewardsPoint(user, true);
			rechargeDto.setRedeemRewardsPoints(true);
			rechargeDto.setRedeemRewardsPointsValue(rewardsPoint.getRewardsPoint_value());
			if (!rechargeDto.isUseWallet() && null != rewardsPoint) {
				paidAmount = calculatePaidAmount(paidAmount, rewardsPoint.getRewardsPoint_value());
			}
		}
		return paidAmount;
	}

	private OrderInfo iniliazeOrderInfo(RechargeDTO rechargeDto, HttpServletRequest request, int paidAmount) {
		String userCredentials = rechargeDto.getUserDto().getId() + rechargeDto.getUserDto().getEmailId();
		OrderInfo orderInfo = utility.initializeOrderInfoForPaymentGateway(rechargeDto.getUserDto().getName(), null,
				rechargeDto.getUserDto().getEmailId(), rechargeDto.getNumber(), null, null, null, null,
				RequestConstants.MOBILE_DATACARD_DTH_CHARGE, paidAmount, RequestConstants.PAY_U, userCredentials);
		request.setAttribute(RequestConstants.ORDER_INFO, orderInfo);
		request.getSession().setAttribute(RequestConstants.MOBILE_RECHARGE_DTO, rechargeDto);
		request.setAttribute(RequestConstants.URL, utility.getBaseUrl() + RechargeConstants.SLASH
				+ PaymentConstants.PAYMENT.toLowerCase() + RequestConstants.PROCESSED_PAYMENT);
		return orderInfo;
	}

	/**
	 * If payment is success 1.Recharge mobile (i) if payment is success then
	 * recharge mobile. 2.Generate rewards points 3.Save mobile recharge details
	 * in DB.
	 * 
	 * @throws IOException
	 */
	@RequestMapping(value = RequestConstants.MOBILE_RECHARGE, method = { RequestMethod.POST })
	public ModelAndView mobileDthAndDataCardRecharge(HttpServletRequest request, HttpServletResponse httpResponse)
			throws IOException {
		String response = request.getParameter(RequestConstants.RESPONSE);
		try {
			if (response != null && null != request.getSession().getAttribute(SessionConstants.USER_BEAN)
					&& null != response
					&& null != request.getSession().getAttribute(RequestConstants.MOBILE_RECHARGE_DTO)
					&& utility.isValidatePayment(request)) {
				Gson gson = new GsonBuilder().create();
				OrderInfo orderInfo = gson.fromJson(response, OrderInfo.class);
				UserDTO user = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
				RechargeDTO rechargeDto = (RechargeDTO) request.getSession()
						.getAttribute(RequestConstants.MOBILE_RECHARGE_DTO);
				String orderId = orderInfo.getTxnId();
				String status = orderInfo.getStatus();
				request.getSession().removeAttribute(RequestConstants.RECHARGE_FORM);
				if (!orderId.equals(Constants.BLANK) && status.equals(Constants.SUCCESS)
						&& mobileRechargeService.getRechargeListByPGOrderId(orderId).size() <= 0) {
					rechargeDto = setRechargeAmount(rechargeDto);
					if (mobileRechargeService.getRechargeServiceName(request).equalsIgnoreCase(Constants.JOLO)) {
						rechargeDto = rechargeService.mobileDthAndDataCardRecharge(rechargeDto, orderId, user);
					} else {
						rechargeDto = mobileRechargeService.mobileDthAndDataCardRecharge(rechargeDto, orderId, user);
					}
					request.setAttribute(RequestConstants.RECHARGE_DTO, rechargeDto);
					request.getSession().removeAttribute(RequestConstants.MOBILE_RECHARGE_DTO);
					if (rechargeDto.getIsSuccesfulRecharge() == 1) {
						// update wallet balance
						updateUserWalletBalance(rechargeDto, user);
						paymentTrackingDetail(request, rechargeDto, user, true, true, false,false);
						ModelAndView view = new ModelAndView(utility.getviewResolverName(request, ViewConstants.RECHARGE_SUCCESS));
						return view;
					} else {
						paymentTrackingDetail(request, rechargeDto, user, false, true, false,false);
						rechargeDto = getPaidPostpaidRechargeAmount(rechargeDto);
						int paidAmount = rechargeDto.getAmount();
						if (rechargeDto.isUseWallet()) {
							paidAmount = calculatePaidAmount(rechargeDto.getAmount(), user.getWalletBalance());
						}
						user.setWalletBalance(user.getWalletBalance() + paidAmount);
						userService.updateUserWalletBalance(user);
						request.setAttribute(PaymentConstants.PAYMENT_STATUS, orderInfo.getStatus());
						ModelAndView view = new ModelAndView(
								utility.getviewResolverName(request, ViewConstants.RECHARGE_FAIURE));
						return view;
					}
				} else {
					paymentTrackingDetail(request, rechargeDto, user, false, false, false,false);
					rechargeDto.setOrderId(orderId);
					mobileRechargeService.saveMobileRechareDetails(rechargeDto);
					request.setAttribute(RequestConstants.RECHARGE_DTO, rechargeDto);
					ModelAndView view = new ModelAndView(
							utility.getviewResolverName(request, ViewConstants.RECHARGE_FAIURE));
					return view;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error in MobileRechargeController==>mobileRecharge method");
			emailSenderService.sendEmailWithMultipleRecipient(Constants.NOREPLY_EMAIL_ID,
					RechargeConstants.RECHARGE_ERROR_EMAIL_ID.split(Constants.COMMA), null,
					"error in mobile recharge method", e.getMessage(), null, null, false);
		}
		httpResponse.sendRedirect(utility.getBaseUrl());
		return null;
	}

	private void updateUserWalletBalance(RechargeDTO rechargeDto, UserDTO user) {
		if (rechargeDto.isUseWallet()) {
			int calculateWalletBalance = calculatePaidAmount(user.getWalletBalance(), rechargeDto.getAmount());
			user.setWalletBalance(calculateWalletBalance);
			userService.updateUserWalletBalance(user);
		}
		if (rechargeDto.isRedeemRewardsPoints()) {
			int calculateWalletBalance = calculatePaidAmount(user.getWalletBalance(),
					rechargeDto.getRedeemRewardsPointsValue());
			user.setWalletBalance(calculateWalletBalance);
			userService.updateUserWalletBalance(user);
		}
		if (rechargeDto.isUseReferral()) {
			int referralAmount = rechargeService.calculatePaidAmountUserReferralAmountCase(rechargeDto.getAmount());
			if (user.getReferralBalance() >= referralAmount) {
				user.setReferralBalance(user.getReferralBalance() - referralAmount);
			} else {
				user.setReferralBalance(0);
			}
			userService.updateUserRefferalBalance(user);
		}
		userService.updateUserCacheMaps(user);
	}

	@RequestMapping(value = "/getPlans", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView setMobilePlansDropDown(@RequestParam("mobileNumber") String mobileNumber,
			@RequestParam("operatorCode") String operatorCode,
			@RequestParam(value = "zoneCode", required = false) String circleCode, HttpServletRequest request) {
		ModelAndView mnv;
		JOLOOperator operator = rechargeService.getJoloOperatorByPayCode(operatorCode);
		String isMobile = (String) request.getSession().getAttribute(Constants.IS_MOBILE);
		if (null != isMobile && isMobile.equalsIgnoreCase(Constants.STR_TRUE)) {
			mnv = new ModelAndView("wap-jsp/wap-jspincludes/wap-mobilePlan");
		} else {
			mnv = new ModelAndView("jsp-includes/mobilePlan");
		}
		mnv.addObject("planList", rechargeService.getOperatorAndCircleWisePlans(operator.getCode(), circleCode));
		return mnv;
	}

	/**
	 * This method used to save payment tracking details.
	 * @param request
	 * @param rechargeDto
	 * @param user
	 * @param isSuccessFulRecharge
	 * @param isPaymentStatus
	 * @param isWallet
	 */
	private void paymentTrackingDetail(HttpServletRequest request,RechargeDTO rechargeDto, UserDTO user, boolean isSuccessFulRecharge, boolean isPaymentStatus, boolean isWallet,boolean walletWithGateway){
		try{
			boolean isUpdate = false;
			PaymentLogDto paymentLogDto = null;
			int paymentLogId = 0;
			try{
			   paymentLogId = (Integer) request.getSession().getAttribute("plid");
			}catch(Exception e){
				e.printStackTrace();
			}
			if(paymentLogId != 0){
			   isUpdate = true;
			   paymentLogDto = trackingService.getPaymentLogById(paymentLogId);
			   paymentLogDto.setRechargeDto(rechargeDto);
			}
			if(paymentLogDto == null){
				paymentLogDto = setUserTrackingDto(request, rechargeDto, user);
			}
			paymentLogDto.setSuccessfulRecharge(isSuccessFulRecharge);
			paymentLogDto.setPaymentStatus(isPaymentStatus);
			paymentLogDto.getRechargeDto().setUseWallet(isWallet);
			paymentLogDto.setFailureReason(rechargeDto.getError());
			paymentLogDto.setWalletWithGateway(walletWithGateway);
			tracker.setSession(request.getSession());
			trackingService.invokePaymentTrackerThread(taskExecutor, tracker, paymentLogDto, isUpdate);
		} catch (Exception e) {
			LOGGER.error("Wallet Payment - Exception while saving Payment tracking logs.", e);
		}
	}
	
	   /**
	    * This method used to get Recharge service Name then set recharge service wise operator details..
	    * @param model
	    * @param request
	    */
	 private void setOperatorCodeValues(ModelMap model,HttpServletRequest request){
		   if (mobileRechargeService.getRechargeServiceName(request).equalsIgnoreCase(Constants.JOLO)) {
			    model.addAttribute(RechargeConstants.PREPAID_OPERATORS, rechargeService.getOperatorList(Constants.PREPAID));
				model.addAttribute(RechargeConstants.POSTPAID_OPERATORS, rechargeService.getOperatorList(Constants.POSTPAID));
				model.addAttribute(RechargeConstants.DTH_OPERATORS, rechargeService.getOperatorList(Constants.DTH));
				model.addAttribute(RechargeConstants.DATACARD_OPERATORS, rechargeService.getOperatorList(Constants.DATACARD));
				model.addAttribute(RechargeConstants.CIRCLES, rechargeService.getAllCircleCodeList());
		   }else{
			    model.addAttribute(RechargeConstants.PREPAID_OPERATORS, PrepaidOperatorCodes.values());
				model.addAttribute(RechargeConstants.POSTPAID_OPERATORS, PostpaidOperatorCodes.values());
				model.addAttribute(RechargeConstants.DTH_OPERATORS, DTHOperatorCodes.values());
				model.addAttribute(RechargeConstants.DATACARD_OPERATORS, DatacardOperatorCodes.values());
				model.addAttribute(RechargeConstants.CIRCLES, StateCodes.values());
		   }
		   model.addAttribute(RechargeConstants.RECHARGE_SERVICE_NAME, mobileRechargeService.getRechargeServiceName(request));
		   model.addAttribute(RechargeConstants.LANDLINE_OPERATORS, LandlineOperatorCodes.values());
	 }
	 
	 /*
	  * Remove session attributes
	  */
	 private void removeSessionAttributes(HttpServletRequest request){
			request.getSession().removeAttribute(RequestConstants.MOBILE_RECHARGE_DTO);
			request.getSession().removeAttribute(SessionConstants.RECHARGE_FORM);
			request.getSession().removeAttribute(RequestConstants.LANDLINE_RECHARGE_DTO);
			request.getSession().removeAttribute(SessionConstants.LANDLINE_FORM);
	 }
	 
	 /**
		 * Set payment tracking details.
		 * @param request
		 * @param rechargeDto
		 * @param user
		 * @return
		 */
		private PaymentLogDto setUserTrackingDto(HttpServletRequest request, RechargeDTO rechargeDto, UserDTO user) {
			PaymentLogDto dto = new PaymentLogDto();
			dto.setChargerId(user.getId());
			dto.setIpAddress(utility.getIpAdress(request));
			//TO-DO
			//dto.setCity(city);
			dto.setSessionId(request.getSession().getId());
			dto.setTimestamp(new Date());
			if(rechargeDto == null) {
				dto.setRechargeDto(new RechargeDTO());
			} else {
				dto.setRechargeDto(rechargeDto);
			}
			return dto;
		}

		private int calculatePaidAmount(int rechargeAmount, int walletBalance) {
			int paidAmount = 0;
			if (rechargeAmount >= walletBalance) {
				paidAmount = rechargeAmount - walletBalance;
			}
			return paidAmount;
		}

		private RechargeDTO setValuesInRechargeDto(RechargeDTO rechargeDto, RechargeForm rechargeForm,
				HttpServletRequest request) {
			rechargeDto.setOperatorValue(rechargeForm.getOperatorValue());
			rechargeDto.setMobileService(rechargeForm.getMobileRechargeService());
			rechargeDto.setServiceType(rechargeForm.getService());
			rechargeDto.setOperatorName(rechargeForm.getOperatorText());
			rechargeDto.setCircleName(rechargeForm.getCircleName());
			rechargeDto.setCircleValue(rechargeForm.getCircleValue());
			int rechargeAmountBasicExtra = mobileRechargeService.addPostpaidBillExtraCharges(rechargeDto.getAmount());
			rechargeDto.setInternetCharges(rechargeAmountBasicExtra - rechargeDto.getAmount());
			rechargeDto.setAmount(rechargeAmountBasicExtra);
			if (rechargeDto.getMobileService() == null || !rechargeDto.getMobileService().equals(Constants.POSTPAID)) {
				rechargeDto.setAmount(rechargeDto.getAmount() - rechargeDto.getInternetCharges());
			}
			String source = (String) request.getSession().getAttribute(Constants.UTM_SOURCE);
			rechargeDto.setRechargeSource(source);
			return rechargeDto;
		}

		private RechargeForm getRechargeFormObject(HttpServletRequest request) {
			RechargeForm rechargeForm = null;
			if (request.getParameter(RequestConstants.NUMBER) == null
					&& null != request.getSession().getAttribute(SessionConstants.RECHARGE_FORM)) {
				rechargeForm = (RechargeForm) request.getSession().getAttribute(SessionConstants.RECHARGE_FORM);
			} else {
				rechargeForm = new RechargeForm();
				rechargeForm.setNumber(request.getParameter(RequestConstants.NUMBER));
				rechargeForm.setMobileRechargeService(request.getParameter(RequestConstants.MOBILE_RECHARGE_SERVICE));
				rechargeForm.setOperatorValue(request.getParameter(RequestConstants.OPERATOR_VALUE));
				rechargeForm.setOperatorText(request.getParameter(RequestConstants.OPERATOR_TEXT));
				rechargeForm.setService(request.getParameter(RequestConstants.SERVICE));
				rechargeForm.setAmount(request.getParameter(RequestConstants.AMOUNT));
				rechargeForm.setCircleName(request.getParameter(RequestConstants.CIRCLE_NAME));
				rechargeForm.setCircleValue(request.getParameter(RequestConstants.CIRCLE_VALUE));
			}
			return rechargeForm;
		}
	
}
