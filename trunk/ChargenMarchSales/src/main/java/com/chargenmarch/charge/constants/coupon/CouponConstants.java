/**
 * 
 */
package com.chargenmarch.charge.constants.coupon;

/**
 * @author mukesh
 *
 */
public class CouponConstants {

	public static final String COUPON_NAME = "coupon_name";
	public static final String COUPON_TYPE = "coupon_type";
	public static final String COUPON_VALUE = "coupon_value";
	public static final String COUPON_USES = "coupon_uses";
	public static final String COUPON_LIMIT = "coupon_limit";
	public static final String MINIMUM_RECHARGE_VALUE = "minimum_recharge_value";
	public static final String COUPON_SERVICE_TYPE = "coupon_service_type";
	public static final String COUPON_STATUS = "coupon_status";
	public static final String START_DATE = "start_date";
	public static final String END_DATE = "end_date";
	public static final String COUPON_PER_USER_LIMIT = "coupon_per_user_limit";
	public static final String FIXED = "fixed";
	public static final String PERCENTAGE = "percentage";
	public static final String MAXIMUM_CASHBACK = "maximumcashback";
	
}

