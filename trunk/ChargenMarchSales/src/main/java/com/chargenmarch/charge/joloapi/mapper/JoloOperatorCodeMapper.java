package com.chargenmarch.charge.joloapi.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.charge.joloapi.constants.JOLOAPIConstants;
import com.chargenmarch.charge.joloapi.dto.JOLOOperator;

/**
 * 
 * @author mukesh
 *
 */
public class JoloOperatorCodeMapper implements RowMapper<JOLOOperator>{
    
	public JOLOOperator mapRow(ResultSet rs, int rowNum) throws SQLException {
		JOLOOperator joloOperatorCode = new JOLOOperator();
		joloOperatorCode.setOperatorName(rs.getString(JOLOAPIConstants.OPERATOR_NAME));
		joloOperatorCode.setPay_code(rs.getString(JOLOAPIConstants.PAY_CODE));
		joloOperatorCode.setCode(rs.getString(JOLOAPIConstants.CODE));
		joloOperatorCode.setRechargeServiceType(rs.getString(JOLOAPIConstants.RECHARGE_SERVICE_TYPE));
		return joloOperatorCode;
	}
}
