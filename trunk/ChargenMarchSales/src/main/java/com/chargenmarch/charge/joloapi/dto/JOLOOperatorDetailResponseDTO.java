package com.chargenmarch.charge.joloapi.dto;

/**
 * 
 * @author mukesh
 *
 */
public class JOLOOperatorDetailResponseDTO {
	
	private String operator_code;
    private String circle_code;
	public String getOperator_code() {
		return operator_code;
	}
	public void setOperator_code(String operator_code) {
		this.operator_code = operator_code;
	}
	public String getCircle_code() {
		return circle_code;
	}
	public void setCircle_code(String circle_code) {
		this.circle_code = circle_code;
	}
    
}
