package com.chargenmarch.charge.joloapi.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.chargenmarch.charge.constants.recharge.RechargeConstants;
import com.chargenmarch.charge.dto.coupon.CouponDTO;
import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.charge.dto.rewardspoint.RewardsPoint;
import com.chargenmarch.charge.forms.coupon.CouponForm;
import com.chargenmarch.charge.joloapi.constants.JOLOAPIConstants;
import com.chargenmarch.charge.joloapi.constants.JOLORechargeResponseDTO;
import com.chargenmarch.charge.joloapi.dao.IRechargeDao;
import com.chargenmarch.charge.joloapi.dao.billing.IBillingDao;
import com.chargenmarch.charge.joloapi.dto.billing.LandLineRechargeDTO;
import com.chargenmarch.charge.joloapi.forms.billing.LandlineForm;
import com.chargenmarch.charge.joloapi.thread.GenerateBillingUtilityThread;
import com.chargenmarch.charge.service.coupon.ICouponService;
import com.chargenmarch.charge.service.rewardspoint.IRewardsPointsService;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.FilePathConstants;
import com.chargenmarch.common.constants.PropertyKeyConstants;
import com.chargenmarch.common.constants.RequestConstants;
import com.chargenmarch.common.constants.SessionConstants;
import com.chargenmarch.common.constants.paymentgateway.PaymentConstants;
import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.service.http.IHTTPService;
import com.chargenmarch.common.service.user.IUserService;
import com.chargenmarch.common.utility.IUtility;

/**
 * This Service used for all billing process, based on jolo api....
 * @author rajat
 *
 */
@Service
@PropertySource(FilePathConstants.CLASSPATH+FilePathConstants.JOLOAPI_PROPERTIES)
public class BillingServiceImpl implements IBillingService {


	private final static Logger LOGGER = LoggerFactory.getLogger(BillingServiceImpl.class);
	
	@Autowired
	private Environment env;
	
	@Autowired
	private IHTTPService httpService;
	
	@Autowired
	ICouponService couponService;
	
	@Autowired
	IUtility utility;
	
	@Autowired
	IEmailSenderService emailSenderService;
	
	@Autowired
	IRewardsPointsService rewardPointService;
	
	@Autowired
	IBillingDao billingDao;
	
	@Autowired 
	IUserService userService;
	
	@Autowired
	ThreadPoolTaskExecutor taskExecutor;
	
	@Autowired
	GenerateBillingUtilityThread generateBillingUtilityThread;
	
	@Autowired
	IRechargeDao rechargeDao;
	

	
	public int calculatePaidAmount(int rechargeAmount, int walletBalance){
		 int paidAmount = 0;
		 if(rechargeAmount>=walletBalance){
			 paidAmount = rechargeAmount - walletBalance;
		 }
		 return paidAmount;
	}
	
	public LandLineRechargeDTO setValuesInLandlineRechargeDto(UserDTO userDto,String orderId,LandlineForm landlineForm,LandLineRechargeDTO landLineDto){
		if(landLineDto==null){
			landLineDto = new LandLineRechargeDTO();
		}
		landLineDto.setUserDto(userDto);
		landLineDto.setAmount(utility.convertStringToInt(landlineForm.getAmount()));
		landLineDto.setAccountnumber(landlineForm.getCustomeraccountno());
		landLineDto.setLandlinenumber(landlineForm.getLandlinenumber());
		landLineDto.setStdcode(landlineForm.getStdcode());
		landLineDto.setOperatorName(landlineForm.getOperatortext());
		landLineDto.setOperatorValue(landlineForm.getOperator());
		landLineDto.setServiceType(landlineForm.getService());
		landLineDto.setIsSuccesfulRecharge(0);
		landLineDto.setOrderId(orderId);
		landLineDto.setInternetCharges(landlineForm.getExtracharges());
		return landLineDto;
	}
	
	public int calculatePaidAmountRewardsPointCase(String isReedom,UserDTO user,LandLineRechargeDTO landlineDto,int paidAmount){
		paidAmount = landlineDto.getAmount();
		if(null!=isReedom && isReedom.equals(Constants.TRUE)){
	        RewardsPoint rewardsPoint  = rewardPointService.reedomRewardsPoint(user,true);
	        landlineDto.setRedeemRewardsPoints(true);
	        landlineDto.setRedeemRewardsPointsValue(rewardsPoint.getRewardsPoint_value());
	        if(!landlineDto.isUseWallet() && null!=rewardsPoint){
				paidAmount = calculatePaidAmount(paidAmount, rewardsPoint.getRewardsPoint_value());
			}
	    }
		return paidAmount;
	}
	
	/*
	 * create url for jolo api
	 * (i)if response = success
	 *    set coupon value to user account
	 *    add user detail into billing table
	 *  (ii) if failed
	 *    add user detail into billing table 
	 * @see com.chargenmarch.charge.joloapi.service.IBillingService#landLineBilling(com.chargenmarch.charge.joloapi.dto.billing.LandLineRechargeDTO, java.lang.String, com.chargenmarch.common.dto.user.UserDTO)
	 */
	public LandLineRechargeDTO landLineBilling(LandLineRechargeDTO landlineDto,String orderId,UserDTO userDto){
		if(null != userDto && null != orderId && null != landlineDto){
			try {
				landlineDto.setOrderId(orderId);
				landlineDto.setUserDto(userDto);
				landlineDto.setIsSuccesfulRecharge(0);
				//create API url for billing 
				String apiUrl = createAPIUrlForBilling(landlineDto);
				String response = httpService.HTTPGetRequest(apiUrl);
				rechargeDao.saveRechargeLogs(JOLOAPIConstants.JOLOAPI_BILLING, apiUrl, response,landlineDto.getServiceType());
				LOGGER.info("landLineBilling response==>"+response);
				ObjectMapper mapper = new ObjectMapper();
				JOLORechargeResponseDTO joloRechargeResponseDTO = mapper.readValue(response, JOLORechargeResponseDTO.class);
				if(null!=joloRechargeResponseDTO && joloRechargeResponseDTO.getStatus().equalsIgnoreCase(JOLOAPIConstants.SUCCESS)){
					    String rechargeOrderId = joloRechargeResponseDTO.getTxid();
					    landlineDto.setRechargeOrderId(rechargeOrderId);
						landlineDto.setIsSuccesfulRecharge(1);
						saveBillingRechargeDetail(landlineDto);
						generateRechargeCouponCashback(landlineDto);
						int rechargeCount = userDto.getRechargeCount() + 1;
						userDto.setRechargeCount(rechargeCount);
						userService.updateUserRechargeAccount(userDto);
						generateBillingUtilityThread.setBillingDto(landlineDto);
						taskExecutor.execute(generateBillingUtilityThread);
						return landlineDto;
					}else{
						saveBillingRechargeDetail(landlineDto);
					}
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.info("Error in mobile Recharge==>mobileNumber="+landlineDto.getLandlinenumber() + " ==>Amount="+landlineDto.getAmount());
				emailSenderService.sendEmailWithMultipleRecipient(Constants.NOREPLY_EMAIL_ID, RechargeConstants.RECHARGE_ERROR_EMAIL_ID.split(Constants.COMMA),env.getProperty(PropertyKeyConstants.APIProperties.RECHARGE_EXCEPTION_MAILLIST).split(Constants.COMMA), "Error in LandLine Bill Payment==>Number="+landlineDto.getLandlinenumber(), e.getMessage(),null,null, false);
			}
			
		}
		return landlineDto;
	}
		
	/**
	 * Create Recharge API url
	 * 1. Check recharge Service and mobile recharge service(Prepaid and postpaid)
	 *   (i) if mobile recharge service is postapid then create billing recharge API Url
	 *        https://joloapi.com/api/cbill.php?mode=0&userid=9785205444&key=986286939135988&operator=APOS&service=9602646089&amount=100&std=stdcode&ca=&orderid=CAM12345&type=json
	 *        {"status":"SUCCESS","error":"0","txid":"Z267182091873","operator":"APOS","service":"9602646089","amount":"100","orderid":"CAM12345","operatorid":"0","balance":null,"margin":null,"time":"February 24 2016 09:58:09 PM"}
	 * @param landLineDto
	 * @return
	 */
	private String createAPIUrlForBilling(LandLineRechargeDTO landLineDto){
		if(landLineDto.getStdcode() == null ||  landLineDto.getStdcode().equals(Constants.BLANK)){
			landLineDto.setStdcode(Constants.SPACE_ENCODED);
		}
		if(landLineDto.getAccountnumber() == null || landLineDto.getAccountnumber().equals(Constants.BLANK)){
			landLineDto.setAccountnumber(Constants.SPACE_ENCODED);
		}
		String apiUrl = env.getProperty(PropertyKeyConstants.JOLOAPIProperties.BILLING_RECHARGE_API);
		apiUrl += setCommonAPIParameter();
		apiUrl += Constants.OPERATOR + Constants.EQUALS + landLineDto.getOperatorValue() ;
		apiUrl += Constants.AMPERSAND + JOLOAPIConstants.SERVICE + Constants.EQUALS + landLineDto.getLandlinenumber() ; 
		apiUrl += Constants.AMPERSAND + Constants.AMOUNT + Constants.EQUALS + landLineDto.getAmount();
		apiUrl += Constants.AMPERSAND + JOLOAPIConstants.STD + Constants.EQUALS + landLineDto.getStdcode();
		apiUrl += Constants.AMPERSAND + JOLOAPIConstants.CUSTOMER_ACCOUNT_NUMBER + Constants.EQUALS + landLineDto.getAccountnumber();
		apiUrl += Constants.AMPERSAND + Constants.ORDERID + Constants.EQUALS + landLineDto.getOrderId();
		
		return apiUrl;
	}
	
	/**
	 * set common parameter in Biiling recharge API.
	 * @return
	 */
	private String setCommonAPIParameter(){
		String apiUrl = Constants.BLANK;
	    apiUrl += Constants.AMPERSAND + JOLOAPIConstants.USERID + Constants.EQUALS + env.getProperty(PropertyKeyConstants.JOLOAPIProperties.JOLOAPI_USERNAME) + Constants.AMPERSAND;
	    apiUrl += JOLOAPIConstants.KEY + Constants.EQUALS + env.getProperty(PropertyKeyConstants.JOLOAPIProperties.JOLOAPI_KEY) + Constants.AMPERSAND ;
	    return apiUrl;
	}
	
	/**
	 * If Recharge is successful then
	 * Generate recharge coupon cashback.
	 * @param rechargeDto
	 */
	private void generateRechargeCouponCashback(LandLineRechargeDTO landlineDto){
		CouponDTO couponDto = couponService.reedomCoupon(landlineDto.getUserDto(), setValuesInCouponform(landlineDto));
		if(null!=couponDto && couponDto.isCouponApply()){
			double couponReedomValue = couponDto.getCouponReedomValue();
			int reedomValue = (int) Math.round(couponReedomValue);
			landlineDto.setCouponValue(reedomValue);
		}else{
			landlineDto.setCouponName(Constants.BLANK);
		}
	}
	
	/**
	 * Save Billing recharge details
	 */
	public int saveBillingRechargeDetail(LandLineRechargeDTO landlineDTO){
		int rechargeId = billingDao.saveBillingDetails(landlineDTO);
		landlineDTO.setRechargeId(rechargeId);
		return rechargeId;
	}
	
	/**
	 * Set coupon values in form for redeeom coupon
	 */
	public CouponForm setValuesInCouponform(LandLineRechargeDTO landlineDto){
		 CouponForm couponForm = new CouponForm();
		 couponForm.setAmount(landlineDto.getAmount());
		 couponForm.setCouponServiceType(landlineDto.getServiceType());
		 couponForm.setCouponName(landlineDto.getCouponName());
		 couponForm.setIsCouponLogInsert(0);
		 return couponForm;
	}
	
	/**
	 * Iniliaze order info object for PayU Payment gateway.
	 */
	public OrderInfo iniliazeOrderInfo(LandLineRechargeDTO landlineDto,HttpServletRequest request,int paidAmount){
		String userCredentials = landlineDto.getUserDto().getId() + landlineDto.getUserDto().getEmailId();
		OrderInfo orderInfo = utility.initializeOrderInfoForPaymentGateway(landlineDto.getUserDto().getName(), null, landlineDto.getUserDto().getEmailId(),  landlineDto.getUserDto().getMobileNo(), 
	    		null, null, null, null, RequestConstants.BILLING_PAYU, paidAmount, RequestConstants.PAY_U,userCredentials);
	    request.setAttribute(RequestConstants.ORDER_INFO, orderInfo);
	    request.getSession().setAttribute(RequestConstants.LANDLINE_RECHARGE_DTO,landlineDto);
	    request.setAttribute(RequestConstants.URL, utility.getBaseUrl() + RechargeConstants.SLASH + PaymentConstants.PAYMENT.toLowerCase() + RequestConstants.PROCESSED_PAYMENT );
	    return orderInfo;
	}
	
	/**
	 * 
	 * Update user wallet balance and redeem rewards points
	 */
	public void updateUserWalletBalance(LandLineRechargeDTO landlineDto,UserDTO user){
		if(landlineDto.isUseWallet()){
			int calculateWalletBalance = calculatePaidAmount(user.getWalletBalance(),landlineDto.getAmount());
		    user.setWalletBalance(calculateWalletBalance);
		    userService.updateUserWalletBalance(user);
		}
		if(landlineDto.isRedeemRewardsPoints()){
			int calculateWalletBalance = calculatePaidAmount(user.getWalletBalance(),landlineDto.getRedeemRewardsPointsValue());
		    user.setWalletBalance(calculateWalletBalance);
		    userService.updateUserWalletBalance(user);
		}
	}
	
	/**
	 * 
	 * Create landlinedto object
	 * If Lanlineform object in Session then get landlineForm object from session.
	 *  
	 */
	public LandlineForm getLandLineFormObject(HttpServletRequest request){
		LandlineForm landlineForm = null;
		if(request.getParameter(RequestConstants.LANDLINE_NUMBER)==null && null!=request.getSession().getAttribute(SessionConstants.LANDLINE_FORM)){
			landlineForm = (LandlineForm) request.getSession().getAttribute(SessionConstants.LANDLINE_FORM);
		}else{
			landlineForm = new LandlineForm();
			landlineForm.setLandlinenumber(request.getParameter(RequestConstants.LANDLINE_NUMBER));
			landlineForm.setOperator(request.getParameter(RequestConstants.OPERATOR_VALUE));
			landlineForm.setOperatortext(request.getParameter(RequestConstants.OPERATOR_TEXT));
			landlineForm.setService(request.getParameter(RequestConstants.SERVICE));
			
			String amount = request.getParameter(RequestConstants.AMOUNT);
			int rechargeAmount = utility.convertStringToInt(amount);
			int rechargeAmountBasicExtra = addExtraCharges(rechargeAmount);
			landlineForm.setAmount(Integer.toString(rechargeAmountBasicExtra));
			landlineForm.setExtracharges(rechargeAmountBasicExtra - rechargeAmount);
			landlineForm.setStdcode(request.getParameter(RequestConstants.STD_CODE));
			landlineForm.setCustomeraccountno(request.getParameter(RequestConstants.CUSTOMER_ACCOUNT_NO));
		}
		return landlineForm;
	 }	 
	
	
	
	public int addExtraCharges(int rechargeAmount){
		rechargeAmount = rechargeAmount + (rechargeAmount * utility.convertStringToInt(env.getProperty(PropertyKeyConstants.JOLOAPIProperties.EXTRA_CHARGES)) / 100);
		return rechargeAmount;
	}
	
	/**
	 * This key used any recharge time.
	 * @return
	 */
	public String getRechargeSecretKey(){
		return env.getProperty(PropertyKeyConstants.APIProperties.RECHARGE_SECRET_KEY);
	}
	
	public List<RechargeDTO> getRechargeListByPGOrderId(String pgOrderId){
		return billingDao.getRechargeListByPGOrderId(pgOrderId);
	} 
}
