package com.chargenmarch.charge.joloapi.dao.billing;

import java.util.List;

import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.charge.joloapi.dto.billing.LandLineRechargeDTO;

public interface IBillingDao {

	public int saveBillingDetails(LandLineRechargeDTO landlineDto);
	public List<RechargeDTO> getRechargeListByPGOrderId(String pgOrderId);
}
