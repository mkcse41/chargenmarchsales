package com.chargenmarch.charge.joloapi.constants;

/**
 * This class used to get JOLO recharge API response.
 * {"status":"SUCCESS","error":"0","txid":"Z613649229110","operator":"AT","service":"9782091210","amount":"10","orderid":"CAMRECH2010",
 * "operatorid":"0","balance":null,"margin":null,"time":"February 22 2016 07:02:49 PM"}
 * @author mukesh
 *
 */
public class JOLORechargeResponseDTO {
	  private String status;
	  private String error;
	  private String txid;
	  private String operator;
	  private String service;
	  private String amount;
	  private String orderid;
	  private String operatorid;
	  private String balance;
	  private String margin;
	  private String time;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getTxid() {
		return txid;
	}
	public void setTxid(String txid) {
		this.txid = txid;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getOperatorid() {
		return operatorid;
	}
	public void setOperatorid(String operatorid) {
		this.operatorid = operatorid;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getMargin() {
		return margin;
	}
	public void setMargin(String margin) {
		this.margin = margin;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
      
      
}
