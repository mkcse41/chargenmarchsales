package com.chargenmarch.charge.service.feedback.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chargenmarch.charge.dao.feedback.IFeedbackDao;
import com.chargenmarch.charge.dto.feedback.FeedbackDto;
import com.chargenmarch.charge.service.feedback.IFeedbackService;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.service.email.impl.EmailSenderServiceImpl;
import com.chargenmarch.common.utility.IUtility;

@Service
public class FeedbackServiceImpl implements IFeedbackService{
	
	private final static Logger logger = LoggerFactory.getLogger(FeedbackServiceImpl.class);
	
	@Autowired
	IFeedbackDao feedbackDao;
	
	@Autowired
	IUtility utility;

	@Autowired
	EmailSenderServiceImpl emailSenderService;
	
	public int addFeedback(FeedbackDto feedbackDto){
		try{
			int status = feedbackDao.addFeedback(feedbackDto);
			if(status == 1){
			  sendFeedbackMail(feedbackDto);	
			}
			return status;
		}
		catch(Exception e){
			logger.error("Error in submiting user review" + e);
		}
		return 0;
	}
	
	private void sendFeedbackMail(FeedbackDto feedbackDto){
		String subject = "User Feedback Mail !!!";
		String message = "Name : "+feedbackDto.getName() +"\n";
		       message += "Email Id : "+feedbackDto.getEmailId() +"\n";
		       message += "Mobile No. : "+feedbackDto.getMobileNo() +"\n";
		       message += "Feedback : "+feedbackDto.getMessage();
		emailSenderService.sendEmail(Constants.NOREPLY_EMAIL_ID, Constants.FEEDBACK_MAIL_ID.split(Constants.COMMA), subject, message, null, null,false);
	}

}
