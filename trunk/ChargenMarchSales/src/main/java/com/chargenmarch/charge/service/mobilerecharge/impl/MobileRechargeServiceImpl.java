package com.chargenmarch.charge.service.mobilerecharge.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.chargenmarch.charge.constants.recharge.RechargeConstants;
import com.chargenmarch.charge.dao.mobilerecharge.IMobileRechargeDao;
import com.chargenmarch.charge.dto.Pay2AllRechargeResponseDTO;
import com.chargenmarch.charge.dto.coupon.CouponDTO;
import com.chargenmarch.charge.dto.funds.AddFundsRequestDTO;
import com.chargenmarch.charge.dto.mobilerecharge.Operator;
import com.chargenmarch.charge.dto.mobilerecharge.Pay2AllOperatorInfo;
import com.chargenmarch.charge.dto.mobilerecharge.Pay2AllServiceProvider;
import com.chargenmarch.charge.dto.mobilerecharge.Plan;
import com.chargenmarch.charge.dto.mobilerecharge.PlansInJsonArray;
import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.charge.enums.recharge.MobilePlanTypes;
import com.chargenmarch.charge.enums.recharge.OperatorPayCodes;
import com.chargenmarch.charge.enums.recharge.ZoneCodes;
import com.chargenmarch.charge.forms.coupon.CouponForm;
import com.chargenmarch.charge.joloapi.constants.JOLOAPIConstants;
import com.chargenmarch.charge.joloapi.dao.IRechargeDao;
import com.chargenmarch.charge.service.coupon.ICouponService;
import com.chargenmarch.charge.service.mobilerecharge.GenerateRechargeUtilityThread;
import com.chargenmarch.charge.service.mobilerecharge.IMobileRechargeService;
import com.chargenmarch.charge.thread.NumberOperatorMapperThread;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.FilePathConstants;
import com.chargenmarch.common.constants.PropertyKeyConstants;
import com.chargenmarch.common.constants.RequestConstants;
import com.chargenmarch.common.dto.email.EmailDTO;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.service.http.IHTTPService;
import com.chargenmarch.common.service.user.IUserService;
import com.chargenmarch.common.utility.IUtility;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

@Service
@PropertySource(FilePathConstants.CLASSPATH+FilePathConstants.API_PROPERTIES)
public class MobileRechargeServiceImpl implements IMobileRechargeService{
	
	private final static Logger logger = LoggerFactory.getLogger(MobileRechargeServiceImpl.class);
	
	@Autowired
	private Environment env;
	
	@Autowired
	private IHTTPService httpService;
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	ICouponService couponService;
	
	@Autowired
	IUtility utility;
	
	
	@Autowired
	private IMobileRechargeDao mobileDao;
	Map<String, Map<String, List<Plan>>> mobilePlansMap;
	
	@PostConstruct
	public void initialize(){
		getMobilePlansByTypeOperatorAndZone();
	}
	
	@Autowired
	private IEmailSenderService emailSenderService;
	
	@Autowired
	ThreadPoolTaskExecutor taskExecutor;
	 
	@Autowired
	NumberOperatorMapperThread numberOperatorMapperThread;
	
	@Autowired
	GenerateRechargeUtilityThread generateRechargeUtilityThread;
	
	@Autowired
	IRechargeDao rechargeDao;
	
	public Operator getOperatorbyNumber(String mobileNumber){
		Operator operator = null;
		try{
			if(mobileNumber!=null && !StringUtils.isEmpty(mobileNumber)){
				//operator = getOperatorByNumberFromDB(mobileNumber);
				String apiUrl = env.getProperty(PropertyKeyConstants.APIProperties.API_OPERATOR_GET_KEY);
				apiUrl += mobileNumber.trim();
				String response = httpService.HTTPGetRequest(apiUrl);
				if(response!=null && !StringUtils.isEmpty(response)){
						ObjectMapper mapper = new ObjectMapper();
						Pay2AllOperatorInfo pay2AllOperatorInfo = mapper.readValue(response, Pay2AllOperatorInfo.class);
						operator = createOperatorObject(pay2AllOperatorInfo);
						if(operator.getCircle() != null){
							numberOperatorMapperThread.setMobileNumber(mobileNumber);
							numberOperatorMapperThread.setOperator(operator);
							taskExecutor.execute(numberOperatorMapperThread);
						} else {
							//If operator details not correct then set circle and operator blank. 
							operator.setCircle(Constants.BLANK);
							operator.setOperator(Constants.BLANK);
							operator.setPay_code(Constants.BLANK);
						}
						return operator;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error in get operator details==>MobileNumber="+mobileNumber);
		    emailSenderService.sendEmail(Constants.NOREPLY_EMAIL_ID, RechargeConstants.RECHARGE_ERROR_EMAIL_ID.split(Constants.COMMA), "Error in get operator details==>MobileNumber="+mobileNumber, e.getMessage(), null, null, false);
		}
		return operator;
	}
	
	private Operator createOperatorObject(Pay2AllOperatorInfo pay2AllOperatorInfo){
		Operator operator  = new Operator();
		operator.setCircle(ZoneCodes.valueOf(pay2AllOperatorInfo.getPlan_code()).getZoneName());
		operator.setCircle_code(pay2AllOperatorInfo.getCircle_id());
		Pay2AllServiceProvider pay2AllServiceProvider = getPay2AllServiceProviderById(utility.convertStringToInt(pay2AllOperatorInfo.getProvider_id()));
		if(null != pay2AllServiceProvider){
			operator.setOperator(pay2AllServiceProvider.getProvider_name());
			operator.setPay_code(pay2AllServiceProvider.getProvider_code());
			operator.setPay_circle_code(pay2AllOperatorInfo.getCircle_id());
			operator.setCode(pay2AllServiceProvider.getProvider_code());
			operator.setPay2allProviderId(pay2AllOperatorInfo.getProvider_id());
		}
		return operator;
	}
	
	/**
	 * Mobile Recharge
	 * 1. Get operator details by Mobile Number.
	 * 2. operator details pass in recharge api 
	 * 3. get recharge API response.
	 * https://www.pay2all.in/recharge/api?username=919602646089&password=chargenmarch&company=V&number=9783832527&circle=RJ&amount=10&orderid=cam1; 
	 * 13408311#Success##57.013
	 * 13408578#Success#RJ0035291059#37.593
	 */
	public RechargeDTO mobileDthAndDataCardRecharge(RechargeDTO rechargeDto,String orderId,UserDTO userDto){
		Operator operator = createOperatorObject(rechargeDto);
		try{
			if(null!=operator && null!=userDto){
				rechargeDto = setValuesInrechargeDto(rechargeDto.getAmount(), userDto, operator,rechargeDto.getNumber(),orderId,rechargeDto);
				String rechargeAPIUrl = createRechargeAPIUrl(rechargeDto);
				String response = httpService.HTTPGetRequest(rechargeAPIUrl);
				//String response = "failure";//Constants.SUCCESS_CAP;//use for testing
				rechargeDao.saveRechargeLogs(Constants.PAY2ALL, rechargeAPIUrl, response,rechargeDto.getServiceType());
				if(response!=null && !StringUtils.isEmpty(response)){
					ObjectMapper mapper = new ObjectMapper();
					Pay2AllRechargeResponseDTO pay2allRechargeResponseDTO = mapper.readValue(response.trim(), Pay2AllRechargeResponseDTO.class);
					if(null !=pay2allRechargeResponseDTO && pay2allRechargeResponseDTO.getStatus().equalsIgnoreCase(Constants.SUCCESS_CAP)){
						String rechargeOrderId = pay2allRechargeResponseDTO.getPayid();
						//rechargeOrderId = "56897845"; // use for testing
						rechargeDto.setRechargeOrderId(rechargeOrderId);
						rechargeDto.setIsSuccesfulRecharge(1);
						saveMobileRechareDetails(rechargeDto);
						CouponDTO couponDto = couponService.reedomCoupon(userDto, setValuesInCouponform(rechargeDto));
						if(null!=couponDto && couponDto.isCouponApply()){
							double couponReedomValue = couponDto.getCouponReedomValue();
							int reedomValue = (int) Math.round(couponReedomValue);
							rechargeDto.setCouponValue(reedomValue);
						}else{
							rechargeDto.setCouponName(Constants.BLANK);
						}
						int rechargeCount = userDto.getRechargeCount() + 1;
						userDto.setRechargeCount(rechargeCount);
						userService.updateUserRechargeAccount(userDto);
						generateRechargeUtilityThread.setRechargeDto(rechargeDto);
						taskExecutor.execute(generateRechargeUtilityThread);
						return rechargeDto;
					}else{
						rechargeDto.setError(pay2allRechargeResponseDTO.getOperator_ref());
						saveMobileRechareDetails(rechargeDto);
					}
				}
			   return rechargeDto;
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Error in mobile Recharge==>mobileNumber="+rechargeDto.getNumber() + " ==>Amount="+rechargeDto.getAmount());
			emailSenderService.sendEmailWithMultipleRecipient(Constants.NOREPLY_EMAIL_ID, RechargeConstants.RECHARGE_ERROR_EMAIL_ID.split(Constants.COMMA),env.getProperty(PropertyKeyConstants.APIProperties.RECHARGE_EXCEPTION_MAILLIST).split(Constants.COMMA), "Error in mobileDthAndDataCardRecharge==>Number="+rechargeDto.getNumber(), e.getMessage(),null,null, false);
		}
		return null;
	}
	
	/**
	 * This mehod used to DTH,Datacard,mobile Postpaid service
	 * @param rechargeDto
	 */
	public Operator createOperatorObject(RechargeDTO rechargeDto){
		 Operator operator = null;
		 if(null!=rechargeDto.getServiceType() && null!=rechargeDto.getMobileService() && rechargeDto.getServiceType().equalsIgnoreCase(Constants.MOBILE) &&
				 rechargeDto.getMobileService().equalsIgnoreCase(RechargeConstants.PREPAID)){
			     operator = getOperatorbyNumber(rechargeDto.getNumber());
			     if(null!=operator && !operator.getPay_code().equals(rechargeDto.getOperatorValue())){
			    	 operator = setValuesInOperator(operator, rechargeDto);
			     }else if(operator==null){
			    	 operator = setValuesInOperator(operator, rechargeDto);
			     }
		 }else{
			     operator = setValuesInOperator(operator, rechargeDto);
		 }
		return operator;	
	}
	
	private Operator setValuesInOperator(Operator operator,RechargeDTO rechargeDto){
		operator = new Operator();
		 if(null!=rechargeDto.getServiceType() && rechargeDto.getServiceType().equalsIgnoreCase(RequestConstants.DTH)){
			 operator.setCircle(RechargeConstants.RAJASTHAN);
			 operator.setCircle_code(RechargeConstants.RAJASTHAN_CIRCLE_CODE);
		 }else{
			 operator.setCircle(rechargeDto.getCircleName());
			 operator.setCircle_code(rechargeDto.getCircleValue());
		 }
		 operator.setPay_code(rechargeDto.getOperatorValue());
		 operator.setOperator(rechargeDto.getOperatorName());
		 Pay2AllServiceProvider pay2AllServiceProvider = getPay2AllServiceProviderByCode(rechargeDto.getOperatorValue());
		 if(null != pay2AllServiceProvider){
			 operator.setPay2allProviderId(String.valueOf(pay2AllServiceProvider.getId()));
		 }
		 return operator;
	}
	
	/**
	 * https://www.pay2all.in/web-api/paynow?api_token=api_token&number=customer_number&provider_id=prvoider_id&amount=amount&client_id=clientuniqueid&cnumber=cnumber
	 * Create mobile recharge API Url.
	 * @param rechargeDTO
	 * @return
	 */
	private String createRechargeAPIUrl(RechargeDTO rechargeDTO){
		String rechargeAPIUrl = env.getProperty(PropertyKeyConstants.APIProperties.RECHARGE_API_URL);
		rechargeAPIUrl += Constants.AMPERSAND + 
		                  Constants.PROVIDER_ID + Constants.EQUALS + rechargeDTO.getOperator().getPay2allProviderId() + Constants.AMPERSAND + 
		                  Constants.NUMBER + Constants.EQUALS +  rechargeDTO.getNumber() + Constants.AMPERSAND +
		                  Constants.AMOUNT + Constants.EQUALS + rechargeDTO.getAmount() + Constants.AMPERSAND +
		                  Constants.CLIENT_ID + Constants.EQUALS + rechargeDTO.getOrderId() ;
		return rechargeAPIUrl;
	}
	
	public RechargeDTO setValuesInrechargeDto(int amount,UserDTO userDto,Operator operator,String mobileNumber,String orderId,RechargeDTO rechargeDto){
		if(rechargeDto==null){
			rechargeDto = new RechargeDTO();
		}
		rechargeDto.setAmount(amount);
		rechargeDto.setUserDto(userDto);
		rechargeDto.setOperator(operator);
		rechargeDto.setNumber(mobileNumber);
		rechargeDto.setIsSuccesfulRecharge(0);
		rechargeDto.setOrderId(orderId);
		rechargeDto.setRechargeServiceName(Constants.PAY2ALL);
		return rechargeDto;
	}
	
	public int addPostpaidBillExtraCharges(int rechargeAmount){
		rechargeAmount = rechargeAmount + (rechargeAmount * utility.convertStringToInt(env.getProperty(PropertyKeyConstants.APIProperties.POSTAPID_BILLING_EXTRA_CHARGES)) / 100);
		return rechargeAmount;
	}
	
	/**
	 * Save mobile recharge details
	 * @param rechargeDto
	 * @return
	 */
	public int saveMobileRechareDetails(RechargeDTO rechargeDto){
		 //Handle recharge failure Case
		 if(rechargeDto.getOperator()==null){
			 rechargeDto.setOperator(createOperatorObject(rechargeDto));
		 }
         int rechargeId = mobileDao.saveMobileRechargeDetails(rechargeDto);
         rechargeDto.setRechargeId(rechargeId);
         return rechargeId;
	}
	
  /**
  * Update plan details	
  */
  public void updatePlanListForAll(){
		logger.info("Plan Updation Started");
		mobileDao.truncateMobilePlans();
		for(OperatorPayCodes operatorCode : OperatorPayCodes.values()){
			for(ZoneCodes zoneCode : ZoneCodes.values()){
				logger.info("Plan Updation For : ZONE CODE : "+zoneCode+" and OPERATOR CODE : "+operatorCode);
				updatePlanList(operatorCode, zoneCode);
			}
		}
		logger.info("Plan Updation Ended");
	}
   
  /**
   *  
   * @param operatorPayCode
   * @param zoneCode
   */
  private void updatePlanList(OperatorPayCodes operatorPayCode, ZoneCodes zoneCode){
		try{
			if(operatorPayCode!=null && zoneCode!=null){
				String apiUrl = env.getProperty(PropertyKeyConstants.APIProperties.API_OPERATOR_PLANS_GET_KEY);
				apiUrl+=Constants.STATE+Constants.EQUALS+zoneCode.name()+Constants.AMPERSAND+Constants.OPERATOR+Constants.EQUALS+operatorPayCode.name();
				
				String response = httpService.HTTPGetRequest(apiUrl);
				if(response!=null && !StringUtils.isEmpty(response)){
					try{
						JsonParser parser = new JsonParser();
						JsonElement element = parser.parse(response);
						JsonObject obj = element.getAsJsonObject();
						Gson gson = new Gson();
						PlansInJsonArray plans = gson.fromJson(obj.toString(), PlansInJsonArray.class);
						updatePlansListinDB(plans, operatorPayCode, zoneCode);
					}catch(JsonSyntaxException e){
						logger.error("Error in JSON Syntax in Operator Plans List API\nJSON : "+response);
						e.printStackTrace();
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

	private void updatePlansListinDB(PlansInJsonArray plans, OperatorPayCodes operatorPayCode, ZoneCodes zoneCode){
		try{
			JsonArray amount = plans.getAm();
			JsonArray description = plans.getDes();
			JsonArray type = plans.getTp();
			JsonArray validity = plans.getVal();
			
			if(amount!=null){
				for(int i=0; i<amount.size(); i++){
					mobileDao.updatePlansListinDB(operatorPayCode.name(), zoneCode.name(), amount.get(i).toString().replace("\"", Constants.BLANK), validity.get(i).toString().replace("\"", Constants.BLANK), description.get(i).toString().replace("\"", Constants.BLANK), type.get(i).toString().replace("\"", Constants.BLANK));
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Map<String, List<Plan>> getDistinctPlanTypes(){
		List<Plan> planList = mobileDao.getDistinctPlanTypes();
		Map<String, List<Plan>> operatorwisePlanMap = new HashMap<String, List<Plan>>();
		
		Iterator<Plan> planListItr = planList.iterator();
		while(planListItr.hasNext()){
			Plan plan = planListItr.next();
			
			if(!operatorwisePlanMap.containsKey(plan.getOperator().name())){
				ArrayList<Plan> opWiseplanList = new ArrayList<Plan>();
				opWiseplanList.add(plan);
				operatorwisePlanMap.put(plan.getOperator().name(), opWiseplanList);
			} else {
				List<Plan> opWisePlanList = (List<Plan>) operatorwisePlanMap.get(plan.getOperator().name());
				opWisePlanList.add(plan);
				operatorwisePlanMap.put(plan.getOperator().name(), opWisePlanList);
			}
		}
		
		return operatorwisePlanMap;
	}
	
	public void updateMobilePlansLocalTypes(String[] type_local, String[] type/*, String[] operator_code*/){
		if(type.length == type_local.length /*&& operator_code.length==type_local.length*/){
			for(int i=0; i<type.length; i++)
				mobileDao.updateMobilePlansLocalTypes(type_local[i], type[i]/*, operator_code[i] */);
		}
		
		getMobilePlansByTypeOperatorAndZone();
	}
	
	private Map<String, Map<String, List<Plan>>> getMobilePlansByTypeOperatorAndZone(){
		mobilePlansMap = new HashMap<String, Map<String,List<Plan>>>();
		for (OperatorPayCodes operator : OperatorPayCodes.values()) {
			for(ZoneCodes zone : ZoneCodes.values()){
				String key_mobilePlansMap = operator.name()+zone.name();
				Map<String, List<Plan>> typeWisePlansListMap =  new HashMap<String, List<Plan>>();
				for(MobilePlanTypes local_type : MobilePlanTypes.values()){
					String key = local_type.name();
					List<Plan> planList = mobileDao.getMobilePlansByTypeOperatorAndZone(local_type.name(), operator.name(), zone.name());
					typeWisePlansListMap.put(key, planList);
				}
				mobilePlansMap.put(key_mobilePlansMap, typeWisePlansListMap);
			}
		}
		return mobilePlansMap;
	}

	public Map<String, List<Plan>> getMobilePlansMapByOperator(String operatorZoneKey) {
		if(mobilePlansMap.containsKey(operatorZoneKey)){
			return mobilePlansMap.get(operatorZoneKey);
		}
		return null;
	}
	
	public void saveOperatorNumberMapping(String mobileNumber, Operator operator){
		    mobileDao.saveOperatorNumberMapping(mobileNumber, operator);
	}
	
	public Operator getOperatorByNumberFromDB(String mobileNumber){
		return mobileDao.getOperatorByNumber(mobileNumber);
	}
	
	private CouponForm setValuesInCouponform(RechargeDTO rechargeDto){
		 CouponForm couponForm = new CouponForm();
		 couponForm.setAmount(rechargeDto.getAmount());
		 couponForm.setCouponServiceType(rechargeDto.getServiceType());
		 couponForm.setCouponName(rechargeDto.getCouponName());
		 couponForm.setIsCouponLogInsert(0);
		 return couponForm;
	}
	
	public List<RechargeDTO> getRechargeListByChargerId(String chargerId){
		return mobileDao.getRechargeListByChargerId(chargerId);
	}
	
	public void updateRechargeCashBack(String rechargeIds){
		mobileDao.updateRechargeCashBack(rechargeIds);
	}
	
	public List<RechargeDTO> getRechargeCashbackListByChargerId(int chargerId){
		   return mobileDao.getRechargeCashbackListByChargerId(chargerId);
	}
	
	public void insertRechargeCashBack(UserDTO user,String rechargeIds,int cashback){
		mobileDao.insertRechargeCashBack(user, rechargeIds, cashback);
	}
	
	/**
	 * 
	 */
	public void generateRechargeCashBackAndEMail(){
		int cashbackRechargecount = utility.convertStringToInt(env.getProperty(PropertyKeyConstants.APIProperties.CASHBACK_RECHARGE_COUNT));
		try{
			Map<Integer,UserDTO> userMap = userService.getUserInfoMapById();
			Iterator<Map.Entry<Integer,UserDTO>> entries = userMap.entrySet().iterator();
			while (entries.hasNext()) {
			    Map.Entry<Integer,UserDTO> entry = entries.next();
			    int chargersId =  entry.getKey();
			    UserDTO userDto = entry.getValue();
			    if(null!=userDto){
			    	List<RechargeDTO> rechargeList = getRechargeCashbackListByChargerId(chargersId);
			    	if(rechargeList.size()>=cashbackRechargecount){
			    		rechargeCashBack(rechargeList, userDto, cashbackRechargecount);
			    	}else if(rechargeList.size()>=cashbackRechargecount-1){
			    		generateRechargeCashbackMail(rechargeList, userDto);
			    	}
			    }
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void rechargeCashBack(List<RechargeDTO> rechargeList,UserDTO user,int cashbackRechargecount){
		try{
		   int count = 1;
		   String rechargeIds = Constants.BLANK;
		   int totalRechargeAmount = 0;
		   int cashbackPercent = utility.convertStringToInt(env.getProperty(PropertyKeyConstants.APIProperties.CASHBACK_RECHARGE_PERCENT));
		   for (RechargeDTO rechargeDTO : rechargeList) {
			       if(rechargeIds.equals(Constants.BLANK)){
			    	   rechargeIds = rechargeDTO.getId() + Constants.BLANK;
			       }else{
			    	   rechargeIds += Constants.COMMA + rechargeDTO.getId();
			       }
			       totalRechargeAmount += rechargeDTO.getAmount();
				   //Update recharge Count 
				   if(count==cashbackRechargecount){
					   break;
				   }
				   count++;
		   }
		   int cashbackAmount = calculateCashbackAmount(totalRechargeAmount, cashbackPercent);
		   //insert recharge cashback logs
		   insertRechargeCashBack(user, rechargeIds, cashbackAmount);
		   //update recharge cashback deliverid by recharge table id
		   updateRechargeCashBack(rechargeIds);
		   //update user wallet balance
		   user.setWalletBalance(user.getWalletBalance() + cashbackAmount);
		   userService.updateUserWalletBalance(user);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private int calculateCashbackAmount(int totalRechargeAmount,int percent){
		  int cashbackAmount = 0;
		  cashbackAmount = Math.round((totalRechargeAmount * percent)/100);
		  return cashbackAmount;
	}
	
	private void generateRechargeCashbackMail(List<RechargeDTO> rechargeList,UserDTO user){
		   int cashbackPercent = utility.convertStringToInt(env.getProperty(PropertyKeyConstants.APIProperties.CASHBACK_RECHARGE_PERCENT));
		   int totalRechargeAmount = 0;
		   for (RechargeDTO rechargeDTO : rechargeList) {
			     totalRechargeAmount += rechargeDTO.getAmount();
		   }
		   int cashbackAmount = calculateCashbackAmount(totalRechargeAmount, cashbackPercent);
		   EmailDTO email = getCashbackMailObject(user, totalRechargeAmount,cashbackAmount);
		   emailSenderService.sendEmail(email);
	}
	
	private EmailDTO getCashbackMailObject(UserDTO user,int totalRechargeAmount,int cashbackAmount){
		String message = utility.classpathResourceLoader("cashback.html").replace("%RECHARGEAMOUNT%", String.valueOf(totalRechargeAmount)).replace("%CASHBACKAMOUNT%", String.valueOf(cashbackAmount)).replace("%NAME%", user.getName());
		return new EmailDTO("ChargenMarch Cashback Information", user.getEmailId().split(Constants.COMMA), Constants.NOREPLY_EMAIL_ID, null, null, message, null, null);
	}
	
	
	public void generateRechargeCashBackByUser(UserDTO userDto){
			int cashbackRechargecount = utility.convertStringToInt(env.getProperty(PropertyKeyConstants.APIProperties.CASHBACK_RECHARGE_COUNT));
			List<RechargeDTO> rechargeList = getRechargeCashbackListByChargerId(userDto.getId());
	    	if(rechargeList.size()>=cashbackRechargecount){
	    		rechargeCashBack(rechargeList, userDto, cashbackRechargecount);
	    	}else if(rechargeList.size()>=cashbackRechargecount-1){
	    		generateRechargeCashbackMail(rechargeList, userDto);
	    	}
	}

	public Map<String, Map<String, List<Plan>>> getMobilePlansMap() {
		return mobilePlansMap;
	}
	
	/**
	 * This method used to get recharge service Name.
	 * 1. If recharge service is JOLO, JOLO API use in Web.
	 * 2. If recharge service is ALL, JOLO API use in Web/WAP.
	 * 3. If recharge service is null, Pay2all API use in Web/WAP.
	 * @param request
	 * @return
	 */
	public String getRechargeServiceName(HttpServletRequest request){
		String rechargeService = env.getProperty(PropertyKeyConstants.APIProperties.RECHARGE_SERVICE);
		String isMobile = (String)request.getSession().getAttribute(Constants.IS_MOBILE);
		if(null!=rechargeService && !rechargeService.equalsIgnoreCase(Constants.ALL) && 
				 null != isMobile && isMobile.equalsIgnoreCase(Constants.STR_TRUE)){
			 rechargeService = Constants.PAY2ALL;
		}else if(null!=rechargeService && rechargeService.equalsIgnoreCase(Constants.ALL)){
			 rechargeService = Constants.JOLO.toUpperCase();
		}else{
			 rechargeService = Constants.PAY2ALL; 
		}
		return rechargeService;
	}
	
	/**
	 * This key used any recharge time.
	 * @return
	 */
	public String getRechargeSecretKey(){
		return env.getProperty(PropertyKeyConstants.APIProperties.RECHARGE_SECRET_KEY);
	}
	
	
	@Cacheable("pay2allServiceprovider")
	private Pay2AllServiceProvider getPay2AllServiceProviderById(int providerId){
		  return mobileDao.getPay2AllServiceProviderById(providerId);
	}
	
	@Cacheable("pay2allServiceproviderByCode")
	private Pay2AllServiceProvider getPay2AllServiceProviderByCode(String providerCode){
		  return mobileDao.getPay2AllServiceProviderByProviderCode(providerCode);
	}
	
	public List<RechargeDTO> getRechargeListByPGOrderId(String pgOrderId){
		 return mobileDao.getRechargeListByPGOrderId(pgOrderId);
	}
	
	public List<RechargeDTO> getRechargeListAfterDate(String date) {
		return mobileDao.getRechargeListAfterDate(date);
	}
	
	public List<RechargeDTO> getRechargeListBeforeDate(String date) {
		return mobileDao.getRechargeListBeforeDate(date);
	}
	
	public List<AddFundsRequestDTO> getPendingFundsRequest(){
		return mobileDao.getPendingFundsRequest();
	}
	
	public int updateFundsRequestStatus(UserDTO user,String bankRefId){
		return mobileDao.updateFundsRequestStatus(user, bankRefId);
	}
}
