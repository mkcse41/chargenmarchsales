package com.chargenmarch.charge.mapper.recharge;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.charge.dto.mobilerecharge.Pay2AllServiceProvider;
import com.chargenmarch.common.constants.Constants;

public class Pay2AllServiceProviderMapper implements RowMapper<Pay2AllServiceProvider>{

	@Override
	public Pay2AllServiceProvider mapRow(ResultSet rs, int rowNum) throws SQLException {
		Pay2AllServiceProvider pay2AllServiceProvider = new Pay2AllServiceProvider();
		pay2AllServiceProvider.setId(rs.getInt(Constants.PROVIDER_ID));
		pay2AllServiceProvider.setProvider_code(rs.getString(Constants.PROVIDER_CODE));
		pay2AllServiceProvider.setProvider_name(rs.getString(Constants.PROVIDER_NAME));
		pay2AllServiceProvider.setProvider_image(rs.getString(Constants.PROVIDER_IMAGE));
		return pay2AllServiceProvider;
	}

}
