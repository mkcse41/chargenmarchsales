package com.chargenmarch.charge.joloapi.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.charge.joloapi.constants.JOLOAPIConstants;
import com.chargenmarch.charge.joloapi.dto.JOLOCircle;

/**
 * 
 * @author mukesh
 *
 */
public class JOLOCircleCodesMapper implements RowMapper<JOLOCircle>{
    
	public JOLOCircle mapRow(ResultSet rs, int rowNum) throws SQLException {
		JOLOCircle joloCircleCode = new JOLOCircle();
		joloCircleCode.setCircleCode(rs.getString(JOLOAPIConstants.CIRCLE_CODE));
		joloCircleCode.setCircleName(rs.getString(JOLOAPIConstants.CIRCLE_NAME));
		return joloCircleCode;
	}

}
