package com.chargenmarch.charge.joloapi.controller.billing;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.chargenmarch.charge.constants.recharge.RechargeConstants;
import com.chargenmarch.charge.joloapi.dto.billing.LandLineRechargeDTO;
import com.chargenmarch.charge.joloapi.forms.billing.LandlineForm;
import com.chargenmarch.charge.joloapi.service.IBillingService;
import com.chargenmarch.charge.service.rewardspoint.impl.RewardsPointServiceImpl;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.RequestConstants;
import com.chargenmarch.common.constants.SessionConstants;
import com.chargenmarch.common.constants.ViewConstants;
import com.chargenmarch.common.constants.paymentgateway.PaymentConstants;
import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.service.user.IUserService;
import com.chargenmarch.common.utility.IUtility;
/*
 * @author rajat
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
@RequestMapping(value=RequestConstants.BILLING) 
public class BillingController {

	private final static Logger LOGGER = LoggerFactory.getLogger(BillingController.class);
	
	@Autowired
	IBillingService billingService;
	
	@Autowired
	IUtility utility;
	
	@Autowired
	RewardsPointServiceImpl rewardsPointsService;
	
	@Autowired 
	IUserService userService;
	
	@Autowired 
	IEmailSenderService emailSenderService;
	
	/*
	 * Review billPayment for Landline
	 * set value in landline DTO 
	 * (i) Show reedom points details
	 * (ii) mobile plan details
	 */
	@RequestMapping(value = RequestConstants.REVIEW_ORDER_HTM, method={ RequestMethod.GET, RequestMethod.POST })
	public ModelAndView reviewOrder(HttpServletRequest request,HttpServletResponse response) throws IOException{
		request.getSession().removeAttribute(RequestConstants.LANDLINE_RECHARGE_DTO);
		LandlineForm landlineForm = billingService.getLandLineFormObject(request);
		try{
			if(null!=request.getSession().getAttribute(SessionConstants.USER_BEAN) && null!=landlineForm){
					ModelAndView view = new ModelAndView(utility.getviewResolverName(request, ViewConstants.REVIEW_ORDER));
					UserDTO user = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
					int rechargeAmount = utility.convertStringToInt(landlineForm.getAmount());
					int paidAmount = billingService.calculatePaidAmount(rechargeAmount, user.getWalletBalance());
					LandLineRechargeDTO landlineRechargeDto = billingService.setValuesInLandlineRechargeDto(user, null,landlineForm,null);
					request.getSession().setAttribute(RequestConstants.LANDLINE_RECHARGE_DTO, landlineRechargeDto);
					request.getSession().setAttribute(SessionConstants.LANDLINE_FORM,landlineForm);
				    view.addObject(Constants.MOBILE_NO, landlineForm.getLandlinenumber());
				    view.addObject(RequestConstants.STD_CODE, landlineForm.getStdcode());
				    view.addObject(Constants.AMOUNT, landlineForm.getAmount());
				    view.addObject(RequestConstants.EXTRA_AMOUNT,landlineForm.getExtracharges());
				    view.addObject(Constants.PAID_AMOUNT, paidAmount);
				    view.addObject(Constants.OPERATOR, landlineForm.getOperatortext());
				    view.addObject(RequestConstants.SERVICE, landlineForm.getService());
				    view.addObject(RequestConstants.REWARDS_POINTS, rewardsPointsService.getRewardsPointDetails(user));
				    return view;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		response.sendRedirect(utility.getBaseUrl());
		return null;
	}
	
	/** 
	 * confirm landline bill payment order
	 * (i) remove landline form from session
	 * (ii) calculate amount, if user use wallet or reward point
	 * 
	 * 1.)if user use wallet for recharge
	 *   (i) check user have sufficient balance
	 *      if user have than redirect to PAY2ALL 
	 *   (ii) if not than redirect to payment gateway PAYU
	 * 2.)else 
	 *    redirect user to payment gateway PAYU  
	 */
	@RequestMapping(value = RequestConstants.CONFIRM_ORDER, method={ RequestMethod.GET, RequestMethod.POST })
	public ModelAndView confirmOrder(HttpServletRequest request,HttpServletResponse response) throws IOException{
		try{
			int paidAmount = 0;
			if(null!=request.getSession().getAttribute(SessionConstants.USER_BEAN) && null!=request.getSession().getAttribute(RequestConstants.LANDLINE_RECHARGE_DTO) && isValidForRecharge()){
				String isUseWallet = request.getParameter(RequestConstants.IS_USE_WALLET);
				String isReedom = request.getParameter(RequestConstants.IS_REDEEM);
				String couponName = request.getParameter(RequestConstants.COUPON_NAME);
				String serviceType = request.getParameter(RequestConstants.SERVICE_TYPE);
				UserDTO user = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
				request.getSession().removeAttribute(SessionConstants.LANDLINE_FORM);
				request.getSession().removeAttribute(RequestConstants.QUICK_RECHARGE_CHECK_VAL);
				LandLineRechargeDTO landlineRechargeDto = (LandLineRechargeDTO) request.getSession().getAttribute(RequestConstants.LANDLINE_RECHARGE_DTO);
				landlineRechargeDto.setCouponName(couponName);
				landlineRechargeDto.setServiceType(serviceType);
				landlineRechargeDto.setSessionId(request.getSession().getId());
				if(isUseWallet!=null && isUseWallet.equals(Constants.TRUE)){
					landlineRechargeDto.setUseWallet(true);
				}
				//If user redeem your rewards points in Case calculate paid amount for recharge.
				paidAmount = billingService.calculatePaidAmountRewardsPointCase(isReedom, user, landlineRechargeDto, paidAmount);
				// If user use CAM wallet
				if(landlineRechargeDto.isUseWallet()){
					   if(user.getWalletBalance() >= paidAmount){
						    //rechargeDto.setCouponName(Constants.BLANK); //for commented code recharge wallet case
						    // generate order id
						    String orderId = String.valueOf(new Date().getTime()).substring(0,7);
						    landlineRechargeDto.setAmount(landlineRechargeDto.getAmount() - landlineRechargeDto.getInternetCharges());
						    // make recharge for user, using wallet or pay2all
						    landlineRechargeDto = billingService.landLineBilling(landlineRechargeDto,orderId, user);
							request.setAttribute(RequestConstants.RECHARGE_DTO, landlineRechargeDto);
							request.getSession().removeAttribute(RequestConstants.LANDLINE_RECHARGE_DTO);
							if(landlineRechargeDto.getIsSuccesfulRecharge() == 1){
							     //update wallet balance
							     user.setWalletBalance(user.getWalletBalance() - landlineRechargeDto.getAmount());
							     userService.updateUserWalletBalance(user);
							     ModelAndView view = new ModelAndView(utility.getviewResolverName(request, ViewConstants.RECHARGE_SUCCESS));
							     return view;
							}else{
								 request.setAttribute(PaymentConstants.PAYMENT_STATUS,PaymentConstants.PAYMENT+Constants.SUCCESS_CAP.toLowerCase());
								 ModelAndView view = new ModelAndView(utility.getviewResolverName(request, ViewConstants.RECHARGE_FAIURE));
							     return view;
							}
					   }else{
						   paidAmount = paidAmount - user.getWalletBalance();
						   billingService.iniliazeOrderInfo(landlineRechargeDto, request, paidAmount);
						   ModelAndView view = new ModelAndView(ViewConstants.PAY_U_LOWER);
						   return view;
					   }
				}else{
					billingService.iniliazeOrderInfo(landlineRechargeDto, request, paidAmount);
					       ModelAndView view = new ModelAndView(ViewConstants.PAY_U_LOWER);
						   return view;
			    }
		 }
		}catch(Exception e){
			e.printStackTrace();
			LOGGER.error("error in confirm Bill Payment order");
			emailSenderService.sendEmailWithMultipleRecipient(Constants.NOREPLY_EMAIL_ID, RechargeConstants.RECHARGE_ERROR_EMAIL_ID.split(Constants.COMMA), null,"error in confirm Bill payment order", e.getMessage(),null,null, false);
		}
		response.sendRedirect(utility.getBaseUrl());
	    return null;
	}
	
	/**
	 * after processing PAYU call this method
	 * If payment is success
	 * 1.Recharge bill 
	 * (i) if payment success then billing process will done.
	 * 2.Generate rewards points
	 * 3.Save billing details in DB.
	 * @throws IOException 
	 */
	@RequestMapping(value = RequestConstants.LANDLINE_BILL, method={ RequestMethod.GET, RequestMethod.POST })
	public ModelAndView LandLineBillRecharge(HttpServletRequest request,HttpServletResponse httpResponse) throws IOException{
		String response = request.getParameter(RequestConstants.RESPONSE);
		try{
			if(null!=request.getSession().getAttribute(SessionConstants.USER_BEAN) && null!=response 
					&& null!=request.getSession().getAttribute(RequestConstants.LANDLINE_RECHARGE_DTO) &&  utility.isValidatePayment(request)){
				Gson gson = new GsonBuilder().create(); 
				OrderInfo orderInfo = gson.fromJson(response, OrderInfo.class);
				UserDTO user = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
				LandLineRechargeDTO landlineRechargeDto = (LandLineRechargeDTO) request.getSession().getAttribute(RequestConstants.LANDLINE_RECHARGE_DTO);
				String orderId = orderInfo.getTxnId();
				String status = orderInfo.getStatus();
				landlineRechargeDto.setAmount(landlineRechargeDto.getAmount() - landlineRechargeDto.getInternetCharges());
				request.getSession().removeAttribute(SessionConstants.LANDLINE_FORM);
				if(!orderId.equals(Constants.BLANK) && status.equals(Constants.SUCCESS) && billingService.getRechargeListByPGOrderId(orderId).size() <= 0){
					landlineRechargeDto = billingService.landLineBilling(landlineRechargeDto,orderId, user);
					request.setAttribute(RequestConstants.RECHARGE_DTO, landlineRechargeDto);
					request.getSession().removeAttribute(RequestConstants.LANDLINE_RECHARGE_DTO);
					if(landlineRechargeDto.getIsSuccesfulRecharge() == 1){
						//update wallet balance
						billingService.updateUserWalletBalance(landlineRechargeDto, user);
						ModelAndView view = new ModelAndView(utility.getviewResolverName(request, ViewConstants.RECHARGE_SUCCESS));
					    return view;
					}else{
						 landlineRechargeDto = getPaidLandlineRechargeAmount(landlineRechargeDto);
						 int paidAmount = landlineRechargeDto.getAmount();
						 if(landlineRechargeDto.isUseWallet()){
						     paidAmount = billingService.calculatePaidAmount(landlineRechargeDto.getAmount(), user.getWalletBalance());
						 }
						 user.setWalletBalance(user.getWalletBalance() + paidAmount);
					     userService.updateUserWalletBalance(user);
					     request.setAttribute(PaymentConstants.PAYMENT_STATUS, orderInfo.getStatus());
						 ModelAndView view = new ModelAndView(utility.getviewResolverName(request, ViewConstants.RECHARGE_FAIURE));
					     return view;
					}
				}else{
					landlineRechargeDto.setOrderId(orderId);
					 billingService.saveBillingRechargeDetail(landlineRechargeDto);
					 request.setAttribute(RequestConstants.RECHARGE_DTO, landlineRechargeDto);
					 ModelAndView view = new ModelAndView(utility.getviewResolverName(request, ViewConstants.RECHARGE_FAIURE));
				     return view;
				}
			 }
		 }catch(Exception e){
			 e.printStackTrace();
			 LOGGER.error("Error in BillingController==>LANDLINE_BILL method");
			 emailSenderService.sendEmailWithMultipleRecipient(Constants.NOREPLY_EMAIL_ID, RechargeConstants.RECHARGE_ERROR_EMAIL_ID.split(Constants.COMMA), null,"error in LANDLINE_BILL method", e.getMessage(),null,null, false);
		 }
		httpResponse.sendRedirect(utility.getBaseUrl());
		return null;
	}
	
	/**
	 * If any case landline recharge is fail then recharge amount and internet charge amount to added in CAM Wallet,
	 * 
	 * @param rechargeDto
	 * @return
	 */
	private LandLineRechargeDTO getPaidLandlineRechargeAmount(LandLineRechargeDTO landlineRechargeDto){
		landlineRechargeDto.setAmount(landlineRechargeDto.getAmount() + landlineRechargeDto.getInternetCharges());
	    return landlineRechargeDto;
    }
	
	private boolean isValidForRecharge(){
		if(billingService.getRechargeSecretKey().equals(Constants.RECHARGE_SECRET_KEY)){
			return true;
		}
		return false;
	}
	
}
