package com.chargenmarch.charge.dto.mobilerecharge;

import com.google.gson.JsonArray;

public class PlansInJsonArray {
	JsonArray status;
	JsonArray am;
	JsonArray val;
	JsonArray des;
	JsonArray tp;
	
	public JsonArray getStatus() {
		return status;
	}
	public void setStatus(JsonArray status) {
		this.status = status;
	}
	public JsonArray getAm() {
		return am;
	}
	public void setAm(JsonArray am) {
		this.am = am;
	}
	public JsonArray getVal() {
		return val;
	}
	public void setVal(JsonArray val) {
		this.val = val;
	}
	public JsonArray getDes() {
		return des;
	}
	public void setDes(JsonArray des) {
		this.des = des;
	}
	public JsonArray getTp() {
		return tp;
	}
	public void setTp(JsonArray tp) {
		this.tp = tp;
	}
}
