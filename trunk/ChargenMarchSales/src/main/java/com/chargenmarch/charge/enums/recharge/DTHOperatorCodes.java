package com.chargenmarch.charge.enums.recharge;

public enum DTHOperatorCodes {
	ATV("Airtel Digital TV"),
	DTV("Dish TV"),
	BTV("Reliance Digital TV"),
	STV("Sun Direct"),
	TTV("Tata Sky"),
	VTV("Videocon D2H");
	
	private String operatorName;
	
	private DTHOperatorCodes(String operatorName){
		this.operatorName = operatorName;
	}
	public String getOperatorName() {
		return operatorName;
	}

}
