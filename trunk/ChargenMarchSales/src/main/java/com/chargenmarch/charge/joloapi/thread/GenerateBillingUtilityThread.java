package com.chargenmarch.charge.joloapi.thread;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.chargenmarch.charge.dto.coupon.CouponDTO;
import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.charge.forms.coupon.CouponForm;
import com.chargenmarch.charge.joloapi.dto.billing.LandLineRechargeDTO;
import com.chargenmarch.charge.joloapi.service.IBillingService;
import com.chargenmarch.charge.service.coupon.ICouponService;
import com.chargenmarch.charge.service.mobilerecharge.IMobileRechargeService;
import com.chargenmarch.charge.service.rewardspoint.IRewardsPointsService;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.dto.email.EmailDTO;
import com.chargenmarch.common.dto.sms.SMSDTO;
import com.chargenmarch.common.enums.sms.SMSTemplate;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.service.sms.ISMSService;
import com.chargenmarch.common.service.user.IUserService;
import com.chargenmarch.common.utility.IUtility;

@Component
@Scope(Constants.PROTOTYPE)
public class GenerateBillingUtilityThread extends Thread{
	private LandLineRechargeDTO landLineDto;
	
	@Autowired
	IRewardsPointsService rewardsPointService;
	
	@Autowired
	IUserService userService;
	
	@Autowired 
	ICouponService couponService;
	
	@Autowired 
	IBillingService billingService;
	
	@Autowired 
	private ISMSService smsService;
	
	@Autowired
	private IEmailSenderService emailService;
	
	@Autowired
	IMobileRechargeService mobileRechargeService;

	@Autowired
	private IUtility util;
	
	public void run(){

		try{
			rewardsPointService.earnRewardsPoint(landLineDto.getUserDto(), landLineDto.getAmount(),landLineDto.getRechargeId());
			userService.updateUserRechargeAccount(landLineDto.getUserDto());
			if(null!=landLineDto.getCouponName() && !landLineDto.getCouponName().equals(Constants.BLANK)){
				CouponDTO couponDto = couponService.reedomCoupon(landLineDto.getUserDto(), billingService.setValuesInCouponform(landLineDto));
				if(null!=couponDto && couponDto.isCouponApply()){
					double couponReedomValue = couponDto.getCouponReedomValue();
					int reedomValue = (int) Math.round(couponReedomValue);
					landLineDto.getUserDto().setWalletBalance(landLineDto.getUserDto().getWalletBalance() + reedomValue);
					userService.updateUserWalletBalance(landLineDto.getUserDto());
					couponService.updateCouponRedeemLogs(landLineDto.getUserDto(), couponDto, landLineDto.getRechargeId());
				}
			}
			//send recharge message
			SMSDTO smsDto = smsService.getSMSObjectByUserAndSMSTemplate(landLineDto.getUserDto(), SMSTemplate.RECHARGE);
			smsDto.getParameterMap().put(1, landLineDto.getServiceType());
			smsDto.getParameterMap().put(2, String.valueOf(landLineDto.getAmount()));
			smsDto.getParameterMap().put(3, landLineDto.getOrderId());
			smsService.sendSMS(smsDto);
			
			//send invoice
			EmailDTO email = getInvoiceMailObject(landLineDto);
			emailService.sendEmail(email);
			
			//recharge cashback
               /*
                * no recharge cash back service availabe for billing process, will implement
                * this service is currently working only for mobile recharge  
                */
			
		    // mobileRechargeService.generateRechargeCashBackByUser(landLineDto.getUserDto());
		}catch(Exception e){
			e.printStackTrace();
		}
				
	
	}
	
	private EmailDTO getInvoiceMailObject(LandLineRechargeDTO landlineDto){
		String message = util.classpathResourceLoader("invoice.html").replace("%NAME%", landlineDto.getUserDto().getName()).replace("%EMAILID%", landlineDto.getUserDto().getEmailId()).replace("%TYPE%", landlineDto.getServiceType()).replace("%NUMBER%", landlineDto.getStdcode()+"-"+landlineDto.getLandlinenumber()).replace("%AMOUNT%", String.valueOf(landlineDto.getAmount())).replace("%ORDERID%", landlineDto.getOrderId());
		return new EmailDTO("Invoice for "+landlineDto.getServiceType()+" Recharge - Order Id :"+landlineDto.getOrderId(), landlineDto.getUserDto().getEmailId().split(Constants.COMMA), Constants.NOREPLY_EMAIL_ID, null, null, message, null, null);
	}
	
	public void setBillingDto(LandLineRechargeDTO landLineDto) {
		this.landLineDto = landLineDto;
	}

}
