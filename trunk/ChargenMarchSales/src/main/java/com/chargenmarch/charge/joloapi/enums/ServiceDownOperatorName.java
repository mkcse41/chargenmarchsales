package com.chargenmarch.charge.joloapi.enums;

import com.chargenmarch.common.constants.Constants;

/**
 * 
 * @author mukesh
 *
 */
public enum ServiceDownOperatorName {
	A("AT"),
	V("VF"),
	AIR("AIRCEL"),
	BS("BSS"),
	BT("BS"),
	I("IDX"),
	MTR("MTD"),
	M("MS"),
	RC("RL"),
	RG("RG"),
	T24("T24"),
	D("TD"),
	DS("TDS"),
	T("TI"),
	TW("TW"),
	UN("UN"),
	VD("VD"),
    VS("VDS"),
    VC("VC"),
    VG("VG");
	
	private String opName;       

    private ServiceDownOperatorName(String s) {
    	opName = s;
    }
    
    public String getOperatorName(){
    	return opName;
    }
    
    public static String getEnumByString(String code){
    	String name = Constants.BLANK;
        for(ServiceDownOperatorName e : ServiceDownOperatorName.values()){
            if(code.equalsIgnoreCase(e.opName)) {
            	name = e.name();
            }
        }
        return name;
    }
}
