package com.chargenmarch.charge.service.coupon;

import com.chargenmarch.charge.dto.coupon.CouponDTO;
import com.chargenmarch.charge.forms.coupon.CouponForm;
import com.chargenmarch.common.dto.user.UserDTO;

/**
 * 
 * @author mukesh
 *
 */
public interface ICouponService {

	public CouponDTO getCouponDetailByCouponName(CouponForm couponForm);
	public CouponDTO insertCouponDetails(CouponForm couponForm);
	public CouponDTO reedomCoupon(UserDTO userDto,CouponForm couponForm);
	public void updateCouponRedeemLogs(UserDTO userDto,CouponDTO couponDto, int rechargeId );
}
