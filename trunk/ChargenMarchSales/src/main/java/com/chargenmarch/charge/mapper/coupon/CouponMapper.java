/**
 * 
 */
package com.chargenmarch.charge.mapper.coupon;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.charge.constants.coupon.CouponConstants;
import com.chargenmarch.charge.dto.coupon.CouponDTO;
import com.chargenmarch.common.constants.Constants;

/**
 * @author mukesh
 *
 */
public class CouponMapper implements RowMapper{
    
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		CouponDTO couponDto = new CouponDTO();
		couponDto.setId(rs.getInt(Constants.ID));
		couponDto.setCouponName(rs.getString(CouponConstants.COUPON_NAME));
		couponDto.setCouponType(rs.getString(CouponConstants.COUPON_TYPE));
		couponDto.setCouponValue(rs.getDouble(CouponConstants.COUPON_VALUE));
		couponDto.setCouponUses(rs.getInt(CouponConstants.COUPON_USES));
		couponDto.setCouponLimit(rs.getInt(CouponConstants.COUPON_LIMIT));
		couponDto.setMinimumRechargeValue(rs.getInt(CouponConstants.MINIMUM_RECHARGE_VALUE));
		couponDto.setCouponServiceType(rs.getString(CouponConstants.COUPON_SERVICE_TYPE));
		couponDto.setCouponStatus(rs.getInt(CouponConstants.COUPON_STATUS));
		couponDto.setStartDate(rs.getDate(CouponConstants.START_DATE));
		couponDto.setEndDate(rs.getDate(CouponConstants.END_DATE));
		couponDto.setCouponPerUserLimit(rs.getInt(CouponConstants.COUPON_PER_USER_LIMIT));
		couponDto.setMaximumcashback(rs.getInt(CouponConstants.MAXIMUM_CASHBACK));
		return couponDto;
		
	}

}
