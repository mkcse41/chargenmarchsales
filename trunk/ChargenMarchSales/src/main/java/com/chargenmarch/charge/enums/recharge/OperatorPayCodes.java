package com.chargenmarch.charge.enums.recharge;

public enum OperatorPayCodes {
	AC("AIRCEL"),
	AT("Bharti Airtel"),
	CG("BSNL  CellOne GSM"),
	DP("MTNL  DOLPHIN"),
	ID("IDEA"),
	MT("MTS India"),
	RC("Reliance Mobile CDMA"),
	RG("Reliance Mobile GSM"),
	TD("TATA DOCOMO"),
	UN("Uninor"),
	VF("Vodafone India"),
	VD("Videocon Mobile Services");
	
	private String operatorPayCode;       

    private OperatorPayCodes(String s) {
    	operatorPayCode = s;
    }
    
    public String getOperatorPayCode(){
    	return operatorPayCode;
    }
    
    public String getName() {
        return name();
    }
}
