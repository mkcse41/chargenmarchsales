package com.chargenmarch.charge.dto.mobilerecharge;

import org.joda.time.DateTime;

import com.chargenmarch.common.dto.user.UserDTO;

public class RechargeDTO {

	private int id;
	private Operator operator;
	private UserDTO userDto;
	private String number;
	private int amount;
	private int isSuccesfulRecharge;
	private int rechargeId;
	private String orderId;
	private String rechargeOrderId;
	private String couponName;
	private String serviceType;
	private String operatorValue;
	private String mobileService; // like as prepaid and postpaid.
	private String operatorName;
	private int couponValue;
	private boolean useWallet;
	private DateTime rechargeDate;
	private String circleName;
	private String circleValue;
	private boolean isRedeemRewardsPoints=false;
	private int redeemRewardsPointsValue;
	private int internetCharges;
	private int isQuickRecharge=0;
	private String error;
	private String rechargeSource;
	private String rechargeServiceName;
	private boolean useReferral;
	private String sessionId;
	
	public String getRechargeServiceName() {
		return rechargeServiceName;
	}
	public void setRechargeServiceName(String rechargeServiceName) {
		this.rechargeServiceName = rechargeServiceName;
	}
	public int getIsQuickRecharge() {
		return isQuickRecharge;
	}
	public void setIsQuickRecharge(int isQuickRecharge) {
		this.isQuickRecharge = isQuickRecharge;
	}
	public Operator getOperator() {
		return operator;
	}
	public void setOperator(Operator operator) {
		this.operator = operator;
	}
	public UserDTO getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDTO userDto) {
		this.userDto = userDto;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getIsSuccesfulRecharge() {
		return isSuccesfulRecharge;
	}
	public void setIsSuccesfulRecharge(int isSuccesfulRecharge) {
		this.isSuccesfulRecharge = isSuccesfulRecharge;
	}
	public int getRechargeId() {
		return rechargeId;
	}
	public void setRechargeId(int rechargeId) {
		this.rechargeId = rechargeId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getRechargeOrderId() {
		return rechargeOrderId;
	}
	public void setRechargeOrderId(String rechargeOrderId) {
		this.rechargeOrderId = rechargeOrderId;
	}
	public String getCouponName() {
		return couponName;
	}
	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getOperatorValue() {
		return operatorValue;
	}
	public void setOperatorValue(String operatorValue) {
		this.operatorValue = operatorValue;
	}
	public String getMobileService() {
		return mobileService;
	}
	public void setMobileService(String mobileService) {
		this.mobileService = mobileService;
	}
	public String getOperatorName() {
		return operatorName;
	}
	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public int getCouponValue() {
		return couponValue;
	}
	public void setCouponValue(int couponValue) {
		this.couponValue = couponValue;
	}
	public boolean isUseWallet() {
		return useWallet;
	}
	public void setUseWallet(boolean isUseWallet) {
		this.useWallet = isUseWallet;
	}
	public DateTime getRechargeDate() {
		return rechargeDate;
	}
	public void setRechargeDate(DateTime rechargeDate) {
		this.rechargeDate = rechargeDate;
	}
	public String getCircleName() {
		return circleName;
	}
	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}
	public String getCircleValue() {
		return circleValue;
	}
	public void setCircleValue(String circleValue) {
		this.circleValue = circleValue;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isRedeemRewardsPoints() {
		return isRedeemRewardsPoints;
	}
	public void setRedeemRewardsPoints(boolean isRedeemRewardsPoints) {
		this.isRedeemRewardsPoints = isRedeemRewardsPoints;
	}
	public int getRedeemRewardsPointsValue() {
		return redeemRewardsPointsValue;
	}
	public void setRedeemRewardsPointsValue(int redeemRewardsPointsValue) {
		this.redeemRewardsPointsValue = redeemRewardsPointsValue;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public int getInternetCharges() {
		return internetCharges;
	}
	public void setInternetCharges(int internetCharges) {
		this.internetCharges = internetCharges;
	}
	public String getRechargeSource() {
		return rechargeSource;
	}
	public void setRechargeSource(String rechargeSource) {
		this.rechargeSource = rechargeSource;
	}
	public boolean isUseReferral() {
		return useReferral;
	}
	public void setUseReferral(boolean isUseReferral) {
		this.useReferral = isUseReferral;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

}
