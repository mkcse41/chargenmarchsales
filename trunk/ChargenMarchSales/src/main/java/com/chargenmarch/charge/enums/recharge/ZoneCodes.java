package com.chargenmarch.charge.enums.recharge;

import java.util.HashMap;
import java.util.Map;

public enum ZoneCodes {
	AP ("Andhra Pradesh"),
	AS ("Assam"),
	BR ("Bihar and Jharkhand"),
	CH ("Chennai"),
	DL ("Delhi Metro"),
	GJ ("Gujarat"),
	HP ("Himachal Pradesh"),
	HR ("Haryana"),
	JK ("Jammu and Kashmir"),
	KL ("Kerala"),
	KA ("Karnataka"),
	KO ("Kolkata Metro"),
	MH ("Maharashtra"),
	MP ("Madhya Pradesh and Chhattisgarh"),
	MU ("Mumbai Metro"),
	NE ("North East India"),
	OR ("Odisha"),
	PB ("Punjab"),
	RJ ("Rajasthan"),
	TN ("Tamil Nadu"),
	UE ("Uttar Pradesh (East)"),
	UW ("Uttar Pradesh (West) and Uttarakhand"),
	WB("West Bengal");
	
	// Reverse-lookup map for getting a state from an abbreviation
    private static final Map<String, ZoneCodes> lookup = new HashMap<String, ZoneCodes>();
    
	  static {
	        for (ZoneCodes d : ZoneCodes.values()) {
	            lookup.put(d.getZoneName(), d);
	        }
	    }
	  
	private String zoneName;       

    private ZoneCodes(String s) {
    	zoneName = s;
    }
    
    public String getZoneName(){
    	return zoneName;
    }
    
    public static ZoneCodes get(String zoneName) {
        return lookup.get(zoneName);
    }
}
