package com.chargenmarch.charge.dto.mobilerecharge;

import java.util.Date;

import com.chargenmarch.charge.enums.recharge.OperatorPayCodes;
import com.chargenmarch.charge.enums.recharge.ZoneCodes;

public class Plan {

	int id;
	OperatorPayCodes operator;
	ZoneCodes zone;
	int amount;
	String validity;
	String description;
	String type;
	String type_local;
	Date insertDate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public OperatorPayCodes getOperator() {
		return operator;
	}
	public void setOperator(OperatorPayCodes operator) {
		this.operator = operator;
	}
	public ZoneCodes getZone() {
		return zone;
	}
	public void setZone(ZoneCodes zone) {
		this.zone = zone;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getValidity() {
		return validity;
	}
	public void setValidity(String validity) {
		this.validity = validity;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}
	public String getType_local() {
		return type_local;
	}
	public void setType_local(String type_local) {
		this.type_local = type_local;
	}
	
}
