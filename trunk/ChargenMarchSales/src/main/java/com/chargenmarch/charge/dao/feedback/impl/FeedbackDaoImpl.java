package com.chargenmarch.charge.dao.feedback.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.chargenmarch.charge.dao.feedback.IFeedbackDao;
import com.chargenmarch.charge.dto.feedback.FeedbackDto;
import com.chargenmarch.charge.query.feedback.FeedbackQueries;

@Repository
public class FeedbackDaoImpl implements IFeedbackDao {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public int addFeedback(FeedbackDto feedbackDto) {
		Object[] args = {feedbackDto.getName(), feedbackDto.getEmailId(), feedbackDto.getMobileNo(),feedbackDto.getMessage()};
		return jdbcTemplate.update(FeedbackQueries.INSERT_INTO_USER_FEEDBACK, args);
	}

}
