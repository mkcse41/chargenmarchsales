package com.chargenmarch.charge.dto.mobilerecharge;

public class Operator {
	String operator;
	String pay_code;
	String code;
	String circle;
	String pay_circle_code;
	String circle_code;
	private String pay2allProviderId;//this field used to new pay2all account.
	
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getPay_code() {
		return pay_code;
	}
	public void setPay_code(String pay_code) {
		this.pay_code = pay_code;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public String getPay_circle_code() {
		return pay_circle_code;
	}
	public void setPay_circle_code(String pay_circle_code) {
		this.pay_circle_code = pay_circle_code;
	}
	public String getCircle_code() {
		return circle_code;
	}
	public void setCircle_code(String circle_code) {
		this.circle_code = circle_code;
	}
	public String getPay2allProviderId() {
		return pay2allProviderId;
	}
	public void setPay2allProviderId(String pay2allProviderId) {
		this.pay2allProviderId = pay2allProviderId;
	}
	
	
	
}
