/**
 * 
 */
package com.chargenmarch.charge.dao.coupon;

import java.util.List;

import com.chargenmarch.charge.dto.coupon.CouponDTO;
import com.chargenmarch.common.dto.user.UserDTO;

/**
 * @author mukesh
 *
 */
public interface ICouponDao {

	public List<CouponDTO> getCouponDetail();
	public void insertCouponDetail(CouponDTO couponDTO);
	public List<CouponDTO> getCouponDetailByCouponName(String couponName,String couponType);
	public List getCouponUsedCountByUser(UserDTO userDto,CouponDTO couponDto);
	public void insertReedomCouponLogs(CouponDTO couponDTO,UserDTO userDto,int recahrgeId);
	public void updateCouponRedeemLogs(UserDTO userDto,CouponDTO couponDto, int rechargeId );
}
