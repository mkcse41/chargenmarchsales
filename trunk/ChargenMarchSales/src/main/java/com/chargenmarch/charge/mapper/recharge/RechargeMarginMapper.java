package com.chargenmarch.charge.mapper.recharge;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.chargenmarch.charge.dto.funds.RechargeMarginDTO;


@Component
@Scope("prototype")
public class RechargeMarginMapper implements RowMapper<RechargeMarginDTO> {

	@Override
	public RechargeMarginDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		RechargeMarginDTO recharge = new RechargeMarginDTO();
        recharge.setCharger_id(rs.getInt("charger_id"));
		recharge.setTotalRechargeAmount(rs.getInt("totalRechargeAmount"));
		recharge.setMarginAmount(rs.getInt("marginAmount"));
		recharge.setMargindate(rs.getString("margindate"));
		return recharge;
	}
}
