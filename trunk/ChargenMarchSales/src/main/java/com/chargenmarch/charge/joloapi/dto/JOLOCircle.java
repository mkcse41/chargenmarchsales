package com.chargenmarch.charge.joloapi.dto;

/**

 * @author mukesh
 *
 */
public class JOLOCircle {

	private String circleName;
	private String circleCode;
	public String getCircleName() {
		return circleName;
	}
	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}
	public String getCircleCode() {
		return circleCode;
	}
	public void setCircleCode(String circleCode) {
		this.circleCode = circleCode;
	}
	@Override
	public String toString() {
		return "JOLOCircle [circleName=" + circleName + ", circleCode=" + circleCode + "]";
	}
	
}
