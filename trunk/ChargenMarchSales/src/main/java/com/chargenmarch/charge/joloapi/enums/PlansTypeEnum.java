package com.chargenmarch.charge.joloapi.enums;

public enum PlansTypeEnum {
	TUP("TUP"),
	FTT("FTT"),
	TWOG("2G"), 
	THREEG("3G"), 
	SMS("SMS"),
	LSC("LSC"),
	OTR("OTR"),
	RMG("RMG");

	String planType;
	
	private PlansTypeEnum(String planType) {
		this.planType = planType;
	}

	public String getPlanType() {
		return planType;
	}
}
