package com.chargenmarch.charge.dto.rewardspoint;

import com.chargenmarch.common.dto.user.UserDTO;

/**
 * 
 * @author mukesh
 *
 */
public class RewardsPoint {

	private UserDTO userDto;
	private int chargers_id;
	private int rewardsPoint;
	private int rewards_point_id;
	private int recharge_amount;
	private int rewardsPoint_value;
	private int earnReswardsPoint;
	private int rechargeId;
	
	public UserDTO getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDTO userDto) {
		this.userDto = userDto;
	}
	public int getRewardsPoint() {
		return rewardsPoint;
	}
	public void setRewardsPoint(int rewardsPoint) {
		this.rewardsPoint = rewardsPoint;
	}
	public int getRewards_point_id() {
		return rewards_point_id;
	}
	public void setRewards_point_id(int rewards_point_id) {
		this.rewards_point_id = rewards_point_id;
	}
	public int getRecharge_amount() {
		return recharge_amount;
	}
	public void setRecharge_amount(int recharge_amount) {
		this.recharge_amount = recharge_amount;
	}
	
	public int getRewardsPoint_value() {
		return rewardsPoint_value;
	}
	public void setRewardsPoint_value(int rewardsPoint_value) {
		this.rewardsPoint_value = rewardsPoint_value;
	}
	public int getChargers_id() {
		return chargers_id;
	}
	public void setChargers_id(int chargers_id) {
		this.chargers_id = chargers_id;
	}
	public int getEarnReswardsPoint() {
		return earnReswardsPoint;
	}
	public void setEarnReswardsPoint(int earnReswardsPoint) {
		this.earnReswardsPoint = earnReswardsPoint;
	}
	public int getRechargeId() {
		return rechargeId;
	}
	public void setRechargeId(int rechargeId) {
		this.rechargeId = rechargeId;
	}
	
	
	
	
}
