package com.chargenmarch.charge.query.rewardspoint;

/**
 * 
 * @author mukesh
 *
 */
public class RewardsPointQuery {

	public static final String INSERT_REWARDS_POINT = "insert into rewards_point set chargers_id=?,rewardsPoint=?,inserttimestamp=now()";
	public static final String INSERT_REWARDS_POINT_LOGS = "insert into rewards_point_logs set rewards_point_id=?,recharge_amount=?,rewardsPoint=?,rechargeId=?,inserttimestamp=now()";
	public static final String INSERT_REEDOM_REWARDS_POINT_LOGS = "insert into reedom_rewards_point_logs set chargers_id=?,rewardsPoint=?,rewardsPoint_value=?,inserttimestamp=now()";
	public static final String UPDATE_REWARDS_POINT = "update rewards_point set rewardsPoint=?,updatetimestamp=now() where chargers_id=?";
	public static final String SELECT_REWARDS_POINT = "select * from rewards_point";
	public static final String SELECT_REWARDS_POINT_BY_ID = "select * from rewards_point where chargers_id=?";


}
