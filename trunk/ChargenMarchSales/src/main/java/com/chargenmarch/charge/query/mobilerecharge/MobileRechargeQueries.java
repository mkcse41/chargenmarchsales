package com.chargenmarch.charge.query.mobilerecharge;

public class MobileRechargeQueries {
 public static final String INSERT_MOBILE_PLAN_DETAILS = "insert into mobile_plans values(null,?,?,?,?,?,?,now(),now())";
 public static final String TRUNCATE_MOBILE_PLAN_DETAILS = "truncate mobile_plans";
 public static final String INSERT_MOBILE_RECHARGE_DETAILS = " insert into recharge set chargers_id=?, mobilenumber=?, operator=?, rechargeAmount=?,isSuccessfullRecharge=?,rechargeOrderId=?,PGOrderId=?,rechargeService=?,inserttimestamp=now(),updatetimestamp=now(),isquickrecharge=?,extracharges=?,rechargesource=?,rechargeServiceName=?,sessionid=?,isSalesUser=?,mobileServiceType=?";

 public static final String SELECT_DISTINCT_RECHARGE_TYPES = "select distinct(type), operator_code, type_local from mobile_plans order by operator_code";
 public static final String UPDATE_MOBILE_PLANS_LOCAL_TYPE = "update mobile_plans set type_local=?,updatedate = now() where type=?";
 public static final String SELECT_MOBILE_PLANS_BY_TYPE_AND_ZONE_AND_OPERATOR = "select * from mobile_plans where type=? and operator_code=? and zone_code=? order by ABS(amount) asc";
 public static final String INSERT_INTO_NUMBER_OPERATOR_MAPPING = "insert into number_operator_mapping values(null,?,?,?,?,?,?,?, now(), now())";
 public static final String SELECT_OPERATOR_BY_NUMBER = "select * from number_operator_mapping where mobilenumber=?";
 public static final String SELECT_RECHARGE_DETAILS_BY_CHARGERID = "select * from recharge where chargers_id=? order by inserttimestamp desc";
 public static final String SELECT_RECHARGE_CASHBACK_BY_CHARGERID = "select * from recharge where chargers_id=? and isSuccessfullRecharge=1 and rechargeService in ('Mobile','DTH','Datacard') and cashBackDelivered=0 order by inserttimestamp desc";
 public static final String INSERT_RECHARGE_CASHBACK = " insert into recharge_cashback set chargers_id=?,rechargeIds=?,cashback=?,insert_time=now(),update_time=now()";
 public static final String UPDATE_RECHARGE_CASHBACK_DETAILS = " update recharge set cashBackDelivered=1,updatetimestamp=now() where id in (%IDS%)";
 public static final String PAY2ALL_SERVICE_PROVIDERS = "select * from pay2allserviceprovider where provider_id=?";
 public static final String PAY2ALL_SERVICE_PROVIDERS_BY_CODE = "select * from pay2allserviceprovider where provider_code=?";
 public static final String SELECT_RECHARGE_DETAILS_BY_DATE = "select * from recharge where inserttimestamp like ?";
 public static final String SELECT_RECHARGE_DETAILS_BY_PGORDERID = "select * from recharge where PGOrderId=?";
 public static final String SELECT_RECHARGE_AFTER_DATE = "select * from recharge where inserttimestamp > ?";
 public static final String SELECT_RECHARGE_BEFORE_DATE = "select * from recharge where inserttimestamp < ?";
}
