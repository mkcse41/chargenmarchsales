package com.chargenmarch.charge.joloapi.dao.billing.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.WebApplicationContext;

import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.charge.joloapi.dao.billing.IBillingDao;
import com.chargenmarch.charge.joloapi.dto.billing.LandLineRechargeDTO;
import com.chargenmarch.charge.joloapi.query.billing.BillingQuery;
import com.chargenmarch.charge.mapper.recharge.RechargeMapper;
import com.chargenmarch.charge.query.mobilerecharge.MobileRechargeQueries;

@Repository
public class BillingDaoImpl implements IBillingDao{

	@Autowired
	JdbcTemplate jdbcTemplate;
	@Autowired
	WebApplicationContext context;
	
	@Override
	public int saveBillingDetails(final LandLineRechargeDTO landlineDto) {
		try{
			KeyHolder holder = new GeneratedKeyHolder();
			jdbcTemplate.update(new PreparedStatementCreator() {           
		           @Override
		           public PreparedStatement createPreparedStatement(Connection connection)
		                        throws SQLException {
		                    PreparedStatement ps = connection.prepareStatement(BillingQuery.INSERT_BILING_DETAILS, Statement.RETURN_GENERATED_KEYS);
		                    ps.setInt(1, landlineDto.getUserDto().getId());
		                    ps.setString(2, landlineDto.getLandlinenumber());
		                    ps.setString(3, landlineDto.getStdcode());
		                    ps.setString(4, landlineDto.getAccountnumber());
		                    ps.setString(5, landlineDto.getOperatorName());
		                    ps.setInt(6, landlineDto.getAmount());
		                    ps.setInt(7, landlineDto.getInternetCharges());
		                    ps.setString(8, landlineDto.getRechargeOrderId());
		                    ps.setString(9, landlineDto.getOrderId());
		                    ps.setString(10, landlineDto.getServiceType());
		                    ps.setInt(11, landlineDto.getIsSuccesfulRecharge());
		                    ps.setInt(12, landlineDto.getIsQuickRecharge());
		                    ps.setString(13, landlineDto.getSessionId());
		                    return ps;
		                }
		            }, holder);
			
			return holder.getKey().intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public List<RechargeDTO> getRechargeListByPGOrderId(String pgOrderId){
		Object args[] = {pgOrderId};
		return jdbcTemplate.query(BillingQuery.SELECT_RECHARGE_DETAILS_BY_PGORDERID, args, context.getBean(RechargeMapper.class));
	}
}
