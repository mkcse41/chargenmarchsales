package com.chargenmarch.charge.joloapi.query.billing;

public class BillingQuery {
	public static final String INSERT_BILING_DETAILS = " insert into billing set chargers_id=?, billlingno=?,stdcode=?,billingaccountno=?, operator=?, billingamount=?,extracharges=?,rechargeorderid=?,PGOrderId=?,inserttimestamp=now(),updatetimestamp=now(),billingservice=?,issuccessfullrecharge=?,isquickrecharge=?,sessionid=?";
	public static final String SELECT_RECHARGE_DETAILS_BY_PGORDERID = "select * from billing where PGOrderid=?";
}
