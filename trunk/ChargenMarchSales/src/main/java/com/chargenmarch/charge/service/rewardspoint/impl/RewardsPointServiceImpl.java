package com.chargenmarch.charge.service.rewardspoint.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.chargenmarch.charge.dao.rewardspoint.IRewardsPointDao;
import com.chargenmarch.charge.dto.rewardspoint.RewardsPoint;
import com.chargenmarch.charge.service.rewardspoint.IRewardsPointsService;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.FilePathConstants;
import com.chargenmarch.common.constants.PropertyKeyConstants;
import com.chargenmarch.common.constants.paymentgateway.PaymentConstants;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.service.user.IUserService;
import com.chargenmarch.common.utility.IUtility;

/**
 * This service used to manage rewards points, earn and reedom points, maintains rewards_point and rewards_point_log table.
 * @author mukesh
 *
 */
@Service
@PropertySource(FilePathConstants.CLASSPATH+FilePathConstants.REWARDS_POINT_PROPERTIES)
public class RewardsPointServiceImpl implements IRewardsPointsService {

	private final static Logger logger = LoggerFactory.getLogger(RewardsPointServiceImpl.class);
	private Map<Integer,RewardsPoint> rewardsPointsDetailMap = null;
	
	@Autowired
	private Environment env;
	
	@Autowired
	private IUtility utility;
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private IRewardsPointDao rewardsPointDao;
	
	@Autowired
	private IEmailSenderService emailSenderService;
	
	@PostConstruct
	public void init(){
		logger.info("Rewards Points Service inilization");
		rewardsPointsDetailMap = new HashMap<Integer, RewardsPoint>();
		loadRewardsPointDetails();
	}
	
	/**
	 * Load all rewards point details
	 */
	private void loadRewardsPointDetails(){
		List<RewardsPoint> rewardsPointList = rewardsPointDao.getRewardsPoint();
		for (RewardsPoint rewardsPoint : rewardsPointList) {
			 if(null!=rewardsPoint){
				 UserDTO userDto = userService.getUserById(rewardsPoint.getChargers_id());
				 rewardsPoint.setUserDto(userDto);
				 rewardsPoint.setChargers_id(userDto.getId());
				 rewardsPointsDetailMap.put(userDto.getId(), rewardsPoint);
			 }
		}
	}
	
	/**
	 * Get rewards point details per user.
	 */
	public RewardsPoint getRewardsPointDetails(UserDTO userDto){
		try{
			RewardsPoint rewardsPoint = getRewardsPointObject(userDto);
			return rewardsPoint;
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error in earn rewards point=>"+e);
			emailSenderService.sendEmail(Constants.NOREPLY_EMAIL_ID, PaymentConstants.PAYMENT_ERROR_EMAIL_ID.split(Constants.COMMA), "RewardsPointServiceImpl==> Error in get Rewards Point Details", e.getMessage(),null,null, false);
		}
		return null;	
	}
	
	/**
	 * Earn Rewards points per mobile recharge.
	 * If you have done 10 rupees mobile rechage,earn 1 rewards points. 
	 * @param userDto
	 * @param amount
	 */
	public void earnRewardsPoint(UserDTO userDto, int amount,int rechargeId){
		try{
			int rewardsPointBaseValue = utility.convertStringToInt(env.getProperty(PropertyKeyConstants.RewardPointProperties.REWARDSPOINT_BASE_VALUE));
			if(null!=userDto && amount>0){
				if(rewardsPointBaseValue!=0){
					int rewardsPoints = amount/rewardsPointBaseValue;
					rewardsPoints = Math.round(rewardsPoints);
					RewardsPoint rewardsPoint = setRewardsPointDetails(rewardsPoints, userDto,amount);
					int rewardsPointId = rewardsPointDao.insertRewardsPoint(rewardsPoint);
					rewardsPoint.setRewards_point_id(rewardsPointId);
					rewardsPoint.setRechargeId(rechargeId);
					rewardsPointDao.insertRewardsPointLogs(rewardsPoint);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error in earn rewards point=>"+e);
			emailSenderService.sendEmail(Constants.NOREPLY_EMAIL_ID, PaymentConstants.PAYMENT_ERROR_EMAIL_ID.split(Constants.COMMA), "RewardsPointServiceImpl==> Error in earn rewards point", e.getMessage(),null,null, false);
		}
	}
	
	/**
	 * Reedom rewards points.
	 * 10 rewards convert 1 rupees.
	 * @param userDto
	 * @return
	 */
	public RewardsPoint reedomRewardsPoint(UserDTO userDto,boolean isRedeemPoints){
		int reedomValue=0;
		RewardsPoint rewardsPoint = null;
		try{
	        rewardsPoint = getRewardsPointObject(userDto);
		    if(null!=rewardsPoint && rewardsPoint.getRewardsPoint()>=10){
				int reedomRewardsPointBaseValue = utility.convertStringToInt(env.getProperty(PropertyKeyConstants.RewardPointProperties.REEDOM_REWARDSPOINT_BASE_VALUE));
				if(reedomRewardsPointBaseValue!=0){
					reedomValue = rewardsPoint.getRewardsPoint()/reedomRewardsPointBaseValue;
					reedomValue = Math.round(reedomValue);
					rewardsPoint.setRewardsPoint_value(reedomValue);
					if(isRedeemPoints){
						//reedom rewards point
						rewardsPointDao.insertReedomRewardsPointLogs(rewardsPoint);
						rewardsPoint.setEarnReswardsPoint(0);
						rewardsPoint.setRecharge_amount(0);
						rewardsPoint.setRewardsPoint(0);
						//reset rewards point details,if rewards point is reedomed
						rewardsPointDao.updateRewardsPoint(rewardsPoint);
						loadRewardsPointDetails();
						//update wallet balance 
						userDto.setWalletBalance(userDto.getWalletBalance() + reedomValue);
						userService.updateUserWalletBalance(userDto);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			emailSenderService.sendEmail(Constants.NOREPLY_EMAIL_ID, PaymentConstants.PAYMENT_ERROR_EMAIL_ID.split(Constants.COMMA), "RewardsPointServiceImpl==> Error in reedom rewards point", e.getMessage(),null,null, false);
		}
		return rewardsPoint;
	}
	
	/**
	 * Set rewards point details in DTO Object earn and reedom rewards point.
	 * @param rewardsPoints
	 * @param userDto
	 * @param rechargeAmount
	 * @return
	 */
	private RewardsPoint setRewardsPointDetails(final int rewardsPoints,final UserDTO userDto,final int rechargeAmount){
		RewardsPoint rewardsPoint = getRewardsPointObject(userDto);
		rewardsPoint.setEarnReswardsPoint(rewardsPoints);
		rewardsPoint.setRewardsPoint(rewardsPoints+rewardsPoint.getRewardsPoint());
		rewardsPoint.setRecharge_amount(rechargeAmount);
		rewardsPoint.setUserDto(userDto);
		rewardsPoint.setChargers_id(userDto.getId());
		rewardsPointsDetailMap.put(userDto.getId(), rewardsPoint);
		return rewardsPoint;
	}
	
	/**
	 * Get rewards point object.
	 * if user exists in rewards_point table (rewardsPointsDetailMap) then return RewardsPoint Object.
	 * if user is not exists in rewards_point table then create new RewardsPoint object and retrun.
	 * @param userDto
	 * @return
	 */
	private RewardsPoint getRewardsPointObject(UserDTO userDto){
		RewardsPoint rewardsPoint = null;
		if(null!=userDto){
			if(rewardsPointsDetailMap.containsKey(userDto.getId())){
				rewardsPoint = rewardsPointsDetailMap.get(userDto.getId());
				return rewardsPoint;
			}
		}
		rewardsPoint = new RewardsPoint();
		return rewardsPoint;
	}
	
	
}
