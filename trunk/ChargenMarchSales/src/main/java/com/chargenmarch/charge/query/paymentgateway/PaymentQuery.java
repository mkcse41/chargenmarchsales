package com.chargenmarch.charge.query.paymentgateway;

/**
 * 
 * @author mukesh
 *
 */
public class PaymentQuery {
	public static final String SELECT_FROM_PRODUCT = "Select * From product";
	public static final String INSERT_INTO_PAYMENT_GATEWAY_ORDER = "insert into payment_order set productId=?,orderId=?,amount=?,firstName=?,lastName=?,emailId=?,mobileNo=?,pincode=?,city=?,state=?,address=?,status=?,insertTimestamp=now(),updateTimestamp=now()";
	public static final String UPDATE_PAYMENT_GATEWAY_ORDER = " update payment_order set status=?,mihpayId=?,mode=?,discount=?,offer=?,bankcode=?,pgType=?,bankRefNum=?,shippingFirstname=?,shippingLastname=?,shippingAddress1=?,shippingAddress2=?,shippingCity=?,shippingState=?,shippingCountry=?,shippingZipcode=?,shippingPhone=?,error=?,hash=?,updateTimestamp=now() where orderId=?";
	public static final String INSERT_INTO_PAYMENT_GATEWAY_LOGS = "insert into payment_logs set payment_orderId=?,request=?,insertTimestamp=now(),updateTimestamp=now()";
	public static final String UPDATE_PAYMENT_GATEWAY_LOGS = "update payment_logs set response=?,updateTimestamp=now() where payment_orderId=?";
	public static final String SELECT_FROM_PAYMENT_GATEWAY_ORDER = "Select max(orderId)+1 as orderId From payment_order where productId=?";
}
