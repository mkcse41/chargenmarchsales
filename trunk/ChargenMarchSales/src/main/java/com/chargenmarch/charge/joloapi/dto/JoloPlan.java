package com.chargenmarch.charge.joloapi.dto;

public class JoloPlan {

	String Detail;
	String Amount;
	String Validity;
	
	public String getDetail() {
		return Detail;
	}
	public void setDetail(String detail) {
		this.Detail = detail;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		this.Amount = amount;
	}
	public String getValidity() {
		return Validity;
	}
	public void setValidity(String validity) {
		this.Validity = validity;
	}
	
}
