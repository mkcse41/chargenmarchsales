package com.chargenmarch.charge.crons;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.chargenmarch.charge.joloapi.service.IRechargeService;
import com.chargenmarch.common.constants.FilePathConstants;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.service.http.IHTTPService;

@Service
//@EnableScheduling
@PropertySource(FilePathConstants.CLASSPATH+FilePathConstants.JOLOAPI_PROPERTIES)
public class RechargeCrons {
	
	private final static Logger logger = LoggerFactory.getLogger(RechargeCrons.class);

	/*@Autowired
	IMobileRechargeService mobileService;
	
	@Autowired
	ISMSService smsService;
	*/
	@Autowired
	Environment env;
	
	@Autowired
	IHTTPService httpService;
	
	@Autowired
	IEmailSenderService mailService;
	
	@Autowired
	IRechargeService rechargeService;
	
	/*@Scheduled(cron="0 0 18 1/1 * ?")
	public void plansUpdateCron(){
		try{
			logger.info("Plans Updator Cron Executed at "+new Date());
			mobileService.updatePlanListForAll();
		} catch (Exception e){
			logger.error("Exception in Plans Updator Cron." + e);
		}
	}*/
	
	/*@Scheduled(cron="0 0/5 * 1/1 * ?")
	public void checkSMSAndRechargeBalance(){
		try{
			logger.info("Check SMS and Recharge Balance Cron Start at  "+new Date());
			smsService.checkSMSBalance();
		} catch (Exception e){
			logger.error("Exception in Check SMS And Recharge BalanceCron ." + e);
		}
	}*/
	
	/*@Scheduled(cron="0 0 01 * * ?")
	public void generateRechargeCashback(){
		try{
			logger.info("Generate Recharge Cashback Cron Start at  "+new Date());
			mobileService.generateRechargeCashBackAndEMail();
		} catch (Exception e){
			logger.error("Exception in Generate Recharge Cashback Cron ." + e);
		}
	}*/
	
/*	@Scheduled(cron="0 0 0/1 1/1 * ?")
	public void offersUpdateCron(){
		try{
			logger.info("Offers Updator Cron Executed at "+new Date());
		}catch(Exception e){
			logger.error("Exception in Offers Updator Cron." + e);
		}
	}*/
	
	/*@Scheduled(cron="0 0 0/1 1/1 * ?")
	public void sendBalanceToMail() {
		try {
			String apiUrl = env.getProperty(PropertyKeyConstants.JOLOAPIProperties.JOLO_BALANCE_CHECK_API_URL);
			String response = httpService.HTTPGetRequest(apiUrl);
			JsonParser parser = new JsonParser();
			JsonObject json = parser.parse(response).getAsJsonObject();
			String mailBody = "Jolo Balance at "+json.get("time").toString()+" is Rs. "+json.get("balance").toString();
			EmailDTO email = new EmailDTO("JOLO Balance Mail", MailingConstants.CAM_GMAIL_ID.split(Constants.COMMA), Constants.NOREPLY_EMAIL_ID, null, null, mailBody, null, null);
			mailService.sendEmail(email);
		} catch(Exception e) {
			logger.error("Exception in Balance Check", e);
		}
	}
	
	@Scheduled(cron="0 0 0/1 1/1 * ?")
	public void checkRechargeStatus() {
		try {
			rechargeService.checkAndUpdateRechargeStatus();
		} catch(Exception e) {
			logger.error("Exception in checkAndUpdateRechargeStatus", e);
		}
	}*/
	
	
	/*@Scheduled(cron="0 30 1 1/1 * ?")
	public void updateRechargeMarginForSalesUser() {
		try {
			rechargeService.updateRechargeMarginForSalesUser();
		} catch(Exception e) {
			logger.error("Exception in updateRechargeMarginForSalesUser", e);
		}
	}*/
}
