package com.chargenmarch.charge.enums.recharge;

public enum PrepaidOperatorCodes {
	AIR("AIRCEL"),
	A("Airtel"),
	BT("BSNL"),
	I("IDEA"),
	MTT("MTNL"),
	M("MTS"),
	RC("Reliance CDMA"),
	RG("Reliance GSM"),
	T("TATA INDICOM"),
	D("TATA DOCOMO"),
	U("TELENOR"),
	V("Vodafone"),
	VD("Videocon");

	private String opName;       

    private PrepaidOperatorCodes(String s) {
    	opName = s;
    }
    
    public String getOperatorName(){
    	return opName;
    }
}

