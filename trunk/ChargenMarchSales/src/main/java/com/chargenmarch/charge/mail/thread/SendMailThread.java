package com.chargenmarch.charge.mail.thread;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.service.user.IUserService;
import com.chargenmarch.common.utility.IUtility;

/**
 * This class used to send mail to all chargenmarch user.
 * @author mukesh
 *
 */
@Component
@Scope(Constants.PROTOTYPE)
public class SendMailThread extends Thread{
	
	private final static Logger logger = LoggerFactory.getLogger(SendMailThread.class);
	
	@Autowired
	IUserService userService;
	
	@Autowired
	IUtility utility;
	
	private String isDelete;
	private String lastId;

	public void setLastId(String lastId) {
		this.lastId = lastId;
	}


	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}


	public void run(){
		try{
			int counter = 0;
			int lastIdInt = 0;
			if(null != lastId && !lastId.equalsIgnoreCase(Constants.BLANK)){
				lastIdInt = utility.convertStringToInt(lastId);
			}
			if(null != isDelete && isDelete.equalsIgnoreCase(Constants.TRUE.toString())){
			   userService.truncateEmailLogs();
			}
			Map<Integer, UserDTO> userInfoMapById = userService.sendMailUserInfoMapById();
			logger.info("lastId ==> "+lastId);
			logger.info("sendMailUserInfoMapById ==> "+userInfoMapById.size());
			for (Map.Entry<Integer, UserDTO> entry : userInfoMapById.entrySet()){
		        final UserDTO user = entry.getValue();
		        if(user.getId() > lastIdInt){
				    userService.sendCAMReferralMail(user);
				    userService.insertEmailLogs(user);
				    if(counter > 150){
				    	counter = 0;
				    	Thread.sleep(600000);
				    }
				    counter++;
		        }
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error in SendMailThread ==> "+e);
		}
	}
}
