package com.chargenmarch.charge.thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.chargenmarch.charge.constants.recharge.RechargeConstants;
import com.chargenmarch.charge.dto.mobilerecharge.Operator;
import com.chargenmarch.charge.service.mobilerecharge.IMobileRechargeService;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.FilePathConstants;
import com.chargenmarch.common.constants.PropertyKeyConstants;
import com.chargenmarch.common.service.email.IEmailSenderService;

@Component
@Scope(Constants.PROTOTYPE)
@PropertySource(FilePathConstants.CLASSPATH+FilePathConstants.API_PROPERTIES)
public class NumberOperatorMapperThread implements Runnable{

	private final static Logger logger = LoggerFactory.getLogger(NumberOperatorMapperThread.class);
	
	@Autowired
	private IMobileRechargeService mobileRechargeService;
	
	@Autowired
	private IEmailSenderService emailSenderService;
	
	@Autowired
	private Environment env;

	private Operator operator;
	private String mobileNumber;	

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public void run(){
		try{
			if(mobileRechargeService.getOperatorByNumberFromDB(mobileNumber)==null){
				   mobileRechargeService.saveOperatorNumberMapping(mobileNumber, operator);
			}
		} catch(Exception e){
			logger.error("Error in saving Operator Number Mapping for Number : "+mobileNumber+"\n"+e);
			emailSenderService.sendEmailWithMultipleRecipient(Constants.NOREPLY_EMAIL_ID, RechargeConstants.RECHARGE_ERROR_EMAIL_ID.split(Constants.COMMA),env.getProperty(PropertyKeyConstants.APIProperties.RECHARGE_EXCEPTION_MAILLIST).split(Constants.COMMA), "Error in saving operator-number mapping for number:" + mobileNumber, e.getMessage(),null,null, false);
		}
	}
}
