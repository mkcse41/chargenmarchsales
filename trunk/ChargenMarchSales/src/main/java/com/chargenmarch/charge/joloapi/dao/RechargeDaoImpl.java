package com.chargenmarch.charge.joloapi.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.WebApplicationContext;

import com.chargenmarch.charge.dto.funds.AddFundsRequestDTO;
import com.chargenmarch.charge.dto.funds.RechargeMarginDTO;
import com.chargenmarch.charge.dto.mobilerecharge.Operator;
import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.charge.funds.controller.FundsController;
import com.chargenmarch.charge.joloapi.dto.JOLOCircle;
import com.chargenmarch.charge.joloapi.dto.JOLOOperator;
import com.chargenmarch.charge.joloapi.dto.JoloErrorDTO;
import com.chargenmarch.charge.joloapi.mapper.FundsRequestMapper;
import com.chargenmarch.charge.joloapi.mapper.JOLOCircleCodesMapper;
import com.chargenmarch.charge.joloapi.mapper.JoloErrorMapper;
import com.chargenmarch.charge.joloapi.mapper.JoloOperatorCodeMapper;
import com.chargenmarch.charge.joloapi.query.Query;
import com.chargenmarch.charge.mapper.mobilerecharge.OperatorMapper;
import com.chargenmarch.charge.mapper.recharge.RechargeMapper;
import com.chargenmarch.charge.mapper.recharge.RechargeMarginMapper;
import com.chargenmarch.common.dto.user.UserDTO;

@Repository
public class RechargeDaoImpl implements IRechargeDao{

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	WebApplicationContext context;
	
	@Override
	public List<JOLOOperator> getJOLOOperatorCodes(){
		return jdbcTemplate.query(Query.SELECT_JOLO_OPERATOR_CODE_DETAILS, new JoloOperatorCodeMapper());
	}
	public List<JOLOOperator> getJOLOOperatorCodesByServiceType(String serviceType){
		Object args[] = {serviceType};
		return jdbcTemplate.query(Query.SELECT_JOLO_OPERATOR_CODE_DETAILS_BY_SERVICE_TYPE, args, new JoloOperatorCodeMapper());
	}
	
	public JOLOOperator getJOLOOperatorCodesByPayCode(String payCode){
		Object args[] = {payCode};
		List<JOLOOperator> operatorList = jdbcTemplate.query(Query.SELECT_JOLO_OPERATOR_CODE_DETAILS_BY_PAY_CODE, args, new JoloOperatorCodeMapper());
		return operatorList.size()>0 ? operatorList.get(0) : null;
	}
	
	public JOLOOperator getJOLOOperatorCodesByCode(String code){
		Object args[] = {code};
		List<JOLOOperator> operatorList = jdbcTemplate.query(Query.SELECT_JOLO_OPERATOR_CODE_DETAILS_BY_CODE, args, new JoloOperatorCodeMapper());
		return  operatorList.size()>0 ? operatorList.get(0) : null;
	}
	
	@Override
	public List<JOLOCircle> getJOLOCircleCodes(){
		return jdbcTemplate.query(Query.SELECT_JOLO_CIRCLE_CODE_DETAILS, new JOLOCircleCodesMapper());
	}
	@Override
	public JOLOCircle getJOLOCircleCodesByCircleCode(String circleCode){
		Object[] args = {circleCode};
		List<JOLOCircle> circleList = jdbcTemplate.query(Query.SELECT_JOLO_CIRCLE_CODE_DETAILS_BY_CODE, args, new JOLOCircleCodesMapper());
		return circleList.size()>0 ? circleList.get(0) : null;
	}
	public Operator getOperatorByNumber(String mobileNumber){
		try{
			Object args[] = {mobileNumber};
			return jdbcTemplate.queryForObject(Query.SELECT_JOLO_NUMBER_OPERATOR_MAPPING, args, new OperatorMapper());
		}catch(EmptyResultDataAccessException e){
			return null;
		}
	}
	
	public void saveJoloOperatorNumberMapping(String mobileNumber, Operator operator){
		Object args[] = {mobileNumber, operator.getOperator(), operator.getPay_code(), operator.getCode(), operator.getCircle(), operator.getPay_circle_code(), operator.getCircle_code()};
		jdbcTemplate.update(Query.INSERT_INTO_NUMBER_OPERATOR_MAPPING, args);
	}
	
	public void saveRechargeLogs(String rechargeService,String request,String response,String serviceType){
		Object args[] = {rechargeService,request, response,serviceType};
		jdbcTemplate.update(Query.INSERT_INTO_RECHARGE_LOGS, args);
	}
	

	public JoloErrorDTO getJOLOErrorDesByCode(String errorCode){
		Object args[] = {errorCode};
		List<JoloErrorDTO> operatorList = jdbcTemplate.query(Query.SELECT_JOLO_ERROR_DES_BY_CODE, args, new JoloErrorMapper());
		return operatorList.size()>0 ? operatorList.get(0) : null;
	}
	
	public List<RechargeDTO> getRechargeList(){
		return jdbcTemplate.query(Query.SELECT_FROM_RECHARGE_INTERVAL, context.getBean(RechargeMapper.class));
	}
	
	public void updateRechargeSatus(RechargeDTO rechargeDto){
		Object args[] = {rechargeDto.getIsSuccesfulRecharge(),rechargeDto.getId()};
		jdbcTemplate.update(Query.UPDATE_RECHARGE_STATUS, args);
	}
	
	public List<RechargeDTO> getSalesUserRechargeListByChargersId(UserDTO user, String dateoneDaysBefore){
		Object args[] = {user.getId(), "%"+dateoneDaysBefore+"%"};
		return jdbcTemplate.query(Query.SELECT_FROM_SALES_USER_RECHARGE_LIST, args ,context.getBean(RechargeMapper.class));
	}
	
	public void saveRechargeMarginLogs(UserDTO user, String date, int totalRechargeAmount, int marginAmount){
		Object args[] = {user.getId(),totalRechargeAmount,marginAmount,date};
		jdbcTemplate.update(Query.INSERT_INTO_RECHARGE_MARGIN_LOGS, args);
	}
	
	public List<RechargeMarginDTO> getRechargeMarginLogs(UserDTO user){
		Object args[] = {user.getId()};
		return jdbcTemplate.query(Query.SELECT_FROM_RECHARGE_MARGIN_LOGS_BY_CHARGER_ID,args, context.getBean(RechargeMarginMapper.class));
	}
	
	public boolean getSalesUserRechargeListByDate(String dateoneDaysBefore,UserDTO userDTO){
		Object args[] = {dateoneDaysBefore,userDTO.getId()};
		List marginList =  jdbcTemplate.queryForList(Query.SELECT_FROM_RECHARGE_MARGIN_LOGS_BY_DATE, args);
		if(null != marginList && !marginList.isEmpty()){
			return true;
		}
		return false;
	}
	
	public void updateJoloOperatorNumberMapping(String mobileNumber, Operator operator){
		Object args[] = {operator.getOperator(), operator.getPay_code(), operator.getCode(), operator.getCircle(), operator.getPay_circle_code(), operator.getCircle_code(),mobileNumber};
		jdbcTemplate.update(Query.UPDATE_NUMBER_OPERATOR_MAPPING, args);
	}
}
