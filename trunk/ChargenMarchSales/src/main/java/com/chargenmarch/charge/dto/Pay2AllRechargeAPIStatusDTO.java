package com.chargenmarch.charge.dto;

public class Pay2AllRechargeAPIStatusDTO {
	
	private String payid;
	private String operator_ref;
	private String status;
	private String message;
	
	public String getPayid() {
		return payid;
	}
	public void setPayid(String payid) {
		this.payid = payid;
	}
	public String getOperator_ref() {
		return operator_ref;
	}
	public void setOperator_ref(String operator_ref) {
		this.operator_ref = operator_ref;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
