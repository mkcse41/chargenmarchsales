package com.chargenmarch.charge.funds.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.chargenmarch.charge.dto.funds.AddFundsRequestDTO;
import com.chargenmarch.charge.joloapi.service.IRechargeService;
import com.chargenmarch.charge.service.mobilerecharge.IMobileRechargeService;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.RequestConstants;
import com.chargenmarch.common.constants.SessionConstants;
import com.chargenmarch.common.dto.email.EmailDTO;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.service.user.IUserService;
import com.chargenmarch.common.utility.IUtility;

/**
 * 
 * @author mukesh
 *
 */

@Controller
@RequestMapping(value = RequestConstants.FUNDS)
public class FundsController {
    
	private static final String FUNDS_REQUEST_DTO = "fundsRequestDTO";
	
	private final static Logger LOGGER = LoggerFactory.getLogger(FundsController.class);
	
	@Autowired
	private IUtility utility;
	
	@Autowired
	private IUserService userservice;
	
	@Autowired
	private IRechargeService rechargeService;
	
	@Autowired
	private IEmailSenderService emailService;
	
	@Autowired
	private IMobileRechargeService mobileRechargeService;
	
	
	@RequestMapping(value = "/addRequest", method = {RequestMethod.POST})
	public @ResponseBody String addfunds(@ModelAttribute(FUNDS_REQUEST_DTO) AddFundsRequestDTO fundsRequestDTO,HttpServletRequest request,HttpServletResponse response) throws IOException {
		try{
			if(null!=request.getSession().getAttribute(SessionConstants.USER_BEAN)){
				UserDTO user = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
				if(null != fundsRequestDTO && fundsRequestDTO.getAmount() != 0 && !StringUtils.isEmpty(fundsRequestDTO.getBankReferenceId())){
					fundsRequestDTO.setUser(user);
					int addFundsRequest = rechargeService.insertAddFundsRequest(fundsRequestDTO);
					if(addFundsRequest !=0 ){
						//send invoice
						final AddFundsRequestDTO fundsRequestEmailDTO = fundsRequestDTO;
						Runnable run = new Runnable() {
						    public void run() {
						        try {
						        	EmailDTO email = getFundsRequestMailObject(fundsRequestEmailDTO);
									emailService.sendEmail(email);
						        } catch (Exception e) {
						           
						        }
						    }
						 };
					    new Thread(run).start();
						
						return Constants.SUCCESS;
					}
				}
				return Constants.FAILURE;
			}
		}catch(Exception e){
			LOGGER.error("Error in funds Request");
			e.printStackTrace();
		}
		response.sendRedirect(utility.getBaseUrl());
	    return null;
	}

	private EmailDTO getFundsRequestMailObject(AddFundsRequestDTO fundsRequestDTO){
		String message = getAddFundsStatusMailString(fundsRequestDTO);
		return new EmailDTO("ADD funds Request - Chargers Id :"+fundsRequestDTO.getUser().getId(), "chargeandmarch@gmail.com".split(Constants.COMMA), Constants.NOREPLY_EMAIL_ID, null, null, message, null, null);
	}
	
	private String getAddFundsStatusMailString(AddFundsRequestDTO fundsRequestDTO){
	    String emailString = "<table border=1><tr><th>Chargers Id</th><th>Name</th><th>Email Id</th><th>Amount</th><th>Payment Method</th><th>Bank Name</th><th>Bank reference Id</th><th>Remarks</th></tr>";
		emailString += "<tr><td>"+fundsRequestDTO.getUser().getId()+"</td><td>"+fundsRequestDTO.getUser().getName()+"</td><td>"+fundsRequestDTO.getUser().getEmailId()+"</td><td>"+fundsRequestDTO.getAmount()+"</td><td>"+fundsRequestDTO.getPaymentMethod()+"</td><td>"+fundsRequestDTO.getBankName()+"</td><td>"+fundsRequestDTO.getBankReferenceId()+"</td><td>"+fundsRequestDTO.getRemarks()+"</td></tr>";
		emailString += "</table>";
		return emailString;
    }
	
	@RequestMapping(value = "/processedRequest", method = {RequestMethod.POST})
	public @ResponseBody String processedRequest(HttpServletRequest request,HttpServletResponse response) throws IOException {
		try{
			  final int amount = utility.convertStringToInt(request.getParameter("amount"));
			  final String bankRefId = request.getParameter("bankRefId");
			  int chargersId = utility.convertStringToInt(request.getParameter("chargersId"));
			  UserDTO userDTO = userservice.getUserById(chargersId);
			  if(null != userDTO && !StringUtils.isEmpty(bankRefId) && amount !=0 ){
				  int status  = mobileRechargeService.updateFundsRequestStatus(userDTO, bankRefId);
				  if(status == 1){
					  userDTO.setWalletBalance(userDTO.getWalletBalance() + amount);
					  userservice.updateUserWalletBalance(userDTO);
					  userservice.updateUserCacheMaps(userDTO);
					  final UserDTO userEmail = userDTO;
					  Runnable run = new Runnable() {
						    public void run() {
						        try {
						        	EmailDTO email = fundsRequestStatusMailObject(userEmail, bankRefId, amount);
									emailService.sendEmail(email);

						        } catch (Exception e) {
						           
						        }
						    }
						 };
					  new Thread(run).start();
					  return Constants.SUCCESS;
				  }
			  }
			  return Constants.FAILURE;
		}catch(Exception e){
			LOGGER.error("Error in funds Request");
			e.printStackTrace();
		}
		response.sendRedirect(utility.getBaseUrl());
	    return null;
	}
	
	private EmailDTO fundsRequestStatusMailObject(UserDTO user,String bankRefId,int amount){
		String currentDate = utility.getCurrentDate(Constants.DateConstants.DEFAULT_DATE_FORMAT);
		String message = utility.classpathResourceLoader("camsales-fundsstatus.html").replace("%NAME%", user.getName()).replace("%TOTALAMOUNT%", String.valueOf(user.getWalletBalance())).replace("%AMOUNT%", String.valueOf(amount)).replace("%BANKREFID%", bankRefId).replace("%EMAILID%", user.getEmailId()).replace("%DATE%", currentDate);
		return new EmailDTO("Recharge Balance Status - ChargenMarch.com", user.getEmailId().split(Constants.COMMA), Constants.NOREPLY_EMAIL_ID, null, null, message, null, null);
	}
   
}
