/**
 * 
 */
package com.chargenmarch.charge.dto.coupon;

import java.sql.Date;

/**
 * @author mukesh
 *
 */
public class CouponDTO {

	private int id;
	private String couponName;
	private String couponType;
	private double couponValue;
	private int couponUses;
	private int couponLimit;
	private int minimumRechargeValue;
	private String couponServiceType;
	private int couponPerUserLimit;
	private int couponStatus;
	private Date startDate;
	private Date endDate;
	private double couponReedomValue;
	private boolean isCouponApply;
	private String errorMessage;
	private int maximumcashback;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCouponName() {
		return couponName;
	}
	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}
	public String getCouponType() {
		return couponType;
	}
	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}
	public double getCouponValue() {
		return couponValue;
	}
	public void setCouponValue(double couponValue) {
		this.couponValue = couponValue;
	}
	public int getCouponUses() {
		return couponUses;
	}
	public void setCouponUses(int couponUses) {
		this.couponUses = couponUses;
	}
	public int getCouponLimit() {
		return couponLimit;
	}
	public void setCouponLimit(int couponLimit) {
		this.couponLimit = couponLimit;
	}
	public int getMinimumRechargeValue() {
		return minimumRechargeValue;
	}
	public void setMinimumRechargeValue(int minimumRechargeValue) {
		this.minimumRechargeValue = minimumRechargeValue;
	}
	public String getCouponServiceType() {
		return couponServiceType;
	}
	public void setCouponServiceType(String couponServiceType) {
		this.couponServiceType = couponServiceType;
	}
	public int getCouponStatus() {
		return couponStatus;
	}
	public void setCouponStatus(int couponStatus) {
		this.couponStatus = couponStatus;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public int getCouponPerUserLimit() {
		return couponPerUserLimit;
	}
	public void setCouponPerUserLimit(int couponPerUserLimit) {
		this.couponPerUserLimit = couponPerUserLimit;
	}
	public double getCouponReedomValue() {
		return couponReedomValue;
	}
	public void setCouponReedomValue(double couponReedomValue) {
		this.couponReedomValue = couponReedomValue;
	}
	public boolean isCouponApply() {
		return isCouponApply;
	}
	public void setCouponApply(boolean isCouponApply) {
		this.isCouponApply = isCouponApply;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public int getMaximumcashback() {
		return maximumcashback;
	}
	public void setMaximumcashback(int maximumcashback) {
		this.maximumcashback = maximumcashback;
	}
	
}


