package com.chargenmarch.charge.controller;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import com.chargenmarch.charge.constants.recharge.RechargeConstants;
import com.chargenmarch.charge.dto.mobilerecharge.Operator;
import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.charge.service.mobilerecharge.IMobileRechargeService;
import com.chargenmarch.charge.thread.PaymentTrackerThread;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.RequestConstants;
import com.chargenmarch.common.constants.ViewConstants;
import com.chargenmarch.common.constants.RequestConstants.CAMWalletConstant;
import com.chargenmarch.common.constants.paymentgateway.PaymentConstants;
import com.chargenmarch.common.constants.SessionConstants;
import com.chargenmarch.common.dao.user.IUserDao;
import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.chargenmarch.common.dto.user.PaymentLogDto;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.enums.user.UserVerificationStatus;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.service.user.IPaymentLogService;
import com.chargenmarch.common.service.user.IUserService;
import com.chargenmarch.common.utility.IUtility;
import com.chargenmarch.common.validator.user.UserDetailValidator;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

/**
 * This controller used to payment service provide for third party website
 * @author mukesh
 *
 */
@Controller
@RequestMapping(value = RequestConstants.CAMWalletConstant.USER_WALLET)
public class CAMWalletController {
    
	private final static Logger logger = LoggerFactory.getLogger(CAMWalletController.class);
	
	@Autowired
	private UserDetailValidator userValidator;
	
	@Autowired
	private IUtility utility;
	
	@Autowired
	private IUserDao userDao;
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private IMobileRechargeService rechargeService;
	
	@Autowired
	ThreadPoolTaskExecutor taskExecutor;
	
	@Autowired
	PaymentTrackerThread tracker;

	@Autowired
	IPaymentLogService trackingService;
	
	@Autowired
	private IEmailSenderService emailService;
	
	@Autowired
	private WebApplicationContext context;
	
	@RequestMapping(value = RequestConstants.CAMWalletConstant.USER_LOGIN , method = RequestMethod.GET)
	public ModelAndView camWallet(@RequestParam(RequestConstants.CAMWalletConstant.AMOUNT) String amount, HttpServletRequest request, HttpServletResponse reaponse) throws Exception{
		request.getSession().setAttribute(RequestConstants.CAMWalletConstant.AMOUNT, amount);
		ModelAndView mv = new ModelAndView(utility.getviewResolverName(request, CAMWalletConstant.CAM_WALLET_LOGIN));
		return mv;
	}
	
	/**
	 * This Method used to login third party payment service user.
	 * 1. User verified and non verified case send otp message to user mobile number.
	 */
	@RequestMapping(value = RequestConstants.CAMWalletConstant.LOGIN , method = RequestMethod.POST)
	@ResponseBody
	public String camWalletLogin(@RequestParam(RechargeConstants.EMAIL_ID) String emailId, @RequestParam(RechargeConstants.PASSWORD) String password,HttpServletRequest request) throws Exception{
		JsonObject response = new JsonObject();
		try{
			UserDTO user = userService.getUserByEmailId(emailId);
			if(null != user && null != password){
				if(user.getPassword().equalsIgnoreCase(password)){
					if(user.getIsverified() == UserVerificationStatus.VERIFIED.ordinal()){
						request.getSession().setAttribute(SessionConstants.USER_BEAN, user);
						response.addProperty(Constants.MESSAGE, Constants.SUCCESS);
						
						/*
						 * Send otp user login case.
						 */
						int otpResponse = sendOTPMessage(user, request);
						if(otpResponse != 1){
							response.addProperty(Constants.MESSAGE, Constants.FAILURE);
						}
						
					} else if(user.getIsverified() == UserVerificationStatus.UNVERIFIED.ordinal()){
						request.getSession().setAttribute(SessionConstants.USER_BEAN, user);
						response.addProperty(Constants.MESSAGE, Constants.NON_VERIFIED);
					}
				}else{
					response.addProperty(Constants.MESSAGE, Constants.FAILURE);
				}
			}else if(user == null){
				response.addProperty(Constants.MESSAGE, "user not exists");
			}else{
				response.addProperty(Constants.MESSAGE, Constants.FAILURE);
			}
		}catch(Exception e){
			logger.error("Error in CAM Wallet login");
			e.printStackTrace();
		}
		return response.toString();
	}
	
	/**
	 * 
	 * @param user
	 * @param result
	 * @param request
	 * @return
	 */
	@RequestMapping(value=RequestConstants.CAMWalletConstant.REGISTER, method = RequestMethod.POST)
	@ResponseBody
	public String userRegister(@Valid UserDTO user, BindingResult result, HttpServletRequest request){
		JsonObject response = new JsonObject();
		try{
			userValidator.validate(user, result);
			if(result.hasErrors()){
				response.addProperty(Constants.MESSAGE, "User Not Registered. Please Try Again.");
			} else{
				if(null != user){
					user.setSource(RequestConstants.CAMWalletConstant.CAM_WALLET);
					int userAddStatus = userService.addUser(user);
					UserDTO sessionUser = null;
					if(userAddStatus == 1){
						sessionUser = userService.getUserByEmailId(user.getEmailId().trim());
						//generate OTP
						if(null != sessionUser){
							int otpResponse = sendOTPMessage(sessionUser, request);
							if(otpResponse == 1){
								response.addProperty(Constants.MESSAGE,"OTP_Screen");
							}else{
								response.addProperty(Constants.MESSAGE, "User Not Registered. Please Try Again.");	
							}
						}
					} else if(userAddStatus == 3){
						sessionUser = userService.getUserInfoMapByEmailId().get(user.getEmailId());
						if(null != sessionUser){
							int otpResponse = sendOTPMessage(sessionUser, request);
							if(otpResponse == 1){
								response.addProperty(Constants.MESSAGE,"OTP_Screen");
							}else{
								response.addProperty(Constants.MESSAGE, "User Already Registered");	
							}
					    }
					}else 
						response.addProperty(Constants.MESSAGE, "User Not Registered. Please Try Again.");
				}
			}
		}catch(Exception e){
			logger.error("Error in Wallet user register==>"+e.getMessage());
			e.printStackTrace();
		}
			
		return response.toString();
	}
	
	@RequestMapping(value=RequestConstants.CAMWalletConstant.USER_REGISTER_OTHER, method = RequestMethod.POST)
	@ResponseBody
	public String userRegisterByThirdParty(@Valid UserDTO user, BindingResult result, HttpServletRequest request){
		
		userValidator.validate(user, result);
		JsonObject response = new JsonObject();
		if(result.hasErrors()){
			List<ObjectError> errorList = result.getAllErrors();
			Iterator<ObjectError> errorItr = errorList.iterator();
			while(errorItr.hasNext()){
				ObjectError error = errorItr.next();
			}
			response.addProperty(Constants.MESSAGE, "User Not Registered. Please Try Again.");
			
		} else{
			int userAvailableStatus = userService.checkUser(user);
			UserDTO sessionUser = null;
			if(userAvailableStatus==1){
				 response.addProperty(Constants.MESSAGE, "Show Mobile Number Screen.");	
			} else if(userAvailableStatus==3){
				sessionUser = userService.getUserInfoMapByEmailId().get(user.getEmailId());
				request.getSession().setAttribute(SessionConstants.USER_BEAN, sessionUser);
				response.addProperty(Constants.MESSAGE,"User Already Registered");
				request.getSession().removeAttribute(RequestConstants.QUICK_RECHARGE_CHECK_VAL);
			}
			else 
				response.addProperty(Constants.MESSAGE, "User Not Registered. Please Try Again.");
		}
		return response.toString();
	}
	
	@RequestMapping(value=RequestConstants.CAMWalletConstant.OTP_RESPONSE , method = RequestMethod.POST)
    @ResponseBody
    public String userOTPVerification(@RequestParam(RechargeConstants.OTP) String otp , HttpServletRequest request){
		JsonObject response = new JsonObject();
		try{
		    UserDTO sessionUser = (UserDTO)request.getSession().getAttribute(SessionConstants.USER_BEAN);
			if(null != sessionUser && sessionUser.getOtp() == utility.convertStringToInt(otp)){
				if(sessionUser.getIsverified() != UserVerificationStatus.VERIFIED.ordinal()){
					userService.updateUserVerifiedStatus(sessionUser, UserVerificationStatus.VERIFIED);
				}
				response.addProperty(Constants.MESSAGE,"User Registered Successfully");
				request.getSession().removeAttribute(RequestConstants.QUICK_RECHARGE_CHECK_VAL);
			}
			else{
				response.addProperty(Constants.MESSAGE,"Please Insert Correct OTP");
			}
		}catch(Exception e){
			logger.error("Error in Wallet OTP Verification ==> "+e.getMessage());
			e.printStackTrace();
		}
		return response.toString();
	}
	
   @RequestMapping(value = RequestConstants.FORGOT_PASSWORD, method=RequestMethod.POST)
   public @ResponseBody String forgotPassword(@RequestParam("emailId") String emailId){
		UserDTO user = userService.getUserInfoMapByEmailId().get(emailId);
		JsonObject response = new JsonObject();
		if(user!=null){
			String message = utility.classpathResourceLoader("Forgot-Password.html").replace("%BASEURL%", context.getBean("baseUrl", String.class)).replace("%NAME%", user.getName()).replace("%CIPHER%", utility.base64Encoder(user.getEmailId()));
			emailService.sendEmail(Constants.NOREPLY_EMAIL_ID, user.getEmailId().split(Constants.COMMA), "Password Recovery Mail", message, null, null, true);
			response.addProperty(Constants.MESSAGE, "Password has been sent to registered mail id provided above");
		} else {
			response.addProperty(Constants.MESSAGE, "No user registered with above provided Email Id");
		}
		return response.toString();
	}
	
	/**
	 * Processed add Money, Send request to PayU Payment Gateway
	 */
	@RequestMapping(value = RequestConstants.CAMWalletConstant.ADD_MONEY, method = RequestMethod.POST)
	public ModelAndView processedLoadAmount(HttpServletRequest request,HttpServletResponse response) throws IOException{
		try{
			String amount = (String) request.getSession().getAttribute(RequestConstants.CAMWalletConstant.AMOUNT);
			if(null != request.getSession().getAttribute(SessionConstants.USER_BEAN) && null != amount){
				UserDTO user = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
				int loadAmount = utility.convertStringToInt(amount);
				String userCredentials = user.getId() + user.getEmailId();
				OrderInfo orderInfo = utility.initializeOrderInfoForPaymentGateway(user.getName(), null, user.getEmailId(),user.getMobileNo(), 
				    		null, null, null, null, RequestConstants.WALLET, loadAmount, RequestConstants.PAY_U,userCredentials);
			    request.setAttribute(RequestConstants.ORDER_INFO, orderInfo);
			    request.setAttribute(RequestConstants.URL, utility.getBaseUrl() + RechargeConstants.SLASH + PaymentConstants.PAYMENT.toLowerCase() + RequestConstants.PROCESSED_PAYMENT );
			    ModelAndView view = new ModelAndView(ViewConstants.PAY_U_LOWER);
			    paymentTrackingDetail(request, null, user, false, false, true);
			    return view;
		    } 
	    }catch(Exception e){
	    	e.printStackTrace();
	    }
		response.sendRedirect(utility.getBaseUrl());
		return null;
	}
	
    /**
     * PayU Payment Response.
     */
	@RequestMapping(value = RequestConstants.CAMWalletConstant.RESPONSE, method={RequestMethod.POST })
	public ModelAndView userWallet(HttpServletRequest request,HttpServletResponse httpResponse) throws IOException{
		String response = request.getParameter(RequestConstants.RESPONSE);
		try{
			if(null!=request.getSession().getAttribute(SessionConstants.USER_BEAN) && null!=response && utility.isValidatePayment(request)){
				Gson gson = new GsonBuilder().create(); 
				OrderInfo orderInfo = gson.fromJson(response, OrderInfo.class);
				UserDTO user = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
				String orderId = orderInfo.getTxnId();
				String status = orderInfo.getStatus();
				float loadAmount = orderInfo.getAmount() + orderInfo.getDiscount();
				RechargeDTO rechargeDto = createRechargeDtoObject(user, Math.round(loadAmount), orderId);
				request.setAttribute(RequestConstants.CAMWalletConstant.RECHARGE_DTO, rechargeDto);
				if(!orderId.equals(Constants.BLANK) && status.equals(Constants.SUCCESS) && rechargeService.getRechargeListByPGOrderId(orderId).size() <= 0){
					rechargeDto.setSessionId(request.getSession().getId());
					user.setWalletBalance(Math.round(loadAmount) + user.getWalletBalance());
					request.getSession().setAttribute(SessionConstants.USER_BEAN,user);
					paymentTrackingDetail(request, null, user, true, true, true);
					userService.updateUserWalletBalance(user);
					rechargeDto.setIsSuccesfulRecharge(1);
				}
				rechargeService.saveMobileRechareDetails(rechargeDto);
			}
		}catch(Exception e){	
			e.printStackTrace();
		}
		ModelAndView mv = new ModelAndView(utility.getviewResolverName(request, CAMWalletConstant.CAM_WALLET_STATUS));
		return mv;
     }
	
	private RechargeDTO createRechargeDtoObject(UserDTO userDto, int amount,String orderId){
		  RechargeDTO rechargeDto = new RechargeDTO();
		  rechargeDto.setUserDto(userDto);
		  rechargeDto.setAmount(amount);
		  rechargeDto.setServiceType(RequestConstants.CAMWalletConstant.CAM_WALLET);
		  rechargeDto.setOrderId(orderId);
		  rechargeDto.setIsSuccesfulRecharge(0);
		  rechargeDto.setOperator(new Operator());
		  return rechargeDto;
    }
	
	/**
	 * This method used to save payment tracking details.
	 * @param request
	 * @param rechargeDto
	 * @param user
	 * @param isSuccessFulRecharge
	 * @param isPaymentStatus
	 * @param isWallet
	 */
	private void paymentTrackingDetail(HttpServletRequest request,RechargeDTO rechargeDto, UserDTO user, boolean isSuccessFulRecharge, boolean isPaymentStatus, boolean isWallet){
		try{
			boolean isUpdate = false;
			PaymentLogDto paymentLogDto = null;
			int paymentLogId = (Integer) request.getSession().getAttribute("plid");
			if(paymentLogId != 0){
			   isUpdate = true;
			   paymentLogDto = trackingService.getPaymentLogById(paymentLogId);
			   paymentLogDto.setRechargeDto(rechargeDto);
			}
			if(paymentLogDto == null){
				paymentLogDto = setUserTrackingDto(request, rechargeDto, user);
			}
			paymentLogDto.setSuccessfulRecharge(isSuccessFulRecharge);
			paymentLogDto.setPaymentStatus(isPaymentStatus);
			paymentLogDto.getRechargeDto().setUseWallet(isWallet);
			paymentLogDto.setFailureReason(rechargeDto.getError());
			tracker.setSession(request.getSession());
			trackingService.invokePaymentTrackerThread(taskExecutor, tracker, paymentLogDto, isUpdate);
		} catch (Exception e) {
			logger.error("Wallet Payment Action - Exception while saving Payment tracking logs.", e);
		}
	}
    
	private PaymentLogDto setUserTrackingDto(HttpServletRequest request, RechargeDTO rechargeDto, UserDTO user) {
		PaymentLogDto dto = new PaymentLogDto();
		dto.setChargerId(user.getId());
		dto.setIpAddress(utility.getIpAdress(request));
		//dto.setCity(city);
		dto.setSessionId(request.getSession().getId());
		dto.setTimestamp(new Date());
		if(rechargeDto == null) {
			dto.setRechargeDto(new RechargeDTO());
		} else {
			dto.setRechargeDto(rechargeDto);
		}
		return dto;
	}
	
	
	private int sendOTPMessage(UserDTO user, HttpServletRequest request){
		try{
			int otp = utility.generateRandomNumber();
			user.setOtp(otp);
			int otpResponse = /*userService.sendOneTimePassword(user)*/1;
			request.getSession().setAttribute(SessionConstants.USER_BEAN, user);
			logger.info("otp is :" + otp);
			return otpResponse;
	     }catch(Exception e){
	    	 logger.error("Error in Send Wallet OTP Message"+e.getMessage());
	    	 e.printStackTrace();
	     }
		 return 0; 
	}
	
	@RequestMapping(value = RequestConstants.CAMWalletConstant.CAM_WALLET_REVIEW_ORDER, method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView reviewOrder(HttpServletRequest request, HttpServletResponse response) throws IOException {
		ModelAndView view = new ModelAndView(utility.getviewResolverName(request, RequestConstants.CAMWalletConstant.CAM_WALLET_REVIEW_ORDER));
		return view;
	}
	
}
