package com.chargenmarch.charge.mapper.rewardspoint;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.charge.constants.rewardspoint.RewardsPointConstant;
import com.chargenmarch.charge.dto.rewardspoint.RewardsPoint;
import com.chargenmarch.common.constants.Constants;

/**
 * 
 * @author mukesh
 *
 */
public class RewardsPointMapper implements RowMapper{
    
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		 RewardsPoint rewardsPoint = new RewardsPoint();
		 rewardsPoint.setRewards_point_id(rs.getInt(Constants.ID));
		 rewardsPoint.setRewardsPoint(rs.getInt(RewardsPointConstant.REWARDS_POINT));
		 rewardsPoint.setChargers_id(rs.getInt(RewardsPointConstant.CHARGERS_ID));
		 return rewardsPoint;
	}

}
