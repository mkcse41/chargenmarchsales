package com.chargenmarch.charge.joloapi.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.charge.joloapi.constants.JOLOAPIConstants;
import com.chargenmarch.charge.joloapi.dto.JoloErrorDTO;

/**
 * 
 * @author mukesh
 *
 */
public class JoloErrorMapper implements RowMapper<JoloErrorDTO>{
    
	public JoloErrorDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

		JoloErrorDTO joloError = new JoloErrorDTO();
		joloError.setErrorCode(rs.getString(JOLOAPIConstants.ERROR_CODE));
		joloError.setErrorDetails(rs.getString(JOLOAPIConstants.ERROR_DESCRIPTION));
		joloError.setLocalErrorDetails(rs.getString(JOLOAPIConstants.LOCAL_ERROR_DESCRIPTION));
		return joloError;
	}

	
	
}
