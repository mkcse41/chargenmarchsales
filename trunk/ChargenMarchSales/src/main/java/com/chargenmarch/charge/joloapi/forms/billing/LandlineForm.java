package com.chargenmarch.charge.joloapi.forms.billing;

public class LandlineForm {
     private String operator;
     private String operatortext;
	 private String landlinenumber;
     private String amount;
     private String stdcode;
     private String customeraccountno;
     private String service;
     private int extracharges;
	
    public int getExtracharges() {
		return extracharges;
	}
	public void setExtracharges(int extracharges) {
		this.extracharges = extracharges;
	}
	public String getService() {
		return service;
	}
	public String getOperatortext() {
		return operatortext;
	}
	public void setOperatortext(String operatortext) {
		this.operatortext = operatortext;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getLandlinenumber() {
		return landlinenumber;
	}
	public void setLandlinenumber(String landlinenumber) {
		this.landlinenumber = landlinenumber;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getStdcode() {
		return stdcode;
	}
	public void setStdcode(String stdcode) {
		this.stdcode = stdcode;
	}
	public String getCustomeraccountno() {
		return customeraccountno;
	}
	public void setCustomeraccountno(String customeraccountno) {
		this.customeraccountno = customeraccountno;
	}
     
	  
}
