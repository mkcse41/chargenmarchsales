package com.chargenmarch.charge.joloapi.dto;

/**
 * 
 * @author mukesh
 *
 */
public class JoloErrorDTO {
    
	private String errorCode;
	private String errorDetails;
	private String localErrorDetails;
	
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorDetails() {
		return errorDetails;
	}
	public void setErrorDetails(String errorDetails) {
		this.errorDetails = errorDetails;
	}
	public String getLocalErrorDetails() {
		return localErrorDetails;
	}
	public void setLocalErrorDetails(String localErrorDetails) {
		this.localErrorDetails = localErrorDetails;
	}
	@Override
	public String toString() {
		return "JoloErrorDTO [errorCode=" + errorCode + ", errorDetails=" + errorDetails + ", localErrorDetails="
				+ localErrorDetails + "]";
	}
	
	

}
