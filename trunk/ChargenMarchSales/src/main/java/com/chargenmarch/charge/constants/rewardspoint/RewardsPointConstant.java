package com.chargenmarch.charge.constants.rewardspoint;

/**
 * 
 * @author mukesh
 *
 */
public class RewardsPointConstant {

	public static final String REWARDS_POINT = "rewardsPoint";
	public static final String CHARGERS_ID = "chargers_id";
}
