package com.chargenmarch.charge.forms.recharge;

public class RechargeForm {
   private String number;
   private String operatorValue;
   private String operatorText;
   private String service;
   private String mobileRechargeService;
   private String amount;
   private String circleName;
   private String circleValue;
   
   
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getOperatorValue() {
		return operatorValue;
	}
	public void setOperatorValue(String operatorValue) {
		this.operatorValue = operatorValue;
	}
	public String getOperatorText() {
		return operatorText;
	}
	public void setOperatorText(String operatorText) {
		this.operatorText = operatorText;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getMobileRechargeService() {
		return mobileRechargeService;
	}
	public void setMobileRechargeService(String mobileRechargeService) {
		this.mobileRechargeService = mobileRechargeService;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCircleName() {
		return circleName;
	}
	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}
	public String getCircleValue() {
		return circleValue;
	}
	public void setCircleValue(String circleValue) {
		this.circleValue = circleValue;
	}
   
   
}
