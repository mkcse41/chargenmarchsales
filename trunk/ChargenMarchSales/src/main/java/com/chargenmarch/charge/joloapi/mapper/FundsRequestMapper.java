package com.chargenmarch.charge.joloapi.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.chargenmarch.charge.dto.funds.AddFundsRequestDTO;
import com.chargenmarch.common.service.user.IUserService;

@Component
@Scope("prototype")
public class FundsRequestMapper implements RowMapper{

	@Autowired
	IUserService userService;
	
	public AddFundsRequestDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		AddFundsRequestDTO fundsRequestDTO = new AddFundsRequestDTO();
		fundsRequestDTO.setUser(userService.getUserInfoMapById().get((rs.getInt("charger_id"))));
		fundsRequestDTO.setAmount(rs.getInt("amount"));
		fundsRequestDTO.setPaymentMethod(rs.getString("paymentmethod"));
		fundsRequestDTO.setBankName(rs.getString("bankname"));
		fundsRequestDTO.setBankReferenceId(rs.getString("bankReferenceId"));
		fundsRequestDTO.setRemarks(rs.getString("remark"));
		fundsRequestDTO.setStatus(rs.getString("status"));
		fundsRequestDTO.setAddDate(new DateTime(rs.getDate("timestamp")));
		return fundsRequestDTO;
	}
}
