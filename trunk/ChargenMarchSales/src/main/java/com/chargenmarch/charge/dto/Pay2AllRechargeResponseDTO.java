package com.chargenmarch.charge.dto;

/**
 * {"payid":"902898","operator_ref":"",
 * "status":"success","message":"Transaction Submitted Successfully Done, Check Status in Transaction Report, Thanks","txstatus_desc":"Pending"}
 * @author mukesh
 *
 */
public class Pay2AllRechargeResponseDTO {

	private String payid;
	private String operator_ref;
	private String status;
	private String message;
	private String txstatus_desc;
	
	public String getPayid() {
		return payid;
	}
	public void setPayid(String payid) {
		this.payid = payid;
	}
	public String getOperator_ref() {
		return operator_ref;
	}
	public void setOperator_ref(String operator_ref) {
		this.operator_ref = operator_ref;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTxstatus_desc() {
		return txstatus_desc;
	}
	public void setTxstatus_desc(String txstatus_desc) {
		this.txstatus_desc = txstatus_desc;
	}
}
