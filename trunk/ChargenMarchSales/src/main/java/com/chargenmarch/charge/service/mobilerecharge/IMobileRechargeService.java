package com.chargenmarch.charge.service.mobilerecharge;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.chargenmarch.charge.dto.funds.AddFundsRequestDTO;
import com.chargenmarch.charge.dto.mobilerecharge.Operator;
import com.chargenmarch.charge.dto.mobilerecharge.Plan;
import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.common.dto.user.UserDTO;

public interface IMobileRechargeService {
	public Operator getOperatorbyNumber(String mobileNumber);
	public void updatePlanListForAll();
	public Map<String, List<Plan>> getDistinctPlanTypes();
	public void updateMobilePlansLocalTypes(String[] type_local, String[] type/*, String[] operator_code*/);
	public RechargeDTO mobileDthAndDataCardRecharge(RechargeDTO rechargeDto,String orderId,UserDTO userDto);
	public Map<String, List<Plan>> getMobilePlansMapByOperator(String operatorZoneKey);
	public RechargeDTO setValuesInrechargeDto(int amount,UserDTO userDto,Operator operator,String mobileNumber,String orderId,RechargeDTO rechargeDto);
	public void saveOperatorNumberMapping(String mobileNumber, Operator operator);
	public Operator getOperatorByNumberFromDB(String mobileNumber);
	public List<RechargeDTO> getRechargeListByChargerId(String userId);
	public int saveMobileRechareDetails(RechargeDTO rechargeDto);
	public List<RechargeDTO> getRechargeCashbackListByChargerId(int chargerId);
	public void insertRechargeCashBack(UserDTO user,String rechargeIds,int cashback);
	public void updateRechargeCashBack(String rechargeIds);
	public void generateRechargeCashBackAndEMail();
	public void generateRechargeCashBackByUser(UserDTO userDto);
	public String getRechargeServiceName(HttpServletRequest request);
	public Operator createOperatorObject(RechargeDTO rechargeDto);
	public int addPostpaidBillExtraCharges(int rechargeAmount);
	public String getRechargeSecretKey();
	public List<RechargeDTO> getRechargeListByPGOrderId(String pgOrderId);
	public List<RechargeDTO> getRechargeListAfterDate(String date);
	public List<RechargeDTO> getRechargeListBeforeDate(String date);
	public List<AddFundsRequestDTO> getPendingFundsRequest();
	public int updateFundsRequestStatus(UserDTO user,String bankRefId);
}
