package com.chargenmarch.charge.joloapi.dto;

/**
 * {"status":"SUCCESS","error":"0","txid":"Z235661643773","clientid":"CAMRECH100513","service":"9838508994",
 * "amount":"20","time":"July 18 2016 07:42:42 PM","margin":"0.36","operatorid":"16440711906"}
 * @author mukesh
 *
 */
public class JOLORechargeAPIStatusDTO {

	private String status;
	private String error;
	private String txid;
	private String clientid;
	private String service;
	private int amount;
	private String time;
	private String margin;
	private String operatorid;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getTxid() {
		return txid;
	}
	public void setTxid(String txid) {
		this.txid = txid;
	}
	public String getClientid() {
		return clientid;
	}
	public void setClientid(String clientid) {
		this.clientid = clientid;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getMargin() {
		return margin;
	}
	public void setMargin(String margin) {
		this.margin = margin;
	}
	public String getOperatorid() {
		return operatorid;
	}
	public void setOperatorid(String operatorid) {
		this.operatorid = operatorid;
	}
}
