package com.chargenmarch.charge.joloapi.service;

import java.util.List;
import java.util.Map;

import com.chargenmarch.charge.dto.funds.AddFundsRequestDTO;
import com.chargenmarch.charge.dto.funds.RechargeMarginDTO;
import com.chargenmarch.charge.dto.mobilerecharge.Operator;
import com.chargenmarch.charge.dto.mobilerecharge.Plan;
import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.charge.joloapi.dto.JOLOCircle;
import com.chargenmarch.charge.joloapi.dto.JOLOOperator;
import com.chargenmarch.common.dto.user.UserDTO;

/**
 * 
 * @author mukesh
 *
 */
public interface IRechargeService {
	public Operator getOperatorDetailByNumber(String mobileNumber);
	public void saveOperatorNumberMapping(String mobileNumber,Operator operator);
	public RechargeDTO mobileDthAndDataCardRecharge(RechargeDTO rechargeDto,String orderId,UserDTO userDto);
	public List<JOLOOperator> getOperatorList(String serviceType);
	public List<JOLOCircle> getAllCircleCodeList();
	public Operator getOperatorByNumberFromDB(String mobileNumber);
	public void updatePlans();
	public Map<String, List<Plan>> getOperatorAndCircleWisePlans(String operatorCode, String circleCode);
	public JOLOOperator getJoloOperatorByCode(String code);
	public JOLOOperator getJoloOperatorByPayCode(String payCode);
	public JOLOCircle getCircleByCode(String code);
	public List<RechargeDTO> getRechargeList();
	public int calculatePaidAmountUserReferralAmountCase(int paidAmount);
	public void checkAndUpdateRechargeStatus();
	public int insertAddFundsRequest(AddFundsRequestDTO fundsRequestDTO);
	public List<AddFundsRequestDTO> getFundsRequestByChargersId(UserDTO user);
	public List<RechargeDTO> getSalesUserRechargeListByChargersId(UserDTO user,String dateoneDaysBefore);
	public void updateRechargeMarginForSalesUser();
	public void saveRechargeMarginLogs(UserDTO user, String date, int totalRechargeAmount, int marginAmount);
	public List<RechargeMarginDTO> getRecahrgeMarginByChargersId(UserDTO user);
	public boolean getSalesUserRechargeListByDate(String dateoneDaysBefore,UserDTO userDTO);
}
