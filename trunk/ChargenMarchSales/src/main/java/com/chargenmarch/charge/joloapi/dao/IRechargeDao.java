package com.chargenmarch.charge.joloapi.dao;

import java.util.List;

import com.chargenmarch.charge.dto.funds.AddFundsRequestDTO;
import com.chargenmarch.charge.dto.funds.RechargeMarginDTO;
import com.chargenmarch.charge.dto.mobilerecharge.Operator;
import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.charge.joloapi.dto.JOLOCircle;
import com.chargenmarch.charge.joloapi.dto.JOLOOperator;
import com.chargenmarch.charge.joloapi.dto.JoloErrorDTO;
import com.chargenmarch.charge.joloapi.query.Query;
import com.chargenmarch.common.dto.user.UserDTO;

/**
 * 
 * @author mukesh
 *
 */
public interface IRechargeDao {

	public List<JOLOOperator> getJOLOOperatorCodes();
	public List<JOLOOperator> getJOLOOperatorCodesByServiceType(String serviceType);
	public JOLOOperator getJOLOOperatorCodesByPayCode(String payCode);
	public JOLOOperator getJOLOOperatorCodesByCode(String code);
	public List<JOLOCircle> getJOLOCircleCodes();
	public Operator getOperatorByNumber(String mobileNumber);
	public void saveJoloOperatorNumberMapping(String mobileNumber, Operator operator);
	public void saveRechargeLogs(String rechargeService,String request,String response,String serviceType);
	public JOLOCircle getJOLOCircleCodesByCircleCode(String circleCode);
	public JoloErrorDTO getJOLOErrorDesByCode(String errorCode);
	public List<RechargeDTO> getRechargeList();
	public void updateRechargeSatus(RechargeDTO rechargeDto);
	public List<RechargeDTO> getSalesUserRechargeListByChargersId(UserDTO user,String dateoneDaysBefore);
	public void saveRechargeMarginLogs(UserDTO user, String date, int totalRechargeAmount, int marginAmount);
	public List<RechargeMarginDTO> getRechargeMarginLogs(UserDTO user);
	public boolean getSalesUserRechargeListByDate(String dateoneDaysBefore,UserDTO userDTO);
	public void updateJoloOperatorNumberMapping(String mobileNumber, Operator operator);
}
