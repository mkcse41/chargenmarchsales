package com.chargenmarch.charge.controller.paymentgateway;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.chargenmarch.charge.thread.PaymentTrackerThread;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.RequestConstants;
import com.chargenmarch.common.constants.ViewConstants;
import com.chargenmarch.common.constants.paymentgateway.PaymentConstants;
import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.chargenmarch.common.enums.paymentgateway.Payment;
import com.chargenmarch.common.forms.paymentgateway.PayUResponseForm;
import com.chargenmarch.common.forms.paymentgateway.PaymentRequestForm;
import com.chargenmarch.common.service.paymentgateway.IPaymentService;
import com.chargenmarch.common.service.user.IPaymentLogService;
import com.chargenmarch.common.utility.IUtility;

/**
 * This Action used to integration with payment gateway such as PayU. 
 * 1. Process PayU payment request.
 * 2. Validate payment response and redirect success, failure url.  
 * @author mukesh
 */

@Controller
@RequestMapping(value = RequestConstants.PAYMENT)
public class PaymentController {

	private static final String PAY_U_RESPONSE_FORM = "payUResponseForm";
	private final static Logger logger = LoggerFactory.getLogger(PaymentController.class);
	 @Autowired
	 IPaymentService paymentService;
	 
	 @Autowired
	 IUtility utility;
	 
	 @Autowired
	 ThreadPoolTaskExecutor taskExecutor;

	 @Autowired
	 PaymentTrackerThread tracker;

	 @Autowired
	 IPaymentLogService trackingService;
		
	 @RequestMapping(value = RequestConstants.PAYU, method = RequestMethod.GET)
	 public ModelAndView welcome(ModelMap model,HttpServletRequest request) {
		// logger.info("PaymentController");	
		 request.setAttribute(Constants.URL, utility.getBaseUrl()+RequestConstants.PAYMENT+RequestConstants.PROCESSED_PAYMENT);
		 return new ModelAndView(ViewConstants.PAY_U_LOWER);
	}
	 
	 
	 /**
     * Base url ==> http://<DomainName>/payment/processedPayment?firstName=mukesh&lastName=swami&emailId=mkcse41@gmail.com&mobileNo=9602646089&pincode=302013&city=jaipur&state=rajasthan&address=jaipur&productName=rsa&amount=200&pgType=PayU1
     * This method used to process PayU Request.
     * 1. Initialize Order info object for PayU Request.
     * 2. Get Service payment gateway wise.
     * 3. process payment.
     *    (i)   Generate order id.
     *    (ii)  Generate check sum code key|txnid|amount|productinfo|firstname|email||udf2||udf4|||||||SALT
     *    (iii) Validate required parameter.
     *    (iv)  if validate request is true then save payment order and payment logs, redirect payment gateway site
     */
    @RequestMapping(value = RequestConstants.PROCESSED_PAYMENT, method = RequestMethod.POST)
	public ModelAndView processedPayment(@ModelAttribute(PAY_U_RESPONSE_FORM) PaymentRequestForm paymentGatewayForm,HttpServletRequest request) throws Exception {
    	OrderInfo orderInfo = paymentService.initializeOrderInfoForRequest(paymentGatewayForm);
    	ModelAndView modelView = new ModelAndView(ViewConstants.PAYU_PAYMENT);
		orderInfo = paymentService.processPayment(orderInfo);
		if(orderInfo.getErrorStringList().size()==0){
			request.setAttribute(PaymentConstants.ORDERINFO, orderInfo);
			request.setAttribute(PaymentConstants.PAYMENT_ERROR_STRING_LIST, orderInfo.getErrorStringList());
			return modelView;
		}
		List<String> errorStringList = orderInfo.getErrorStringList();
		request.setAttribute(PaymentConstants.PAYMENT_ERROR_STRING_LIST, errorStringList);
		return modelView;
	}
	
	/**
	 * Base url ==> http://<DomainName>/payment/paymentResponse?<PayUResponseFormData>
	 * This Method used to
	 * 1. Initialize Order info object for PayU Response.
	 * 2. Validate payment order
	 * 3. if payment order is validate then update order status in payment order table.
	 * @return
	 */
    @RequestMapping(value = RequestConstants.PAYMENT_RESPONSE, method = RequestMethod.POST)
	public ModelAndView paymentResponse(@ModelAttribute(PAY_U_RESPONSE_FORM) PayUResponseForm payUResponseForm,HttpServletRequest request) throws Exception {
		OrderInfo orderInfo = paymentService.initializeOrderInfoForPayUResponse(payUResponseForm);
		orderInfo = paymentService.validatePaymentOrder(orderInfo);
		request.getSession().setAttribute(PaymentConstants.ORDERINFO, orderInfo);
		if(orderInfo.getErrorStringList().size()==0){
			if(orderInfo.getStatus().equalsIgnoreCase(Payment.SUCCESS.name()) &&  !orderInfo.getProductInfo().isRequiredResponse() ){
				return new ModelAndView(Constants.REDIRECT+Constants.COLON + orderInfo.getSuccessUrl());
			}else if( orderInfo.getStatus().equalsIgnoreCase(Payment.CANCEL.name()) &&  !orderInfo.getProductInfo().isRequiredResponse()){
				return new ModelAndView(Constants.REDIRECT+Constants.COLON + orderInfo.getCancelUrl());
			}else if( orderInfo.getStatus().equalsIgnoreCase(Payment.FAILURE.name()) &&  !orderInfo.getProductInfo().isRequiredResponse() ){
				return new ModelAndView(Constants.REDIRECT+Constants.COLON + orderInfo.getFailureUrl());
			}
		}
		String url = getRedirectUrl(orderInfo);
		ModelAndView modelView = new ModelAndView(ViewConstants.PAYMENT_RESPONSE);
		request.setAttribute(PaymentConstants.ERROR_STRING_LIST, orderInfo.getErrorStringList());
		request.setAttribute(PaymentConstants.RESPONSE,orderInfo.getResponsejson());
		request.setAttribute(PaymentConstants.URL, url);
		return modelView;
 
	}
    
    /**
     * This method used to 
     * Get redirect url basis of payment gateway response 
     * 1.) If payment is success then get payment success url. and same as failure and cancel payment case.
     * @param orderInfo
     * @return
     */
    private String getRedirectUrl(OrderInfo orderInfo){
    	String url = Constants.BLANK;
    	if(orderInfo.getStatus().equalsIgnoreCase(Payment.SUCCESS.name())){
			url = orderInfo.getSuccessUrl();
		}else if( orderInfo.getStatus().equalsIgnoreCase(Payment.CANCEL.name())){
			url = orderInfo.getCancelUrl();
		}else if( orderInfo.getStatus().equalsIgnoreCase(Payment.FAILURE.name())){
			url = orderInfo.getFailureUrl();
		}
    	logger.info("PaymentController==>Url=>"+url);
    	return url;
    }
    
	
}
