package com.chargenmarch.charge.dao.mobilerecharge.impl;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.WebApplicationContext;

import com.chargenmarch.charge.dao.mobilerecharge.IMobileRechargeDao;
import com.chargenmarch.charge.dto.funds.AddFundsRequestDTO;
import com.chargenmarch.charge.dto.mobilerecharge.Operator;
import com.chargenmarch.charge.dto.mobilerecharge.Pay2AllServiceProvider;
import com.chargenmarch.charge.dto.mobilerecharge.Plan;
import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.charge.joloapi.mapper.FundsRequestMapper;
import com.chargenmarch.charge.joloapi.query.Query;
import com.chargenmarch.charge.mapper.mobilerecharge.MobilePlanMapper;
import com.chargenmarch.charge.mapper.mobilerecharge.OperatorMapper;
import com.chargenmarch.charge.mapper.recharge.Pay2AllServiceProviderMapper;
import com.chargenmarch.charge.mapper.recharge.RechargeMapper;
import com.chargenmarch.charge.query.mobilerecharge.MobileRechargeQueries;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.dto.user.UserDTO;

@Repository
public class MobileRechargeDaoImpl implements IMobileRechargeDao{

	@Autowired
	JdbcTemplate jdbcTemplate;
	@Autowired
	WebApplicationContext context;
	
	@Override
	public int updatePlansListinDB(String operatorName, String zoneName,
			String amount, String validity, String description, String type ) {
		Object[] args = {operatorName, zoneName, amount, validity, description, type};
		return jdbcTemplate.update(MobileRechargeQueries.INSERT_MOBILE_PLAN_DETAILS, args);
	}

	@Override
	public void truncateMobilePlans() {
		jdbcTemplate.update(MobileRechargeQueries.TRUNCATE_MOBILE_PLAN_DETAILS);
	}
	@Override
	public List<Plan> getDistinctPlanTypes(){
		return jdbcTemplate.query(MobileRechargeQueries.SELECT_DISTINCT_RECHARGE_TYPES, new MobilePlanMapper());
	}
	@Override
	public void updateMobilePlansLocalTypes(String type_local, String type/*, String operator_code*/){
		Object[] args = {type_local, type/*, operator_code*/};
		jdbcTemplate.update(MobileRechargeQueries.UPDATE_MOBILE_PLANS_LOCAL_TYPE, args);
	}
	@Override
	public List<Plan> getMobilePlansByTypeOperatorAndZone(String type, String operatorName, String zone){
		Object[] args = {type, operatorName, zone};
		return jdbcTemplate.query(MobileRechargeQueries.SELECT_MOBILE_PLANS_BY_TYPE_AND_ZONE_AND_OPERATOR, args, new MobilePlanMapper());
	}

	public int saveMobileRechargeDetails(final RechargeDTO rechargeDto){
		try{
			KeyHolder holder = new GeneratedKeyHolder();
			jdbcTemplate.update(new PreparedStatementCreator() {           
		           @Override
		           public PreparedStatement createPreparedStatement(Connection connection)
		                        throws SQLException {
		                    PreparedStatement ps = connection.prepareStatement(MobileRechargeQueries.INSERT_MOBILE_RECHARGE_DETAILS, Statement.RETURN_GENERATED_KEYS);
		                    ps.setInt(1, rechargeDto.getUserDto().getId());
		                    ps.setString(2, rechargeDto.getNumber());
		                    ps.setString(3, rechargeDto.getOperator().getOperator());
		                    ps.setInt(4, rechargeDto.getAmount());
		                    ps.setInt(5, rechargeDto.getIsSuccesfulRecharge());
		                    ps.setString(6, rechargeDto.getRechargeOrderId());
		                    ps.setString(7, rechargeDto.getOrderId());
		                    ps.setString(8, rechargeDto.getServiceType());
		                    ps.setInt(9, rechargeDto.getIsQuickRecharge());
		                    ps.setInt(10, rechargeDto.getInternetCharges());
		                    ps.setString(11, rechargeDto.getRechargeSource());
		                    ps.setString(12, rechargeDto.getRechargeServiceName());
		                    ps.setString(13, rechargeDto.getSessionId());
		                    ps.setInt(14, rechargeDto.getUserDto().getIsSalesUser());
		                    ps.setString(15, rechargeDto.getMobileService());
		                    return ps;
		                }
		            }, holder);
			
			return holder.getKey().intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	public void saveOperatorNumberMapping(String mobileNumber, Operator operator){
		Object args[] = {mobileNumber, operator.getOperator(), operator.getPay_code(), operator.getCode(), operator.getCircle(), operator.getPay_circle_code(), operator.getCircle_code()};
		jdbcTemplate.update(MobileRechargeQueries.INSERT_INTO_NUMBER_OPERATOR_MAPPING, args);
	}
	public Operator getOperatorByNumber(String mobileNumber){
		try{
			Object args[] = {mobileNumber};
			return jdbcTemplate.queryForObject(MobileRechargeQueries.SELECT_OPERATOR_BY_NUMBER, args, new OperatorMapper());
		}catch(EmptyResultDataAccessException e){
			return null;
		}
	}
	
	public List<RechargeDTO> getRechargeListByChargerId(String chargerId){
		Object args[] = {chargerId};
		return jdbcTemplate.query(MobileRechargeQueries.SELECT_RECHARGE_DETAILS_BY_CHARGERID, args, context.getBean(RechargeMapper.class));
	}
	
	public List<RechargeDTO> getRechargeCashbackListByChargerId(int chargerId){
		Object args[] = {chargerId};
		return jdbcTemplate.query(MobileRechargeQueries.SELECT_RECHARGE_CASHBACK_BY_CHARGERID, args, context.getBean(RechargeMapper.class));
	}
	
	public void insertRechargeCashBack(UserDTO user,String rechargeIds,int cashback){
		Object[] args = {user.getId(),rechargeIds,cashback};
		jdbcTemplate.update(MobileRechargeQueries.INSERT_RECHARGE_CASHBACK, args);
	}
	
	public void updateRechargeCashBack(String rechargeIds) {
		//Object[] args = {Constants.OPEN_BRACES + rechargeIds + Constants.CLOSE_BRACES};
		String query = MobileRechargeQueries.UPDATE_RECHARGE_CASHBACK_DETAILS.replace("%IDS%", rechargeIds);
		jdbcTemplate.update(query);
	}
	
	@Override
	public Pay2AllServiceProvider getPay2AllServiceProviderById(int providerId){
		Object args[] = {providerId};
		List<Pay2AllServiceProvider> providerList = jdbcTemplate.query(MobileRechargeQueries.PAY2ALL_SERVICE_PROVIDERS, args, new Pay2AllServiceProviderMapper());
		return providerList.size()>0 ? providerList.get(0) : null;
	}
	
	@Override
	public Pay2AllServiceProvider getPay2AllServiceProviderByProviderCode(String providerCode){
		Object args[] = {providerCode};
		List<Pay2AllServiceProvider> providerList = jdbcTemplate.query(MobileRechargeQueries.PAY2ALL_SERVICE_PROVIDERS_BY_CODE, args, new Pay2AllServiceProviderMapper());
		return providerList.size()>0 ? providerList.get(0) : null;
	}
	
	public List<RechargeDTO> getRechargeListByDate(String date){
		Object args[] = {Constants.PERCENT+date+Constants.PERCENT};
		return jdbcTemplate.query(MobileRechargeQueries.SELECT_RECHARGE_DETAILS_BY_DATE, args, context.getBean(RechargeMapper.class));
	}
	
	public List<RechargeDTO> getRechargeListByPGOrderId(String pgOrderId){
		Object args[] = {pgOrderId};
		return jdbcTemplate.query(MobileRechargeQueries.SELECT_RECHARGE_DETAILS_BY_PGORDERID, args, context.getBean(RechargeMapper.class));
	}
	
	public List<RechargeDTO> getRechargeListAfterDate(String date){
		Object args[] = {date};
		return jdbcTemplate.query(MobileRechargeQueries.SELECT_RECHARGE_AFTER_DATE, args, context.getBean(RechargeMapper.class));
	}
	public List<RechargeDTO> getRechargeListBeforeDate(String date){
		Object args[] = {date};
		return jdbcTemplate.query(MobileRechargeQueries.SELECT_RECHARGE_BEFORE_DATE, args, context.getBean(RechargeMapper.class));
	}
	
	public int addFundsRequest(AddFundsRequestDTO fundsRequestDTO){
		if(checkDuplicateFundsRequest(fundsRequestDTO).isEmpty()){
			Object args[] = {fundsRequestDTO.getUser().getId(),fundsRequestDTO.getAmount(), fundsRequestDTO.getPaymentMethod(), fundsRequestDTO.getBankName(), fundsRequestDTO.getBankReferenceId(),fundsRequestDTO.getRemarks()};
			return jdbcTemplate.update(Query.ADD_FUNDS_REQUEST_QUERY, args);
		}
		return 0;
	}
	
	private List<AddFundsRequestDTO> checkDuplicateFundsRequest(AddFundsRequestDTO fundsRequestDTO){
		Object args[] = {fundsRequestDTO.getUser().getId(),fundsRequestDTO.getBankReferenceId()};
		return jdbcTemplate.query(Query.SELECT_FUNDS_REQUEST_BY_BANK_REF_ID, args, context.getBean(FundsRequestMapper.class));
	}
	
	private List<AddFundsRequestDTO> checkFundsRequestStatus(UserDTO user,String bankRefId){
		Object args[] = {user.getId(),bankRefId};
		return jdbcTemplate.query(Query.SELECT_FUNDS_REQUEST_BY_STATUS_BANK_REF_ID, args, context.getBean(FundsRequestMapper.class));
	}
	
	public List<AddFundsRequestDTO> getFundsRequestByChargersId(UserDTO user){
		Object args[] = {user.getId()};
		return jdbcTemplate.query(Query.SELECT_FUNDS_REQUEST_BY_CHARGERS_ID, args, context.getBean(FundsRequestMapper.class));
	}
	
	public List<AddFundsRequestDTO> getPendingFundsRequest(){
		return jdbcTemplate.query(Query.SELECT_PENDING_FUNDS_REQUEST, context.getBean(FundsRequestMapper.class));
	}
	
	public int updateFundsRequestStatus(UserDTO user,String bankRefId){
		if(checkFundsRequestStatus(user,bankRefId).isEmpty()){
			Object args[] = {user.getId(),bankRefId};
			return jdbcTemplate.update(Query.UPDATE_FUNDS_REQUEST_QUERY, args);
		}
		return 0;
	}
}
