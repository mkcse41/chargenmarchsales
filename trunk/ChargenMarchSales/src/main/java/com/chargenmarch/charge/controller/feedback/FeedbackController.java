package com.chargenmarch.charge.controller.feedback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.chargenmarch.charge.dto.feedback.FeedbackDto;
import com.chargenmarch.charge.service.feedback.IFeedbackService;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.RequestConstants;
import com.chargenmarch.common.validator.user.UserDetailValidator;
import com.google.gson.JsonObject;


@Controller
@RequestMapping(value =  RequestConstants.FEEDBACK)
public class FeedbackController {
	private final static Logger logger = LoggerFactory.getLogger(FeedbackController.class);
	
	public static final String FEEDBACK_DTO = "feedbackDto";
	
	@Autowired
	private IFeedbackService feedbackService;
	
	@Autowired
	private UserDetailValidator userdetailValidator;
 
	@RequestMapping(value=RequestConstants.FEEDBACK_HTM, method = RequestMethod.POST)
	@ResponseBody
	public String userFeedback(@ModelAttribute(FEEDBACK_DTO) FeedbackDto feedbackDto){
		JsonObject response = new JsonObject();
		
		int feedbackStatus = feedbackService.addFeedback(feedbackDto); 
			if(feedbackStatus == 1){
				response.addProperty(Constants.MESSAGE,"Feedback Submitted Successfully");
			}else{
				response.addProperty(Constants.MESSAGE,"Feedback Not Submitted Successfully");
			}
		return response.toString();
	}

}
