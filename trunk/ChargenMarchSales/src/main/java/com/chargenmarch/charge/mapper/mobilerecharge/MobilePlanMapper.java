package com.chargenmarch.charge.mapper.mobilerecharge;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.charge.dto.mobilerecharge.Plan;
import com.chargenmarch.charge.enums.recharge.OperatorPayCodes;
import com.chargenmarch.charge.enums.recharge.ZoneCodes;
import com.chargenmarch.common.constants.Constants;

public class MobilePlanMapper implements RowMapper<Plan>{

	@Override
	public Plan mapRow(ResultSet rs, int rowNum) throws SQLException {
		Plan plan = new Plan();
		
		if(hasColumn(rs, Constants.ID))
			plan.setId(rs.getInt(Constants.ID));
		/*if(hasColumn(rs, Constants.OPERATOR+Constants.UNDERSCORE+Constants.CODE))
			plan.setOperator(rs.getString(Constants.OPERATOR+Constants.UNDERSCORE+Constants.CODE));
		if(hasColumn(rs, Constants.ZONE+Constants.UNDERSCORE+Constants.CODE))
			plan.setZone(ZoneCodes.valueOf(rs.getString(Constants.ZONE+Constants.UNDERSCORE+Constants.CODE)));*/
		if(hasColumn(rs, Constants.TYPE))
			plan.setType(rs.getString(Constants.TYPE));
		if(hasColumn(rs, Constants.AMOUNT))
			plan.setAmount(rs.getInt(Constants.AMOUNT));
		if(hasColumn(rs, Constants.VALIDITY))
			plan.setValidity(rs.getString(Constants.VALIDITY));
		if(hasColumn(rs, Constants.DESCRIPTION))
			plan.setDescription(rs.getString(Constants.DESCRIPTION));
		/*if(hasColumn(rs, Constants.TYPE_LOCAL))
			plan.setType_local(rs.getString(Constants.TYPE_LOCAL));*/
		return plan;
	}
	
	private boolean hasColumn(ResultSet rs, String columnName) throws SQLException {
	    ResultSetMetaData rsmd = rs.getMetaData();
	    int columns = rsmd.getColumnCount();
	    for (int x = 1; x <= columns; x++) {
	        if (columnName.equals(rsmd.getColumnName(x))) {
	            return true;
	        }
	    }
	    return false;
	}
}
