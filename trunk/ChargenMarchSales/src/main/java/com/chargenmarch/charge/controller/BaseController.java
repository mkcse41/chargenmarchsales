package com.chargenmarch.charge.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.chargenmarch.charge.constants.recharge.RechargeConstants;
import com.chargenmarch.charge.enums.recharge.DTHOperatorCodes;
import com.chargenmarch.charge.enums.recharge.DatacardOperatorCodes;
import com.chargenmarch.charge.enums.recharge.PostpaidOperatorCodes;
import com.chargenmarch.charge.enums.recharge.PrepaidOperatorCodes;
import com.chargenmarch.charge.enums.recharge.StateCodes;
import com.chargenmarch.charge.joloapi.enums.billing.LandlineOperatorCodes;
import com.chargenmarch.charge.joloapi.service.IRechargeService;
import com.chargenmarch.charge.service.mobilerecharge.IMobileRechargeService;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.RequestConstants;
import com.chargenmarch.common.constants.SessionConstants;
import com.chargenmarch.common.constants.ViewConstants;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.service.user.IUserService;
import com.chargenmarch.common.utility.IUtility;

@Controller
public class BaseController {

	@Autowired
	IUserService userService;
	
	@Autowired 
	IUtility utility;
	
	@Autowired
	IMobileRechargeService mobileRechargeService;
	
	@Autowired
	IRechargeService rechargeService;
	
	/*@Autowired
	ReportingCrons cron;*/
	
	@RequestMapping(value = "/", method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.HEAD})
	public ModelAndView welcome(ModelMap model,HttpServletRequest request,HttpServletResponse response) throws IOException {
		ModelAndView mnv = new ModelAndView(utility.getviewResolverName(request,ViewConstants.CAM_SALES_LOGIN));
		if(null!=request.getSession().getAttribute(SessionConstants.USER_BEAN)){
			UserDTO user = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
			request.setAttribute(Constants.USER, user);
			setOperatorCodeValues(model, request);
			removeSessionAttributes(request);
			response.sendRedirect(utility.getBaseUrl() + ViewConstants.SLASH + RequestConstants.RECHARGE + ViewConstants.SLASH + Constants.HOME);
		    return null;
		}
		if(isRedirectBaseUrl(request)){
			response.sendRedirect(utility.getBaseUrl());
		    return null;
		}
		return mnv;
	}

   /**
    * This method used to get Recharge service Name then set recharge service wise operator details..
    * @param model
    * @param request
    */
   private void setOperatorCodeValues(ModelMap model,HttpServletRequest request){
	   if (mobileRechargeService.getRechargeServiceName(request).equalsIgnoreCase(Constants.JOLO)) {
		    model.addAttribute(RechargeConstants.PREPAID_OPERATORS, rechargeService.getOperatorList(Constants.PREPAID));
			model.addAttribute(RechargeConstants.POSTPAID_OPERATORS, rechargeService.getOperatorList(Constants.POSTPAID));
			model.addAttribute(RechargeConstants.DTH_OPERATORS, rechargeService.getOperatorList(Constants.DTH));
			model.addAttribute(RechargeConstants.DATACARD_OPERATORS, rechargeService.getOperatorList(Constants.DATACARD));
			model.addAttribute(RechargeConstants.CIRCLES, rechargeService.getAllCircleCodeList());
	   }else{
		    model.addAttribute(RechargeConstants.PREPAID_OPERATORS, PrepaidOperatorCodes.values());
			model.addAttribute(RechargeConstants.POSTPAID_OPERATORS, PostpaidOperatorCodes.values());
			model.addAttribute(RechargeConstants.DTH_OPERATORS, DTHOperatorCodes.values());
			model.addAttribute(RechargeConstants.DATACARD_OPERATORS, DatacardOperatorCodes.values());
			model.addAttribute(RechargeConstants.CIRCLES, StateCodes.values());
	   }
	   model.addAttribute(RechargeConstants.RECHARGE_SERVICE_NAME, mobileRechargeService.getRechargeServiceName(request));
	   model.addAttribute(RechargeConstants.LANDLINE_OPERATORS, LandlineOperatorCodes.values());
   }
	
   private  boolean isRedirectBaseUrl(HttpServletRequest request) {
		 String requestUrl = request.getRequestURL().toString();
		 if(null!=requestUrl && requestUrl.equalsIgnoreCase(utility.getBaseUrl() + RechargeConstants.SLASH)){
			 return false;
		 }
		 return true;
	}
	
	
	private void removeSessionAttributes(HttpServletRequest request){
		request.getSession().removeAttribute(RequestConstants.MOBILE_RECHARGE_DTO);
		request.getSession().removeAttribute(SessionConstants.RECHARGE_FORM);
		request.getSession().removeAttribute(RequestConstants.LANDLINE_RECHARGE_DTO);
		request.getSession().removeAttribute(SessionConstants.LANDLINE_FORM);
	}
	
	@RequestMapping(value = "/contactus", method = RequestMethod.GET)
	public ModelAndView contactus(ModelMap model,HttpServletRequest request) {
		ModelAndView mnv = new ModelAndView(ViewConstants.CONTACTUS);
		return mnv;
	}
	
	@RequestMapping(value = "/privacy-policy", method = RequestMethod.GET)
	public ModelAndView privacyPolicy(ModelMap model,HttpServletRequest request) {
		ModelAndView mnv = new ModelAndView(ViewConstants.PRIVACY_POLICY);
		return mnv;
	}
	
	@RequestMapping(value = "/termsandcondition", method = RequestMethod.GET)
	public ModelAndView termsAndCondition(ModelMap model,HttpServletRequest request) {
		ModelAndView mnv = new ModelAndView(ViewConstants.TNC);
		return mnv;
	}
	
	/*@RequestMapping(value = "/daily", method = RequestMethod.GET)
	public void daily(ModelMap model,HttpServletRequest request) {
		cron.dailyReportMailer();
	}*/
}