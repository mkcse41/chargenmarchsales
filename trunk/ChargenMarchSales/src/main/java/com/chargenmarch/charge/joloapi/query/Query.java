package com.chargenmarch.charge.joloapi.query;

public class Query {
	 public static final String SELECT_JOLO_OPERATOR_CODE_DETAILS = "select * from jolo_operator_mapping order by operatorName";
	 public static final String SELECT_JOLO_OPERATOR_CODE_DETAILS_BY_SERVICE_TYPE = "select * from jolo_operator_mapping WHERE recharge_service_type=? order by operatorName";
	 public static final String SELECT_JOLO_OPERATOR_CODE_DETAILS_BY_PAY_CODE = "select * from jolo_operator_mapping WHERE pay_code=?";
	 public static final String SELECT_JOLO_OPERATOR_CODE_DETAILS_BY_CODE = "select * from jolo_operator_mapping WHERE code=?";
	 public static final String SELECT_JOLO_CIRCLE_CODE_DETAILS = "select * from jolo_circle_mapping order by circlename";
	 public static final String SELECT_JOLO_CIRCLE_CODE_DETAILS_BY_CODE = "select * from jolo_circle_mapping where circle_code=?";
	 public static final String SELECT_JOLO_NUMBER_OPERATOR_MAPPING = "select * from jolo_number_operator_mapping where mobilenumber=?";
	 public static final String INSERT_INTO_NUMBER_OPERATOR_MAPPING = "insert into jolo_number_operator_mapping values(null,?,?,?,?,?,?,?, now(), now())";
	 public static final String INSERT_INTO_RECHARGE_LOGS = "insert into recharge_logs set rechargeService=?,request=?,response=?,serviceType=?,insertTimestamp=now(),updateTimestamp=now()";
	 public static final String SELECT_JOLO_ERROR_DES_BY_CODE = "select * from jolo_error_code WHERE errorcode=?";
	 public static final String SELECT_FROM_RECHARGE_INTERVAL = "select * from recharge where inserttimestamp >= NOW() - INTERVAL 25 MINUTE and isSuccessfullRecharge = 1";
	 public static final String GET_USER_REFERRAL_RECHARGE_AMOUNT_BY_USERID = "select sum(rechargeAmount) from recharge where chargers_id=? and inserttimestamp > '2016-07-17 00:00:00' and isSuccessfullRecharge=1 and PGOrderId not like '%CAMRECH%'";
	 public static final String UPDATE_RECHARGE_STATUS = "update recharge set isSuccessfullRecharge = ?,updatetimestamp=now() where id=?";
	 public static final String ADD_FUNDS_REQUEST_QUERY = "insert into CAMFundsRequest set charger_id=?,amount=?,paymentmethod=?,bankname=?,bankReferenceId=?,remark=?, timestamp=now(),updatetimestamp=now()";
	 public static final String SELECT_FUNDS_REQUEST_BY_BANK_REF_ID = "select * from CAMFundsRequest WHERE charger_id=? and bankReferenceId=?";
	 public static final String SELECT_FUNDS_REQUEST_BY_CHARGERS_ID = "select * from CAMFundsRequest WHERE charger_id=?";
	 public static final String SELECT_PENDING_FUNDS_REQUEST = "select * from CAMFundsRequest WHERE status='PENDING'";
	 public static final String SELECT_FUNDS_REQUEST_BY_STATUS_BANK_REF_ID = "select * from CAMFundsRequest WHERE charger_id=? and bankReferenceId=? and status='SUCCESS'";
	 public static final String UPDATE_FUNDS_REQUEST_QUERY = "update CAMFundsRequest set status='SUCCESS',updatetimestamp=now() where charger_id=? and bankReferenceId=?";
	 public static final String SELECT_FROM_SALES_USER_RECHARGE_LIST = "select * from recharge where chargers_id = ? and isSalesUser = 1 and isSuccessfullRecharge = 1 and (rechargeService in ('DTH','DataCard') or (rechargeService='Mobile' and mobileServiceType='prepaid')) and inserttimestamp like ?";
	 public static final String INSERT_INTO_RECHARGE_MARGIN_LOGS = "insert into recharge_margin_logs set charger_id=?,totalRechargeAmount=?,marginAmount=?,margindate=?,timestamp=now(),updatetimestamp=now()";
	 public static final String SELECT_FROM_RECHARGE_MARGIN_LOGS_BY_CHARGER_ID = "select * from recharge_margin_logs where charger_id=? order by margindate DESC";
	 public static final String SELECT_FROM_RECHARGE_MARGIN_LOGS_BY_DATE = "select * from recharge_margin_logs where margindate=? and charger_id=?";
	 public static final String UPDATE_NUMBER_OPERATOR_MAPPING = "update jolo_number_operator_mapping set operator=?,pay_code=?,code=?,circle=?,pay_circle_code=?,circle_code=?,updatedate=now() where mobilenumber=?";

}
