package com.chargenmarch.charge.dao.rewardspoint.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.chargenmarch.charge.dao.rewardspoint.IRewardsPointDao;
import com.chargenmarch.charge.dto.rewardspoint.RewardsPoint;
import com.chargenmarch.charge.mapper.rewardspoint.RewardsPointMapper;
import com.chargenmarch.charge.query.rewardspoint.RewardsPointQuery;

@Repository
public class RewardsPointDaoImpl implements IRewardsPointDao{
    
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	
	public int insertRewardsPoint(RewardsPoint rewardsPoint) {
		List<RewardsPoint> rewardsPointList = getRewardsPointById(rewardsPoint);
		if(rewardsPointList.isEmpty()){
			Object args[]= new Object[] {rewardsPoint.getUserDto().getId(),rewardsPoint.getRewardsPoint()};
			jdbcTemplate.update(RewardsPointQuery.INSERT_REWARDS_POINT, args);
			rewardsPointList = getRewardsPointById(rewardsPoint);
			return rewardsPointList.size()>0?rewardsPointList.get(0).getRewards_point_id():0;
		}else{
			updateRewardsPoint(rewardsPoint);
			return rewardsPointList.size()>0?rewardsPointList.get(0).getRewards_point_id():0;
		}
	}
	
	public void insertRewardsPointLogs(RewardsPoint rewardsPoint) {
		Object args[]= new Object[] {rewardsPoint.getRewards_point_id(),rewardsPoint.getRecharge_amount(),rewardsPoint.getEarnReswardsPoint(),rewardsPoint.getRechargeId()};
		jdbcTemplate.update(RewardsPointQuery.INSERT_REWARDS_POINT_LOGS, args);
	}
	
	public void insertReedomRewardsPointLogs(RewardsPoint rewardsPoint) {
		Object args[]= new Object[] {rewardsPoint.getUserDto().getId(),rewardsPoint.getRewardsPoint(),rewardsPoint.getRewardsPoint_value()};
		jdbcTemplate.update(RewardsPointQuery.INSERT_REEDOM_REWARDS_POINT_LOGS, args);
	}
	
	public void updateRewardsPoint(RewardsPoint rewardsPoint) {
		Object args[]= new Object[] {rewardsPoint.getRewardsPoint(),rewardsPoint.getUserDto().getId()};
		jdbcTemplate.update(RewardsPointQuery.UPDATE_REWARDS_POINT, args);
	}
	
	public List<RewardsPoint> getRewardsPoint() {
		return (List) jdbcTemplate.query(RewardsPointQuery.SELECT_REWARDS_POINT,new RewardsPointMapper());
	}
	
	public List<RewardsPoint> getRewardsPointById(RewardsPoint rewardsPoint) {
		Object args[]= new Object[] {rewardsPoint.getUserDto().getId()};
		return (List) jdbcTemplate.query(RewardsPointQuery.SELECT_REWARDS_POINT_BY_ID,args,new RewardsPointMapper());
	}
	
}
