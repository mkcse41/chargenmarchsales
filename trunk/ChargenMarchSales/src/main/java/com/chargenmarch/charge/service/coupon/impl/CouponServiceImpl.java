package com.chargenmarch.charge.service.coupon.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chargenmarch.charge.constants.coupon.CouponConstants;
import com.chargenmarch.charge.dao.coupon.ICouponDao;
import com.chargenmarch.charge.dto.coupon.CouponDTO;
import com.chargenmarch.charge.forms.coupon.CouponForm;
import com.chargenmarch.charge.service.coupon.ICouponService;
import com.chargenmarch.common.dto.user.UserDTO;

/**
 * This service used to check coupon is exist or not,coupon valid or not and reedom coupon,maintain reedom coupon logs.
 * @author mukesh
 *
 */
@Service("couponService")
public class CouponServiceImpl implements ICouponService {
    
	private final static Logger logger = LoggerFactory.getLogger(CouponServiceImpl.class);
	private Map<String,CouponDTO> couponInfoMap = null;
	
	@Autowired
	ICouponDao couponDao;
	
	@PostConstruct
	public void init(){
		logger.info("Coupon Service Initilization");
		couponInfoMap = new HashMap<String,CouponDTO>();
		loadCouponData();
	}
	
	/**
	 * load all active coupon name. 
	 */
	private void loadCouponData(){
		List<CouponDTO> couponList = couponDao.getCouponDetail();
		for (CouponDTO couponDTO : couponList) {
			if(null!=couponDTO){
				String couponKey = couponDTO.getCouponName() + couponDTO.getCouponServiceType().toLowerCase();
				couponInfoMap.put(couponKey, couponDTO);
			}
		}
	}
	
	/*
	 * Get coupon details by coupon name.
	 * 1. if coupon name is exists in Coupon name then return couponDTo object,
	 * 2. if coupon name is not exists in couponInfo map then query in dB by coupon name
	 * 3. if any case not exists in DB and map then return null value.
	 */
	public CouponDTO getCouponDetailByCouponName(CouponForm couponForm){
		if(null!=couponForm){
			String couponKey = couponForm.getCouponName() + couponForm.getCouponServiceType().toLowerCase();
			if(couponInfoMap.containsKey(couponKey)){
				return couponInfoMap.get(couponKey);
			}else{
				logger.info("Coupon not exists in coupon info Map");
				List<CouponDTO> couponList = couponDao.getCouponDetailByCouponName(couponForm.getCouponName(),couponForm.getCouponServiceType().toLowerCase());
				return couponList.isEmpty()?null:couponList.get(0);
			}
		}
		return null;
	}
	
	/**
	 * Reedom coupon by coupon name
	 * 1. First of all check coupon is exist or not.
	 * 2. isValidCoupon : In valid Coupon :- Check coupon limit is available for this user and coupon limit is not exceeds.
	 * 3. isCouponApplicable : coupon type is match coupon service type and coupon limit is zero then return  true,
	 *  if coupon limit is not zero then check coupon uses limit is not exceeds coupon limit,if not exceeds then return true.
	 * 4. isValidCouponForUser : if coupon per user limit is zero then return true.
	 *  (i) if coupon per user limit is not zero then check coupon apply count check in DB
	 */
	public CouponDTO reedomCoupon(UserDTO userDto,CouponForm couponForm){
		try{
			if(null!=couponForm){
				CouponDTO couponDto = getCouponDetailByCouponName(couponForm);
				if(null!=userDto && isValidCoupon(couponDto, userDto, couponForm.getAmount(), couponForm.getCouponServiceType())){
					couponDto.setCouponApply(true);
					if(couponDto.getCouponType().equals(CouponConstants.FIXED)){
						  if(couponDto.getCouponValue()>couponDto.getMaximumcashback()){
							  couponDto.setCouponReedomValue(couponDto.getMaximumcashback());
						  }else{
						      couponDto.setCouponReedomValue(couponDto.getCouponValue());
					      }
					}else if(couponDto.getCouponType().equals(CouponConstants.PERCENTAGE)){
						  double couponAmount = (couponForm.getAmount()*couponDto.getCouponValue())/100;
						  if(couponAmount>couponDto.getMaximumcashback()){
							  couponDto.setCouponReedomValue(couponDto.getMaximumcashback());
						  }else{
						      couponDto.setCouponReedomValue(couponAmount);
					      }
					}
					//insert reedom coupon logs.
					if(couponForm.getIsCouponLogInsert()==1){ // commented code for maintain recharge failure cases.
					     couponDao.insertReedomCouponLogs(couponDto, userDto, 0);
					}
					return couponDto;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return setErrorMeesge("Coupon not valid.");
	}
	
	private CouponDTO setErrorMeesge(String message){
		CouponDTO couponDto = new CouponDTO();
		couponDto.setErrorMessage(message);
		return couponDto;
	}
	/**
	 * create CouponDTO object and insert coupon details in DB. 
	 */
	public CouponDTO insertCouponDetails(CouponForm couponForm){
		if(null!=couponForm){
			CouponDTO couponDto = setCouponDTOObject(couponForm);
			couponDao.insertCouponDetail(couponDto);
			loadCouponData();
			couponDto = getCouponDetailByCouponName(couponForm);
			return couponDto;
		}
		return setErrorMeesge("Required field missing.");
	}
	
	/**
	 * In valid Coupon :- Check coupon limit is available for this user and coupon limit is not exceeds.
	 */
	private boolean isValidCoupon(CouponDTO couponDto,UserDTO userDto,double amount,String couponType){
		if(null!=couponDto){
			 if(isCouponApplicable(couponDto, amount, couponType) && isValidCouponForUser(userDto, couponDto)){
				 return true;
			 }
		}
		return false;
	}
	
	/**
	 * isValidCouponForUser : if coupon per user limit is zero then return true.
	 *  (i) if coupon per user limit is not zero then check coupon apply count check in DB
	 */
	private boolean isValidCouponForUser(UserDTO userDto,CouponDTO couponDto){
		 if(couponDto.getCouponPerUserLimit()==0){
			 return true;
		 }else if(couponDto.getCouponPerUserLimit()>0 && couponDao.getCouponUsedCountByUser(userDto, couponDto).size()<couponDto.getCouponPerUserLimit()){
			 return true;
		 }else{
			 couponDto.setErrorMessage("Coupon limit is Exceeds for this customer.");
		 }
		 return false;
	}
	
	/**
	 * coupon type is match coupon service type and coupon limit is zero then return  true,
	 *  if coupon limit is not zero then check coupon uses limit is not exceeds coupon limit,if not exceeds then return true.
	 */
	private boolean isCouponApplicable(CouponDTO couponDto,double amount,String couponType){
		 if(couponDto.getCouponLimit()==0 && couponDto.getCouponServiceType().equalsIgnoreCase(couponType) 
				 && amount>=couponDto.getMinimumRechargeValue()){
			 return true;
		 }else if(couponDto.getCouponLimit() > 0 && couponDto.getCouponLimit()>couponDto.getCouponUses()
				 && couponDto.getCouponServiceType().equalsIgnoreCase(couponType) && amount>=couponDto.getMinimumRechargeValue()){
			 return true;
		 }else{
			 couponDto.setErrorMessage("Coupon limit is Exceeds."); 
		 }
		 return false;
	}
	
	private CouponDTO setCouponDTOObject(CouponForm couponForm){
		CouponDTO couponDto = new CouponDTO();
		couponDto.setCouponName(couponForm.getCouponName());
		couponDto.setCouponValue(couponForm.getCouponValue());
		couponDto.setCouponUses(couponForm.getCouponUses());
		couponDto.setCouponType(couponForm.getCouponType());
		couponDto.setCouponStatus(couponForm.getCouponStatus());
		couponDto.setCouponServiceType(couponForm.getCouponServiceType().toLowerCase());
		couponDto.setCouponPerUserLimit(couponForm.getCouponPerUserLimit());
		couponDto.setCouponLimit(couponForm.getCouponLimit());
		couponDto.setStartDate(couponForm.getStartDate());
		couponDto.setEndDate(couponForm.getEndDate());
		couponDto.setCouponUses(couponForm.getCouponUses());
		couponDto.setMinimumRechargeValue(couponForm.getMinimumRechargeValue());
		couponDto.setMaximumcashback(couponForm.getMaximumcashback());
		return couponDto;
	}
	
	public void updateCouponRedeemLogs(UserDTO userDto,CouponDTO couponDto, int rechargeId){
		couponDao.updateCouponRedeemLogs(userDto, couponDto, rechargeId);
	}
	
}
