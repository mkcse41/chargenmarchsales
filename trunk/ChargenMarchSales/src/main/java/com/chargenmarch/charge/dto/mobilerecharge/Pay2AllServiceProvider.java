package com.chargenmarch.charge.dto.mobilerecharge;

/**
 * This class store all pay2all operator code.
 * @author mukesh
 *
 */
public class Pay2AllServiceProvider {
    private int id;
    private String provider_name;
    private String provider_code;
    private String service;
    private String provider_image;
    private String status;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProvider_name() {
		return provider_name;
	}
	public void setProvider_name(String provider_name) {
		this.provider_name = provider_name;
	}
	public String getProvider_code() {
		return provider_code;
	}
	public void setProvider_code(String provider_code) {
		this.provider_code = provider_code;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getProvider_image() {
		return provider_image;
	}
	public void setProvider_image(String provider_image) {
		this.provider_image = provider_image;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Pay2AllServiceProvider [id=" + id + ", provider_name="
				+ provider_name + ", provider_code=" + provider_code
				+ ", service=" + service + ", provider_image=" + provider_image
				+ ", status=" + status + "]";
	}
    
    
    
}
