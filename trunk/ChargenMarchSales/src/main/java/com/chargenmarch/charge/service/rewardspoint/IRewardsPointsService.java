package com.chargenmarch.charge.service.rewardspoint;

import com.chargenmarch.charge.dto.rewardspoint.RewardsPoint;
import com.chargenmarch.common.dto.user.UserDTO;

/**
 * 
 * @author mukesh
 *
 */
public interface IRewardsPointsService {
	public void earnRewardsPoint(UserDTO userDto, int amount,int rechargeId);
	public RewardsPoint reedomRewardsPoint(UserDTO userDto,boolean isRedeemPoints);
	public RewardsPoint getRewardsPointDetails(UserDTO userDto);
}
