package com.chargenmarch.charge.enums.recharge;

public enum RechargeType {
	Mobile,
	DataCard,
	DTH,
	Wallet
}
