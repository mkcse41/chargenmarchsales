package com.chargenmarch.charge.dao.mobilerecharge;

import java.util.List;

import com.chargenmarch.charge.dto.funds.AddFundsRequestDTO;
import com.chargenmarch.charge.dto.mobilerecharge.Operator;
import com.chargenmarch.charge.dto.mobilerecharge.Pay2AllServiceProvider;
import com.chargenmarch.charge.dto.mobilerecharge.Plan;
import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.common.dto.user.UserDTO;

public interface IMobileRechargeDao {
	public int updatePlansListinDB(String operatorName, String zoneName, String amount, String validity, String description, String type);
	public void truncateMobilePlans();
	public int saveMobileRechargeDetails(final RechargeDTO rechargeDto);
	public List<Plan> getDistinctPlanTypes();
	public void updateMobilePlansLocalTypes(String type_local, String type/*, String operator_code*/);
	public List<Plan> getMobilePlansByTypeOperatorAndZone(String local_type, String operatorName, String zone);
	public void saveOperatorNumberMapping(String mobileNumber, Operator operator);
	public Operator getOperatorByNumber(String mobileNumber);
	public List<RechargeDTO> getRechargeListByChargerId(String chargerId);
	public List<RechargeDTO> getRechargeCashbackListByChargerId(int chargerId);
	public void insertRechargeCashBack(UserDTO user,String rechargeIds,int cashback);
	public void updateRechargeCashBack(String rechargeIds);
	public Pay2AllServiceProvider getPay2AllServiceProviderById(int providerId);
	public Pay2AllServiceProvider getPay2AllServiceProviderByProviderCode(String providerCode);
	public List<RechargeDTO> getRechargeListByDate(String date);
	public List<RechargeDTO> getRechargeListByPGOrderId(String pgOrderId);
	public List<RechargeDTO> getRechargeListAfterDate(String date);
	public List<RechargeDTO> getRechargeListBeforeDate(String date);
	public int addFundsRequest(AddFundsRequestDTO fundsRequestDTO);
	public List<AddFundsRequestDTO> getFundsRequestByChargersId(UserDTO user);
	public List<AddFundsRequestDTO> getPendingFundsRequest();
	public int updateFundsRequestStatus(UserDTO user,String bankRefId);
}
