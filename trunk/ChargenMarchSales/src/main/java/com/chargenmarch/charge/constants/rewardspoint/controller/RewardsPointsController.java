package com.chargenmarch.charge.constants.rewardspoint.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.chargenmarch.charge.constants.recharge.RechargeConstants;
import com.chargenmarch.charge.dto.rewardspoint.RewardsPoint;
import com.chargenmarch.charge.service.rewardspoint.IRewardsPointsService;
import com.chargenmarch.common.constants.RequestConstants;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.service.user.IUserService;
import com.chargenmarch.common.utility.IUtility;
import com.google.gson.Gson;

@Controller
@RequestMapping(value=RequestConstants.REWARDS_POINTS_URL)
public class RewardsPointsController {

	private final static Logger logger = LoggerFactory.getLogger(RewardsPointsController.class);
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private IUtility utility;
	
	@Autowired
	private IRewardsPointsService rewardsPointsService;
	
	@RequestMapping(value = RequestConstants.REDEEM_POINTS, method ={ RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody String redeemRewardsPoints(@RequestParam(RechargeConstants.EMAIL_ID) String emailId) throws Exception {
		try{
			if(null!=emailId){
			   UserDTO userDto = userService.getUserByEmailId(emailId);
			   if(null!=userDto){
				   RewardsPoint rewardsPoint = rewardsPointsService.reedomRewardsPoint(userDto,false);
				   return new Gson().toJson(rewardsPoint);
			   }
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
