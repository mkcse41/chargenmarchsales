	
package com.chargenmarch.charge.controller.user;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import com.chargenmarch.charge.constants.recharge.RechargeConstants;
import com.chargenmarch.charge.dto.funds.AddFundsRequestDTO;
import com.chargenmarch.charge.dto.funds.RechargeMarginDTO;
import com.chargenmarch.charge.dto.mobilerecharge.Operator;
import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.charge.forms.recharge.RechargeForm;
import com.chargenmarch.charge.joloapi.service.IRechargeService;
import com.chargenmarch.charge.mail.thread.SendMailThread;
import com.chargenmarch.charge.service.mobilerecharge.IMobileRechargeService;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.RequestConstants;
import com.chargenmarch.common.constants.SessionConstants;
import com.chargenmarch.common.constants.ViewConstants;
import com.chargenmarch.common.constants.paymentgateway.PaymentConstants;
import com.chargenmarch.common.dao.user.IUserDao;
import com.chargenmarch.common.dto.email.EmailDTO;
import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.enums.user.UserVerificationStatus;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.service.user.IUserService;
import com.chargenmarch.common.utility.IUtility;
import com.chargenmarch.common.validator.user.UserDetailValidator;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

@Controller
@RequestMapping(value=RequestConstants.USER)
public class UserController {
	private final static Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserDetailValidator userValidator;
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private IUserDao userDao;
	
	@Autowired
	private IUtility utility;
	
	@Autowired
	private IMobileRechargeService rechargeService;
	
	@Autowired
	private IEmailSenderService emailService;
	
	@Autowired
	ThreadPoolTaskExecutor taskExecutor;
	
	@Autowired
	SendMailThread mailThread;
	
	@Autowired
	private WebApplicationContext context;
	
	@Autowired
	private IRechargeService rService;
	
	@RequestMapping(value=RequestConstants.REGISTERED_HTM, method = RequestMethod.POST)
	@ResponseBody
	public String userRegister(@Valid UserDTO user, BindingResult result, HttpServletRequest request){
		
		userValidator.validate(user, result);
		JsonObject response = new JsonObject();
		if(result.hasErrors()){
			List<ObjectError> errorList = result.getAllErrors();
			Iterator<ObjectError> errorItr = errorList.iterator();
			while(errorItr.hasNext()){
				ObjectError error = errorItr.next();
			}
			response.addProperty(Constants.MESSAGE, "User Not Registered. Please Try Again.");
			
		} else{
			/**
			 * This code used for egsale.chargenmarch.com
			 * If utm_source contains egsale then set source="egsale";
			 */
			String utmSource = (String) request.getSession().getAttribute(Constants.UTM_SOURCE);
			if(null!=utmSource && utmSource.contains(Constants.EGSALE)){
				user.setSource(utmSource);
			}
			String userReferrralCode = request.getParameter(Constants.USER_REFERRAL_CODE);
			
			int userAddStatus = userService.addUser(user);
			UserDTO sessionUser = null;
			if(userAddStatus==1){
				sessionUser = userService.getUserByEmailId(user.getEmailId().trim());
				/**
				 * First of all check user referral code,
				 * 1. If user referral code is valid then set cashback amount for particular user.
				 */
				if(null != userReferrralCode && !userReferrralCode.equals(Constants.BLANK)){
					   request.getSession().setAttribute(Constants.USER_REFERRAL_CODE, userReferrralCode);
				}
				//generate OTP
				int otp = utility.generateRandomNumber();
				logger.info("Your OTP:"+otp);
				sessionUser.setOtp(otp);
				int otpResponse = userService.sendOneTimePassword(sessionUser);
				request.getSession().setAttribute(SessionConstants.USER_BEAN, sessionUser);
				if(otpResponse == 1){
					response.addProperty(Constants.MESSAGE,"OTP_Screen");
				}
				else{
					response.addProperty(Constants.MESSAGE, "User Not Registered. Please Try Again.");	
				}
			} else if(userAddStatus==3){
				sessionUser = userService.getUserInfoMapByEmailId().get(user.getEmailId());
				request.getSession().setAttribute(SessionConstants.USER_BEAN, sessionUser);
				response.addProperty(Constants.MESSAGE,"User Already Registered");
				request.getSession().removeAttribute(RequestConstants.QUICK_RECHARGE_CHECK_VAL);
			}
			else 
				response.addProperty(Constants.MESSAGE, "User Not Registered. Please Try Again.");
		}
		
		return response.toString();
	}
	
	/*
	 * get Response after send OTP screen
	 */
	@RequestMapping(value=RequestConstants.OTP_RESPONSE_HTM, method = RequestMethod.POST)
	@ResponseBody
	public String getOtpResponse(@RequestParam(RechargeConstants.EMAIL_ID) String emailId , HttpServletRequest request) {
		JsonObject response = new JsonObject();
		UserDTO sessionUser = null;
		if(!StringUtils.isEmpty(emailId)){
			try{
				sessionUser = userService.getUserByEmailId(emailId.trim());
				if(null == sessionUser){
					 sessionUser = userDao.getUserByEmailId(emailId.trim());
				}
				//generate OTP
				int otpResponse = 0;
				if(null != sessionUser){
					int otp = utility.generateRandomNumber();
					logger.info("Your OTP:"+otp);
					sessionUser.setOtp(otp);
					otpResponse = userService.sendOneTimePassword(sessionUser);
					request.getSession().setAttribute(SessionConstants.USER_BEAN, sessionUser);
				}
				if(otpResponse == 1){
					response.addProperty(Constants.MESSAGE,"OTP_Screen");
				}else{
					response.addProperty(Constants.MESSAGE, "User Not Registered. Please Try Again.");	
				}
			}catch(Exception e){
				logger.error("Error in Otp Response==>"+e);
				response.addProperty(Constants.MESSAGE, "User Not Registered. Please Try Again.");	
				e.printStackTrace();
			}
		}
		return response.toString();
	}
	
	@RequestMapping(value=RequestConstants.REGISTERED_OTHER_HTM, method = RequestMethod.POST)
	@ResponseBody
	public String userRegisterByThirdParty(@Valid UserDTO user, BindingResult result, HttpServletRequest request){
		
		userValidator.validate(user, result);
		JsonObject response = new JsonObject();
		if(result.hasErrors()){
			List<ObjectError> errorList = result.getAllErrors();
			Iterator<ObjectError> errorItr = errorList.iterator();
			while(errorItr.hasNext()){
				ObjectError error = errorItr.next();
			}
			response.addProperty(Constants.MESSAGE, "User Not Registered. Please Try Again.");
			
		} else{
			int userAvailableStatus = userService.checkUser(user);
			UserDTO sessionUser = null;
			if(userAvailableStatus==1){
				 response.addProperty(Constants.MESSAGE, "Show Mobile Number Screen.");	
			} else if(userAvailableStatus==3){
				sessionUser = userService.getUserInfoMapByEmailId().get(user.getEmailId());
				request.getSession().setAttribute(SessionConstants.USER_BEAN, sessionUser);
				response.addProperty(Constants.MESSAGE,"User Already Registered");
				request.getSession().removeAttribute(RequestConstants.QUICK_RECHARGE_CHECK_VAL);
			}
			else 
				response.addProperty(Constants.MESSAGE, "User Not Registered. Please Try Again.");
		}
		return response.toString();
	}
	
	
	
	@RequestMapping(value=RequestConstants.OTP_VERIFICATION , method = RequestMethod.POST)
    @ResponseBody
    public String userOTPVerification(@RequestParam(RechargeConstants.OTP) String otp , HttpServletRequest request){
		JsonObject response = new JsonObject();
		UserDTO sessionUser = (UserDTO)request.getSession().getAttribute(SessionConstants.USER_BEAN);
			if(null != sessionUser && sessionUser.getOtp() == utility.convertStringToInt(otp)){
				userService.updateUserVerifiedStatus(sessionUser, UserVerificationStatus.VERIFIED);
				String referralCode = (String) request.getSession().getAttribute(Constants.USER_REFERRAL_CODE);
				if(null  != referralCode && !referralCode.equals(Constants.BLANK)){
					UserDTO userDTO = userService.getUserByReferralCode(referralCode.toUpperCase());
				    if(null != userDTO && userService.getReferralLogs(sessionUser).isEmpty()){
						//Block commented by Durlabh to stop giving 10rs referral amount on every successful registration
				      /*int referralCashbackAmount = userService.getReferralCashbackAmount();
					   	int referralBalance = userDTO.getReferralBalance() + referralCashbackAmount;
					   	userDTO.setReferralBalance(referralBalance); */
				    	userDTO.setReferralBalance(0);
				    	userService.updateUserRefferalBalance(userDTO);
				    	userService.insertReferralLogs(sessionUser, userDTO, 0);
				    	request.getSession().removeAttribute(Constants.USER_REFERRAL_CODE);

				   }
				}
				response.addProperty(Constants.MESSAGE,"User Registered Successfully");
				request.getSession().removeAttribute(RequestConstants.QUICK_RECHARGE_CHECK_VAL);
			}
			else{
				response.addProperty(Constants.MESSAGE,"Please Insert Correct OTP");
			}
		return response.toString();
	}
    
	@RequestMapping(value = RequestConstants.USER_LOGIN, method = RequestMethod.POST)
	public @ResponseBody String userLogin(@RequestParam(RechargeConstants.EMAIL_ID) String emailId, @RequestParam(RechargeConstants.PASSWORD) String password,HttpServletRequest request) throws Exception {
		UserDTO user = userService.getUserByEmailId(emailId);
		JsonObject response = new JsonObject();
		if(null!=user && null!=password){
			if(user.getPassword().equalsIgnoreCase(password)){
				if(user.getIsverified() == UserVerificationStatus.VERIFIED.ordinal() && user.getIsSalesUser() == 1){
					request.getSession().setAttribute(SessionConstants.USER_BEAN, user);
					response.addProperty(Constants.MESSAGE, Constants.SUCCESS);
					request.getSession().removeAttribute(RequestConstants.QUICK_RECHARGE_CHECK_VAL);
				} else if(user.getIsverified() == UserVerificationStatus.UNVERIFIED.ordinal() && user.getIsSalesUser() == 1){
					request.getSession().setAttribute(SessionConstants.USER_BEAN, user);
					response.addProperty(Constants.MESSAGE, Constants.NON_VERIFIED);
				}else{
					response.addProperty(Constants.MESSAGE, Constants.FAILURE);
				}
			}else{
				response.addProperty(Constants.MESSAGE, Constants.FAILURE);
			}
		}
		else if(user == null){
			response.addProperty(Constants.MESSAGE, "user not exists");
		}
		else{
			response.addProperty(Constants.MESSAGE, Constants.FAILURE);
		}
		
		return response.toString();
	}
	
	@RequestMapping(value = RequestConstants.USER_PROFILE, method = RequestMethod.GET)
	public ModelAndView userProfile(HttpServletRequest request,HttpServletResponse response) throws Exception {
		ModelAndView mnv = new ModelAndView(utility.getviewResolverName(request, ViewConstants.USER_PROFILE));
		//ModelAndView mnv = new ModelAndView(ViewConstants.USER_PROFILE);
		if(null!=request.getSession().getAttribute(SessionConstants.USER_BEAN)){
			UserDTO user = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
			List<RechargeDTO> transactionList = rechargeService.getRechargeListByChargerId(String.valueOf(user.getId()));
			List<AddFundsRequestDTO> addFundsRequest = rService.getFundsRequestByChargersId(user);
			List<RechargeMarginDTO> rechargeMargin = rService.getRecahrgeMarginByChargersId(user);
			mnv.addObject(Constants.USER, user);
			mnv.addObject("transactionList", transactionList);
			mnv.addObject("addFundsRequestList", addFundsRequest);
			mnv.addObject("rechargeMarginLogs", rechargeMargin);
			return mnv;
		}
		response.sendRedirect(utility.getBaseUrl());
		return null;
	}
	
	@RequestMapping(value = RequestConstants.USER_LOGOUT, method = RequestMethod.GET)
	public String userLogout(HttpServletRequest request,HttpServletResponse response) throws Exception {
	    request.getSession().invalidate();
	    response.sendRedirect(utility.getBaseUrl());
	    return null;
	}
	
	@RequestMapping(value = RequestConstants.CHANGE_PASSWORD, method = RequestMethod.POST)
	@ResponseBody
	public String changePassword(@RequestParam("newPass") String newPassword, @RequestParam("oldPass") String currentPassword, HttpServletRequest request){
		UserDTO user = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
		if(null!=user && user.getPassword().equals(currentPassword)){
			user.setPassword(newPassword);
			userService.updateUserPassword(user);
			request.getSession().setAttribute(SessionConstants.USER_BEAN, user);
			return "Successfully Updated Password";
		} else {
			return "Please enter correct current password";
		}
	}
	

	@RequestMapping(value = RequestConstants.ADD_MONEY, method={ RequestMethod.GET, RequestMethod.POST })
	public ModelAndView processedLoadAmount(HttpServletRequest request,HttpServletResponse response) throws IOException{
		try{
			String amount = request.getParameter(Constants.AMOUNT);
			if(null!=request.getSession().getAttribute(SessionConstants.USER_BEAN) && null!=amount){
				UserDTO user = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
				int loadAmount = utility.convertStringToInt(amount);
				loadAmount = userService.calculateWalletLoadMoney(loadAmount);
				String userCredentials = user.getId() + user.getEmailId();
				OrderInfo orderInfo = utility.initializeOrderInfoForPaymentGateway(user.getName(), null, user.getEmailId(),user.getMobileNo(), 
				    		null, null, null, null, RequestConstants.WALLET, loadAmount, RequestConstants.PAY_U,userCredentials);
			    request.setAttribute(RequestConstants.ORDER_INFO, orderInfo);
			    request.setAttribute(RequestConstants.URL, utility.getBaseUrl() + RechargeConstants.SLASH + PaymentConstants.PAYMENT.toLowerCase() + RequestConstants.PROCESSED_PAYMENT );
			    ModelAndView view = new ModelAndView(ViewConstants.PAY_U_LOWER);
			    return view;
		    } 
	    }catch(Exception e){
	    	e.printStackTrace();
	    }
		response.sendRedirect(utility.getBaseUrl());
		return null;
	}
	
	
	@RequestMapping(value = RequestConstants.USER_WALLET, method={RequestMethod.POST })
	public ModelAndView userWallet(HttpServletRequest request,HttpServletResponse httpResponse) throws IOException{
		String response = request.getParameter(RequestConstants.RESPONSE);
		try{
			if(null!=request.getSession().getAttribute(SessionConstants.USER_BEAN) && null!=response && utility.isValidatePayment(request)){
				Gson gson = new GsonBuilder().create(); 
				OrderInfo orderInfo = gson.fromJson(response, OrderInfo.class);
				UserDTO user = (UserDTO) request.getSession().getAttribute(SessionConstants.USER_BEAN);
				String orderId = orderInfo.getTxnId();
				String status = orderInfo.getStatus();
				float loadAmount = orderInfo.getAmount() + orderInfo.getDiscount();
				RechargeDTO rechargeDto = createRechargeDtoObject(user, Math.round(loadAmount), orderId);
				if(!orderId.equals(Constants.BLANK) && status.equals(Constants.SUCCESS) && rechargeService.getRechargeListByPGOrderId(orderId).size() <= 0){
					rechargeDto.setSessionId(request.getSession().getId());
					user.setWalletBalance(Math.round(loadAmount) + user.getWalletBalance());
					request.getSession().setAttribute(SessionConstants.USER_BEAN,user);
					userService.updateUserWalletBalance(user);
					rechargeDto.setIsSuccesfulRecharge(1);
				}
				rechargeService.saveMobileRechareDetails(rechargeDto);
			}
		}catch(Exception e){	
			e.printStackTrace();
		}
		httpResponse.sendRedirect(utility.getBaseUrl() + RequestConstants.USER + RequestConstants.USER_PROFILE);
		return null;
   }
	
  private RechargeDTO createRechargeDtoObject(UserDTO userDto, int amount,String orderId){
	  RechargeDTO rechargeDto = new RechargeDTO();
	  rechargeDto.setUserDto(userDto);
	  rechargeDto.setAmount(amount);
	  rechargeDto.setServiceType(RequestConstants.WALLET);
	  rechargeDto.setOrderId(orderId);
	  rechargeDto.setIsSuccesfulRecharge(0);
	  rechargeDto.setOperator(new Operator());
	  rechargeDto.setInternetCharges(userService.calculateWalletLoadMoney(amount) - amount);
	  return rechargeDto;
  }
	
   @RequestMapping(value = RequestConstants.FORGOT_PASSWORD, method=RequestMethod.POST)
   public @ResponseBody String forgotPassword(@RequestParam("emailId") String emailId){
		UserDTO user = userService.getUserInfoMapByEmailId().get(emailId);
		JsonObject response = new JsonObject();
		if(user!=null){
			String message = utility.classpathResourceLoader("Forgot-Password.html").replace("%BASEURL%", context.getBean("baseUrl", String.class)).replace("%NAME%", user.getName()).replace("%CIPHER%", utility.base64Encoder(user.getEmailId()));
			emailService.sendEmail(Constants.NOREPLY_EMAIL_ID, user.getEmailId().split(Constants.COMMA), "Password Recovery Mail", message, null, null, true);
			response.addProperty(Constants.MESSAGE, "Password has been sent to registered mail id provided above");
		} else {
			response.addProperty(Constants.MESSAGE, "No user registered with above provided Email Id");
		}
		return response.toString();
	}
   
   @RequestMapping(value = RequestConstants.RESET_PASSWORD_URL+"/{emailcipher}", method={ RequestMethod.GET, RequestMethod.POST })
	public ModelAndView resetPassword(@PathVariable("emailcipher") String emailCipher,HttpServletRequest request) throws IOException{
		 ModelAndView view = new ModelAndView(utility.getviewResolverName(request, RequestConstants.RESET_PASSWORD));
		 String emailId = utility.base64Decoder(emailCipher);
		 UserDTO user = userService.getUserInfoMapByEmailId().get(emailId);
		 if(user == null){
			 view.setViewName(ViewConstants.VIEW_INDEX);
		 } else {
			 view.addObject("user", user);
		 }
		 return view;
	}
   
   @RequestMapping(value="/updatedPassword", method=RequestMethod.POST)
   public ModelAndView successfulPasswordReset(HttpServletResponse response, @RequestParam("email") String emailId, @RequestParam("password") String password) throws IOException{
	   userService.resetPassword(emailId, password);
	   response.sendRedirect(utility.getBaseUrl());
	   return null;
   }
   
   
   @RequestMapping(value = RequestConstants.SIGNIN, method={ RequestMethod.GET, RequestMethod.POST })
	public ModelAndView userLoginWap(HttpServletRequest request) throws IOException{
		 ModelAndView view = new ModelAndView(utility.getviewResolverName(request, RequestConstants.WAP_USER_LOGIN));
		 RechargeForm rechargeForm = getRechargeFormObject(request);
		 request.getSession().setAttribute(RequestConstants.QUICK_RECHARGE_CHECK_VAL, request.getParameter(RequestConstants.QUICK_RECHARGE_CHECK_VAL));
		 request.getSession().setAttribute(SessionConstants.RECHARGE_FORM,rechargeForm);
		 return view;
	}
   
    @RequestMapping(value=RequestConstants.UPDATE_POPUP_INFO, method = RequestMethod.POST)
	@ResponseBody
	public String updatePopUpInfo(@Valid UserDTO user, BindingResult result, HttpServletRequest request){
    	JsonObject response = new JsonObject();
    	request.getSession().setAttribute(SessionConstants.UPDATE_POPUP_INFO, true);
        response.addProperty(Constants.MESSAGE,"successfully_updated");    
        return response.toString();
   }
   
   
   private RechargeForm getRechargeFormObject(HttpServletRequest request){
		RechargeForm rechargeForm = null;
		if(request.getParameter(RequestConstants.NUMBER)==null && null!=request.getSession().getAttribute(SessionConstants.RECHARGE_FORM)){
		     rechargeForm = (RechargeForm) request.getSession().getAttribute(SessionConstants.RECHARGE_FORM);
		}else{
			rechargeForm = new RechargeForm();
			rechargeForm.setNumber(request.getParameter(RequestConstants.NUMBER));
			rechargeForm.setMobileRechargeService(request.getParameter(RequestConstants.MOBILE_RECHARGE_SERVICE));
			rechargeForm.setOperatorValue(request.getParameter(RequestConstants.OPERATOR_VALUE));
			rechargeForm.setOperatorText(request.getParameter(RequestConstants.OPERATOR_TEXT));
			rechargeForm.setService(request.getParameter(RequestConstants.SERVICE));
			rechargeForm.setAmount(request.getParameter(RequestConstants.AMOUNT));
			rechargeForm.setCircleName(request.getParameter(RequestConstants.CIRCLE_NAME));
			rechargeForm.setCircleValue(request.getParameter(RequestConstants.CIRCLE_VALUE));
		}
		return rechargeForm;
	 }	 
   
   @RequestMapping(value = RequestConstants.VERIFY_USERS, method=RequestMethod.GET)
   public @ResponseBody List<UserDTO> sendVerificationMail() {
	List<UserDTO> unverifiedUsers = userService.getUserByVerificationStatus(UserVerificationStatus.UNVERIFIED);
	if(unverifiedUsers != null) {
		for(UserDTO user : unverifiedUsers) {
			String link = userService.generateVerificationLink(user);
			EmailDTO email = new EmailDTO("ChargenMarch - Account Verification Reminder", user.getEmailId().split(Constants.COMMA), Constants.NOREPLY_EMAIL_ID, null, null, link, null, null);
			emailService.sendEmail(email);
		}
	}
	return unverifiedUsers;
   }
   
   @RequestMapping(value = RequestConstants.VERIFY+"/{emailcipher}", method=RequestMethod.GET)
   public @ResponseBody UserDTO verifyUser(@PathVariable("emailcipher") String emailCipher) {
		UserDTO user = userService.getUserByEmailId(utility.base64Decoder(emailCipher));
		user.setIsverified(UserVerificationStatus.VERIFIED.ordinal());
		userService.updateUserVerifiedStatus(user, UserVerificationStatus.VERIFIED);
		return user;
   }
   
   @RequestMapping(value = "generateReferralCode", method=RequestMethod.GET)
   public @ResponseBody UserDTO generateReferralCode() {
	    Map<Integer, UserDTO> userInfoMapById = userService.getUserInfoMapById();
	    for (Map.Entry<Integer, UserDTO> entry : userInfoMapById.entrySet()){
	        UserDTO user = entry.getValue();
	        userService.updateUserReferralCode(user);
	    }
		return null;
   }
   
   @RequestMapping(value = "referralMail", method=RequestMethod.GET)
   public @ResponseBody UserDTO userRefferalMail(HttpServletRequest request) {
	   String isDelete = request.getParameter("isDelete");
	   String lastId = request.getParameter("lastId");
	   mailThread.setLastId(lastId);
	   mailThread.setIsDelete(isDelete);
	   taskExecutor.execute(mailThread);
	   return null;
   }
   
   /**
    * This method used to update use waller balance by email id 
    * @param request
    * @return
    */
   @RequestMapping(value = "walletbal", method=RequestMethod.GET)
   public @ResponseBody UserDTO updateUserWalletBalance(HttpServletRequest request) {
	    UserDTO user = null;
	    String emailId = request.getParameter(Constants.EMAIL_ID.toLowerCase());
	    String walletbal = request.getParameter(Constants.WALLETBAL);
	    if(null != emailId && !emailId.equals(Constants.BLANK)){
	    	int walletbalance = utility.convertStringToInt(walletbal);
	    	user = userService.getUserByEmailId(emailId);
	        if(null != user){
	        	user.setWalletBalance( walletbalance + user.getWalletBalance() );
	        	userService.updateUserWalletBalance(user);
	        	userService.updateUserCacheMaps(user);
	        }
	    }
	    return user;
   }
   
   
   /**
    * This method used to update use referral balan.
    * @param request
    * @return
    */
   @RequestMapping(value = "referralbal", method=RequestMethod.GET)
   public @ResponseBody UserDTO updateUserReferralBalance(HttpServletRequest request) {
	    userService.updateUserReferralAmount();
	    return null;
   }
   
   @RequestMapping(value = "saveBrowserNotificationLogs", method={RequestMethod.POST, RequestMethod.GET})
   public @ResponseBody UserDTO saveBrowserNotificationId(HttpServletRequest request) {
	    String id = request.getParameter(Constants.ID);
	    String browserNotificationId = request.getParameter(Constants.BROWSER_NOTIFICATION_ID);
	    if(!StringUtils.isEmpty(id) && !StringUtils.isEmpty(browserNotificationId)){
	    	UserDTO user = userService.getUserById(utility.convertStringToInt(id));
	    	if(null != user){
	    		user.setBrowserNotificationId(browserNotificationId);
		        userService.saveOrUpdateBrowserNotificationLogs(user, browserNotificationId);
		        request.getSession().setAttribute(SessionConstants.USER_BEAN, user);
	    	}
	    }
	    return null;
   }
}