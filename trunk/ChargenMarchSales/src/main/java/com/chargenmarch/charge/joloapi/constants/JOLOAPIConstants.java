package com.chargenmarch.charge.joloapi.constants;

public class JOLOAPIConstants {
   public static final String USERID = "userid";
   public static final String KEY = "key";
   public static final String TYPE = "type";
   public static final String JSON = "json";
   public static final String MOB = "mob";
   public static final String OPERATOR_NAME = "operatorName";
   public static final String PAY_CODE = "pay_code";
   public static final String CODE = "code";
   public static final String RECHARGE_SERVICE_TYPE = "recharge_service_type";
   public static final String CIRCLE_NAME = "circleName";
   public static final String CIRCLE_CODE = "circle_code";
   public static final String PREPAID = "prepaid";
   public static final String JOLOAPI = "joloapi";
   public static final String SUCCESS = "success";
   public static final String OPERATOR = "operator";
   public static final String SERVICE = "service";
   public static final String AMOUNT = "amount";
   public static final String ORDERID = "orderid";
   public static final String STD = "std";
   public static final String CUSTOMER_ACCOUNT_NUMBER = "ca";
   public static final String SUCCESS_CAP = "SUCCESS";
   public static final String ERROR_CODE = "errorcode";
   public static final String ERROR_DESCRIPTION = "errordescription";
   public static final String LOCAL_ERROR_DESCRIPTION = "local_error_desc";
   public static final String JOLOAPI_BILLING = "joloapiLandline";
   public static final String TXN = "txn";
   public static final String SERVICE_TYPE = "servicetype";
  
}
