package com.chargenmarch.charge.joloapi.dto;

/**
 * This Class used to store operator wise data IN Jolo API.
 * @author mukesh
 *
 */
public class JOLOOperator {
    private String operatorName;
    private String pay_code;
    private String code;
    private String rechargeServiceType;
    private String operator_code;
    private String circle_code;
    
    
	public String getOperatorName() {
		return operatorName;
	}
	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}
	public String getPay_code() {
		return pay_code;
	}
	public void setPay_code(String pay_code) {
		this.pay_code = pay_code;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getRechargeServiceType() {
		return rechargeServiceType;
	}
	public void setRechargeServiceType(String rechargeServiceType) {
		this.rechargeServiceType = rechargeServiceType;
	}
	public String getOperator_code() {
		return operator_code;
	}
	public void setOperator_code(String operator_code) {
		this.operator_code = operator_code;
	}
	public String getCircle_code() {
		return circle_code;
	}
	public void setCircle_code(String circle_code) {
		this.circle_code = circle_code;
	}
	@Override
	public String toString() {
		return "JOLOOperator [operatorName=" + operatorName + ", pay_code=" + pay_code + ", code=" + code
				+ ", rechargeServiceType=" + rechargeServiceType + ", operator_code=" + operator_code + ", circle_code="
				+ circle_code + "]";
	}
	
}
