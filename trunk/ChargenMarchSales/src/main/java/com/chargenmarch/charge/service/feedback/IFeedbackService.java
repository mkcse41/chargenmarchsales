package com.chargenmarch.charge.service.feedback;

import com.chargenmarch.charge.dto.feedback.FeedbackDto;

public interface IFeedbackService {
	public int addFeedback(FeedbackDto feedbackDto);
}
