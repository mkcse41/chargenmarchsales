package com.chargenmarch.charge.dao.coupon.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.chargenmarch.charge.dao.coupon.ICouponDao;
import com.chargenmarch.charge.dto.coupon.CouponDTO;
import com.chargenmarch.charge.mapper.coupon.CouponMapper;
import com.chargenmarch.charge.query.coupon.CouponQuery;
import com.chargenmarch.common.dto.user.UserDTO;

/**
 * 
 * @author mukesh
 *
 */
@Repository
public class CouponDaoImpl implements ICouponDao {

	private final static Logger logger = LoggerFactory.getLogger(CouponDaoImpl.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
		
	public List<CouponDTO> getCouponDetail() {
		return (List) jdbcTemplate.query(CouponQuery.SELECT_FROM_COUPON, new CouponMapper());
	}
	
	public void insertCouponDetail(CouponDTO couponDTO) {
		Object args[]= new Object[] {couponDTO.getCouponName(),couponDTO.getCouponType(),couponDTO.getCouponValue(),couponDTO.getCouponLimit(),couponDTO.getMinimumRechargeValue(),couponDTO.getCouponServiceType(),couponDTO.getStartDate(),couponDTO.getEndDate(),couponDTO.getCouponPerUserLimit(),couponDTO.getMaximumcashback()};
		jdbcTemplate.update(CouponQuery.INSERT_INTO_COUPON, args);
	}
	
	public List<CouponDTO> getCouponDetailByCouponName(String couponName,String couponType) {
		Object args[]= new Object[] {couponName,couponType};
		return (List) jdbcTemplate.query(CouponQuery.SELECT_FROM_COUPON_BY_COUPON_NAME,args, new CouponMapper());
	}
	
	public List getCouponUsedCountByUser(UserDTO userDto,CouponDTO couponDto) {
		Object args[]= new Object[] {userDto.getId(),couponDto.getId()};
		return (List) jdbcTemplate.queryForList(CouponQuery.SELECT_FROM_REEDOM_COUPON_LOGS_BY_USER,args);
	}
	
	public void insertReedomCouponLogs(CouponDTO couponDTO,UserDTO userDto,int recahrgeId) {
		Object args[]= new Object[] {userDto.getId(),couponDTO.getId(),couponDTO.getCouponReedomValue(),recahrgeId,};
		jdbcTemplate.update(CouponQuery.INSERT_INTO_REEDOM_COUPON_LOGS, args);
	}
	
	public void updateCouponRedeemLogs(UserDTO userDto,CouponDTO couponDto, int rechargeId ){
		Object args[]= new Object[] {rechargeId, userDto.getId() , couponDto.getId()};
		jdbcTemplate.update(CouponQuery.UPDATE_REEDOM_COUPON_LOGS, args);
	}
}
