package com.chargenmarch.charge.joloapi.enums.billing;

/*
 * based on jolo api
 */
public enum LandlineOperatorCodes {
	ATL("Airtel Landline"),
	BSL("BSNL Landline"),
	RCOM("Reliance Landline"),
	TIL("Tata Landline"),
	MTDL("MTNL Delhi Landline"),
	TIK("Tikona Internet");

	private String opName;       

    private LandlineOperatorCodes(String operatorName) {
    	opName = operatorName;
    }
    
    public String getOperatorName(){
    	return opName;
    }
}
