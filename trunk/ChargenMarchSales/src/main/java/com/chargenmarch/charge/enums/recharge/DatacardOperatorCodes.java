package com.chargenmarch.charge.enums.recharge;

public enum DatacardOperatorCodes {
	MB("MTS MBlaze"),
	MBB("MTS MBrowse"),
	PM("Photon Max"),
	RN("Reliance NetConnect"),
	RT("Reliance NetConnect+"),
	TPW("Tata Photon Whiz"),
	TPM("Tata Photon Max");
	
	private String operatorName;
	
	private DatacardOperatorCodes(String operatorName){
		this.operatorName = operatorName;
	}
	public String getOperatorName() {
		return operatorName;
	}

}
