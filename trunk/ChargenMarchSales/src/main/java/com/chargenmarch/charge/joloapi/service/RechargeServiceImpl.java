package com.chargenmarch.charge.joloapi.service;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.chargenmarch.charge.constants.recharge.RechargeConstants;
import com.chargenmarch.charge.dao.mobilerecharge.IMobileRechargeDao;
import com.chargenmarch.charge.dto.Pay2AllRechargeAPIStatusDTO;
import com.chargenmarch.charge.dto.coupon.CouponDTO;
import com.chargenmarch.charge.dto.funds.AddFundsRequestDTO;
import com.chargenmarch.charge.dto.funds.RechargeMarginDTO;
import com.chargenmarch.charge.dto.mobilerecharge.Operator;
import com.chargenmarch.charge.dto.mobilerecharge.Plan;
import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.charge.forms.coupon.CouponForm;
import com.chargenmarch.charge.joloapi.constants.JOLOAPIConstants;
import com.chargenmarch.charge.joloapi.constants.JOLORechargeResponseDTO;
import com.chargenmarch.charge.joloapi.dao.IRechargeDao;
import com.chargenmarch.charge.joloapi.dto.JOLOCircle;
import com.chargenmarch.charge.joloapi.dto.JOLOOperator;
import com.chargenmarch.charge.joloapi.dto.JOLOOperatorDetailResponseDTO;
import com.chargenmarch.charge.joloapi.dto.JOLORechargeAPIStatusDTO;
import com.chargenmarch.charge.joloapi.dto.JoloErrorDTO;
import com.chargenmarch.charge.joloapi.dto.JoloPlan;
import com.chargenmarch.charge.joloapi.enums.PlansTypeEnum;
import com.chargenmarch.charge.joloapi.enums.ServiceDownOperatorName;
import com.chargenmarch.charge.joloapi.thread.JOLONumberOperatorMappingThread;
import com.chargenmarch.charge.service.coupon.ICouponService;
import com.chargenmarch.charge.service.mobilerecharge.GenerateRechargeUtilityThread;
import com.chargenmarch.charge.service.mobilerecharge.IMobileRechargeService;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.FilePathConstants;
import com.chargenmarch.common.constants.PropertyKeyConstants;
import com.chargenmarch.common.dto.email.EmailDTO;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.service.http.IHTTPService;
import com.chargenmarch.common.service.user.IUserService;
import com.chargenmarch.common.service.user.impl.UserServiceImpl;
import com.chargenmarch.common.utility.IUtility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * This Service used to get Operator details API, mobile,DTH and Datacard Recharge & operator plan API.
 * @author mukesh
 *
 */
@Service
@PropertySource(FilePathConstants.CLASSPATH+FilePathConstants.JOLOAPI_PROPERTIES)
public class RechargeServiceImpl implements IRechargeService {
    
	private final static Logger LOGGER = LoggerFactory.getLogger(RechargeServiceImpl.class);

	@Autowired
	private Environment env;
	
	@Autowired
	private IHTTPService httpService;
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	ICouponService couponService;
	
	@Autowired
	IUtility utility;
	
	@Autowired
	IEmailSenderService emailSenderService;
	
	@Autowired
	IRechargeDao rechargeDao;
	
	@Autowired
	ThreadPoolTaskExecutor taskExecutor;
	
	@Autowired
	IMobileRechargeService mobileRechargeService;
	
	@Autowired
	IMobileRechargeDao mobileRechargeDao;
	
	@Autowired
	JOLONumberOperatorMappingThread joloNumberOperatorMapperThread;
	
	@Autowired
	GenerateRechargeUtilityThread generateRechargeUtilityThread;
	
	@Cacheable("circleCache")
	public JOLOCircle getCircleByCode(String code){
		return rechargeDao.getJOLOCircleCodesByCircleCode(code);
	}
	/**
	 * Get all recharge circle code.
	 */
	@Cacheable("circleCache")
	public List<JOLOCircle> getAllCircleCodeList(){
		return rechargeDao.getJOLOCircleCodes();
	}
	

	/**
	 * Get mobile operator by mobile number.
	 * Url : https://joloapi.com/api/findoperator.php?userid=9785205444&key=986286939135988&mob=7737174679&type=json.
	 * Response : {"operator_code":"17","circle_code":"13"}. 
	 * @param mobileNumber
	 * @return
	 */
	public Operator getOperatorDetailByNumber(String mobileNumber){
		Operator operator = null;
		try{
			    LOGGER.info("getOperatorDetailByNumber ==>MobileNumber=>"+mobileNumber);
				if(null!=mobileNumber && !StringUtils.isEmpty(mobileNumber)){
					operator = getOperatorByNumberFromDB(mobileNumber);
					if(operator==null){
						String apiUrl = env.getProperty(PropertyKeyConstants.JOLOAPIProperties.OPERATOR_DETAILS_API);
						apiUrl += setCommonAPIParameter() + JOLOAPIConstants.MOB + Constants.EQUALS + mobileNumber ;
						String response = httpService.HTTPGetRequest(apiUrl);
						 LOGGER.info("getOperatorDetailByNumber ==>response=>"+response);
						if(response!=null && !StringUtils.isEmpty(response)){
							ObjectMapper mapper = new ObjectMapper();
							JOLOOperatorDetailResponseDTO joloOpeatorCodes = mapper.readValue(response.trim(), JOLOOperatorDetailResponseDTO.class);
							if(null!=joloOpeatorCodes){
								operator = createOperatorObject(joloOpeatorCodes);
								joloNumberOperatorMapperThread.setMobileNumber(mobileNumber);
								joloNumberOperatorMapperThread.setOperator(operator);
								taskExecutor.execute(joloNumberOperatorMapperThread);
							} 
							return operator;
					}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
				LOGGER.error("Issue in JOLO get operator details API ==>MobileNumber=>"+mobileNumber);
			    emailSenderService.sendEmail(env.getProperty(PropertyKeyConstants.JOLOAPIProperties.RECHARGE_EXCEPTION), env.getProperty(PropertyKeyConstants.JOLOAPIProperties.RECHARGE_EXCEPTION_MAILLIST).split(Constants.COMMA), "Error in get operator details==>MobileNumber="+mobileNumber, e.getMessage(), null, null, false);
			}
		return operator;
	}
	
	public Operator getOperatorByNumberFromDB(String mobileNumber){
		return rechargeDao.getOperatorByNumber(mobileNumber);
	}
	
	public void saveOperatorNumberMapping(String mobileNumber,Operator operator){
		rechargeDao.saveJoloOperatorNumberMapping(mobileNumber, operator);
	}
	
	private String setCommonAPIParameter(){
		String apiUrl = Constants.BLANK;
	    apiUrl += Constants.AMPERSAND + JOLOAPIConstants.USERID + Constants.EQUALS + env.getProperty(PropertyKeyConstants.JOLOAPIProperties.JOLOAPI_USERNAME) + Constants.AMPERSAND;
	    apiUrl += JOLOAPIConstants.KEY + Constants.EQUALS + env.getProperty(PropertyKeyConstants.JOLOAPIProperties.JOLOAPI_KEY) + Constants.AMPERSAND ;
	    apiUrl += JOLOAPIConstants.TYPE + Constants.EQUALS + JOLOAPIConstants.JSON + Constants.AMPERSAND;
	    return apiUrl;
	}
	
	/**
	 * Create operator object operator and circle find api.
	 * @param joloOpeatorCodes
	 * @return
	 */
	private Operator createOperatorObject(JOLOOperatorDetailResponseDTO joloOpeatorDetailsDto){
		Operator operator = new Operator();
		JOLOCircle joloCirclecode = null;
		joloCirclecode = getCircleByCode(joloOpeatorDetailsDto.getCircle_code());
		JOLOOperator joloOpeatorCodes = getJoloOperatorByCode(joloOpeatorDetailsDto.getOperator_code());
		operator.setCircle(joloCirclecode==null?Constants.BLANK:joloCirclecode.getCircleName());
		operator.setCircle_code(joloCirclecode==null?Constants.BLANK:joloCirclecode.getCircleCode());
		operator.setPay_circle_code(joloCirclecode==null?Constants.BLANK:joloCirclecode.getCircleCode());
		operator.setCode(joloOpeatorCodes==null?Constants.BLANK:joloOpeatorCodes.getCode());
		operator.setOperator(joloOpeatorCodes==null?Constants.BLANK:joloOpeatorCodes.getOperatorName());
		operator.setPay_code(joloOpeatorCodes==null?Constants.BLANK:joloOpeatorCodes.getPay_code());
		return operator;
	}
	

	/**
	 * Mobile Recharge
	 * 1. Get operator details by Mobile Number.
	 * 2. operator details pass in recharge api 
	 * 3. get recharge API response.
	 * Request Url : https://joloapi.com/api/recharge.php?mode=0&userid=9785205444&key=986286939135988&operator=AT&service=9782091210&amount=10&orderid=CAMRECH2010&type=json 
	 * Response : {"status":"SUCCESS","error":"0","txid":"Z613649229110","operator":"AT","service":"9782091210","amount":"10","orderid":"CAMRECH2010","operatorid":"0","balance":null,"margin":null,"time":"February 22 2016 07:02:49 PM"}
	 */
	public RechargeDTO mobileDthAndDataCardRecharge(RechargeDTO rechargeDto,String orderId,UserDTO userDto){
		Operator operator = mobileRechargeService.createOperatorObject(rechargeDto);
		try{
			String response = Constants.BLANK;
			if(null!=operator && null!=userDto){
				rechargeDto = mobileRechargeService.setValuesInrechargeDto(rechargeDto.getAmount(), userDto, operator,rechargeDto.getNumber(),orderId,rechargeDto);
				if(isJoloOperatorNotAllowedRechargeTime(rechargeDto)){
					  return rechargeThroughPay2All(rechargeDto,true);
				}else{
					  rechargeDto.setRechargeServiceName(JOLOAPIConstants.JOLOAPI);
					  response = rechargeResponse(rechargeDto);
				}
				LOGGER.info("mobileDthAndDataCardRecharge ==>rechargeAPI response=>"+response);
				if(response!=null && !StringUtils.isEmpty(response)){
					ObjectMapper mapper = new ObjectMapper();
					JOLORechargeResponseDTO joloRechargeResponseDTO = mapper.readValue(response.trim(), JOLORechargeResponseDTO.class);
					if(null!=joloRechargeResponseDTO && joloRechargeResponseDTO.getStatus().equalsIgnoreCase(JOLOAPIConstants.SUCCESS)){
						String rechargeOrderId = joloRechargeResponseDTO.getTxid();
						rechargeDto.setRechargeOrderId(rechargeOrderId);
						rechargeDto.setIsSuccesfulRecharge(1);
						saveMobileRechareDetails(rechargeDto);
						generateRechargeCouponCashback(rechargeDto);
						int rechargeCount = userDto.getRechargeCount() + 1;
						userDto.setRechargeCount(rechargeCount);
						userService.updateUserRechargeAccount(userDto);
						generateRechargeUtilityThread.setRechargeDto(rechargeDto);
						taskExecutor.execute(generateRechargeUtilityThread);
						return rechargeDto;
					}else{
						setJoloErrorDetail(joloRechargeResponseDTO, rechargeDto);
						if(isPrepaidMobileService(rechargeDto)){
						   rechargeDto = rechargeThroughPay2All(rechargeDto, false);
						}
					}
				}
			   return rechargeDto;
			}
		}catch(Exception e){
			e.printStackTrace();
			LOGGER.info("Error in JOLO Recharge API==>mobileNumber="+rechargeDto.getNumber() + " ==>Amount="+rechargeDto.getAmount());
		    emailSenderService.sendEmail(env.getProperty(PropertyKeyConstants.JOLOAPIProperties.RECHARGE_EXCEPTION), env.getProperty(PropertyKeyConstants.JOLOAPIProperties.RECHARGE_EXCEPTION_MAILLIST).split(Constants.COMMA), "Error in get operator details==>MobileNumber="+rechargeDto.getNumber(), e.getMessage(), null, null, false);
		}
		return null;
	}
	
	/**
	 * Create Recharge API url
	 * 1. Check recharge Service and mobile recharge service(Prepaid and postpaid)
	 *   (i) if mobile recharge service is postapid then create billing recharge API Url
	 *        https://joloapi.com/api/cbill.php?mode=0&userid=9785205444&key=986286939135988&operator=APOS&service=9602646089&amount=100&std=stdcode&ca=&orderid=CAM12345&type=json
	 *        {"status":"SUCCESS","error":"0","txid":"Z267182091873","operator":"APOS","service":"9602646089","amount":"100","orderid":"CAM12345","operatorid":"0","balance":null,"margin":null,"time":"February 24 2016 09:58:09 PM"}
	 *   (ii) if mobile recharge service is not postapid then create Recharge recharge API Url
	 *        Request Url : https://joloapi.com/api/recharge.php?mode=0&userid=9785205444&key=986286939135988&operator=AT&service=9782091210&amount=10&orderid=CAMRECH2010&type=json 
	 *        Response : {"status":"SUCCESS","error":"0","txid":"Z613649229110","operator":"AT","service":"9782091210","amount":"10","orderid":"CAMRECH2010","operatorid":"0","balance":null,"margin":null,"time":"February 22 2016 07:02:49 PM"}
	 * @param rechargeDTO
	 * @return
	 * @throws ParseException 
	 */
	private String rechargeResponse(RechargeDTO rechargeDTO) throws ParseException{
		String response = Constants.BLANK;
		String apiUrl = Constants.BLANK;
		if(null!=rechargeDTO.getServiceType() && rechargeDTO.getServiceType().equalsIgnoreCase(RechargeConstants.MOBILE) 
				&& null!=rechargeDTO.getMobileService() && rechargeDTO.getMobileService().equalsIgnoreCase(RechargeConstants.POSTPAID)){
			apiUrl = createAPIUrlForPostpaidMobile(rechargeDTO);
			LOGGER.info("rechargeResponse==>Postpaid ==>rechargeAPIUrl=>"+apiUrl);
			response = httpService.HTTPGetRequest(apiUrl);
			return response;
		}else {
			apiUrl = createRechargeAPIUrl(rechargeDTO);
			LOGGER.info("rechargeResponse ==>Mobile,DTH,Datacard==>rechargeAPIUrl=>"+apiUrl);
			response = httpService.HTTPGetRequest(apiUrl);
		}
		rechargeDao.saveRechargeLogs(JOLOAPIConstants.JOLOAPI, apiUrl, response,rechargeDTO.getServiceType());
		return response;
	}
	
	/**
	 * 
	 * If Recharge is not successful, set recharge error details rechargeDto Object.
	 * @param joloRechargeResponseDTO
	 * @param rechargeDto
	 * @return
	 */
	private RechargeDTO setJoloErrorDetail(JOLORechargeResponseDTO joloRechargeResponseDTO, RechargeDTO rechargeDto){
		JoloErrorDTO joloErrorDetail = getJoloErroDetailsByErrorCode(joloRechargeResponseDTO.getError());
		if(null!=joloErrorDetail){
			rechargeDto.setError(joloErrorDetail.getLocalErrorDetails());
		}
		return rechargeDto;
	}
	
	/**
	 * Create API url for postapid mobile recharge.
	 * @param rechargeDTO
	 * @return
	 */
	private String createAPIUrlForPostpaidMobile(RechargeDTO rechargeDTO){
		String apiUrl = env.getProperty(PropertyKeyConstants.JOLOAPIProperties.BILLING_RECHARGE_API);
		apiUrl += setCommonAPIParameter();
		apiUrl += Constants.OPERATOR + Constants.EQUALS + rechargeDTO.getOperatorValue() ;
		apiUrl += Constants.AMPERSAND + JOLOAPIConstants.SERVICE + Constants.EQUALS + rechargeDTO.getNumber() ; 
		apiUrl += Constants.AMPERSAND + Constants.AMOUNT + Constants.EQUALS + rechargeDTO.getAmount();
		apiUrl += Constants.AMPERSAND + JOLOAPIConstants.STD + Constants.EQUALS + Constants.SPACE_ENCODED;
		apiUrl += Constants.AMPERSAND + JOLOAPIConstants.CUSTOMER_ACCOUNT_NUMBER + Constants.EQUALS + Constants.SPACE_ENCODED;
		apiUrl += Constants.AMPERSAND + Constants.ORDERID + Constants.EQUALS + rechargeDTO.getOrderId();
		return apiUrl;
	}
	
	
	/**
	 * https://joloapi.com/api/recharge.php?mode=1&userid=yourusername&key=yourapikey&operator=operatorcode
	 * &service=mobilenumber&amount=amount&orderid=youruniqueorderid&type=text
	 * @param rechargeDTO
	 * @return
	 */
	private String createRechargeAPIUrl(RechargeDTO rechargeDTO){
		String rechargeAPIUrl = env.getProperty(PropertyKeyConstants.JOLOAPIProperties.RECHARGE_API_URL);
		rechargeAPIUrl += JOLOAPIConstants.USERID + Constants.EQUALS + env.getProperty(PropertyKeyConstants.JOLOAPIProperties.JOLOAPI_USERNAME) + Constants.AMPERSAND + 
				          JOLOAPIConstants.KEY + Constants.EQUALS + env.getProperty(PropertyKeyConstants.JOLOAPIProperties.JOLOAPI_KEY) + Constants.AMPERSAND + 
		                  JOLOAPIConstants.OPERATOR + Constants.EQUALS + rechargeDTO.getOperator().getPay_code() + Constants.AMPERSAND +
		                  JOLOAPIConstants.SERVICE + Constants.EQUALS +  rechargeDTO.getNumber() + Constants.AMPERSAND +
		                  JOLOAPIConstants.AMOUNT + Constants.EQUALS + rechargeDTO.getAmount() + Constants.AMPERSAND +
		                  JOLOAPIConstants.ORDERID + Constants.EQUALS + rechargeDTO.getOrderId() + Constants.AMPERSAND +
		                  JOLOAPIConstants.TYPE + Constants.EQUALS + JOLOAPIConstants.JSON ;
		return rechargeAPIUrl;
	}
	
	/**
	 * If Recharge is successful then
	 * Generate recharge coupon cashback.
	 * @param rechargeDto
	 */
	private void generateRechargeCouponCashback(RechargeDTO rechargeDto){
		CouponDTO couponDto = couponService.reedomCoupon(rechargeDto.getUserDto(), setValuesInCouponform(rechargeDto));
		if(null!=couponDto && couponDto.isCouponApply()){
			double couponReedomValue = couponDto.getCouponReedomValue();
			int reedomValue = (int) Math.round(couponReedomValue);
			rechargeDto.setCouponValue(reedomValue);
		}else{
			rechargeDto.setCouponName(Constants.BLANK);
		}
	}
	
	private CouponForm setValuesInCouponform(RechargeDTO rechargeDto){
		 CouponForm couponForm = new CouponForm();
		 couponForm.setAmount(rechargeDto.getAmount());
		 couponForm.setCouponServiceType(rechargeDto.getServiceType());
		 couponForm.setCouponName(rechargeDto.getCouponName());
		 couponForm.setIsCouponLogInsert(0);
		 return couponForm;
	}

	/**
	 * Save mobile recharge details
	 * @param rechargeDto
	 * @return
	 */
	public int saveMobileRechareDetails(RechargeDTO rechargeDto){
		 //Handle recharge failure Case
		 if(rechargeDto.getOperator()==null){
			 rechargeDto.setOperator(mobileRechargeService.createOperatorObject(rechargeDto));
		 }
         int rechargeId = mobileRechargeDao.saveMobileRechargeDetails(rechargeDto);
         rechargeDto.setRechargeId(rechargeId);
         return rechargeId;
	}
    
	/**
	 * Get jolo operator code list by service Type like as DTH,Datacard,Mobile prepaid and postpaid.
	 * @param serviceType
	 * @return
	 */
	@Cacheable("operatorCache")
	public List<JOLOOperator> getOperatorList(String serviceType){
		return rechargeDao.getJOLOOperatorCodesByServiceType(serviceType);
	}
	
	/**
	 * Add Mobile Plans To DB
	 */
	public void updatePlans() {
		List<JOLOOperator> operatorCodes = getOperatorList("prepaid");
		List<JOLOCircle> circleCodes = getAllCircleCodeList();
		String plansApiUrl = env.getProperty(PropertyKeyConstants.JOLOAPIProperties.OPERATOR_PLANS_API);
		for (JOLOCircle circleCode : circleCodes) {
			for (JOLOOperator operatorCode : operatorCodes) {
				for (PlansTypeEnum type : PlansTypeEnum.values()) {
					String fullApiUrl = plansApiUrl + "opt=" + operatorCode.getCode() + "&cir="
							+ circleCode.getCircleCode() + "&typ="+type.getPlanType();
					String response = httpService.HTTPGetRequest(fullApiUrl);
					if(response != null && !StringUtils.isEmpty(response)){
						try{
						GsonBuilder builder = new GsonBuilder();
						Gson gson = builder.create();
						List<JoloPlan> joloPlans = gson.fromJson(response, new TypeToken<List<JoloPlan>>(){}.getType());
						for(JoloPlan plan : joloPlans){
							LOGGER.info("Adding Plans For : Operator Code :"+operatorCode.getCode()+" Circle Code : " + circleCode.getCircleCode());
							mobileRechargeDao.updatePlansListinDB(operatorCode.getCode(), circleCode.getCircleCode(),plan.getAmount(), plan.getValidity(), plan.getDetail(), type.getPlanType());
						}
					}catch (Exception e) {
						LOGGER.error("Error in Adding Plans For : Operator Code :"+operatorCode.getCode()+" Circle Code : " + circleCode.getCircleCode(), e);
					}
					}
					
				}
			}
		}
		LOGGER.info("Plans Added.");
	}
	
	@Cacheable("plansCache")
	public Map<String, List<Plan>> getOperatorAndCircleWisePlans(String operatorCode, String circleCode){
		Map<String, List<Plan>> planMap = new HashMap<String, List<Plan>>();
		if(null!=operatorCode && null!=circleCode){
			for(PlansTypeEnum type : PlansTypeEnum.values()){
				List<Plan> planList = mobileRechargeDao.getMobilePlansByTypeOperatorAndZone(type.getPlanType(), operatorCode, circleCode);
				planMap.put(type.getPlanType(), planList);
			}
		}
		return planMap;
	}

	
	@Cacheable("operatorCache")
	public JOLOOperator getJoloOperatorByCode(String code){
		return rechargeDao.getJOLOOperatorCodesByCode(code);
	}
	
	@Cacheable("operatorCache")
	public JOLOOperator getJoloOperatorByPayCode(String payCode){
		return rechargeDao.getJOLOOperatorCodesByPayCode(payCode);
	}
	
	/**
	 * In Jolo API
	 * Airtel and Vodafone 12 to 6 prepaid service down
	 * @param rechargeDTO
	 * @return
	 * @throws ParseException
	 */
	public boolean isJoloOperatorNotAllowedRechargeTime(RechargeDTO rechargeDTO) throws ParseException{
		 String currentTime = utility.getCurrentTime();
		 String startTime = env.getProperty(PropertyKeyConstants.JOLOAPIProperties.JOLO_OPERATORWISE_DOWN_STARTTIME);
		 String endTime = env.getProperty(PropertyKeyConstants.JOLOAPIProperties.JOLO_OPERATORWISE_END_STARTTIME);
		 String serviceDownOperatorName = env.getProperty(PropertyKeyConstants.JOLOAPIProperties.JOLO_OPERATORNAME_SERVICEDOWN);
		 String currentOperatorName = Constants.COMMA + rechargeDTO.getOperatorName() + Constants.COMMA;
		 boolean isOperatorNotAllowedRechargeTime = utility.isTimeBetweenTwoTime(startTime, endTime, currentTime);
		 if(isOperatorNotAllowedRechargeTime && null!=rechargeDTO.getServiceType() && 
				 rechargeDTO.getServiceType().equalsIgnoreCase(RechargeConstants.MOBILE) && null!=rechargeDTO.getMobileService() 
				 && rechargeDTO.getMobileService().equalsIgnoreCase(RechargeConstants.PREPAID) && serviceDownOperatorName.contains(currentOperatorName) ){
		     return true;
		 }
		 return false;
	}
	
	/**
	 * Check mobile number service is prepaid or not.
	 * @param rechargeDTO
	 * @return
	 */
	private boolean isPrepaidMobileService(RechargeDTO rechargeDTO){
		if(null!=rechargeDTO.getServiceType() && 
				 rechargeDTO.getServiceType().equalsIgnoreCase(RechargeConstants.MOBILE) && null!=rechargeDTO.getMobileService() 
				 && rechargeDTO.getMobileService().equalsIgnoreCase(RechargeConstants.PREPAID)){
			return true;
		}
		return false;
	}
	
	/**
	 * if JOLO Operator service is down then recharge through Pay2All Service API. 
	 * @param rechargeDto
	 * @return
	 */
	private RechargeDTO rechargeThroughPay2All(RechargeDTO rechargeDto,boolean isServiceDown){
		  Operator operator = mobileRechargeService.getOperatorbyNumber(rechargeDto.getNumber());
	      String currentOpertorValue = ServiceDownOperatorName.getEnumByString(rechargeDto.getOperatorValue());
	      if(isServiceDown && null!=currentOpertorValue && !currentOpertorValue.equals(Constants.BLANK)){
			      rechargeDto.setOperatorValue(currentOpertorValue);
			      operator.setPay_code(currentOpertorValue);
			      rechargeDto.setOperator(operator);
	      }else{
	    	      rechargeDto.setOperatorValue(operator.getPay_code());
		          rechargeDto.setOperator(operator); 
	      }
	      rechargeDto.setRechargeServiceName(Constants.PAY2ALL);
		  rechargeDto = mobileRechargeService.mobileDthAndDataCardRecharge(rechargeDto, rechargeDto.getOrderId(), rechargeDto.getUserDto());
	      return rechargeDto;
	}
	
	@Cacheable("errorCodeCache")
	public JoloErrorDTO getJoloErroDetailsByErrorCode(String errorCode){
		return rechargeDao.getJOLOErrorDesByCode(errorCode);
	}
	
	public List<RechargeDTO> getRechargeList(){
		return rechargeDao.getRechargeList();
	}
	
	/**
	 * This method used to , Recharge order id success or not.
     * If any case recharge is not success then recharge amount update particular user wallet balance.
	 */
	public void checkAndUpdateRechargeStatus(){
		List<RechargeDTO> rechargeList = getRechargeList();
		for (RechargeDTO rechargeDTO : rechargeList) {
			 if(null != rechargeDTO && rechargeDTO.getIsSuccesfulRecharge() == 1){
				 if(null != rechargeDTO.getRechargeServiceName() && rechargeDTO.getRechargeServiceName().equalsIgnoreCase(JOLOAPIConstants.JOLOAPI)){
					 checkAndUpdateJoloRechargeStatus(rechargeDTO);
				 }else if(null != rechargeDTO.getRechargeServiceName() && rechargeDTO.getRechargeServiceName().equalsIgnoreCase(Constants.PAY2ALL)){
					 checkAndUpdatePay2AllRechargeAPIStatus(rechargeDTO);
				 }
			 }
		}
	}
	
	/**
	 * 
	 * User referral balance , As per recharge amount 
	 */
	public int calculatePaidAmountUserReferralAmountCase(int paidAmount){
		 int referralCashBackPercent = utility.convertStringToInt(env.getProperty(PropertyKeyConstants.JOLOAPIProperties.JOLO_REFERRAL_RECHARGE_PERCENT));
		 System.out.println(Math.round((Float.parseFloat(String.valueOf(paidAmount)) * Float.parseFloat(String.valueOf(referralCashBackPercent))/100)));
		 int referralCashBackAmount = Math.round((Float.parseFloat(String.valueOf(paidAmount)) * Float.parseFloat(String.valueOf(referralCashBackPercent))/100));
		 return paidAmount - referralCashBackAmount;
	}
	
   /**
    * This method used to , Recharge order id success or not.
    * If any case recharge is not success then recharge amount update particular user wallet balance.
    * https://joloapi.com/api/rechargestatus.php?userid=9785205444&key=986286939135988&servicetype=1&type=json&txn=Z235661643773
    * {"status":"SUCCESS","error":"0","txid":"Z235661643773","clientid":"CAMRECH100513","service":"9838508994","amount":"20","time":"July 18 2016 07:42:42 PM","margin":"0.36","operatorid":"16440711906"}
    * @param rechargeDto
    */
   private void checkAndUpdateJoloRechargeStatus(RechargeDTO rechargeDto){
	   try{
		   String rechargeOrderId = rechargeDto.getRechargeOrderId();
		   String rechargeUrl = env.getProperty(PropertyKeyConstants.JOLOAPIProperties.JOLO_RECHARGE_STATUS_URL);
		   String serviceType = Constants.BLANK;
		   if(rechargeDto.getServiceType().equalsIgnoreCase(Constants.MOBILE)){
			   serviceType = "1";
		   }else if(rechargeDto.getServiceType().equalsIgnoreCase(Constants.DTH)){
			   serviceType = "2";
		   }else if(rechargeDto.getServiceType().equalsIgnoreCase(Constants.DATACARD)){
			   serviceType = "3";
		   }else if(rechargeDto.getServiceType().equalsIgnoreCase(Constants.POSTPAID)){
			   serviceType = "4";
		   }
		   rechargeUrl += rechargeUrl + Constants.AMPERSAND + JOLOAPIConstants.TXN + Constants.EQUALS + rechargeOrderId + 
				   Constants.AMPERSAND + JOLOAPIConstants.SERVICE_TYPE + Constants.EQUALS + serviceType;
		   String response = httpService.HTTPGetRequest(rechargeUrl);
		   if(!StringUtils.isEmpty(response)){
			   ObjectMapper mapper = new ObjectMapper();
			   JOLORechargeAPIStatusDTO joloRechargeAPIStatusDTO = mapper.readValue(response.trim(), JOLORechargeAPIStatusDTO.class);
			   if(null != joloRechargeAPIStatusDTO.getStatus() && !joloRechargeAPIStatusDTO.getStatus().equalsIgnoreCase(Constants.SUCCESS)){
				    UserDTO user = rechargeDto.getUserDto();
				    user.setWalletBalance(user.getWalletBalance() + joloRechargeAPIStatusDTO.getAmount());
				    //Set recharge is not successful
				    rechargeDto.setIsQuickRecharge(0);
				    rechargeDao.updateRechargeSatus(rechargeDto);
				    userService.updateUserWalletBalance(user);
			   }
		   }
	   }catch(Exception e){
		   e.printStackTrace();
	   }
   }
   
   private void checkAndUpdatePay2AllRechargeAPIStatus(RechargeDTO rechargeDto){
	   try{
		   String rechargeOrderId = rechargeDto.getRechargeOrderId();
		   String rechargeUrl = env.getProperty(PropertyKeyConstants.JOLOAPIProperties.PAY2ALL_RECHARGE_STATUS_URL);
		   rechargeUrl += rechargeUrl + rechargeOrderId;
		   String response = httpService.HTTPGetRequest(rechargeUrl);
		   if(!StringUtils.isEmpty(response)){
			   ObjectMapper mapper = new ObjectMapper();
			   Pay2AllRechargeAPIStatusDTO pay2AllRechargeAPIStatusDTO = mapper.readValue(response.trim(), Pay2AllRechargeAPIStatusDTO.class);
			   if(null != pay2AllRechargeAPIStatusDTO.getStatus() && !pay2AllRechargeAPIStatusDTO.getStatus().equalsIgnoreCase(Constants.SUCCESS)){
				    UserDTO user = rechargeDto.getUserDto();
				    user.setWalletBalance(user.getWalletBalance() + rechargeDto.getAmount());
				    //Set recharge is not successful
				    rechargeDto.setIsQuickRecharge(0);
				    rechargeDao.updateRechargeSatus(rechargeDto);
				    userService.updateUserWalletBalance(user);
			   }
		   }
	   }catch(Exception e){
		   e.printStackTrace();
	   }
   }
   
	public int insertAddFundsRequest(AddFundsRequestDTO fundsRequestDTO){
		return mobileRechargeDao.addFundsRequest(fundsRequestDTO);
	}
	public List<AddFundsRequestDTO> getFundsRequestByChargersId(UserDTO user){
		return mobileRechargeDao.getFundsRequestByChargersId(user);
	}
	
	public List<RechargeDTO> getSalesUserRechargeListByChargersId(UserDTO user, String dateoneDaysBefore){
		return rechargeDao.getSalesUserRechargeListByChargersId(user,dateoneDaysBefore);
	}
	
	public boolean getSalesUserRechargeListByDate(String dateoneDaysBefore,UserDTO userDTO){
		return rechargeDao.getSalesUserRechargeListByDate(dateoneDaysBefore,userDTO);
	}
	
	public void updateRechargeMarginForSalesUser(){
		try{
			DateTimeFormatter formatter = DateTimeFormat.forPattern(Constants.DateConstants.DEFAULT_DATE_FORMAT);
			String currentDate = utility.getCurrentDate(Constants.DateConstants.DEFAULT_DATE_FORMAT);
			DateTime dt = formatter.parseDateTime(currentDate).minusDays(1);
			String dateoneDaysBefore = formatter.print(dt);
			int marginPercent = utility.convertStringToInt(env.getProperty(PropertyKeyConstants.APIProperties.CAM_SALES_RECHARGE_MARGIN));
			List<UserDTO> camSalesUserList = userService.getCAMSalesUserList();
			if(!camSalesUserList.isEmpty()){
				for (UserDTO userDTO : camSalesUserList) {
					int totalRechargeAmount = 0;
					List<RechargeDTO> rechargeList = getSalesUserRechargeListByChargersId(userDTO, dateoneDaysBefore);
					if(!getSalesUserRechargeListByDate(dateoneDaysBefore,userDTO)){
						for (RechargeDTO rechargeDTO : rechargeList) {
							totalRechargeAmount = totalRechargeAmount + rechargeDTO.getAmount();
						}
						int marginAmount = Math.round((totalRechargeAmount * marginPercent)/100);
						if(marginAmount != 0){
							marginAmount = marginAmount + 1; //for sales user 
							userDTO.setWalletBalance(userDTO.getWalletBalance() + marginAmount);
							userService.updateUserWalletBalance(userDTO);
							//Save Recharge margin
							saveRechargeMarginLogs(userDTO, dateoneDaysBefore, totalRechargeAmount, marginAmount);
							EmailDTO email = rechargeMarginMailObject(userDTO, dateoneDaysBefore, totalRechargeAmount, marginAmount);
							emailSenderService.sendEmail(email);
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void saveRechargeMarginLogs(UserDTO user, String date, int totalRechargeAmount, int marginAmount){
		 rechargeDao.saveRechargeMarginLogs(user, date, totalRechargeAmount, marginAmount);
	}
	
	public List<RechargeMarginDTO> getRecahrgeMarginByChargersId(UserDTO user){
		return rechargeDao.getRechargeMarginLogs(user);
	}
	
	private EmailDTO rechargeMarginMailObject(UserDTO user,String date,int totalRechargeAmount, int marginAmount){
		String currentDate = utility.getCurrentDate(Constants.DateConstants.DEFAULT_DATE_FORMAT);
		String message = utility.classpathResourceLoader("recharge-margin.html").replace("%NAME%", user.getName()).replace("%TOTALAMOUNT%", String.valueOf(user.getWalletBalance())).replace("%AMOUNT%", String.valueOf(marginAmount)).replace("%EMAILID%", user.getEmailId()).replace("%RECHAREAMOUNT%", String.valueOf(totalRechargeAmount)).replace("%MARGINAMOUNT%", String.valueOf(marginAmount)).replace("%DATE%", currentDate);
		return new EmailDTO("Recharge Margin - Date:-"+date, user.getEmailId().split(Constants.COMMA), Constants.NOREPLY_EMAIL_ID, null, null, message, null, null);
	}
	
}
