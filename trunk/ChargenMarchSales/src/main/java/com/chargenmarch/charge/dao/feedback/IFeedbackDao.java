package com.chargenmarch.charge.dao.feedback;

import com.chargenmarch.charge.dto.feedback.FeedbackDto;

public interface IFeedbackDao {
	public int addFeedback(FeedbackDto feedbackDto);
}
