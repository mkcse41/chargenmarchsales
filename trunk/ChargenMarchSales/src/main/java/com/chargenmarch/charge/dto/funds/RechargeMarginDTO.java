package com.chargenmarch.charge.dto.funds;

public class RechargeMarginDTO {

	private int charger_id;
	private int totalRechargeAmount;
	private int marginAmount;
	private String margindate;
	public int getCharger_id() {
		return charger_id;
	}
	public void setCharger_id(int charger_id) {
		this.charger_id = charger_id;
	}
	public int getTotalRechargeAmount() {
		return totalRechargeAmount;
	}
	public void setTotalRechargeAmount(int totalRechargeAmount) {
		this.totalRechargeAmount = totalRechargeAmount;
	}
	public int getMarginAmount() {
		return marginAmount;
	}
	public void setMarginAmount(int marginAmount) {
		this.marginAmount = marginAmount;
	}
	public String getMargindate() {
		return margindate;
	}
	public void setMargindate(String margindate) {
		this.margindate = margindate;
	}
}
