package com.chargenmarch.charge.enums.recharge;

public enum StateCodes {

	ANDHRA_PRADESH("35"),
	
	ASSAM("34"),
	
	BIHAR_JHARKHAND("22"),
	
	CHENNAI("7"),
	
	CHHATTISGARH("27"),
	
	DELHI_NCR("5"),
	
	GUJARAT("12"),
	
	HARYANA("20"),
	
	HIMACHAL_PRADESH("21"),
	
	JAMMU_KASHMIR("25"),
	
	KARNATAKA("9"),
	
	KERALA("14"),
	
	KOLKATA("6"),
	
	MADHYA_PRADESH_CHHATTISGARH("16"),
	
	MAHARASHTRA("4"),
	
	MUMBAI("3"),
	
	NORTH_EAST("26"),
	
	ORISSA("23"),
	
	PUNJAB("1"),
	
	RAJASTHAN("18"),
	
	TAMIL_NADU("8"),
	
	UP_EAST("10"),
	
	//UP_WEST("11"),
	
	UP_WEST_AND_UTTARAKHAND("36"),
	
	WEST_BENGAL("2");

	String circleCode;
	
	private StateCodes(String circleCode){
		this.circleCode = circleCode;
	}

	public String getCircleCode() {
		return circleCode;
	}
}
