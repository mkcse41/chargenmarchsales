package com.chargenmarch.charge.service.mobilerecharge;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.chargenmarch.charge.dto.coupon.CouponDTO;
import com.chargenmarch.charge.dto.mobilerecharge.Operator;
import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.charge.forms.coupon.CouponForm;
import com.chargenmarch.charge.joloapi.dao.IRechargeDao;
import com.chargenmarch.charge.service.coupon.ICouponService;
import com.chargenmarch.charge.service.rewardspoint.IRewardsPointsService;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.dto.email.EmailDTO;
import com.chargenmarch.common.dto.sms.SMSDTO;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.enums.sms.SMSTemplate;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.service.sms.ISMSService;
import com.chargenmarch.common.service.user.IUserService;
import com.chargenmarch.common.utility.IUtility;

/**
 * This thread used to earn rewards points amount wise.
 * @author mukesh
 *
 */
@Component
@Scope(Constants.PROTOTYPE)
public class GenerateRechargeUtilityThread extends Thread {

	private RechargeDTO rechargeDto;
	
	@Autowired
	private IRewardsPointsService rewardsPointService;
	@Autowired
	private IUserService userService;
	@Autowired
	private ICouponService couponService;
	@Autowired
	private IUtility util;
	@Autowired
	private IEmailSenderService emailService;
	@Autowired
	private ISMSService smsService;
	
	@Autowired
	IRechargeDao rechargeDao;
	
	@Autowired
	IMobileRechargeService mobileRechargeService;
	
	public void run(){
		try{
			rewardsPointService.earnRewardsPoint(rechargeDto.getUserDto(), rechargeDto.getAmount(),rechargeDto.getRechargeId());
			userService.updateUserRechargeAccount(rechargeDto.getUserDto());
			if(null!=rechargeDto.getCouponName() && !rechargeDto.getCouponName().equals(Constants.BLANK)){
				CouponDTO couponDto = couponService.reedomCoupon(rechargeDto.getUserDto(), setValuesInCouponform(rechargeDto));
				if(null!=couponDto && couponDto.isCouponApply()){
					double couponReedomValue = couponDto.getCouponReedomValue();
					int reedomValue = (int) Math.round(couponReedomValue);
					rechargeDto.getUserDto().setWalletBalance(rechargeDto.getUserDto().getWalletBalance() + reedomValue);
					userService.updateUserWalletBalance(rechargeDto.getUserDto());
					couponService.updateCouponRedeemLogs(rechargeDto.getUserDto(), couponDto, rechargeDto.getRechargeId());
				}
			}
			
			/**
			 * Update referral balance.
			 */
			UserDTO user = rechargeDto.getUserDto();
			UserDTO referrerUser = userService.getReferrerByUser(user);
			if(null != referrerUser && user.getRechargeCount() < 3){
				int referralAmount = referrerUser.getReferralBalance() + getReferralAmount(rechargeDto.getAmount());
				referrerUser.setReferralBalance(referralAmount);
				userService.updateUserRefferalBalance(referrerUser);
		    }
			
			//send recharge message
			SMSDTO smsDto = smsService.getSMSObjectByUserAndSMSTemplate(rechargeDto.getUserDto(), SMSTemplate.RECHARGE);
			smsDto.getParameterMap().put(1, rechargeDto.getServiceType());
			smsDto.getParameterMap().put(2, String.valueOf(rechargeDto.getAmount()));
			smsDto.getParameterMap().put(3, rechargeDto.getOrderId());
			smsService.sendSMS(smsDto);
			
			//send invoice
			try{
				EmailDTO email = getInvoiceMailObject(rechargeDto);
				emailService.sendEmail(email);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			//recharge cashback
			if(null != rechargeDto.getUserDto() && rechargeDto.getUserDto().getIsSalesUser() == 0){
                 mobileRechargeService.generateRechargeCashBackByUser(rechargeDto.getUserDto());
			}
			
			try{
				Operator operator = rechargeDao.getOperatorByNumber(rechargeDto.getNumber());
				//update operator
				if(null != operator){
					operator.setOperator(rechargeDto.getOperator().getOperator());
					operator.setPay_code(rechargeDto.getOperator().getPay_code());
				    rechargeDao.updateJoloOperatorNumberMapping(rechargeDto.getNumber(), operator);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
				
	}
	
	private EmailDTO getInvoiceMailObject(RechargeDTO rechargeDto){
		String message = util.classpathResourceLoader("invoice.html").replace("%NAME%", rechargeDto.getUserDto().getName()).replace("%EMAILID%", rechargeDto.getUserDto().getEmailId()).replace("%TYPE%", rechargeDto.getServiceType()).replace("%NUMBER%", rechargeDto.getNumber()).replace("%AMOUNT%", String.valueOf(rechargeDto.getAmount())).replace("%ORDERID%", rechargeDto.getOrderId());
		return new EmailDTO("Invoice for "+rechargeDto.getServiceType()+" Recharge - Order Id :"+rechargeDto.getOrderId(), rechargeDto.getUserDto().getEmailId().split(Constants.COMMA), Constants.NOREPLY_EMAIL_ID, null, null, message, null, null);
	}
	
	private CouponForm setValuesInCouponform(RechargeDTO rechargeDto){
		 CouponForm couponForm = new CouponForm();
		 couponForm.setAmount(rechargeDto.getAmount());
		 couponForm.setCouponServiceType(rechargeDto.getServiceType());
		 couponForm.setCouponName(rechargeDto.getCouponName());
		 couponForm.setIsCouponLogInsert(1);
		 return couponForm;
	}

	public void setRechargeDto(RechargeDTO rechargeDto) {
		this.rechargeDto = rechargeDto;
	}
	
	private int getReferralAmount(int amount) {
		return (int) Math.round(amount* 0.05);
	}
}
