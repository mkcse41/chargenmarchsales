
package com.chargenmarch.charge.enums.recharge;

import java.util.HashMap;
import java.util.Map;

public enum PlanZoneCode {
	AP ("ANDHRA PRADESH"),
	AS ("ASSAM"),
	BR ("BIHAR JHARKHAND"),
	CH ("CHENNAI"),
	DL ("DELHI NCR"),
	GJ ("GUJARAT"),
	HP ("HIMACHAL PRADESH"),
	HR ("HARYANA"),
	JK ("JAMMU KASHMIR"),
	KL ("KERALA"),
	KA ("KARNATAKA"),
	KO ("KOLKATA"),
	MH ("MAHARASHTRA"),
	MP ("MADHYA PRADESH CHHATTISGARH"),
	MU ("MUMBAI"),
	NE ("NORTH EAST"),
	OR ("ORISSA"),
	PB ("PUNJAB"),
	RJ ("RAJASTHAN"),
	TN ("TAMIL NADU"),
	UE ("UP EAST"),
	UW ("UP WEST AND UTTARAKHAND"),
	WB("WEST BENGAL");
	
	// Reverse-lookup map for getting a day from an abbreviation
    private static final Map<String, PlanZoneCode> lookup = new HashMap<String, PlanZoneCode>();
    
	  static {
	        for (PlanZoneCode d : PlanZoneCode.values()) {
	            lookup.put(d.getZoneName(), d);
	        }
	    }
	  
	private String zoneName;       

    private PlanZoneCode(String s) {
    	zoneName = s;
    }
    
    public String getZoneName(){
    	return zoneName;
    }
    
    public static PlanZoneCode get(String zoneName) {
        return lookup.get(zoneName);
    }
}
