package com.chargenmarch.charge.mapper.recharge;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.service.user.IUserService;

@Component
@Scope("prototype")
public class RechargeMapper implements RowMapper<RechargeDTO> {

	@Autowired
	IUserService userService;
	
	@Override
	public RechargeDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		RechargeDTO recharge = new RechargeDTO();
        recharge.setId(rs.getInt("id"));
		recharge.setNumber(rs.getString("mobilenumber"));
		recharge.setAmount(rs.getInt("rechargeAmount"));
		recharge.setIsSuccesfulRecharge(rs.getInt("isSuccessfullRecharge"));
		if(recharge.getIsSuccesfulRecharge()==1){
			recharge.setRechargeOrderId(rs.getString("rechargeOrderId"));
		} else {
			recharge.setRechargeOrderId(Constants.BLANK);
		}
		recharge.setOrderId(rs.getString("PGOrderId"));
		recharge.setUserDto(userService.getUserInfoMapById().get((rs.getInt("chargers_id"))));
		recharge.setOperatorName(rs.getString("operator"));
		recharge.setRechargeDate(new DateTime(rs.getDate("inserttimestamp")));
		recharge.setServiceType(rs.getString("rechargeService"));
		recharge.setRechargeServiceName(rs.getString("rechargeServiceName"));
		return recharge;
	}

}
