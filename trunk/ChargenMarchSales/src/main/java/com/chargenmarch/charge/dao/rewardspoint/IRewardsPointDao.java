/**
 * 
 */
package com.chargenmarch.charge.dao.rewardspoint;

import java.util.List;

import com.chargenmarch.charge.dto.rewardspoint.RewardsPoint;

/**
 * @author mukesh
 *
 */
public interface IRewardsPointDao {

	public int insertRewardsPoint(RewardsPoint rewardsPoint);
	public void insertRewardsPointLogs(RewardsPoint rewardsPoint);
	public void insertReedomRewardsPointLogs(RewardsPoint rewardsPoint);
	public void updateRewardsPoint(RewardsPoint rewardsPoint);
	public List<RewardsPoint> getRewardsPoint();
}
