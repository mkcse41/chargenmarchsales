package com.chargenmarch.charge.joloapi.dto.billing;

import org.joda.time.DateTime;

import com.chargenmarch.common.dto.user.UserDTO;

public class LandLineRechargeDTO {

	private int id;
	private UserDTO userDto;
	private String landlinenumber;
	private String stdcode;
	private String accountnumber;
	private int amount;
	private int isSuccesfulRecharge;
	private int rechargeId;
	private String orderId;
	private String rechargeOrderId;
	private String couponName;
	private String serviceType;
	private String operatorValue;
	private String operatorName;
	private int couponValue;
	private boolean isUseWallet;
	private DateTime rechargeDate;
	private boolean isRedeemRewardsPoints=false;
	private int redeemRewardsPointsValue;
	private int isQuickRecharge = 0;
	private int internetCharges = 0;
	private String error;
	private String sessionId;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public UserDTO getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDTO userDto) {
		this.userDto = userDto;
	}
	public String getLandlinenumber() {
		return landlinenumber;
	}
	public void setLandlinenumber(String landlinenumber) {
		this.landlinenumber = landlinenumber;
	}
	public String getStdcode() {
		return stdcode;
	}
	public void setStdcode(String stdcode) {
		this.stdcode = stdcode;
	}
	public String getAccountnumber() {
		return accountnumber;
	}
	public void setAccountnumber(String accountnumber) {
		this.accountnumber = accountnumber;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getIsSuccesfulRecharge() {
		return isSuccesfulRecharge;
	}
	public void setIsSuccesfulRecharge(int isSuccesfulRecharge) {
		this.isSuccesfulRecharge = isSuccesfulRecharge;
	}
	public int getRechargeId() {
		return rechargeId;
	}
	public void setRechargeId(int rechargeId) {
		this.rechargeId = rechargeId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getRechargeOrderId() {
		return rechargeOrderId;
	}
	public void setRechargeOrderId(String rechargeOrderId) {
		this.rechargeOrderId = rechargeOrderId;
	}
	public String getCouponName() {
		return couponName;
	}
	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getOperatorValue() {
		return operatorValue;
	}
	public void setOperatorValue(String operatorValue) {
		this.operatorValue = operatorValue;
	}
	public String getOperatorName() {
		return operatorName;
	}
	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}
	public int getCouponValue() {
		return couponValue;
	}
	public void setCouponValue(int couponValue) {
		this.couponValue = couponValue;
	}
	public boolean isUseWallet() {
		return isUseWallet;
	}
	public void setUseWallet(boolean isUseWallet) {
		this.isUseWallet = isUseWallet;
	}
	public DateTime getRechargeDate() {
		return rechargeDate;
	}
	public void setRechargeDate(DateTime rechargeDate) {
		this.rechargeDate = rechargeDate;
	}
	public boolean isRedeemRewardsPoints() {
		return isRedeemRewardsPoints;
	}
	public void setRedeemRewardsPoints(boolean isRedeemRewardsPoints) {
		this.isRedeemRewardsPoints = isRedeemRewardsPoints;
	}
	public int getRedeemRewardsPointsValue() {
		return redeemRewardsPointsValue;
	}
	public void setRedeemRewardsPointsValue(int redeemRewardsPointsValue) {
		this.redeemRewardsPointsValue = redeemRewardsPointsValue;
	}
	public int getIsQuickRecharge() {
		return isQuickRecharge;
	}
	public void setIsQuickRecharge(int isQuickRecharge) {
		this.isQuickRecharge = isQuickRecharge;
	}
	public int getInternetCharges() {
		return internetCharges;
	}
	public void setInternetCharges(int internetCharges) {
		this.internetCharges = internetCharges;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	
}
