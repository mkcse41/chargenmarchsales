package com.chargenmarch.charge.mapper.mobilerecharge;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.charge.constants.recharge.RechargeConstants;
import com.chargenmarch.charge.dto.mobilerecharge.Operator;

public class OperatorMapper implements RowMapper<Operator>{
	
	@Override
	public Operator mapRow(ResultSet rs, int rowNum) throws SQLException {
		Operator operator = new Operator();
		operator.setOperator(rs.getString(RechargeConstants.OPERATOR));
		operator.setCode(rs.getString(RechargeConstants.CODE));
		operator.setCircle(rs.getString(RechargeConstants.CIRCLE));
		operator.setCircle_code(rs.getString(RechargeConstants.CIRCLE_CODE));
		operator.setPay_circle_code(rs.getString(RechargeConstants.PAY_CIRCLE_CODE));
		operator.setPay_code(rs.getString(RechargeConstants.PAY_CODE));
		return operator;
	}
	
}
