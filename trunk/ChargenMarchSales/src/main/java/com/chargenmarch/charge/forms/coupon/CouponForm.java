/**
 * 
 */
package com.chargenmarch.charge.forms.coupon;

import java.sql.Date;

/**
 * @author mukesh
 *
 */
public class CouponForm {

	private String couponName;
	private String couponType;
	private double couponValue;
	private int couponUses;
	private int couponLimit;
	private int minimumRechargeValue;
	private String couponServiceType;
	private int couponPerUserLimit;
	private int couponStatus;
	private Date startDate;
	private Date endDate;
	private String emailId;
	private String mobileNo;
	private double amount;
	private int isCouponLogInsert = 0;
	private int maximumcashback;
	
	public String getCouponName() {
		return couponName;
	}
	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}
	public String getCouponType() {
		return couponType;
	}
	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}
	public double getCouponValue() {
		return couponValue;
	}
	public void setCouponValue(double couponValue) {
		this.couponValue = couponValue;
	}
	public int getCouponUses() {
		return couponUses;
	}
	public void setCouponUses(int couponUses) {
		this.couponUses = couponUses;
	}
	public int getCouponLimit() {
		return couponLimit;
	}
	public void setCouponLimit(int couponLimit) {
		this.couponLimit = couponLimit;
	}
	public int getMinimumRechargeValue() {
		return minimumRechargeValue;
	}
	public void setMinimumRechargeValue(int minimumRechargeValue) {
		this.minimumRechargeValue = minimumRechargeValue;
	}
	public String getCouponServiceType() {
		return couponServiceType;
	}
	public void setCouponServiceType(String couponServiceType) {
		this.couponServiceType = couponServiceType;
	}
	public int getCouponPerUserLimit() {
		return couponPerUserLimit;
	}
	public void setCouponPerUserLimit(int couponPerUserLimit) {
		this.couponPerUserLimit = couponPerUserLimit;
	}
	public int getCouponStatus() {
		return couponStatus;
	}
	public void setCouponStatus(int couponStatus) {
		this.couponStatus = couponStatus;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public int getIsCouponLogInsert() {
		return isCouponLogInsert;
	}
	public void setIsCouponLogInsert(int isCouponLogInsert) {
		this.isCouponLogInsert = isCouponLogInsert;
	}
	public int getMaximumcashback() {
		return maximumcashback;
	}
	public void setMaximumcashback(int maximumcashback) {
		this.maximumcashback = maximumcashback;
	}
	
	
	
	
	
}
