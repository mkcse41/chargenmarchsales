
package com.chargenmarch.charge.query.coupon;

/**
 * @author mukesh
 *
 */
public class CouponQuery {
 
	public static final String SELECT_FROM_COUPON = "Select * From coupon where coupon_status=0";
	public static final String INSERT_INTO_COUPON = "insert into coupon set coupon_name=?,coupon_type=?,coupon_value=?,coupon_limit=?,minimum_recharge_value=?,coupon_service_type=?,start_date=?,end_date=?,coupon_per_user_limit=?,maximumcashback=?,inserttimestamp=now()";
	public static final String SELECT_FROM_COUPON_BY_COUPON_NAME = "Select * From coupon where coupon_status=0 and coupon_name=? and coupon_service_type=?";
	public static final String INSERT_INTO_REEDOM_COUPON_LOGS = "insert into coupon_reedom_logs set chargers_id=?,coupon_id=?,coupon_value=?,rechargeId=?,inserttimestamp=now(),updatetimestamp=now()";
	public static final String SELECT_FROM_REEDOM_COUPON_LOGS_BY_USER = "Select * From coupon_reedom_logs where rechargeId!=0 and chargers_id=? and coupon_id=?";
	public static final String UPDATE_REEDOM_COUPON_LOGS = "update coupon_reedom_logs set rechargeId=? where chargers_id=? and coupon_id=?";

}
