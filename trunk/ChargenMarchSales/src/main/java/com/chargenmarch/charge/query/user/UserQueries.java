package com.chargenmarch.charge.query.user;

public class UserQueries {
	public static final String INSERT_USER_DETAILS = "insert into charger_details set name=?,emailid=?,mobileno=?,source=?,password=?,isCAMlogin=?,registrationdate=now(),updatedate=now(),rechargecount=0,walletbal=0,referralCode=?";
	public static final String UPDATE_USER_DETAILS = "update charger_details set name=?,mobileno=?,source=?,password=?,isCAMlogin=?,registrationdate=now(),updatedate=now(),rechargecount=0,walletbal=0 where emailid=?";		
	public static final String SELECT_FROM_USERS_BY_MOBILE = "select * from charger_details where mobileno=?";
	public static final String SELECT_FROM_USERS_BY_EMAILID = "select * from charger_details where emailid=?";
	public static final String SELECT_FROM_USERS_BY_ID = "select * from charger_details where id=?";
	public static final String SELECT_ALL_USERS = "select * from charger_details";
	public static final String UPDATE_USER_WALLET_BAL = "update charger_details set walletbal=? where id=?";
	public static final String UPDATE_USER_RECHARGE_COUNT = "update charger_details set rechargecount=? where id=?";
	public static final String UPDATE_USER_PASSWORD = "update charger_details set password=? where id=?";
	public static final String UPDATE_USER_VERIFIED = "update charger_details set isverified = ? where emailid=?";
	public static final String SELECT_FROM_USERS_BY_VERIFICATION_STATUS = "select * from charger_details where isverified=?";
	public static final String SELECT_FROM_USERS_BY_REGISTRATION_DATE = "select * from charger_details where registrationdate like ?";
	public static final String INSERT_USER_REFERRAL_LOGS = "insert into user_referral_logs set chargers_id=?,referrer_chargers_id=?,referral_cashback_amount=?,insertTimestamp=now(),updateTimestamp=now()";
	public static final String UPDATE_USER_REFERRAL_CODE= "update charger_details set referralCode=? where id=?";
	public static final String GET_USER_REFERRAL_LOGS = "select * from user_referral_logs where chargers_id=?";
	public static final String INSERT_EMAIL_LOGS_LOGS = "insert into send_email_logs set chargers_id=?,insertTimestamp=now(),updateTimestamp=now()";
	public static final String TRUNCATE_EMAIL_LOGS = "truncate send_email_logs";
	public static final String GET_USER_REFERRAL_AMOUNT_BY_USERID = "select sum(referral_cashback_amount) as referral_cashback_amount from user_referral_logs where referrer_chargers_id=?";
	public static final String SELECT_FROM_CHARGERS_DETAILS = "select * from charger_details where walletbal > 0 and referralAmount > 0";
	public static final String UPDATE_USER_REFERRAL_BAL = "update charger_details set referralAmount=? where id=?";
	public static final String GET_USER_PAYMENT_INFO_BY_EMAILID = "select sum(amount) from payment_order where status='success' and emailid=?";
	public static final String SELECT_FROM_CHARGERS_DETAILS_WALLET = "select * from charger_details where walletbal > 0";
	public static final String GET_REFERRER_USER_ID = "select referrer_chargers_id from user_referral_logs where chargers_id=?";
	public static final String SAVE_BROWSER_NOTIFICATION_LOGS = "insert into browser_notification_logs set charger_id=?,browser_notification_id=?,timestamp=now(),updatetimestamp=now()";
	public static final String UPDATE_BROWSER_NOTIFICATION_LOGS = "update browser_notification_logs set browser_notification_id=?,updatetimestamp=now() where charger_id=?";
	public static final String IS_EXISTS_BROWSER_NOTIFICATION_LOGS_BY_CHARGERS_ID = "select * from browser_notification_logs where charger_id=?";
	public static final String GET_BROWSER_NOTIFICATION_LOGS = "select * from browser_notification_logs";
	public static final String SELECT_FROM_CAM_SALES_USER = "select * from charger_details where isSalesUser = 1";
}
