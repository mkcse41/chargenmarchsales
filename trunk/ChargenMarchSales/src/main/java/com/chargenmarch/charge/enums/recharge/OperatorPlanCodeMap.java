package com.chargenmarch.charge.enums.recharge;

public enum OperatorPlanCodeMap {
	      AIR("AC"),
			A("AT"),
		   BS("CG"),
		   BT("CG"),
			I("ID"),
			MTR("DP"),
			MTT("DP"),
			M("MT"),
			RC("RC"),
			RG("RG"),
			D("TD"),
			DS("TD"),
			UN("UN"),
			V("VD"),
			VD("VD");

	private String opName;       

    private OperatorPlanCodeMap(String s) {
    	opName = s;
    }
    
    public String getOperatorName(){
    	return opName;
    }
}

