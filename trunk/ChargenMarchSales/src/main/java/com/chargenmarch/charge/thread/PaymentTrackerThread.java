package com.chargenmarch.charge.thread;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.dto.user.PaymentLogDto;
import com.chargenmarch.common.service.user.IPaymentLogService;

/**
 * @author gambit
 * Thread to handle user activity tracking across the website
 */

@Component
@Scope(Constants.PROTOTYPE)
public class PaymentTrackerThread implements Runnable{

	@Autowired
	IPaymentLogService trackingService;
	
	private PaymentLogDto trackingDto;
	private boolean update;
	private HttpSession session;
	
	public PaymentLogDto getTrackingDto() {
		return trackingDto;
	}
	public void setTrackingDto(PaymentLogDto trackingDto) {
		this.trackingDto = trackingDto;
	}
	
	public boolean isUpdate() {
		return update;
	}
	public void setUpdate(boolean update) {
		this.update = update;
	}
	public HttpSession getSession() {
		return session;
	}
	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	@Override
	public void run() {
		if(update) {
			trackingService.updatePaymentLog(trackingDto);
		} else {
			int id = trackingService.logPaymentActivity(trackingDto);
			trackingDto.setId(id);
			session.setAttribute("plid", id);
		}
	}
}
