package com.chargenmarch.charge.enums.recharge;

public enum PostpaidOperatorCodes {

	AP("AIRTEL POSTPAID"),
	AIRP("AIRCEL POSTPAID"),
	IP("IDEA POSTPAID"),
	MP("MTS POSTPAID"),
	PCL("Postpaid Cellone"),
	DP("TATA DOCOMO POSTPAID"),
	RCP("RELIANCE CDMA POSTPAID"),
	RGP("RELIANCE GSM POSTPAID"),
	VP("VODAFONE POSTPAID");
	
	private String operatorName;
	
	private PostpaidOperatorCodes(String operatorName){
		this.operatorName = operatorName;
	}
	public String getOperatorName() {
		return operatorName;
	}
	
	
}
