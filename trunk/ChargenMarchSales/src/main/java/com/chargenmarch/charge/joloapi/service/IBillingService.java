package com.chargenmarch.charge.joloapi.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.charge.forms.coupon.CouponForm;
import com.chargenmarch.charge.joloapi.dto.billing.LandLineRechargeDTO;
import com.chargenmarch.charge.joloapi.forms.billing.LandlineForm;
import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.chargenmarch.common.dto.user.UserDTO;

/**
 * 
 * @author rajat
 *
 */
public interface IBillingService {
	public LandlineForm getLandLineFormObject(HttpServletRequest request);
	public int calculatePaidAmount(int rechargeAmount, int walletBalance);
	public LandLineRechargeDTO setValuesInLandlineRechargeDto(UserDTO userDto,String orderId,LandlineForm landlineForm,LandLineRechargeDTO landLineDto);
	public int calculatePaidAmountRewardsPointCase(String isReedom,UserDTO user,LandLineRechargeDTO landlineDto,int paidAmount);
	public LandLineRechargeDTO landLineBilling(LandLineRechargeDTO landlineDto,String orderId,UserDTO userDto);
	public CouponForm setValuesInCouponform(LandLineRechargeDTO landlineDto);
	public OrderInfo iniliazeOrderInfo(LandLineRechargeDTO landlineDto,HttpServletRequest request,int paidAmount);
	public void updateUserWalletBalance(LandLineRechargeDTO landlineDto,UserDTO user);
	public int saveBillingRechargeDetail(LandLineRechargeDTO landlineDTO);
	public int addExtraCharges(int rechargeAmount);
	public String getRechargeSecretKey();
	public List<RechargeDTO> getRechargeListByPGOrderId(String pgOrderId);
}
