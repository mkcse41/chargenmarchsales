package com.chargenmarch.charge.controller.coupon;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.chargenmarch.charge.dto.coupon.CouponDTO;
import com.chargenmarch.charge.forms.coupon.CouponForm;
import com.chargenmarch.charge.service.coupon.ICouponService;
import com.chargenmarch.common.constants.RequestConstants;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.service.user.IUserService;
import com.google.gson.Gson;


/**
 * This controller used to create and reedom coupon,manage coupon. 
 * @author mukesh
 *
 */

@Controller
@RequestMapping(value = RequestConstants.COUPON)
public class CouponController {

	private static final String COUPON_FORM = "couponForm";
	private final static Logger logger = LoggerFactory.getLogger(CouponController.class);
	
	@Autowired
	IUserService userService;
	
	@Autowired
	ICouponService couponService;

	//<domain-name>/coupon/redeem?couponName=CAM20&amount=10&mobileNo=9602646089&couponServiceType=mobile
	@RequestMapping(value = RequestConstants.REDEEM_COUPON, method ={ RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody String redeemCoupon(@ModelAttribute(COUPON_FORM) CouponForm couponForm) throws Exception {
		if(null!=couponForm){
		   UserDTO userDto = userService.getUserByEmailId(couponForm.getEmailId());
		   CouponDTO couponDto = couponService.reedomCoupon(userDto, couponForm); 
		   return new Gson().toJson(couponDto);
		}
		return null;
	}
	
	//<domain-name>/coupon/insert?couponName=CAM50&couponType=fixed&couponValue=50&couponUses=0&couponLimit=5&minimumRechargeValue=100&couponServiceType=mobile&couponPerUserLimit=2&couponStatus=0&startDate=2015-10-18&endDate=2015-10-28&maximumcashback=50
	@RequestMapping(value = RequestConstants.INSERT_COUPON, method = RequestMethod.GET)
	public @ResponseBody CouponDTO insertCoupon(@ModelAttribute(COUPON_FORM) CouponForm couponForm) throws Exception {
		if(null!=couponForm){
			 CouponDTO couponDto = couponService.insertCouponDetails(couponForm);
			 return couponDto;
		}
		return null;
	}
	
}
