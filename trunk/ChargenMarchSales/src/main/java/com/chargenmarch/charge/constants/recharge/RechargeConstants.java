package com.chargenmarch.charge.constants.recharge;

public class RechargeConstants {
	
	public static final String MOBILE_NUMBER = "mobileNumber";
	public static final String OPERATOR_DETAILS = "operatorDetails";
	public static final String EMAIL_ID = "emailId";
	public static final String PASSWORD = "password";
	public static final String IS_REGISTER = "isRegister";
	public static final String OTP = "otp";
	public static final String RECHARGE_ERROR_EMAIL_ID = "mukesh@chargenmarch.com, durlabh@chargenmarch.com";

	public static final String OPERATOR="operator";
	public static final String PAY_CODE="pay_code";
	public static final String CODE="code";
	public static final String CIRCLE="circle";
	public static final String PAY_CIRCLE_CODE="pay_circle_code";
	public static final String CIRCLE_CODE="circle_code";
	public static String SLASH = "/";
	
	public static final String PREPAID_OPERATORS="prepaidoperators";
	public static final String POSTPAID_OPERATORS="postpaidoperators";
	public static final String DTH_OPERATORS="dthoperators";
	public static final String DATACARD_OPERATORS="datacardoperators";
	public static final String LANDLINE_OPERATORS="landlineoperators";
	public static final String CIRCLES="circles";
	public static final String PREPAID = "prepaid";
	public static final String RAJASTHAN = "RAJASTHAN";
	public static final String RAJASTHAN_CIRCLE_CODE = "18";
	public static final String RECHARGE_SERVICE_NAME = "rechargeServiceName";
	public static final String POSTPAID = "postpaid";
	public static final String MOBILE = "mobile";
	
	
}
