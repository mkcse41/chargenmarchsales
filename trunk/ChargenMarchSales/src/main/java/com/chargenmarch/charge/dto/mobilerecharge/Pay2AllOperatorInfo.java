package com.chargenmarch.charge.dto.mobilerecharge;

/**
 * {"circle_id":29,"provider_id":2,"plan_code":"RJ","plan_provider":"VF"}
 * @author mukesh
 *
 */
public class Pay2AllOperatorInfo {

	private String circle_id;
	private String provider_id;
	private String plan_code;
	private String plan_provider;
	public String getCircle_id() {
		return circle_id;
	}
	public void setCircle_id(String circle_id) {
		this.circle_id = circle_id;
	}
	public String getProvider_id() {
		return provider_id;
	}
	public void setProvider_id(String provider_id) {
		this.provider_id = provider_id;
	}
	public String getPlan_code() {
		return plan_code;
	}
	public void setPlan_code(String plan_code) {
		this.plan_code = plan_code;
	}
	public String getPlan_provider() {
		return plan_provider;
	}
	public void setPlan_provider(String plan_provider) {
		this.plan_provider = plan_provider;
	}
	@Override
	public String toString() {
		return "Pay2AllOperatorInfo [circle_id=" + circle_id + ", provider_id="
				+ provider_id + ", plan_code=" + plan_code + ", plan_provider="
				+ plan_provider + "]";
	}
	
}
