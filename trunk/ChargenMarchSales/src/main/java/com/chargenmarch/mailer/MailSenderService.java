package com.chargenmarch.mailer;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.dao.user.IUserDao;
import com.chargenmarch.common.dto.email.EmailDTO;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.utility.IUtility;

@Service
public class MailSenderService {

	@Autowired 
	private IUtility utility;
	
	@Autowired
	private IEmailSenderService emailSenderService;
	
	@Autowired IUserDao userDao;

		
		public void sendMail(){
			String message = utility.classpathResourceLoader("LeagueStartMail.html");
			String user = null;
				List<UserDTO> allUserList = userDao.getAllUsers();
				Iterator<UserDTO> userListItr = allUserList.iterator();
				while(userListItr.hasNext()){
					UserDTO userDto = userListItr.next();
					user  = userDto.getEmailId();
					EmailDTO emailDto = new EmailDTO("CAM Cricket League - get ready to in!", user.split(Constants.COMMA), Constants.NOREPLY_EMAIL_ID, null, null, message, null, null);
					emailSenderService.sendEmail(emailDto);
				}
			
		}
}