package com.chargenmarch.common.constants;

public interface MailerKeyConstants {
	
	public interface DailyReportKey {
		public static final String TOTAL_REGISTRATION= "%TOTALREGISTRATION%";
		public static final String TOTAL_REGISTRATION_COMPUTER= "%TOTALREGISTRATIONCOMPUTER%";
		public static final String TOTAL_REGISTRATION_MOBILE= "%TOTALREGISTRATIONMOBILE%";
		public static final String TOTAL_VERIFIED_USERS= "%TOTALVERIFIEDUSERS%";
		public static final String TOTAL_UNVERIFIED_USERS= "%TOTALUNVERIFIEDUSERS%";    
		
		public static final String TOTAL_RECHARGE= "%TOTALRECHARGE%";
		public static final String SUCCESSFUL_RECHARGE= "%SUCCESSFULRECHARGE%";
		public static final String UNSUCCESSFUL_RECHARGE= "%UNSUCCESSFULRECHARGE%";
		
		public static final String TOTAL_MOBILE_RECHARGE= "%TOTALMOBILERECHARGE%";
		public static final String SUCCESSFUL_MOBILE_RECHARGE= "%SUCCESSFULMOBILERECHARGE%";
		public static final String UNSUCCESSFUL_MOBILE_RECHARGE= "%UNSUCCESSFULMOBILERECHARGE%";
		
		public static final String TOTAL_DTH_RECHARGE= "%TOTALDTHRECHARGE%";
		public static final String SUCCESSFUL_DTH_RECHARGE= "%SUCCESSFULDTHRECHARGE%";
		public static final String UNSUCCESSFUL_DTH_RECHARGE= "%UNSUCCESSFULDTHRECHARGE%";
		
		public static final String TOTAL_DATACARD_RECHARGE= "%TOTALDATACARDRECHARGE%";
		public static final String SUCCESSFUL_DATACARD_RECHARGE= "%SUCCESSFULDATACARDRECHARGE%";
		public static final String UNSUCCESSFUL_DATACARD_RECHARGE= "%UNSUCCESSFULDATACARDRECHARGE%";
		
		public static final String TOTAL_WALLET_RECHARGE= "%TOTALWALLETRECHARGE%";
		public static final String SUCCESSFUL_WALLET_RECHARGE= "%SUCCESSFULWALLETRECHARGE%";
		public static final String UNSUCCESSFUL_WALLET_RECHARGE= "%UNSUCCESSFULWALLETRECHARGE%";
	}

}
