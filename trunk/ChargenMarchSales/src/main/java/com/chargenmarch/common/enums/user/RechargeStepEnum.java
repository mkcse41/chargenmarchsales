package com.chargenmarch.common.enums.user;

public enum RechargeStepEnum {
	
	HOME_PAGE(1),
	REVIEW_ORDER(2),
	CONFIRM_ORDER(3),
	PAYMENT_RESULT(4),
	PAYMENT_RESULT_BY_WALLET(5),
	PAYMENT_RESULT_BY_REFERRAL(6),
	PAYMENT_GATEWAY(7),
	OTHER_PAGE(99);
	
	private int id;
	
	RechargeStepEnum (int id) {
		this.id=id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getStep() {
		return this.name();
	}
}
