package com.chargenmarch.common.service.email.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.dto.email.EmailDTO;
import com.chargenmarch.common.service.email.IEmailSenderService;



/**
 * 
 * @author mukesh
 *
 */
@Service("mailService")
public class EmailSenderServiceImpl implements IEmailSenderService {
    
	 private final static Logger logger = LoggerFactory.getLogger(EmailSenderServiceImpl.class);
	@Autowired
    private JavaMailSenderImpl mailSender;
     
    public void sendEmail(String from,String[] to,String sub,String msgBody,String fileName,String filePath, boolean isHTML){
         
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(to);
            helper.setSubject(sub);
            helper.setText(msgBody, isHTML);
            if(null!=fileName && fileName.equals(Constants.BLANK) &&  null!=filePath && filePath.equals(Constants.BLANK)){
              helper.addAttachment(fileName, new ByteArrayResource(getFileArray(filePath)));
            }
            mailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
            logger.error("Error in send mail");
        }
    }
    
    
    /**
     * Send mail with Multiple Recipient
     */
    public void sendEmailWithMultipleRecipient(String from,String[] to,String[] ccEmailIds,String sub,String msgBody,String fileName,String filePath, boolean isHTML){
    	MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(to);
            helper.setSubject(sub);
            helper.setText(msgBody, isHTML);
            if(null!=ccEmailIds && ccEmailIds.length>0){
            	for(String ccAddress : ccEmailIds){
                    helper.addCc (ccAddress);
                }
            }
            if(null!=fileName && fileName.equals(Constants.BLANK) &&  null!=filePath && filePath.equals(Constants.BLANK)){
              helper.addAttachment(fileName, new ByteArrayResource(getFileArray(filePath)));
            }
            mailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
            logger.error("Error in send mail");
        }
    }
    
    private byte[] getFileArray(String fileName){
    	 StringBuilder sb = new StringBuilder();
    	 try{
	    	 InputStream is = new FileInputStream(new File(fileName));
	         byte[] tmp = new byte[4*1024];
	         int size = 0;
	         while((size = is.read(tmp)) != -1){
	             sb.append(new String(tmp, 0, size));
	         }
    	 }catch(Exception e){
    		 e.printStackTrace();
    	 }
         return sb.toString().getBytes();
    }
    
    public void sendEmail(EmailDTO email){
    	try{
    	   sendEmailWithMultipleRecipient(email.getFrom(), email.getTo(), email.getCc(), email.getSubject(), email.getMessage(), email.getFileName(), email.getFilePath(), true);
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }

}
