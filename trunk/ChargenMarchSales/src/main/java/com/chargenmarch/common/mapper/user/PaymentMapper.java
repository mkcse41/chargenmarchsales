package com.chargenmarch.common.mapper.user;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.common.dto.user.PaymentLogDto;

public class PaymentMapper implements RowMapper<PaymentLogDto>{

	@Override
	public PaymentLogDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		PaymentLogDto dto = new PaymentLogDto();
		dto.setChargerId(rs.getInt("chargerid"));
		dto.setCity(rs.getString("city"));
		dto.setFailureReason(rs.getString("failurereason"));
		dto.setId(rs.getInt("id"));
		dto.setIpAddress(rs.getString("ipaddress"));
		dto.setPaymentStatus(rs.getBoolean("paymentstatus"));
		dto.setSessionId(rs.getString("sessionid"));
		dto.setSuccessfulRecharge(rs.getBoolean("successfulrecharge"));
		dto.setTimestamp(rs.getDate("timestamp"));
		dto.setWalletWithGateway(rs.getBoolean("walletwithgateway"));
		
		RechargeDTO rechargeDto = new RechargeDTO();
		rechargeDto.setAmount(rs.getInt("amount"));
		rechargeDto.setMobileService(rs.getString("mobileService"));
		rechargeDto.setServiceType(rs.getString("serviceType"));
		rechargeDto.setIsQuickRecharge(rs.getInt("isQuickRecharge"));
		rechargeDto.setIsSuccesfulRecharge(rs.getInt("successfulrecharge"));
		rechargeDto.setError(rs.getString("failurereason"));
		rechargeDto.setUseReferral(rs.getBoolean("isUseReferral"));
		rechargeDto.setUseWallet(rs.getBoolean("isUseWallet"));
		
		dto.setRechargeDto(rechargeDto);
		return dto;
	}

}
