package com.chargenmarch.common.service.sms;

import com.chargenmarch.common.dto.sms.SMSDTO;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.enums.sms.SMSStatusCodes;
import com.chargenmarch.common.enums.sms.SMSTemplate;

public interface ISMSService {
	
	/**
	 * @param user
	 * @param smsType
	 * @return {@link SMSDTO}
	 * Use this method only to initiate SMS DTO creation as it specifies type as SMS logged in SMS type
	 * which is the 1st step of the flow
	 */
	public SMSDTO getSMSObjectByUserAndSMSTemplate(UserDTO user, SMSTemplate templateName);
	
	public int updateSMSStatus(SMSDTO sms, SMSStatusCodes status);
	
	public int saveSMStoLog(SMSDTO sms) throws Throwable;
	public int sendSMS(SMSDTO sms);
	public void checkSMSBalance();
}
