package com.chargenmarch.common.dao.usercampaign.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.chargenmarch.campaign.user.Query.UserCampaignQuery;
import com.chargenmarch.common.dao.usercampaign.IUserCampaignDao;
import com.chargenmarch.common.dto.campaign.user.CampaignUserDTO;

@Repository
public class UserCampaignDaoImpl implements IUserCampaignDao {

	@Autowired
	private NamedParameterJdbcTemplate namedParamJdbcTemplate;

	private final static Logger logger = LoggerFactory.getLogger(UserCampaignDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * add user data when user hit any url first time in a session.
	 */
	public void addUserCampaignData(CampaignUserDTO campaignUserDTO) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(campaignUserDTO);
		namedParamJdbcTemplate.update(UserCampaignQuery.INSERT_CAMPAIGN_TRACKING_LOG, params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * add user complete tracking by session id, pages visited by user in a
	 * single session.
	 */
	public void addUserCompleteTracking(CampaignUserDTO campaignUserDTO) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(campaignUserDTO);
		namedParamJdbcTemplate.update(UserCampaignQuery.INSERT_CAMPAIGN_TRACKING_DETAIL_LOG, params);
	}
}
