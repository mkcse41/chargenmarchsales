package com.chargenmarch.common.utility.impl;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class CustomDateDeSerializer implements JsonDeserializer<Date> {
	
	private final static Logger logger = LoggerFactory.getLogger(CustomDateDeSerializer.class);
	
	String dateFormat;
	public CustomDateDeSerializer(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	
	public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Date date = null;
		try{//2016-06-16T06:50:00 //
			date = new SimpleDateFormat(dateFormat).parse(json.getAsString());
        }
		catch(ParseException e){
			logger.error("Exception in Parsing Date for GSON", e);
		}
		return date;
	}

}
