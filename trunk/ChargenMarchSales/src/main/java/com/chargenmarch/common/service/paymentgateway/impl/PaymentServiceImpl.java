package com.chargenmarch.common.service.paymentgateway.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.dao.paymentgateway.IPaymentDao;
import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.chargenmarch.common.dto.paymentgateway.PaymentGatewayInfo;
import com.chargenmarch.common.dto.paymentgateway.ProductInfo;
import com.chargenmarch.common.enums.paymentgateway.PaymentGateWayName;
import com.chargenmarch.common.forms.paymentgateway.PayUResponseForm;
import com.chargenmarch.common.forms.paymentgateway.PaymentRequestForm;
import com.chargenmarch.common.service.paymentgateway.IPaymentService;
import com.chargenmarch.common.service.paymentgateway.thread.InsertOrUpdatePaymentOrderThread;
import com.chargenmarch.common.service.paymentgateway.thread.PaymentLoggingThread;
import com.chargenmarch.common.utility.IUtility;



/**
 * This Service used to load payment related product info.
 * Save payment order and payment logs  
 * @author mukesh
 */
@Service
public class PaymentServiceImpl implements IPaymentService {

	private final static Logger logger = LoggerFactory.getLogger(PaymentServiceImpl.class);
	private Map<String, ProductInfo> productInfoMap;
	private static PayUServiceImpl _payUServiceInstance;
	
	@Autowired
	private IPaymentDao paymentDao;
	@Autowired
	private IUtility utility;
	
/*	@Autowired
	ThreadPoolTaskExecutor taskExecutor;
	@Autowired
	InsertOrUpdatePaymentOrderThread insertOrUpdatePaymentOrderThread;
	@Autowired
	PaymentLoggingThread paymentLoggingThread;*/
	
	public static PayUServiceImpl getPayUServiceInstance(){
		if( _payUServiceInstance == null)
			_payUServiceInstance = new PayUServiceImpl();
		return _payUServiceInstance;
	}

	@PostConstruct
	public void init(){
		this.productInfoMap = new HashMap<String, ProductInfo>();
		loadProductInfo();
	}
	
	/**
	 * Load all product info store in product info Map.
	 */
	private void loadProductInfo(){
		List<ProductInfo> productInfoList = paymentDao.loadProductData();
		for (ProductInfo productInfo : productInfoList) {
			if(null!=productInfo){
				productInfo.setPaymentGateWayInfo(setPaymentGateWayInfo(productInfo));
				productInfoMap.put(productInfo.getProductName(), productInfo);
			}
		}
	}
	
	/**
	 * Set payment gateway object 
	 * Merchant Id : provided by PayU.
	 * Salt Id :  provided by PayU.
	 * Payment gateway Url : payment gateway url used to redirect testing and live payU site.
	 * @param productInfo
	 * @return
	 */
	private PaymentGatewayInfo setPaymentGateWayInfo(ProductInfo productInfo){
		    PaymentGatewayInfo paymentGatewayInfo = new PaymentGatewayInfo();
		    paymentGatewayInfo.setMerchantId(productInfo.getMerchantId());
		    paymentGatewayInfo.setSalt(productInfo.getSalt());
		    paymentGatewayInfo.setUrl(productInfo.getPgUrl());
		    return paymentGatewayInfo;
	}
	
	/**
	 * Save payment order and payment logs 
	 */
	public void savePaymentOrder(OrderInfo orderInfo,String mode,String productId,String request,String response,IPaymentService paymentService){
		//save payment order		
		InsertOrUpdatePaymentOrderThread insertOrUpdateThread = new InsertOrUpdatePaymentOrderThread(orderInfo,mode,paymentService);
		insertOrUpdateThread.start();
		
		PaymentLoggingThread paymentLoggingThread = new PaymentLoggingThread(request,response,orderInfo,mode,paymentService);
		paymentLoggingThread.start();
	}
	
	/**
	 * Get product Object by product Name
	 * Product Info object all information store like as merchantId,salt Id,Payment gateway url,success and failure url.
	 * @param productName
	 * @return
	 */
	public ProductInfo getProductInfoMap(String productName) {
		logger.info("PayuService productName==>"+productName);
		logger.info("PayuService productInfoMap==>"+productInfoMap);
		return productInfoMap.get(productName);
	}
	
	/**
	 * Insert payment order in payment order table.
	 */
	public void insertPaymentOrder(OrderInfo orderInfo){
		paymentDao.insertPaymentOrder(orderInfo);
	}
	
	/**
	 * Update payment order by order id.
	 */
	public void updatePaymentOrder(OrderInfo orderInfo){
		paymentDao.updatePaymentOrder(orderInfo);
	}
	
	/**
	 * Insert payment order logs in payment logs.
	 * store payment request and response in JSON format. 
	 */
	public void insertPaymentOrderLogs(String request,String productId){
		paymentDao.insertPaymentOrderLogs(request, productId);
	}
	/**
	 * Update payment response by order id.
	 * 
	 */
	public void updatePaymentOrderLogs(String response,String productId){
		paymentDao.updatePaymentOrderLogs(response, productId);
	}

	/**
	 * Get max order id by product wise.
	 * @param productId
	 * @return
	 */
	public int getOrderId(int productId){
		return paymentDao.getOrderId(productId);
	}
	@Override
	public OrderInfo processPayment(OrderInfo orderInfo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OrderInfo validatePaymentOrder(OrderInfo orderInfo) {
		return null;
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * This method used to initialization order info object for PayU request.
	 * All the required parameter values set in order info object.
	 * @param paymentGatewayForm
	 * @return
	 */
	public OrderInfo initializeOrderInfoForRequest(PaymentRequestForm paymentGatewayForm){
		OrderInfo orderInfo = new OrderInfo();
		orderInfo.setFirstName(utility.stringValueHandler(paymentGatewayForm.getFirstName()));
		orderInfo.setLastName(utility.stringValueHandler(paymentGatewayForm.getLastName()));
		orderInfo.setEmailId(utility.stringValueHandler(paymentGatewayForm.getEmailId()));
		orderInfo.setMobileno(utility.stringValueHandler(paymentGatewayForm.getMobileNo()));
		orderInfo.setPinCode(utility.stringValueHandler(paymentGatewayForm.getPincode()));
		orderInfo.setCity(utility.stringValueHandler(paymentGatewayForm.getCity()));
		orderInfo.setState(utility.stringValueHandler(paymentGatewayForm.getState()));
		orderInfo.setAddress(utility.stringValueHandler(paymentGatewayForm.getAddress()));
		orderInfo.setAmount(Float.parseFloat((paymentGatewayForm.getAmount() == null) || 
				(Constants.BLANK.equals(paymentGatewayForm.getAmount())) ? Constants.ZERO_STRING:paymentGatewayForm.getAmount()));
		orderInfo.setProductName(utility.stringValueHandler(paymentGatewayForm.getProductName()));
		orderInfo.setPgType(utility.stringValueHandler(paymentGatewayForm.getPgType()));
		orderInfo.setLeadId(utility.convertStringToInt(paymentGatewayForm.getCarLeadId()));
        orderInfo.setUserCredentials(paymentGatewayForm.getUserCredentials());
		return orderInfo;
	}
	
	/**
	 * This method used to initialization order info object for PayU response.
	 * @param payUResponseForm
	 * @return
	 */
	public OrderInfo initializeOrderInfoForPayUResponse(PayUResponseForm payUResponseForm){
		OrderInfo orderInfo = new OrderInfo();
		orderInfo.setFirstName(utility.stringValueHandler(payUResponseForm.getFirstname()));
		orderInfo.setLastName(utility.stringValueHandler(payUResponseForm.getLastname()));
		orderInfo.setEmailId(utility.stringValueHandler(payUResponseForm.getEmail()));
		orderInfo.setMobileno(utility.stringValueHandler(payUResponseForm.getPhone()));
		orderInfo.setPinCode(utility.stringValueHandler(payUResponseForm.getZipcode()));
		orderInfo.setCity(utility.stringValueHandler(payUResponseForm.getCity()));
		orderInfo.setState(utility.stringValueHandler(payUResponseForm.getState()));
		orderInfo.setAddress(utility.stringValueHandler(payUResponseForm.getAddress()));
		orderInfo.setAmount(Float.parseFloat(payUResponseForm.getAmount()==null? Constants.ZERO_STRING:payUResponseForm.getAmount()));
		orderInfo.setProductName(utility.stringValueHandler(payUResponseForm.getProductinfo()));
		orderInfo.setPgType(utility.stringValueHandler(PaymentGateWayName.PayU.name()));
		orderInfo.setTxnId(utility.stringValueHandler(payUResponseForm.getTxnid()));
		orderInfo.setHash(utility.stringValueHandler(payUResponseForm.getHash()));
		orderInfo.setError(utility.stringValueHandler(payUResponseForm.getError()));
		orderInfo.setBankcode(utility.stringValueHandler(payUResponseForm.getBankcode()));
		orderInfo.setPG_TYPE(utility.stringValueHandler(payUResponseForm.getPG_TYPE()));
		orderInfo.setBank_ref_num(utility.stringValueHandler(payUResponseForm.getBank_ref_num()));
		orderInfo.setShipping_firstname(utility.stringValueHandler(payUResponseForm.getShipping_firstname()));
		orderInfo.setShipping_lastname(utility.stringValueHandler(payUResponseForm.getShipping_lastname()));
		orderInfo.setShipping_address1(utility.stringValueHandler(payUResponseForm.getShipping_address1()));
		orderInfo.setShipping_address2(utility.stringValueHandler(payUResponseForm.getShipping_address2()));
		orderInfo.setShipping_city(utility.stringValueHandler(payUResponseForm.getShipping_city()));
		orderInfo.setShipping_state(utility.stringValueHandler(payUResponseForm.getShipping_state()));
		orderInfo.setShipping_country(utility.stringValueHandler(payUResponseForm.getShipping_country()));
		orderInfo.setShipping_phone(utility.stringValueHandler(payUResponseForm.getShipping_phone()));
		orderInfo.setShipping_zipcode(utility.stringValueHandler(payUResponseForm.getShipping_zipcode()));
		orderInfo.setMihpayid(utility.stringValueHandler(payUResponseForm.getMihpayid()));
		orderInfo.setMode(utility.stringValueHandler(payUResponseForm.getMode()));
		orderInfo.setState(utility.stringValueHandler(payUResponseForm.getStatus()));
		orderInfo.setDiscount(Float.parseFloat(payUResponseForm.getDiscount()==null? Constants.ZERO_STRING:payUResponseForm.getDiscount()));
		orderInfo.setOffer(utility.stringValueHandler(payUResponseForm.getOffer()));
		orderInfo.setUnmappedstatus(utility.stringValueHandler(payUResponseForm.getUnmappedstatus()));
		orderInfo.setStatus(utility.stringValueHandler(payUResponseForm.getStatus()));
		return orderInfo;
	}
	
}
