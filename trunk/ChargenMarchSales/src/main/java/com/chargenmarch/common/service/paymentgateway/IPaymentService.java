package com.chargenmarch.common.service.paymentgateway;

import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.chargenmarch.common.dto.paymentgateway.ProductInfo;
import com.chargenmarch.common.forms.paymentgateway.PayUResponseForm;
import com.chargenmarch.common.forms.paymentgateway.PaymentRequestForm;

/**
 * 
 * @author mukesh
 *
 */
public interface IPaymentService {

	
	public OrderInfo processPayment(OrderInfo orderInfo);
	public OrderInfo validatePaymentOrder(OrderInfo orderInfo);
	public void insertPaymentOrder(OrderInfo orderInfo);
	public void updatePaymentOrder(OrderInfo orderInfo);
	public void insertPaymentOrderLogs(String request,String productId) ;
	public void updatePaymentOrderLogs(String response,String productId) ;
	public OrderInfo initializeOrderInfoForRequest(PaymentRequestForm paymentGatewayForm);
	public OrderInfo initializeOrderInfoForPayUResponse(PayUResponseForm payUResponseForm);
	public void savePaymentOrder(OrderInfo orderInfo,String mode,String orderId,String request,String response,IPaymentService paymentService);
	public ProductInfo getProductInfoMap(String productName);
}
