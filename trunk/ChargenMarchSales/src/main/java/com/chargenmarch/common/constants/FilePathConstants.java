package com.chargenmarch.common.constants;

public class FilePathConstants {
	public static final String CLASSPATH = "classpath:";
	public static final String API_PROPERTIES = "api.properties";
	public static final String REWARDS_POINT_PROPERTIES = "rewardspoint.properties";
	public static final String SMS_PROPERTIES = "sms.properties";
	public static final String JOLOAPI_PROPERTIES = "joloapi.properties";
	public static final String SERVER_PROPERTIES = "server.properties";
	public static final String AIR_TICKET_PROPERTIES = "airticket.properties";
}
