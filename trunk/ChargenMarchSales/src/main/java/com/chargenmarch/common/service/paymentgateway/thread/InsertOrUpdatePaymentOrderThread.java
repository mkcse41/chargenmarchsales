package com.chargenmarch.common.service.paymentgateway.thread;



import com.chargenmarch.common.constants.paymentgateway.PaymentConstants;
import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.chargenmarch.common.service.paymentgateway.IPaymentService;

/**
 * This thread used to insert or update payment order.
 * @author mukesh
 *
 */

public class InsertOrUpdatePaymentOrderThread extends Thread{

	private OrderInfo orderInfo;
	private String mode;
	private IPaymentService paymentService;
	
	public InsertOrUpdatePaymentOrderThread(OrderInfo orderInfo,String mode,IPaymentService paymentService){
		this.orderInfo = orderInfo;
		this.mode = mode;
		this.paymentService = paymentService;
	}
	
	public void run(){
		if(mode.equalsIgnoreCase(PaymentConstants.ADD)){
			paymentService.insertPaymentOrder(orderInfo);
		}else{
			paymentService.updatePaymentOrder(orderInfo);
		}
	}

	public OrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(OrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}
	
}
