package com.chargenmarch.common.dao.user.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.chargenmarch.charge.joloapi.query.Query;
import com.chargenmarch.charge.query.user.UserQueries;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.dao.user.IUserDao;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.enums.user.UserVerificationStatus;
import com.chargenmarch.common.mapper.user.UserMapper;
import com.chargenmarch.common.service.user.IUserService;

@Repository
public class UserDaoImpl implements IUserDao{
	
	private final static Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	IUserService userServie;
	
	public int addUser(UserDTO user){
		
		if(userServie.getUserInfoMapByEmailId().containsKey(user.getEmailId())){
			Object[] args = {user.getName(), user.getMobileNo(), user.getSource(),user.getPassword(), user.getIsCAMLogin(), user.getEmailId()};
			return jdbcTemplate.update(UserQueries.UPDATE_USER_DETAILS, args);
		}
		else{
			Object[] args = {user.getName(), user.getEmailId(), user.getMobileNo(), user.getSource(),user.getPassword(), user.getIsCAMLogin(),user.getReferralCode()};
			return jdbcTemplate.update(UserQueries.INSERT_USER_DETAILS, args);
		}
	}
	
	public List<UserDTO> getUserByMobile(String mobileNo){
		Object[] args = {mobileNo};
		return (List<UserDTO>) jdbcTemplate.query(UserQueries.SELECT_FROM_USERS_BY_MOBILE, args, new UserMapper());
	}
	
	public List<UserDTO> getUserById(int id){
		Object[] args = {id};
		return (List<UserDTO>) jdbcTemplate.query(UserQueries.SELECT_FROM_USERS_BY_ID, args, new UserMapper());
	}
	
	public List<UserDTO> getAllUsers(){
		return (List<UserDTO>) jdbcTemplate.query(UserQueries.SELECT_ALL_USERS, new UserMapper());
	}
	
	public int updateUserWalletBalance(UserDTO user){
		Object[] args = {user.getWalletBalance(),user.getId()};
		return jdbcTemplate.update(UserQueries.UPDATE_USER_WALLET_BAL, args);
	}
	
	public int updateUserRechargeAccount(UserDTO user){
		Object[] args = {user.getRechargeCount() ,user.getId()};
		return jdbcTemplate.update(UserQueries.UPDATE_USER_RECHARGE_COUNT, args);
	}
	
	public int updateUserPassword(UserDTO user){
		Object[] args = {user.getPassword(), user.getId()};
		return jdbcTemplate.update(UserQueries.UPDATE_USER_PASSWORD, args);
	}

	public UserDTO getUserByEmailId(String emailId) {
		Object[] args = {emailId};
		List<UserDTO> userDtoList =  (List<UserDTO>) jdbcTemplate.query(UserQueries.SELECT_FROM_USERS_BY_EMAILID, args, new UserMapper());
		if(null != userDtoList && userDtoList.size()>0){
			return userDtoList.get(0);
		}
		return null;
	}
	
	public int updateUserVerified(UserDTO user){
		Object[] args = {user.getIsverified(),user.getEmailId()};
		return jdbcTemplate.update(UserQueries.UPDATE_USER_VERIFIED, args);
	}
	public List<UserDTO> getUserByVerificationStatus(UserVerificationStatus verifiedStatus) {
		Object[] args = {verifiedStatus.ordinal()};
		return jdbcTemplate.query(UserQueries.SELECT_FROM_USERS_BY_VERIFICATION_STATUS, args, new UserMapper());
	}
	
	public List<UserDTO> getUserListByRegistrationDate(String date) {
		Object[] args = {Constants.PERCENT+date+Constants.PERCENT};
		return jdbcTemplate.query(UserQueries.SELECT_FROM_USERS_BY_REGISTRATION_DATE, args, new UserMapper());
	}
	
	public int insertReferralLogs(UserDTO user,UserDTO referralUserDTO,int cashbackAmount){
		Object[] args = {user.getId(), referralUserDTO.getId(), cashbackAmount};
		return jdbcTemplate.update(UserQueries.INSERT_USER_REFERRAL_LOGS, args);
	}
	
	public int updateRefferalCodeById(UserDTO user,String referralCode){
		Object[] args = {referralCode,user.getId()};
		return jdbcTemplate.update(UserQueries.UPDATE_USER_REFERRAL_CODE, args);
	}
	
	public List getReferralLogs(UserDTO user){
		Object[] args = {user.getId()};
		return jdbcTemplate.queryForList(UserQueries.GET_USER_REFERRAL_LOGS, args);
	}
	
	public int insertEmailLogs(UserDTO user){
		Object[] args = {user.getId()};
		return jdbcTemplate.update(UserQueries.INSERT_EMAIL_LOGS_LOGS, args);
	}
	
	public int truncateEmailLogs(){
		return jdbcTemplate.update(UserQueries.TRUNCATE_EMAIL_LOGS);
	}
	
	public int getReferralAmountByChargersId(UserDTO user){
		Object[] args = {user.getId()};
		Long userReferralAmount = jdbcTemplate.queryForLong(UserQueries.GET_USER_REFERRAL_AMOUNT_BY_USERID, args);
        String referralAmount = String.valueOf(userReferralAmount);
        int refAmount = Integer.parseInt(referralAmount);
		return refAmount;
	}
	
	public int updateUserRefferalBalance(UserDTO user){
		Object[] args = {user.getReferralBalance(),user.getId()};
		return jdbcTemplate.update(UserQueries.UPDATE_USER_REFERRAL_BAL, args);
	}
	
	public int getReferralRechargeAmountByChargersId(UserDTO user){
		Object[] args = {user.getId()};
		Long userReferralRechargeAmount = jdbcTemplate.queryForLong(Query.GET_USER_REFERRAL_RECHARGE_AMOUNT_BY_USERID, args);
        String referralRechargeAmount = String.valueOf(userReferralRechargeAmount);
        int refrechargeAmount = Integer.parseInt(referralRechargeAmount);
		return refrechargeAmount;
	}
	

	public List<UserDTO> getChargersDetailsInfo(){
		List<UserDTO> chargersDetailsList = jdbcTemplate.query(UserQueries.SELECT_FROM_CHARGERS_DETAILS, new UserMapper());
		return chargersDetailsList;
	}
	
	public List<UserDTO> getChargersDetailsWalletBalInfo(){
		List<UserDTO> chargersDetailsList = jdbcTemplate.query(UserQueries.SELECT_FROM_CHARGERS_DETAILS_WALLET, new UserMapper());
		return chargersDetailsList;
	}
	
	public int getPaymentInfoByEmailId(UserDTO user){
		Object[] args = {user.getId()};
		Long userPaymentAmount = jdbcTemplate.queryForLong(UserQueries.GET_USER_PAYMENT_INFO_BY_EMAILID, args);
        String paymentAmount = String.valueOf(userPaymentAmount);
        int refrechargeAmount = Integer.parseInt(paymentAmount);
		return refrechargeAmount;
	}
	
	public UserDTO getReferrerByUser(UserDTO user) {
		int referrerUserId = getReferredIdByUser(user);
		List<UserDTO> userList = getUserById(referrerUserId); 
		if(!userList.isEmpty() && userList != null){
			return userList.get(0);
		}
		return null;
	}
	
	public int getReferredIdByUser(UserDTO user) {
		try{
			Object[] args = {user.getId()};
			return jdbcTemplate.queryForObject(UserQueries.GET_REFERRER_USER_ID, args, Integer.class);
		} catch(EmptyResultDataAccessException e) {
			logger.error("No referrer_chargers_id found for charger ID " +user.getId() + " in table user_referral_logs.");
			return -1;
		}
    }
	
   public int saveOrUpdateBrowserNotificationLogs(UserDTO user,String browserId){
	   if(!isExistsBrowserNotificationLogs(user)){
		      Object[] args = {user.getId(), browserId};
		      return jdbcTemplate.update(UserQueries.SAVE_BROWSER_NOTIFICATION_LOGS, args);
	   }else{
		      Object[] args = {browserId, user.getId()};
			  return jdbcTemplate.update(UserQueries.UPDATE_BROWSER_NOTIFICATION_LOGS, args); 
	   }
   }
   
   private boolean isExistsBrowserNotificationLogs(UserDTO userDTO){
	   Object[] args = {userDTO.getId()};
	   List browserNotificationList = jdbcTemplate.queryForList(UserQueries.IS_EXISTS_BROWSER_NOTIFICATION_LOGS_BY_CHARGERS_ID, args);
	   if(null != browserNotificationList && browserNotificationList.size() > 0){
		   return true;
	   }
	   return false;
   }
   
   public List getBrowserNotificationLogs(){
	   List browserNotificationList = jdbcTemplate.queryForList(UserQueries.GET_BROWSER_NOTIFICATION_LOGS);
	   return browserNotificationList;
   }
   
   public List<UserDTO> getCAMSalesUserList(){
		List<UserDTO> salesUserList = jdbcTemplate.query(UserQueries.SELECT_FROM_CAM_SALES_USER, new UserMapper());
		return salesUserList;
	}
   
}