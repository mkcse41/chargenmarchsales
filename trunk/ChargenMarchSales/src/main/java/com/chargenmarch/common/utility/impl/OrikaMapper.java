package com.chargenmarch.common.utility.impl;

import java.util.Map;

import ma.glasnost.orika.Converter;
import ma.glasnost.orika.Mapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author Rajat Singhvi
 * 
 */

@Component
public class OrikaMapper extends ConfigurableMapper implements ApplicationContextAware {
	

	private MapperFactory factory;
	private MapperFacade mapperFacade;
	
	private ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
		addAllSpringBeans(applicationContext);
		registerMappers();
	}

	@Override
	protected void configure(MapperFactory factory) {
		this.factory = factory;
		this.mapperFacade=factory.getMapperFacade();
	}
	
	public void registerMappers() {
	}
	
	public MapperFacade getMapperFacade() {
		return mapperFacade;
	}
	
	public MapperFactory getMapperFactory() {
		return factory;
	}

	
	/**
	 * Constructs and registers a {@link ClassMapBuilder} into the {@link MapperFactory} using a {@link Mapper}.
	 * 
	 * @param mapper
	 */
	@SuppressWarnings("rawtypes")
	public void addMapper(Mapper<?, ?> mapper) {
		factory.classMap(mapper.getAType(), mapper.getBType())
				.byDefault()
				.customize((Mapper) mapper)
				.register();
	}
	
	public void addConverter(Converter<?, ?> converter) {
		factory.getConverterFactory().registerConverter(converter);
	}
	
	/**
	 * Scans the appliaction context and registers all Mappers and Converters found in it.
	 * 
	 * @param applicationContext
	 */
	@SuppressWarnings("rawtypes")
	private void addAllSpringBeans(final ApplicationContext applicationContext) {
		Map<String, Mapper> mappers = applicationContext.getBeansOfType(Mapper.class);
		for (Mapper mapper : mappers.values()) {
			addMapper(mapper);
		}
		Map<String, Converter> converters = applicationContext.getBeansOfType(Converter.class);
		for (Converter converter : converters.values()) {
			addConverter(converter);
		}
	}


}

