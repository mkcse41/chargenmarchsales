package com.chargenmarch.common.filter;


import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.dto.campaign.user.CampaignUserDTO;
import com.chargenmarch.common.dto.campaign.user.CampaignUserDTO.Tracking;
import com.chargenmarch.common.thread.SaveCampaignUserDataThread;

import eu.bitwalker.useragentutils.UserAgent;

/**
 * This class used to intercept the request and save user tracking details like as ip address,city and url.
 * @author mukesh
 *
 */
public class CamInterceptor implements HandlerInterceptor{
	
	private final static Logger logger = LoggerFactory.getLogger(CamInterceptor.class);
	
	@Autowired
	ThreadPoolTaskExecutor taskExecutor;
	
	@Autowired
	SaveCampaignUserDataThread saveCampaignUserDataThread;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		// track user Events
		saveUserTrackingDetails(request,response);
		
		String userAgent = request.getHeader(Constants.USER_AGENT);
		if(null != userAgent){
			logger.info("Taking user to "+getDeviceTypeByUserAgent(userAgent,request)+ " version");
		    return true;
		}
		else{
			logger.info("No user Agent Detected.");
			return false;
		}
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	private String getDeviceTypeByUserAgent(String userAgent, HttpServletRequest request){
		String utmSource = request.getParameter(Constants.UTM_SOURCE);
		if(null!=utmSource && request.getSession().getAttribute(Constants.UTM_SOURCE)==null){
		    request.getSession().setAttribute(Constants.UTM_SOURCE, utmSource);
	    }
		if(null!=userAgent && userAgent.contains(Constants.MOBILE)){
			request.getSession().setAttribute(Constants.IS_MOBILE,Constants.STR_TRUE);
			request.getSession().setAttribute(Constants.UTM_SOURCE, Constants.MOBILE);
			return Constants.MOBILE;
		}else if(null!=userAgent && userAgent.contains(Constants.ANDROID)){
			request.getSession().setAttribute(Constants.IS_ANDROID,Constants.STR_TRUE);
			request.getSession().setAttribute(Constants.UTM_SOURCE, Constants.ANDROID);
			return Constants.ANDROID;
		}
		else {
			request.getSession().removeAttribute(Constants.IS_MOBILE);
			request.getSession().setAttribute(Constants.UTM_SOURCE, Constants.WEB);
			return Constants.COMPUTER;
		}
	}
	
	/**
	 * This method used to save user tracking details like as ip address.
	 * @param request
	 * @param response
	 */
	private void saveUserTrackingDetails(HttpServletRequest request , HttpServletResponse response){
		    HttpSession session = request.getSession();
			String cxtPath = request.getContextPath();
			String path = request.getRequestURI();
			if(Constants.BACK_SLASH.equals(cxtPath) && path.startsWith(cxtPath)){
				path = path.substring(cxtPath.length());
			}
			if (!path.contains(Constants.RESOURCES) && !path.contains(Constants.SERVICE_WORKER)) {
				userCompleteTracking(request,path);
				CampaignUserDTO campaignUserDto = (CampaignUserDTO)session.getAttribute(Constants.CAMPAIGN_USER_DTO);
				if(campaignUserDto == null){
					campaignUserDto = new CampaignUserDTO();
				// get session_id , if session id is not present in map then
				// create campaignUserDto and save it to DB.
				String sessionId = session.getId();
				// get ip_address of user
				String ipAddress = getRemoteAddr(request);
				// get request_source
				String userAgent = request.getHeader(Constants.USER_AGENT);
				if (null != userAgent) {
					String source = getDeviceTypeByUserAgent(userAgent, request);
					campaignUserDto.setSource(source);
					UserAgent userAgentobj = UserAgent.parseUserAgentString(userAgent);
					String browserName = userAgentobj.getBrowser().getName();
					String operatingSystemName = userAgentobj.getOperatingSystem().getName();
					campaignUserDto.setBrowser(browserName);
					campaignUserDto.setOperatingSystem(operatingSystemName);
				}
				campaignUserDto.setSession_id(sessionId);
				campaignUserDto.setIpaddress(ipAddress);
				campaignUserDto.setRequest_url(path);
				campaignUserDto.setTracking(Tracking.SINGLEPATH);
				session.setAttribute(Constants.CAMPAIGN_USER_DTO, campaignUserDto);
				saveCampaignUserDataThread.setData(campaignUserDto);
				taskExecutor.execute(saveCampaignUserDataThread);
			}
		}
	}
	
	/**
	 * This method used to get user ip address.
	 * @param request
	 * @return
	 */
	private String getRemoteAddr(HttpServletRequest request) {
		try {
			String ip = request.getHeader(Constants.X_REAL_IP);
			if (null != ip && !Constants.BLANK.equals(ip.trim()) && !Constants.UNKNOWN.equalsIgnoreCase(ip)) {
				return ip;
			}
			ip = request.getHeader(Constants.X_FORWARDED_FOR);
			if (null != ip && !Constants.BLANK.equals(ip.trim()) && !Constants.UNKNOWN.equalsIgnoreCase(ip)) {
				// get first ip from proxy ip
				int index = ip.indexOf(Constants.COMMA);
				if (index != -1) {
					return ip.substring(0, index);
				} else {
					return ip;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return request.getRemoteAddr();
	}
	
	private void userCompleteTracking(HttpServletRequest request , String path){
		HttpSession session = request.getSession();
		Set<String> userTrackingPath = (Set<String>)session.getAttribute(Constants.USER_TRACKING_PATHSET);
		if(null == userTrackingPath){
			userTrackingPath = new HashSet<String>();
		}
		if(!userTrackingPath.contains(path) && !path.contains(Constants.RESOURCES) && !path.contains(Constants.SERVICE_WORKER)){
			CampaignUserDTO campaignUserDto = new CampaignUserDTO();
			campaignUserDto.setRequest_url(path);
			campaignUserDto.setSession_id(request.getSession().getId());
			campaignUserDto.setTracking(Tracking.MULTIPATH);
			saveCampaignUserDataThread.setData(campaignUserDto);
			taskExecutor.execute(saveCampaignUserDataThread);
			userTrackingPath.add(path);
			session.setAttribute(Constants.USER_TRACKING_PATHSET, userTrackingPath);
		}
	}

}