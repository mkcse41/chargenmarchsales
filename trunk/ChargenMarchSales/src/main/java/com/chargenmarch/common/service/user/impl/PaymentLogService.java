package com.chargenmarch.common.service.user.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.chargenmarch.charge.thread.PaymentTrackerThread;
import com.chargenmarch.common.dao.user.IPaymentLogDao;
import com.chargenmarch.common.dto.user.PaymentLogDto;
import com.chargenmarch.common.service.user.IPaymentLogService;

/**
 * @author gambit 
 * Service to track payments
 */
@Service
public class PaymentLogService implements IPaymentLogService {

	@Autowired
	private IPaymentLogDao trackingDao;

	/**	
	 * Logs every step of user activity according to the DTO
	 * 
	 * @param dto
	 * @return int
	 */
	@Override
	public int logPaymentActivity(PaymentLogDto dto) {
		return trackingDao.logPaymentActivity(dto);
	}
	
	@Override
	public int updatePaymentLog(PaymentLogDto dto) {
		return trackingDao.updatePaymentLog(dto);
	}
	
	@Override
	public PaymentLogDto getPaymentLogById(int id) {
		List<PaymentLogDto> list = trackingDao.getPaymentLogById(id);
		return list != null ? list.get(0) : null;
	}
	
	public void invokePaymentTrackerThread(ThreadPoolTaskExecutor taskExecutor, PaymentTrackerThread tracker, PaymentLogDto userTrackingDto, boolean isUpdate) {
		tracker.setTrackingDto(userTrackingDto);
		tracker.setUpdate(isUpdate);
		taskExecutor.execute(tracker);
	}

}
