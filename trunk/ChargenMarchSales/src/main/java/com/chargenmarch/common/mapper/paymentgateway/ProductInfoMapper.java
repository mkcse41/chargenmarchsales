package com.chargenmarch.common.mapper.paymentgateway;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.common.constants.paymentgateway.PaymentConstants;
import com.chargenmarch.common.dto.paymentgateway.ProductInfo;



public class ProductInfoMapper implements RowMapper{
    
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		ProductInfo productInfo = new ProductInfo();
		productInfo.setId(rs.getInt(PaymentConstants.ID));
		productInfo.setProductName(rs.getString(PaymentConstants.PRODUCT_NAME));
		productInfo.setMerchantId(rs.getString(PaymentConstants.MERCHANTID));
		productInfo.setSalt(rs.getString(PaymentConstants.SALT));
		productInfo.setSurl(rs.getString(PaymentConstants.SURL));
		productInfo.setCurl(rs.getString(PaymentConstants.CURL));
		productInfo.setFurl(rs.getString(PaymentConstants.FURL));
		productInfo.setReceiverEmailId(rs.getString(PaymentConstants.RECEIVER_EMAILID));
		productInfo.setRecipient(rs.getString(PaymentConstants.RECIPIENT));
		productInfo.setIsSendMail(rs.getString(PaymentConstants.ISSENDMAIL));
		productInfo.setRequiredResponse(rs.getBoolean(PaymentConstants.IS_REQUIRED_RESPONSE));
		productInfo.setOrderprefix(rs.getString(PaymentConstants.ORDER_PREFIX));
		productInfo.setPgUrl(rs.getString(PaymentConstants.PG_URL));
		productInfo.setPaymentGateWay(rs.getString(PaymentConstants.PAYMENT_GATEWAY));
		return productInfo;
	}
}
