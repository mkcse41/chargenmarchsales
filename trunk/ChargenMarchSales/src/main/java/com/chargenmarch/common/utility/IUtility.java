package com.chargenmarch.common.utility;

import java.text.ParseException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public interface IUtility {
	/**
	 * This method returns blank string if parameter value contains null,
	 * otherwise, it will return same string.
	 * 
	 * @param str
	 * @return
	 */
	public String stringValueHandler(String str);

	/**
	 * This method is used to remove non numeric value from string and convert
	 * it into integer & Shoot Mail if not possible
	 */
	public int convertStringToInt(String str);

	/**
	 * Returns the base URL of server if provided in dispatcher-servlet else
	 * Main Site URL.
	 * 
	 * @return String
	 */
	public String getBaseUrl();

	public JsonObject parseJSON(String jsonLine);

	public OrderInfo initializeOrderInfoForPaymentGateway(String firstName, String lastName, String emailId,
			String mobileNo, String pinCode, String city, String state, String address, String productName, int amount,
			String pgType, String userCredentials);

	public String classpathResourceLoader(String fileName);

	public String base64Encoder(String rawText);

	public String base64Decoder(String cipher);

	public int generateRandomNumber();

	public String getviewResolverName(HttpServletRequest request, String jspName);

	public boolean isTimeBetweenTwoTime(String initialTime, String finalTime, String currentTime) throws ParseException;

	public String getCurrentTime();

	public Gson getGson();

	/**
	 * Returns a map of with attribute name as key & attribute value as value of
	 * map
	 * 
	 * @param object
	 * @return Map<String, String>
	 */
	public Map<String, String> convertObjectToMap(Object object);
	
	/**
	 * This method must be used where we need a GSON object with Date
	 * DeSerializer.
	 * 
	 * @param dateFormat
	 * @return Gson
	 */
	public Gson getGson(String dateFormat);
	
	public String getCurrentDate(String dateFormat);
	public String generateRandomString();
	public boolean isValidatePayment(HttpServletRequest request);
	public String getIpAdress(HttpServletRequest request);
}
