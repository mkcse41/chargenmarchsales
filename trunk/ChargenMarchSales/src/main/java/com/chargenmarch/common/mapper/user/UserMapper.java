package com.chargenmarch.common.mapper.user;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.chargenmarch.common.dto.user.UserDTO;



public class UserMapper implements RowMapper<UserDTO> {

	public UserDTO mapRow(ResultSet res, int rowNum) throws SQLException {
		UserDTO user = new UserDTO();
		user.setId(res.getInt("id"));
		user.setName(res.getString("name"));
		user.setEmailId(res.getString("emailid"));
		user.setMobileNo(res.getString("mobileno"));
		user.setRegistrationDate(res.getDate("registrationdate"));
		user.setUpdatedate(res.getDate("updatedate"));
		user.setRechargeCount(res.getInt("rechargecount"));
		user.setPassword(res.getString("password"));
		user.setSource(res.getString("source"));
		user.setWalletBalance(res.getInt("walletbal"));
		user.setIsverified(res.getInt("isverified"));
		user.setReferralCode(res.getString("referralCode"));
		user.setReferralBalance(res.getInt("referralAmount"));
		user.setIsSalesUser(res.getInt("isSalesUser"));
		return user;
	}

}
