package com.chargenmarch.common.service.paymentgateway.thread;


import com.chargenmarch.common.constants.paymentgateway.PaymentConstants;
import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.chargenmarch.common.service.paymentgateway.IPaymentService;


/**
 * This thread used to insert payment request and response in payment_log table.
 * @author mukesh
 *
 */
/*@Component
@Scope(Constants.PROTOTYPE)*/
public class PaymentLoggingThread extends Thread{

	private IPaymentService paymentService;
	
	private String request;
	private String response;
	private OrderInfo orderInfo;
	private String mode;
	
	public PaymentLoggingThread(String request,String response,OrderInfo orderInfo,String mode,IPaymentService paymentService) {
		super();
		this.request = request;
		this.response = response;
		this.orderInfo = orderInfo;
		this.mode = mode;
		this.paymentService = paymentService;
	}

	public void run(){
		if(mode.equalsIgnoreCase(PaymentConstants.ADD)){
			paymentService.insertPaymentOrderLogs(request, orderInfo.getTxnId());
		}else{
			paymentService.updatePaymentOrderLogs(response, orderInfo.getTxnId());
		}
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public OrderInfo getOrderInfo() {
		return orderInfo;
	}

	public void setOrderInfo(OrderInfo orderInfo) {
		this.orderInfo = orderInfo;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}
	
}
