package com.chargenmarch.common.dto.sms;

import java.util.HashMap;
import java.util.Map;

import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.enums.sms.SMSStatusCodes;
import com.chargenmarch.common.enums.sms.SMSTemplate;

public class SMSDTO {
	private int id;
	private UserDTO user;
	private String gateway;
	private SMSTemplate template;
	private SMSStatusCodes statusCode;
	private String message;
	private Map<Integer,String> parameterMap = new HashMap<Integer, String>();
	private String response;

	public SMSDTO(UserDTO user, String gateway, SMSTemplate template,SMSStatusCodes statusCode){
		this.user = user;
		this.gateway = gateway;
		this.template = template;
		this.statusCode = statusCode;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getGateway() {
		return gateway;
	}
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}
	
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}

	public SMSTemplate getTemplate() {
		return template;
	}

	public void setTemplate(SMSTemplate template) {
		this.template = template;
	}

	public Map<Integer, String> getParameterMap() {
		return parameterMap;
	}

	public void setParameterMap(Map<Integer, String> parameterMap) {
		this.parameterMap = parameterMap;
	}

	public SMSStatusCodes getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(SMSStatusCodes statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}


	
}
