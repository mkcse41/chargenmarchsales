package com.chargenmarch.common.service.http;

import java.util.Map;

public interface IHTTPService {
	public String HTTPPostRequest(String url, Map<String, String> paramMap);
	public String HTTPGetRequest(String url);
	/*public String HTTPGetRequestWithRestTemplate(String url);
	public String HTTPPostRequestWithRestTemplate(String url, Map<String, ?> paramMap);*/
}
