package com.chargenmarch.common.constants;

public interface PropertyKeyConstants {
	
	//sms.properties
	public interface SMSProperties{
		public static final String SMS_GATEWAY_URL = "sms.gateway.url";
		public static final String SMS_GATEWAY_NAME = "sms.gateway.name";
		public static final String OTP_SMS_TEMPLATE = "otp.sms.template";
		public static final String SUCCESS_SMS_TEMPLATE = "success.sms.template";
		public static final String SMS_GATEWAY_USERNAME ="sms.gateway.username";
		public static final String SMS_GATEWAY_PASSWORD = "sms.gateway.password";
		public static final String SMS_SENDER_NAME = "sms.senderName";
		public static final String CHECK_SMS_BALANCE_URL = "check.sms.balance.url";
		public static final String CHECK_SMS_BALANCE_EMAIL = "check.SMS.balance.email";
		public static final String CHECK_SMS_BALANCE_COUNT = "check.SMS.balance.count";
		public static final String NO_REPLY_MAIL_ID = "no.reply.mail.id";
		public static final String SMS_TEXT_LOCAL_GATEWAY_URL = "textlocal.sms.api.url";
		public static final String SMS_SERVICE_NAME = "sms.service.name";
		
		
	}
	//api.properties
	public interface APIProperties{
		public static final String API_OPERATOR_GET_KEY = "operator.get.api";
		public static final String API_OPERATOR_PLANS_GET_KEY = "operator.plans.get.api";
		public static final String RECHARGE_API_URL = "pay2all.recharge.api.url";
		public static final String PAY2ALL_USERNAME = "pay2all.username";
		public static final String PAY2ALL_PASSWORD = "pay2all.password";
		public static final String RECHARGE_EXCEPTION_MAILLIST = "recharge.exception.maillist";
		public static final String CASHBACK_RECHARGE_COUNT= "cashback.recharge.count";
		public static final String CASHBACK_RECHARGE_PERCENT = "cashback.recharge.percent";
		public static final String RECHARGE_SERVICE = "recharge.service";
		public static final String POSTAPID_BILLING_EXTRA_CHARGES = "postpaid.billing.extra.charges";
		public static final String RECHARGE_SECRET_KEY = "recharge.secret.key";
		public static final String USER_REFERRAL_CASHBACK = "user.referral.cashbackamount";
		public static final String CAM_SALES_WALLET_EXTRA_CHARGES = "camsales.wallet.extra.charges";
		public static final String CAM_SALES_RECHARGE_MARGIN= "camsales.recharge.margin";		
	}
	//rewardspoint.properties
	public interface RewardPointProperties{
		public static final String REWARDSPOINT_BASE_VALUE = "rewardspoint.base.value";
		public static final String REEDOM_REWARDSPOINT_BASE_VALUE = "reedom.rewardspoint.base.value";
	}
	//jolo api.properties
	public interface JOLOAPIProperties{
		public static final String OPERATOR_DETAILS_API = "operator.details.api";
		public static final String BILLING_RECHARGE_API = "billing.api.url";
		public static final String OPERATOR_PLANS_API = "operator.plan.api";
		public static final String RECHARGE_API_URL = "recharge.api.url";
		public static final String JOLOAPI_USERNAME = "joloapi.username";
		public static final String JOLOAPI_KEY = "jolo.apikey";
		public static final String RECHARGE_EXCEPTION_MAILLIST = "recharge.exception.maillist";
		public static final String RECHARGE_EXCEPTION = "exception.mail.id";
		public static final String CASHBACK_RECHARGE_COUNT= "cashback.recharge.count";
		public static final String CASHBACK_RECHARGE_PERCENT = "cashback.recharge.percent";
		public static final String EXTRA_CHARGES = "landline.billing.extra.charges";
		public static final String JOLO_OPERATORWISE_DOWN_STARTTIME = "jolo.operatorwise.servicedown.starttime";
		public static final String JOLO_OPERATORWISE_END_STARTTIME = "jolo.operatorwise.servicedown.endtime";
		public static final String JOLO_OPERATORNAME_SERVICEDOWN = "jolo.operatorname.service.down";
		public static final String JOLO_BALANCE_CHECK_API_URL = "jolo.balance.check.api.url";
		public static final String JOLO_RECHARGE_STATUS_URL = "jolo.recharge.status.url";
		public static final String JOLO_REFERRAL_RECHARGE_PERCENT = "user.referral.recharge.percent";
		public static final String PAY2ALL_RECHARGE_STATUS_URL = "pay2all.recharge.status";
		
	}
	
	//rewardspoint.properties
	public interface ServerProperties{
		public static final String PAYMENT_VERIFICATION_URL = "payment.verification.url";
	}
	
	//airticket.properties
	public interface AirTicketProperties {
		public static final String SERVICE_PROVIDER_URL_DOMESTIC = "air.service.providers.domestic";
		public static final String SERVICE_PROVIDER_URL_INTERNATIONAL = "air.service.providers.international";
		public static final String AIR_SEARCH_API = "air.search.api";
	}
}