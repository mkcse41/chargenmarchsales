package com.chargenmarch.common.dao.sms;

import com.chargenmarch.common.dto.sms.SMSDTO;
import com.chargenmarch.common.enums.sms.SMSStatusCodes;

public interface ISMSDao {
	public int saveSMStoLog(SMSDTO sms) throws Throwable;
	public int updateSMSStatus(SMSDTO sms, SMSStatusCodes status);
}
