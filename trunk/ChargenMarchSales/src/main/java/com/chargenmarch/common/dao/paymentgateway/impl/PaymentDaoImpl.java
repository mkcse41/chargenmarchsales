package com.chargenmarch.common.dao.paymentgateway.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.chargenmarch.charge.query.paymentgateway.PaymentQuery;
import com.chargenmarch.common.dao.paymentgateway.IPaymentDao;
import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.chargenmarch.common.dto.paymentgateway.ProductInfo;
import com.chargenmarch.common.mapper.paymentgateway.ProductInfoMapper;

@Repository
public class PaymentDaoImpl implements IPaymentDao{
    @Autowired
	private JdbcTemplate jdbcTemplate;
		
	public List<ProductInfo> loadProductData() {
		return (List) jdbcTemplate.query(PaymentQuery.SELECT_FROM_PRODUCT, new ProductInfoMapper());
	}
	
	public void insertPaymentOrder(OrderInfo orderInfo) {
		Object args[]= new Object[] {orderInfo.getProductId(),orderInfo.getOrderId(),orderInfo.getAmount(),orderInfo.getFirstName(),orderInfo.getLastName(),orderInfo.getEmailId(),orderInfo.getMobileno(),orderInfo.getPinCode(),orderInfo.getCity(),orderInfo.getState(),orderInfo.getAddress(),orderInfo.getStatus()};
		jdbcTemplate.update(PaymentQuery.INSERT_INTO_PAYMENT_GATEWAY_ORDER, args);
	}
	
	
	public void updatePaymentOrder(OrderInfo orderInfo) {
		Object args[]= new Object[] {orderInfo.getStatus(),orderInfo.getMihpayid(),orderInfo.getMode(),orderInfo.getDiscount(),
				orderInfo.getOffer(),orderInfo.getBankcode(),orderInfo.getPG_TYPE(),orderInfo.getBank_ref_num(),orderInfo.getShipping_firstname(),
				orderInfo.getShipping_lastname(),orderInfo.getShipping_address1(),orderInfo.getShipping_address2(),orderInfo.getShipping_city(),
				orderInfo.getShipping_state(),orderInfo.getShipping_country(),orderInfo.getShipping_zipcode(),orderInfo.getShipping_phone(),orderInfo.getError(),orderInfo.getHash(),orderInfo.getOrderId()};
			jdbcTemplate.update(PaymentQuery.UPDATE_PAYMENT_GATEWAY_ORDER, args);
	}
	
	public void insertPaymentOrderLogs(String request,String productId) {
		Object args[]= new Object[] {productId,request};
		jdbcTemplate.update(PaymentQuery.INSERT_INTO_PAYMENT_GATEWAY_LOGS, args);
	}
	
	public void updatePaymentOrderLogs(String response,String productId) {
		Object args[]= new Object[] {response,productId};
		jdbcTemplate.update(PaymentQuery.UPDATE_PAYMENT_GATEWAY_LOGS, args);
	}
	
	public int getOrderId(int productId){
		Object args[]= new Object[] {productId};
		return jdbcTemplate.queryForInt(PaymentQuery.SELECT_FROM_PAYMENT_GATEWAY_ORDER,args);
	}
	
}
