package com.chargenmarch.common.constants;

public class SessionConstants {
	public static final String USER_BEAN = "userbean";
	public static final String RECHARGE_FORM = "rechargeForm";
	public static final String LANDLINE_FORM = "LandlineForm";
	public static final String UPDATE_POPUP_INFO = "updatePopUpInfo";

}
