package com.chargenmarch.common.factory;


import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.chargenmarch.common.enums.paymentgateway.PaymentGateWayName;
import com.chargenmarch.common.service.paymentgateway.IPaymentService;
import com.chargenmarch.common.service.paymentgateway.impl.PaymentServiceImpl;

/**
 * 
 * @author mukesh
 *
 */
public class PaymentServiceFactory {

	public static IPaymentService getService(OrderInfo orderInfo){
		if(orderInfo.getPgType().equalsIgnoreCase(PaymentGateWayName.PayU.name())){
			return PaymentServiceImpl.getPayUServiceInstance();
		}
		return PaymentServiceImpl.getPayUServiceInstance();
	}
}
