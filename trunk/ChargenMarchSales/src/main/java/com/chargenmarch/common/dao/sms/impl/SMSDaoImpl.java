package com.chargenmarch.common.dao.sms.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.chargenmarch.charge.query.sms.SMSQueries;
import com.chargenmarch.common.dao.sms.ISMSDao;
import com.chargenmarch.common.dto.sms.SMSDTO;
import com.chargenmarch.common.enums.sms.SMSStatusCodes;

@Repository
public class SMSDaoImpl implements ISMSDao{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public int saveSMStoLog(final SMSDTO sms) throws Throwable{
		try{
			KeyHolder holder = new GeneratedKeyHolder();
			jdbcTemplate.update(new PreparedStatementCreator() {           
		           @Override
		           public PreparedStatement createPreparedStatement(Connection connection)
		                        throws SQLException {
		                    PreparedStatement ps = connection.prepareStatement(SMSQueries.INSERT_INTO_SMS_LOG, Statement.RETURN_GENERATED_KEYS);
		                    ps.setString(1, sms.getUser().getMobileNo());
		                    ps.setString(2, sms.getMessage());
		                    ps.setString(3, sms.getGateway());
		                    ps.setInt(4, sms.getStatusCode().ordinal());
		                    return ps;
		                }
		            }, holder);
			
			return holder.getKey().intValue();
		} catch(Exception e){
			throw e;
		}
	}
	
	public int updateSMSStatus(SMSDTO sms, SMSStatusCodes status){
		Object[] args = {status.ordinal(),sms.getResponse(),sms.getId()};
		return jdbcTemplate.update(SMSQueries.UPDATE_SMS_STATUS, args);
	}

}
