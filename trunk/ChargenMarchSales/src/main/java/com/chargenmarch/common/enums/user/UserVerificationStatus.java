package com.chargenmarch.common.enums.user;

public enum UserVerificationStatus {
	UNVERIFIED,	
	VERIFIED;
}
