package com.chargenmarch.common.forms.paymentgateway;


/**
 * 
 * @author mukesh
 *
 */
public class PayUResponseForm{

	private String mihpayid;
	private String mode;
	private String status;
	private String key;
	private String txnid;
	private String amount ;
	private String discount ;
	private String offer;
	private String productinfo ;
	private String firstname  ;
	private String lastname;
	private String email;
	private String phone ;
	private String zipcode ;
	private String Hash;
	private String Error ;
	private String bankcode ;
	private String city;
	private String state;
	private String country ;
	private String address1;
	private String PG_TYPE;
	private String bank_ref_num ;
	private String shipping_firstname ;
	private String shipping_lastname ;
	private String shipping_address1;
	private String shipping_address2;
	private String shipping_city;
	private String shipping_state ;
	private String shipping_country ;
	private String shipping_zipcode ;
	private String shipping_phone ;
	private String unmappedstatus;
	
	public String getMihpayid() {
		return mihpayid;
	}
	public void setMihpayid(String mihpayid) {
		this.mihpayid = mihpayid;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getTxnid() {
		return txnid;
	}
	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getOffer() {
		return offer;
	}
	public void setOffer(String offer) {
		this.offer = offer;
	}
	public String getProductinfo() {
		return productinfo;
	}
	public void setProductinfo(String productinfo) {
		this.productinfo = productinfo;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	
	public String getHash() {
		return Hash;
	}
	public void setHash(String hash) {
		Hash = hash;
	}
	public String getError() {
		return Error;
	}
	public void setError(String error) {
		Error = error;
	}
	public String getBankcode() {
		return bankcode;
	}
	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getAddress() {
		return address1;
	}
	public void setAddress(String address) {
		this.address1 = address;
	}
	public String getPG_TYPE() {
		return PG_TYPE;
	}
	public void setPG_TYPE(String pG_TYPE) {
		PG_TYPE = pG_TYPE;
	}
	public String getBank_ref_num() {
		return bank_ref_num;
	}
	public void setBank_ref_num(String bank_ref_num) {
		this.bank_ref_num = bank_ref_num;
	}
	public String getShipping_firstname() {
		return shipping_firstname;
	}
	public void setShipping_firstname(String shipping_firstname) {
		this.shipping_firstname = shipping_firstname;
	}
	public String getShipping_lastname() {
		return shipping_lastname;
	}
	public void setShipping_lastname(String shipping_lastname) {
		this.shipping_lastname = shipping_lastname;
	}
	public String getShipping_address1() {
		return shipping_address1;
	}
	public void setShipping_address1(String shipping_address1) {
		this.shipping_address1 = shipping_address1;
	}
	public String getShipping_address2() {
		return shipping_address2;
	}
	public void setShipping_address2(String shipping_address2) {
		this.shipping_address2 = shipping_address2;
	}
	public String getShipping_city() {
		return shipping_city;
	}
	public void setShipping_city(String shipping_city) {
		this.shipping_city = shipping_city;
	}
	public String getShipping_state() {
		return shipping_state;
	}
	public void setShipping_state(String shipping_state) {
		this.shipping_state = shipping_state;
	}
	public String getShipping_country() {
		return shipping_country;
	}
	public void setShipping_country(String shipping_country) {
		this.shipping_country = shipping_country;
	}
	public String getShipping_zipcode() {
		return shipping_zipcode;
	}
	public void setShipping_zipcode(String shipping_zipcode) {
		this.shipping_zipcode = shipping_zipcode;
	}
	public String getShipping_phone() {
		return shipping_phone;
	}
	public void setShipping_phone(String shipping_phone) {
		this.shipping_phone = shipping_phone;
	}
	public String getUnmappedstatus() {
		return unmappedstatus;
	}
	public void setUnmappedstatus(String unmappedstatus) {
		this.unmappedstatus = unmappedstatus;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	
	
	
}
