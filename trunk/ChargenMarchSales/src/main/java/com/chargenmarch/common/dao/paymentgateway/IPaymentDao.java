package com.chargenmarch.common.dao.paymentgateway;

import java.util.List;

import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.chargenmarch.common.dto.paymentgateway.ProductInfo;

public interface IPaymentDao {

	public List<ProductInfo> loadProductData();
	public void insertPaymentOrder(OrderInfo orderInfo);
	public void updatePaymentOrder(OrderInfo orderInfo);
	public void insertPaymentOrderLogs(String request,String productId) ;
	public void updatePaymentOrderLogs(String response,String productId) ;
	public int getOrderId(int productId);
}
