package com.chargenmarch.common.dto.paymentgateway;

public class ProductInfo {
    private int id;
    private String productName;
    private String merchantId;
    private String salt;
    private String paymentGateWay;
    private String surl;
    private String furl;
    private String curl;
    private String receiverEmailId;
    private String recipient;
    private String isSendMail;
    private boolean isRequiredResponse;
    private String orderprefix;
    private String pgUrl;
    private PaymentGatewayInfo paymentGateWayInfo;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	public String getPaymentGateWay() {
		return paymentGateWay;
	}
	public void setPaymentGateWay(String paymentGateWay) {
		this.paymentGateWay = paymentGateWay;
	}
	public String getSurl() {
		return surl;
	}
	public void setSurl(String surl) {
		this.surl = surl;
	}
	public String getFurl() {
		return furl;
	}
	public void setFurl(String furl) {
		this.furl = furl;
	}
	public String getCurl() {
		return curl;
	}
	public void setCurl(String curl) {
		this.curl = curl;
	}
	public String getReceiverEmailId() {
		return receiverEmailId;
	}
	public void setReceiverEmailId(String receiverEmailId) {
		this.receiverEmailId = receiverEmailId;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public String getIsSendMail() {
		return isSendMail;
	}
	public void setIsSendMail(String isSendMail) {
		this.isSendMail = isSendMail;
	}
	public boolean isRequiredResponse() {
		return isRequiredResponse;
	}
	public void setRequiredResponse(boolean isRequiredResponse) {
		this.isRequiredResponse = isRequiredResponse;
	}
	public String getOrderprefix() {
		return orderprefix;
	}
	public void setOrderprefix(String orderprefix) {
		this.orderprefix = orderprefix;
	}
	public String getPgUrl() {
		return pgUrl;
	}
	public void setPgUrl(String pgUrl) {
		this.pgUrl = pgUrl;
	}
	public PaymentGatewayInfo getPaymentGateWayInfo() {
		return paymentGateWayInfo;
	}
	public void setPaymentGateWayInfo(PaymentGatewayInfo paymentGateWayInfo) {
		this.paymentGateWayInfo = paymentGateWayInfo;
	}

	
  
  
}
