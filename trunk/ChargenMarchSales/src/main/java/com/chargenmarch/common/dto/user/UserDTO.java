package com.chargenmarch.common.dto.user;

import java.util.Date;

public class UserDTO {
	private int id;
	private String name;
	private String emailId;
	private String mobileNo;
	private int rechargeCount;
	private Date registrationDate;
	private Date updatedate;
	private String source;
	private String password;
	private int walletBalance;
	private String isCAMLogin;
	private int otp;
	private int isverified;
	private String referralCode;
	private int referralBalance;
	private String browserNotificationId;
	private int isSalesUser;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public int getRechargeCount() {
		return rechargeCount;
	}
	public void setRechargeCount(int rechargeCount) {
		this.rechargeCount = rechargeCount;
	}
	public Date getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
	public Date getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getWalletBalance() {
		return walletBalance;
	}
	public void setWalletBalance(int walletBalance) {
		this.walletBalance = walletBalance;
	}	
	public String getIsCAMLogin() {
		return isCAMLogin;
	}
	public void setIsCAMLogin(String isCAMLogin) {
		this.isCAMLogin = isCAMLogin;
	}
	public int getOtp() {
		return otp;
	}
	public void setOtp(int otp) {
		this.otp = otp;
	}
	public int getIsverified() {
		return isverified;
	}
	public void setIsverified(int isverified) {
		this.isverified = isverified;
	}
	public String getReferralCode() {
		return referralCode;
	}
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
	public int getReferralBalance() {
		return referralBalance;
	}
	public void setReferralBalance(int referralBalance) {
		this.referralBalance = referralBalance;
	}
	
	public String getBrowserNotificationId() {
		return browserNotificationId;
	}
	public void setBrowserNotificationId(String browserNotificationId) {
		this.browserNotificationId = browserNotificationId;
	}
	
	public int getIsSalesUser() {
		return isSalesUser;
	}
	public void setIsSalesUser(int isSalesUser) {
		this.isSalesUser = isSalesUser;
	}
	@Override
	public String toString() {
		return "UserDTO [id=" + id + ", name=" + name + ", emailId=" + emailId + ", mobileNo=" + mobileNo
				+ ", rechargeCount=" + rechargeCount + ", registrationDate=" + registrationDate + ", updatedate="
				+ updatedate + ", source=" + source + ", password=" + password + ", walletBalance=" + walletBalance
				+ ", isCAMLogin=" + isCAMLogin + ", otp=" + otp + ", isverified=" + isverified + ", referralCode="
				+ referralCode + ", referralBalance=" + referralBalance + "]";
	}
	
}
