package com.chargenmarch.common.dto.paymentgateway;

/**
 * 
 * @author mukesh
 *
 */
public class PaymentGatewayInfo {

	private String url;
	private String merchantId;
	private String salt;

	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	
}
