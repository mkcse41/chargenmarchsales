package com.chargenmarch.common.service.http.impl;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.chargenmarch.common.service.http.IHTTPService;

@Service
public class HTTPServiceImpl implements IHTTPService{
	private final static Logger logger = LoggerFactory.getLogger(HTTPServiceImpl.class);
	
	private RestTemplate restTemplate;
	
	public RestTemplate getRestTemplate() {
		if (restTemplate == null) {
			restTemplate = new RestTemplate();
		}
		return restTemplate;
	}

	/*public String HTTPPostRequest(String url, Map paramMap){
		try{
			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(url);
			if(paramMap!=null)
				post.setEntity(new UrlEncodedFormEntity(getPostParamList(paramMap)));
			
			HttpResponse response = httpClient.execute(post);
			HttpEntity entity = response.getEntity();
			String responseString = EntityUtils.toString(entity, Constants.UTF_8);
			return responseString;
		}catch(ClientProtocolException e){
			e.printStackTrace();
			return Constants.BLANK;
		}
		catch(IOException e){
			e.printStackTrace();
			return Constants.BLANK;
		}
		catch(Exception e){
			e.printStackTrace();
			return Constants.BLANK;
		}
	}
	
	private ArrayList<NameValuePair> getPostParamList(Map paramMap){
		ArrayList<NameValuePair> urlParams = new ArrayList<NameValuePair>();
		
		if(paramMap!=null && paramMap.size()>Constants.ZERO){
			Iterator<Entry> iterator = paramMap.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry entry = (Map.Entry) iterator.next();
				urlParams.add(new BasicNameValuePair(String.valueOf(entry.getKey()), String.valueOf(entry.getValue())));
			}
		}
		return urlParams;
	}
	
	public String HTTPGetRequest(String url){
		try{
			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpResponse response = httpClient.execute(new HttpGet(url));
			HttpEntity entity = response.getEntity();
			String responseString = EntityUtils.toString(entity, Constants.UTF_8);
			return responseString;
		}catch(ClientProtocolException e){
			e.printStackTrace();
			return Constants.BLANK;
		}
		catch(IOException e){
			e.printStackTrace();
			return Constants.BLANK;
		}
		catch(Exception e){
			e.printStackTrace();
			return Constants.BLANK;
		}
	}
	*/
	public String HTTPGetRequest(String url) {
		RestTemplate restTemplate = getRestTemplate();
		URI uri = null;
		try {
			uri = new URI(url);
			return restTemplate.getForObject(uri, String.class);
		} catch (URISyntaxException e) {
			logger.error("Error in URI creation in HTTP GET .\n URL : "+url, e);
		}
		return restTemplate.getForObject(url, String.class);
	}
	
	public String HTTPPostRequest(String url, Map<String, String> paramMap) {
		RestTemplate restTemplate = getRestTemplate();
		MultiValueMap<String, String> map = getMultiValueMap(paramMap);
		URI uri = null;
		try {
			uri = new URI(url);
			return restTemplate.postForObject(uri, map, String.class);
		} catch (URISyntaxException e) {
			logger.error("Error in URI creation in HTTP POST .\n URL : "+url, e);
		}
		return restTemplate.postForObject(url, map, String.class);		
	}
	
	public MultiValueMap<String, String> getMultiValueMap(Map<String, String> paramMap) {
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		for(Entry<String, String> entry : paramMap.entrySet()) {
			map.add(entry.getKey(), entry.getValue());
		}
		return map;
	}
}
