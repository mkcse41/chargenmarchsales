package com.chargenmarch.common.service.paymentgateway.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.FilePathConstants;
import com.chargenmarch.common.constants.PropertyKeyConstants;
import com.chargenmarch.common.constants.paymentgateway.PaymentConstants;
import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.chargenmarch.common.dto.paymentgateway.PaymentGatewayInfo;
import com.chargenmarch.common.dto.paymentgateway.PaymentValidateDTO;
import com.chargenmarch.common.dto.paymentgateway.ProductInfo;
import com.chargenmarch.common.enums.paymentgateway.Payment;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.service.paymentgateway.IPaymentService;
import com.chargenmarch.common.utility.IUtility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

/**
 * 10 Oct 2015
 * This Service used to validate payU request,generate checksum and save payment order and payment logs.
 * @author mukesh
 */
@Service("paymentService")
@PropertySource(FilePathConstants.CLASSPATH+FilePathConstants.SERVER_PROPERTIES)
public class PayUServiceImpl extends PaymentServiceImpl{

	private final static Logger logger = LoggerFactory.getLogger(PayUServiceImpl.class);
	
	@Autowired
	IEmailSenderService emailSenderService;
	
	@Autowired
	IPaymentService paymentService;
	
	@Autowired IUtility utility;
	
	@Autowired
	private Environment env;
	
    /**
     * This method used to 
     *    (i)   Generate order id.
     *    (ii)  Generate check sum code key|txnid|amount|productinfo|firstname|email||udf2||udf4|||||||SALT provided by PayU Payment service
     *    (iii) Validate required parameter.
     *    (iv)  if validate request is true then save payment order and payment logs, redirect payment gateway site
     */
	public OrderInfo processPayment(OrderInfo orderInfo){
		try{
			logger.info("paymentService==>"+paymentService);
			ProductInfo productInfo = getProductInfoMap(orderInfo.getProductName());
			logger.info("productInfo==>"+productInfo);
			PaymentGatewayInfo paymentGateWayInfo = productInfo==null?null:productInfo.getPaymentGateWayInfo();
			if(null!=orderInfo && null!=productInfo && null!=paymentGateWayInfo && null!=productInfo.getOrderprefix() && !productInfo.getOrderprefix().equals(Constants.BLANK)){
				int txnId = getOrderId(productInfo==null?Constants.ZERO:productInfo.getId());
				String orderId = String.valueOf(productInfo.getOrderprefix()+Constants.BLANK+txnId);
				setValuesInOrderInfoObject(orderInfo, orderId, productInfo, paymentGateWayInfo,txnId);
				//validate PayU request
				List<String> errorStringList = validatePayURequest(orderInfo);
				if(errorStringList.size()==0){
					String checksumCode = generateRequestCheckSumCode(orderInfo, setPaymentGateWayInfo(productInfo.getMerchantId(), productInfo.getSalt(), paymentGateWayInfo));
					orderInfo.setHash(checksumCode);
					//save payment order and payment logs
					savePaymentOrder(orderInfo, PaymentConstants.ADD, String.valueOf(orderInfo.getTxnId()), convertObjectToJSON(orderInfo), Constants.BLANK,paymentService);
				}else{
					orderInfo.setErrorStringList(errorStringList);
				}
			}else{
				orderInfo.getErrorStringList().add("Missing required field like as order prefix,product name,payment gateway not valid");
			}
			return orderInfo;	
		}catch(Exception e){
			orderInfo.getErrorStringList().add("Missing required field like as product name not valid,payment gateway not valid");
			logger.error("Error in payU processed payment");
			String errorString = getPaymentStatusMailString(orderInfo)+"<br/>"+e.getMessage();
		    emailSenderService.sendEmail(Constants.NOREPLY_EMAIL_ID, PaymentConstants.PAYMENT_ERROR_EMAIL_ID.split(Constants.COMMA), "Error in PayU Process Payment order "+orderInfo.getTxnId(), errorString,null,null, false);
			e.printStackTrace();
		}
		return orderInfo;	
	}
 
	/**
	 * Generate check sum code key|txnid|amount|productinfo|firstname|email||udf2||udf4|||||||SALT  Provided by PayU Payment service
	 * @param orderInfo
	 * @param paymentGateWayInfo
	 * @return
	 */
    public String generateRequestCheckSumCode(OrderInfo orderInfo,PaymentGatewayInfo paymentGateWayInfo ){
	  String checkSumCode = Constants.BLANK;
	  if(null!=paymentGateWayInfo){
		   String hexString = paymentGateWayInfo.getMerchantId()+PaymentConstants.PIPE+orderInfo.getTxnId()+PaymentConstants.PIPE+orderInfo.getAmount()+PaymentConstants.PIPE+orderInfo.getProductName()+PaymentConstants.PIPE+orderInfo.getFirstName()+PaymentConstants.PIPE+orderInfo.getEmailId()+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+paymentGateWayInfo.getSalt();
		   logger.info("requst hex String==>"+hexString);
		   try {
			   checkSumCode = convertStringToCheckSumCode(PaymentConstants.SHA_512, hexString);
			   logger.info("requst checkSumCode String==>"+checkSumCode);
			   return checkSumCode;
		   } catch (Exception e) {
			   logger.error("Error in generate checksum code ==>>> emailId="+orderInfo.getEmailId()+",mobileNo="+orderInfo.getMobileno()+"");
			   String errorString = getPaymentStatusMailString(orderInfo)+"<br/>"+e.getMessage();
			   emailSenderService.sendEmail(Constants.NOREPLY_EMAIL_ID, PaymentConstants.PAYMENT_ERROR_EMAIL_ID.split(Constants.COMMA), "Error in generate checksum code order "+orderInfo.getTxnId(), errorString,null,null, false);
			   e.printStackTrace();
		 }
	  }
	  return checkSumCode;
   }
  
   /**
    * Generate check sum code algorithm wise like as SHA-256,SHA-512,MD5 
    * @param algorithm
    * @param str
    * @return
    * @throws Exception
    */
   private String convertStringToCheckSumCode(String algorithm,String str)throws Exception {
		try{
	        MessageDigest md = MessageDigest.getInstance(algorithm);
	        md.update(str.getBytes());
	        byte byteData[] = md.digest();
	        //convert the byte to hex format method 1
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < byteData.length; i++) {
	         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
	        }
	        //convert the byte to hex format method 2
	        StringBuffer hexString = new StringBuffer();
	    	for (int i=0;i<byteData.length;i++) {
	    		String hex=Integer.toHexString(0xff & byteData[i]);
	   	     	if(hex.length()==1) hexString.append(Constants.ZERO_STRING);
	   	     	hexString.append(hex);
	    	}
	    	return hexString.toString();
	     }catch(Exception e){
	    	 e.printStackTrace();
	     }
		return Constants.BLANK;
   }
   
   /* Validate PayU Request,Check all required parameter for PayU payment gateway request */
   private List<String> validatePayURequest(OrderInfo orderInfo){
	   List<String> errorStringList = new ArrayList<String>();
	   if(orderInfo.getFirstName().equals(Constants.BLANK)){
		   errorStringList.add("FirstName");
	   }
	   if(orderInfo.getEmailId().equals(Constants.BLANK)){
		   errorStringList.add("Emailid");
	   }
	   if(orderInfo.getMobileno().equals(Constants.BLANK)){
		   errorStringList.add("MobileNo");
	   }
	   if(orderInfo.getPinCode().equals(Constants.BLANK)){
		   errorStringList.add("Pincode");
	   }
	   if(orderInfo.getAmount()<=0){
		  errorStringList.add("Amount is greater then zero");
	   }
	   if(orderInfo.getProductName().equals(Constants.BLANK)){
		   errorStringList.add("Product Name");
	   }
	   if(orderInfo.getTxnId().equals(Constants.BLANK)){
		   errorStringList.add("Error in generate OrderId");
	   }
	   if(orderInfo.getUserCredentials().equals(Constants.BLANK)){
		   errorStringList.add("User credentials is not blank");
	   }
	   return errorStringList;
   }
   
   private PaymentGatewayInfo setPaymentGateWayInfo(String merchantId,String salt,PaymentGatewayInfo paymentGatewayInfo){
		paymentGatewayInfo.setSalt(salt);
		paymentGatewayInfo.setMerchantId(merchantId);
		return paymentGatewayInfo;
	}
	
   private String convertObjectToJSON(OrderInfo orderInfo){
	   try{
	     Gson gson = new Gson();
		 return  gson.toJson(orderInfo);
	   }catch(Exception e){
		   logger.error("Error in convertObjectToJSON");
		   e.printStackTrace();
		   
	   }
	   return Constants.BLANK;
   }
   
   //hash = SALT|status|||||||||||email|firstname|productinfo|amount|txnid|key
   public String generateResponseCheckSumCode(OrderInfo orderInfo,PaymentGatewayInfo paymentGateWayInfo ){
		  String checkSumCode = Constants.BLANK;
		  if(null!=paymentGateWayInfo){
			   String hexString = paymentGateWayInfo.getSalt()+PaymentConstants.PIPE+orderInfo.getStatus()+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+PaymentConstants.PIPE+orderInfo.getEmailId()+PaymentConstants.PIPE+orderInfo.getFirstName()+PaymentConstants.PIPE+orderInfo.getProductName()+PaymentConstants.PIPE+orderInfo.getAmount()+PaymentConstants.PIPE+orderInfo.getTxnId()+PaymentConstants.PIPE+paymentGateWayInfo.getMerchantId();
			   try {
				   checkSumCode = convertStringToCheckSumCode(PaymentConstants.SHA_512, hexString);
				   return checkSumCode;
			   } catch (Exception e) {
				   logger.error("Error in generate checksum code ==>>> emailId="+orderInfo.getEmailId()+",mobileNo="+orderInfo.getMobileno()+"");
				   e.printStackTrace();
			 }
		  }
		  return checkSumCode;
	   }
   
   /**
    * Validate payU Response
    */
   public OrderInfo validatePaymentOrder(OrderInfo orderInfo){
	   try{
			ProductInfo productInfo = getProductInfoMap(orderInfo.getProductName());
			PaymentGatewayInfo paymentGateWayInfo = productInfo==null?null:productInfo.getPaymentGateWayInfo();
			if(null!=orderInfo && null!=productInfo && null!=paymentGateWayInfo && isValidatePayment(orderInfo,paymentGateWayInfo)){
				orderInfo.setPaymentGateWayInfo(paymentGateWayInfo);
				String response = convertObjectToJSON(orderInfo);
				orderInfo.setProductId(productInfo.getId());
				setUrlsInOrderInfoObject(orderInfo, productInfo, response);
				//save payment order and payment logs
				savePaymentOrder(orderInfo, PaymentConstants.UPDATE, String.valueOf(orderInfo.getOrderId()), Constants.BLANK, convertObjectToJSON(orderInfo),paymentService);
				//send payment status 
				if(null!=productInfo.getIsSendMail() && productInfo.getIsSendMail().equalsIgnoreCase(PaymentConstants.TRUE.toLowerCase())){
					sendPaymentStatusMail(orderInfo,productInfo);
				}
			}
		}catch(Exception e){
			    orderInfo.getErrorStringList().add("Error in validate payment order");
			    String errorString = getPaymentStatusMailString(orderInfo)+"<br/>"+e.getMessage();
				emailSenderService.sendEmail(Constants.NOREPLY_EMAIL_ID, PaymentConstants.PAYMENT_ERROR_EMAIL_ID.split(Constants.COMMA), "Error in PayU Validate Payment order "+orderInfo.getTxnId(), errorString,null,null, false);
			    logger.error("PayUServiceImpl ==> Error in validate payment order");
				e.printStackTrace();
	    }
	   return orderInfo;
   }
   
   private OrderInfo setValuesInOrderInfoObject(OrderInfo orderInfo,String txnId,ProductInfo productInfo,PaymentGatewayInfo paymentGateWayInfo,int orderId){
	    String userCredentials =   paymentGateWayInfo.getMerchantId() + PaymentConstants.COLON + orderInfo.getUserCredentials();
	    orderInfo.setTxnId(txnId);
	    orderInfo.setOrderId(String.valueOf(orderId));
		orderInfo.setProductId(productInfo.getId());
		orderInfo.setStatus(Payment.PENDING.name());
		orderInfo.setPaymentGateWayInfo(paymentGateWayInfo);
		orderInfo.setSuccessUrl(utility.getBaseUrl()+PaymentConstants.SLASH+PaymentConstants.PAYU_RESPONSE_URL);
		orderInfo.setFailureUrl(utility.getBaseUrl()+PaymentConstants.SLASH+PaymentConstants.PAYU_RESPONSE_URL);
		orderInfo.setCancelUrl(utility.getBaseUrl()+PaymentConstants.SLASH+PaymentConstants.PAYU_RESPONSE_URL);
		orderInfo.setUserCredentials(userCredentials);
		return orderInfo;
   }
   
   private void sendPaymentStatusMail(OrderInfo orderInfo,ProductInfo productInfo){
	   String receiverEmailId = productInfo.getReceiverEmailId()==null?Constants.BLANK:productInfo.getReceiverEmailId();
	   String recipient[] = productInfo.getRecipient()==null?Constants.BLANK.split(PaymentConstants.COMMA):productInfo.getRecipient().split(PaymentConstants.COMMA);
	   if(receiverEmailId.equalsIgnoreCase(Constants.BLANK)){
	        String  mailString = getPaymentStatusMailString(orderInfo);
	        emailSenderService.sendEmail(Constants.NOREPLY_EMAIL_ID, PaymentConstants.PAYMENT_ERROR_EMAIL_ID.split(Constants.COMMA), "Order "+orderInfo.getTxnId()+" Status", mailString,null,null, false);
	   }
   }
   
   private String getPaymentStatusMailString(OrderInfo orderInfo){
	    String errorEmailString = "<table border=1><tr><th>Order Id</th><th>Product Name</th><th>Amount</th><th>FirstName</th><th>Email Id</th><th>MobileNo</th><th>Status</th></tr>";
		errorEmailString += "<tr><td>"+orderInfo.getTxnId()+"</td><td>"+orderInfo.getProductName()+"</td><td>"+orderInfo.getAmount()+"</td><td>"+orderInfo.getFirstName()+"</td><td>"+orderInfo.getEmailId()+"</td><td>"+orderInfo.getMobileno()+"</td><td>"+orderInfo.getStatus()+"</td></tr>";
		errorEmailString += "</table>";
		return errorEmailString;
   }
   
   private OrderInfo setUrlsInOrderInfoObject(OrderInfo orderInfo,ProductInfo productInfo,String response){
	   String baseUrl = utility.getBaseUrl();
	   logger.info("payment redirect url==>"+baseUrl);
	   if(productInfo.isRequiredResponse()){
		   orderInfo.setSuccessUrl(baseUrl+PaymentConstants.SLASH+productInfo.getSurl());
		   orderInfo.setFailureUrl(baseUrl+PaymentConstants.SLASH+productInfo.getFurl());
		   orderInfo.setCancelUrl(baseUrl+PaymentConstants.SLASH+productInfo.getCurl()); 
		   orderInfo.setResponsejson(response);
       }else{
    	   orderInfo.setSuccessUrl(productInfo.getSurl());
		   orderInfo.setFailureUrl(productInfo.getFurl());
		   orderInfo.setCancelUrl(productInfo.getCurl());
       }
	   orderInfo.setProductInfo(productInfo);
	   int length = productInfo.getOrderprefix().length();
	   String orderId = orderInfo.getTxnId().length()>=length?orderInfo.getTxnId().substring(length):orderInfo.getTxnId();
	   orderInfo.setOrderId(orderId);
	   return orderInfo;
   }
   
   private boolean isValidatePayment(OrderInfo orderInfo,PaymentGatewayInfo paymentGateWayInfo){
	    String payUVerificationUrl = env.getProperty(PropertyKeyConstants.ServerProperties.PAYMENT_VERIFICATION_URL);
	    //String payUVerificationUrl = "https://test.payu.in/merchant/postservice?form=2";
		String method = "verify_payment";
		 //Salt of the merchant
		String salt = paymentGateWayInfo.getSalt();
		//Key of the merchant
		String key = paymentGateWayInfo.getMerchantId();
		String var1= orderInfo.getTxnId();//transaction id
		String toHash = key + "|" + method + "|" + var1 + "|" + salt;	
		
	    // Create connection 
	    try {
	    	    String Hashed = convertStringToCheckSumCode("SHA-512",toHash);
		        String Poststring = "key=" + key +  "&command=" + method +  "&hash=" + Hashed + "&var1=" + var1 ;
			    URL url = new URL(payUVerificationUrl);
			    URLConnection conn = url.openConnection();
			    conn.setDoOutput(true);
			    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			    wr.write(Poststring);
			    wr.flush();
			    // Get the response
			    BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			    String line;
			    String response = "";
			    while ((line = rd.readLine()) != null) {
			        response += line;
			    }                    
			    try {
			    	 logger.info("Payment Validate Response==>"+response);
			    	 Gson gson = new GsonBuilder().create(); 
				     JsonObject json =  gson.fromJson(response, JsonObject.class);
				     String paymentValidateJson = json.getAsJsonObject("transaction_details").get(var1).toString();
				     PaymentValidateDTO paymentValidateDto =  gson.fromJson(paymentValidateJson, PaymentValidateDTO.class);
				     if(null!=paymentValidateDto 
				    		 && paymentValidateDto.getStatus().equalsIgnoreCase(orderInfo.getStatus())){
				         logger.info("Payment Validate ==>"+paymentValidateDto.getStatus());
				         return true;
				     }
			     }catch(Exception e){
			    	 e.printStackTrace();
			     }
	  } catch (Exception e) {
	   	   e.printStackTrace();
	   	   logger.error("Error in verify Payment method");
	  }
		return false; 
   }
}
