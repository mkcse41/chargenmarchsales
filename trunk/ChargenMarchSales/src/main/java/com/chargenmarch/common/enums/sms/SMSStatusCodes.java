package com.chargenmarch.common.enums.sms;

public enum SMSStatusCodes {
	LOGGED,
	SENDING_INITIATED,
	SUCCESSFUL,
	FAILED,
}
