package com.chargenmarch.common.cron;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;
import com.chargenmarch.charge.service.mobilerecharge.IMobileRechargeService;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.dao.user.IUserDao;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.enums.user.UserVerificationStatus;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.utility.IUtility;

@Service
//@EnableScheduling
public class UserCrons {

	@Autowired
	Environment env;

	@Autowired
	IEmailSenderService mailService;

	@Autowired
	IUserDao userDao;

	@Autowired
	IUtility util;
	
	@Autowired
	IMobileRechargeService rechargeDao;
	
	//@Scheduler
	public void unverifiedUsersMailer() {
		List<UserDTO> unverifiedUsers = userDao.getUserByVerificationStatus(UserVerificationStatus.UNVERIFIED);
		for(UserDTO user : unverifiedUsers) {
			//mail
		}
	}
	
	//@Scheduler
	public void firstRechargeReminder() {
		DateTimeFormatter formatter = DateTimeFormat.forPattern(Constants.DateConstants.DEFAULT_DATE_FORMAT);
		String currentDate = util.getCurrentDate(Constants.DateConstants.DEFAULT_DATE_FORMAT);
		DateTime dt = formatter.parseDateTime(currentDate).minusDays(3);
		String dateThreeDaysBefore = formatter.print(dt);
		
		List<UserDTO> userList = userDao.getUserListByRegistrationDate(dateThreeDaysBefore);
		if(userList == null || userList.size()==0) {
			return;
		}
		
		for(UserDTO user : userList) {
			List<RechargeDTO> rechargeList = rechargeDao.getRechargeListByChargerId(String.valueOf(user.getId()));
			if(rechargeList == null || rechargeList.size() ==0) {
				//Mailer Code
			}
		}
	}
	
	//@Scheduler
	public void regularRechargeReminder() {
		DateTimeFormatter formatter = DateTimeFormat.forPattern(Constants.DateConstants.DEFAULT_DATE_FORMAT);
		String currentDate = util.getCurrentDate(Constants.DateConstants.DEFAULT_DATE_FORMAT);
		DateTime dt = formatter.parseDateTime(currentDate).minusDays(7);
		String dateSevenDaysBefore = formatter.print(dt);
		
		List<RechargeDTO> rechargeLastWeek = rechargeDao.getRechargeListAfterDate(dateSevenDaysBefore);
		List<RechargeDTO> rechargeBeforeLastWeek = rechargeDao.getRechargeListBeforeDate(dateSevenDaysBefore);
		List<Integer> userIdLastWeek = new ArrayList<Integer>();
		List<Integer> userIdBeforeLastWeek = new ArrayList<Integer>();
		List<UserDTO> userList = new ArrayList<UserDTO>();
		
		if(rechargeBeforeLastWeek == null || rechargeBeforeLastWeek.size() == 0) {
			return;
		}
		
		if(rechargeLastWeek == null || rechargeLastWeek.size()==0) {
			//Send mail to all from rechargeBeforeLastWeek
			return;
		}
		
		for(RechargeDTO recharge : rechargeLastWeek) {
			userIdLastWeek.add(recharge.getUserDto().getId());
		}
		
		for(RechargeDTO recharge : rechargeBeforeLastWeek) {
			userIdBeforeLastWeek.add(recharge.getUserDto().getId());
		}
		
		//Remove all the users from last week
		userIdBeforeLastWeek.removeAll(userIdLastWeek);
		
		for(Integer userId : userIdBeforeLastWeek) {
			List<UserDTO> user = userDao.getUserById(userId);
			if(user != null && user.size()!= 0) {
				userList.add(user.get(0));
			}
		}
		
		//Mailer code for UserList
	}
}
