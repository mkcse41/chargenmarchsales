package com.chargenmarch.common.constants;

public class RequestConstants {
	public static final String SLASH = "/";
	public static final String USER = "/user";
	public static final String REGISTER = "/register";
	public static final String REGISTERED_HTM = "/registered.htm";
	public static final String UPDATE_POPUP_INFO = "/updatePopUpInfo.htm";
	public static final String REGISTERED_OTHER_HTM = "/registeredOther.htm";
	public static final String OTP_RESPONSE_HTM = "/otp_response.htm";
	public static final String OTP_VERIFICATION = "/otpverification.htm";
	public static final String PAYMENT = "/payment";
	public static final String PAYU = "/payu";
	public static final String PROCESSED_PAYMENT = "/processedPayment";
	public static final String PAYMENT_RESPONSE ="/paymentResponse";
	public static final String COUPON ="/coupon";
	public static final String REDEEM_COUPON = "/redeem";
	public static final String INSERT_COUPON = "/insert";
	public static final String USER_LOGIN = "/login";
	public static final String USER_PROFILE = "/profile";
	public static final String CHARGERS_PROFILE = "chargers_profile";
	public static final String INDEX = "index";
	public static final String USER_LOGOUT = "/logout.htm";
	public static final String REVIEW_ORDER_URL = "/review-order";
	public static final String MOBILE = "/mobile";
	public static final String MOBILE_PLAN_URL = "/mobilePlan";
	public static final String FEEDBACK = "/feedback";
	public static final String FEEDBACK_HTM = "/feedback.htm";
	public static final String REVIEW_ORDER_HTM = "review-order.htm";
	public static final String RESPONSE = "response";
	public static final String RECHARGE = "recharge";
	public static final String PLAN_LIST = "planList";
	public static final String MOBILE_RECHARGE_DTO = "mobileRechargeDto";
	public static final String IS_REDEEM = "isRedeem";
	public static final String IS_USE_WALLET = "isUseWallet";
	public static final String IS_USE_REFERRAR = "isUseReferral";
	public static final String CONFIRM_ORDER = "confirm-order.htm";
	public static final String QUICK_RECHARGE_CONFIRM_ORDER = "quick-recharge-confirm-order.htm";
	public static final String MOBILE_DATACARD_DTH_CHARGE = "Mobile-Datacard-DTH";
	public static final String PAY_U = "PayU";
	public static final String ORDER_INFO = "orderInfo";
	public static final String PAYU_PAYMENT = "PayUpayment";
	public static final String COUPON_NAME = "couponName";
	public static final String SERVICE_TYPE = "serviceType";
	public static final String RECHARGE_FORM = "rechargeForm";
	public static final String NUMBER = "number";
	public static final String OPERATOR_VALUE = "operatorValue";
	public static final String OPERATOR_TEXT = "operatorText";
	public static final String SERVICE = "service";
	public static final String MOBILE_RECHARGE_SERVICE = "mobileRechargeService";
	public static final String AMOUNT = "amount";
	public static final String MOBILE_RECHARGE = "/mobilerecharge";
	public static final String RECHARGE_DTO = "rechargeDTO";
	public static final String URL = "url";
	public static final String REWARDS_POINTS = "rewardsPoints";
	public static final String REWARDS_POINTS_URL = "/rewardsPoints";
	public static final String REDEEM_POINTS = "/redeemPoints";
	public static final String CHANGE_PASSWORD = "/changePassword";
	public static final String GET_TRANSACTION_LIST = "/getTransactionList";
	public static final String ADD_MONEY = "/addMoney";
	public static final String WALLET = "Wallet";
	public static final String USER_WALLET = "/wallet";

	public static final String FORGOT_PASSWORD = "/forgotPassword";
	public static final String CIRCLE_VALUE = "circleValue";
	public static final String CIRCLE_NAME = "circleName";
	public static final String DTH = "DTH";
	public static final String RESET_PASSWORD_URL = "/resetPassword";
	public static final String RESET_PASSWORD = "resetPassword";
	public static final String SIGNIN = "/signIn";
	public static final String WAP_USER_LOGIN = "userlogin";
	public static final String QUICK_RECHARGE_CHECK_VAL =  "quickRechargeCheckVal";
	public static final String BILLING = "billing";
	public static final String LANDLINE_RECHARGE_DTO = "landlineRechargeDto";
	public static final String LANDLINE_NUMBER = "landlinenumber";
	public static final String STD_CODE = "stdcode";
	public static final String CUSTOMER_ACCOUNT_NO = "customeraccountno";
	public static final String BILLING_PAYU = "Billing";
	public static final String LANDLINE_BILL = "landlinebill";
	public static final String LANDLINE_FORM = "LandlineForm";
	public static final String BASIC_AMOUNT = "basicamount";
	public static final String EXTRA_AMOUNT = "extraamount";
	public static final String VERIFY_USERS = "/verifyUsers";
	public static final String VERIFY = "/verify/";
	public static final String FUNDS = "/funds";
	
	public interface AdminConstant{
		public static final String ADMIN = "/admin";
		public static final String USER_INFORMATION = "/userInformation";
		public static final String USER_INFORMATION_BYEMAIL = "/getUserInformationByEmail";
		public static final String USER_INFORMATION_BYUSERID = "/getUserInformationByUserId";
		public static final String USER_INFORMATION_BYMOBILE = "/getUserInformationByMobile";
		public static final String USER_TRANSACTION_HISTORY_BYUSERID = "/getUserTransactionHistoryByUserId";
		public static final String UPDATE_USER_BY_EMAIL_ADMIN = "/updateUserByEmailAdmin";
		public static final String UPDATE_USER_BY_USERID_ADMIN = "/updateUserByUserIdAdmin";
		public static final String MARGIN = "/margin";
	}
	
	public interface CAMWalletConstant{
		public static final String USER_WALLET = "/userwallet";
		public static final String LOGIN = "/login";
		public static final String USER_LOGIN = "/userlogin";
		public static final String USER_REGISTER_OTHER = "/userregisterother";
		public static final String CAM_WALLET_LOGIN = "cam-walletlogin";
		public static final String USER = "/user";
		public static final String REGISTER = "/register";
		public static final String AMOUNT = "amount";
		public static final String ADD_MONEY = "/addMoney";
		public static final String CAM_WALLET = "CAMWALLET";
		public static final String RECHARGE_DTO = "rechargeDto";
		public static final String STATUS = "status";
		public static final String CAM_WALLET_STATUS = "cam-wallet-status";
		public static final String RESPONSE = "response";
		public static final String OTP_RESPONSE = "/otpresponse";
		public static final String REVIEW_ORDER = "/review-order";
		public static final String CAM_WALLET_REVIEW_ORDER = "camwalletReviewOrder";
		
	}
	
	public interface MarchRequest {
		public static final String MARCH_AIR = "/march/air";
	}
	
	public interface Leagues{
		 // constants for urls 	
		public static final String CAM_CRICKET_LEAGUES = "/cam_cricket_leagues.htm";
		public static final String SHOW_CRICKET_QUESTIONS = "/show_cricket_questions.htm";
		public static final String SUBMIT_CORRECT_ANSWER = "/submit_correct_answer.htm";
		
		//Request Constants
		public static final String CRICKET_QUESTIONS = "cricketquestions";
		public static final String MESSAGE = "message";
		public static final String CRICKET_QADTO_LIST = "cricketQADtoList";
		public static final String CRICKET_LEAGUE_MAP = "cricketLeagueMap";
		
		//redirect pages
		public static final String CRICKET_LEAGUES = "cricketleagues";
		public static final String CRICKET_LEAGUES_QUESTIONS = "cricketleaguesQuestions";
		
		public static final String SUBMIT_CRICKET_LEAGUES_ANSWER = "/submit_leagues_answer.htm";
		public static final String CRICKET_LEAGUES_RESULT = "cricketleaguesresult";
		public static final String NAME = "name";
		public static final String PRICE = "price";
		public static final String MIN_USER = "min_user";
		public static final String MAX_USER = "max_user";
		public static final String MAX_USER_PRIZE_COUNT = "max_user_prize_count";
		public static final String CRICKET_LEAUGE_ID = "cricket_leauge_Id";
		public static final String LEAGUE_WINNING_NUMBER = "league_winning_number";
		public static final String LEAGUE_WINNING_AMOUNT = "league_winning_amount";
		public static final String ID = "id";
		public static final String BLANK = "";
		public static final String SLASH = "/";
		public static final String COMMA = ",";
		public static final String CRICKET_LEAGUE = "CricketLeague";
		public static final String PAY_U = "PayU";
		public static final String URL = "url";
		public static final String ORDER_INFO = "orderInfo";
		public static final String PROCESSED_PAYMENT = "/processedPayment";
		public static final String CRICKET_LEAGUES_RESULT_DTO = "cricketLeaguesResultDto";
		public static final int cricketLeague = 0;
		public static final String PAYMENT_RESPONSE = "paymentresponse";
		public static final String RESPONSE = "response";
		public static final String CRICKET_LEAGUE_PAYMENT_RESPONSE = "/paymentresponse.htm";
		public static final String LEAGUE_CRICKET = "league/cricket";
		public static final String FETCH_QUESTIONS = "/fetchquestions";
		public static final String CHECK_ANSWERS = "/checkanswers/{matchId}";
		public static final String WINNERS = "/winners/{matchId}";
		public static final String MATCH_NAME = "matchname";
		public static final Object SUCCESS = "success";
		public static final String STATUS = "status";
		public static final String TRUE = "true";
		public static final String FALSE = "false";
		
	}
}