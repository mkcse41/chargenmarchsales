package com.chargenmarch.common.constants;

public class ViewConstants {
	public static final String VIEW_INDEX = "index";
	public static final String USER_REGISTER = "userRegister";
	public static final String CHARGERS_PROFILE = "chargers_profile";
	public static final String CONTACTUS = "contactus";
	public static final String PRIVACY_POLICY = "privacy-policy";
	public static final String TNC = "tnc";
	public static final String USER_PROFILE = "userprofile";
	public static final String REVIEW_ORDER = "reviewOrder";
	public static final String RECHARGE_SUCCESS = "cam-recharge-success";
	public static final String RECHARGE_FAIURE = "cam-recharge-failure";
	public static final String PAY_U_LOWER = "payU";
	public static final String ADD_TYPES = "addtypes";
	public static final String MOBILE_PLAN = "mobilePlan";
	public static final String PAYU_PAYMENT = "PayUpayment";
	public static final String PAYMENT_RESPONSE = "paymentResponse";
	public static final String EXCEPTION = "exception";
	public static final String PAGE_NOT_FOUND = "page-not-found";
	public static final String AIR_SEARCH_RESULT = "air-search-result";
	public static final String AIR_TICKET_HOME = "air-ticket-home";
	public static final String CAM_SALES_LOGIN = "camsales-login";
	public static final String SLASH = "/";
	
	public interface AdminPanelViewConstanat{
		public static final String ADMIN = "admin/";
		public static final String ADMIN_PANEL = "adminpanel";
	}
}
