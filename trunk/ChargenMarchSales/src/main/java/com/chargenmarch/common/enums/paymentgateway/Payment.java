package com.chargenmarch.common.enums.paymentgateway;

/**
 * 
 * @author mukesh
 *
 */
public enum Payment {
       SUCCESS,
       PENDING,
       FAILURE,
       CANCEL
}
