package com.chargenmarch.common.cron;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.chargenmarch.charge.dao.mobilerecharge.IMobileRechargeDao;
import com.chargenmarch.common.dao.user.IUserDao;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.utility.IUtility;

@Service
//@EnableScheduling
public class ReportingCrons {

	private final static Logger logger = LoggerFactory.getLogger(ReportingCrons.class);

	@Autowired
	Environment env;

	@Autowired
	IEmailSenderService mailService;

	@Autowired
	IUserDao userDao;

	@Autowired
	IUtility util;

	@Autowired
	IMobileRechargeDao rechargeDao;

//	@Scheduled(cron = "0 55 23 1/1 * ?")
	/*public void dailyReportMailer() {
		List<UserDTO> userlist = userDao
				.getUserListByRegistrationDate(util.getCurrentDate(Constants.DateConstants.DEFAULT_DATE_FORMAT));
		List<RechargeDTO> rechargeList = rechargeDao
				.getRechargeListByDate(util.getCurrentDate(Constants.DateConstants.DEFAULT_DATE_FORMAT));
		int totalUserCount = 0, verifiedUsersCount = 0, unverifiedUserCount = 0, webUsers = 0, wapUsers = 0;
		int totalRecharge = 0, successfulRecharge = 0, unsuccessfulRecharge = 0;
		int totalMobileRecharge = 0, successfulMobileRecharge = 0, unsuccessfulMobileRecharge = 0;
		int totalDTHRecharge = 0, successfulDTHRecharge = 0, unsuccessfulDTHRecharge = 0;
		int totalDataCardRecharge = 0, successfulDataCardRecharge = 0, unsuccessfulDataCardRecharge = 0;
		int totalWalletRecharge = 0, successfulWalletRecharge = 0, unsuccessfulWalletRecharge = 0;

		totalUserCount = userlist.size();

		for (UserDTO user : userlist) {
			if (user.getIsverified() == UserVerificationStatus.VERIFIED.ordinal()) {
				++verifiedUsersCount;
			} else {
				++unverifiedUserCount;
			}

			if (user.getSource().equalsIgnoreCase(String.valueOf(Constants.WEB))) {
				++webUsers;
			} else if (user.getSource().equalsIgnoreCase(Constants.WAP)) {
				++wapUsers;
			}
		}

		totalRecharge = rechargeList.size();

		for (RechargeDTO recharge : rechargeList) {
			RechargeType type = RechargeType.valueOf(recharge.getServiceType());
			switch (type.ordinal()) {
			case 0:
				++totalMobileRecharge;
				break;
			case 1:
				++totalDataCardRecharge;
				break;
			case 2:
				++totalDTHRecharge;
				break;
			case 3:
				++totalWalletRecharge;
				break;
			default:
				break;
			}
			if (recharge.getIsSuccesfulRecharge() == 1) {
				++successfulRecharge;
				switch (type.ordinal()) {
				case 0:
					++successfulMobileRecharge;
					break;
				case 1:
					++successfulDataCardRecharge;
					break;
				case 2:
					++successfulDTHRecharge;
					break;
				case 3:
					++successfulWalletRecharge;
					break;
				default:
					break;
				}
			} else {
				++unsuccessfulRecharge;
				switch (type.ordinal()) {
				case 0:
					++unsuccessfulMobileRecharge;
					break;
				case 1:
					++unsuccessfulDataCardRecharge;
					break;
				case 2:
					++unsuccessfulDTHRecharge;
					break;
				case 3:
					++unsuccessfulWalletRecharge;
					break;
				default:
					break;
				}
			}
		}
		String message = util.classpathResourceLoader("dailyReportMailer.html");

		message = message.replace(MailerKeyConstants.DailyReportKey.TOTAL_REGISTRATION, String.valueOf(totalUserCount));
		message = message.replace(MailerKeyConstants.DailyReportKey.TOTAL_REGISTRATION_COMPUTER,
				String.valueOf(webUsers));
		message = message.replace(MailerKeyConstants.DailyReportKey.TOTAL_REGISTRATION_MOBILE,
				String.valueOf(wapUsers));
		message = message.replace(MailerKeyConstants.DailyReportKey.TOTAL_VERIFIED_USERS,
				String.valueOf(verifiedUsersCount));
		message = message.replace(MailerKeyConstants.DailyReportKey.TOTAL_UNVERIFIED_USERS,
				String.valueOf(unverifiedUserCount));

		message = message.replace(MailerKeyConstants.DailyReportKey.TOTAL_RECHARGE, String.valueOf(totalRecharge));
		message = message.replace(MailerKeyConstants.DailyReportKey.SUCCESSFUL_RECHARGE,
				String.valueOf(successfulRecharge));
		message = message.replace(MailerKeyConstants.DailyReportKey.UNSUCCESSFUL_RECHARGE,
				String.valueOf(unsuccessfulRecharge));

		message = message.replace(MailerKeyConstants.DailyReportKey.TOTAL_MOBILE_RECHARGE,
				String.valueOf(totalMobileRecharge));
		message = message.replace(MailerKeyConstants.DailyReportKey.SUCCESSFUL_MOBILE_RECHARGE,
				String.valueOf(successfulMobileRecharge));
		message = message.replace(MailerKeyConstants.DailyReportKey.UNSUCCESSFUL_MOBILE_RECHARGE,
				String.valueOf(unsuccessfulMobileRecharge));

		message = message.replace(MailerKeyConstants.DailyReportKey.TOTAL_DTH_RECHARGE,
				String.valueOf(totalDTHRecharge));
		message = message.replace(MailerKeyConstants.DailyReportKey.SUCCESSFUL_DTH_RECHARGE,
				String.valueOf(successfulDTHRecharge));
		message = message.replace(MailerKeyConstants.DailyReportKey.UNSUCCESSFUL_DTH_RECHARGE,
				String.valueOf(unsuccessfulDTHRecharge));

		message = message.replace(MailerKeyConstants.DailyReportKey.TOTAL_DATACARD_RECHARGE,
				String.valueOf(totalDataCardRecharge));
		message = message.replace(MailerKeyConstants.DailyReportKey.SUCCESSFUL_DATACARD_RECHARGE,
				String.valueOf(successfulDataCardRecharge));
		message = message.replace(MailerKeyConstants.DailyReportKey.UNSUCCESSFUL_DATACARD_RECHARGE,
				String.valueOf(unsuccessfulDataCardRecharge));

		message = message.replace(MailerKeyConstants.DailyReportKey.TOTAL_WALLET_RECHARGE,
				String.valueOf(totalWalletRecharge));
		message = message.replace(MailerKeyConstants.DailyReportKey.SUCCESSFUL_WALLET_RECHARGE,
				String.valueOf(successfulWalletRecharge));
		message = message.replace(MailerKeyConstants.DailyReportKey.UNSUCCESSFUL_WALLET_RECHARGE,
				String.valueOf(unsuccessfulWalletRecharge));
		EmailDTO email = new EmailDTO(
				"ChargenMarch Daily Report - " + util.getCurrentDate(Constants.DateConstants.DEFAULT_DATE_FORMAT),
				MailingConstants.CAM_GMAIL_ID.split(Constants.COMMA), Constants.NOREPLY_EMAIL_ID, null, null, message,
				null, null);
		mailService.sendEmail(email);
	}*/

}
