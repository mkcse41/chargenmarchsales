package com.chargenmarch.common.exceptionhandler;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.FilePathConstants;
import com.chargenmarch.common.constants.MailingConstants;
import com.chargenmarch.common.constants.ViewConstants;
import com.chargenmarch.common.service.email.IEmailSenderService;

@ControllerAdvice
@PropertySource(FilePathConstants.CLASSPATH+FilePathConstants.API_PROPERTIES)
public class ExceptionNotifier {
	
	@Autowired
	IEmailSenderService emailService;
	@Autowired
	Environment env;
	
	@ExceptionHandler(Exception.class)
	public ModelAndView mailException(HttpServletRequest request, Exception ex) throws Throwable{
		String expectionStack = ExceptionUtils.getStackTrace(ex);
		ex.printStackTrace();
		emailService.sendEmail(env.getProperty(MailingConstants.EXCEPTION_MAIL_ID), env.getProperty(MailingConstants.RECHARGE_MAILLIST).split(Constants.COMMA), "Exception on ChargenMarch", "Exception for URL : "+request.getRequestURL()+"\n"+expectionStack , null, null, false);
		/*throw ex;*/
		return new ModelAndView(ViewConstants.EXCEPTION);
	}
	
	@ExceptionHandler({NoHandlerFoundException.class, HttpRequestMethodNotSupportedException.class})
	public ModelAndView sendToErrorPage(){
		return new ModelAndView(ViewConstants.PAGE_NOT_FOUND);
	}
}
