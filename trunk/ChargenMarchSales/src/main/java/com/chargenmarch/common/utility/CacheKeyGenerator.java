package com.chargenmarch.common.utility;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.stereotype.Component;

@Component
public class CacheKeyGenerator implements KeyGenerator {

	private final static Logger logger = LoggerFactory.getLogger(CacheKeyGenerator.class);
	
	@Override
	public Object generate(Object target, Method method, Object... params) {
		String key = target.getClass().getName()+method.getName();
		for(Object object : params){
			key += object.toString();
		}
		return key;
	}

}
