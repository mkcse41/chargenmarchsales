package com.chargenmarch.common.dao.user;

import java.util.List;

import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.enums.user.UserVerificationStatus;

public interface IUserDao {
	public int addUser(UserDTO user);
	public List<UserDTO> getUserByMobile(String mobileNo);
	public List<UserDTO> getAllUsers();
	public List<UserDTO> getUserById(int id);
	public UserDTO getUserByEmailId(String emailId);
	public int updateUserRechargeAccount(UserDTO user);
	public int updateUserWalletBalance(UserDTO user);
	public int updateUserPassword(UserDTO user);
	public int updateUserVerified(UserDTO user);
	public List<UserDTO> getUserByVerificationStatus(UserVerificationStatus verifiedStatus);
	public List<UserDTO> getUserListByRegistrationDate(String date);
	public int insertReferralLogs(UserDTO user,UserDTO referralUserDTO,int cashbackAmount);
	public int updateRefferalCodeById(UserDTO user,String referralCode);
	public List getReferralLogs(UserDTO user);
	public int insertEmailLogs(UserDTO user);
	public int truncateEmailLogs();
	public int getReferralAmountByChargersId(UserDTO user);
	public int updateUserRefferalBalance(UserDTO user);
	public int getReferralRechargeAmountByChargersId(UserDTO user);
	public List<UserDTO> getChargersDetailsInfo();
	public int getPaymentInfoByEmailId(UserDTO user);
	public List<UserDTO> getChargersDetailsWalletBalInfo();
	public UserDTO getReferrerByUser(UserDTO user);
	public int saveOrUpdateBrowserNotificationLogs(UserDTO user,String browserId);
	public List getBrowserNotificationLogs();
	public List<UserDTO> getCAMSalesUserList();
}
