package com.chargenmarch.common.service.user.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.FilePathConstants;
import com.chargenmarch.common.constants.PropertyKeyConstants;
import com.chargenmarch.common.constants.RequestConstants;
import com.chargenmarch.common.dao.user.IUserDao;
import com.chargenmarch.common.dto.sms.SMSDTO;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.enums.sms.SMSTemplate;
import com.chargenmarch.common.enums.user.UserVerificationStatus;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.service.sms.ISMSService;
import com.chargenmarch.common.service.user.IUserService;
import com.chargenmarch.common.thread.UserConfirmationMailSenderThread;
import com.chargenmarch.common.utility.IUtility;

@Service
@PropertySource(FilePathConstants.CLASSPATH+FilePathConstants.API_PROPERTIES)
public class UserServiceImpl implements IUserService{
	
	private final static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	IUserDao userDao;
	
	private Map<Integer, UserDTO> userInfoMapById;
	private Map<String, UserDTO> userInfoMapByEmailId;
	private Map<String, UserDTO> userInfoMapByMobileNo;
	private Map<String, UserDTO> userInfoMapByReferralCode;
	private Map<Integer, UserDTO> sendMailUserInfoMapById;
	private Map<Integer, String> browserNotificationDataMapById;

	@Autowired
	private IEmailSenderService emailSenderService;
	@Autowired
	private ISMSService smsSenderService;
	
	@Autowired
	private IUtility utility;
	
	@Autowired
	UserConfirmationMailSenderThread userConfirmationMailSenderThread;
	
	@Autowired
	ThreadPoolTaskExecutor taskExecutor;
	
	@Autowired
	private WebApplicationContext context;
	
	@Autowired
	private Environment env;
	
	@PostConstruct
	public void init(){
		logger.info("User Service initialization");
		userInfoMapById = new HashMap<Integer, UserDTO>();
		sendMailUserInfoMapById = new HashMap<Integer, UserDTO>();
		userInfoMapByEmailId = new HashMap<String, UserDTO>();
		userInfoMapByMobileNo = new HashMap<String, UserDTO>();
		userInfoMapByReferralCode = new HashMap<String, UserDTO>();
		browserNotificationDataMapById = new HashMap<Integer, String>();
		loadBrowserNotificationData();
		loadUserInfo();
	}
	
	private void loadUserInfo(){
		List<UserDTO> allUserList = userDao.getAllUsers();
		Iterator<UserDTO> userListItr = allUserList.iterator();
		while(userListItr.hasNext()){
			UserDTO user = userListItr.next();
			user.setBrowserNotificationId(browserNotificationDataMapById.get(user.getId()));
			userInfoMapById.put(user.getId(), user);
			sendMailUserInfoMapById.put(user.getId(), user);
			userInfoMapByEmailId.put(user.getEmailId(), user);
			userInfoMapByMobileNo.put(user.getMobileNo(), user);
			userInfoMapByReferralCode.put(user.getReferralCode(), user);
		}
	}
	
	public Map<Integer, UserDTO> getUserInfoMapById() {
		return userInfoMapById;
	}
	
	private void loadBrowserNotificationData(){
		List browserNotificationLogs = userDao.getBrowserNotificationLogs();
		if(null != browserNotificationLogs && !browserNotificationLogs.isEmpty()){
			for (Object object : browserNotificationLogs) {
				 Map notificationMap = (Map) object;
				 int userId = (Integer) notificationMap.get("charger_id");
				 String browserNotificationId = (String) notificationMap.get("browser_notification_id");
				 browserNotificationDataMapById.put(userId, browserNotificationId);
			}
		}
	}
	
	public Map<Integer, UserDTO> sendMailUserInfoMapById() {
		Map<Integer, UserDTO> userInfoMap = sendMailUserInfoMapById;
		return userInfoMap;
	}

	public Map<String, UserDTO> getUserInfoMapByEmailId() {
		return userInfoMapByEmailId;
	}
	
	public Map<String, UserDTO> getUserInfoMapByReferralCode() {
		return userInfoMapByReferralCode;
	}
	
	public int addUser(UserDTO user){
		try{
			if(!doesUserExist(user)){
				if(Constants.BLANK == user.getPassword()){
					//set ramdom password.
					user.setPassword(randomGeneratePassword());
				}
				user.setReferralCode(randomGenerateReferralCode());
				int status = userDao.addUser(user);
				if(status==1){
					updateUserCacheMaps(user);
				}
				return status;
			}else{
				return Constants.THREE;
			}
		} catch(Exception e){
			logger.error("Error in inserting User Details" + e);
		}
		return 0;
	}
	
	public int checkUser(UserDTO user){
		try{
			if(!doesUserExist(user)){
				/*boolean autoGeneratedPass = false;*/
				if(Constants.BLANK == user.getPassword()){
					//set ramdom password.
					user.setPassword(randomGeneratePassword());
					/*autoGeneratedPass = true;*/
				}
				return Constants.ONE;
			}
			else{
				return Constants.THREE;
			}
		} catch(Exception e){
			logger.error("Error in inserting User Details" + e);
		}
		return 0;
	}
	
	public void sendWelcomeMail(UserDTO user){
		String subject = "Welcome to Chargenmarch";
		String message = utility.classpathResourceLoader("register.html");
		message = message.replace("%NAME%", user.getName()).replace("%EMAILID%", user.getEmailId()).replace("%PASSWORD%", user.getPassword()).replace("%REFERRALCODE%", user.getReferralCode());
		emailSenderService.sendEmail(Constants.NOREPLY_EMAIL_ID, user.getEmailId().split(Constants.COMMA), subject, message, null, null, true);
	}
	
	public UserDTO getUserByMobile(String mobileNo){
		List<UserDTO> userList = userDao.getUserByMobile(mobileNo);
		if(userList!=null && userList.size()>0){
			return (UserDTO) userList.get(0);
		}
		return null;
	}
	
	public UserDTO getUserByEmailId(String emailId){
		if(userInfoMapByEmailId.containsKey(emailId)){
			return userInfoMapByEmailId.get(emailId);
		}
		return null;
	}
	
	public UserDTO getUserByReferralCode(String referralCode){
		if(userInfoMapByReferralCode.containsKey(referralCode)){
			return userInfoMapByReferralCode.get(referralCode);
		}
		return null;
	}
	
	
	public UserDTO getUserByMobileNo(String mobileNo){
		if(userInfoMapByMobileNo.containsKey(mobileNo)){
			return userInfoMapByMobileNo.get(mobileNo);
		}
		return null;
	}
	
	public UserDTO getUserById(int id){
		if(userInfoMapById.containsKey(id)){
			return userInfoMapById.get(id);
		}else{
			List<UserDTO> userList = userDao.getUserById(id);
			if(userList!=null && userList.size()>0){
				return (UserDTO) userList.get(0);
			}
	    }
		return null;
	}
	
	public boolean doesUserExist(UserDTO user){
		
		UserDTO userDto = getUserInfoMapByEmailId().get(user.getEmailId());
		if(userDto==null){
			userDto = getUserByMobileNo(user.getMobileNo());
		}
		if(null != userDto && userDto.getIsverified() == 1 /*&& userDto.getMobileNo().equals(user.getMobileNo())*/ ){
			return true;
		}
		else{
			return false;
		}
		/*return getUserInfoMapByEmailId().containsKey(user.getEmailId());*/
	}
	
	private String randomGeneratePassword(){
		String password = Constants.BLANK;
		String uuid = UUID.randomUUID().toString();
		if(null!=uuid && uuid.contains(Constants.HYFUN)){
			String passwordArr[] = uuid.split(Constants.HYFUN);
			if(passwordArr.length>0){
				password = passwordArr[0];
			}
		}
		return password;
	}
	
	/**
	 * Generate referral code for user register.
	 * 
	 * @return
	 */
	private String randomGenerateReferralCode(){
		String referralCode = Constants.BLANK;
		String uuid = UUID.randomUUID().toString();
		if(null!=uuid && uuid.contains(Constants.HYFUN)){
			String refferalArr[] = uuid.split(Constants.HYFUN);
			if(refferalArr.length>2){
				referralCode = refferalArr[1].toUpperCase() + refferalArr[2].toUpperCase();
			}else if(refferalArr.length>0){
				referralCode = refferalArr[0].toUpperCase();
			}
		}
		return referralCode;
	}
	
	public int updateUserRechargeAccount(UserDTO user){
		int rowCount =  userDao.updateUserRechargeAccount(user);
		if(rowCount==1){
			updateUserCacheMaps(user);
		}
		return rowCount;
	}
	
	public int updateUserWalletBalance(UserDTO user){
		int rowCount = userDao.updateUserWalletBalance(user);
		if(rowCount==1){
			updateUserCacheMaps(user);
		}
		return rowCount;
	}
	
	public int updateUserPassword(UserDTO user){
		int rowCount = userDao.updateUserPassword(user);
		if(rowCount==1){
			updateUserCacheMaps(user);
		}
		return rowCount;
	}
	
	public int updateUserVerifiedStatus(UserDTO user, UserVerificationStatus status){
		user.setIsverified(status.ordinal());
		int rowCount = userDao.updateUserVerified(user);
		if(rowCount == 1){
			updateUserCacheMaps(user);
			// send welcome mail to user.
			if(user.getIsverified() == UserVerificationStatus.VERIFIED.ordinal()) {
				userConfirmationMailSenderThread.setUser(user);
				taskExecutor.execute(userConfirmationMailSenderThread);
			}
			//sendWelcomeMail(user);
		}
		return rowCount;
	}
	
	public void resetPassword(String email, String password){
		UserDTO user = getUserInfoMapByEmailId().get(email);
		user.setPassword(password);
		userDao.updateUserPassword(user);
		updateUserCacheMaps(user);
	}
	
	public void updateUserCacheMaps(UserDTO user){
		UserDTO userFromDB = userDao.getUserByEmailId(user.getEmailId());
		userInfoMapByEmailId.put(user.getEmailId(), userFromDB);
		sendMailUserInfoMapById.put(userFromDB.getId(), userFromDB);
		userInfoMapById.put(userFromDB.getId(), userFromDB);
		userInfoMapByMobileNo.put(userFromDB.getMobileNo(),userFromDB);
		userInfoMapByReferralCode.put(userFromDB.getReferralCode(), userFromDB);
	}
	
	public int sendOneTimePassword(UserDTO user){
		SMSDTO smsDto = smsSenderService.getSMSObjectByUserAndSMSTemplate(user, SMSTemplate.MOBILE_VERFICATION);
		smsDto.getParameterMap().put(1, String.valueOf(user.getOtp()));
		return smsSenderService.sendSMS(smsDto);
	}
	
	public List<UserDTO> getUserByVerificationStatus(UserVerificationStatus verifiedStatus){
		return userDao.getUserByVerificationStatus(verifiedStatus);
	}
	
	public String generateVerificationLink(UserDTO user){
		return context.getBean("baseUrl", String.class)+RequestConstants.USER+RequestConstants.VERIFY+utility.base64Encoder(user.getEmailId());
	}
	
	/**
	 * Get referral cashback amount 
	 * @return
	 */
	public int getReferralCashbackAmount(){
		int referralCashbackAmount = 0;
		String referralCashBack = env.getProperty(PropertyKeyConstants.APIProperties.USER_REFERRAL_CASHBACK);
		if(null != referralCashBack && !referralCashBack.equals(Constants.BLANK)){
			referralCashbackAmount = utility.convertStringToInt(referralCashBack);
		}
		return referralCashbackAmount;
	}
	
	public int insertReferralLogs(UserDTO user,UserDTO referralUserDTO,int cashbackAmount){
		 return userDao.insertReferralLogs(user, referralUserDTO, cashbackAmount);
	}
	
	public int updateRefferalCodeById(UserDTO user,String referralCode){
		 return userDao.updateRefferalCodeById(user, referralCode);
	}
	
	/**
	 * This method used to 
	 * 1. Generate User referral Code.
	 * 2. set and update user referral code by chargersid
	 * 3. update user cache map.
	 */
	public void updateUserReferralCode(UserDTO user){
		 String referralCode = randomGenerateReferralCode();
		 user.setReferralCode(randomGenerateReferralCode());
		 updateRefferalCodeById(user, referralCode);
		 updateUserCacheMaps(user);
	}
	
	public void sendCAMReferralMail(UserDTO user){
		/*String subject = "Just Share Your Referral Code and Earn a Cashback of 5%";
		String message = utility.classpathResourceLoader("referral.html");*/
		String subject = "ChargenMarch - 5% Cashback on Mobile and DTH Recharge";
		String message = utility.classpathResourceLoader("notification.html");
		message = message.replace("%NAME%", user.getName()).replace("%EMAILID%", user.getEmailId()).replace("%MOBILENO%", user.getMobileNo()).replace("%REFERRALCODE%", user.getReferralCode());
		emailSenderService.sendEmail(Constants.NOREPLY_EMAIL_ID, user.getEmailId().split(Constants.COMMA), subject, message, null, null, true);
	}
	
	public List getReferralLogs(UserDTO user){
		return userDao.getReferralLogs(user);
	}
	
	public int insertEmailLogs(UserDTO user){
		return userDao.insertEmailLogs(user);
	}
	
	public int truncateEmailLogs(){
		return userDao.truncateEmailLogs();
	}
	
	public int getReferralAmountByChargersId(UserDTO user){
		return userDao.getReferralAmountByChargersId(user);
	}
	
	public int updateUserRefferalBalance(UserDTO user){
		return userDao.updateUserRefferalBalance(user);
	}
	
	public int getReferralRechargeAmountByChargersId(UserDTO user){
		return userDao.getReferralRechargeAmountByChargersId(user);
	}

	
	public void updateUserReferralAmount(){
		/*List<UserDTO> userWalletDetails = userDao.getChargersDetailsInfo();
		for (UserDTO user : userWalletDetails) {
			 int totalWalletBalance = user.getWalletBalance();
			 int referralBalance = getReferralAmountByChargersId(user);
			 if(totalWalletBalance >= referralBalance){
				 int walletBal = totalWalletBalance - referralBalance;
				 user.setWalletBalance(walletBal);
				 int refRechargeAmount = getReferralRechargeAmountByChargersId(user);
				 if(referralBalance >= refRechargeAmount){
					 referralBalance = referralBalance - refRechargeAmount;
				 }else{
					 referralBalance = 0;
				 }
				 user.setReferralBalance(referralBalance);
				 updateUserWalletBalance(user);
				 updateUserRefferalBalance(user);
			}else{
				 int refRechargeAmount = getReferralRechargeAmountByChargersId(user);
				 if(referralBalance >= refRechargeAmount){
					 referralBalance = referralBalance - refRechargeAmount;
				 }else{
					 referralBalance = 0;
				 }
				 user.setReferralBalance(referralBalance);
				 updateUserRefferalBalance(user);
			}
			updateUserCacheMaps(user);
		}*/
		
		List<UserDTO> userDetails = userDao.getChargersDetailsInfo();
		for (UserDTO userDTO : userDetails) {
			int paymentAmount = userDao.getPaymentInfoByEmailId(userDTO);
			logger.info("Emailid ==>"+userDTO.getEmailId() + " => Wal Bal => "+userDTO.getWalletBalance() + " ==> ref bal ==> "+userDTO.getReferralBalance() + "=> Pay amt => "+paymentAmount);
			userDTO.setWalletBalance(paymentAmount);
			userDao.updateUserWalletBalance(userDTO);
		}
	}

	public UserDTO getReferrerByUser(UserDTO user) {
		UserDTO referrer = userDao.getReferrerByUser(user);
		return referrer;
	}
	
	public int saveOrUpdateBrowserNotificationLogs(UserDTO user,String browserId){
		return userDao.saveOrUpdateBrowserNotificationLogs(user, browserId);
	}
	
    public int calculateWalletLoadMoney(int amount){
    	int extraChargesPercent = utility.convertStringToInt(env.getProperty(PropertyKeyConstants.APIProperties.CAM_SALES_WALLET_EXTRA_CHARGES));
    	if(amount >= 30){
    		int percentAmount = Math.round((amount * extraChargesPercent)/100);
    		return (amount + percentAmount);
    	}
    	return amount;
    }
    
    public List<UserDTO> getCAMSalesUserList(){
    	return userDao.getCAMSalesUserList();
    }
}
