package com.chargenmarch.common.service.sms.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.FilePathConstants;
import com.chargenmarch.common.constants.PropertyKeyConstants;
import com.chargenmarch.common.dao.sms.ISMSDao;
import com.chargenmarch.common.dto.sms.SMSDTO;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.enums.sms.SMSStatusCodes;
import com.chargenmarch.common.enums.sms.SMSTemplate;
import com.chargenmarch.common.service.email.IEmailSenderService;
import com.chargenmarch.common.service.http.IHTTPService;
import com.chargenmarch.common.service.sms.ISMSService;
import com.chargenmarch.common.utility.IUtility;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Service
@PropertySource(FilePathConstants.CLASSPATH+FilePathConstants.SMS_PROPERTIES)
public class SMSServiceImpl implements ISMSService{
	
	private final static Logger logger = LoggerFactory.getLogger(SMSServiceImpl.class);
	
	@Autowired
	private Environment env;

	@Autowired
	private ISMSDao smsDao;

	@Autowired
	private IHTTPService httpService;
	
	@Autowired
	private IUtility utility;
	
	@Autowired
	private IEmailSenderService emailSenderService;
	
	public SMSDTO getSMSObjectByUserAndSMSTemplate(UserDTO user, SMSTemplate templateName){
		String smsGateway = getSMSGateway();
		return new SMSDTO(user,smsGateway,templateName, SMSStatusCodes.LOGGED);
		
	}
	
	public int saveSMStoLog(SMSDTO sms) throws Throwable{
		return smsDao.saveSMStoLog(sms);
	}
	
	/**
	 * Request Url : http://websms.way2mint.com/index.php/web_service/sendSMS?username=testsmst&password=mint2014&destination=9602646089
	 * &template_name=Cab%20Booking&templateParameters[A]=123abc&templateParameters[B]=Mukesh&templateParameters[c]=9782091210
	 * &response_format=json&sender_id=KHPNIL
     * Response : Array ( [status] => 1 [id] => 1254699 [sender_name] => KHPNIL ) {"content":{"message":"Message Successfully Sent","campaign_id":1254699},"response_code":401,"response_msg":"Request processed Successfully","timestamp":1451061910}
	 * @param sms
	 */
	public int sendSMS(SMSDTO sms){
		try{
			if(null!=sms && null!=sms.getUser() ){
				String smsServiceName = env.getProperty(PropertyKeyConstants.SMSProperties.SMS_SERVICE_NAME);
				if(!StringUtils.isEmpty(smsServiceName) && smsServiceName.equalsIgnoreCase(Constants.TEXT_LOCAL)){
					    return sendSMSThroughTextLocal(sms);
				}else{
						String smsTemplate = getSMSTemplate(sms.getTemplate(), sms);
						sms.setMessage(smsTemplate);
					    String url = createSendSMSUrl(sms);
					    int id = saveSMStoLog(sms);
					    String response = httpService.HTTPGetRequest(url);
					    sms.setId(id);
					    sms.setResponse(Constants.BLANK);
					    if(response!=null && !StringUtils.isEmpty(response)){
						      int index = response.indexOf(Constants.CLOSE_BRACES);
						      String responseString  = response.length()>index?response.substring(index+1).trim():response;
						      sms.setResponse(responseString);
						      Gson gson = new Gson();
						      JsonElement element = gson.fromJson (responseString, JsonElement.class);
						      JsonObject jsonObj = element.getAsJsonObject();
						      String responseCode = jsonObj.get(Constants.RESPONSE_CODE).toString();
						      if(null!=responseCode && responseCode.equals(Constants.SMS_SUCCESS_CODE)){
						    	  updateSMSStatus(sms, SMSStatusCodes.SUCCESSFUL);
						    	  return 1;
						      }else{
						          updateSMSStatus(sms, SMSStatusCodes.FAILED);
						      }
						}
					    updateSMSStatus(sms, SMSStatusCodes.SENDING_INITIATED);
					}
			   }
		  }catch(Exception e){
			  e.printStackTrace();
		  } catch (Throwable e) {
			  e.printStackTrace();
		}
        return 0;
	}
	
	
	public int updateSMSStatus(SMSDTO sms, SMSStatusCodes status){
		return smsDao.updateSMSStatus(sms, status);
	}	
	
	private String getSMSGateway(){
		return env.getProperty(PropertyKeyConstants.SMSProperties.SMS_GATEWAY_NAME);
	}
	
	private String createSendSMSUrl(SMSDTO sms){
		String url = env.getProperty(PropertyKeyConstants.SMSProperties.SMS_GATEWAY_URL);
		String templateName = sms.getTemplate().getTemplateParameter().split(Constants.HASH)[0];
		int parameterLength = utility.convertStringToInt(sms.getTemplate().getTemplateParameter().split(Constants.HASH)[1]);
		url +=  Constants.USERNAME + Constants.EQUALS + env.getProperty(PropertyKeyConstants.SMSProperties.SMS_GATEWAY_USERNAME) + Constants.AMPERSAND + 
                Constants.PASSWORD + Constants.EQUALS + env.getProperty(PropertyKeyConstants.SMSProperties.SMS_GATEWAY_PASSWORD) + Constants.AMPERSAND +  
                Constants.DESTINATION + Constants.EQUALS + sms.getUser().getMobileNo() + Constants.AMPERSAND + 
                Constants.TEMPLATE_NAME + Constants.EQUALS + templateName.replace(Constants.SPACE, Constants.SPACE_ENCODED) + Constants.AMPERSAND + 
                Constants.RESPONSE_FORMAT + Constants.EQUALS + Constants.JSON + Constants.AMPERSAND + 
                Constants.SENDER_ID + Constants.EQUALS + env.getProperty(PropertyKeyConstants.SMSProperties.SMS_SENDER_NAME); 
       url += createTemplateParameter(parameterLength, sms);
       return url;
	}
	
	private String getSMSTemplate(SMSTemplate smsTemplate,SMSDTO sms){
		String messageTemplate = null;
		if(smsTemplate.equals(smsTemplate.MOBILE_VERFICATION)){
		   messageTemplate = env.getProperty(PropertyKeyConstants.SMSProperties.OTP_SMS_TEMPLATE);
		} else if(smsTemplate.equals(smsTemplate.RECHARGE)){
		   messageTemplate = env.getProperty(PropertyKeyConstants.SMSProperties.SUCCESS_SMS_TEMPLATE);
		}
		return messageTemplate;
		}
	
  private String createTemplateParameter(int parameterLength,SMSDTO sms){
	  String templateParameter = Constants.BLANK;
	  for (int count = 1; count <= parameterLength; count++) {
		  char charValue = (char)(count+64);
		  templateParameter +=  Constants.AMPERSAND +  Constants.TEMPLATE_PARAMETERS+Constants.OPEN_BRACKETS + String.valueOf(charValue)  +  Constants.END_BRACKETS + Constants.EQUALS + sms.getParameterMap().get(count);
		  sms.setMessage(sms.getMessage().replaceAll("%PARAMETER"+count+"%", sms.getParameterMap().get(count)));
	  }
	return templateParameter;
  }
  
  public void checkSMSBalance(){
	  try{
	         String checkSMSBalanceUrl = env.getProperty(PropertyKeyConstants.SMSProperties.CHECK_SMS_BALANCE_URL);
			 checkSMSBalanceUrl +=  Constants.USERNAME + Constants.EQUALS + env.getProperty(PropertyKeyConstants.SMSProperties.SMS_GATEWAY_USERNAME) + Constants.AMPERSAND + 
             Constants.PASSWORD + Constants.EQUALS + env.getProperty(PropertyKeyConstants.SMSProperties.SMS_GATEWAY_PASSWORD) + Constants.AMPERSAND +  
             Constants.RESPONSE_FORMAT + Constants.EQUALS + Constants.JSON;
			 String response = httpService.HTTPGetRequest(checkSMSBalanceUrl);
			 int checkSMSBalanceCount = utility.convertStringToInt(env.getProperty(PropertyKeyConstants.SMSProperties.CHECK_SMS_BALANCE_COUNT));
			 if(response!=null && !StringUtils.isEmpty(response)){
			      Gson gson = new Gson();
			      JsonElement element = gson.fromJson (response, JsonElement.class);
			      JsonObject jsonObj = element.getAsJsonObject();
			      String responseCode = jsonObj.get(Constants.RESPONSE_CODE).toString();
			      if(null!=responseCode && responseCode.equals(Constants.SMS_SUCCESS_CODE)){
			    	  JsonObject jsonContent = jsonObj.getAsJsonObject(Constants.CONTENT);
			    	  int transactionCount = utility.convertStringToInt(jsonContent.get(Constants.TRANSACTIONAL_SMS).toString());
			    	  if(transactionCount<=checkSMSBalanceCount){
			    		  emailSenderService.sendEmail(env.getProperty(PropertyKeyConstants.SMSProperties.NO_REPLY_MAIL_ID), env.getProperty(PropertyKeyConstants.SMSProperties.CHECK_SMS_BALANCE_EMAIL).split(Constants.COMMA), "Transaction SMS Balance Low", "Transaction SMS Balance="+checkSMSBalanceCount, null, null, false);
			    	  }
			      }
			}
	   }catch(Exception e){
		   e.printStackTrace();
	   }
  }
  
  public int sendSMSThroughTextLocal(SMSDTO sms){
		try{
			if(null!=sms && null!=sms.getUser() ){
				String smsTemplate = getSMSTemplate(sms.getTemplate(), sms);
				sms.setMessage(smsTemplate);
				sms.setGateway(Constants.TEXT_LOCAL);
				int parameterLength = utility.convertStringToInt(sms.getTemplate().getTemplateParameter().split(Constants.HASH)[1]);
				createTemplateParameter(parameterLength, sms);
			    String url = env.getProperty(PropertyKeyConstants.SMSProperties.SMS_TEXT_LOCAL_GATEWAY_URL);
			    url = url.replace("%NUMBER%", sms.getUser().getMobileNo());
			    Map paramMap = new HashMap();
			    paramMap.put("message", sms.getMessage());
			    logger.info("SMS Textlocal URL==>"+url);
			    int id = saveSMStoLog(sms);
			    String response = httpService.HTTPPostRequest(url, paramMap);
			    sms.setId(id);
			    sms.setResponse(Constants.BLANK);
			    logger.info("SMS Textlocal Response==>"+response);
			    if(response!=null && !StringUtils.isEmpty(response)){
				      sms.setResponse(response);
				      Gson gson = new Gson();
				      JsonElement element = gson.fromJson (response, JsonElement.class);
				      JsonObject jsonObj = element.getAsJsonObject();
				      String responseCode = jsonObj.get(Constants.STATUS.toLowerCase()).toString();
				      if(null!=responseCode && responseCode.contains(Constants.SUCCESS.toLowerCase())){
				    	  updateSMSStatus(sms, SMSStatusCodes.SUCCESSFUL);
				    	  return 1;
				      }else{
				          updateSMSStatus(sms, SMSStatusCodes.FAILED);
				          return 0;
				      }
				}
			    updateSMSStatus(sms, SMSStatusCodes.SENDING_INITIATED);
			}
		  }catch(Exception e){
			  e.printStackTrace();
		  } catch (Throwable e) {
			  e.printStackTrace();
		}
		return 0;
	}
	 
}
