package com.chargenmarch.common.dao.user;

import java.util.List;

import com.chargenmarch.common.dto.user.PaymentLogDto;

/**
 * @author gambit
 * Dao Layer to handle User Activity tracking across the website
 * 
 */
public interface IPaymentLogDao {
	
	/**
	 * Logs every step of user activity according to the DTO
	 * @param dto
	 * @return int
	 */
	public int logPaymentActivity(PaymentLogDto dto);
	public int updatePaymentLog(PaymentLogDto dto);
	public List<PaymentLogDto> getPaymentLogById(int id);
}
