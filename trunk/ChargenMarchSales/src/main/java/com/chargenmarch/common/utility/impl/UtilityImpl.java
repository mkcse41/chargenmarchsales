package com.chargenmarch.common.utility.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.FilePathConstants;
import com.chargenmarch.common.constants.PropertyKeyConstants;
import com.chargenmarch.common.constants.paymentgateway.PaymentConstants;
import com.chargenmarch.common.dto.paymentgateway.OrderInfo;
import com.chargenmarch.common.dto.paymentgateway.PaymentGatewayInfo;
import com.chargenmarch.common.dto.paymentgateway.PaymentValidateDTO;
import com.chargenmarch.common.utility.IUtility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Service
@PropertySource(FilePathConstants.CLASSPATH+FilePathConstants.SERVER_PROPERTIES)
public class UtilityImpl implements IUtility {
	private final static Logger logger = LoggerFactory.getLogger(UtilityImpl.class);
	
	@Autowired
	private String baseUrl;
	
	@Autowired
	private ResourceLoader resourceLoader;
	
	private Gson gson;
	
	@Autowired
	private Environment env;
	
	/**
	 * This method returns blank string if parameter value contains null,
	 * otherwise, it will return same string.
	 * @param str
	 * @return
	 */
	public String stringValueHandler(String str) {
		if (str == null){
			str = Constants.BLANK;
		}
		return str;
	}
	
	/**
	 * This method is used to remove non numeric value from string and convert it into integer & Shoot Mail if not possible
	 */
	public int convertStringToInt(String str) {
		if(str!=null && !str.equals(null)) {
			String strTemp = str;
			strTemp = strTemp.replaceAll(Constants.CHAR_SET_ALL_NON_INT, Constants.BLANK);
			if(strTemp.length() > Constants.ZERO) {
				int intValue = Constants.ZERO;
				try{
					intValue = Integer.parseInt(strTemp);
				}catch(NumberFormatException ex){
					logger.error("NumberFormatException for String "+strTemp);
				}
				return intValue;

			}else {
				if (str!=null && !str.equals(null)) {
					try {
						throw new Exception();
					} catch(Exception e) {
						logger.error("Error in parsing string to int : "+str);
					}
				}
			}
		}
		return Constants.ZERO;
	}
	
	public String getBaseUrl(){
		return baseUrl!=null ? baseUrl : Constants.SITE_URL;
	}
	
	public JsonObject parseJSON(String jsonLine) {
	    JsonElement jelement = new JsonParser().parse(jsonLine);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    return jobject;
	}
	
	//firstName=mukesh&lastName=swami&emailId=mkcse41@gmail.com&mobileNo=9602646089
	//&pincode=302013&city=jaipur&state=rajasthan&address=jaipur&productName=rsa&amount=200&pgType=PayU1&userCredentials=emil@gmail.com1
	public OrderInfo initializeOrderInfoForPaymentGateway(String firstName,String lastName,String emailId,String mobileNo,
			String pinCode,String city,String state,String address,String productName,int amount,String pgType,String userCredentials){
		OrderInfo orderInfo = new OrderInfo();
		orderInfo.setFirstName(firstName);
		orderInfo.setLastName(lastName);
		orderInfo.setEmailId(emailId);
		orderInfo.setMobileno(mobileNo);
		orderInfo.setPinCode(pinCode);
		orderInfo.setCity(city);
		orderInfo.setState(state);
		orderInfo.setAddress(address);
		orderInfo.setProductName(productName);
		orderInfo.setAmount(amount);
		orderInfo.setPgType(pgType);
		orderInfo.setPinCode("302012");
		orderInfo.setUserCredentials(userCredentials);
		return orderInfo;
	}
	
	public String classpathResourceLoader(String fileName){
		try{
			StringBuffer data = new StringBuffer();
			Resource resource = resourceLoader.getResource(Constants.CLASSPATH+Constants.COLON+fileName);
			InputStream is = resource.getInputStream();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			while (true) {
	            String line = reader.readLine();
	            if (line == null)
	                break;
	            data.append(line);
	        }
			return data.toString();
		} catch(Exception e){
			logger.error("Error in loading Classpath Resource");
		}
		return null;
	}
	
	public String base64Encoder(String rawText){
		return Base64Utils.encodeToString(rawText.getBytes());
	}
	public String base64Decoder(String cipher){
		byte[] decodedByteArr = Base64Utils.decodeFromString(cipher);
		return new String(decodedByteArr);
	}
	public int generateRandomNumber(){
		Random rnd = new Random();
		int number = 100000 + rnd.nextInt(900000);
		return number;
	}

	public String getviewResolverName(HttpServletRequest request, String jspName) {
		 String isMobile = (String)request.getSession().getAttribute(Constants.IS_MOBILE);
		 if(null != isMobile && isMobile.equalsIgnoreCase(Constants.STR_TRUE)){
			 return Constants.MOBILE_JSP_FOLDER+Constants.BACK_SLASH+Constants.WAP+Constants.HYFUN+jspName;
		 }
		return jspName;
	}
	
	
	/**
	 * current time between two times
	 * @param initialTime
	 * @param finalTime
	 * @param currentTime
	 * @return
	 * @throws ParseException
	 */
	public  boolean isTimeBetweenTwoTime(String initialTime, String finalTime, String currentTime) throws ParseException {
        String reg = "^([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$";
        if (initialTime.matches(reg) && finalTime.matches(reg) && currentTime.matches(reg)) {
            boolean valid = false;
            //Start Time
            java.util.Date inTime = new SimpleDateFormat("HH:mm:ss").parse(initialTime);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(inTime);

            //Current Time
            java.util.Date checkTime = new SimpleDateFormat("HH:mm:ss").parse(currentTime);
            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(checkTime);

            //End Time
            java.util.Date finTime = new SimpleDateFormat("HH:mm:ss").parse(finalTime);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(finTime);

            if (finalTime.compareTo(initialTime) < 0) {
                calendar2.add(Calendar.DATE, 1);
                calendar3.add(Calendar.DATE, 1);
            }

            java.util.Date actualTime = calendar3.getTime();
            if ((actualTime.after(calendar1.getTime()) || actualTime.compareTo(calendar1.getTime()) == 0) 
                    && actualTime.before(calendar2.getTime())) {
                valid = true;
            }
            return valid;
        } else {
            throw new IllegalArgumentException("Not a valid time, expecting HH:MM:SS format");
        }
    }

	/**
	 * get current time.
	 * @return
	 */
	public String getCurrentTime(){
	    Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(cal.getTime());
	}

	/**
	 * get current Date.
	 * @return
	 */
	public String getCurrentDate(String dateFormat){
	    Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.format(cal.getTime());
	}
	
	public Gson getGson() {
		if(gson == null) {
			GsonBuilder builder = new GsonBuilder();
			gson = builder.create();
		}
		return gson;
	}
	
	public Gson getGson(String dateFormat) {
		GsonBuilder builder = new GsonBuilder();
		gson = builder.registerTypeAdapter(Date.class, new CustomDateDeSerializer(dateFormat)).create();
		return gson;
	}
	
	/**
	 * Returns a map of with attribute name as key & attribute value as value of
	 * map
	 * 
	 * @param object
	 * @return Map<String, String>
	 */
	public Map<String, String> convertObjectToMap(Object object) {
		Map<String, String> paramMap = null;
		try {
			paramMap = BeanUtils.describe(object);
			if(paramMap.containsKey(Constants.CLASS)) {
				paramMap.remove(Constants.CLASS);
			}
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		return paramMap;
	}
	
	public String generateRandomString(){
		String randomString = Constants.BLANK;
		String uuid = UUID.randomUUID().toString();
		if(null!=uuid && uuid.contains(Constants.HYFUN)){
			String randomStringArr[] = uuid.split(Constants.HYFUN);
			if(randomStringArr.length>0){
				randomString = randomStringArr[0].toUpperCase();
			}
		}
		return randomString;
	}
	
	/**
	 * Validate Payment request
	 * @param orderInfo
	 * @param paymentGateWayInfo
	 * @return
	 */
	public boolean isValidatePayment(HttpServletRequest request){
		    OrderInfo orderInfo = (OrderInfo) request.getSession().getAttribute(PaymentConstants.ORDERINFO);
		    if(null != orderInfo){
		    	PaymentGatewayInfo paymentGateWayInfo = orderInfo.getPaymentGateWayInfo();
		    	if(null != paymentGateWayInfo){
				    String payUVerificationUrl = env.getProperty(PropertyKeyConstants.ServerProperties.PAYMENT_VERIFICATION_URL);
				    //String payUVerificationUrl = "https://test.payu.in/merchant/postservice?form=2";
					String method = "verify_payment";
					 //Salt of the merchant
					String salt = paymentGateWayInfo.getSalt();
					//Key of the merchant
					String key = paymentGateWayInfo.getMerchantId();
					String var1 = orderInfo.getTxnId();//transaction id
					String toHash = key + "|" + method + "|" + var1 + "|" + salt;	
				    // Create connection 
				    try {
				    	 String response = getResponseFromUrl(payUVerificationUrl, method, toHash, var1, key);           
				    	 logger.info("Payment Validate Response Utility==>"+orderInfo.getTxnId() +" => Response=> "+response);
				    	 Gson gson = new GsonBuilder().create(); 
					     JsonObject json =  gson.fromJson(response, JsonObject.class);
					     String paymentValidateJson = json.getAsJsonObject("transaction_details").get(var1).toString();
					     PaymentValidateDTO paymentValidateDto =  gson.fromJson(paymentValidateJson, PaymentValidateDTO.class);
					     if(null!=paymentValidateDto 
					    		 && paymentValidateDto.getStatus().equalsIgnoreCase(orderInfo.getStatus())){
					         logger.info("Payment Validate Response Utility==>"+orderInfo.getTxnId() +" => Response=> "+paymentValidateDto.getStatus());
					         request.getSession().removeAttribute(PaymentConstants.ORDERINFO);
					         return true;
					     }
				  } catch (Exception e) {
				   	   e.printStackTrace();
				   	   logger.error("Error in verify Payment method");
				  }
		     }
		  }
		  request.getSession().removeAttribute(PaymentConstants.ORDERINFO);
		  return false; 
	   }

	private String getResponseFromUrl(String payUVerificationUrl, String method, String toHash, String var1, String key) throws Exception{
		String Hashed = convertStringToCheckSumCode("SHA-512",toHash);
        String Poststring = "key=" + key +  "&command=" + method +  "&hash=" + Hashed + "&var1=" + var1 ;
	    URL url = new URL(payUVerificationUrl);
	    URLConnection conn = url.openConnection();
	    conn.setDoOutput(true);
	    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
	    wr.write(Poststring);
	    wr.flush();
	    // Get the response
	    BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    String line;
	    String response = "";
	    while ((line = rd.readLine()) != null) {
	        response += line;
	    }
		return response;      
	}
	   /**
	    * Generate check sum code algorithm wise like as SHA-256,SHA-512,MD5 
	    * @param algorithm
	    * @param str
	    * @return
	    * @throws Exception
	    */
	   private String convertStringToCheckSumCode(String algorithm,String str)throws Exception {
			try{
		        MessageDigest md = MessageDigest.getInstance(algorithm);
		        md.update(str.getBytes());
		        byte byteData[] = md.digest();
		        //convert the byte to hex format method 1
		        StringBuffer sb = new StringBuffer();
		        for (int i = 0; i < byteData.length; i++) {
		         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		        }
		        //convert the byte to hex format method 2
		        StringBuffer hexString = new StringBuffer();
		    	for (int i=0;i<byteData.length;i++) {
		    		String hex=Integer.toHexString(0xff & byteData[i]);
		   	     	if(hex.length()==1) hexString.append(Constants.ZERO_STRING);
		   	     	hexString.append(hex);
		    	}
		    	return hexString.toString();
		     }catch(Exception e){
		    	 e.printStackTrace();
		     }
			return Constants.BLANK;
	   }
	   
	   public String getIpAdress(HttpServletRequest request) {
		   String ipAddress = request.getHeader("X-FORWARDED-FOR");
		   if (ipAddress == null) {
		   	   ipAddress = request.getRemoteAddr();
		   }
		   return ipAddress;
	   }
}
