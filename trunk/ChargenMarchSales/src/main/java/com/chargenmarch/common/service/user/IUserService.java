package com.chargenmarch.common.service.user;

import java.util.List;
import java.util.Map;

import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.enums.user.UserVerificationStatus;

public interface IUserService {
	public int addUser(UserDTO user);
	public UserDTO getUserByMobile(String mobileNo);
	/*
	 * Get all users by Mobile Number
	 * */
	public UserDTO getUserById(int id);
	public UserDTO getUserByEmailId(String emailId);
	public UserDTO getUserByMobileNo(String mobileNo);
	public int updateUserWalletBalance(UserDTO user);
	public int updateUserRechargeAccount(UserDTO user);
	public int updateUserPassword(UserDTO user);
	public int updateUserVerifiedStatus(UserDTO user, UserVerificationStatus status);
	public Map<Integer, UserDTO> getUserInfoMapById();
	public Map<String, UserDTO> getUserInfoMapByEmailId();
	public void resetPassword(String email, String password);
	public int sendOneTimePassword(UserDTO user);
	public int checkUser(UserDTO user);
	public void sendWelcomeMail(UserDTO user);
	public List<UserDTO> getUserByVerificationStatus(UserVerificationStatus verified);
	public String generateVerificationLink(UserDTO user);
	public UserDTO getUserByReferralCode(String referralCode);
	public int getReferralCashbackAmount();
	public int insertReferralLogs(UserDTO user,UserDTO referralUserDTO,int cashbackAmount);
	public int updateRefferalCodeById(UserDTO user,String referralCode);
	public void updateUserReferralCode(UserDTO user);
	public void sendCAMReferralMail(UserDTO user);
	public void updateUserCacheMaps(UserDTO user);
	public List getReferralLogs(UserDTO user);
	public int insertEmailLogs(UserDTO user);
	public int truncateEmailLogs();
	public int getReferralAmountByChargersId(UserDTO user);
	public int updateUserRefferalBalance(UserDTO user);
	public void updateUserReferralAmount();
	public int getReferralRechargeAmountByChargersId(UserDTO user);
	public UserDTO getReferrerByUser(UserDTO user);
	public Map<Integer, UserDTO> sendMailUserInfoMapById();
	public int saveOrUpdateBrowserNotificationLogs(UserDTO user,String browserId);
	public int calculateWalletLoadMoney(int amount);
	public List<UserDTO> getCAMSalesUserList();
}
