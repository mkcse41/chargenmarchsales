package com.chargenmarch.common.validator.user;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.dto.user.UserDTO;

@Component
public class UserDetailValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return UserDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		UserDTO user = (UserDTO) obj;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, Constants.NAME, "name.error","Incorrect Name.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, Constants.EMAIL_ID, "email.error","Incorrect EmailId.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, Constants.MOBILE_NO, "mobile.error","Incorrect Mobile Number.");
		
		
		
		if(user.getMobileNo()!=null && user.getMobileNo().length()!=10){
			errors.reject(Constants.MOBILE_NO, "Less Than 10 Digits in Mobile.");
		}
		
	}
}
