package com.chargenmarch.common.thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.chargenmarch.charge.constants.recharge.RechargeConstants;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.constants.FilePathConstants;
import com.chargenmarch.common.constants.PropertyKeyConstants;
import com.chargenmarch.common.dto.user.UserDTO;
import com.chargenmarch.common.service.email.impl.EmailSenderServiceImpl;
import com.chargenmarch.common.service.user.IUserService;

@Component
@Scope(Constants.PROTOTYPE)
@PropertySource(FilePathConstants.CLASSPATH+FilePathConstants.API_PROPERTIES)
public class UserConfirmationMailSenderThread implements Runnable{

	private final static Logger logger = LoggerFactory.getLogger(UserConfirmationMailSenderThread.class);
	@Autowired
	IUserService userSerive;
	@Autowired 
	EmailSenderServiceImpl emailSenderService;
	@Autowired
	private Environment env;
	
	private UserDTO userDto;
	
	public void setUser(UserDTO userDto){
		this.userDto = userDto;
	}
	
	public void run() {
		try{
			userSerive.sendWelcomeMail(userDto);
		} catch(Exception e){
			logger.error("Error in sending mail at user confirmation : "+userDto.getEmailId()+"\n"+e);
			emailSenderService.sendEmailWithMultipleRecipient(Constants.NOREPLY_EMAIL_ID, RechargeConstants.RECHARGE_ERROR_EMAIL_ID.split(Constants.COMMA),env.getProperty(PropertyKeyConstants.APIProperties.RECHARGE_EXCEPTION_MAILLIST).split(Constants.COMMA), "Error in sending mail at user confirmation:" + userDto.getEmailId(), e.getMessage(),null,null, false);
		}
	}
}
