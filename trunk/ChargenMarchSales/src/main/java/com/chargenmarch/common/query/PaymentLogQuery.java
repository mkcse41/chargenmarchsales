package com.chargenmarch.common.query;

public interface PaymentLogQuery {

	public static final String INSERT_INTO_PAYMENT_TRACKING_LOG= "insert into payment_tracking_logs set chargerid=:chargerId, ipaddress=:ipAddress, city=:city, sessionid=:sessionId, paymentstatus=:paymentStatus, successfulrecharge=:successfulRecharge, failurereason=:failureReason, number=:rechargeDto.number, amount=:rechargeDto.amount, mobileService=:rechargeDto.mobileService, serviceType=:rechargeDto.serviceType, isUseWallet=:rechargeDto.useWallet, isQuickRecharge=:rechargeDto.isQuickRecharge, isUseReferral=:rechargeDto.useReferral, walletwithgateway=:walletWithGateway, timestamp=now()";
	public static final String UPDATE_PAYMENT_TRACKING_LOG= "update payment_tracking_logs set paymentstatus=?, successfulrecharge=?, failurereason=?,updatetimestamp=now() where id=?";
	public static final String SELECT_FROM_PAYMENT_TRACKING_LOG_BY_ID= "select * from payment_tracking_logs where id=:id";
}
