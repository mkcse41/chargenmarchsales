package com.chargenmarch.common.dao.usercampaign;

import com.chargenmarch.common.dto.campaign.user.CampaignUserDTO;

public interface IUserCampaignDao {
	// add user data when user hit any url first time in a session.
	public void addUserCampaignData(CampaignUserDTO campaignUserDTO);
	
	// add user complete tracking by session id, pages visited by user in a single session.
	public void addUserCompleteTracking(CampaignUserDTO campaignUserDTO);
}
