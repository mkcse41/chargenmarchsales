package com.chargenmarch.common.dto.paymentgateway;

import java.util.ArrayList;
import java.util.List;

public class OrderInfo {

	private String firstName;
	private String lastName;
	private String emailId;
	private String mobileno;
	private String pinCode;
	private String city;
	private String state;
	private String address;
	private String productName;
	private float amount;
	private String txnId;
	private String hash;
	private String successUrl;
	private String failureUrl;
	private String cancelUrl;
	private List<String> errorStringList = new ArrayList<String>();
	private String status;
	private int productId;
	private String pgType;
	private PaymentGatewayInfo paymentGateWayInfo;
	private ProductInfo productInfo;
	private String userCredentials; //This field used to store user card details
	/* This Field use in payment gateway response  */
	private String mihpayid;
	private String mode;
	private String Error ;
	private String bankcode ;
	private String PG_TYPE;
	private String bank_ref_num ;
	private String shipping_firstname ;
	private String shipping_lastname ;
	private String shipping_address1;
	private String shipping_address2;
	private String shipping_city;
	private String shipping_state ;
	private String shipping_country ;
	private String shipping_zipcode ;
	private String shipping_phone ;
	private String unmappedstatus;
	private float discount;
	private String offer;
	private String responsejson;
	private String orderId;
	private int leadId;

	
	public String getOffer() {
		return offer;
	}
	public void setOffer(String offer) {
		this.offer = offer;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public String getTxnId() {
		return txnId;
	}
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public String getSuccessUrl() {
		return successUrl;
	}
	public void setSuccessUrl(String successUrl) {
		this.successUrl = successUrl;
	}
	public String getFailureUrl() {
		return failureUrl;
	}
	public void setFailureUrl(String failureUrl) {
		this.failureUrl = failureUrl;
	}
	public String getCancelUrl() {
		return cancelUrl;
	}
	public void setCancelUrl(String cancelUrl) {
		this.cancelUrl = cancelUrl;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public String getPgType() {
		return pgType;
	}
	public void setPgType(String pgType) {
		this.pgType = pgType;
	}
	public PaymentGatewayInfo getPaymentGateWayInfo() {
		return paymentGateWayInfo;
	}
	public void setPaymentGateWayInfo(PaymentGatewayInfo paymentGateWayInfo) {
		this.paymentGateWayInfo = paymentGateWayInfo;
	}
	public List<String> getErrorStringList() {
		return errorStringList;
	}
	public void setErrorStringList(List<String> errorStringList) {
		this.errorStringList = errorStringList;
	}
	
	
	public String getMihpayid() {
		return mihpayid;
	}
	public void setMihpayid(String mihpayid) {
		this.mihpayid = mihpayid;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getError() {
		return Error;
	}
	public void setError(String error) {
		Error = error;
	}
	public String getBankcode() {
		return bankcode;
	}
	public void setBankcode(String bankcode) {
		this.bankcode = bankcode;
	}
	public String getPG_TYPE() {
		return PG_TYPE;
	}
	public void setPG_TYPE(String pG_TYPE) {
		PG_TYPE = pG_TYPE;
	}
	public String getBank_ref_num() {
		return bank_ref_num;
	}
	public void setBank_ref_num(String bank_ref_num) {
		this.bank_ref_num = bank_ref_num;
	}
	public String getShipping_firstname() {
		return shipping_firstname;
	}
	public void setShipping_firstname(String shipping_firstname) {
		this.shipping_firstname = shipping_firstname;
	}
	public String getShipping_lastname() {
		return shipping_lastname;
	}
	public void setShipping_lastname(String shipping_lastname) {
		this.shipping_lastname = shipping_lastname;
	}
	public String getShipping_address1() {
		return shipping_address1;
	}
	
	public ProductInfo getProductInfo() {
		return productInfo;
	}
	public void setProductInfo(ProductInfo productInfo) {
		this.productInfo = productInfo;
	}
	public void setShipping_address1(String shipping_address1) {
		this.shipping_address1 = shipping_address1;
	}
	public String getShipping_address2() {
		return shipping_address2;
	}
	public void setShipping_address2(String shipping_address2) {
		this.shipping_address2 = shipping_address2;
	}
	public String getShipping_city() {
		return shipping_city;
	}
	public void setShipping_city(String shipping_city) {
		this.shipping_city = shipping_city;
	}
	public String getShipping_state() {
		return shipping_state;
	}
	public void setShipping_state(String shipping_state) {
		this.shipping_state = shipping_state;
	}
	public String getShipping_country() {
		return shipping_country;
	}
	public void setShipping_country(String shipping_country) {
		this.shipping_country = shipping_country;
	}
	public String getShipping_zipcode() {
		return shipping_zipcode;
	}
	public void setShipping_zipcode(String shipping_zipcode) {
		this.shipping_zipcode = shipping_zipcode;
	}
	public String getShipping_phone() {
		return shipping_phone;
	}
	public void setShipping_phone(String shipping_phone) {
		this.shipping_phone = shipping_phone;
	}
	public String getUnmappedstatus() {
		return unmappedstatus;
	}
	public void setUnmappedstatus(String unmappedstatus) {
		this.unmappedstatus = unmappedstatus;
	}
	
	
	public float getDiscount() {
		return discount;
	}
	public void setDiscount(float discount) {
		this.discount = discount;
	}
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getResponsejson() {
		return responsejson;
	}
	public void setResponsejson(String responsejson) {
		this.responsejson = responsejson;
	}
	
	
	public int getLeadId() {
		return leadId;
	}
	public void setLeadId(int leadId) {
		this.leadId = leadId;
	}	
	
	public String getUserCredentials() {
		return userCredentials;
	}
	public void setUserCredentials(String userCredentials) {
		this.userCredentials = userCredentials;
	}
	@Override
	public String toString() {
		return "OrderInfo [firstName=" + firstName + ", lastName=" + lastName
				+ ", emailId=" + emailId + ", mobileno=" + mobileno
				+ ", pinCode=" + pinCode + ", city=" + city + ", state="
				+ state + ", address=" + address + ", productName="
				+ productName + ", amount=" + amount + ", txnId=" + txnId
				+ ", hash=" + hash + ", successUrl=" + successUrl
				+ ", failureUrl=" + failureUrl + ", cancelUrl=" + cancelUrl
				+ ", errorStringList=" + errorStringList + ", status=" + status
				+ ", productId=" + productId + ", pgType=" + pgType
				+ ", paymentGateWayInfo=" + paymentGateWayInfo
				+ ", productInfo=" + productInfo + ", mihpayid=" + mihpayid
				+ ", mode=" + mode + ", Error=" + Error + ", bankcode="
				+ bankcode + ", PG_TYPE=" + PG_TYPE + ", bank_ref_num="
				+ bank_ref_num + ", shipping_firstname=" + shipping_firstname
				+ ", shipping_lastname=" + shipping_lastname
				+ ", shipping_address1=" + shipping_address1
				+ ", shipping_address2=" + shipping_address2
				+ ", shipping_city=" + shipping_city + ", shipping_state="
				+ shipping_state + ", shipping_country=" + shipping_country
				+ ", shipping_zipcode=" + shipping_zipcode
				+ ", shipping_phone=" + shipping_phone + ", unmappedstatus="
				+ unmappedstatus + ", discount=" + discount + ", offer="
				+ offer + ", responsejson=" + responsejson + ", orderId="
				+ orderId + ", leadId=" + leadId + "]";
	}
	
	
}
