package com.chargenmarch.common.dto.user;

import java.util.Date;

import com.chargenmarch.charge.dto.mobilerecharge.RechargeDTO;

/**
 * @author gambit
 * DTO to track user activity around the website
 */
public class PaymentLogDto {
	private int id;
	private int chargerId;
	private String ipAddress;
	private String city;
//	private String url;
	private String sessionId;
//	private boolean paymentStep;
	private boolean paymentStatus;
	private boolean successfulRecharge;
	private Date timestamp;
//	private RechargeStepEnum rechargeStep;
	private String failureReason;
	private boolean walletWithGateway;
	private RechargeDTO rechargeDto;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getChargerId() {
		return chargerId;
	}
	public void setChargerId(int chargerId) {
		this.chargerId = chargerId;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public boolean isPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(boolean paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public boolean isSuccessfulRecharge() {
		return successfulRecharge;
	}
	public void setSuccessfulRecharge(boolean successfulRecharge) {
		this.successfulRecharge = successfulRecharge;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public String getFailureReason() {
		return failureReason;
	}
	public void setFailureReason(String failureReason) {
		this.failureReason = failureReason;
	}
	public RechargeDTO getRechargeDto() {
		return rechargeDto;
	}
	public void setRechargeDto(RechargeDTO rechargeDto) {
		this.rechargeDto = rechargeDto;
	}
	
	public boolean isWalletWithGateway() {
		return walletWithGateway;
	}
	public void setWalletWithGateway(boolean walletWithGateway) {
		this.walletWithGateway = walletWithGateway;
	}
	@Override
	public String toString() {
		return "PaymentLogDto [id=" + id + ", chargerId=" + chargerId + ", ipAddress=" + ipAddress + ", city=" + city
				+ ", sessionId=" + sessionId + ", paymentStatus=" + paymentStatus + ", successfulRecharge="
				+ successfulRecharge + ", timestamp=" + timestamp + ", failureReason=" + failureReason
				+ ", walletWithGateway=" + walletWithGateway + ", rechargeDto=" + rechargeDto + "]";
	}
	
}