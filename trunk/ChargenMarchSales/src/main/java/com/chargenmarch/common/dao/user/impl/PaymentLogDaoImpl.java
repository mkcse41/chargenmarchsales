package com.chargenmarch.common.dao.user.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.chargenmarch.charge.joloapi.query.Query;
import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.dao.user.IPaymentLogDao;
import com.chargenmarch.common.dto.user.PaymentLogDto;
import com.chargenmarch.common.mapper.user.PaymentMapper;
import com.chargenmarch.common.query.PaymentLogQuery;

/**
 * @author gambit
 * Dao Layer to handle User Activity tracking across the website
 * 
 */
@Repository
public class PaymentLogDaoImpl implements IPaymentLogDao{
	private final static Logger logger = LoggerFactory.getLogger(PaymentLogDaoImpl.class);
	
	@Autowired
	private NamedParameterJdbcTemplate namedParamJdbcTemplate;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	/**
	 * Logs every step of user activity according to the DTO
	 * @param dto
	 * @return int
	 */
	@Override
	public int logPaymentActivity(PaymentLogDto dto) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(dto);
		final KeyHolder holder = new GeneratedKeyHolder();
		namedParamJdbcTemplate.update(PaymentLogQuery.INSERT_INTO_PAYMENT_TRACKING_LOG, params, holder, new String[] {Constants.ID});
		Number generatedId = holder.getKey();
		return generatedId.intValue();
	}
	
	@Override
	public int updatePaymentLog(PaymentLogDto dto) {
		Object args[] = {dto.isPaymentStatus(), dto.isSuccessfulRecharge(),dto.getFailureReason(), dto.getId()};
		return jdbcTemplate.update(PaymentLogQuery.UPDATE_PAYMENT_TRACKING_LOG, args);
	}

	@Override
	public List<PaymentLogDto> getPaymentLogById(int id) {
		SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);
		return (List<PaymentLogDto>) namedParamJdbcTemplate.query(PaymentLogQuery.SELECT_FROM_PAYMENT_TRACKING_LOG_BY_ID, namedParameters, new PaymentMapper());
		
	}
	
	

}
