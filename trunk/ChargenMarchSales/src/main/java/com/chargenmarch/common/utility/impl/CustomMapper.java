package com.chargenmarch.common.utility.impl;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.BeansException;

/**
* 
* @author rajat.singhvi
*	
* Transfer value from a DTO to another DTO
*	
* Type of field should be same in both classes (DTO) 
*
*/

public class CustomMapper {
	public static Object map(Object object, Class<?> classname){
		Object mappedObj = new Object();
		try {
			mappedObj = doMapping(object,classname);
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mappedObj;
	}
	
	private static Object doMapping(Object object, Class<?> classname)
			throws InstantiationException, IllegalAccessException {

		Map<Object, Object> map = new HashMap<Object, Object>();

		Object classObj = classname.newInstance();

		// source object data store in map
		Field[] field = object.getClass().getDeclaredFields();
		for (int i = 0; i < field.length; i++) {
			field[i].setAccessible(true);
			// System.out.println(field[i].getName()); // field name
			// System.out.println( field[i].get(object) ); // field value
			map.put(field[i].getName(), field[i]);
		}

		// set data into Destination Object
		Field[] allFields = classname.getDeclaredFields();
		for (Field fieldObj : allFields) {
			fieldObj.setAccessible(true);

			Field fieldFromMap = (Field) map.get(fieldObj.getName());
			if (fieldObj.getType().equals(fieldFromMap.getType())) {
				Object Value = fieldFromMap.get(object);
				fieldObj.set(classObj, Value);
			}
		}
		return classObj;
	}
}
