package com.chargenmarch.common.service.usercampaign.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chargenmarch.common.dao.usercampaign.IUserCampaignDao;
import com.chargenmarch.common.dto.campaign.user.CampaignUserDTO;
import com.chargenmarch.common.service.usercampaign.IUserCampaignService;

@Service
public class UserCampaignServiceImpl implements IUserCampaignService {
	private final static Logger logger = LoggerFactory.getLogger(UserCampaignServiceImpl.class);

	@Autowired
	IUserCampaignDao userCampaignDao;

	/*
	 * (non-Javadoc)
	 * @see com.chargenmarch.common.service.usercampaign.IUserCampaignService#addUserCampaignData(com.chargenmarch.common.dto.campaign.user.CampaignUserDTO)
	 * 
	 * add user data when user hit any url first time in a session.
	 */
	public void addUserCampaignData(CampaignUserDTO campaignUserDTO) {
		userCampaignDao.addUserCampaignData(campaignUserDTO);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.chargenmarch.common.service.usercampaign.IUserCampaignService#addUserCompleteTracking(com.chargenmarch.common.dto.campaign.user.CampaignUserDTO)
	 * 
	 * add user complete tracking by session id, pages visited by user in a single session.
	 */
	public void addUserCompleteTracking(CampaignUserDTO campaignUserDTO) {
		userCampaignDao.addUserCompleteTracking(campaignUserDTO);
	}
	

}
