package com.chargenmarch.common.constants.paymentgateway;

public class PaymentConstants {

	public static final String PAYMENT_GATE_WAY = "PaymentGateWay";
	public static final String PAYU_SALT = "salt";
	public static final String MERCHANTID = "merchantId";
	public static final String count = "count";
	public static final String PAY_U = "PayU";
	public static final String SHA_512 = "SHA-512";
	public static final String AMT = "amt";
	public static final String PRODUCT_NAME = "productName";
	public static final String PAYMENT = "payment";
	public static final String PAYU_RESPONSE_URL = "payment/paymentResponse";
	public static final String SALT = "salt";
	public static final String ADD = "Add";
	public static final String SURL = "surl";
	public static final String CURL = "curl";
	public static final String FURL = "furl";
	public static final String RECEIVER_EMAILID = "receiverEmailId";
	public static final String RECIPIENT = "recipient";
	public static final String ISSENDMAIL = "isSendMail";
	public static final String IS_REQUIRED_RESPONSE = "isRequiredResponse";
	public static final String ORDER_PREFIX = "orderprefix";
	public static final String PG_URL = "pgUrl";
	public static final String PIPE = "|";
	public static final String UPDATE = "Update";
	public static final String TRUE = "True";
	public static final String FALSE = "False";
	public static final String SLASH = "/";
	public static final String COMMA = ",";
	public static final String ORDERINFO = "orderInfo";
	public static final String ERROR_STRING_LIST = "errorStringList";
	public static final String ID = "Id";
	public static final String URL = "url";
	public static final String RESPONSE = "response";
	public static final String PAYMENT_GATEWAY = "paymentGateWay";
	public static final String PAYMENT_ERROR_EMAIL_ID = "mukesh@chargenmarch.com, durlabh@chargenmarch.com";
	public static final String PAYMENT_ERROR_STRING_LIST = "paymentErrorStringList";
	public static final String COLON = ":";
	public static final String PAYMENT_STATUS = "paymentStatus";
	
}
