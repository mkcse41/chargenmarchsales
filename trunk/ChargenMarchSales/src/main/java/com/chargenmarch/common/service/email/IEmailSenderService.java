package com.chargenmarch.common.service.email;

import com.chargenmarch.common.dto.email.EmailDTO;

/**
 * 
 * @author mukesh
 *
 */
public interface IEmailSenderService {
	public void sendEmail(String from,String[] to,String sub,String msgBody,String fileName,String filePath, boolean isHTML);
	public void sendEmailWithMultipleRecipient(String from,String[] to,String[] ccEmailIds,String sub,String msgBody,String fileName,String filePath, boolean isHTML);
	public void sendEmail(EmailDTO email);
}
