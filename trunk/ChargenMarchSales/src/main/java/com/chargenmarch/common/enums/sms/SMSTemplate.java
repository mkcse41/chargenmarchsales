package com.chargenmarch.common.enums.sms;

public enum SMSTemplate {
	MOBILE_VERFICATION("Mobile Verfication#1"),
	RECHARGE("Recharge#3");
    
	private String templateParameter;

	public String getTemplateParameter() {
		return templateParameter;
	}

	private SMSTemplate(String templateParameter) {
		this.templateParameter = templateParameter;
	}
	
}
