package com.chargenmarch.common.constants;

public interface MailingConstants {
	public static final String EXCEPTION_MAIL_ID = "exception.mail.id";
	public static final String RECHARGE_MAILLIST = "recharge.exception.maillist";
	public static final String GMAIL_MAILLIST = "gmail.maillist";
	public static final String CAM_GMAIL_ID = "chargeandmarch@gmail.com";
}
