package com.chargenmarch.common.service.user;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.chargenmarch.charge.thread.PaymentTrackerThread;
import com.chargenmarch.common.dto.user.PaymentLogDto;


/**
 * @author gambit
 * Service to track all the user activity across the website
 */
public interface IPaymentLogService {

	/**
	 * Logs every step of user activity according to the DTO
	 * @param dto
	 * @return int
	 */
	public int logPaymentActivity(PaymentLogDto dto);
	public int updatePaymentLog(PaymentLogDto dto);
	public PaymentLogDto getPaymentLogById(int id);
	public void invokePaymentTrackerThread(ThreadPoolTaskExecutor taskExecutor, PaymentTrackerThread tracker, PaymentLogDto userTrackingDto, boolean isUpdate);
}
