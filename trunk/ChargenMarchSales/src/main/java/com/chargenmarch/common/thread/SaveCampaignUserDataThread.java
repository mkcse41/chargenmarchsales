package com.chargenmarch.common.thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.chargenmarch.common.constants.Constants;
import com.chargenmarch.common.dto.campaign.user.CampaignUserDTO;
import com.chargenmarch.common.dto.campaign.user.CampaignUserDTO.Tracking;
import com.chargenmarch.common.service.usercampaign.IUserCampaignService;

/**
 * This class used to save user tracking details.
 * @author mukesh
 *
 */
@Component
@Scope(Constants.PROTOTYPE)
public class SaveCampaignUserDataThread implements Runnable{

	private final static Logger logger = LoggerFactory.getLogger(SaveCampaignUserDataThread.class);
	
	@Autowired
	IUserCampaignService userCampaignService;
	
	private CampaignUserDTO campaignUserDTO;
	
	public void setData(CampaignUserDTO campaignUserDTO){
		this.campaignUserDTO = campaignUserDTO;
	}
	
	/* This thread use to save user tracking event in database.
	 * (non-Javadoc)
	 * 
	 * case 1.) if user comes first time in a session then store users complete data with landing url.
	 *           set  SINGLEPATH in Tracking for identification. 
	 * 
	 * case 2.) store users complete tracking , all pages user visits in a single session.
	 * 			set MULTIPATH in Tracking for identification. 
	 * 
	 */
	public void run() {
		try {
			if(campaignUserDTO.getTracking().equals(Tracking.MULTIPATH)){
				userCampaignService.addUserCompleteTracking(campaignUserDTO);
			}else{
				userCampaignService.addUserCampaignData(campaignUserDTO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("error in store user campaign data"+e);
		}
	}
}
