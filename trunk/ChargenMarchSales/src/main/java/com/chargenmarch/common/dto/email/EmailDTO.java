package com.chargenmarch.common.dto.email;

public class EmailDTO {
	String subject;
	String[] to;
	String from;
	String[] cc;
	String[] bcc;
	String message;
	String fileName;
	String filePath;
	
	public EmailDTO(String subject, String[] to, String from, String[] cc, String[] bcc, String message,
			String fileName, String filePath) {
		this.subject = subject;
		this.to = to;
		this.from = from;
		this.cc = cc;
		this.bcc = bcc;
		this.message = message;
		this.fileName = fileName;
		this.filePath = filePath;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String[] getTo() {
		return to;
	}
	public void setTo(String[] to) {
		this.to = to;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String[] getCc() {
		return cc;
	}
	public void setCc(String[] cc) {
		this.cc = cc;
	}
	public String[] getBcc() {
		return bcc;
	}
	public void setBcc(String[] bcc) {
		this.bcc = bcc;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
}
