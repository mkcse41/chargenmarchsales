-- MySQL dump 10.13  Distrib 5.6.27, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: chargenmarch
-- ------------------------------------------------------
-- Server version	5.6.27-0ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jolo_circle_mapping`
--

DROP TABLE IF EXISTS `jolo_circle_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jolo_circle_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `circleName` varchar(45) NOT NULL,
  `circle_code` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jolo_circle_mapping`
--

LOCK TABLES `jolo_circle_mapping` WRITE;
/*!40000 ALTER TABLE `jolo_circle_mapping` DISABLE KEYS */;
INSERT INTO `jolo_circle_mapping` VALUES (1,'Andhra Pradesh','5'),(2,'Assam','19'),(3,'Bihar & Jharkhand','17'),(4,'Chennai','23'),(5,'Delhi/NCR','1'),(6,'Gujarat','8'),(7,'Haryana','16'),(8,'Himachal Pradesh','21'),(9,'Jammu & Kashmir','22'),(10,'Karnataka','7'),(11,'Kerala','14'),(12,'Kolkata','3'),(13,'Mumbai','2'),(14,'North East','20'),(15,'Orissa','18'),(16,'Punjab','15'),(17,'Rajasthan','13'),(18,'Tamil Nadu','6'),(19,'Uttar Pradesh (E)','9'),(20,'Uttar Pradesh (W)','11'),(21,'West Bengal','12'),(22,'Madhya Pradesh','10'),(23,'None','0'),(24,'Maharashtra','4');
/*!40000 ALTER TABLE `jolo_circle_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jolo_number_operator_mapping`
--

DROP TABLE IF EXISTS `jolo_number_operator_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jolo_number_operator_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobilenumber` varchar(45) NOT NULL,
  `operator` varchar(45) NOT NULL,
  `pay_code` varchar(45) NOT NULL,
  `code` varchar(45) NOT NULL,
  `circle` varchar(45) NOT NULL,
  `pay_circle_code` varchar(45) NOT NULL,
  `circle_code` varchar(45) NOT NULL,
  `insertdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatedate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mobilenumber_UNIQUE` (`mobilenumber`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jolo_number_operator_mapping`
--

LOCK TABLES `jolo_number_operator_mapping` WRITE;
/*!40000 ALTER TABLE `jolo_number_operator_mapping` DISABLE KEYS */;
INSERT INTO `jolo_number_operator_mapping` VALUES (1,'9828487909','VODAFONE','VF','22','Rajasthan','13','13','2016-02-22 18:33:44','2016-02-22 18:33:44');
/*!40000 ALTER TABLE `jolo_number_operator_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jolo_operator_mapping`
--

DROP TABLE IF EXISTS `jolo_operator_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jolo_operator_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operatorName` varchar(45) NOT NULL,
  `pay_code` varchar(45) NOT NULL,
  `code` varchar(45) NOT NULL DEFAULT '0',
  `recharge_service_type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jolo_operator_mapping`
--

LOCK TABLES `jolo_operator_mapping` WRITE;
/*!40000 ALTER TABLE `jolo_operator_mapping` DISABLE KEYS */;
INSERT INTO `jolo_operator_mapping` VALUES (1,'AIRTEL','AT','28','prepaid'),(2,'BSNL','BS','3','prepaid'),(3,'BSNL SPECIAL','BSS','3','prepaid'),(4,'IDEA','IDX','8','prepaid'),(5,'VODAFONE','VF','22','prepaid'),(6,'RELIANCE CDMA','RL','12','prepaid'),(7,'RELIANCE GSM','RG','13','prepaid'),(8,'UNINOR','UN','19','prepaid'),(9,'UNINOR SPECIAL','UNS','19','prepaid'),(10,'MTS','MS','10','prepaid'),(11,'AIRCEL','AL','1','prepaid'),(12,'RELIANCE GSM','RG','13','prepaid'),(13,'TATA DOCOMO GSM ','TD','17','prepaid'),(14,'TATA DOCOMO GSM SPECIAL','TDS','17','prepaid'),(15,'TATA INDICOM (CDMA)','TI','18','prepaid'),(16,'MTNL DELHI','MTD','20','prepaid'),(17,'MTNL DELHI SPECIAL','MTDS','20','prepaid'),(18,'MTNL MUMBAI','MTM','6','prepaid'),(19,'MTNL MUMBAI SPECIAL','MTMS','20','prepaid'),(20,'VIDEOCON','VD','5','prepaid'),(21,'VIDEOCON SPECIAL','VDS','5','prepaid'),(22,'VIRGIN GSM','VG','30','prepaid'),(23,'VIRGIN GSM SPECIAL','VGS','30','prepaid'),(24,'VIRGIN CDMA','VC','32','prepaid'),(25,'T24','T24','33','prepaid'),(26,'T24 SPECIAL','T24S','33','prepaid'),(27,'TATA WALKY','TW','0','prepaid'),(28,'AIRTEL DTH','AD','0','DTH'),(29,'SUN DIRECT DTH','SD','0','DTH'),(30,'TATA SKY','TS','0','DTH'),(31,'DISH TV','DT','0','DTH'),(32,'RELIANCE BIG TV','BT','0','DTH'),(33,'VIDEOCON D2H','VT','0','DTH'),(34,'AIRTEL','ATD','0','datacard'),(35,'BSNL','BSD','0','datacard'),(36,'IDEA','IDXD','0','datacard'),(37,'VODAFONE','VFD','0','datacard'),(38,'RELIANCE CDMA','RLD','0','datacard'),(39,'UNINOR','UND','0','datacard'),(40,'MTS','MSD','0','datacard'),(41,'AIRCEL','ALD','0','datacard'),(42,'TATA INDICOM (CDMA)','TID','0','datacard'),(43,'MTNL DELHI','MTDD','0','datacard'),(44,'MTNL MUMBAI','MTMD','0','datacard'),(45,'VIDEOCON','VDD','0','datacard'),(46,'AIRTEL','APOS','0','postpaid'),(47,'BSNL','BPOS','0','postpaid'),(48,'IDEA','IPOS','0','postpaid'),(49,'VODAFONE','VPOS','0','postpaid'),(50,'RELIANCE GSM','RGPOS','0','postpaid'),(51,'RELIANCE CDMA','RCPOS','0','postpaid'),(52,'TATA DOCOMO GSM','DGPOS','0','postpaid'),(53,'TATA INDICOM (CDMA)','DCPOS','0','postpaid'),(54,'AIRCEL','CPOS','0','postpaid');
/*!40000 ALTER TABLE `jolo_operator_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recharge_logs`
--

DROP TABLE IF EXISTS `recharge_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recharge_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rechargeService` varchar(20) DEFAULT NULL,
  `request` text,
  `response` text,
  `insertTimestamp` datetime NOT NULL,
  `updateTimestamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recharge_logs`
--

LOCK TABLES `recharge_logs` WRITE;
/*!40000 ALTER TABLE `recharge_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `recharge_logs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-23  0:13:18
