-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: chargenmarch
-- ------------------------------------------------------
-- Server version	5.5.46-0ubuntu0.14.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `airline_cityinfo`
--

DROP TABLE IF EXISTS `airline_cityinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `airline_cityinfo` (
  `airport_code` varchar(5) DEFAULT NULL,
  `airport_name` varchar(100) DEFAULT NULL,
  `country_code` varchar(5) DEFAULT NULL,
  `country_name` varchar(40) DEFAULT NULL,
  `city` varchar(40) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `time_zone` varchar(10) DEFAULT NULL,
  `isPopular` varchar(2) DEFAULT NULL,
  `citydisplayname` varchar(40) DEFAULT NULL,
  `cityType` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `airline_cityinfo`
--

LOCK TABLES `airline_cityinfo` WRITE;
/*!40000 ALTER TABLE `airline_cityinfo` DISABLE KEYS */;
INSERT INTO `airline_cityinfo` VALUES ('IXA','Agartala','IN','India','Agartala','91.2404','23.887','5.5',NULL,NULL,'domestic'),('AGX','Agatti','IN','India','Agatti Island','72.176','10.8237','5.5',NULL,NULL,'domestic'),('AGR','Agra','IN','India','Agra','77.9609','27.1558','5.5',NULL,NULL,'domestic'),('AMD','Ahmedabad','IN','India','Ahmedabad','72.6347','23.0772','5.5',NULL,NULL,'domestic'),('AKD','Akola','IN','India','Akola','77.0586','20.699','5.5',NULL,NULL,'domestic'),('IXD','Allahabad','IN','India','Allahabad','81.7339','25.4401','5.5',NULL,NULL,'domestic'),('','Along','IN','India','Along','94.802','28.1753','5.5',NULL,NULL,'domestic'),('ATQ','Amritsar','IN','India','Amritsar','74.7973','31.7096','5.5',NULL,NULL,'domestic'),('IXU','Aurangabad','IN','India','Aurangabad','75.3981','19.8627','5.5',NULL,NULL,'domestic'),('BLR','Bangalore','IN','India','Bangalore','77.6682','12.95','5.5',NULL,NULL,'domestic'),('IXG','Belgaum','IN','India','Belgaum','74.6183','15.8593','5.5',NULL,NULL,'domestic'),('BEP','Bellary','IN','India','Bellary','76.8828','15.1628','5.5',NULL,NULL,'domestic'),('','Bhatinda','IN','India','Bhatinda','74.7558','30.2701','5.5',NULL,NULL,'domestic'),('BHO','Bhopal','IN','India','Bhopal','77.3374','23.2875','5.5',NULL,NULL,'domestic'),('BBI','Bhubaneshwar','IN','India','Bhubaneswar','85.8178','20.2444','6',NULL,NULL,'domestic'),('BHJ','Bhuj','IN','India','Bhuj','69.6702','23.2878','5.5',NULL,NULL,'domestic'),('','Nal','IN','India','Bikaner','73.2072','28.0706','5.5',NULL,NULL,'domestic'),('PAB','Bilaspur','IN','India','Bilaspur','82.111','21.9884','5.5',NULL,NULL,'domestic'),('BOM','Chhatrapati Shivaji Intl','IN','India','Bombay','72.8679','19.0887','5.5',NULL,NULL,'domestic'),('CCU','Netaji Subhash Chandra Bose Intl','IN','India','Calcutta','88.4467','22.6547','5.5',NULL,NULL,'domestic'),('IXC','Chandigarh','IN','India','Chandigarh','76.7885','30.6735','5.5',NULL,NULL,'domestic'),('CJB','Coimbatore','IN','India','Coimbatore','77.0434','11.03','5.5',NULL,NULL,'domestic'),('CDP','Cuddapah','IN','India','Cuddapah','78.7728','14.51','5.5',NULL,NULL,'domestic'),('NMB','Daman','IN','India','Daman','72.8432','20.4344','5.5',NULL,NULL,'domestic'),('DED','Dehradun','IN','India','Dehra Dun','78.1803','30.1897','5.5',NULL,NULL,'domestic'),('','Safdarjung','IN','India','Delhi','77.2058','28.5845','5.5',NULL,NULL,'domestic'),('DEL','Indira Gandhi Intl','IN','India','Delhi','77.1031','28.5665','5.5',NULL,NULL,'domestic'),('DBD','Dhanbad','IN','India','Dhanbad','86.4253','23.834','5.5',NULL,NULL,'domestic'),('DIB','Dibrugarh Airport','IN','India','Dibrugarh','95.0169','27.4839','5.5',NULL,NULL,'domestic'),('DIU','Diu Airport','IN','India','Diu','70.9211','20.7131','5.5',NULL,NULL,'domestic'),('GAY','Gaya','IN','India','Gaya','84.9512','24.7443','5.5',NULL,NULL,'domestic'),('GOI','Goa','IN','India','Goa','73.8314','15.3808','5.5',NULL,NULL,'domestic'),('GOP','Gorakhpur Airport','IN','India','Gorakhpur','83.4497','26.7397','5.5',NULL,NULL,'domestic'),('','Guna','IN','India','Guna','77.3473','24.6547','5.5',NULL,NULL,'domestic'),('GAU','Lokpriya Gopinath Bordoloi International Airport','IN','India','Guwahati','91.5859','26.1061','5.5',NULL,NULL,'domestic'),('GWL','Gwalior','IN','India','Gwalior','78.2278','26.2933','5.5',NULL,NULL,'domestic'),('','Hissar','IN','India','Hissar','75.7553','29.1794','5.5',NULL,NULL,'domestic'),('HYD','Hyderabad','IN','India','Hyderabad','78.4676','17.4531','5.5',NULL,NULL,'domestic'),('IDR','Devi Ahilyabai Holkar','IN','India','Indore','75.8011','22.7218','5.5',NULL,NULL,'domestic'),('JLR','Jabalpur','IN','India','Jabalpur','80.052','23.1778','5.5',NULL,NULL,'domestic'),('JAI','Jaipur','IN','India','Jaipur','75.8122','26.8242','5.5',NULL,NULL,'domestic'),('JSA','Jaisalmer','IN','India','Jaisalmer','70.865','26.8887','5.5',NULL,NULL,'domestic'),('IXJ','Jammu','IN','India','Jammu','74.8374','32.6891','5',NULL,NULL,'domestic'),('JGA','Jamnagar','IN','India','Jamnagar','70.0126','22.4655','5.5',NULL,NULL,'domestic'),('IXW','Jamshedpur','IN','India','Jamshedpur','86.1688','22.8132','5.5',NULL,NULL,'domestic'),('JDH','Jodhpur','IN','India','Jodhpur','73.0489','26.2511','5.5',NULL,NULL,'domestic'),('JRH','Jorhat','IN','India','Jorhat','94.1755','26.7315','5.5',NULL,NULL,'domestic'),('IXH','Kailashahar','IN','India','Kailashahar','92.0072','24.3082','5.5',NULL,NULL,'domestic'),('IXY','Kandla','IN','India','Kandla','70.1003','23.1127','5.5',NULL,NULL,'domestic'),('','Kanpur Chakeri','IN','India','Kanpur','80.4101','26.4043','5.5',NULL,NULL,'domestic'),('KNU','Kanpur','IN','India','Kanpur','80.3649','26.4414','5.5',NULL,NULL,'domestic'),('IXK','Keshod','IN','India','Keshod','70.2704','21.3171','5.5',NULL,NULL,'domestic'),('HJR','Khajuraho','IN','India','Khajuraho','79.9186','24.8172','5.5',NULL,NULL,'domestic'),('KLH','Kolhapur','IN','India','Kolhapur','74.2894','16.6647','5.5',NULL,NULL,'domestic'),('KTU','Kota','IN','India','Kota','75.8456','25.1602','5.5',NULL,NULL,'domestic'),('KUU','Kullu Manali','IN','India','Kulu','77.1544','31.8767','5.5',NULL,NULL,'domestic'),('IXL','Leh','IN','India','Leh','77.5465','34.1359','5.5',NULL,NULL,'domestic'),('IXI','Lilabari','IN','India','Lilabari','94.0976','27.2955','5.5',NULL,NULL,'domestic'),('LKO','Lucknow','IN','India','Lucknow','80.8893','26.7606','5.5',NULL,NULL,'domestic'),('MAA','Chennai Intl','IN','India','Madras','80.1805','12.9944','5.5',NULL,NULL,'domestic'),('IXM','Madurai','IN','India','Madurai','78.0934','9.83451','5.5',NULL,NULL,'domestic'),('IXE','Mangalore','IN','India','Mangalore','74.8901','12.9613','5.5',NULL,NULL,'domestic'),('MOH','Dibrugarh','IN','India','Mohanbari','95.0169','27.4839','5.5',NULL,NULL,'domestic'),('MYQ','Mysore Airport','IN','India','Mysore','76.6497','12.3072','5.5',NULL,NULL,'domestic'),('NAG','Dr Ambedkar Intl','IN','India','Nagpur','79.0472','21.0922','5.5',NULL,NULL,'domestic'),('IXP','Pathankot','IN','India','Pathankot','75.6346','32.2338','5.5',NULL,NULL,'domestic'),('PBD','Porbandar','IN','India','Porbandar','69.6572','21.6487','5.5',NULL,NULL,'domestic'),('IXZ','Port Blair','IN','India','Port Blair','92.7297','11.6412','5.5',NULL,NULL,'domestic'),('PNQ','Pune','IN','India','Pune','73.9197','18.5821','5.5',NULL,NULL,'domestic'),('RPR','Raipur','IN','India','Raipur','81.7388','21.1804','5.5',NULL,NULL,'domestic'),('RJA','Rajahmundry','IN','India','Rajahmundry','81.8182','17.1104','5.5',NULL,NULL,'domestic'),('RAJ','Rajkot','IN','India','Rajkot','70.7795','22.3092','5.5',NULL,NULL,'domestic'),('IXR','Birsa Munda','IN','India','Ranchi','85.3217','23.3143','5.5',NULL,NULL,'domestic'),('RRK','Rourkela','IN','India','Rourkela','84.8146','22.2567','5.5',NULL,NULL,'domestic'),('','Salem','IN','India','Salem','78.0656','11.7833','5.5',NULL,NULL,'domestic'),('TNI','Satna','IN','India','Satna','80.8549','24.5623','5.5',NULL,NULL,'domestic'),('SHL','Shillong Airport','IN','India','Shillong','91.9787','25.7036','5.5',NULL,NULL,'domestic'),('SSE','Sholapur','IN','India','Sholapur','75.9348','17.628','5.5',NULL,NULL,'domestic'),('SXR','Srinagar','IN','India','Srinagar','74.7742','33.9871','5.5',NULL,NULL,'domestic'),('STV','Surat','IN','India','Surat','72.7418','21.1141','5.5',NULL,NULL,'domestic'),('UDR','Udaipur','IN','India','Udaipur','73.8961','24.6177','5.5',NULL,NULL,'domestic'),('VNS','Varanasi','IN','India','Varanasi','82.8593','25.4524','5.5',NULL,NULL,'domestic'),('VGA','Vijayawada','IN','India','Vijayawada','80.7968','16.5304','5.5',NULL,NULL,'domestic'),('','Zero','IN','India','Zero','93.8281','27.5883','5.5',NULL,NULL,'domestic');
/*!40000 ALTER TABLE `airline_cityinfo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-17 11:55:23
