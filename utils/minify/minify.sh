#!/bin/sh

#USAGE :  sh minify.sh

#This code will overwrite the CSS and JS in given path as new compressed JS and CSS
#It is necessary to make a new folder and execute it there. Otherwise original files will get replaced

#Change path as per your local path
LOCATION=/home/gambit/Desktop/css/*.css

for file in $LOCATION
do
echo "Compressing and Replacing $file"
#You need this jar in your system and change --type to JS or CSS as per requirement
java -jar yuicompressor-2.4.2.jar --type css $file -o $file
done